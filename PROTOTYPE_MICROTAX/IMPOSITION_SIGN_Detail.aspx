﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition_Detail.master" AutoEventWireup="true" CodeFile="IMPOSITION_SIGN_Detail.aspx.cs" Inherits="IMPOSITION_SIGN_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 class="title">อัตราภาษี - ป้าย</h2> <!-- หัวข้อ -->
<a role="button" class="btn btn-default" href="IMPOSITION_SIGN.aspx" style="float:right;margin-top:8px;"><i class="fa fa-arrow-left" style="float:left;margin-right:10px;"></i>ย้อนกลับ</a>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <form action="#" class="form-horizontal row-border"> <!-- form -->
    
        <div class="form-group">
			<label for="" class="control-label col-sm-3">ชื่อประเภท</label>
            <div class="col-sm-7">
                <div class="col-sm-11">
					<input type="text" class="form-control">
                </div>
			</div>
		</div>

        <div class="form-group">
			<label for="" class="control-label col-sm-3">อัตราภาษี</label>
            <div class="col-sm-7">
                <div class="col-sm-5">
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-addon">บาท</span>
				</div>
                </div>
                <label for="" class="control-label col-sm-1" style="text-align: center;">ต่อ</label>
                <div class="col-sm-5">
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-addon">ตร.ซม</span>
				</div>
                </div>

            </div>
        </div>
            

        <div class="form-group">
			<label for="" class="control-label col-sm-3">อัตราภาษีขั้นต่ำ</label>
            <div class="col-sm-7">
                <div class="col-sm-11">
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-addon">&nbsp;บาท&nbsp;</span>
				</div>  
                </div>            
				</div>
        </div>
<div class="panel-footer">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<button class="btn-primary btn">บันทึก</button>
					<button class="btn-default btn">ยกเลิก</button>
				</div>
			</div>
		</div>
</form>
</asp:Content>

