﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_DATA.master" AutoEventWireup="true" CodeFile="INFO_Alley_ListView.aspx.cs" Inherits="INFO_Alley_ListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ซอย /  ตรอก </h3>
     <div class="pull-right" >

            <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="INFO_Alley_DetailView.aspx"><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>  
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<style>
    td,th{
        text-align:center;
    }
</style>


    <div class="row">
        <div class="cennow mt5n"><span class="resultfind">รายการซอย/ตรอก พบ 4 รายการ</span></div>
	<div class="col-md-12">
    			<div class="panel-body p-n ">
                <div class="table-vertical">
                <table  class="table table-striped table-bordered" >
					<thead>
						<tr>
                            <th style="width:10%;">ลำดับ</th>
                            <th style="width:15%;">รหัสซอย</th>
							<th style="width:40%;">ถนน</th>
							<th style="width:15%;">จำนวนซอย</th>
                            <th style="width:15%;">จำนวนแปลงที่ดิน</th>
							<th style="width:5%;visibility: hidden;font-size:3px">เครื่องมือ</th>
						</tr>
					</thead>

						<tr style="background-color:#ffffff">
							<td data-title="ลำดับ" ><input type="text" class="form-control mgb mgb" placeholder="Search..."   /></td>
							<td data-title="รหัสซอย"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
                            <td data-title="ถนน"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="ซอย"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
		        			<td data-title="จำนวนแปลงที่ดิน"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="เครื่องมือ" class="vamiddle">
                               <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt w40b">                                           
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
							   </td>
					   </tr>
					<tbody>
						<tr>
							<td data-title="ลำดับ" >1</td>
							<td data-title="รหัสซอย">001/001</td>
							<td data-title="ถนน">บางขุนเทียน</td>
							   <td data-title="ซอย">ซอยที่หนึ่ง</td>
                               <td data-title="จำนวนแปลงที่ดิน">7 แปลง</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="#"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="INFO_Alley_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
					    	<tr>
							<td data-title="ลำดับ">2</td>
							<td data-title="รหัสซอย">001/002</td>
							<td data-title="ถนน">บางขุนเทียน</td>
							   <td data-title="ซอย">ซอยที่สอง</td>
                               <td data-title="จำนวนแปลงที่ดิน">5 แปลง</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="#"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="INFO_Alley_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">3</td>
							<td data-title="รหัสซอย">002/001</td>
							<td data-title="ถนน">โป๊ะตึกตึก</td>
							   <td data-title="ซอย">ซอยหนึ่ง</td>
                               <td data-title="จำนวนแปลงที่ดิน">12 แปลง</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="#"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="INFO_Alley_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">4</td>
							<td data-title="รหัสซอย">002/002</td>
							<td data-title="ถนน">โป๊ะตึกตึก</td>
							   <td data-title="ซอย">ซอยสองสยอง</td>
                               <td data-title="จำนวนแปลงที่ดิน">10 แปลง</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="#"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="INFO_Alley_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>

					</tbody>
				</table>
				<div class="panel-footer">
                    <ul class="pagination pagination-sm pull-left">
                    <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
                    </ul>
				</div>     
                </div>
            </div>

		</div>
	
</div>





                            <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">เพิ่มข้อมูลติดต่อ</h4>
                            </div>
                            <div class="modal-body" style="padding:0px;">
                            <div class="panel-body p-n" style="font-size: 14px;">                             
                           
                </div>
                                  <div class="modal-footer">
                                  </div>
					              
        </div>
      </div>
      
    </div>
  </div>

</asp:Content>

