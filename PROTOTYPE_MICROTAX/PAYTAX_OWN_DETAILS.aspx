﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_PAYTAX_OWNERSHIP.master" AutoEventWireup="true" CodeFile="PAYTAX_OWN_DETAILS.aspx.cs" Inherits="PAYTAX_OWN_DETAILS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 class="title pull-left">การชำระภาษี</h3>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <style>
            hr{
       padding:0;margin:0;
    }
input[type=checkbox] {
  /* All browsers except webkit*/
  transform: scale(2);

  /* Webkit browsers*/
  -webkit-transform: scale(2);
}

.paddingClass{
    
    margin:20px;
    padding:5px;
}
    </style>

    <form id="form1" runat="server">
<div class="row">
    <div class="col-md-12">

						<table class="table table-bordered" style="margin-bottom:0px;">
								<tr>
									<td width="15%" class="Text_Column_Header">
                                        <span>รหัสเจ้าของ</span>
									</td>
									<td width="25%" class="Text_Column_Value" >
                                        <span><b>#########</b></span> 
									</td>
									<td width="30%" class="Text_Column_Header">
                                        <span>หมายเลขผู้เสียภาษี</span>
									</td>
									<td width="30%" class="Text_Column_Value" >
                                        <span><b>1309901083805</b></span> 
									</td>
								</tr>

                                <tr>
									<td class="Text_Column_Header">
                                        <span>ชื่อ-นามสกุล</span>
									</td>
									<td class="Text_Column_Value" >
                                        <span><b>นาย ณัฐกุล สุวานิช</b></span> 
									</td>
									<td class="Text_Column_Header">
                                        <span>ประเภท</span>
									</td>
									<td class="Text_Column_Value" >
                                        <span><b>บุคคลธรรมดา</b></span> 
									</td>
								</tr>

								<tr>
									<td class="Text_Column_Header">
                                        <span>ที่อยู่</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span>580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span> 
									</td>
								</tr>

                                <tr>
									<td class="Text_Column_Header">
                                        <span>ข้อมูลติดต่อ</span>
									</td>
									<td class="Text_Column_Value" >
                                        <a href="OWN_INDIVIDUAL_DetailView.aspx"><span class="label label-success">VIEW</span></a> 
									</td>
									<td class="Text_Column_Header">
                                        <span>ประวัติการ  ชำระภาษี</span>
									</td>
									<td class="Text_Column_Value" >
                                        <a href="PAYTAX_HISTORY_Listview.aspx"><span class="label label-success">VIEW</span></a> 
									</td>
								</tr>

                                <tr>
									<td class="Text_Column_Header">
                                        <span>มูลค่าทรัพย์สินรวม</span>
									</td>
									<td class="Text_Column_Value" >
                                        <span style="color:red"><b>1,000,000</b></span> 
									</td>
									<td class="Text_Column_Header">
                                        <span>มูลค่าภาษีรวม</span>
									</td>
									<td class="Text_Column_Value" >
                                        <span style="color:red"><b>3,000</b></span> 
									</td>
								</tr>
						</table>                    
					    
             </div>
</div >
    <asp:Panel ID="Panel1" runat="server" Visible="true">
 <div class="panel-body">
				<div class="tab-content">
          <div id="search-classic" class="tab-pane active">
              <form action="" class="form-horizontal row-border">
                <div class="row" >				
<div class="alert alert-info" >
                <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;">ประเภทกรรมสิทธิ์</div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                    	<input type="checkbox" class="tectonic" id="option01" value="01" >
						<label for="option01"></label>
						<span>ทั้งหมด</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option02" value="02" >
						<label for="option02"></label>
						<span>ที่รกร้างว่างเปล่า</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option03" value="03" >
						<label for="option03"></label>
						<span>เกษตรกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option04" value="04" >
						<label for="option04"></label>
						<span>กำลังก่อสร้าง</span></div>
                </div>
			    </div>
            </div>
        <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;"></div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option05" value="02" >
						<label for="option05"></label>
						<span>บ้านพักอาศัย</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option06" value="03" >
						<label for="option06"></label>
						<span>พาณิชยกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option07" value="04" >
						<label for="option07"></label>
						<span>ป้าย</span></div>
                </div>
			    </div>
            </div>
            <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;"></div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                    	</div>
                    <div class="col-md-3">
                    	</div>
                    <div class="col-md-3">
                    	</div>
                </div>
			    </div>
            </div>
      
    </div>
       

   </div>
	        	<div class="form-group">
                 <div class="input-group search-page">
                     <div class="col-md-4" >
			        	<select  class="form-control input-lg">
					<option value="">--</option>        
					<option value="1">-</option>
					<option value="2">-</option>
                    <option value="3">-</option>
			        	</select>
			         </div>
                     <div class="col-lg-8">
                                    <input type="text" placeholder=". . . . ." class="form-control input-lg"> 
                     </div>    
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                    </span>
                 </div>
                </div>
              </form>
              <h3>Displaying results</h3>
       
              <hr />
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_agr"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/l2.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                        
                                                   <div class="titlepay_b">
                                                  <!--      <span style="font-weight: bold;">ประเภทกรรมสิทธิ์&nbsp;</span> -->
                                                        <span style="font-weight: bold;color:green;">01A121/001&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:black;">ที่รกร้างว่างเปล่า&nbsp;&nbsp;</span>
 
                                                  <!--      <span style="color:Black;">(บ้านหลัก)&nbsp;</span>  -->
                                                    </div>	
                                                   <div class="titlepay_c" style="font-size:15px;color:gray;">
                                                        <span>เอกสารสิทธิ์&nbsp;โฉนด&nbsp;·&nbsp;เลขที่&nbsp;32444&nbsp;·</span>
                                                        <span>&nbsp;เลขระวางที่&nbsp;5036II5420-7 &nbsp;·</span>
                                                        <span>&nbsp;หน้าสำรวจที่&nbsp;45039&nbsp;·</span>
                                                        <span>&nbsp;พื้นที่&nbsp;<b>3</b>&nbsp;ไร่&nbsp;<b>2</b>&nbsp;งาน&nbsp;<b>30</b>&nbsp;ตารางวา</span>
                                                    </div>	  
                                        			<div class="titlepay_d">
                                                         <span>63, ธนาคารอาคารสงเคราะห์สำนักงานใหญ่ อาคาร 1, ถนนพระรามที่ 9, แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320</span>
							                        </div>                                      						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>
                                                    
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ราคาประเมิน</span>
                                                        <span class="col-md-2">6,000,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เพดานภาษี</span>
                                                        <span class="col-md-2">5.00</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">อัตราภาษี ( ที่รกร้างว่างเปล่า 1-3 ปี )</span>
                                                        <span class="col-md-2">1.00</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">รวมยอดภาษี</span>
                                                        <span class="col-md-2" style="color:red">60,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>


                                                   <div class="titlepay_c">
                                                           <div class="col-md-5" ><span style="font-weight:bold;color:blue;">ค่าปรับเกินกำหนด ตั้งแต่ วันที่ dd / mm / yyyy</span></div> 
                                                   <!--        <div class="col-md-2" style="text-align:right"><span>550,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>  -->
                                                        <span class="col-md-2">&nbsp;</span>
                                                       <span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เกินกำหนดชำระเป็นจำนวน</span>
                                                        <span class="col-md-2">4</span><span class="col-md-1">เดือน</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ค่าปรับต่อเดือน</span>
                                                        <span class="col-md-2">2.5</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">รวมยอดค่าปรับ ( เกินกำหนด 4 เดือน X ค่าปรับต่อเดือน 2.5 % )</span>
                                                        <span class="col-md-2" style="color:red">6,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                        <span class="col-md-2" style="text-decoration: underline;font-weight:bold;color:red;">66,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>                             
                                            </td>
	                                    	</tr>
                                        	</tbody>
                                           </table>
                                            <div class="spacelineheight">&nbsp;</div>
                                       </a>
              </div>         
                
              <hr />
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_Gray"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/d1.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">พื้นที่กำลังก่อสร้าง&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">01B223/007</span>
                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">63, ธนาคารอาคารสงเคราะห์สำนักงานใหญ่ อาคาร 1, ถนนพระรามที่ 9, แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320</span>
							                        </div>
                                       						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ราคาประเมิน</span>
                                                        <span class="col-md-2">3,000,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เพดานภาษี</span>
                                                        <span class="col-md-2">5.00</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">อัตราภาษี ( อยู่ระหว่างการพัฒนา )</span>
                                                            <span class="col-md-2">0.01</span>
                                                            <span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดภาษี</span>
                                                           <span class="col-md-2" style="color:red;">3,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ค่าปรับเกินกำหนด ตั้งแต่ วันที่ dd / mm / yyyy </span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">เกินกำหนดชำระเป็นจำนวน</span>
                                                           <span class="col-md-2">4</span><span class="col-md-1">เดือน</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ค่าปรับต่อเดือน</span>
                                                           <span class="col-md-2">2.5</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดค่าปรับ ( เกินกำหนด 4 เดือน X ค่าปรับต่อเดือน 2.5 % )</span>
                                                           <span class="col-md-2" style="color:red">300.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                           <span class="col-md-2" style="color:red;text-decoration: underline;font-weight:bold">3,300.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
                                              <hr>
	                                          <div class="spacelineheight">&nbsp;</div>
                                       </a>
              </div>
              <hr>
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_cons"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/l1.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">เกษตรกรรม&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">02C023/123</span>
                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">600-602 แขวง ถนนเพชรบุรี เขต ราชเทวี กรุงเทพมหานคร 10400</span>
							                        </div>
                                       						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ราคาประเมิน</span>
                                                        <span class="col-md-2">74,975,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เพดานภาษี</span>
                                                        <span class="col-md-2">0.20</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">อัตราภาษี ( มูลค่าทรัพย์สินระหว่าง 50ถึง100ล้านบาท )</span>
                                                            <span class="col-md-2">0.05</span>
                                                            <span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดภาษี</span>
                                                           <span class="col-md-2" style="color:red;">374,875.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดค่าปรับ</span>
                                                           <span class="col-md-2" style="color:red">0.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                           <span class="col-md-2" style="color:red;text-decoration: underline;font-weight:bold">374,875.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>                                                    
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                        <div class="spacelineheight">&nbsp;</div>
                                       </a>
              </div>
              <hr>
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_wil"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/l1.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">ป้าย&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">02C023/123</span>
                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">สำนักงานใหญ่ อาคารกรุงเทพประกันภัย. 25 ถนนสาทรใต้ แขวงทุ่งมหาเมฆ เขตสาทร กรุงเทพฯ 10120</span>
							                        </div>
                                       						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">จำนวนด้านของป้าย</span>
                                                        <span class="col-md-2">2</span><span class="col-md-1">ด้าน&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;font-weight:bold">ด้านที่ 1  <span>ประเภท ป้ายมีอักษรไทยล้วน</span> อัตราภาษี</span>
                                                        <span class="col-md-2">3.00</span><span class="col-md-2">บาท / 500 ตร.ซม&nbsp;</span>
                                                        <span class="col-md-2">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">ขนาดของป้าย</span>
                                                            <span class="col-md-2">20,000</span>
                                                            <span class="col-md-1">ตร.ซม</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ป้ายด้านที่ 1 ภาษีรวม    (3.00/500) x 20,000  </span>
                                                           <span class="col-md-2" style="color:red;">120.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                     <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;font-weight:bold">ด้านที่ 2  <span>ประเภท ป้ายไม่มีอักษรไทย</span> อัตราภาษี</span>
                                                        <span class="col-md-2">40.00</span><span class="col-md-2">บาท / 500 ตร.ซม&nbsp;</span>
                                                        <span class="col-md-2">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">ขนาดของป้าย</span>
                                                            <span class="col-md-2">100,000</span>
                                                            <span class="col-md-1">ตร.ซม</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left;">ป้ายด้านที่ 2 ภาษีรวม    (40.00/500) x 100,000  </span>
                                                           <span class="col-md-2" style="color:red;">8,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                           <span class="col-md-2" style="color:red;text-decoration: underline;font-weight:bold">8,120.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                        <div class="spacelineheight">&nbsp;</div>
                                       </a>
              </div>
              <hr />
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_bus"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/l1.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">พาณิชยกรรม&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">02C023/123</span>
                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">อาคารผู้โดยสาร ชั้น 6 (แถว F, ประตูทางเข้าที่ 3) 999 หมู่ 10 ถนน บางนา-ตราด ตำบลราชาเทวะ อำเภอบางพลี จังหวัดสมุทรปราการ 10540</span>
							                        </div>
                                       						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ราคาประเมิน</span>
                                                        <span class="col-md-2">100,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เพดานภาษี</span>
                                                        <span class="col-md-2">2.00</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">อัตราภาษี ( มูลค่าทรัพย์สินน้อยกว่า 20 ล้านบาท )</span>
                                                            <span class="col-md-2">0.30</span>
                                                            <span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดภาษี</span>
                                                           <span class="col-md-2" style="color:red;">300.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดค่าปรับ</span>
                                                           <span class="col-md-2" style="color:red">0.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                           <span class="col-md-2" style="color:red;text-decoration: underline;font-weight:bold">300.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                         <div class="spacelineheight">&nbsp;</div>
                                       </a>
              </div>
              <hr />
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_sht"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="top"  style="width:35px;padding-top:10px">
                                    <input  type="checkbox" name="optiona" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"    valign="top"   style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/l1.jpg" class="img-responsive" style="width:100%;padding-top:10px">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">บ้านพักอาศัย (บ้านหลังหลัก)&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">01D038/321</span>
                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">333/3 ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร 10900</span>
							                        </div>
                                       						                                                                       
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left;color:blue;font-weight: bold;">ยอดชำระประจำปี 2558</span>
                                                        <span class="col-md-2">&nbsp;</span><span class="col-md-1">&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">ราคาประเมิน</span>
                                                        <span class="col-md-2">50,000,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>               
                                                    </div>

                                                    <div class="titlepay_c">
                                                    <div class="col-md-12" style="text-align:right">
                                                        <span class="col-md-6" style="float:left;text-align:left">เพดานภาษี</span>
                                                        <span class="col-md-2">0.50</span><span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span>
                                                        <span class="col-md-3">&nbsp;</span>

                                                    </div>
                                 
                                                    <!--       <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span >600,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    -->
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                            <span class="col-md-6" style="float:left;text-align:left">อัตราภาษี ( มูลค่าทรัพย์สินระหว่าง 50ถึง100ล้านบาท )</span>
                                                            <span class="col-md-2">0.05</span>
                                                            <span class="col-md-1">&nbsp;%&nbsp;&nbsp;</span><span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดภาษี</span>
                                                           <span class="col-md-2" style="color:red;">25,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>

                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">รวมยอดค่าปรับ</span>
                                                           <span class="col-md-2" style="color:red">0.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                        <div class="col-md-12" style="text-align:right">
                                                           <span class="col-md-6" style="float:left;text-align:left">ยอดชำระทั้งหมด</span>
                                                           <span class="col-md-2" style="color:red;text-decoration: underline;font-weight:bold">25,000.00</span><span class="col-md-1">บาท&nbsp;</span>
                                                           <span class="col-md-3">&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-12 " style="text-align:right;padding-right:20px">
                                                         <input type="button" class="btn btn-default"  value="ชำระรายการนี้">
                                                    </div>           
                                                    </div>
                                                    <div class="titlepay_c">
                                                    <div class="col-md-11" style="text-align:right">                                                         
                                                    </div>
                                                    <div class="col-md-1">&nbsp;</div>               
                                                    </div>
 
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
                                            <hr>
	                                         <div class="spacelineheight">&nbsp;</div>
                                       </a>
                                       
              </div>
          <!--    
              <hr>
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_sht"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="middle"  style="width:100px">
                                    <input  type="checkbox" name="optiona" id="opta" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"      style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/h1.jpg" class="img-responsive" style="width:100%;">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">บ้านพักอาศัย&nbsp;(บ้านหลังที่สอง)&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">09E283/443&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:blue;">&nbsp;ยอดชำระประจำปีที่ 2558  &nbsp;</span>

                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">2309/1 ถนน สุขุมวิท แขวง บางจาก เขต พระโขนง กรุงเทพมหานคร 10260</span>
							                        </div>
                                       						                                                                       

                                                    <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span>174,975,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span>บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>อัตราภาษี มูลค่าทรัพย์สิน100ล้านบาทขึ้นไป</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="color:orange;">0.10</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >%</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ยอดชำระทั้งหมด</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="font-weight:bold;color:red">17497500.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              <hr>
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_bus"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="middle"  style="width:100px">
                                    <input  type="checkbox" name="optiona" id="opta" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"      style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/cd2.jpg" class="img-responsive" style="width:100%;">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">พาณิชยกรรม&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">09E283/443&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:blue;">&nbsp;ยอดชำระประจำปีที่ 2558  &nbsp;</span>

                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">เลขที่ 43/2 หมู่ 1 ซอยท่านผู้หญิงฉลวยสุทธิอรรถนฤมนตร์ แขวง สามเสนใน เขต พญาไท กรุงเทพมหานคร 12120</span>
							                        </div>
                                       						                                                                       

                                                    <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span>174,975,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span>บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>อัตราภาษี มูลค่าทรัพย์สิน100ล้านบาทขึ้นไป</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="color:orange;">0.10</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >%</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ยอดชำระทั้งหมด</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="font-weight:bold;color:red">17497500.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              <hr>
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class="NormalText_cons"   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td align="center"  valign="middle"  style="width:100px">
                                    <input  type="checkbox" name="optiona" id="opta" class="paddingClass"   />
                                    </td>
	                        		<td  align="center"      style="width:15%">
                                        <div class="titlepay_b">
                                        <img  src="assets/demo/avatar/s1.jpg" class="img-responsive" style="width:100%;">
                                        </div>
                                     </td>
		                        	<td valign="top" style="padding-left:10px">
                                                   <div class="titlepay_b">

                                                        <span style="font-weight: bold;color:black;">ป้าย&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">09E283/443&nbsp;&nbsp;</span>
                                                        <span style="font-weight: bold;color:blue;">&nbsp;ยอดชำระประจำปีที่ 2558  &nbsp;</span>

                                                   </div>	
							                        <div class="titlepay_d">
                                                         <span style="font-size: 15px;">เลขที่ 43/2 หมู่ 1 ซอยท่านผู้หญิงฉลวยสุทธิอรรถนฤมนตร์ แขวง สามเสนใน เขต พญาไท กรุงเทพมหานคร 12120</span>
							                        </div>
                                       						                                                                       

                                                    <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ราคาประเมิน</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span>174,975,000.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span>บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>อัตราภาษี มูลค่าทรัพย์สิน100ล้านบาทขึ้นไป</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="color:orange;">0.10</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >%</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
                                                   <div class="titlepay_c">
                                                           <div class="col-md-5"><span>ยอดชำระทั้งหมด</span></div> 
                                                           <div class="col-md-2" style="text-align:right"><span style="font-weight:bold;color:red">17497500.00</span></div>
                                                           <div class="col-md-1" style="text-align:right"><span >บาท</span></div>
                                                           <div class="col-md-4">&nbsp;</div>
                                                    </div>
							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              <hr>

               -->
              <div class="search-result" >
                                    <a  style="text-decoration:none !important;">
                                    <table   width="100%"  class=""   >
	                            	<tbody>
                                    
                                    <tr>
                                    <td colspan="3">
                                        <div class="col-md-4">
                                            <input  type="checkbox" name="optiona"    /> <span class="titlepay_c ml5">เลือกรายการทั้งหมด</span>
                                        </div>
                                        <div class="col-md-3" style="text-align:right">
                                            <span class="titlepay_b">ส่วนรวมทั้งหมดที่ต้องจ่าย</span>
                                        </div>
                                        <div class="col-md-2" style="text-align:right">
                                            <span class="titlepay_b" style="color:red">456,000.00</span>
                                        </div>
                                        <div class="col-md-1" style="text-align:center">
                                            <span class="titlepay_b">บาท</span>
                                        </div>
                                        <div class="col-md-2"  style="text-align:right;padding-right:20px">
                                            <asp:Button ID="Button1" CssClass="btn btn-primary ml5" runat="server" Text="จ่ายทั้งหมด" PostBackUrl="~/PAYTAX_INVOICE.aspx" />
                                        </div>
                                    </td>
	                                    	</tr>
                                        	</tbody></table>
                                       </a>
              </div>
              
              <ul class="pagination pagination-sm pull-left">
                <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
                  
              </ul>
          </div>

          
        </div>
		</div>
	
</asp:Panel>

</form>

</asp:Content>

