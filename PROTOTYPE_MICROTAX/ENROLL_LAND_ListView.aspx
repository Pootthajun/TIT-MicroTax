﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Enroll.master" AutoEventWireup="true" CodeFile="ENROLL_LAND_ListView.aspx.cs" Inherits="ENROLL_LAND_ListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> <h3 class="title pull-left">ที่ดิน </h3>
    <div class="pull-right" >

            <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="ENROLL_LAND_DetailView.aspx"><i class="ion-android-create lr10" ></i>เพิ่มข้อมูล</a>  
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
 




<style>
    td,th{
        text-align:center;
    }
</style>
    <div class="row">
             <div class="cennow mt5n"><span class="resultfind">รายการที่ดิน พบ 8 รายการ</span></div>
	<div class="col-md-12">
    			<div class="panel-body p-n">
                <div class="table-vertical">
                <table  class="table table-striped table-bordered" >
					<thead>
						<tr>
                            <th style="width:10%;">ลำดับ</th>
                            <th style="width:10%;">รหัสที่ดิน</th>
							<th style="width:25%;">เจ้าของกรรมสิทธิ์</th>
							<th style="width:20%;">เอกสารสิทธิ์</th>
							<th style="width:15%;">เนื้อที่ทั้งหมด</th>
                            <th style="width:15%;">ราคาประเมิน</th>
							<th style="width:5%;visibility: hidden;font-size:3px">เครื่องมือ</th>
						</tr>
					</thead>

						<tr style="background-color:#ffffff">
							<td data-title="ลำดับ" ><input type="text" class="form-control mgb" placeholder="Search..."  style="display:none" /></td>
							<td data-title="รหัสที่ดิน"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
                            <td data-title="เจ้าของกรรมสิทธิ์"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="เอกสารสิทธิ์"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="เนื้อที่ทั้งหมด"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
		        			<td data-title="ราคาประเมิน"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="เครื่องมือ" class="vamiddle">
                               <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt w40b">                                           
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
							   </td>
					   </tr>
					<tbody>
						<tr>
							<td data-title="ลำดับ" >1</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A01 111/222</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย บางขุนเทียน เกรียนแตก</td>
							   <td data-title="เอกสารสิทธิ์">โฉนด เลขที่ 11</td>
                               <td data-title="เนื้อที่ทั้งหมด">3 - 4 - 1</td>
                               <td data-title="ราคาประเมิน">50,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
					    	<tr>
							<td data-title="ลำดับ">2</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A02 112/502</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย เกรียนแตก แบกปูน</td>
							   <td data-title="เอกสารสิทธิ์">โฉนด เลขที่ 2</td>
                               <td data-title="เนื้อที่ทั้งหมด">5 - 3 - 1</td>
                               <td data-title="ราคาประเมิน">34,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">3</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/102</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย แบกปูน ไปโบกตึก</td>
							   <td data-title="เอกสารสิทธิ์">นส. ๓ เลขที่ 12</td>
                               <td data-title="เนื้อที่ทั้งหมด">6 - 3 - 1</td>
                               <td data-title="ราคาประเมิน">61,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">4</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/301</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย ไปโบกตึก อย่างบึกบึน</td>
							   <td data-title="เอกสารสิทธิ์">สด. ๙ เลขที่ 7</td>
                               <td data-title="เนื้อที่ทั้งหมด">6 - 2 - 4</td>
                               <td data-title="ราคาประเมิน">4,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
                        <tr>
							<td data-title="ลำดับ" >5</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A01 111/222</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย บางขุนเทียน เกรียนแตก</td>
							   <td data-title="เอกสารสิทธิ์">โฉนด เลขที่ 11</td>
                               <td data-title="เนื้อที่ทั้งหมด">3 - 4 - 1</td>
                               <td data-title="ราคาประเมิน">50,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
					    	<tr>
							<td data-title="ลำดับ">6</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A02 112/502</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย เกรียนแตก แบกปูน</td>
							   <td data-title="เอกสารสิทธิ์">โฉนด เลขที่ 2</td>
                               <td data-title="เนื้อที่ทั้งหมด">5 - 3 - 1</td>
                               <td data-title="ราคาประเมิน">34,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">7</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/102</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย แบกปูน ไปโบกตึก</td>
							   <td data-title="เอกสารสิทธิ์">นส. ๓ เลขที่ 12</td>
                               <td data-title="เนื้อที่ทั้งหมด">6 - 3 - 1</td>
                               <td data-title="ราคาประเมิน">61,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">8</td>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/301</td>
							<td data-title="เจ้าของกรรมสิทธิ์" class="leftnow">นาย ไปโบกตึก อย่างบึกบึน</td>
							   <td data-title="เอกสารสิทธิ์">สด. ๙ เลขที่ 7</td>
                               <td data-title="เนื้อที่ทั้งหมด">6 - 2 - 4</td>
                               <td data-title="ราคาประเมิน">4,000,000</td>
							   <td data-title="เครื่องมือ" class="vamiddle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="ENROLL_Land_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>

					</tbody>
				</table>
				<div class="panel-footer">
                    <ul class="pagination pagination-sm pull-left">
                    <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
                    </ul>
				</div>     
                </div>
            </div>

		</div>
	
</div>
</asp:Content>

