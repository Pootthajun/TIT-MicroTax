﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_OwnerShip.master" AutoEventWireup="true" CodeFile="OWN_GOVERNMENT_ListView.aspx.cs" Inherits="OWN_GOVERNMENT_ListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">รัฐบาล </h3>
    <div class="pull-right">
            <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="OWN_GOVERNMENT_DetailView.aspx"><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>  
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<style>
    td,th{
        text-align:center;
    }
</style>


    <div class="row">
                    <div class="cennow mt5n"><span class="resultfind">รายการรัฐบาล พบ 4 รายการ</span></div>
	<div class="col-md-12">
    			<div class="panel-body p-n">
                <div class="table-vertical">
                <table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th style="width:10%;">ลำดับ</th>
                            <th style="width:20%;">ชื่อ - รัฐบาล</th>
							<th style="width:20%;">เลขที่ทะเบียน</th>
							<th style="width:45%;">ที่อยู่</th>
							<th style="width:5%;visibility: hidden;font-size:3px">เครื่องมือ</th>
						</tr>
					</thead>

						<tr style="background-color:#ffffff">
							<td data-title="ลำดับ" ><input type="text" class="form-control" placeholder="Search..."  style="display:none" /></td>
                            <td data-title="ชื่อ - รัฐบาล"><input type="text" class="form-control" placeholder="Search..."   /></td>
							<td data-title="เลขที่ทะเบียน"><input type="text" class="form-control" placeholder="Search..."   /></td>
		        			<td data-title="ที่อยู่"><input type="text" class="form-control" placeholder="Search..."   /></td>
							<td data-title="เครื่องมือ" class="vamiddle"
                               <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt w40b">                                           
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
							   </td>
					   </tr>
					<tbody>
						<tr>
							<td data-title="ลำดับ" >1</td>
							<td data-title="ชื่อ - รัฐบาล">ชื่อทดสอบ1</td>
							   <td data-title="เลขที่ทะเบียน">1309984759689</td>
                               <td data-title="ที่อยู่">63, ธนาคารอาคารสงเคราะห์สำนักงานใหญ่ อาคาร 1, ถนนพระรามที่ 9, แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร, 10320</td>
							   <td data-title="เครื่องมือ" class="vamiddle"
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="OWN_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="OWN_GOVERNMENT_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>	
                            
                               </tr>
					    	<tr>
							<td data-title="ลำดับ">2</td>
							<td data-title="ชื่อ - รัฐบาล">ชื่อทดสอบ2</td>
							   <td data-title="เลขที่ทะเบียน">3490989098791</td>
                               <td data-title="ที่อยู่">905 ซอย ลุมพินี นวมินทร์ แขวง คลองจั่น เขต บางกะปิ กรุงเทพมหานคร 10240</td>
							   <td data-title="เครื่องมือ" class="vamiddle"
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="OWN_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="OWN_GOVERNMENT_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">3</td>
							<td data-title="ชื่อ - รัฐบาล">ชื่อทดสอบ3</td>
							   <td data-title="เลขที่ทะเบียน">3490989098791</td>
                               <td data-title="ที่อยู่">905 ซอย ลุมพินี นวมินทร์ แขวง คลองจั่น เขต บางกะปิ กรุงเทพมหานคร 10240</td>
							   <td data-title="เครื่องมือ" class="vamiddle"
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="OWN_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="OWN_GOVERNMENT_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>
					    	<tr>
							<td data-title="ลำดับ">4</td>
							<td data-title="ชื่อ - รัฐบาล">ชื่อทดสอบ4</td>
							   <td data-title="เลขที่ทะเบียน">1309889092323</td>
                               <td data-title="ที่อยู่">319, อาคารจามจุรีสแควร์ ชั้น 19, ถนนพญาไท, เแขวงปทุมวัน เขตปทุมวัน กรุงเทพมหานคร, 10330</td>
							   <td data-title="เครื่องมือ" class="vamiddle"
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                         
				    	                <li><a href="OWN_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="OWN_GOVERNMENT_DetailView.aspx"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>
						   </tr>

					</tbody>
				</table>
				<div class="panel-footer">
                    <ul class="pagination pagination-sm pull-left">
                    <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
                    </ul>
				</div>     
                </div>
            </div>

		</div>
	
</div>



</asp:Content>
