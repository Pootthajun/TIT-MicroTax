﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Enroll.master" AutoEventWireup="true" CodeFile="ENROLL_View.aspx.cs" Inherits="ENROLL_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ข้อมูลทะเบียนทรัพย์</h3> <!-- หัวข้อ -->
<a role="button" class="btn btn-default tr8 fr" href="ENROLL_USE_ListView.aspx"><i class="fa fa-arrow-left lr10"></i>ย้อนกลับ</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<link type="text/css" href="assets/plugins/jsgrid/css/demos.css" rel="stylesheet">
<link type="text/css" href="assets/plugins/jsgrid/css/jsgrid.css" rel="stylesheet">
<link type="text/css" href="assets/plugins/jsgrid/css/theme.css" rel="stylesheet">

    <br/>
    <legend><b>รายละเอียดกรรมสิทธิ์</b></legend>
    <div class="row">

    <div class="col-md-3">

        <img src="assets/demo/avatar/60.jpg" class="img-responsive" style="height:250px;margin-right:auto;">
    </div>
    <div class="col-md-9">
						<table class="table table-bordered" style="margin-bottom:0px;">
								<tbody><tr>
									<td class="Text_Column_Header" width="15%">
                                        <span>รหัสที่ดิน</span>
									</td>
									<td class="Text_Column_Value" width="25%">
                                        <span><b>#########</b></span> 
									</td>
									<td class="Text_Column_Header" width="30%">
                                        <span>เนื้อที่ทั้งหมด</span>
									</td>
									<td class="Text_Column_Value" width="30%">
                                        <span><b>13 ไร่ 5 งาน 8 ตารางวา</b></span> 
									</td>
								</tr>

                                <tr>
									<td class="Text_Column_Header">
                                        <span>เอกสารสิทธิ์</span>
									</td>
									<td class="Text_Column_Value">
                                        <span><b>โฉนด เลขที่ 13</b></span> 
									</td>
									<td class="Text_Column_Header">
                                        <span>ประเภทกรรมสิทธิ์</span>
									</td>
									<td class="Text_Column_Value">
                                        <span><b>ที่ดิน</b></span> 
									</td>
								</tr>

								<tr>
									<td class="Text_Column_Header">
                                        <span>ที่อยู่</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span>580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span> 
									</td>
								</tr>
                                <tr>
									<td class="Text_Column_Header">
                                        <span>ราคาประเมิน</span>
									</td>
									<td class="Text_Column_Value">
                                        <span><b>4,450,000 บาท</b></span> 
									</td>
									<td class="Text_Column_Header">
                                        <span>ภาษีจากประเมิน</span>
									</td>
									<td class="Text_Column_Value">
                                        <span><b>50,000 บาท</b></span> 
									</td>
								</tr>                                    
                                <tr>
									<td class="Text_Column_Header">
                                        <span>ผู้ครอบครอง</span>
									</td>
									<td class="Text_Column_Value">
                                        <span><b>นาย ศิวนัทธ์ ประดิษฐ์</b></span> 
									</td>
                                </tr>
						</tbody></table>                    
					    
             </div>

</div>
        

    
    <br/>
    <legend><b>ประวัติผู้ถือครอง</b></legend>
    <div class="row">
	<div class="col-md-12">
    			<div class="panel-body p-n" style="font-size: 16.5px;text-align:center;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="background:#73ABE3;">
							<th style="width:100px;text-align:center;">หมายเลขผู้เสียภาษี</th>
							<th style="width:170px;text-align:center;">ชื่อผู้ถือครอง</th>
							<th style="width:100px;text-align:center;">วันที่ถือครอง</th>
							<th style="width:100px;text-align:center;">วันที่สิ้นสุด</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ถือครอง">นาย สมนึก ศึกสงคราม</td>
                               <td data-title="วันที่ถือครอง">15 มกราคม 2551</td>
                               <td data-title="วันที่สิ้นสุด">5 ธันวาคม 2553</td>
					
                            
                               </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ถือครอง">นาย สมหมาย เพื่อนขายตรง</td>
                               <td data-title="วันที่ถือครอง">5 ธันวาคม 2553</td>
                               <td data-title="วันที่สิ้นสุด">3 เมษายน 2555</td>
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ถือครอง">นาย ศิวนัทธ์ ประดิษฐ์</td>
                               <td data-title="วันที่ถือครอง">3 เมษายน 2555</td>
                               <td data-title="วันที่สิ้นสุด">-</td>
						   </tr>

					</tbody>
				</table>
                </div>
            </div>

		</div>
	
</div>





    
    <br/>
    <legend><b>ประวัติการชำระภาษี</b></legend>
    <div class="row">
	<div class="col-md-12">
    			<div class="panel-body p-n" style="font-size: 16.5px;text-align:center;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="background:#73ABE3;">
							<th style="width:130px;text-align:center;">หมายเลขผู้เสียภาษี</th>
							<th style="width:120px;text-align:center;">ชื่อผู้ชำระภาษี</th>
							<th style="width:120px;text-align:center;">ผู้รับเงิน</th>
							<th style="width:100px;text-align:center;">วันที่ชำระ</th>
							<th style="width:100px;text-align:center;">ยอดที่ชำระ(บาท)</th>            
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นาย จรัสแสง แดงดี</td>
                               <td data-title="วันที่ชำระ">15 เมษายน 2551</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
                               </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นาย ศิวนัทธ์ ประดิษฐ์</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2552</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2553</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>		
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมหมาย เพื่อนขายตรง</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">2 เมษายน 2554</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>	
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย ศิวนัทธ์ ประดิษฐ์</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">25 เมษายน 2555</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
						   </tr>

					</tbody>
				</table>
                </div>
            </div>
		</div>	
</div>      
</asp:Content>

