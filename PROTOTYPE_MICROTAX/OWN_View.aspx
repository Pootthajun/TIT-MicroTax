﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_OwnerShip.master" AutoEventWireup="true" CodeFile="OWN_View.aspx.cs" Inherits="OWN_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ข้อมูลเจ้าของกรรมสิทธิ์</h3> <!-- หัวข้อ -->
<a role="button" class="btn btn-default tr8 fr" href="#"><i class="fa fa-arrow-left lr10"></i>ย้อนกลับ</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <br/>
    <legend><b>รายละเอียดเจ้าของกรรมสิทธิ์</b></legend>
    <div class="row">
    <div class="col-md-12">
        <br/>
						<table class="table table-bordered" style="margin-bottom:0px;">
								<tbody><tr>
									<td class="Text_Column_Header" style="width:10%">
                                        <span>รหัสเจ้าของ</span>
									</td>
									<td class="Text_Column_Value" style="width:10%">
                                        <span><b>#########</b></span> 
									</td>
									<td class="Text_Column_Header" style="width:10%">
                                        <span>หมายเลขผู้เสียภาษี</span>
									</td>
									<td class="Text_Column_Value" style="width:10%">
                                        <span><b>1309901083805</b></span> 
									</td>
                                    
									<td class="Text_Column_Header" style="width:10%">
                                        <span>ชื่อ-นามสกุล</span>
									</td>
									<td class="Text_Column_Value" style="width:10%">
                                        <span><b>นาย ณัฐกุล สุวานิช</b></span> 
									</td>
                                </tr>

								<tr>
									<td class="Text_Column_Header">
                                        <span>ที่อยู่</span>
									</td>
									<td class="Text_Column_Value" colspan="5">
                                        <span>580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span> 
									</td>
								</tr>
						</tbody></table>                    
					    
             </div>
</div>
        

    
    <br/>
    <legend><b>ทรัพย์สินที่ถือครอง</b></legend>
    <div class="row">
	<div class="col-md-12">
    			<div class="panel-body p-n" style="font-size: 16.5px;text-align:center;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="background:#73ABE3;">
							<th style="width:50px;text-align:center;">รหัสที่ดิน</th>
							<th style="width:270px;text-align:center;">ที่ตั้ง</th>
							<th style="width:100px;text-align:center;">ประเภท</th>
							<th style="width:100px;text-align:center;">เนื้อที่ทั้งหมด</th>
							<th style="width:120px;text-align:center;">ราคาประเมิน(บาท)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A01 111/222</td>
							   <td data-title="ที่ตั้ง">จังหวัดนครราชสีมา ตำบลท่าช้าง หมู่บ้านเฉลิมพระเกียรติ</td>
							   <td data-title="ประเภท">บ้านพักอาศัย</td>
                               <td data-title="เนื้อที่ทั้งหมด">5-12-4</td>
                               <td data-title="ราคาประเมิน(บาท)">50,000,000</td>
                            
                               </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A02 112/502</td>
							   <td data-title="ที่ตั้ง">จังหวัดนครราชสีมา ตำบลท่าช้าง หมู่บ้านเฉลิมพระเกียรติ</td>
							   <td data-title="ประเภท">อาคารพาณิชย์</td>
                               <td data-title="เนื้อที่ทั้งหมด">10-2-5</td>
                               <td data-title="ราคาประเมิน(บาท)">50,000,000</td>
						   </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/102</td>
							   <td data-title="ที่ตั้ง">จังหวัดนครราชสีมา ตำบลท่าช้าง กมู่บ้านเฉลิมพระเกียรติ</td>
							   <td data-title="ประเภท">ที่ดิน</td>
                               <td data-title="เนื้อที่ทั้งหมด">6-3-5</td>
                               <td data-title="ราคาประเมิน(บาท)">50,000,000</td>
						   </tr>

					</tbody>
				</table>
                </div>
            </div>

		</div>
	
</div>





    
    <br/>
    <legend><b>ประวัติการชำระภาษี</b></legend>
    <div class="row">
	<div class="col-md-12">
    			<div class="panel-body p-n" style="font-size: 16.5px;text-align:center;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="background:#73ABE3;">
							<th style="width:50px;text-align:center;">รหัสที่ดิน</th>
							<th style="width:90px;text-align:center;">รายการภาษี</th>
							<th style="width:90px;text-align:center;">งวดที่ชำระ</th>
							<th style="width:140px;text-align:center;">ผู้รับเงิน</th>
							<th style="width:110px;text-align:center;">วันที่ชำระ</th>
							<th style="width:120px;text-align:center;">ยอดที่ชำระ(บาท)</th>      
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A01 111/222</td>
							   <td data-title="รายการภาษี">อาคารพาณิชย์</td>
							   <td data-title="งวดที่ชำระ">ประจำปี 2553</td>
							   <td data-title="ผู้รับเงิน">นาย จรัสแสง แดงดี</td>
                               <td data-title="วันที่ชำระ">15 เมษายน 2553</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>		
                            
                               </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A02 112/502</td>
							   <td data-title="รายการภาษี">อาคารพาณิชย์</td>
							   <td data-title="งวดที่ชำระ">ประจำปี 2554</td>
							   <td data-title="ผู้รับเงิน">นาย ศิวนัทธ์ ประดิษฐ์</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2554</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
						   </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/102</td>
							   <td data-title="รายการภาษี">ที่ดิน</td>
							   <td data-title="งวดที่ชำระ">ประจำปี 2554</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2554</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
						   </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/301</td>
							   <td data-title="รายการภาษี">อาคารพาณิชย์</td>
							   <td data-title="งวดที่ชำระ">ประจำปี 2555</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">2 เมษายน 2555</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>	
						   </tr>
					    	<tr>
							<td data-title="รหัสที่ดิน" class="leftnow">A03 114/301</td>
							   <td data-title="รายการภาษี">ป้าย</td>
							   <td data-title="งวดที่ชำระ">ประจำปี 2555</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">25 เมษายน 2555</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
						   </tr>

					</tbody>
				</table>
                </div>
            </div>
		</div>	
</div>      
</asp:Content>

