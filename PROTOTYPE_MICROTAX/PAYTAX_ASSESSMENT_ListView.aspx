﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_PAYTAX.master" AutoEventWireup="true" CodeFile="PAYTAX_ASSESSMENT_ListView.aspx.cs" Inherits="PAYTAX_ASSESSMENT_ListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">การชำระภาษี</h3>
        <div class="dropdown pull-right" style="margin-top:14px;margin-right:8px;">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o" style="float:left;margin-right:10px;"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
    
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <style>
        table {
    display: table;
    border-collapse: separate;
    border-spacing: 2px;
    border-color: grey;
    
             }
    .row{
    margin-left:0px;
    margin-right:0px;
    }
    hr{
       padding:0;margin:0;
    }
    </style>
<form id="form1" runat="server" class="form-horizontal row-border">
<div class="row" >				
<div class="alert alert-info" >
                <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;">ประเภทกรรมสิทธิ์</div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                    	<input type="checkbox" class="tectonic" id="option01" value="01" >
						<label for="option01"></label>
						<span>ทั้งหมด</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option02" value="02" >
						<label for="option02"></label>
						<span>ที่รกร้างว่างเปล่า</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option03" value="03" >
						<label for="option03"></label>
						<span>เกษตรกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option04" value="04" >
						<label for="option04"></label>
						<span>กำลังก่อสร้าง</span></div>
                </div>
			    </div>
            </div>
        <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;"></div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option05" value="02" >
						<label for="option05"></label>
						<span>บ้านพักอาศัย</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option06" value="03" >
						<label for="option06"></label>
						<span>พาณิชยกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option07" value="04" >
						<label for="option07"></label>
						<span>ป้าย</span></div>
                </div>
			    </div>
            </div>
            <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;"></div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                    	</div>
                    <div class="col-md-3">
                    	</div>
                    <div class="col-md-3">
                    	</div>
                </div>
			    </div>
            </div>
      
    </div>
       

   </div>
<asp:Panel ID="tab1" runat="server" Visible="true">
        
            <asp:Panel ID="Panel1" runat="server" Visible="true">    
<div class="panel-body">
				<div class="tab-content">
          <div id="search-classic" class="tab-pane active">
        <form action="" class="form-horizontal row-border">
	        	<div class="form-group">
                 <div class="input-group search-page">
                     <div class="col-md-4" >
			        	<select id="mark" name="mark" class="form-control input-lg">
					<option value="">--</option>        
					<option value="1">บุคคลธรรมดา</option>
					<option value="2">บริษัท</option>
                    <option value="3">ห้างหุ้นส่วน</option>
                    <option value="4">รัฐบาล</option>
                    <option value="5">รัฐวิสาหกิจ</option>
                    <option value="6">สมาคม</option>
                    <option value="7">มูลนิธิ</option>
                    <option value="8">วัด</option>
			        	</select>
			         </div>
                     <div class="col-lg-8">
                                    <input type="text" placeholder="ชื่อ . . . ." class="form-control input-lg"> 
                     </div>    
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                    </span>
                 </div>
                </div>
              </form>
            
              <h3>ผลลัพธ์ทั้งหมด 10 รายการ</h3>
              <hr>
              <div class="search-result" onmouseover="this.style.backgroundColor='#efefef';" onmouseout="this.style.backgroundColor='';" >
                                    <a href="PAYTAX_OWN_DETAILS.aspx"  target="_blank" style="text-decoration:none !important;">
                                    <table   class="pay_detail">
	                            	<tbody><tr>
		                        	<td valign="top">
                                                   <div class="titlepay_b">
                                                        <span style="font-weight: bold;">1-3099-01083-8-05&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">นาย ณัฐกุล สุวานิช</span>
                                                        <span style="color:Black;">(บุคคลธรรมดา)&nbsp;</span>
                                                    </div>
							                        <div class="titlepay_b">
								                        <span class="st">
                                                         <span style="color:black;font-size: 16px;">580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span>
								                        </span>
							                        </div>
							                        <div class="titlepay_b"> 
                                                            <div class="row">
                                                            <div class="col-md-3">
                                                            <span style="font-size:16px">กรรมสิทธิ์ที่ถือครอง&nbsp;&nbsp;</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6" ><span style="color:teal;font-size:14px"><asp:Label ID="Label14" runat="server" Text="บ้านพักอาศัย"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label15" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label16" runat="server" Text="พาณิชยกรรม"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label17" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label18" runat="server" Text="ป้าย"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label19" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            </div>
							                        </div>
                                                    <div class="titlepay_b"> 
                                                            <div class="row">
                                                            <div class="col-md-3">

                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label20" runat="server" Text="เกษตรกรรม"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label21" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label22" runat="server" Text="ที่รกร้างว่างเปล่า"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label23" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label24" runat="server" Text="กำลังพัฒนา"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label25" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
							                               </div>
                                                    </div>

							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              <div class="search-result" onmouseover="this.style.backgroundColor='#efefef';" onmouseout="this.style.backgroundColor='';" >
                                    <a href="PAYTAX_OWN_DETAILS.aspx"  target="_blank" style="text-decoration:none !important;">
                                    <table  class="pay_detail">
	                            	<tbody><tr>
		                        	<td valign="top">
                                                   <div class="titlepay_b">
                                                        <span style="font-weight: bold;">1-3099-01083-8-05&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">นาย ณัฐกุล สุวานิช</span>
                                                        <span style="color:Black;">(บุคคลธรรมดา)&nbsp;</span>
                                                    </div>
							                        <div class="titlepay_b">
								                        <span class="st">
                                                         <span style="color:black;font-size: 16px;">580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span>
								                        </span>
							                        </div>
							                        <div class="titlepay_b"> 
                                                            <div class="row">
                                                            <div class="col-md-3">
                                                            <span style="font-size:16px">กรรมสิทธิ์ที่ถือครอง&nbsp;&nbsp;</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label1" runat="server" Text="บ้านพักอาศัย"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label2" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label3" runat="server" Text="พาณิชยกรรม"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label5" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label6" runat="server" Text="ป้าย"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label7" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
							                               </div>
                                                    </div>
                                                 
							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              <div class="search-result" onmouseover="this.style.backgroundColor='#efefef';" onmouseout="this.style.backgroundColor='';" >
                                    <a href="PAYTAX_OWN_DETAILS.aspx"  target="_blank" style="text-decoration:none !important;">
                                    <table    class="pay_detail" >
	                            	<tbody><tr>
		                        	<td valign="top">
                                                   <div class="titlepay_b">
                                                        <span style="font-weight: bold;">1-3099-01083-8-05&nbsp;</span>
                                                        <span style="font-weight: bold;color:green;">นาย ณัฐกุล สุวานิช</span>
                                                        <span style="color:Black;">(บุคคลธรรมดา)&nbsp;</span>
                                                    </div>
							                        <div class="titlepay_b">
								                        <span class="st">
                                                         <span style="color:black;font-size: 16px;">580, ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span>
								                        </span>
							                        </div>
							                        <div class="titlepay_b"> 
                                                            <div class="row">
                                                            <div class="col-md-3">
                                                            <span style="font-size:16px">กรรมสิทธิ์ที่ถือครอง&nbsp;&nbsp;</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                            <div class="col-md-6" style="text-"><span style="color:teal;font-size:14px"><asp:Label ID="Label26" runat="server" Text="บ้านพักอาศัย"></asp:Label></span></div>
                                                            <div class="col-md-6"><span style="color:teal;font-size:14px"><asp:Label ID="Label27" runat="server" Text="1"></asp:Label></span></div>
                                                            </div>
							                               </div>
                                                    </div>

							                        <div class="spacelineheight">&nbsp;</div>
                                            </td>
	                                    	</tr>
                                        	</tbody></table>
	                                   
                                       </a>
              </div>
              

              <hr>
              <ul class="pagination pagination-sm pull-left">
                <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
              </ul>
          </div>

          
        </div>
                </div>
                
            </asp:Panel>





 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-xl" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <div class="titlepay_b">
                                                        <span style="font-weight: bold;">บุคคลธรรมดา &nbsp;</span>
                                                        <span style="font-weight: bold;color:black;">1-3099-01083-8-05 &nbsp;</span>
                                                        <span style="color:Green;font-weight: bold;">นาย ณัฐกุล สุวานิช&nbsp;</span>        
                                                   </div>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>
</asp:Panel>
    </form>
</asp:Content>

