﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition.master" AutoEventWireup="true" CodeFile="IMPOSITION_FARM_Listview.aspx.cs" Inherits="IMPOSITION_FARM_Listview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">เกษตรกรรม</h3>
      <div class="pull-right" >
             <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="IMPOSITION_Detailview.aspx" ><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<style>
    td,th{
        text-align:center;
    }
</style>


    <div class="row">
    <h4 style="text-align:center"><b>เกษตรกรรม</b> อัตราเพดาน 0.2 %</h4> 
    <div class="col-md-1"></div>
	<div class="col-md-10">
            
    			<div class="panel-body p-n" style="font-size: 16.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead style="background-color:skyblue">
						<tr>
							<th style="width:50px;">ลำดับ</th>
							<th style="width:300px;">มูลค่าทรัพย์สิน(บาท)</th>
							<th style="width:40px;">คิดภาษี(%)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
                               <td data-title="ลำดับ">1</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">ต่ำกว่า 50,000,000</td>
                               <td data-title="คิดภาษี(%)">0%</td>
							   
                               </tr>
					    	<tr>
                               <td data-title="ลำดับ">2</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">50,000,000 ถึง 100,000,000</td>
                               <td data-title="คิดภาษี(%)">0.05%</td>
							   
						   </tr>
					    	<tr>
                               <td data-title="ลำดับ">3</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">มากกว่า 100,000,000</td>
                               <td data-title="คิดภาษี(%)">0.10%</td>
							   
						   </tr>

					</tbody>
				</table>
				<div class="panel-footer"></div>     
                </div>
            </div>

		</div>
	    <div class="col-md-1"></div>
</div>

</asp:Content>

