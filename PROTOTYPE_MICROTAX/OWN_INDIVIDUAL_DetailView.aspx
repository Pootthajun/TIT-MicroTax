﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_OwnerShip_Detail.master" AutoEventWireup="true" CodeFile="OWN_INDIVIDUAL_DetailView.aspx.cs" Inherits="OWN_INDIVIDUAL_DetailView" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">    เจ้าของกรรมสิทธิ์ - บุคคลธรรมดา</h3> <!-- หัวข้อ -->
<a role="button" class="btn btn-default tr8 fr" href="OWN_INDIVIDUAL_ListView.aspx"><i class="fa fa-arrow-left lr10" ></i>ย้อนกลับ</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> <!-- ข้อความ -->
   				<div class="tab-content">
					<div class="tab-pane active" id="timeline">
						<ul class="timeline">
							<li class="timeline-success">
								<div class="timeline-icon hidden-xs">1</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                    <form class="grid-form">


			<fieldset>
				<legend>บุคลลธรรมดา  ( รหัสเจ้าของ: <u style="color:blue;">ยังกรอกข้อมูลไม่ครบ</u> )</legend>
				<div data-row-span="5">
					<div data-field-span="1"  >
					    <label>&nbsp;</label>
						<select class="dlhide h37"  >
				                                <option>คำนำหน้า</option>
				                                <option>นาย</option>
				                                <option>นาง</option>
				                                <option>นางสาว</option>
						</select>
					</div>
					<div data-field-span="2"    >
				    <label>&nbsp;</label>
						<input type="text" placeholder="ชื่อ"  class="f20">
					</div>
					<div data-field-span="2"    >
				    <label>&nbsp;</label>
						<input type="text" placeholder="นามสกุล"  class="f20">
					</div>
				</div>
				<div data-row-span="3">
					<div data-field-span="1"   >
						<label id="lb1">&nbsp;</label> 
                        <input id="inp1" type="text"  onfocusout="showlabel('inp1','lb1','หมายเลขบัตรประชาชน');"  class="form-control mask f20" data-inputmask="'mask':'9  -  9999  -  99999  -  9  -  99'" placeholder="หมายเลขบัตรประชาชน">
					</div>

				<!--	<div data-field-span="1" class="h64">
						<label>&nbsp;</label>
						<input type="text" disabled="disabled" placeholder="รหัสเจ้าของกรรมสิทธิ์">
					</div> -->
				</div>
                </fieldset>
                <div class="clearfix pt20">
				<div class="pull-right">
                <br><br><br>
				</div>
			    </div>
                </form>

                </div>
                </div>
                  
            </li>

                            <!-- 2-->

							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">2</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">
                                        <fieldset>
			            	<legend>ที่อยู่</legend>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb2">&nbsp;</label>
							                    <input id="inp2" type="text" placeholder="บ้านเลขที่" onfocusout="showlabel('inp2','lb2','บ้านเลขที่');" >
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb3">&nbsp;</label>
							                    <input id="inp3" type="text" placeholder="หมู่บ้าน/ชื่อสถานที่" onfocusout="showlabel('inp3','lb3','หมู่บ้าน/ชื่อสถานที่');">
						                    </div>
					                    </div>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb4">&nbsp;</label>
							                    <input id="inp4" type="text" placeholder="ซอย" onfocusout="showlabel('inp4','lb4','ซอย');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb5">&nbsp;</label>
							                    <input id="inp5" type="text" placeholder="ถนน" onfocusout="showlabel('inp5','lb5','ถนน');">
						                    </div>
					                    </div>
					                    <div data-row-span="4">
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37" >
				                                <option>ระบุจังหวัด</option>
				                                <option>จังหวัด01</option>
				                                <option>จังหวัด02</option>
				                                <option>จังหวัด03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37">
				                                <option>ระบุอำเภอ</option>
				                                <option>อำเภอ01</option>
				                                <option>อำเภอ02</option>
				                                <option>อำเภอ03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37">
				                                <option>ระบุตำบล</option>
				                                <option>ตำบล01</option>
				                                <option>ตำบล02</option>
				                                <option>ตำบล03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb6">&nbsp;</label>
							                    <input id="inp6" onfocusout="showlabel('inp6','lb6','ไปรษณีย์');" type="text" placeholder="ไปรษณีย์">
						                    </div>
					                    </div>
				                    </fieldset>
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
                                        <br><br><br>
				                    </div>
			                        </div>
                                    </form>
									</div>

								</div>
							</li>
                            <!-- 3-->
							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">3</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">
                                        <fieldset>
                                        <legend>ข้อมูลติดต่อ </legend>
                        <div class="col-md-10">
                        <div class="table-vertical">
						<table class="table table-striped">
							<thead>
								<tr>
                                    <th  class="rightnow" style="width:5%">ลำดับ</th>
                                    <th  class="cennow" style="width:25%">ประเภท</th>
									<th  class="leftnow" style="width:70%">รายละเอียด</th>
								</tr>
							</thead>
							<tbody>


						<!--		<tr>
                                    <td style="vertical-align:middle;"><i class="fa fa-phone fa-2x"></i><span class="ml7"><b>เบอร์โทรศัพท์</b></span></td>
									<td>                
                                        <div class="input-group">
				                    	<div class="tokenfield form-control"><input type="text" class="form-control"  value="" /></div>
					                    <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
			                    	</div>
									</td>
								</tr> -->
								<tr >
                                    <td class="rightnow" data-title="ลำดับ"><span>1</span></td>
                                    <td  data-title="ประเภท"><i class="fa fa-phone fa-2x"></i><span class="ml7"><b>เบอร์โทรศัพท์</b></span></td>
									<td  data-title="รายละเอียด" ><input type="text" class="form-control" value="085-0979309" /></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>2</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-tty fa-2x"></i><span class="ml7"><b>เบอร์บ้าน</b></span></td>
									<td data-title="รายละเอียด"><input type="text" class="form-control" value="สามารถพิม แก้ไขได้เลย" /></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>3</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-fax fa-2x"></i><span class="ml7"><b>Fax</b></span></td>
									<td data-title="รายละเอียด"><input type="text" class="form-control" value="044-299605-0" /></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>4</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-facebook-square fa-2x"></i><span class="ml7"><b>Facebook</b></span></td>
									<td data-title="รายละเอียด"><input type="text" class="form-control" value="ณัฐกุล สุวานิช" /></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>5</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-envelope fa-2x"></i><span class="ml7"><b>อีเมล์</b></span></td>
									<td data-title="รายละเอียด"><input type="text" class="form-control" value="supershell2537@gmail.com" /></td>
								</tr>
								<tr>
                                    <td></td>
                                    <td colspan="2" data-title="ลำดับ"><span><a href="#" class="btn btn-sm btn-success btn-label pull-left">เพิ่ม</a></span></td>
								</tr>

						<!--		<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>6</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-skype fa-2x"></i><span class="ml7"><b>Skype</b></span></td>
									<td data-title="รายละเอียด"><span>NTKGO</span></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>7</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-instagram fa-2x"></i><span class="ml7"><b>Instagram</b></span></td>
									<td data-title="รายละเอียด"><span>NTK-INSTAGRAM</span></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>8</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-twitter fa-2x"></i><span class="ml7"><b>Twitter</b></span></td>
									<td data-title="รายละเอียด"><span>NTK-Twitter</span></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>9</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-youtube fa-2x"></i><span class="ml7"><b>Youtube</b></span></td>
									<td data-title="รายละเอียด"><span>nuttakul suvanich</span></td>
								</tr>
								<tr>
                                    <td class="rightnow" data-title="ลำดับ"><span>10</span></td>
                                    <td data-title="ประเภท"><i class="fa fa-wechat fa-2x"></i><span class="ml7"><b>Wechat</b></span></td>
									<td data-title="รายละเอียด"><span>NTK-wechat</span></td>
								</tr> -->
							</tbody>
						</table>                    
					</div>
                    </div>

                                   
    		                		
				                    </fieldset>
                                    </form>
									</div>

								</div>
							</li>
						</ul>
                                <div class="panel-footer">
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
					                    <button class="btn-primary btn">บันทึก</button>
					                    <button class="btn-defualt btn">&nbsp;ล้าง&nbsp;</button>
				                    </div>
			                        </div>
                                </div>
                        </div>

                        </div>

</asp:Content>

