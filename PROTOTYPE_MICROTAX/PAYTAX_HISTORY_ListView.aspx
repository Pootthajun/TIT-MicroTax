﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_PAYTAX.master" AutoEventWireup="true" CodeFile="PAYTAX_HISTORY_ListView.aspx.cs" Inherits="PAYTAX_HISTORY_ListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ประวัติการชำระภาษี</h3>
        <div class="dropdown pull-right" style="margin-top:14px;margin-right:8px;">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o" style="float:left;margin-right:10px;"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <style>
        table {
    display: table;
    border-collapse: separate;
    border-spacing: 2px;
    border-color: grey;
    
}
    .row{
    margin-left:0px;
    margin-right:0px;
    }
    hr{
       padding:0;margin:0;
    }
    </style>
<form id="form1" runat="server" class="form-horizontal row-border">
<div class="row" >				
<div class="alert alert-info" >
                <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;">ประเภทกรรมสิทธิ์</div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                    	<input type="checkbox" class="tectonic" id="option01" value="01" >
						<label for="option01"></label>
						<span>ทั้งหมด</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option02" value="02" >
						<label for="option02"></label>
						<span>ที่รกร้างว่างเปล่า</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option03" value="03" >
						<label for="option03"></label>
						<span>เกษตรกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option04" value="04" >
						<label for="option04"></label>
						<span>กำลังก่อสร้าง</span></div>
                </div>
			    </div>
            </div>
        <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;"></div>
				<div class="col-md-10"> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option05" value="02" >
						<label for="option05"></label>
						<span>บ้านพักอาศัย</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option06" value="03" >
						<label for="option06"></label>
						<span>พาณิชยกรรม</span></div>
                    <div class="col-md-3">
                    	<input type="checkbox" class="tectonic" id="option07" value="04" >
						<label for="option07"></label>
						<span>ป้าย</span></div>
                </div>
			    </div>
            </div>
    <br />
                <div class="row">
				<div class="col-md-2" style="font-size:17.5px;text-align:center;">วันที่จ่ายภาษี</div>
				<div class="col-md-2"> 
                <div class="row">
                <input type="date" class="form-control" />
                </div>
			    </div>
                <div class="col-md-1" style="font-size:17.5px;text-align:center;">ถึง</div>
				<div class="col-md-2"> 
                <div class="row">
                <input type="date" class="form-control" />
                </div>
			    </div>
            </div>
      
    </div>
       

   </div>
<asp:Panel ID="tab1" runat="server" Visible="true">
        
            <asp:Panel ID="Panel1" runat="server" Visible="true">    
<div class="panel-body">
				<div class="tab-content">
          <div id="search-classic" class="tab-pane active">
        <form action="" class="form-horizontal row-border">
	        	<div class="form-group">
                 <div class="input-group search-page">
                     <div class="col-md-4" >
			        	<select id="mark" name="mark" class="form-control input-lg">
					<option value="">--</option>        
					<option value="1">บุคคลธรรมดา</option>
					<option value="2">บริษัท</option>
                    <option value="3">ห้างหุ้นส่วน</option>
                    <option value="4">รัฐบาล</option>
                    <option value="5">รัฐวิสาหกิจ</option>
                    <option value="6">สมาคม</option>
                    <option value="7">มูลนิธิ</option>
                    <option value="8">วัด</option>
			        	</select>
			         </div>
                     <div class="col-lg-8">
                                    <input type="text" placeholder="ชื่อ . . . ." class="form-control input-lg"> 
                     </div>    
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                    </span>

                 </div>

                </div>
              </form>
            
              <h3>ผลลัพธ์ทั้งหมด 5 รายการ</h3>
              <hr>
    <div class="row">
	<div class="col-md-12">
    			<div class="panel-body p-n" style="font-size: 16.5px;text-align:center;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="background:#73ABE3;">
							<th style="width:130px;text-align:center;">หมายเลขผู้เสียภาษี</th>
                            <th style="width:100px;text-align:center;">ประเภท</th>
							<th style="width:120px;text-align:center;">ชื่อผู้ชำระภาษี</th>
							<th style="width:120px;text-align:center;">ผู้รับเงิน</th>
							<th style="width:90px;text-align:center;">วันที่ชำระ</th>
							<th style="width:90px;text-align:center;">ยอดที่ชำระ(บาท)</th>                            
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
                            <td data-title="ประเภท">ป้าย</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นาย จรัสแสง แดงดี</td>
                               <td data-title="วันที่ชำระ">15 เมษายน 2551</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
	
                            
                               </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
                                <td data-title="ประเภท">พื้นที่รกร้าง</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นาย ศิวนัทธ์ ประดิษฐ์</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2552</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
	
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
                                <td data-title="ประเภท">เกษตรกรรม</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมนึก ศึกสงคราม</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">5 เมษายน 2553</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
	
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
                                <td data-title="ประเภท">เกษตรกรรม</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย สมหมาย เพื่อนขายตรง</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">2 เมษายน 2554</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
	
						   </tr>
					    	<tr>
							<td data-title="หมายเลขผู้เสียภาษี">1309901083805</td>
                                <td data-title="ประเภท">ป้าย</td>
							   <td data-title="ชื่อผู้ชำระภาษี">นาย ศิวนัทธ์ ประดิษฐ์</td>
							   <td data-title="ผู้รับเงิน">นางสาว สายสมร นอนรุ้ง</td>
                               <td data-title="วันที่ชำระ">25 เมษายน 2555</td>
                               <td data-title="ยอดที่ชำระ(บาท)">50,000</td>
	
						   </tr>

					</tbody>
				</table>
                </div>
            </div>
		</div>	
</div>      

          
        </div>
			</div>

            </asp:Panel>





            <asp:Panel ID="Panel2" runat="server" Visible="true">         
           </asp:Panel> 
           <asp:Panel ID="Panel3" runat="server" Visible="True">                                      
               
                                    <div class="row">
                                    <div class="col-md-11">
   	           <ul class="contentSearch" style="  list-style-type: none;">
                    
                                                   
               </ul>
                                    </div>
                                    </div>
               <div class="row">
                                    <div class="col-md-11">
   	           <ul class="contentSearch" style="  list-style-type: none;">
                    
                                                   
               </ul>
                                    </div>
                                    </div>
               <div class="row">
                                    <div class="col-md-11">
   	           <ul class="contentSearch" style="  list-style-type: none;">
                    
                                                   
               </ul>
                                    </div>
                                    </div>
               <div class="row">
                                    <div class="col-md-11">
   	           <ul class="contentSearch" style="  list-style-type: none;">
                    
                                                   
               </ul>

                                    </div>
                                    </div>    
            </asp:Panel>
                           <asp:Panel ID="Panel4" runat="server" Visible="False">
                               <asp:Label ID="Label4" runat="server" Text="ทะเบียนใบอนุญาติ"></asp:Label>
                               
                           </asp:Panel>


 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-xl" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <div class="titlepay_b">
                                                        <span style="font-weight: bold;">บุคคลธรรมดา &nbsp;</span>
                                                        <span style="font-weight: bold;color:black;">1-3099-01083-8-05 &nbsp;</span>
                                                        <span style="color:Green;font-weight: bold;">นาย ณัฐกุล สุวานิช&nbsp;</span>        
                                                   </div>
      </div>
      <div class="modal-body">
          <asp:Panel ID="Md_Panel1" runat="server" Visible="true">
              <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-2" style="text-align:center">
            <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="True" Text="ทั้งหมด" CssClass="form-control" />
            </div>
            <div class="col-lg-2" style="text-align:center">
            <asp:CheckBox ID="CheckBox_land" runat="server"  Text="ทะเบียนที่ดิน" AutoPostBack="True" CssClass="form-control" />
            </div>
            <div class="col-lg-2" style="text-align:center">
            <asp:CheckBox ID="CheckBox_build" runat="server"  AutoPostBack="True" Text="ทะเบียนสิ่งก่อสร้าง" CssClass="form-control" />
            </div>
            <div class="col-lg-2" style="text-align:center">
            <asp:CheckBox ID="CheckBox_sign" runat="server"   AutoPostBack="True" Text="ทะเบียนป้าย" CssClass="form-control" />
            </div>
            <div class="col-lg-2" style="text-align:center">
            <asp:CheckBox ID="CheckBox_license" runat="server"   AutoPostBack="True" Text="ทะเบียนใบอนุญาติ" CssClass="form-control" />
            </div>             
              </div>
          </asp:Panel>
          <asp:Panel ID="Md_Panel2" runat="server" Visible="False">
          </asp:Panel>
          <asp:Panel ID="Md_Panel3" runat="server" Visible="False">
          </asp:Panel>
          <asp:Panel ID="Md_Panel4" runat="server" Visible="False">
          </asp:Panel>
          <asp:Panel ID="Md_Panel5" runat="server" Visible="False">
          </asp:Panel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</asp:Panel>
    </form>
</asp:Content>
