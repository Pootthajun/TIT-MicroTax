﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Enroll.master" AutoEventWireup="true" CodeFile="ENROLL_FARM_DetailView.aspx.cs" Inherits="new4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h3 class="title pull-left">การเกษตร</h3>
<a role="button" class="btn btn-default tr8 fr" href="ENROLL_FARM_ListView.aspx" ><i class="fa fa-arrow-left lr10"></i>ย้อนกลับ</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

  
<link type="text/css" href="assets/plugins/kartik-file-uploader/css/fileinput.min.css" rel="stylesheet">
<link type="text/css" href="assets/plugins/dropzone/css/dropzone.css" rel="stylesheet">




  				<div class="tab-content">
					<div class="tab-pane active" id="timeline">
						<ul class="timeline">
							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">1</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    
                                        <fieldset>
			            	<legend>รายละเอียดที่ตั้งการเกษตร</legend>
					                       <div class="row">
                                            <div class="col-md-6">
                                    
                                                <form class="grid-form">
                                        <div data-row-span="3">
						                    <div data-field-span="1">
							                    <label>โซนที่ดิน</label>
							                <select class="dlhide h37"  >
				                                <option>ระบุโซน</option>
				                                <option>01A</option>
				                                <option>02A</option>
				                                <option>01B</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb1">&nbsp;</label>
						                        <input id="inp1" type="text" placeholder="บล๊อค"  class="f20" onfocusout="showlabel('inp1','lb1','บล๊อค');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb2">&nbsp;</label>
						                        <input id="inp2" type="text" placeholder="ล็อต"  class="f20" onfocusout="showlabel('inp2','lb2','ล็อต');">
						                    </div>
                                            </div>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb3">&nbsp;</label>
						                        <input id="inp3" type="text" placeholder="ประเภทเอกสารสิทธิ์"  class="f20" onfocusout="showlabel('inp3','lb3','ประเภทเอกสารสิทธิ์');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb4">&nbsp;</label>
						                        <input id="inp4" type="text" placeholder="เลขที่เอกสารสิทธิ์"  class="f20" onfocusout="showlabel('inp4','lb4','เลขที่เอกสารสิทธิ์');">
						                    </div>
					                    </div> 
                                             <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb5">&nbsp;</label>
						                        <input id="inp5" type="text" placeholder="ตำบล"  class="f20" onfocusout="showlabel('inp5','lb5','ตำบล');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb50">&nbsp;</label>
						                        <input id="inp50" type="text" placeholder="หมู่บ้าน"  class="f20" onfocusout="showlabel('inp50','lb50','หมู่บ้าน');">
						                    </div>
					                    </div>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb6">&nbsp;</label>
						                        <input id="inp6" type="text" placeholder="ถนน"  class="f20" onfocusout="showlabel('inp6','lb6','ถนน');">
						                    </div>
						                    <div data-field-span="1">
							                   <label id="lb7">&nbsp;</label>
						                        <input id="inp7" type="text" placeholder="ซอย"  class="f20" onfocusout="showlabel('inp7','lb7','ซอย');">
						                    </div>
					                    </div>

                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                            <div id="Div3" style="width: 100%; height: 300px; overflow: hidden; position: relative; background-color: rgb(229, 227, 223);"><div class="gm-style" style="left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: absolute; z-index: 0;"><div style="left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: absolute; z-index: 0; cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur), default;"><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, 15, -45); transform-origin: 0px 0px 0px;"><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 100;"><div style="left: 0px; top: 0px; position: absolute; z-index: 0;"><div aria-hidden="true" style="left: 0px; top: 0px; visibility: inherit; position: absolute; z-index: 1;"><div style="left: 239px; top: 46px; width: 256px; height: 256px; position: absolute;"></div><div style="left: -17px; top: 46px; width: 256px; height: 256px; position: absolute;"></div><div style="left: -17px; top: -210px; width: 256px; height: 256px; position: absolute;"></div><div style="left: 239px; top: -210px; width: 256px; height: 256px; position: absolute;"></div><div style="left: -17px; top: 302px; width: 256px; height: 256px; position: absolute;"></div><div style="left: 239px; top: 302px; width: 256px; height: 256px; position: absolute;"></div></div></div></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 101;"><div style="left: 0px; top: 0px; position: absolute; z-index: 30;"><div aria-hidden="true" style="left: 0px; top: 0px; visibility: inherit; position: absolute; z-index: 1;"><div style="left: 239px; top: 46px; width: 256px; height: 256px; overflow: hidden; position: absolute;"><canvas width="256" height="256" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;" draggable="false"></canvas></div><div style="left: -17px; top: 46px; width: 256px; height: 256px; overflow: hidden; position: absolute;"><canvas width="256" height="256" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;" draggable="false"></canvas></div><div style="left: -17px; top: -210px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: 239px; top: -210px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: -17px; top: 302px; width: 256px; height: 256px; overflow: hidden; position: absolute;"><canvas width="256" height="256" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;" draggable="false"></canvas></div><div style="left: 239px; top: 302px; width: 256px; height: 256px; overflow: hidden; position: absolute;"><canvas width="256" height="256" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;" draggable="false"></canvas></div></div></div></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 102;"></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 103;"><div style="left: 0px; top: 0px; position: absolute; z-index: -1;"><div aria-hidden="true" style="left: 0px; top: 0px; visibility: inherit; position: absolute; z-index: 1;"><div style="left: 239px; top: 46px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: -17px; top: 46px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: -17px; top: -210px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: 239px; top: -210px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: -17px; top: 302px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div><div style="left: 239px; top: 302px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div></div></div><div style="left: 239px; top: 160px; width: 22px; height: 40px; overflow: hidden; position: absolute; z-index: 200;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 22px; height: 40px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png"></div></div><div style="left: 0px; top: 0px; position: absolute; z-index: 0;"><div style="width: 501px; height: 400px; overflow: hidden;"><img style="width: 501px; height: 400px;" src="http://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&amp;1i4258065&amp;2i2979026&amp;2e1&amp;3u15&amp;4m2&amp;1u501&amp;2u400&amp;5m5&amp;1e0&amp;5sen-US&amp;6sus&amp;10b1&amp;12b1&amp;token=46722"></div></div><div style="left: 0px; top: 0px; position: absolute; z-index: 0;"><div aria-hidden="true" style="left: 0px; top: 0px; visibility: inherit; position: absolute; z-index: 1;"><div style="transition:opacity 200ms ease-out; left: 239px; top: 46px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11637!4i256!2m3!1e0!2sm!3i362031225!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=35673"></div><div style="transition:opacity 200ms ease-out; left: -17px; top: 46px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11637!4i256!2m3!1e0!2sm!3i362031225!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=75394"></div><div style="left: -17px; top: -210px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11636!4i256!2m3!1e0!2sm!3i362031333!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=128642"></div><div style="left: 239px; top: -210px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11636!4i256!2m3!1e0!2sm!3i362031333!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=88921"></div><div style="left: -17px; top: 302px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11638!4i256!2m3!1e0!2sm!3i362031225!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=111415"></div><div style="left: 239px; top: 302px; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11638!4i256!2m3!1e0!2sm!3i362031225!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=71694"></div></div></div></div><div class="gm-style-pbc" style="left: 0px; top: 0px; width: 100%; height: 100%; display: none; position: absolute; z-index: 2; opacity: 0; transition-duration: 0.3s;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="left: 0px; top: 0px; width: 100%; height: 100%; position: absolute; z-index: 3;"></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 4; transform: matrix(1, 0, 0, 1, 15, -45); transform-origin: 0px 0px 0px;"><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 104;"></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 105;"></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 106;"><div title="Drag Me" class="gmnoprint" style="left: 239px; top: 160px; width: 22px; height: 40px; overflow: hidden; position: absolute; z-index: 200; cursor: pointer; opacity: 0.01;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 22px; height: 40px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png"></div></div><div style="left: 0px; top: 0px; width: 100%; position: absolute; z-index: 107;"><div style="display: none; z-index: -202; cursor: pointer;"><div style="width: 30px; height: 27px; overflow: hidden; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 90px; height: 27px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/undo_poly.png"></div></div></div></div></div><div style="left: 0px; bottom: 0px; margin-right: 5px; margin-left: 5px; position: absolute; z-index: 1000000;"><a title="Click to see this area on Google Maps" style="overflow: visible; display: inline; position: static; float: none;" href="https://maps.google.com/maps?ll=46.151087,2.746427&amp;z=15&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank"><div style="width: 66px; height: 26px; cursor: pointer;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 66px; height: 26px; position: absolute; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png"></div></a></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); border-image: none; left: 101px; top: 110px; width: 256px; height: 148px; color: rgb(34, 34, 34); font-family: Roboto,Arial,sans-serif; display: none; position: absolute; z-index: 10000002; box-shadow: 0px 4px 16px rgba(0,0,0,0.2); background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="top: 12px; width: 13px; height: 13px; right: 12px; overflow: hidden; position: absolute; z-index: 10000; cursor: pointer; opacity: 0.7;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: -2px; top: -336px; width: 59px; height: 492px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png"></div></div><div class="gmnoprint" style="width: 121px; right: 167px; bottom: 0px; position: absolute; z-index: 1000001;"><div class="gm-style-cc" style="height: 14px; line-height: 14px; -ms-user-select: none;" draggable="false"><div style="width: 100%; height: 100%; position: absolute; opacity: 0.7;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="text-align: right; color: rgb(68, 68, 68); padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; vertical-align: middle; display: inline-block; white-space: nowrap; position: relative; direction: ltr;"><a style="color: rgb(68, 68, 68); text-decoration: none; display: none; cursor: pointer;">Map Data</a><span>Map data ©2016 Google</span></div></div></div><div class="gmnoscreen" style="right: 0px; bottom: 0px; position: absolute;"><div style="text-align: right; color: rgb(68, 68, 68); font-family: Roboto,Arial,sans-serif; font-size: 11px; direction: ltr; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div class="gmnoprint gm-style-cc" style="height: 14px; right: 95px; bottom: 0px; line-height: 14px; position: absolute; z-index: 1000001; -ms-user-select: none;" draggable="false"><div style="width: 100%; height: 100%; position: absolute; opacity: 0.7;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="text-align: right; color: rgb(68, 68, 68); padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; vertical-align: middle; display: inline-block; white-space: nowrap; position: relative; direction: ltr;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer;" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank">Terms of Use</a></div></div><div style="margin: 10px 14px; top: 0px; width: 25px; height: 25px; right: 0px; overflow: hidden; display: none; position: absolute;"><img class="gm-fullscreen-control" style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: -52px; top: -86px; width: 164px; height: 112px; position: absolute; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/sv5.png"></div><div class="gm-style-cc" style="height: 14px; right: 0px; bottom: 0px; line-height: 14px; position: absolute; -ms-user-select: none;" draggable="false"><div style="width: 100%; height: 100%; position: absolute; opacity: 0.7;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="text-align: right; color: rgb(68, 68, 68); padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; vertical-align: middle; display: inline-block; white-space: nowrap; position: relative; direction: ltr;"><a title="Report errors in the road map or imagery to Google" style="color: rgb(68, 68, 68); font-family: Roboto,Arial,sans-serif; font-size: 10px; text-decoration: none; position: relative;" href="https://www.google.com/maps/@46.1510865,2.7464266,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" target="_new">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" style="margin: 10px; right: 28px; bottom: 69px; position: absolute; -ms-user-select: none;" draggable="false" controlWidth="28" controlHeight="55"><div class="gmnoprint" style="left: 0px; top: 0px; position: absolute;" controlWidth="28" controlHeight="55"><div style="border-radius: 2px; width: 28px; height: 55px; cursor: pointer; box-shadow: 0px 1px 4px -1px rgba(0,0,0,0.3); -ms-user-select: none; background-color: rgb(255, 255, 255);" draggable="false"><div title="Zoom in" style="left: 0px; top: 0px; width: 28px; height: 27px; position: relative;"><div style="left: 7px; top: 6px; width: 15px; height: 15px; overflow: hidden; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: 0px; width: 120px; height: 54px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png"></div></div><div style="left: 16%; top: 0px; width: 67%; height: 1px; overflow: hidden; position: relative; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="left: 0px; top: 0px; width: 28px; height: 27px; position: relative;"><div style="left: 7px; top: 6px; width: 15px; height: 15px; overflow: hidden; position: absolute;"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0px; top: -15px; width: 120px; height: 54px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png"></div></div></div></div><div class="gmnoprint" style="display: none; position: absolute;" controlWidth="28" controlHeight="0"><div title="Rotate map 90 degrees" style="border-radius: 2px; width: 28px; height: 28px; overflow: hidden; display: none; position: absolute; cursor: pointer; box-shadow: 0px 1px 4px -1px rgba(0,0,0,0.3); background-color: rgb(255, 255, 255);"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: -141px; top: 6px; width: 170px; height: 54px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png"></div><div class="gm-tilt" style="border-radius: 2px; top: 0px; width: 28px; height: 28px; overflow: hidden; position: absolute; cursor: pointer; box-shadow: 0px 1px 4px -1px rgba(0,0,0,0.3); background-color: rgb(255, 255, 255);"><img style="margin: 0px; padding: 0px; border: 0px; border-image: none; left: -141px; top: -13px; width: 170px; height: 54px; position: absolute; max-width: none; -ms-user-select: none;" draggable="false" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png"></div></div></div></div></div>
                                             
                                            </div>
                                        </div>        
				                    </fieldset>
                                     <div class="clearfix pt20">
				                        <div class="pull-right">
                                        <br>
				                        </div>
			                        </div>
									</div>

								</div>
							</li>

                            <!-- 2-->

							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">2</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    
                                        <fieldset>
			            	<legend>รายละเอียดการทำการเกษตร</legend>
					                       <div class="row">
                                               
                                            <div class="col-md-6">
                                    
                                  <form class="grid-form">
                                        <div data-row-span="3">
						                    <div data-field-span="1">
							                    <label>รหัสพื้นที่การเกษตร</label>
							                <select class="dlhide h37"  >
				                                <option>ระบุโซน</option>
				                                <option>01A</option>
				                                <option>02A</option>
				                                <option>01B</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb8">&nbsp;</label>
						                        <input id="inp8" type="text" placeholder="บล๊อค"  class="f20" onfocusout="showlabel('inp8','lb8','บล๊อค');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb9">&nbsp;</label>
						                        <input id="inp9" type="text" placeholder="ล็อต"  class="f20" onfocusout="showlabel('inp9','lb9','ล็อต');">
						                    </div>
                                            </div>
					                    <div data-row-span="1">
						                    <div data-field-span="1">
							                    <label id="lb10">&nbsp;</label>
						                        <input id="inp10" type="text" placeholder="ประเภทการทำเกษตรกรรม"  class="f20" onfocusout="showlabel('inp10','lb10','ประเภทการทำเกษตรกรรม');">
						                    </div>
                                        </div>
                                        <div data-row-span="1">
						                    <div data-field-span="1">
							                    <label id="lb11">&nbsp;</label>
						                        <input id="inp11" type="text" placeholder="ลักษณะการทำเกษตรกรรม"  class="f20" onfocusout="showlabel('inp11','lb11','ลักษณะการทำเกษตรกรรม');">
						                    </div>
					                    </div> 
                                             <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb12">&nbsp;</label>
						                        <input id="inp12" type="text" placeholder="พื้นที่ทั้งหมด"  class="f20" onfocusout="showlabel('inp12','lb12','พื้นที่ทั้งหมด');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb13">&nbsp;</label>
						                        <input id="inp13" type="text" placeholder="ราคาประเมิน"  class="f20" onfocusout="showlabel('inp13','lb13','ราคาประเมิน');">
						                    </div>
					                    </div>
					                    <div data-row-span="1">
						                    <div data-field-span="1">
							                    <label id="lb14">&nbsp;</label>
						                        <input id="inp14" type="text" placeholder="หมายเหตุ"  class="f20" onfocusout="showlabel('inp14','lb14','หมายเหตุ');">
						                    </div>
					                    </div>

                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                <form action="upload.php" class="dropzone dz-clickable">
                                                    <div class="dz-default dz-message">
                                                        <span class="hidden-xs">กดเพื่อเพิ่มรูปภาพ</span>
                                                        <span class="visible-xs">กดเพื่อเพิ่มรูปภาพ</span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>        
				                    </fieldset>
                                     <div class="clearfix pt20">
				                        <div class="pull-right">
				                        </div>
			                        </div>
									</div>

								</div>
							</li>

                            <!-- 3-->

							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">3</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">
                                        <fieldset>
			            	<legend>เจ้าของกรรมสิทธิ์</legend>
                            <div class="panel-body p-n">
                            <div class="table-vertical">
                            <table class="table table-striped">
                            <thead>
                                <tr style="background-color:#bbb39c;" >
                                    <th style="width:50px;" class="cennow">ลำดับ</th>
                                    <th class="cennow">ประเภท</th>
                                    <th class="cennow">ชื่อ</th>
                                    <th class="cennow">นามสกุล</th>
                                    <th class="cennow">ที่อยู่</th>
                                    <th class="cennow" style="width:5%;display:none ">เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td data-title="ลำดับ">1</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">สุขุม</td>
                                    <td data-title="นามสกุล">เยือกเย็น</td>
                                    <td data-title="ที่อยู่">33/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">2</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">ยิ่งลักษณ์</td>
                                    <td data-title="นามสกุล">บุญฤทธิ์</td>
                                    <td data-title="ที่อยู่">90/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>
                                <tr>
                                    <td data-title="ลำดับ"></td>
                                    <td data-title="คำนำหน้า"></td>
                                    <td data-title="ชื่อ"></td>
                                    <td data-title="นามสกุล"></td>
                                    <td data-title="ที่อยู่"></td>
                                    <td data-title="เครื่องมือ"><button type="button" class="btn-success btn" data-toggle="modal" data-target="#myModal" >เพิ่ม</button></td>
                                </tr>

                            </tbody>
                        </table>
                    </div> 
                  </div>
				                    </fieldset>
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
                                    <br>
				                    </div>
			                        </div>
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">

                                                </div>                                            
                                                <div class="col-md-3"></div>    
                                            </div>  
                                    </form>
                          
                            <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">เพิ่มข้อมูลติดต่อ</h4>
                            </div>
                            <div class="modal-body p-n">
                            <div class="panel-body p-n">                             
                            <div class="table-vertical">
                            <table class="table table-striped">
                            <thead>
                                <tr style="background-color:#bbb39c;" >
                                    <th  class="cennow w50">ลำดับ</th>
                                    <th class="cennow">ประเภท</th>
                                    <th class="cennow">ชื่อ</th>
                                    <th class="cennow">นามสกุล</th>
                                    <th class="cennow">ที่อยู่</th>
                                    <th  class="cennow w100">เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td data-title="ลำดับ">1</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">สุขุม</td>
                                    <td data-title="นามสกุล">เยือกเย็น</td>
                                    <td data-title="ที่อยู่">33/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	

                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">2</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">ยิ่งลักษณ์</td>
                                    <td data-title="นามสกุล">บุญฤทธิ์</td>
                                    <td data-title="ที่อยู่">90/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	

                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">3</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">พิพัฒน์พงศ์</td>
                                    <td data-title="นามสกุล">สงนาค</td>
                                    <td data-title="ที่อยู่">90/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	

                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">4</td>
                                    <td data-title="คำนำหน้า">บุคคลธรรมดา</td>
                                    <td data-title="ชื่อ">ศุภพงศ์</td>
                                    <td data-title="นามสกุล">เวียงตาลจันทรา</td>
                                    <td data-title="ที่อยู่">90/2 อำเภอบางใหญ่ ตำบลใจเล็ก ถนนคนกันเอง ซอยบุญรักษา</td>
                                    							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                                  <div class="modal-footer">
                                  </div>
					              
        </div>
      </div>
      
    </div>
  </div>
                                        
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
					                    <button class="btn-primary btn">บันทึก</button>
					                    <button class="btn-defualt btn">&nbsp;ล้าง&nbsp;</button>
				                    </div>

									</div>
									</div>

								</div>
							</li>					
  
						</ul>
                        </div>
                        </div>

<script type="text/javascript" src="assets/plugins/kartik-file-uploader/js/fileinput.min.js"></script>
<script type="text/javascript" src="assets/demo/kartik-file-uploader/data.js"></script>
<script type="text/javascript" src="assets/plugins/dropzone/dropzone.js"></script>

</asp:Content>

