﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="frmMenu.ascx.cs" Inherits="frmMenu" %>

      <ul class="acc-menu">
		    <li class="nav-separator" data-localize="nav_overview">ข้อมูลพื้นฐาน</li>
		  
		    <li><a href="INFO_DISTRICT_ListView.aspx"><span class="icon"><i class="ion-android-done-all"></i></span><span class="text" data-localize="nav_projects">ตำบล</span></a></li>
		    <li><a href="INFO_VILLAGE_ListView.aspx"><span class="icon"><i class="fa fa-home"></i></span><span class="text"  data-localize="nav_to_do_list">หมู่บ้าน (ชุมชน)  </span></a></li>
		    <li><a href="INFO_ROAD_ListView.aspx"><span class="icon"><i class="fa fa-road"></i></span><span class="text" data-localize="nav_notes">ถนน</span></a></li>
		    <li><a href="INFO_Alley_ListView.aspx"><span class="icon"><i class="fa fa-road"></i></span><span class="text" data-localize="nav_customizer">ซอย (ตรอก)</span></a></li>
	
            <li><a href="INFO_TypeOfBusiness_ListView.aspx"><span class="icon"><i class="ion-ios-paper"></i></span><span class="text" data-localize="nav_customizer">ประเภทกิจการค้า</span></a></li>
            <li><a href="INFO_StyleOfBusiness_ListView.aspx"><span class="icon"><i class="ion-briefcase"></i></span><span class="text" data-localize="nav_customizer">ลักษณะกิจการค้า</span></a></li>

		    <!--<li><a href="INFO_PROPERTY_ListView.aspx"><span class="icon"><i class="ion-ios-shuffle-strong" style="color:#03a9f4;"></i></span><span class="text" data-localize="nav_customizer">การเปลี่ยนแปลงทรัพย์สิน</span></a></li>!-->
		    <li><a href="INFO_OfficialList_ListView.aspx"><span class="icon"><i class="ion-person-stalker"></i></span><span class="text" data-localize="nav_customizer">รายการเจ้าหน้าที่</span></a></li>
		    <!--<li><a href="INFO_BuildingType_ListView.aspx"><span class="icon"><i class="ion-ios-grid-view" style="color:#03a9f4;"></i></span><span class="text" data-localize="nav_customizer">ประเภทอาคาร</span></a></li> !-->

	    </ul>





