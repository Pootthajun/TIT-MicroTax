﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_MAP_Main.master" AutoEventWireup="true" CodeFile="MAP.aspx.cs" Inherits="Default3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div class="row">
        <div class="col-md-3">
        <div class="panel panel-default alt with-footer">
      <div class="panel-body" style="padding-top:0px;font-size: 17.5px;">
          <br/>
    <div class ="row    ">         
           <div class="col-md-3">
                 <p style="font-size:16px;">จังหวัด</p>
           </div>
           <div class="col-md-9">
                        <select name="selector1" id="select1" class="form-control" >
									<option>โปรดระบุถนน</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
                                    <option>หนองย่างาม</option>

								</select>
           </div>
    </div>
    <div class ="row">         
           <div class="col-md-3">
                 <p style="font-size:16px;">ตำบล</p>
           </div>
           <div class="col-md-9">
                        <select name="selector1" id="select2" class="form-control" >
									<option>โปรดระบุถนน</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
                                    <option>หนองย่างาม</option>

								</select>
           </div>
    </div>
    <div class ="row">         
           <div class="col-md-3">
                 <p style="font-size:16px;">หมู่บ้าน</p>
           </div>
           <div class="col-md-9">
                        <select name="selector1" id="select3" class="form-control" >
									<option>โปรดระบุถนน</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
									<option>หนองย่างาม</option>
                                    <option>หนองย่างาม</option>

								</select>
           </div>
    </div>

    <div class ="row">         
           <div class="col-md-12">
<div class="alert alert-info" style="font-size:16.5px;" >
                <div class="row">
				<div class="col-md-12"> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option01" value="01" >
						<label for="option02"></label>
						<span>ที่รกร้าง</span></div> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option02" value="02" >
						<label for="option02"></label>
						<span>บ้านพักอาศัย</span></div> 
			    </div>
            </div>
                <div class="row">
				<div class="col-md-12"> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option03" value="03" >
						<label for="option02"></label>
						<span>พาณิชยกรรม</span></div> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option04" value="04" >
						<label for="option02"></label>
						<span>เกษตรกรรม</span></div>
			    </div>
            </div>
                <div class="row">
				<div class="col-md-12"> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option05" value="05" >
						<label for="option02"></label>
						<span>พื้นที่พัฒนา</span></div> 
                    <div class="col-md-6">
                    	<input type="checkbox" class="tectonic" id="option06" value="06" >
						<label for="option02"></label>
						<span>ป้าย</span></div>
			    </div>
            </div>
      
    </div>

            </div>
    </div>
    <div class ="row    ">         
           <div class="col-md-3">
                 <p style="font-size:16px;">คำค้นหา</p>
           </div>
           <div class="col-md-9">
                        <input type="text" class="form-control" />
               <br>
           </div>
    </div>

                            <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title "><b>พื้นที่พัฒนา</b></h4>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                            <div class="modal-body" style="padding:0px;">
                                  	
                            <div class="panel-body p-n" style="font-size: 14px;">    
                                <div class="row">

    <div class="col-md-5">

        <img src="assets/demo/avatar/60.jpg" class="img-responsive" style="height:250px;margin-right:auto;">
    </div>
    <div class="col-md-7">
						<table class="table table-bordered" style="margin-bottom:0px;">
								<tbody><tr>
									<td class="Text_Column_Header" width="15%">
                                        <span>รหัสที่ดิน</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span><b>A001 002/003</b></span> 
									</td>
								</tr>
                                <tr>
									<td class="Text_Column_Header" width="15%">
                                        <span>เอกสารสิทธิ์</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span><b>โฉนด เลขที่ 13</b></span> 
									</td>
								</tr>                                   
                                <tr>
									<td class="Text_Column_Header">
                                        <span>เนื้อที่ทั้งหมด</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span><b>10 ไร่ 5 งาน 2 ตารางวา</b></span> 
									</td>
                                </tr>                                   
                                <tr>
									<td class="Text_Column_Header">
                                        <span>ผู้ครอบครอง</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span><b>นาย ศิวนัทธ์ ประดิษฐ์</b></span> 
									</td>
                                </tr>
                                <tr>
									<td class="Text_Column_Header">
                                        <span>ราคาประเมิน</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span>4,450,000 บาท</span> 
									</td>
								</tr>
                                <tr>
									<td class="Text_Column_Header">
                                        <span>ภาษีจากประเมิน</span>
									</td>
									<td class="Text_Column_Value" colspan="3">
                                        <span>50,000 บาท</span> 
									</td>
								</tr>
						</tbody></table>                    
					    
             </div>

</div>       						<table class="table table-bordered" style="margin-bottom:0px;">
								<tbody><tr>
									<td class="Text_Column_Header" style="width:10px;">
                                         <span>ที่ตั้ง</span>
									</td>
									<td class="Text_Column_Value" style="width:250px;">
                                        <span">5/8 ถนนประดิษฐ์มนูธรรม, แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร, 10310 แขวง สะพานสอง เขต วังทองหลาง กรุงเทพมหานคร 10310</span> 
		
									</td>
								</tr>  </tbody> 
                                    </table>            
       
        						                    
                            
                            
                            
                            
                            
                            
                            </div></div></div>
                            <div class="col-md-1"></div>
                            </div>
                                  <div class="modal-footer">
                                  </div>
					              
                            
        
      </div>
      
    </div>
  </div>


<div class="row">
<div class="col-md-12">
        <div class="panel panel-default alt with-footer">
            <div class="panel-heading">
                <h2 class="title">พบข้อมูลทั้งหมด</h2>
                <div class="panel-ctrls">
                    <div class="badge badge-info">6 รายการ</div>
                </div>
            </div>
            <div class="panel-body p-n" style="max-height: 250px;overflow-x: auto;">
                <ul class="media-list scroll-content m-n">
                    <li class="media b-bl">
                        <a href="#">
                            <div class="media-left">
                                <span class="icon success"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">ที่ดินรกร้าง</span> รหัส A01 111/222
                                <span class="time">นาย จรัสแสง แดงดี</span>
                            </div>
                        </a>
                    </li>
                    <li class="media b-bl">
                        <a href="#">
                            <div class="media-left">
                                <span class="icon purple"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">บ้านพักอาศํย</span> รหัส A01 111/002
                                <span class="time">นาย สมชาย หมายจันทร์</span>
                            </div>
                        </a>
                    </li>
                    <li class="media b-bl">
                        <a href="#">
                            <div class="media-left">
                                <span class="icon blue"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">พาณิชยกรรม</span> รหัส A01 002/003
                                <span class="time">นาย อสุรา หมาเมามาย</span>
                            </div>
                        </a>
                    </li>
                    <li class="media b-bl">
                        <a href="#">
                            <div class="media-left">
                                <span class="icon lime"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">เกษตรกรรม</span> รหัส A01 002/003
                                <span class="time">นาย สงบ สุข</span>
                            </div>
                        </a>
                    </li>
                    <li class="media b-bl">
                        <a data-toggle="modal" data-target="#myModal">
                            <div class="media-left">
                                <span class="icon orange"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">พื้นที่พัฒนา</span> รหัส A01 002/003
                                <span class="time">นาย สงบ สุข</span>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="#">
                            <div class="media-left">
                                <span class="icon teal"></span>
                            </div>
                            <div class="media-body">
                                <span class="name">ป้าย</span> รหัส A01 002/003
                                <span class="time">นาย สงบ สุข</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


    </div>
</div>
       
        </div>
       </div> 
        </div>


        <div class="col-md-9">
        <div class="panel panel-default alt with-footer">
        
      <div class="panel-body" style="padding-top:0px;font-size: 17.5px;">

            
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="assets/plugins/gmaps/gmaps.js"></script>
<script type="text/javascript" src="assets/demo/demo-gmaps.js"></script>


<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script type="text/javascript" src="assets/plugins/location-picker/dist/locationpicker.jquery.min.js"></script>
<script type="text/javascript" src="assets/demo/demo-location-picker.js"></script>
    
        <div class="panel-body">
            <img src="assets/demo/avatar/61.png" class="img-responsive" style="height:576px;width:100%;">    
        </div>
        </div>
       </div> 
        </div>
    </div>
</asp:Content>

