﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_PAYTAX_OWNERSHIP.master" AutoEventWireup="true" CodeFile="PAYTAX_INVOICE.aspx.cs" Inherits="PAYTAX_INVOICE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
            <div class="panel-body p-xxl">
                <div class="clearfix">
                    <div class="row">
                        <p class="text-center">
                            <img class="text-left" src="assets/img/m5.png" class="mt-md mb-md" alt="tectonic" width="176" height="48">
                        </p>
                        
                        <address class="mt-md mb-md text-center">
                            <strong>ใบชำระภาษี สำนักงานเทศบาลนครนนทบุรี</strong><br>
                            ถนนรัตนาธิเบศร์ ต. บางกระสอ<br>
                            อ. เมือง จ. นนทบุรี, 11000<br>
                        </address>
                    </div>
                </div>
                <div class="row mb-xl mr ml">
                    <div class="col-md-12">
                        
                        <div class="pull-left">                            
                            <h3 class="text-muted">.</h3>                         
                            <address>
                                <strong>ผู้ชำระเงิน: </strong> นายศิวนัทธ์  ประดิษฐ์ (1230003322111)<br>
                               บ้านเลขที่ 12/11 ตำบลคนจริง อำเภอคนแท้<br>จังหวัด นนทบุรี 30000<br>
                            </address>
                        </div>
                        
                        <div class="pull-right">
                            <h3 class="text-muted text-right">..</h3>    
                            <ul class="text-right list-unstyled">
                                <li><strong>หมายเลขใบชำระภาษี</strong> #10007819 </li>
                                <li><strong>ยอดวันที่:</strong> 20/04/2016</li>
                                <li><strong>วันที่จ่าย:</strong> 29/05/2016</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row mb-xl">
                    <div class="col-md-12">
                        <div class="panel-transparent mb-sm">
                            <div class="panel-body">
                                <table class="table table-hover mb0">
                                    <thead>
                                        <tr>
                                            <th style="width:5%">รหัสที่ดิน</th>
                                            <th style="width:57%">รายการ</th>
                                            <th class="text-right" style="width:13%">รายการคำนวน</th>
                                            <th class="text-right" style="width:5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>01A121/001</td>
                                            <td>ที่รกร้างว่างเปล่า</td>
                                            <td class="text-right"></td>
                                            <td class="text-right"></td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td></td>
                                            <td>ราคาประเมิน</td>
                                            <td class="text-right">6,000,000.00</td>
                                            <td class="text-right">บาท</td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td></td>
                                            <td>อัตราภาษี ที่รกร้างว่างเปล่า 1-3 ปี</td>
                                            <td class="text-right">1.00</td>
                                            <td class="text-right">%</td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td></td>
                                            <td>รวมยอดภาษี</td>
                                            <td class="text-right">60,000.00</td>
                                            <td class="text-right">บาท</td>
                                        </tr>                                                                                                                                                                                                                                          
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row mr ml mt-xxl" style="border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;">
                            <div class="col-md-9 pull-left">
                                <p class="text-left mb-sm"><strong class="signature">ผู้รับเงิน</strong></p>
                                <p class="text-left pt pb">
                                    <img class="img-responsive" src="https://haciendaelrefugio.files.wordpress.com/2013/04/jim-signature.png" alt="signature" width= "130" height= "130">
                                </p>
                                <p class="text-left mb-sm"><strong class="signature">ศิวนัทธ์  ประดิษฐ์</strong></p>
                                <p class="text-left designation">ผู้ว่าการอำเภอ</p>
                                <p class="text-left authorization">อำเภอหนองขาม จังหวัดนนทบุรี</p>
                            </div>
                            <div class="col-md-3 pull-ight">
                                <p class="text-right"><strong>ยอดภาษี: 60,000 บาท</strong></p>
                                <p class="text-right discount">ค้างจ่าย 2 เดือน: 6,000 บาท</p>
                                <p class="text-right vat">ค่าบริการ : 5 บาท</p>
                                <hr>
                                <h4 class="text-right" style="font-weight: bold;">รวมทั้งสิ้น 66,005 บาท</h4>
                                <div class="aftersign" style="border-top: 2px solid rgb(238, 238, 238); width: 150px;float: right;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row mr ml">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <a href="javascript:window.print()" class="btn btn-success"><i class="ion-printer"></i></a>
                            <a href="#" class="btn btn-primary btn-label"><i class="ion-ios-paperplane"></i>Send</a>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>

