﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition.master" AutoEventWireup="true" CodeFile="IMPOSITION_LAND_Listview.aspx.cs" Inherits="IMPOSITION_LAND_Detailview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ที่ดินรกร้าง</h3>
      <div class="pull-right" >
                 <a role="button" class="btn btn-default tr8 fr" href="#"><i class="fa fa-file-text-o lr10"></i>ออกรายงาน</a>
                 <a role="button" class="btn btn-default tr8 fr" href="IMPOSITION_Detailview.aspx"><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>  
     </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
<style>
    td,th{
        text-align:center;
    }
</style>



    <div class="row">
    <h4 style="text-align:center"><b>ที่ดินรกร้าง</b> อัตราเพดาน 5.0 %</h4> 
    <div class="col-md-1"></div>
	<div class="col-md-10">
            
    			<div class="panel-body p-n" style="font-size: 16.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead style="background-color:skyblue">
						<tr>
							<th style="width:50px;">ลำดับ</th>
							<th style="width:300px;">จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)</th>
							<th style="width:40px;">คิดภาษี(%)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
                               <td data-title="ลำดับ">1</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">1 ถึง 3 ปี</td>
                               <td data-title="คิดภาษี(%)">1%</td>
							   
                               </tr>
					    	<tr>
                               <td data-title="ลำดับ">2</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">4 ถึง 6</td>
                               <td data-title="คิดภาษี(%)">2%</td>
							   
						   </tr>
					    	<tr>
                               <td data-title="ลำดับ">3</td>
							   <td data-title="จำนวนปีที่ไม่ได้ใช้ประโยชน์(ปี)">มากกว่า 7</td>
                               <td data-title="คิดภาษี(%)">3%</td>
							   
						   </tr>

					</tbody>
				</table>
				<div class="panel-footer"></div>     
                </div>
            </div>

		</div>
	    <div class="col-md-1"></div>
</div>



</asp:Content>
