﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition.master" AutoEventWireup="true" CodeFile="IMPOSITION_Listview.aspx.cs" Inherits="IMPOSITION_BUILD_Detailview_aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">อัตราภาษี</h3>
   <div class="pull-right" >
             <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="IMPOSITION_Detailview.aspx" ><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>  
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
<style>
</style>
    <div class="row">
        	<div class="col-md-3">
                <div style="background-color:#c0f7e0;padding:10px">
                   <h3 style="text-align:center;margin-top:0px;"><b>เกษตรกรรม</b></h3> 
                   <h4 style="text-align:center;"><span>อัตราเพดาน</span><input type="text" value="0.02 %" /></h4>
                 </div>
    			<div class="panel-body p-n" style="font-size: 14.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" >
				
					<tbody><tr>
							   <td class ="cennow" colspan="3" data-title="มูลค่าของทรัพย์สิน(บาท)">มูลค่าทรัพย์สิน</td>
                               </tr>
						      <tr class="cennow">
                               <td data-title="ตั้งแต่" >ตั้งแต่   (ลบ.)</td>
                               <td data-title="ถึง">ถึง   (ลบ.)</td>
                               <td data-title="คิดภาษี(%)">อัตราภาษี (%)</td>
							   
                              </tr>
						      <tr>

							   <td data-title="ตั้งแต่"><input type="text" value="0" /></td>
                               <td data-title="ถึง"><input type="text" value="50" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="ยกเว้น" /></td>        
							   
                              </tr>
					    	<tr>

							   <td data-title="ตั้งแต่"><input type="text" value="50" /></td>
                               <td data-title="ถึง"><input type="text" value="100" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.05" /></td>    
							   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="100" /></td>
                               <td data-title="ถึง"><input type="text" value="" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.10" /></td>    
							   
						   </tr>
					    	
					</tbody>
				</table>
                </div>
            </div>

		</div><div class="col-md-3">
                <div style="background-color:#a4d7f6;padding:10px">
                   <h3 style="text-align:center;margin-top:0px"><b>บ้านพักอาศัย</b></h3> 
                   <h4 style="text-align:center;"><span>อัตราเพดาน</span><input type="text" value="0.05 %" /></h4>
                 </div>
    			<div class="panel-body p-n" style="font-size: 14.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="1" width="100%">
				
					<tbody><tr>
							   <td class ="cennow" colspan="3" data-title="มูลค่าของทรัพย์สิน(บาท)">มูลค่าทรัพย์สิน(บ้านหลังหลัก)</td>
                               </tr>

						      <tr class="cennow">

                               <td data-title="ตั้งแต่" >ตั้งแต่   (ลบ.)</td>
                               <td data-title="ถึง">ถึง   (ลบ.)</td>
                               <td data-title="คิดภาษี(%)">อัตราภาษี (%)</td>
							   
                              </tr>		
                        				   
                             <tr>
							   <td data-title="ตั้งแต่"><input type="text" value="0" /></td>
                               <td data-title="ถึง"><input type="text" value="50" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="ยกเว้น" /></td>  							   
                               </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="50" /></td>
                               <td data-title="ถึง"><input type="text" value="100" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.05" /></td>  						   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="100" /></td>
                               <td data-title="ถึง"><input type="text" value="" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.10" /></td>  
							   
						   </tr>
					    	
					</tbody>
				</table> 
                </div>
            </div>




    			<div class="panel-body p-n" style="font-size: 14.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="1" width="100%">
				
					<tbody><tr>
							   <td class ="cennow" colspan="3" data-title="มูลค่าของทรัพย์สิน(บาท)">มูลค่าทรัพย์สิน(บ้านหลังที่สอง)</td>
                               </tr>

						      <tr class="cennow">

                               <td data-title="ตั้งแต่" >ตั้งแต่   (ลบ.)</td>
                               <td data-title="ถึง">ถึง   (ลบ.)</td>
                               <td data-title="คิดภาษี(%)">อัตราภาษี (%)</td>
							   
                              </tr>		
                        				   
                             <tr>
							   <td data-title="ตั้งแต่"><input type="text" value="0" /></td>
                               <td data-title="ถึง"><input type="text" value="5" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.03" /></td>  							   
                               </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="5" /></td>
                               <td data-title="ถึง"><input type="text" value="10" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.05" /></td>  						   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="10" /></td>
                               <td data-title="ถึง"><input type="text" value="20" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.10" /></td>  							   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="20" /></td>
                               <td data-title="ถึง"><input type="text" value="30" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.15" /></td>  							   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="30" /></td>
                               <td data-title="ถึง"><input type="text" value="50" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.20" /></td>  							   
						   </tr>
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="50" /></td>
                               <td data-title="ถึง"><input type="text" value="100" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.25" /></td>  							   
						   </tr>                                         
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="100" /></td>
                               <td data-title="ถึง"><input type="text" value="" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.30" /></td>  							   
						   </tr>       					    	
					</tbody>
				</table> 
                </div>
            </div>

		      </div>
        <div class="col-md-3">
        <div style="background-color:#ffd1c4;padding:10px">
           <h3 style="text-align:center;margin-top:0px"><b>พาณิชยกรรม</b></h3> 
                   <h4 style="text-align:center;"><span>อัตราเพดาน</span><input type="text" value="2.00 %" /></h4>
         </div>
    			<div class="panel-body p-n" style="font-size: 14.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered" cellspacing="1" width="100%">
				
					<tbody><tr>
							   <td class ="cennow" colspan="3" data-title="มูลค่าของทรัพย์สิน(บาท)">มูลค่าทรัพย์สิน</td>
                               </tr>
						      <tr class="cennow">

                               <td data-title="ตั้งแต่" >ตั้งแต่   (ลบ.)</td>
                               <td data-title="ถึง">ถึง   (ลบ.)</td>
                               <td data-title="คิดภาษี(%)">อัตราภาษี (%)</td>
							   
                              </tr>	
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="0" /></td>
                               <td data-title="ถึง"><input type="text" value="20" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.30" /></td>  							   
						   </tr>       
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="20" /></td>
                               <td data-title="ถึง"><input type="text" value="50" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.50" /></td>  							   
						   </tr>       
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="50" /></td>
                               <td data-title="ถึง"><input type="text" value="100" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.70" /></td>  							   
						   </tr>       
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="100" /></td>
                               <td data-title="ถึง"><input type="text" value="1000" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="0.90" /></td>  							   
						   </tr>       
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="1000" /></td>
                               <td data-title="ถึง"><input type="text" value="3000" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="1.20" /></td>  							   
						   </tr>       
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="3000" /></td>
                               <td data-title="ถึง"><input type="text" value="" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="1.50" /></td>  							   
						   </tr>   
					</tbody>
				</table>
                </div>
            </div>
        </div><div class="col-md-3 p-n">
               <div style="background-color:#fff9b5;padding:10px">
                   <h3 style="text-align:center;margin-top:0px"><b>ที่รกร้างว่างเปล่า</b></h3> 
                   <h4 style="text-align:center;"><span>อัตราเพดาน</span><input type="text" value="5.00 %" /></h4>
                </div>
    			<div class="panel-body p-n" style="font-size: 14.5px;">
                <div class="table-vertical">
                <table class="table table-striped table-bordered cennow" cellspacing="1" width="100%" >
				
					<tbody>
						<tr>
							   <td class ="cennow" colspan="3" data-title="มูลค่าของทรัพย์สิน(บาท)">ปีที่ถูกปล่อยให้รกร้าง</td>
                               </tr>
						      <tr class="cennow">

                               <td data-title="ตั้งแต่" >ตั้งแต่   (ปี)</td>
                               <td data-title="ถึง">ถึง   (ปี)</td>
                               <td data-title="คิดภาษี(%)">อัตราภาษี (%)</td>
							   
                              </tr>	
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="1" /></td>
                               <td data-title="ถึง"><input type="text" value="3" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="1.00" /></td>  							   
						   </tr>   
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="4" /></td>
                               <td data-title="ถึง"><input type="text" value="6" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="2.00" /></td>  							   
						   </tr>   
					    	<tr>
							   <td data-title="ตั้งแต่"><input type="text" value="7" /></td>
                               <td data-title="ถึง"><input type="text" value="" /></td>
                               <td data-title="คิดภาษี(%)"><input type="text" value="3.00" /></td>  							   
						   </tr>     
					</tbody>
				</table>     
                </div>
            </div>
              </div>

</div>
        <div class="clearfix pt20">
				                    <div class="pull-right">
					                    <button class="btn-primary btn">บันทึก</button>
				                    </div>

									</div>
</asp:Content>