﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition.master" AutoEventWireup="true" CodeFile="IMPOSITION_Detailview.aspx.cs" Inherits="IMPOSITION_HOME_Listview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h2 class="title">บ้านพักอาศัย</h2>
       <div style="float:right;">
                 <a role="button" class="btn btn-default" href="#" style="float:right;margin-top:8px;"><i class="fa fa-file-text-o" style="float:left;margin-right:10px;"></i>ออกรายงาน</a>
                  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">




    
<link type="text/css" href="assets/plugins/kartik-file-uploader/css/fileinput.min.css" rel="stylesheet">
<link type="text/css" href="assets/plugins/dropzone/css/dropzone.css" rel="stylesheet">
    <br/>
   				<div class="tab-content">
					<div class="tab-pane active" id="timeline">
						<ul class="timeline">
							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">1</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    
                                        <fieldset>
			            	<legend>รายระเอียดอัตราภาษี</legend>
					                       
                                            <form class="grid-form">
                    <div data-row-span="2">
					<div data-field-span="1" style="height: 64px;">
					    <label>&nbsp;</label>
						<select class="dlhide" style="height:38px;">
				                                <option>ประเภทอัตราภาษี</option>
				                                <option>ที่ดินรกร้าง</option>
				                                <option>บ้านพักอาศัย</option>
				                                <option>เกษตร</option>
						</select>
					</div>
					<div data-field-span="1" style="height: 64px;" class="">
				    <label>ชื่ออัตราภาษี</label>
												<select class="dlhide" style="height:38px;">
				                                <option>ระบุ</option>
				                                <option>บ้านหลังหลัก</option>
				                                <option>บ้านหลังที่สอง</option>
						</select>
					</div>
				</div>
                <div data-row-span="2">
					<div data-field-span="1" style="height: 64px;">
					    <label>&nbsp;</label>
						<input type="text" placeholder="วันที่เริ่มบังคับใช้"  style="font-size:20px;">
					</div>
					<div data-field-span="1" style="height: 64px;" class="">
				    <label>&nbsp;</label>
						<input type="text" placeholder="หมายเหตุ"  style="font-size:20px;">
					</div>
				</div>
                                            </form>
                                                   
				                    </fieldset>
                                     <div class="clearfix pt20">
				                        <div class="pull-right">
				                        </div>
			                        </div>
									</div>

								</div>
							</li>

                            <!-- 2-->
                            <br/>
							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">2</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    
                                        <fieldset>
			            	<legend>เงื่อนไขอัตราภาษี</legend>

                    <br/>
					<div class="form-group">
						<div class="row m-n">
							<div class="col-xs-2">อัตราเพดานภาษี</div>
							<div class="col-xs-2">
                                <div class="input-group">
                                    <input style="text-align:center;" type="number" value="0.20" step="0.01" min="0.00" max="100.00" class="form-control">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
							</div>
						</div>

            <div class="form-group" id="myDivsp">
                             <div class="col-xs-2">เงื่อนไขภาษี</div><br/>
            <div id="my0Div">
                <div class="row m-n" id="myDivsprowsp0">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <select class="form-control" id="ddlViewBy0" onchange="changetype('ddlViewBy0','myDivsprowsp0');">
                            <option>-</option>
                            <option value="1" selected="">ระหว่าง</option>
                            <option value="2">ตั้งแต่</option></select> 

                    </div>
                    
                    
                    <div class="col-md-2">
                        <input style="text-align:right;" type="number" class="form-control" value="0" placeholder="ต่ำสุด" step="0.01" min="0.00">
                    </div>
                    <div class="col-md-1">
                        <select class="form-control"><option>บาท</option><option>ปี</option></select>
                    </div>
                    <div class="col-md-2">
                        <input type="number"  style="text-align:right;" value="50" class="form-control" placeholder="สูงสุด" step="0" min="0.00">
                    </div>
                    <div class="col-md-1">
                        <select class="form-control">
                            <option>บาท</option>
                            <option>ปี</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <input style="text-align:center;" type="number" value="0.00" step="0.01" min="0.00" max="100.00" class="form-control">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="col-md-1"> 
                        <input type="button" value="Remove" onclick="removerows('myDivsprowsp0')" ;="">
                    </div>

                </div>

            </div>
            </div>
             
                    <div class="row m-n" >
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
        
              <p><a href="javascript:;" onclick="addElementsp('myDivsp');">เพิ่มเงื่อนไข</a></p>          </div>
           </div>

					                               
				                    </fieldset>
                                     <div class="clearfix pt20">
				                        <div class="pull-right">
                                        <br>
				                        </div>
			                        </div>
									</div>

								</div>
							</li>
                            <!-- 3-->

							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">3</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">
                                        <fieldset>
			            	<legend>ทดสอบคำนวณภาษี</legend>
                                                 					                               
					<div class="form-group">

						<div class="row m-n">
                                <input type="text" class="form-control" />
                          </div>
						<div class="row m-n">
							<div class="col-xs-2"></div>
							<div class="col-xs-8">
								<pre id="result" class="mt20">
                
                                    
                                    
                                    
								</pre>
								<button id="randomize" class="btn btn-default btn-block">Calculate Tax</button>
                            </div>
                            </div>
                        </div>				                    </fieldset>
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
                                    
                                    <br>
				                    </div>
			                        </div>
                                        
                                    </form>
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
					                    <button class="btn-primary btn">Submit</button>
					                    <button class="btn-defualt btn">&nbsp;CLEAR&nbsp;</button>
				                    </div>

									</div>
                                    </div>
								</div>
							</li>
						

						</ul>
                        </div>
                        </div>


    
<script type="text/javascript" src="assets/plugins/kartik-file-uploader/js/fileinput.min.js"></script>
<script type="text/javascript" src="assets/demo/kartik-file-uploader/data.js"></script>
<script type="text/javascript" src="assets/plugins/dropzone/dropzone.js"></script>








</asp:Content>

