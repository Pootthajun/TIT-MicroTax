﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_OwnerShip_Detail.master" AutoEventWireup="true" CodeFile="OWN_FOUNDATION_DetailView.aspx.cs" Inherits="OWN_FOUNDATION_DetailView" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">มูลนิธิ</h3> <!-- หัวข้อ -->
<a role="button" class="btn btn-default tr8 fr" href="OWN_FOUNDATION_ListView.aspx"><i class="fa fa-arrow-left lr10"></i>ย้อนกลับ</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> <!-- ข้อความ -->
   				<div class="tab-content">   <!-- tab all -->
					<div class="tab-pane active" id="timeline">   
						<ul class="timeline">   <!--all  time line  -->
							<li class="timeline-success">   <!-- time line 1 -->
								<div class="timeline-icon hidden-xs">1</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                    <form class="grid-form">


			<fieldset>
				<legend>มูลนิธิ  ( รหัสเจ้าของ: <u style="color:blue;">ยังกรอกข้อมูลไม่ครบ</u> )</legend>
		<!--		<div data-row-span="5">
	        		<div data-field-span="1" class="h64">
					    <label>&nbsp;</label>
						<select class="dlhide h37" style="height:38px;">
				                                <option>คำนำหน้า</option>
				                                <option>นาย</option>
				                                <option>นาง</option>
				                                <option>นางสาว</option>
						</select>
					</div>
					<div data-field-span="2" class="h64"  >
				    <label>&nbsp;</label>
						<input type="text" placeholder="ชื่อ"  class="f20">
					</div>
					<div data-field-span="2" class="h64"  >
				    <label>&nbsp;</label>
						<input type="text" placeholder="นามสกุล"  class="f20">
					</div>
				</div>
                -->
				<div data-row-span="2">
					<div data-field-span="1"    >
						<label id="lb0">&nbsp;</label> 
                        <input id="inp0" type="text" class="f20"  onfocusout="showlabel('inp0','lb0','ชื่อมูลนิธิ');" placeholder="ชื่อมูลนิธิ">
					</div>
					<div data-field-span="1"   >
						<label id="lb1">&nbsp;</label> 
                        <input id="inp1" type="text" onfocusout="showlabel('inp1','lb1','หมายเลขผู้เสียภาษี');" class="form-control mask f20" data-inputmask="'mask':'9  -  9999  -  99999  -  9  -  99'" placeholder="หมายเลขผู้เสียภาษี">
					</div>

				<!--	<div data-field-span="1" class="h64">
						<label>&nbsp;</label>
						<input type="text" disabled="disabled" placeholder="รหัสเจ้าของกรรมสิทธิ์">
					</div> -->
				</div>
                </fieldset> 
                <div class="clearfix pt20">   <!-- foot timeline1  -->
				<div class="pull-right">
                <br><br><br>
				</div>
			    </div>
                </form>

                </div>
                </div>
                  
            </li>
             <!-- end timeline1 -->

                            

							<li class="timeline-danger">   <!-- time line 2 -->
								<div class="timeline-icon hidden-xs">2</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">
                                        <fieldset>
			            	<legend>ที่อยู่</legend>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb2">&nbsp;</label>
							                    <input id="inp2" type="text" placeholder="บ้านเลขที่"  onfocusout="showlabel('inp2','lb2','บ้านเลขที่');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb3">&nbsp;</label>
							                    <input id="inp3" type="text" placeholder="หมู่บ้าน/ชื่อสถานที่" onfocusout="showlabel('inp3','lb3','หมู่บ้าน/ชื่อสถานที่');">
						                    </div>
					                    </div>
					                    <div data-row-span="2">
						                    <div data-field-span="1">
							                    <label id="lb4">&nbsp;</label>
							                    <input id="inp4" type="text" placeholder="ซอย" onfocusout="showlabel('inp4','lb4','ซอย');">
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb5">&nbsp;</label>
							                    <input id="inp5" type="text" placeholder="ถนน" onfocusout="showlabel('inp5','lb5','ถนน');">
						                    </div>
					                    </div>
					                    <div data-row-span="4">
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37"  >
				                                <option>ระบุจังหวัด</option>
				                                <option>จังหวัด01</option>
				                                <option>จังหวัด02</option>
				                                <option>จังหวัด03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37"  >
				                                <option>ระบุอำเภอ</option>
				                                <option>อำเภอ01</option>
				                                <option>อำเภอ02</option>
				                                <option>อำเภอ03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label>&nbsp;</label>
							                <select class="dlhide h37"  >
				                                <option>ระบุตำบล</option>
				                                <option>ตำบล01</option>
				                                <option>ตำบล02</option>
				                                <option>ตำบล03</option>
						                    </select>
						                    </div>
						                    <div data-field-span="1">
							                    <label id="lb6">&nbsp;</label>
							                    <input id="inp6" type="text" placeholder="ไปรษณีย์" onfocusout="showlabel('inp6','lb6','ไปรษณีย์');">
						                    </div>
					                    </div>
				                    </fieldset>
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
                                    <br><br><br>
				                    </div>
			                        </div>
                                    </form>
									</div>

								</div>
							</li>
                             <!-- end timeline2 -->

                            <!-- timeline3-->
							<li class="timeline-danger">
								<div class="timeline-icon hidden-xs">3</div>
								<div class="timeline-body pt-n">
									<div class="timeline-content">
                                    <form class="grid-form">     
                                    <fieldset> 	
                           	        <legend>ข้อมูลติดต่อ</legend>

         <!-- modal -->
         <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg" >
    
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">เพิ่มข้อมูลติดต่อ</h4>
                </div>
                <div class="modal-body" style="padding:0px;">
                                      

				                            <div data-row-span="5">
					                            <div data-field-span="1" class="h64">
					                                <label>&nbsp;</label>
						                            <select class="dlhide h37" style="height:38px;">
				                                                            <option>คำนำหน้า</option>
				                                                            <option>นาย</option>
				                                                            <option>นาง</option>
				                                                            <option>นางสาว</option>
						                            </select>
					                            </div>
					                            <div data-field-span="2" class="h64"  >
				                                <label >&nbsp;</label>
						                            <input type="text" placeholder="ชื่อ" class="f20">
					                            </div>
					                            <div data-field-span="2" class="h64"  >
				                                <label>&nbsp;</label>
						                            <input type="text" placeholder="นามสกุล"  class="f20">
					                            </div>
				                            </div>
					                            <div data-row-span="2">
						                            <div data-field-span="1">
							                            <label id="lb7">&nbsp;</label>
							                            <input id="inp7" type="text" placeholder="โทรศัพท์" onfocusout="showlabel('inp7','lb7','โทรศัพท์');">
						                            </div>
						                            <div data-field-span="1">
							                            <label id="lb8">&nbsp;</label>
							                            <input id="inp8" type="text" placeholder="โทรศัพท์มือถือ" onfocusout="showlabel('inp8','lb8','โทรศัพท์มือถือ');">
						                            </div>
					                            </div>
					                            <div data-row-span="2">
						                            <div data-field-span="1">
							                            <label id="lb9">&nbsp;</label>
							                            <input id="inp9" type="text" placeholder="โทรสาร"  onfocusout="showlabel('inp9','lb9','โทรสาร');">
						                            </div>
						                            <div data-field-span="1">
							                            <label id="lb10">&nbsp;</label>
							                            <input id="inp10" type="text" placeholder="อีเมล์"  onfocusout="showlabel('inp10','lb10','อีเมล์');">
						                            </div>
					                            </div>
                                          <div class="modal-footer">
					                            <button class="btn-primary btn">บันทึก</button>
					                            <button class="btn-defualt btn">ล้าง</button>
                                          </div>
					              
                </div>
              </div>
      
            </div>
          </div>
         <!-- end modal -->
      
				                    </fieldset>  


                                    </form>
         <!-- table -->
                        <div class="table-vertical">
                        <table class="table table-striped">
                            <thead>
                                <tr >
                                    <th class="cennow w50">ลำดับ</th>
                                    <th class="cennow">คำนำหน้า</th>
                                    <th class="cennow">ชื่อ</th>
                                    <th class="cennow">นามสกุล</th>
                                    <th class="cennow">โทรศัพท์</th>
                                    <th class="cennow">โทรศัพท์มือถือ</th>
                                    <th class="cennow w90">โทรสาร</th>
                                    <th class="cennow">อีเมลล์</th>
                                    <th class="cennow w100" style="width:5%;font-size:3px;display:none">เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody>
                              <tr>
                                    <td data-title="ลำดับ"><input type="text"  class="texttable" /></td>
                                    <td data-title="คำนำหน้า"><input type="text" class="texttable" /></td>
                                    <td data-title="ชื่อ"><input type="text" class="texttable" /></td>
                                    <td data-title="นามสกุล"><input type="text" class="texttable" /></td>
                                    <td data-title="โทรศัพท์"><input type="text" class="texttable" /></td>
                                    <td data-title="โทรศัพท์มือถือ"><input type="text" class="texttable" /></td>
                                    <td data-title="โทรสาร"><input type="text" class="texttable" /></td>
                                    <td data-title="อีเมลล์"><input type="text" class="texttable"/></td>
                                    <td data-title="เครื่องมือ" class="cennow"> <button type="button" class="btn-success btn" data-toggle="modal" data-target="#myModal" >เพิ่ม</button>
                                    </td>
                                </tr> 
                                <tr>
                                    <td data-title="ลำดับ">1</td>
                                    <td data-title="คำนำหน้า">นาย</td>
                                    <td data-title="ชื่อ">สุขุม</td>
                                    <td data-title="นามสกุล">เยือกเย็น</td>
                                    <td data-title="โทรศัพท์">02-530-5340</td>
                                    <td data-title="โทรศัพท์มือถือ">085-096-8475</td>
                                    <td data-title="โทรสาร">-</td>
                                    <td data-title="อีเมลล์">sukum@test.com</td>
							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">2</td>
                                    <td data-title="คำนำหน้า">นาง</td>
                                    <td data-title="ชื่อ">ยิ่งลักษณ์</td>
                                    <td data-title="นามสกุล">บุญฤทธิ์</td>
                                    <td data-title="โทรศัพท์">02-514-1434</td>
                                    <td data-title="โทรศัพท์มือถือ">-</td>
                                    <td data-title="โทรสาร">02-510-1433</td>
                                    <td data-title="อีเมลล์">lot_taew@hotmail.com</td>
							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">3</td>
                                    <td data-title="คำนำหน้า">นาย</td>
                                    <td data-title="ชื่อ">พิพัฒน์พงศ์</td>
                                    <td data-title="นามสกุล">สงนาค</td>
                                    <td data-title="โทรศัพท์">-</td>
                                    <td data-title="โทรศัพท์มือถือ">098-934-3833</td>
                                    <td data-title="โทรสาร">-</td>
                                    <td data-title="อีเมลล์">czechzat@hotmail.com</td>
							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">4</td>
                                    <td data-title="คำนำหน้า">นาย</td>
                                    <td data-title="ชื่อ">ศุภพงศ์</td>
                                    <td data-title="นามสกุล">เวียงตาลจันทรา</td>
                                    <td data-title="โทรศัพท์">02-277-6396</td>
                                    <td data-title="โทรศัพท์มือถือ">-</td>
                                    <td data-title="โทรสาร">044-299603</td>
                                    <td data-title="อีเมลล์">damonjang-11-@hotmail.com</td>
							       	<td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>		
                                </tr>

                                <tr>
                                    <td data-title="ลำดับ">5</td>
                                    <td data-title="คำนำหน้า">นางสาว</td>
                                    <td data-title="ชื่อ">วรรณา</td>
                                    <td data-title="นามสกุล">คำเพิ่ม</td>
                                    <td data-title="โทรศัพท์">-</td>
                                    <td data-title="โทรศัพท์มือถือ">085-8222-133</td>
                                    <td data-title="โทรสาร">-</td>
                                    <td data-title="อีเมลล์">jarujo_28989@hotmail.com</td>
							       <td data-title="เครื่องมือ" class="vamiddle">
                                       <div class="btn-toolbar" >
                                        <div class="btn-group fl" >
                                            <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                                <i class="ion-android-menu"></i>
                                            </button>
                                            <ul class="dropdown-menu mg">
                                         
				    	                    
				    	                    <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                    <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>	
                                </tr>

                            </tbody>
                        </table>
                 <!-- end table -->
                    </div>
									</div>

								</div>
                 <!-- footer -->
                                 <div class="panel-footer">
                                    <div class="clearfix pt20">
				                    <div class="pull-right">
					                    <button class="btn-primary btn">บันทึก</button>
					                    <button class="btn-defualt btn">&nbsp;ล้าง&nbsp;</button>
				                    </div>
			                        </div>
                                </div>
                 <!-- end footer -->
							</li>
                 <!-- end timeline3 -->

						</ul>
                 <!-- end all timeline -->
                        </div>
</div>
                 <!-- end -->


</asp:Content>

