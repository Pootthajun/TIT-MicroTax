﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Imposition.master" AutoEventWireup="true" CodeFile="IMPOSITION_SIGN.aspx.cs" Inherits="IMPOSITION_SIGN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><h3 class="title pull-left">ป้าย</h3>
        <div class="pull-right middle" >

            <div class="dropdown pull-right rt14">
				<a role="button" class="btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false" >
					<span class="text hidden-sm" data-localize="topnav_dropdown"><i class="fa fa-file-text-o lr10"></i> ออกรายงาน</span>
				</a>
				<ul class="dropdown-menu" style="width:120px">
				    	<li><a href="#"><i class="fa fa-print" style="color:#00cc00" ></i><span >PRINT</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-pdf-o" style="color:#ff3300"></i><span >PDF</span></a></li>
				    	<li><a href="#"><i class="fa fa-file-excel-o" style="color:#0066ff;"></i><span >EXCEL</span></a></li>	
			</ul>
			</div>
                 <a role="button" class="btn btn-default tr8 fr" href="IMPOSITION_SIGN_Detail.aspx" ><i class="ion-android-create lr10"></i>เพิ่มข้อมูล</a>  
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">


<style>
    td,th{
        text-align:center;
    }
    .textleft{
        text-align:left;
    }
</style>
<div class="row">
<div class="table-vertical">
                <table  class="table table-striped table-bordered">
					<thead style="background-color:skyblue;">
						<tr>
                            <th style="width:10%;">ลำดับ</th>
                            <th style="width:30%;">ประเภท</th>
							<th style="width:25%;">อัตราภาษี (บาท)</th>
							<th style="width:15%;">มาตราส่วน (ตร.ซม)</th>
							<th style="width:15%;">อัตราภาษีขั้นต่ำ (บาท)</th>
                            <th style="width:5%;visibility: hidden;font-size:3px">เครื่องมือ</th>
						</tr>
					</thead>

						<tr style="background-color:#ffffff">
							<td data-title="ลำดับ" ><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="ประเภท"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
                            <td data-title="อัตราภาษี (บาท)"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="มาตราส่วน (ตร.ซม)"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="อัตราภาษีขั้นต่ำ (บาท)"><input type="text" class="form-control mgb" placeholder="Search..."   /></td>
							<td data-title="เครื่องมือ" style="vertical-align:middle">
                               <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt w40b" >                                           
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
							   </td>
					   </tr>
					<tbody>
						<tr>
							<td data-title="ลำดับ" >1</td>
							<td data-title="ประเภท" class="leftnow">ป้ายมีอักษรไทยล้วน</td>
							<td data-title="อัตราภาษี (บาท)" class="leftnow">3.00</td>
							   <td data-title="มาตราส่วน">500</td>
                               <td data-title="อัตราภาษีขั้นต่ำ (บาท)">200.00</td>
   							   <td data-title="เครื่องมือ (ตร.ซม)" style="vertical-align:middle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                        <li><a href="#"><i class="fa fa-map-marker" style="color:brown"></i><span>แผนที่</span></a></li>
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>

					    	  <tr>
							<td data-title="ลำดับ" >2</td>
							<td data-title="ประเภท" class="leftnow">ป้ายมีอักษรไทยปนอังกฤษ</td>
							<td data-title="อัตราภาษี (บาท)" class="leftnow">20.00</td>
							   <td data-title="มาตราส่วน">500</td>
                               <td data-title="อัตราภาษีขั้นต่ำ (บาท)">200.00</td>
   							   <td data-title="เครื่องมือ (ตร.ซม)" style="vertical-align:middle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                        <li><a href="#"><i class="fa fa-map-marker" style="color:brown"></i><span>แผนที่</span></a></li>
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
					    	   <tr>
							<td data-title="ลำดับ" >3</td>
							<td data-title="ประเภท" class="leftnow">	ก ป้ายไม่มีอักษรไทย</td>
							<td data-title="อัตราภาษี (บาท)" class="leftnow">40.00</td>
							   <td data-title="มาตราส่วน">500</td>
                               <td data-title="อัตราภาษีขั้นต่ำ (บาท)">200.00</td>
   							   <td data-title="เครื่องมือ (ตร.ซม)" style="vertical-align:middle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                        <li><a href="#"><i class="fa fa-map-marker" style="color:brown"></i><span>แผนที่</span></a></li>
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
                               <tr>
							<td data-title="ลำดับ" >4</td>
							<td data-title="ประเภท" class="leftnow">	ข ป้ายมีอักษรไทยบางส่วนอยู่ต่ำกว่าภาษาอังกฤษ</td>
							<td data-title="อัตราภาษี (บาท)" class="leftnow">40.00</td>
							   <td data-title="มาตราส่วน">500</td>
                               <td data-title="อัตราภาษีขั้นต่ำ (บาท)">200.00</td>
   							   <td data-title="เครื่องมือ (ตร.ซม)" style="vertical-align:middle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                        <li><a href="#"><i class="fa fa-map-marker" style="color:brown"></i><span>แผนที่</span></a></li>
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>
					    	  <tr>
							<td data-title="ลำดับ" >5</td>
							<td data-title="ประเภท" class="leftnow">อื่นๆ</td>
							<td data-title="อัตราภาษี (บาท)" class="leftnow">40.00</td>
							   <td data-title="มาตราส่วน">500</td>
                               <td data-title="อัตราภาษีขั้นต่ำ (บาท)">100.00</td>
   							   <td data-title="เครื่องมือ (ตร.ซม)" style="vertical-align:middle">
                                   <div class="btn-toolbar" >
                                    <div class="btn-group fr"  >
                                        <button type="button" class="btn btn-default-alt alt dropdown-toggle w40" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                                            
                                            <i class="ion-android-menu"></i>
                                        </button>
                                        <ul class="dropdown-menu mg">
                                        <li><a href="#"><i class="fa fa-map-marker" style="color:brown"></i><span>แผนที่</span></a></li>
				    	                <li><a href="ENROLL_View.aspx"><i class="fa fa-search" style="color:#00cc00"></i><span>ดู</span></a></li>
				    	                <li><a href="#"><i class="fa fa-wrench" style="color:#ff3300"></i><span>แก้ไข</span></a></li>
				    	                <li><a href="#"><i class="ion-android-delete" style="color:#0066ff;"></i><span>ลบ</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </td>		
                            
                               </tr>

					</tbody>
				</table>
				<div class="panel-footer">
                    <ul class="pagination pagination-sm pull-left">
                    <li class="disabled"><a href="#"><i class="ion-ios-arrow-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="ion-ios-arrow-right"></i></a></li>
                    </ul>
				</div>     
                </div>
	
</div>
                    


 



</asp:Content>

