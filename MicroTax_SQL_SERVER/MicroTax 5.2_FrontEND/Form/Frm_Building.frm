VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Building 
   ClientHeight    =   9105
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12900
   ControlBox      =   0   'False
   Icon            =   "Frm_Building.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Building.frx":151A
   ScaleHeight     =   9105
   ScaleWidth      =   12900
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Cmb_Building_Type_II_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Building.frx":15854
      Left            =   810
      List            =   "Frm_Building.frx":15856
      TabIndex        =   231
      Top             =   480
      Visible         =   0   'False
      Width           =   2745
   End
   Begin VB.ComboBox Cmb_Building_Type_II 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Building.frx":15858
      Left            =   1290
      List            =   "Frm_Building.frx":1585A
      TabIndex        =   230
      Top             =   1770
      Width           =   1965
   End
   Begin VB.ComboBox cb_Building_Change 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4230
      Style           =   2  'Dropdown List
      TabIndex        =   229
      Top             =   870
      Visible         =   0   'False
      Width           =   8655
   End
   Begin VB.ComboBox cb_Building_Change_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   630
      Style           =   2  'Dropdown List
      TabIndex        =   228
      Top             =   90
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.ComboBox Cmb_Building_Type 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Building.frx":1585C
      Left            =   1290
      List            =   "Frm_Building.frx":1585E
      TabIndex        =   5
      Top             =   2190
      Width           =   1965
   End
   Begin VB.CheckBox Chk_Flag_PayTax1 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   8550
      TabIndex        =   224
      Top             =   3540
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_PayTax0 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   9990
      TabIndex        =   223
      Top             =   3540
      Width           =   195
   End
   Begin VB.TextBox Txt_StoryRemark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4230
      TabIndex        =   220
      Text            =   "-"
      Top             =   900
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.CheckBox Chk_BUILDING_FLAG1 
      BackColor       =   &H00D6D6D6&
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   2400
      TabIndex        =   135
      Top             =   2670
      Width           =   195
   End
   Begin VB.CheckBox Chk_BUILDING_FLAG0 
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   1290
      TabIndex        =   132
      Top             =   2670
      Width           =   195
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Building.frx":15860
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   2
      Left            =   4290
      Picture         =   "Frm_Building.frx":18F0F
      Style           =   1  'Graphical
      TabIndex        =   96
      TabStop         =   0   'False
      Top             =   4920
      Width           =   4290
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Building.frx":1C0B4
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   1
      Left            =   30
      Picture         =   "Frm_Building.frx":1F4A5
      Style           =   1  'Graphical
      TabIndex        =   95
      TabStop         =   0   'False
      Top             =   4710
      Value           =   -1  'True
      Width           =   4260
   End
   Begin VB.PictureBox picParent 
      BackColor       =   &H00696969&
      Height          =   2145
      Left            =   10560
      ScaleHeight     =   2085
      ScaleWidth      =   2235
      TabIndex        =   117
      Top             =   1260
      Width           =   2295
      Begin VB.PictureBox picPicture 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   2085
         Left            =   0
         MouseIcon       =   "Frm_Building.frx":2240F
         ScaleHeight     =   2085
         ScaleWidth      =   2235
         TabIndex        =   118
         Top             =   0
         Width           =   2235
      End
      Begin VB.Image ImgSize 
         Height          =   1305
         Left            =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.CommandButton Btn_FullScreen 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_Building.frx":22719
      Style           =   1  'Graphical
      TabIndex        =   115
      Top             =   3090
      Width           =   345
   End
   Begin VB.CommandButton Btn_SelectImg 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_Building.frx":22CA3
      Style           =   1  'Graphical
      TabIndex        =   116
      Top             =   2790
      Width           =   345
   End
   Begin VB.ComboBox Cmb_Land_ID 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1290
      TabIndex        =   12
      Top             =   3960
      Width           =   1755
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Building.frx":2322D
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   3
      Left            =   8580
      Picture         =   "Frm_Building.frx":262E2
      Style           =   1  'Graphical
      TabIndex        =   97
      TabStop         =   0   'False
      Top             =   4920
      Width           =   4290
   End
   Begin VB.CheckBox CHK_BUILDING_STATUS0 
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   11490
      TabIndex        =   75
      Top             =   4050
      Width           =   195
   End
   Begin VB.CheckBox CHK_BUILDING_STATUS1 
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   11490
      TabIndex        =   73
      Top             =   4440
      Width           =   195
   End
   Begin VB.TextBox Txt_BUILDING_PRICE 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   4170
      Locked          =   -1  'True
      MaxLength       =   11
      TabIndex        =   7
      Text            =   "0.00"
      Top             =   3030
      Width           =   1425
   End
   Begin VB.ComboBox Cmb_Tamlay 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Building.frx":28F7A
      Left            =   4140
      List            =   "Frm_Building.frx":28FAE
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   2580
      Width           =   1485
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4215
      Left            =   30
      TabIndex        =   59
      Top             =   4920
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   7435
      _Version        =   393216
      Style           =   1
      TabHeight       =   529
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "Frm_Building.frx":28FF2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Image1(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2(30)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label2(31)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Shape1(15)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label2(32)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label2(33)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Shape1(16)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label2(34)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label2(35)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Shape1(17)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label2(36)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Shape1(18)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label2(37)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label2(38)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Label2(39)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Label2(40)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Shape1(19)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Shape1(24)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Shape1(25)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Label2(29)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Shape1(14)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Label2(112)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "DTP_BUILDING_STATE_DATE"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Chk_BUILDING_STATE2"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Chk_BUILDING_STATE1"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Chk_BUILDING_STATE3"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Txt_BUILDING_STATE_PRICE"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Txt_BUILDING_STATE_OTHER_PRICE"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "Txt_BUILDING_STATE_YEAR"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "Txt_BUILDING_STATE_REMARK"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "Txt_BUILDING_STATE_DETAILS"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "Txt_Building_Name"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).ControlCount=   32
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "Frm_Building.frx":2900E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LstV_OwnerShip"
      Tab(1).Control(1)=   "LstV_OwnerUse"
      Tab(1).Control(2)=   "Lb_User_Real_ID"
      Tab(1).Control(3)=   "Lb_User_Email"
      Tab(1).Control(4)=   "Lb_User_Address"
      Tab(1).Control(5)=   "Lb_User_Name"
      Tab(1).Control(6)=   "Lb_User_Type"
      Tab(1).Control(7)=   "Lb_Email"
      Tab(1).Control(8)=   "Lb_Owner_Address"
      Tab(1).Control(9)=   "Lb_Owner_Name"
      Tab(1).Control(10)=   "Lb_Owner_Type"
      Tab(1).Control(11)=   "Lb_Ownership_Real_ID"
      Tab(1).Control(12)=   "Shape1(34)"
      Tab(1).Control(13)=   "Label2(51)"
      Tab(1).Control(14)=   "Shape1(33)"
      Tab(1).Control(15)=   "Label2(50)"
      Tab(1).Control(16)=   "Label2(49)"
      Tab(1).Control(17)=   "Shape1(32)"
      Tab(1).Control(18)=   "Label2(48)"
      Tab(1).Control(19)=   "Shape1(31)"
      Tab(1).Control(20)=   "Shape1(30)"
      Tab(1).Control(21)=   "Label2(47)"
      Tab(1).Control(22)=   "Label2(46)"
      Tab(1).Control(23)=   "Shape1(29)"
      Tab(1).Control(24)=   "Shape1(28)"
      Tab(1).Control(25)=   "Label2(45)"
      Tab(1).Control(26)=   "Shape1(27)"
      Tab(1).Control(27)=   "Label2(44)"
      Tab(1).Control(28)=   "Label2(41)"
      Tab(1).Control(29)=   "Shape1(26)"
      Tab(1).Control(30)=   "Label2(52)"
      Tab(1).Control(31)=   "Shape1(35)"
      Tab(1).Control(32)=   "Shape1(23)"
      Tab(1).Control(33)=   "Shape1(22)"
      Tab(1).Control(34)=   "Shape1(21)"
      Tab(1).Control(35)=   "Label2(43)"
      Tab(1).Control(36)=   "Label2(42)"
      Tab(1).Control(37)=   "Shape1(20)"
      Tab(1).Control(38)=   "Shape3(2)"
      Tab(1).Control(39)=   "Shape3(5)"
      Tab(1).Control(40)=   "Image1(2)"
      Tab(1).ControlCount=   41
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "Frm_Building.frx":2902A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Image1(3)"
      Tab(2).Control(1)=   "Label2(54)"
      Tab(2).Control(2)=   "Label2(55)"
      Tab(2).Control(3)=   "Label2(56)"
      Tab(2).Control(4)=   "Shape1(36)"
      Tab(2).Control(5)=   "Label2(57)"
      Tab(2).Control(6)=   "Label2(58)"
      Tab(2).Control(7)=   "Lb_GrandTotal_Tax"
      Tab(2).Control(8)=   "Label2(59)"
      Tab(2).Control(9)=   "Label2(60)"
      Tab(2).Control(10)=   "Label2(61)"
      Tab(2).Control(11)=   "Shape1(55)"
      Tab(2).Control(12)=   "Shape1(56)"
      Tab(2).Control(13)=   "Txt_RentYear_Total_All"
      Tab(2).Control(14)=   "Shape1(74)"
      Tab(2).Control(15)=   "SSTab2"
      Tab(2).Control(16)=   "Cmb_BuildingType"
      Tab(2).Control(17)=   "Chk_Person"
      Tab(2).Control(18)=   "ChkSystem"
      Tab(2).Control(19)=   "combo1"
      Tab(2).Control(20)=   "Cmb_BuildingType_ID"
      Tab(2).Control(21)=   "Txt_RentYear_Total1"
      Tab(2).ControlCount=   22
      Begin VB.TextBox Txt_Building_Name 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Left            =   2550
         Locked          =   -1  'True
         TabIndex        =   235
         Top             =   1650
         Width           =   8955
      End
      Begin VB.TextBox Txt_RentYear_Total1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   -71700
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   18
         Text            =   "0.00"
         Top             =   555
         Width           =   1290
      End
      Begin VB.ComboBox Cmb_BuildingType_ID 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         ItemData        =   "Frm_Building.frx":29046
         Left            =   -69390
         List            =   "Frm_Building.frx":29048
         TabIndex        =   148
         Top             =   540
         Visible         =   0   'False
         Width           =   2505
      End
      Begin VB.ComboBox combo1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Building.frx":2904A
         Left            =   -66270
         List            =   "Frm_Building.frx":2905D
         Locked          =   -1  'True
         TabIndex        =   147
         Top             =   945
         Width           =   4035
      End
      Begin VB.CheckBox ChkSystem 
         Enabled         =   0   'False
         Height          =   195
         Left            =   -74865
         TabIndex        =   139
         Top             =   1020
         Width           =   210
      End
      Begin VB.CheckBox Chk_Person 
         Enabled         =   0   'False
         Height          =   195
         Left            =   -74865
         TabIndex        =   136
         Top             =   615
         Width           =   210
      End
      Begin VB.ComboBox Cmb_BuildingType 
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Building.frx":29106
         Left            =   -71730
         List            =   "Frm_Building.frx":29108
         Locked          =   -1  'True
         TabIndex        =   94
         Top             =   945
         Width           =   4635
      End
      Begin TabDlg.SSTab SSTab2 
         Height          =   2505
         Left            =   -74910
         TabIndex        =   93
         Top             =   1440
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   4419
         _Version        =   393216
         TabOrientation  =   1
         Style           =   1
         Tabs            =   5
         Tab             =   1
         TabsPerRow      =   5
         TabHeight       =   520
         TabCaption(0)   =   "�����������͹"
         TabPicture(0)   =   "Frm_Building.frx":2910A
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Image1(4)"
         Tab(0).Control(1)=   "Label2(62)"
         Tab(0).Control(2)=   "Label2(63)"
         Tab(0).Control(3)=   "Label2(64)"
         Tab(0).Control(4)=   "Label2(65)"
         Tab(0).Control(5)=   "Label2(66)"
         Tab(0).Control(6)=   "Label2(67)"
         Tab(0).Control(7)=   "Label2(68)"
         Tab(0).Control(8)=   "Label2(69)"
         Tab(0).Control(9)=   "Label2(70)"
         Tab(0).Control(10)=   "Label2(71)"
         Tab(0).Control(11)=   "Label2(72)"
         Tab(0).Control(12)=   "Label2(73)"
         Tab(0).Control(13)=   "Label2(74)"
         Tab(0).Control(14)=   "Label2(75)"
         Tab(0).Control(15)=   "Shape1(37)"
         Tab(0).Control(16)=   "Shape1(38)"
         Tab(0).Control(17)=   "Shape1(39)"
         Tab(0).Control(18)=   "Shape1(40)"
         Tab(0).Control(19)=   "Shape1(41)"
         Tab(0).Control(20)=   "Shape1(42)"
         Tab(0).Control(21)=   "Shape1(43)"
         Tab(0).Control(22)=   "Shape1(44)"
         Tab(0).Control(23)=   "Shape1(45)"
         Tab(0).Control(24)=   "Shape1(46)"
         Tab(0).Control(25)=   "Shape1(47)"
         Tab(0).Control(26)=   "Shape1(48)"
         Tab(0).Control(27)=   "Shape1(49)"
         Tab(0).Control(28)=   "LB_RentYear_Total2"
         Tab(0).Control(29)=   "Label2(98)"
         Tab(0).Control(30)=   "Lb_RentSumTax"
         Tab(0).Control(31)=   "Shape1(79)"
         Tab(0).Control(32)=   "Shape1(80)"
         Tab(0).Control(33)=   "Chk_Same"
         Tab(0).Control(34)=   "Txt_RentSame"
         Tab(0).Control(35)=   "Txt_Rent_Month(1)"
         Tab(0).Control(36)=   "Txt_Rent_Month(5)"
         Tab(0).Control(37)=   "Txt_Rent_Month(9)"
         Tab(0).Control(38)=   "Txt_Rent_Month(2)"
         Tab(0).Control(39)=   "Txt_Rent_Month(6)"
         Tab(0).Control(40)=   "Txt_Rent_Month(10)"
         Tab(0).Control(41)=   "Txt_Rent_Month(3)"
         Tab(0).Control(42)=   "Txt_Rent_Month(7)"
         Tab(0).Control(43)=   "Txt_Rent_Month(11)"
         Tab(0).Control(44)=   "Txt_Rent_Month(4)"
         Tab(0).Control(45)=   "Txt_Rent_Month(8)"
         Tab(0).Control(46)=   "Txt_Rent_Month(12)"
         Tab(0).ControlCount=   47
         TabCaption(1)   =   "��������ª����Թ����ç���͹"
         TabPicture(1)   =   "Frm_Building.frx":29126
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "Image1(5)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "Label2(76)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "Label2(77)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "Label2(78)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "Label2(79)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "Label2(80)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "Shape1(52)"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).Control(7)=   "Shape1(53)"
         Tab(1).Control(7).Enabled=   0   'False
         Tab(1).Control(8)=   "Label2(81)"
         Tab(1).Control(8).Enabled=   0   'False
         Tab(1).Control(9)=   "Label2(82)"
         Tab(1).Control(9).Enabled=   0   'False
         Tab(1).Control(10)=   "Label2(83)"
         Tab(1).Control(10).Enabled=   0   'False
         Tab(1).Control(11)=   "Shape1(54)"
         Tab(1).Control(11).Enabled=   0   'False
         Tab(1).Control(12)=   "Label2(84)"
         Tab(1).Control(12).Enabled=   0   'False
         Tab(1).Control(13)=   "Label2(85)"
         Tab(1).Control(13).Enabled=   0   'False
         Tab(1).Control(14)=   "LB_TYPE_A_RATE2"
         Tab(1).Control(14).Enabled=   0   'False
         Tab(1).Control(15)=   "Label2(99)"
         Tab(1).Control(15).Enabled=   0   'False
         Tab(1).Control(16)=   "Label2(103)"
         Tab(1).Control(16).Enabled=   0   'False
         Tab(1).Control(17)=   "Shape1(61)"
         Tab(1).Control(17).Enabled=   0   'False
         Tab(1).Control(18)=   "Shape1(62)"
         Tab(1).Control(18).Enabled=   0   'False
         Tab(1).Control(19)=   "Shape1(63)"
         Tab(1).Control(19).Enabled=   0   'False
         Tab(1).Control(20)=   "Shape1(64)"
         Tab(1).Control(20).Enabled=   0   'False
         Tab(1).Control(21)=   "Shape1(65)"
         Tab(1).Control(21).Enabled=   0   'False
         Tab(1).Control(22)=   "Shape1(66)"
         Tab(1).Control(22).Enabled=   0   'False
         Tab(1).Control(23)=   "Shape1(67)"
         Tab(1).Control(23).Enabled=   0   'False
         Tab(1).Control(24)=   "Shape1(78)"
         Tab(1).Control(24).Enabled=   0   'False
         Tab(1).Control(25)=   "LB_TYPE_A_RATE1"
         Tab(1).Control(25).Enabled=   0   'False
         Tab(1).Control(26)=   "LB_TYPE_A_RATE1_SUM"
         Tab(1).Control(26).Enabled=   0   'False
         Tab(1).Control(27)=   "LB_TYPE_A_RATE2_SUM"
         Tab(1).Control(27).Enabled=   0   'False
         Tab(1).Control(28)=   "LB_TYPE_A_RATE3_SUM"
         Tab(1).Control(28).Enabled=   0   'False
         Tab(1).Control(29)=   "LB_RentYear_Total3"
         Tab(1).Control(29).Enabled=   0   'False
         Tab(1).Control(30)=   "Lb_UseSumTax1"
         Tab(1).Control(30).Enabled=   0   'False
         Tab(1).Control(31)=   "LB_TYPE_A_RATE3"
         Tab(1).Control(31).Enabled=   0   'False
         Tab(1).Control(32)=   "LB_TYPE_A_AREA"
         Tab(1).Control(32).Enabled=   0   'False
         Tab(1).Control(33)=   "Label2(113)"
         Tab(1).Control(33).Enabled=   0   'False
         Tab(1).Control(34)=   "LB_TYPE_A_RATE4"
         Tab(1).Control(34).Enabled=   0   'False
         Tab(1).Control(35)=   "Label2(114)"
         Tab(1).Control(35).Enabled=   0   'False
         Tab(1).Control(36)=   "Shape1(81)"
         Tab(1).Control(36).Enabled=   0   'False
         Tab(1).Control(37)=   "Shape1(82)"
         Tab(1).Control(37).Enabled=   0   'False
         Tab(1).Control(38)=   "Label2(115)"
         Tab(1).Control(38).Enabled=   0   'False
         Tab(1).Control(39)=   "Chk_Method1"
         Tab(1).Control(39).Enabled=   0   'False
         Tab(1).Control(40)=   "Txt_Building_OtherArea"
         Tab(1).Control(40).Enabled=   0   'False
         Tab(1).Control(41)=   "Txt_Building_NearArea"
         Tab(1).Control(41).Enabled=   0   'False
         Tab(1).Control(42)=   "Txt_Building_Area"
         Tab(1).Control(42).Enabled=   0   'False
         Tab(1).Control(43)=   "TXT_Percent"
         Tab(1).Control(43).Enabled=   0   'False
         Tab(1).ControlCount=   44
         TabCaption(2)   =   "�ѵ�Ҥ����Ҥ�������Ҩ���"
         TabPicture(2)   =   "Frm_Building.frx":29142
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Image1(6)"
         Tab(2).Control(1)=   "Label2(100)"
         Tab(2).Control(2)=   "Label2(86)"
         Tab(2).Control(3)=   "Label2(87)"
         Tab(2).Control(4)=   "Label2(88)"
         Tab(2).Control(5)=   "Label2(89)"
         Tab(2).Control(6)=   "Shape1(57)"
         Tab(2).Control(7)=   "Label2(104)"
         Tab(2).Control(8)=   "Shape1(68)"
         Tab(2).Control(9)=   "Shape1(69)"
         Tab(2).Control(10)=   "Shape1(70)"
         Tab(2).Control(11)=   "Shape1(50)"
         Tab(2).Control(12)=   "Lb_UseSumTax2"
         Tab(2).Control(13)=   "LB_RentYear_Total4"
         Tab(2).Control(14)=   "LB_TYPE_B_RATE1"
         Tab(2).Control(15)=   "Txt_BUILDING_TOTAL_ROOM_TYPEB"
         Tab(2).Control(16)=   "Chk_Method2"
         Tab(2).Control(17)=   "Txt_BUILDING_QTY_B_MONTH"
         Tab(2).ControlCount=   18
         TabCaption(3)   =   "�ѵ�Ҥ����Ҩҡ�ѡɳ���л�����"
         TabPicture(3)   =   "Frm_Building.frx":2915E
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Image1(7)"
         Tab(3).Control(1)=   "Label2(90)"
         Tab(3).Control(2)=   "Label2(91)"
         Tab(3).Control(3)=   "Label2(92)"
         Tab(3).Control(4)=   "Shape1(58)"
         Tab(3).Control(5)=   "Label2(93)"
         Tab(3).Control(6)=   "Label2(101)"
         Tab(3).Control(7)=   "Shape1(51)"
         Tab(3).Control(8)=   "Label2(105)"
         Tab(3).Control(9)=   "Shape1(71)"
         Tab(3).Control(10)=   "Shape1(72)"
         Tab(3).Control(11)=   "Shape1(73)"
         Tab(3).Control(12)=   "Lb_UseSumTax3"
         Tab(3).Control(13)=   "LB_RentYear_Total5"
         Tab(3).Control(14)=   "LB_TYPE_C_RATE1"
         Tab(3).Control(15)=   "Txt_BUILDING_SUMAREA"
         Tab(3).Control(16)=   "Txt_BUILDING_QTY_C_MONTH"
         Tab(3).Control(17)=   "Chk_Method3"
         Tab(3).ControlCount=   18
         TabCaption(4)   =   "�ѵ�Ҩҡ��ͧ������;�鹷����ҵ���ѹ"
         TabPicture(4)   =   "Frm_Building.frx":2917A
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "Image1(8)"
         Tab(4).Control(1)=   "Label2(94)"
         Tab(4).Control(2)=   "Label2(95)"
         Tab(4).Control(3)=   "Label2(96)"
         Tab(4).Control(4)=   "Label2(97)"
         Tab(4).Control(5)=   "Shape1(59)"
         Tab(4).Control(6)=   "Shape1(60)"
         Tab(4).Control(7)=   "Label2(102)"
         Tab(4).Control(8)=   "Label2(106)"
         Tab(4).Control(9)=   "Shape1(75)"
         Tab(4).Control(10)=   "Shape1(76)"
         Tab(4).Control(11)=   "Shape1(77)"
         Tab(4).Control(12)=   "Lb_UseSumTax4"
         Tab(4).Control(13)=   "LB_RentYear_Total6"
         Tab(4).Control(14)=   "LB_TYPE_D_RATE1"
         Tab(4).Control(15)=   "Txt_BUILDING_TOTAL_ROOM_TYPED"
         Tab(4).Control(16)=   "Txt_BUILDING_QTY_D_DAY"
         Tab(4).Control(17)=   "Chk_Method4"
         Tab(4).ControlCount=   18
         Begin VB.TextBox TXT_Percent 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   7155
            TabIndex        =   240
            Text            =   "0"
            Top             =   150
            Width           =   525
         End
         Begin VB.TextBox Txt_Building_Area 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   1515
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   31
            Text            =   "0"
            Top             =   585
            Width           =   1515
         End
         Begin VB.TextBox Txt_Building_NearArea 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   1515
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   32
            Text            =   "0"
            Top             =   930
            Width           =   1515
         End
         Begin VB.TextBox Txt_Building_OtherArea 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   1515
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   33
            Text            =   "0"
            Top             =   1275
            Width           =   1515
         End
         Begin VB.TextBox Txt_BUILDING_QTY_B_MONTH 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -64710
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   227
            Text            =   "0"
            Top             =   975
            Width           =   1545
         End
         Begin VB.CheckBox Chk_Method4 
            Enabled         =   0   'False
            Height          =   195
            Left            =   -74790
            TabIndex        =   218
            Top             =   120
            Width           =   195
         End
         Begin VB.CheckBox Chk_Method3 
            Enabled         =   0   'False
            Height          =   195
            Left            =   -74790
            TabIndex        =   216
            Top             =   120
            Width           =   195
         End
         Begin VB.CheckBox Chk_Method2 
            Enabled         =   0   'False
            Height          =   195
            Left            =   -74790
            TabIndex        =   214
            Top             =   120
            Width           =   195
         End
         Begin VB.CheckBox Chk_Method1 
            Enabled         =   0   'False
            Height          =   195
            Left            =   210
            TabIndex        =   212
            Top             =   120
            Width           =   195
         End
         Begin VB.TextBox Txt_BUILDING_QTY_D_DAY 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -64710
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   38
            Text            =   "0"
            Top             =   975
            Width           =   1515
         End
         Begin VB.TextBox Txt_BUILDING_QTY_C_MONTH 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -64710
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   36
            Text            =   "0"
            Top             =   975
            Width           =   1515
         End
         Begin VB.TextBox Txt_BUILDING_TOTAL_ROOM_TYPED 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -73665
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   37
            Text            =   "0"
            Top             =   975
            Width           =   1515
         End
         Begin VB.TextBox Txt_BUILDING_SUMAREA 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -73665
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   35
            Text            =   "0"
            Top             =   975
            Width           =   1515
         End
         Begin VB.TextBox Txt_BUILDING_TOTAL_ROOM_TYPEB 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Left            =   -73665
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   34
            Text            =   "0"
            Top             =   975
            Width           =   1515
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   12
            Left            =   -65025
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   30
            Text            =   "0.00"
            Top             =   1530
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   8
            Left            =   -65025
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   26
            Text            =   "0.00"
            Top             =   1080
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   4
            Left            =   -65025
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   22
            Text            =   "0.00"
            Top             =   585
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   11
            Left            =   -67815
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   29
            Text            =   "0.00"
            Top             =   1575
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   7
            Left            =   -67815
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   25
            Text            =   "0.00"
            Top             =   1080
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   3
            Left            =   -67815
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   21
            Text            =   "0.00"
            Top             =   585
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   10
            Left            =   -70740
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   28
            Text            =   "0.00"
            Top             =   1575
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   6
            Left            =   -70740
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   24
            Text            =   "0.00"
            Top             =   1125
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   2
            Left            =   -70740
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   20
            Text            =   "0.00"
            Top             =   585
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   9
            Left            =   -73620
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   27
            Text            =   "0.00"
            Top             =   1575
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   5
            Left            =   -73620
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   23
            Text            =   "0.00"
            Top             =   1080
            Width           =   1020
         End
         Begin VB.TextBox Txt_Rent_Month 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Index           =   1
            Left            =   -73620
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   19
            Text            =   "0.00"
            Top             =   585
            Width           =   1020
         End
         Begin VB.TextBox Txt_RentSame 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00EBEBE7&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   270
            Left            =   -64260
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   164
            Text            =   "0.00"
            Top             =   135
            Width           =   1515
         End
         Begin VB.CheckBox Chk_Same 
            Enabled         =   0   'False
            Height          =   195
            Left            =   -66135
            TabIndex        =   163
            Top             =   150
            Width           =   210
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѵ�ҤԴ�Թ����                  %"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   115
            Left            =   5940
            TabIndex        =   241
            Top             =   240
            Width           =   2070
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   82
            Left            =   5850
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   81
            Left            =   5850
            Top             =   1890
            Width           =   1815
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���������»� ��鹵��                                                     � /��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   114
            Left            =   4185
            TabIndex        =   239
            Top             =   1605
            Width           =   3990
         End
         Begin VB.Label LB_TYPE_A_RATE4 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   5880
            TabIndex        =   238
            Tag             =   "0.00"
            Top             =   1590
            Width           =   1755
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��Ҵ��鹷�� ��鹵��                                                       ��.�."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   113
            Left            =   4185
            TabIndex        =   237
            Top             =   1920
            Width           =   4035
         End
         Begin VB.Label LB_TYPE_A_AREA 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   5880
            TabIndex        =   236
            Tag             =   "0.00"
            Top             =   1890
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_A_RATE3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   5880
            TabIndex        =   189
            Top             =   1245
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_B_RATE1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   -69945
            TabIndex        =   194
            Top             =   960
            Width           =   1725
         End
         Begin VB.Label LB_TYPE_C_RATE1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   -69330
            TabIndex        =   197
            Top             =   990
            Width           =   1515
         End
         Begin VB.Label LB_TYPE_D_RATE1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   -69375
            TabIndex        =   199
            Top             =   990
            Width           =   1560
         End
         Begin VB.Label LB_RentYear_Total6 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   200
            Top             =   1815
            Width           =   1545
         End
         Begin VB.Label Lb_UseSumTax4 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   209
            Top             =   1485
            Width           =   1545
         End
         Begin VB.Label LB_RentYear_Total5 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   198
            Top             =   1815
            Width           =   1545
         End
         Begin VB.Label Lb_UseSumTax3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   208
            Top             =   1485
            Width           =   1545
         End
         Begin VB.Label LB_RentYear_Total4 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   195
            Top             =   1815
            Width           =   1575
         End
         Begin VB.Label Lb_UseSumTax2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64710
            TabIndex        =   205
            Top             =   1485
            Width           =   1575
         End
         Begin VB.Label Lb_UseSumTax1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   10380
            TabIndex        =   204
            Top             =   1920
            Width           =   1755
         End
         Begin VB.Label LB_RentYear_Total3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   10380
            TabIndex        =   193
            Top             =   1590
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_A_RATE3_SUM 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   10380
            TabIndex        =   192
            Top             =   1275
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_A_RATE2_SUM 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   10380
            TabIndex        =   191
            Top             =   930
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_A_RATE1_SUM 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   10380
            TabIndex        =   190
            Top             =   585
            Width           =   1755
         End
         Begin VB.Label LB_TYPE_A_RATE1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   5880
            TabIndex        =   187
            Top             =   615
            Width           =   1755
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   80
            Left            =   -64260
            Top             =   2190
            Width           =   1545
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   79
            Left            =   -64260
            Top             =   1860
            Width           =   1545
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   78
            Left            =   10350
            Top             =   1890
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   77
            Left            =   -69390
            Top             =   960
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   76
            Left            =   -64740
            Top             =   1800
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   75
            Left            =   -64740
            Top             =   1470
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   73
            Left            =   -64740
            Top             =   1800
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   72
            Left            =   -64740
            Top             =   1470
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   71
            Left            =   -69360
            Top             =   960
            Width           =   1575
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   50
            Left            =   -64740
            Top             =   960
            Width           =   1605
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   70
            Left            =   -64740
            Top             =   1800
            Width           =   1635
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   69
            Left            =   -64740
            Top             =   1470
            Width           =   1635
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   68
            Left            =   -69960
            Top             =   930
            Width           =   1755
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   67
            Left            =   10350
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   66
            Left            =   10350
            Top             =   1230
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   65
            Left            =   10350
            Top             =   900
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   64
            Left            =   10350
            Top             =   570
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   63
            Left            =   5850
            Top             =   1230
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   62
            Left            =   5850
            Top             =   900
            Width           =   1815
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00C0C0FF&
            Height          =   315
            Index           =   61
            Left            =   5850
            Top             =   570
            Width           =   1815
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���͡�Ըչ��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   106
            Left            =   -74490
            TabIndex        =   219
            Top             =   90
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���͡�Ըչ��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   105
            Left            =   -74490
            TabIndex        =   217
            Top             =   90
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���͡�Ըչ��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   104
            Left            =   -74490
            TabIndex        =   215
            Top             =   90
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���͡�Ըչ��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   103
            Left            =   510
            TabIndex        =   213
            Top             =   90
            Width           =   600
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������»�                                          �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   102
            Left            =   -65640
            TabIndex        =   210
            Top             =   1890
            Width           =   2760
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   60
            Left            =   -64740
            Top             =   945
            Width           =   1575
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   51
            Left            =   -64740
            Top             =   945
            Width           =   1575
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������»�                                          �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   101
            Left            =   -65640
            TabIndex        =   207
            Top             =   1890
            Width           =   2760
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ��Сͺ��ä��                                                �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   99
            Left            =   8370
            TabIndex        =   203
            Top             =   1935
            Width           =   4095
         End
         Begin VB.Label Lb_RentSumTax 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64245
            TabIndex        =   202
            Top             =   2190
            Width           =   1515
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ������                                         �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   98
            Left            =   -65685
            TabIndex        =   201
            Top             =   2265
            Width           =   3195
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                         �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   93
            Left            =   -65685
            TabIndex        =   196
            Top             =   1545
            Width           =   2775
         End
         Begin VB.Label LB_TYPE_A_RATE2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   5850
            TabIndex        =   188
            Top             =   930
            Width           =   1785
         End
         Begin VB.Label LB_RentYear_Total2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00EBEBE7&
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   -64245
            TabIndex        =   186
            Top             =   1890
            Width           =   1515
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   59
            Left            =   -73695
            Top             =   945
            Width           =   1575
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   58
            Left            =   -73695
            Top             =   945
            Width           =   1575
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   57
            Left            =   -73695
            Top             =   945
            Width           =   1575
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                         �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   97
            Left            =   -65685
            TabIndex        =   185
            Top             =   1575
            Width           =   2775
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ�ѹ����ͧ����                                           �ѹ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   96
            Left            =   -66405
            TabIndex        =   184
            Top             =   1005
            Width           =   3555
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�����Ҿ�鹷�� (�����»�)                                       � /��ͧ/�ѹ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00575555&
            Height          =   240
            Index           =   95
            Left            =   -70905
            TabIndex        =   183
            Top             =   1005
            Width           =   4035
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ��ͧ                                          ��ͧ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00575555&
            Height          =   240
            Index           =   94
            Left            =   -74595
            TabIndex        =   182
            Top             =   1005
            Width           =   2955
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ��͹����ͧ����                                       ��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   92
            Left            =   -66405
            TabIndex        =   181
            Top             =   1005
            Width           =   3675
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "��鹷���ç���͹ (�����»�)                                       �/��.�./��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00575555&
            Height          =   240
            Index           =   91
            Left            =   -71085
            TabIndex        =   180
            Top             =   1005
            Width           =   4410
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ��鹷��                                          ��.�."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00575555&
            Height          =   240
            Index           =   90
            Left            =   -74595
            TabIndex        =   179
            Top             =   1005
            Width           =   3090
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������»�                                          �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   89
            Left            =   -65640
            TabIndex        =   178
            Top             =   1860
            Width           =   2760
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ��͹����ͧ����                                       ��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   88
            Left            =   -66405
            TabIndex        =   177
            Top             =   1005
            Width           =   3675
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ��ͧ                                          ��ͧ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00575555&
            Height          =   240
            Index           =   87
            Left            =   -74595
            TabIndex        =   176
            Top             =   1005
            Width           =   2955
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѵ����Ң�鹵��                                         � /��ͧ/��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   86
            Left            =   -70950
            TabIndex        =   175
            Top             =   1005
            Width           =   3795
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��鹷����� �                                            ��.�."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   85
            Left            =   495
            TabIndex        =   174
            Top             =   1320
            Width           =   3060
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�����»�(��鹷����� �)                                                   � /��.�./��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   84
            Left            =   4185
            TabIndex        =   173
            Top             =   1320
            Width           =   4710
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   54
            Left            =   1485
            Top             =   1245
            Width           =   1575
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                                 �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   83
            Left            =   9360
            TabIndex        =   172
            Top             =   1320
            Width           =   3135
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                                 �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   82
            Left            =   9360
            TabIndex        =   171
            Top             =   1005
            Width           =   3135
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                                 �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   81
            Left            =   9360
            TabIndex        =   170
            Top             =   690
            Width           =   3135
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   53
            Left            =   1485
            Top             =   900
            Width           =   1575
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   52
            Left            =   1485
            Top             =   570
            Width           =   1575
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�����»�(��鹷�������ͧ)                                               � /��.�./��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   80
            Left            =   4185
            TabIndex        =   169
            Top             =   1005
            Width           =   4710
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�����»�(��鹷���Ҥ��)                                                 � /��.�./��͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   79
            Left            =   4185
            TabIndex        =   168
            Top             =   690
            Width           =   4710
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������»�                                                 �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   78
            Left            =   9405
            TabIndex        =   167
            Top             =   1650
            Width           =   3075
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��鹷�������ͧ                                        ��.�."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   77
            Left            =   495
            TabIndex        =   166
            Top             =   975
            Width           =   3060
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��鹷���Ҥ��                                          ��.�."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   76
            Left            =   510
            TabIndex        =   165
            Top             =   660
            Width           =   3060
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   49
            Left            =   -65055
            Top             =   1500
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   48
            Left            =   -65055
            Top             =   1050
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   47
            Left            =   -65055
            Top             =   555
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   46
            Left            =   -67845
            Top             =   1545
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   45
            Left            =   -67845
            Top             =   1050
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   44
            Left            =   -67845
            Top             =   555
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   43
            Left            =   -70770
            Top             =   1545
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   42
            Left            =   -70770
            Top             =   1095
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   41
            Left            =   -70770
            Top             =   555
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   40
            Left            =   -73650
            Top             =   1545
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   39
            Left            =   -73650
            Top             =   1050
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   38
            Left            =   -73650
            Top             =   555
            Width           =   1080
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H008B8B8B&
            BorderColor     =   &H00D6BD9C&
            Height          =   315
            Index           =   37
            Left            =   -64305
            Top             =   105
            Width           =   1605
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������ҡѹ�ء��͹                                        �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   75
            Left            =   -65820
            TabIndex        =   162
            Top             =   150
            Width           =   3300
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��������»�                                         �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   74
            Left            =   -65235
            TabIndex        =   161
            Top             =   1950
            Width           =   2715
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "����Ҿѹ��                               �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   73
            Left            =   -71715
            TabIndex        =   160
            Top             =   600
            Width           =   2190
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�չҤ�                                 �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   72
            Left            =   -68700
            TabIndex        =   159
            Top             =   600
            Width           =   2085
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "����¹                               �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   71
            Left            =   -65820
            TabIndex        =   158
            Top             =   600
            Width           =   2025
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "����Ҥ�                               �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   70
            Left            =   -74595
            TabIndex        =   157
            Top             =   1095
            Width           =   2205
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�Զع�¹                                  �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   69
            Left            =   -71715
            TabIndex        =   156
            Top             =   1095
            Width           =   2205
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�á�Ҥ�                              �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   68
            Left            =   -68700
            TabIndex        =   155
            Top             =   1095
            Width           =   2115
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ԧ�Ҥ�                             �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   67
            Left            =   -65820
            TabIndex        =   154
            Top             =   1095
            Width           =   2010
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ��¹                                  �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   66
            Left            =   -74595
            TabIndex        =   153
            Top             =   1590
            Width           =   2190
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���Ҥ�                                    �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   65
            Left            =   -71715
            TabIndex        =   152
            Top             =   1590
            Width           =   2205
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "��Ȩԡ�¹                            �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   64
            Left            =   -68700
            TabIndex        =   151
            Top             =   1590
            Width           =   2130
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ�Ҥ�                              �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   63
            Left            =   -65820
            TabIndex        =   150
            Top             =   1590
            Width           =   2025
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���Ҥ�                                  �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   62
            Left            =   -74595
            TabIndex        =   149
            Top             =   600
            Width           =   2205
         End
         Begin VB.Image Image1 
            Height          =   2505
            Index           =   4
            Left            =   -62625
            Top             =   990
            Width           =   12675
         End
         Begin VB.Image Image1 
            Height          =   3915
            Index           =   5
            Left            =   12285
            Top             =   90
            Width           =   12705
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H0099AA88&
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ����Թ                                         �"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   100
            Left            =   -65685
            TabIndex        =   206
            Top             =   1545
            Width           =   2775
         End
         Begin VB.Image Image1 
            Height          =   3915
            Index           =   6
            Left            =   -62670
            Top             =   135
            Width           =   12705
         End
         Begin VB.Image Image1 
            Height          =   3915
            Index           =   7
            Left            =   -62625
            Top             =   135
            Width           =   12705
         End
         Begin VB.Image Image1 
            Height          =   3915
            Index           =   8
            Left            =   -62625
            Top             =   90
            Width           =   12705
         End
      End
      Begin VB.TextBox Txt_BUILDING_STATE_DETAILS 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Left            =   2550
         Locked          =   -1  'True
         TabIndex        =   17
         Text            =   "-"
         Top             =   2640
         Width           =   8955
      End
      Begin VB.TextBox Txt_BUILDING_STATE_REMARK 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Left            =   2550
         Locked          =   -1  'True
         TabIndex        =   16
         Text            =   "-"
         Top             =   3570
         Width           =   8955
      End
      Begin VB.TextBox Txt_BUILDING_STATE_YEAR 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Left            =   10950
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   15
         Text            =   "0"
         Top             =   1080
         Width           =   555
      End
      Begin VB.TextBox Txt_BUILDING_STATE_OTHER_PRICE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Left            =   5190
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   14
         Text            =   "0.00"
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox Txt_BUILDING_STATE_PRICE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Left            =   2550
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   13
         Text            =   "0.00"
         Top             =   1080
         Width           =   1005
      End
      Begin VB.CheckBox Chk_BUILDING_STATE3 
         Enabled         =   0   'False
         Height          =   195
         Left            =   570
         TabIndex        =   80
         Top             =   2130
         Width           =   210
      End
      Begin VB.CheckBox Chk_BUILDING_STATE1 
         Enabled         =   0   'False
         Height          =   195
         Left            =   570
         TabIndex        =   78
         Top             =   3180
         Width           =   210
      End
      Begin VB.CheckBox Chk_BUILDING_STATE2 
         Enabled         =   0   'False
         Height          =   195
         Left            =   570
         TabIndex        =   76
         Top             =   780
         Width           =   210
      End
      Begin MSComCtl2.DTPicker DTP_BUILDING_STATE_DATE 
         Height          =   330
         Left            =   7980
         TabIndex        =   119
         Top             =   1050
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   582
         _Version        =   393216
         MousePointer    =   99
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "Frm_Building.frx":29196
         CalendarBackColor=   16777215
         CalendarForeColor=   0
         CalendarTitleBackColor=   11236681
         CalendarTitleForeColor=   16777215
         CalendarTrailingForeColor=   16729907
         Format          =   76349441
         CurrentDate     =   37987
         MaxDate         =   94234
         MinDate         =   36526
      End
      Begin MSComctlLib.ListView LstV_OwnerShip 
         Height          =   1335
         Left            =   -74880
         TabIndex        =   120
         Top             =   2820
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   2355
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "��������Ңͧ"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "������Ңͧ�����Է���"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "�������"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Email"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "��˹��Է��"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "�Ѻ������"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LstV_OwnerUse 
         Height          =   1335
         Left            =   -68610
         TabIndex        =   131
         Top             =   2820
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   2355
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "��������Ңͧ"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "����ͺ��ͧ"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "�������"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Email"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����Ҥ��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   112
         Left            =   1680
         TabIndex        =   234
         Top             =   1710
         Width           =   630
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   14
         Left            =   2520
         Top             =   1620
         Width           =   9015
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00C0C0FF&
         Height          =   315
         Index           =   74
         Left            =   -64530
         Top             =   540
         Width           =   1755
      End
      Begin VB.Label Txt_RentYear_Total_All 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00EBEBE7&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   -66900
         TabIndex        =   211
         Top             =   540
         Visible         =   0   'False
         Width           =   1020
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00FFFFFF&
         Height          =   2565
         Index           =   56
         Left            =   -74925
         Top             =   1425
         Width           =   12705
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00C0C0C0&
         Height          =   2625
         Index           =   55
         Left            =   -74940
         Top             =   1395
         Width           =   12765
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ըջ����Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   61
         Left            =   -67035
         TabIndex        =   146
         Top             =   990
         Width           =   720
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   60
         Left            =   -72570
         TabIndex        =   145
         Top             =   990
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   59
         Left            =   -62625
         TabIndex        =   144
         Top             =   540
         Width           =   345
      End
      Begin VB.Label Lb_GrandTotal_Tax 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00EBEBE7&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   -64515
         TabIndex        =   143
         Top             =   570
         Width           =   1710
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "���շ������Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   58
         Left            =   -65820
         TabIndex        =   142
         Top             =   585
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   57
         Left            =   -70275
         TabIndex        =   141
         Top             =   585
         Width           =   300
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   36
         Left            =   -71715
         Top             =   540
         Width           =   1335
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "�����Թ���к�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   56
         Left            =   -74610
         TabIndex        =   140
         Top             =   990
         Width           =   1395
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "��������»�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   55
         Left            =   -72570
         TabIndex        =   138
         Top             =   585
         Width           =   765
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "�����Թ����Ҿ�ѡ�ҹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   54
         Left            =   -74610
         TabIndex        =   137
         Top             =   585
         Width           =   1875
      End
      Begin VB.Label Lb_User_Real_ID 
         Alignment       =   2  'Center
         BackColor       =   &H0099AA88&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -66690
         TabIndex        =   130
         Top             =   900
         Width           =   1725
      End
      Begin VB.Label Lb_User_Email 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66630
         TabIndex        =   129
         Top             =   2460
         Width           =   4305
      End
      Begin VB.Label Lb_User_Address 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66630
         TabIndex        =   128
         Top             =   2100
         Width           =   4305
      End
      Begin VB.Label Lb_User_Name 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66630
         TabIndex        =   127
         Top             =   1740
         Width           =   4305
      End
      Begin VB.Label Lb_User_Type 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -66630
         TabIndex        =   126
         Top             =   1380
         Width           =   4305
      End
      Begin VB.Label Lb_Email 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -73200
         TabIndex        =   125
         Top             =   2460
         Width           =   4305
      End
      Begin VB.Label Lb_Owner_Address 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -73200
         TabIndex        =   124
         Top             =   2100
         Width           =   4305
      End
      Begin VB.Label Lb_Owner_Name 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -73200
         TabIndex        =   123
         Top             =   1740
         Width           =   4305
      End
      Begin VB.Label Lb_Owner_Type 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -73200
         TabIndex        =   122
         Top             =   1380
         Width           =   4305
      End
      Begin VB.Label Lb_Ownership_Real_ID 
         Alignment       =   2  'Center
         BackColor       =   &H0099AA88&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -73260
         TabIndex        =   121
         Top             =   900
         Width           =   1725
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H0099AA88&
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   29
         Left            =   840
         TabIndex        =   77
         Top             =   750
         Width           =   495
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   34
         Left            =   -66660
         Top             =   1350
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����������ͺ���ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   51
         Left            =   -68490
         TabIndex        =   107
         Top             =   1380
         Width           =   1770
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   33
         Left            =   -66660
         Top             =   2430
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   50
         Left            =   -67410
         TabIndex        =   106
         Top             =   2520
         Width           =   600
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   49
         Left            =   -67170
         TabIndex        =   105
         Top             =   2160
         Width           =   345
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   32
         Left            =   -66660
         Top             =   1710
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����ͺ���ͧ�ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   48
         Left            =   -68490
         TabIndex        =   104
         Top             =   1770
         Width           =   1815
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   31
         Left            =   -66660
         Top             =   2070
         Width           =   4365
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00756E60&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   315
         Index           =   30
         Left            =   -66630
         Top             =   960
         Width           =   1725
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ʼ���ͺ���ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   47
         Left            =   -68235
         TabIndex        =   103
         Top             =   990
         Width           =   1485
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������Ңͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   46
         Left            =   -74385
         TabIndex        =   102
         Top             =   990
         Width           =   945
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00756E60&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   315
         Index           =   29
         Left            =   -73200
         Top             =   960
         Width           =   1725
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   28
         Left            =   -73230
         Top             =   2070
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Ңͧ�ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   45
         Left            =   -74700
         TabIndex        =   101
         Top             =   1770
         Width           =   1275
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   27
         Left            =   -73230
         Top             =   1710
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   44
         Left            =   -73740
         TabIndex        =   100
         Top             =   2160
         Width           =   345
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   41
         Left            =   -73980
         TabIndex        =   99
         Top             =   2520
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   26
         Left            =   -73230
         Top             =   2430
         Width           =   4365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������Ңͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   52
         Left            =   -74670
         TabIndex        =   98
         Top             =   1380
         Width           =   1230
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   35
         Left            =   -73230
         Top             =   1350
         Width           =   4365
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00C0C0C0&
         Height          =   3705
         Index           =   25
         Left            =   60
         Top             =   450
         Width           =   12765
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00FFFFFF&
         Height          =   3645
         Index           =   24
         Left            =   90
         Top             =   480
         Width           =   12705
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00C0C0C0&
         Height          =   3705
         Index           =   23
         Left            =   -68670
         Top             =   450
         Width           =   6495
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00C0C0C0&
         Height          =   3705
         Index           =   22
         Left            =   -74940
         Top             =   450
         Width           =   6255
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00FFFFFF&
         Height          =   3645
         Index           =   21
         Left            =   -74910
         Top             =   480
         Width           =   6195
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Ңͧ�ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   43
         Left            =   -74460
         TabIndex        =   92
         Top             =   570
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����ͺ��ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   42
         Left            =   -68010
         TabIndex        =   91
         Top             =   570
         Width           =   1005
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00BAD7E6&
         BorderColor     =   &H00FFFFFF&
         Height          =   3645
         Index           =   20
         Left            =   -68640
         Top             =   480
         Width           =   6435
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   19
         Left            =   2520
         Top             =   2610
         Width           =   9015
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������´��ä��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   40
         Left            =   1125
         TabIndex        =   90
         Top             =   2700
         Width           =   1200
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   39
         Left            =   11670
         TabIndex        =   89
         Top             =   1110
         Width           =   105
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   38
         Left            =   6390
         TabIndex        =   88
         Top             =   1140
         Width           =   300
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   37
         Left            =   3720
         TabIndex        =   87
         Top             =   1140
         Width           =   300
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   18
         Left            =   2520
         Top             =   3540
         Width           =   9015
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "* �����˵�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   36
         Left            =   1410
         TabIndex        =   86
         Top             =   3630
         Width           =   915
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   17
         Left            =   10920
         Top             =   1050
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ�շ�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   35
         Left            =   9720
         TabIndex        =   85
         Top             =   1140
         Width           =   915
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ��������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   34
         Left            =   7110
         TabIndex        =   84
         Top             =   1140
         Width           =   660
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   16
         Left            =   5160
         Top             =   1050
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Թ��� �"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   33
         Left            =   4380
         TabIndex        =   83
         Top             =   1140
         Width           =   585
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   32
         Left            =   1920
         TabIndex        =   82
         Top             =   1140
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   15
         Left            =   2520
         Top             =   1050
         Width           =   1065
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Сͺ��ä��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   31
         Left            =   840
         TabIndex        =   81
         Top             =   2100
         Width           =   1170
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   30
         Left            =   870
         TabIndex        =   79
         Top             =   3150
         Width           =   480
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   315
         Index           =   2
         Left            =   -74880
         Top             =   510
         Width           =   2325
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   315
         Index           =   5
         Left            =   -68610
         Top             =   510
         Width           =   2325
      End
      Begin VB.Image Image1 
         Height          =   3915
         Index           =   2
         Left            =   -75000
         Top             =   330
         Width           =   12840
      End
      Begin VB.Image Image1 
         Height          =   3915
         Index           =   1
         Left            =   30
         Top             =   300
         Width           =   12840
      End
      Begin VB.Image Image1 
         Height          =   3915
         Index           =   3
         Left            =   -75000
         Top             =   315
         Width           =   12840
      End
   End
   Begin VB.TextBox Txt_BUILDING_HOME_NO 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4170
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   1830
      Width           =   1425
   End
   Begin VB.TextBox Txt_BUILDING_WIDTH 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   6960
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   10
      Text            =   "0"
      Top             =   2580
      Width           =   765
   End
   Begin VB.TextBox Txt_BUILDING_HEIGHT 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   6960
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   11
      Text            =   "0"
      Top             =   3000
      Width           =   765
   End
   Begin VB.TextBox Txt_BUILDING_FLOOR 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   6960
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   9
      Text            =   "0"
      Top             =   2160
      Width           =   765
   End
   Begin VB.TextBox Txt_BUILDING_ROOM 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   6960
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   8
      Text            =   "0"
      Top             =   1740
      Width           =   765
   End
   Begin VB.TextBox Txt_BUILDING_HOME_ID 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4170
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   2220
      Width           =   1425
   End
   Begin VB.TextBox Txt_Building_ID 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2430
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   2
      Top             =   1380
      Width           =   795
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1290
      TabIndex        =   1
      Top             =   1350
      Width           =   1035
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Building.frx":294B0
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_Building.frx":29D19
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Building.frx":2C511
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_Building.frx":2CD3F
      Style           =   1  'Graphical
      TabIndex        =   44
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Building.frx":2F5C9
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_Building.frx":2FEB8
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_Building.frx":32A13
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_Building.frx":332C5
      Style           =   1  'Graphical
      TabIndex        =   42
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Building.frx":35D00
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_Building.frx":36563
      Style           =   1  'Graphical
      TabIndex        =   41
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Building.frx":38F88
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_Building.frx":3981F
      Style           =   1  'Graphical
      TabIndex        =   40
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Building.frx":3C104
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_Building.frx":3C96B
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin MSComctlLib.ListView LstV_Img_Name 
      Height          =   2145
      Left            =   8550
      TabIndex        =   114
      Top             =   1290
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   3784
      View            =   3
      Arrange         =   2
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�ٻ�ç���͹"
         Object.Width           =   2752
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTP_BUILDING_DATE 
      Height          =   330
      Left            =   1290
      TabIndex        =   39
      Top             =   2970
      Width           =   1965
      _ExtentX        =   3466
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_Building.frx":3F262
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   76349441
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ҹ�Ţ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   2
      Left            =   3330
      TabIndex        =   233
      Top             =   1860
      Width           =   750
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   109
      Left            =   540
      TabIndex        =   232
      Top             =   1860
      Width           =   645
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   111
      Left            =   8850
      TabIndex        =   226
      Top             =   3540
      Width           =   960
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   110
      Left            =   10290
      TabIndex        =   225
      Top             =   3540
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ê�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   108
      Left            =   6570
      TabIndex        =   222
      Top             =   3540
      Width           =   1050
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*���˵ػ�Ѻ����¹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   107
      Left            =   2610
      TabIndex        =   221
      Top             =   960
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   53
      Left            =   2730
      TabIndex        =   134
      Top             =   2670
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Ǥ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   28
      Left            =   1590
      TabIndex        =   133
      Top             =   2670
      Width           =   645
   End
   Begin VB.Label Lb_Tambon 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   9150
      TabIndex        =   113
      Top             =   4440
      Width           =   1905
   End
   Begin VB.Label Lb_Village 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   6660
      TabIndex        =   112
      Top             =   4440
      Width           =   1845
   End
   Begin VB.Label Lb_Street 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   3720
      TabIndex        =   111
      Top             =   4440
      Width           =   1755
   End
   Begin VB.Label Lb_Soi 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1320
      TabIndex        =   110
      Top             =   4440
      Width           =   1725
   End
   Begin VB.Label Lb_Land_Notic 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   8850
      TabIndex        =   109
      Top             =   3960
      Width           =   2205
   End
   Begin VB.Label Lb_Type_Land 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   4980
      TabIndex        =   108
      Top             =   3990
      Width           =   2205
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ŧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   27
      Left            =   11850
      TabIndex        =   74
      Top             =   4020
      Width           =   885
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����ŧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   26
      Left            =   11850
      TabIndex        =   72
      Top             =   4440
      Width           =   675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ����͡����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   25
      Left            =   7350
      TabIndex        =   71
      Top             =   4020
      Width           =   1365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������͡����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   24
      Left            =   3240
      TabIndex        =   70
      Top             =   4020
      Width           =   1590
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   23
      Left            =   870
      TabIndex        =   69
      Top             =   4440
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   22
      Left            =   3270
      TabIndex        =   68
      Top             =   4440
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� (�����)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   21
      Left            =   5580
      TabIndex        =   67
      Top             =   4440
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   20
      Left            =   8610
      TabIndex        =   66
      Top             =   4440
      Width           =   435
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   19
      Left            =   465
      TabIndex        =   65
      Top             =   4020
      Width           =   735
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ʶҹ������ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   18
      Left            =   480
      TabIndex        =   64
      Top             =   3540
      Width           =   1485
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   11
      Left            =   4140
      Top             =   3000
      Width           =   1485
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   17
      Left            =   3675
      TabIndex        =   63
      Top             =   3060
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ����͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   525
      TabIndex        =   62
      Top             =   3060
      Width           =   675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҿ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   735
      TabIndex        =   61
      Top             =   2670
      Width           =   465
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ŷ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   3330
      TabIndex        =   60
      Top             =   2670
      Width           =   750
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   9
      Left            =   4140
      Top             =   1800
      Width           =   1485
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2175
      Index           =   8
      Left            =   30
      Top             =   1260
      Width           =   5715
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2115
      Index           =   7
      Left            =   60
      Top             =   1290
      Width           =   5655
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   14
      Left            =   7920
      TabIndex        =   58
      Top             =   3060
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   7920
      TabIndex        =   57
      Top             =   2670
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   12
      Left            =   7920
      TabIndex        =   56
      Top             =   2250
      Width           =   240
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   11
      Left            =   7920
      TabIndex        =   55
      Top             =   1830
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   10
      Left            =   6495
      TabIndex        =   54
      Top             =   3060
      Width           =   300
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ҧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   6390
      TabIndex        =   53
      Top             =   2670
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   6015
      TabIndex        =   52
      Top             =   2280
      Width           =   780
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   6
      Left            =   6930
      Top             =   2970
      Width           =   825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   5
      Left            =   6930
      Top             =   2550
      Width           =   825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   2
      Left            =   6930
      Top             =   2130
      Width           =   825
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ��ͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   7
      Left            =   5910
      TabIndex        =   51
      Top             =   1830
      Width           =   885
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҵ��鹷���ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   6
      Left            =   6390
      TabIndex        =   50
      Top             =   1380
      Width           =   1530
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2175
      Index           =   4
      Left            =   5790
      Top             =   1260
      Width           =   2745
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2115
      Index           =   3
      Left            =   5820
      Top             =   1290
      Width           =   2685
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѡɳ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   3
      Left            =   615
      TabIndex        =   49
      Top             =   2280
      Width           =   585
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   1
      Left            =   6930
      Top             =   1710
      Width           =   825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   4140
      Top             =   2190
      Width           =   1485
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʺ�ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   3390
      TabIndex        =   48
      Top             =   2280
      Width           =   690
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   10
      Left            =   2400
      Top             =   1350
      Width           =   855
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   150
      TabIndex        =   47
      Top             =   1500
      Width           =   1050
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������´�ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   360
      TabIndex        =   46
      Top             =   990
      Width           =   1590
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   4
      Left            =   30
      Top             =   930
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   0
      Left            =   5850
      Top             =   1350
      Width           =   2625
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   1
      Left            =   30
      Top             =   3480
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1005
      Index           =   13
      Left            =   60
      Top             =   3840
      Width           =   12795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1065
      Index           =   12
      Left            =   30
      Top             =   3810
      Width           =   12855
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   3
      Left            =   5790
      Top             =   3480
      Width           =   2715
   End
End
Attribute VB_Name = "Frm_Building"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Status As String
'Dim QueryZoneBlock As ADODB.Recordset
Dim QueryBuilding_Rate As ADODB.Recordset

Private Sub CalOfRent()
   Dim i As Byte
   Dim sum_rent As Currency
   For i = 1 To 12
        sum_rent = sum_rent + Txt_Rent_Month(i).Text
   Next i
    LB_RentYear_Total2.Caption = sum_rent
    Lb_RentSumTax.Caption = (sum_rent * 12.5) / 100
    
    Txt_RentYear_Total_All.Caption = LB_RentYear_Total2.Caption
    Lb_GrandTotal_Tax.Caption = Format$((sum_rent * 12.5) / 100, "0.00")
End Sub

Private Sub CalOfUse(TaxStep As Byte)
   Dim sum_use As Currency
Select Case TaxStep
            Case 1
                    If CSng(Txt_Building_Area.Text) > CSng(LB_TYPE_A_AREA.Caption) Then
                            LB_TYPE_A_RATE1_SUM.Caption = Format$((((CSng(Txt_Building_Area.Text) * CSng(LB_TYPE_A_RATE1.Caption)) * 12) * 12.5) / 100, "###,##0.00")
                    Else
                            LB_TYPE_A_RATE1_SUM = LB_TYPE_A_RATE4.Caption
                    End If
                    If CInt(Txt_Percent.Text) > 0 Then
                            LB_TYPE_A_RATE1_SUM = LB_TYPE_A_RATE1_SUM + CInt(Txt_Percent.Text) / 100 * CSng(LB_TYPE_A_RATE1_SUM)
                    End If
                    LB_TYPE_A_RATE2_SUM.Caption = Format$((((CSng(Txt_Building_NearArea.Text) * CSng(LB_TYPE_A_RATE2.Caption)) * 12) * 12.5) / 100, "###,##0.00")
                    LB_TYPE_A_RATE3_SUM.Caption = Format$((((CSng(Txt_Building_OtherArea.Text) * CSng(LB_TYPE_A_RATE3.Caption)) * 12) * 12.5) / 100, "###,##0.00")
                   
                   sum_use = ((CSng(LB_TYPE_A_RATE1_SUM.Caption) + CSng(LB_TYPE_A_RATE2_SUM.Caption) + CSng(LB_TYPE_A_RATE3_SUM.Caption)) * 100) / 12.5
                   LB_RentYear_Total3.Caption = Format$(sum_use, "###,##0.00")
                   Lb_UseSumTax1.Caption = Format$((sum_use * 12.5) / 100, "###,##0.00")
                   
                   Txt_RentYear_Total_All.Caption = Format$(CSng(LB_RentYear_Total2.Caption) + CSng(LB_RentYear_Total3.Caption), "###,##0.00") '�����������»�
                    Lb_GrandTotal_Tax.Caption = Format$(CSng(Lb_UseSumTax1.Caption) + CSng(Lb_RentSumTax.Caption), "###,##0.00") '���������ջ����Թ
            Case 2
                      sum_use = ((CSng(Txt_BUILDING_TOTAL_ROOM_TYPEB.Text) * CSng(LB_TYPE_B_RATE1.Caption)) * CSng(Txt_BUILDING_QTY_B_MONTH.Text))
                      LB_RentYear_Total4.Caption = sum_use
                      Lb_UseSumTax2.Caption = Format$((sum_use * 12.5) / 100, "###,##0.00")
                      
                       Txt_RentYear_Total_All.Caption = Format$(CSng(LB_RentYear_Total2.Caption) + CSng(LB_RentYear_Total4.Caption), "###,##0.00") '�����������»�
                       Lb_GrandTotal_Tax.Caption = Format$(CSng(Lb_UseSumTax2.Caption) + CSng(Lb_RentSumTax.Caption), "###,##0.00") '���������ջ����Թ
            Case 3
                      sum_use = ((CSng(Txt_BUILDING_SUMAREA.Text) * CSng(LB_TYPE_C_RATE1.Caption)) * CSng(Txt_BUILDING_QTY_C_MONTH.Text))
                      LB_RentYear_Total5.Caption = sum_use
                      Lb_UseSumTax3.Caption = Format$((sum_use * 12.5) / 100, "###,##0.00")
                      
                      Txt_RentYear_Total_All.Caption = Format$(CSng(LB_RentYear_Total2.Caption) + CSng(LB_RentYear_Total5.Caption), "###,##0.00") '�����������»�
                       Lb_GrandTotal_Tax.Caption = Format$(CSng(Lb_UseSumTax3.Caption) + CSng(Lb_RentSumTax.Caption), "###,##0.00")
            Case 4
                      sum_use = ((CSng(Txt_BUILDING_TOTAL_ROOM_TYPED.Text) * CSng(LB_TYPE_D_RATE1.Caption)) * CSng(Txt_BUILDING_QTY_D_DAY.Text))
                      LB_RentYear_Total6.Caption = sum_use
                      Lb_UseSumTax4.Caption = Format$((sum_use * 12.5) / 100, "###,##0.00")
                     
                     Txt_RentYear_Total_All.Caption = Format$(CSng(LB_RentYear_Total2.Caption) + CSng(LB_RentYear_Total6.Caption), "###,##0.00") '�����������»�
                     Lb_GrandTotal_Tax.Caption = Format$(CSng(Lb_UseSumTax4.Caption) + CSng(Lb_RentSumTax.Caption), "###,##0.00")
End Select
                    
End Sub

Private Sub ShowPicture(Path_Picture As String)
On Error GoTo HldNoPic
Dim ShrinkScale As Single
                
                ImgSize.Stretch = False
                ImgSize.Picture = LoadPicture(Path_Picture)
                originalWidth = ImgSize.Width
                originalHeight = ImgSize.Height
                sWidth = ImgSize.Width
                sHeight = ImgSize.Height
                picPicture.AutoSize = True
                picPicture.Picture = LoadPicture(Path_Picture)
                
                picPicture.Left = 0
                picPicture.Top = 0
                        
                ImgSize.Stretch = True
                ImgSize.Width = sWidth
                ImgSize.Height = sHeight
        
                sWidth = picParent.Width
                ShrinkScale = picParent.Width / ImgSize.Width
                sHeight = CSng(sHeight * ShrinkScale)
        
        If sHeight > picParent.Height Then
                ShrinkScale = picParent.Height / sHeight
                sWidth = CSng(sWidth * ShrinkScale)
                sHeight = CSng(sHeight * ShrinkScale) - 1
        End If
                ImgSize.Stretch = False
                  Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
                  Exit Sub
HldNoPic:
            picPicture.Picture = LoadPicture("")
            ImgSize.Picture = LoadPicture("")
End Sub

Private Sub SizeImage(picBox As PictureBox, sizePic As Picture, sizeWidth As Single, sizeHeight As Single)
On Error Resume Next
        picBox.Picture = LoadPicture("")
        picBox.Width = sizeWidth
        picBox.Height = sizeHeight
        picBox.AutoRedraw = True
        picBox.PaintPicture sizePic, 0, 0, sizeWidth, sizeHeight
        picBox.Picture = picBox.Image
        picBox.AutoRedraw = False
        picBox.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

Private Sub SET_REFRESH()
        GBQueryZone.Requery
        GBQueryZoneTax.Requery
        GBQueryLandData.Requery
        GBQueryBuildingData.Requery
        GBQueryBuildingType.Requery
        GBQueryBuildingRate.Requery
        GBQueryBuildingDetails.Requery
   
        Cmb_Zoneblock.Clear
        GBQueryZone.Filter = "ZONE_ID <> '' "
If GBQueryZone.RecordCount > 0 Then
            GBQueryZone.MoveFirst
    Do While Not GBQueryZone.EOF
                  If GBQueryZone.Fields("ZONE_BLOCK").Value <> Empty Then
                    Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
    Loop
End If
'         Cmb_Tamlay.Clear
'If GBQueryZoneTax.RecordCount > 0 Then
'            GBQueryZoneTax.MoveFirst
'    Do While Not GBQueryZoneTax.EOF
'                  If GBQueryZoneTax.Fields("ZONETAX_ID").Value <> Empty Then
'                       Cmb_Tamlay.AddItem GBQueryZoneTax.Fields("ZONETAX_ID").Value
'                  End If
'                    GBQueryZoneTax.MoveNext
'    Loop
'End If

       Cmb_BuildingType.Clear
       Cmb_BuildingType_ID.Clear
If GBQueryBuildingType.RecordCount > 0 Then
       GBQueryBuildingType.MoveFirst
       Do While Not GBQueryBuildingType.EOF
                If GBQueryBuildingType.Fields("BUILDING_SHAPE_ID").Value <> Empty Then
                       Cmb_BuildingType.AddItem GBQueryBuildingType.Fields("BUILDING_DETAILS").Value
                       Cmb_Building_Type.AddItem GBQueryBuildingType.Fields("BUILDING_DETAILS").Value
                       Cmb_BuildingType_ID.AddItem GBQueryBuildingType.Fields("BUILDING_SHAPE_ID").Value
                End If
                GBQueryBuildingType.MoveNext
       Loop
            Cmb_Building_Type_II_ID.Clear
            Cmb_Building_Type_II.Clear
            If GBQueryBuildingDetails.RecordCount > 0 Then
                    GBQueryBuildingDetails.MoveFirst
                Do While Not GBQueryBuildingDetails.EOF
                      Cmb_Building_Type_II_ID.AddItem GBQueryBuildingDetails.Fields("BUILDING_TYPE_ID").Value
                      Cmb_Building_Type_II.AddItem GBQueryBuildingDetails.Fields("BUILDING_TYPE_DETAILS").Value
                      GBQueryBuildingDetails.MoveNext
                Loop
            End If
End If
End Sub

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
    Btn_Search.Enabled = False:      Btn_Refresh.Enabled = False
     Btn_SelectImg.Enabled = True: Btn_FullScreen.Enabled = True
     Txt_BUILDING_ID.Locked = False:                   Txt_BUILDING_ID.BackColor = &H80000005
     Txt_BUILDING_HOME_NO.Locked = False:                  Txt_BUILDING_HOME_NO.BackColor = &H80000005
     Txt_BUILDING_PRICE.Locked = False:                           Txt_BUILDING_PRICE.BackColor = &H80000005
     Txt_BUILDING_HOME_ID.Locked = False:                     Txt_BUILDING_HOME_ID.BackColor = &H80000005
'     Txt_BUILDING_TYPE.Locked = False:                        Txt_BUILDING_TYPE.BackColor = &H80000005
     Txt_BUILDING_ROOM.Locked = False:                       Txt_BUILDING_ROOM.BackColor = &H80000005
     Txt_BUILDING_FLOOR.Locked = False:                      Txt_BUILDING_FLOOR.BackColor = &H80000005
     Txt_BUILDING_HEIGHT.Locked = False:                     Txt_BUILDING_HEIGHT.BackColor = &H80000005
     Txt_BUILDING_WIDTH.Locked = False:                        Txt_BUILDING_WIDTH.BackColor = &H80000005
     
        Txt_BUILDING_STATE_PRICE.BackColor = &H80000005
       Txt_BUILDING_STATE_OTHER_PRICE.BackColor = &H80000005
       Txt_BUILDING_STATE_YEAR.BackColor = &H80000005
       Txt_BUILDING_STATE_REMARK.Locked = False: Txt_BUILDING_STATE_REMARK.BackColor = &H80000005
       Txt_BUILDING_STATE_DETAILS.BackColor = &H80000005
       Txt_Building_Name.BackColor = &H80000005
        
        Cmb_Zoneblock.Enabled = True
        Cmb_Tamlay.Enabled = True
        Cmb_Building_Type.Enabled = True
        Cmb_Building_Type_II.Enabled = True
        
        Chk_BUILDING_FLAG0.Enabled = True
        Chk_BUILDING_FLAG1.Enabled = True
        
        Cmb_Land_ID.Enabled = True
        
        Chk_BUILDING_STATE2.Enabled = True
        Chk_BUILDING_STATE3.Enabled = True
        Chk_BUILDING_STATE1.Enabled = True
        
        CHK_BUILDING_STATUS0.Enabled = True
        CHK_BUILDING_STATUS1.Enabled = True
     
     Txt_RentYear_Total1.BackColor = &H80000005: Txt_RentYear_Total1.Locked = False
     Txt_RentSame.BackColor = &H80000005
     For i = 1 To 12
           Txt_Rent_Month(i).BackColor = &H80000005
     Next i
        
         Txt_Building_Area.Locked = False:                         Txt_Building_Area.BackColor = &H80000005
         Txt_Percent.Locked = False:                                   Txt_Percent.BackColor = &H80000005
         Txt_Building_NearArea.Locked = False:                Txt_Building_NearArea.BackColor = &H80000005
         Txt_Building_OtherArea.Locked = False:               Txt_Building_OtherArea.BackColor = &H80000005
         
         Txt_BUILDING_TOTAL_ROOM_TYPEB.Locked = False:               Txt_BUILDING_TOTAL_ROOM_TYPEB.BackColor = &H80000005
         Txt_BUILDING_QTY_B_MONTH.Locked = False:                                    Txt_BUILDING_QTY_B_MONTH.BackColor = &H80000005
         
         Txt_BUILDING_SUMAREA.Locked = False:                                      Txt_BUILDING_SUMAREA.BackColor = &H80000005
         Txt_BUILDING_QTY_C_MONTH.Locked = False:                                      Txt_BUILDING_QTY_C_MONTH.BackColor = &H80000005
         
         Txt_BUILDING_TOTAL_ROOM_TYPED.Locked = False:               Txt_BUILDING_TOTAL_ROOM_TYPED.BackColor = &H80000005
         Txt_BUILDING_QTY_D_DAY.Locked = False:                                          Txt_BUILDING_QTY_D_DAY.BackColor = &H80000005
         
         Chk_Person.Enabled = True
         ChkSystem.Enabled = True
         
            Cmb_BuildingType.Enabled = True
            combo1.Enabled = True
         
                Chk_Method1.Enabled = True
                Chk_Method2.Enabled = True
                Chk_Method3.Enabled = True
                Chk_Method4.Enabled = True
                Chk_Flag_PayTax0.Enabled = True
                Chk_Flag_PayTax1.Enabled = True
         
         If STATE = "EDIT" Then
             Cmb_Zoneblock.Enabled = False
              Txt_BUILDING_ID.Locked = True:                                  Txt_BUILDING_ID.BackColor = &HEBEBE7
            Txt_StoryRemark.Visible = True
            Label2(107).Visible = True
            Shape3(0).BackColor = &HC0C000
            Shape3(1).BackColor = &HC0C000
            Shape3(2).BackColor = &HC0C000
            Shape3(3).BackColor = &HC0C000
            Shape3(4).BackColor = &HC0C000
            Shape3(5).BackColor = &HC0C000
            If ChkSystem.Value = Checked Then
                    Txt_Percent.Enabled = True
                    Txt_Percent.BackColor = &H80000005
            End If
            cb_Building_Change_ID.Clear
            cb_Building_Change.Clear
            If GBQueryBuildingChange.RecordCount > 0 Then
                GBQueryBuildingChange.MoveFirst
                Do While Not GBQueryBuildingChange.EOF
                      cb_Building_Change_ID.AddItem GBQueryBuildingChange.Fields("BUILDING_CHANGE_ID").Value
                      cb_Building_Change.AddItem GBQueryBuildingChange.Fields("BUILDING_CHANGE_DETAILS").Value
                      GBQueryBuildingChange.MoveNext
                Loop
            End If
            cb_Building_Change.Visible = True
            cb_Building_Change.ListIndex = 0
            cb_Building_Change_ID.ListIndex = 0
         End If
         
End If
If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                        Txt_BUILDING_ID.Text = Empty
                        Txt_BUILDING_HOME_ID.Text = Empty
                        Txt_BUILDING_HOME_NO.Text = Empty
                        Txt_BUILDING_PRICE.Text = Empty
                        Txt_BUILDING_ROOM.Text = Empty
'                        Txt_BUILDING_TYPE.Text = Empty
                        Txt_BUILDING_FLOOR.Text = Empty
                        Txt_BUILDING_HEIGHT.Text = Empty
                        Txt_BUILDING_WIDTH.Text = Empty
                        Txt_BUILDING_STATE_PRICE.Text = Empty
                        Txt_BUILDING_STATE_OTHER_PRICE.Text = Empty
                        Txt_BUILDING_STATE_YEAR.Text = Empty
                        Txt_BUILDING_STATE_REMARK.Text = Empty
                        Txt_BUILDING_STATE_DETAILS.Text = Empty
                        Txt_Building_Name.Text = vbNullString
                        
                        Lb_Tambon.Caption = Empty
                        Lb_Village.Caption = Empty
                        Lb_Soi.Caption = Empty
                        Lb_Street.Caption = Empty
                        Lb_Land_Notic.Caption = Empty
                        Lb_Type_Land.Caption = Empty
                        
                          Lb_Ownership_Real_ID.Caption = Empty
                          Lb_Owner_Type.Caption = Empty
                          Lb_Owner_Name.Caption = Empty
                          Lb_Owner_Address.Caption = Empty
                          Lb_Email.Caption = Empty
                          
                          Lb_User_Real_ID.Caption = Empty
                          Lb_User_Type.Caption = Empty
                          Lb_User_Name.Caption = Empty
                          Lb_User_Address.Caption = Empty
                          Lb_User_Email.Caption = Empty
                          
                          LstV_Img_Name.ListItems.Clear
                          LstV_OwnerShip.ListItems.Clear
                          LstV_OwnerUse.ListItems.Clear
                          Cmb_Tamlay.ListIndex = -1
                          Cmb_Building_Type.ListIndex = -1
'                          Cmb_Building_Type_II.ListIndex = -1
                          DTP_BUILDING_DATE.Value = Now
                          DTP_BUILDING_STATE_DATE.Value = Now
                          picPicture.Picture = LoadPicture("")
                          picPicture.Width = 2235
                          picPicture.Height = 2085
                          picPicture.Left = 0
                          
                          Txt_RentYear_Total1.Text = Empty
                          Txt_RentYear_Total_All.Caption = Empty
                          Txt_RentSame.Text = Empty
        cb_Building_Change.Visible = False
         Txt_Building_Area.Text = Empty
         Txt_Building_NearArea.Text = Empty
         Txt_Building_OtherArea.Text = Empty
         
         Txt_BUILDING_TOTAL_ROOM_TYPEB.Text = Empty
         Txt_BUILDING_QTY_B_MONTH.Text = Empty
         
         Txt_BUILDING_SUMAREA.Text = Empty
         Txt_BUILDING_QTY_C_MONTH.Text = Empty
         
         Txt_BUILDING_TOTAL_ROOM_TYPED.Text = Empty
         Txt_BUILDING_QTY_D_DAY.Text = Empty

LB_TYPE_A_RATE1.Caption = Empty:       LB_TYPE_A_RATE1_SUM.Caption = Empty
LB_TYPE_A_RATE2.Caption = Empty:       LB_TYPE_A_RATE2_SUM.Caption = Empty
LB_TYPE_A_RATE3.Caption = Empty:       LB_TYPE_A_RATE3_SUM.Caption = Empty
LB_TYPE_A_RATE4.Caption = Empty:       LB_TYPE_A_AREA.Caption = "0"
Txt_Percent.Text = "0"
LB_RentYear_Total3.Caption = Empty

LB_TYPE_B_RATE1.Caption = Empty:         Txt_BUILDING_QTY_B_MONTH.Text = Empty
LB_RentYear_Total4.Caption = Empty

LB_TYPE_C_RATE1.Caption = Empty:      Txt_BUILDING_QTY_C_MONTH.Text = Empty
LB_RentYear_Total5.Caption = Empty


LB_TYPE_D_RATE1.Caption = Empty:    Txt_BUILDING_QTY_D_DAY.Text = Empty
LB_RentYear_Total6.Caption = Empty

Lb_RentSumTax.Caption = Empty
Lb_UseSumTax1.Caption = Empty
Lb_UseSumTax2.Caption = Empty
Lb_UseSumTax3.Caption = Empty
Lb_UseSumTax4.Caption = Empty
Label2(4).Tag = vbNullString
Label2(6).Tag = vbNullString
    If STATE <> "ADD" Then
        Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
        Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
        Btn_Search.Enabled = True:      Btn_Refresh.Enabled = True
        Btn_SelectImg.Enabled = False: Btn_FullScreen.Enabled = False
        Txt_BUILDING_ID.Locked = True:                                  Txt_BUILDING_ID.BackColor = &HEBEBE7
        Txt_BUILDING_HOME_NO.Locked = True:              Txt_BUILDING_HOME_NO.BackColor = &HEBEBE7
        Txt_BUILDING_HOME_ID.Locked = True:                 Txt_BUILDING_HOME_ID.BackColor = &HEBEBE7
        Txt_BUILDING_PRICE.Locked = True:                       Txt_BUILDING_PRICE.BackColor = &HEBEBE7
        Txt_BUILDING_ROOM.Locked = True:                       Txt_BUILDING_ROOM.BackColor = &HEBEBE7
'        Txt_BUILDING_TYPE.Locked = True:                        Txt_BUILDING_TYPE.BackColor = &HEBEBE7
        Txt_BUILDING_FLOOR.Locked = True:                     Txt_BUILDING_FLOOR.BackColor = &HEBEBE7
        Txt_BUILDING_HEIGHT.Locked = True:                    Txt_BUILDING_HEIGHT.BackColor = &HEBEBE7
        Txt_BUILDING_WIDTH.Locked = True:                       Txt_BUILDING_WIDTH.BackColor = &HEBEBE7
        Txt_BUILDING_STATE_PRICE.Locked = True:          Txt_BUILDING_STATE_PRICE.BackColor = &HEBEBE7
        Txt_BUILDING_STATE_OTHER_PRICE.Locked = True:                        Txt_BUILDING_STATE_OTHER_PRICE.BackColor = &HEBEBE7
        Txt_BUILDING_STATE_YEAR.Locked = True:                        Txt_BUILDING_STATE_YEAR.BackColor = &HEBEBE7
        Txt_BUILDING_STATE_REMARK.Locked = True:                        Txt_BUILDING_STATE_REMARK.BackColor = &HEBEBE7
        Txt_BUILDING_STATE_DETAILS.Locked = True:                        Txt_BUILDING_STATE_DETAILS.BackColor = &HEBEBE7
        Txt_Building_Name.Locked = True:                                                     Txt_Building_Name.BackColor = &HEBEBE7
        
        Cmb_Zoneblock.Enabled = False
        Cmb_Tamlay.Enabled = False
        Cmb_Building_Type.Enabled = False
        Cmb_Building_Type_II.Enabled = False
        
        Chk_BUILDING_FLAG0.Enabled = False
        Chk_BUILDING_FLAG1.Enabled = False
        
        Cmb_Land_ID.Enabled = False
        
        Chk_BUILDING_STATE2.Enabled = False: Chk_BUILDING_STATE2.Value = Unchecked
        Chk_BUILDING_STATE3.Enabled = False: Chk_BUILDING_STATE3.Value = Unchecked
        Chk_BUILDING_STATE1.Enabled = False: Chk_BUILDING_STATE1.Value = Unchecked
        
        CHK_BUILDING_STATUS0.Enabled = False
        CHK_BUILDING_STATUS1.Enabled = False
        
        Chk_Same.Enabled = False
        Txt_RentYear_Total1.Locked = True:                        Txt_RentYear_Total1.BackColor = &HEBEBE7
        Txt_RentSame.Locked = True:                                   Txt_RentSame.BackColor = &HEBEBE7
                               For i = 1 To 12
                                       Txt_Rent_Month(i).BackColor = &HEBEBE7
                                       Txt_Rent_Month(i).Text = Empty
                                Next i
         
         Txt_Building_Area.Locked = True:                         Txt_Building_Area.BackColor = &HEBEBE7
         Txt_Percent.Locked = True:                                   Txt_Percent.BackColor = &HEBEBE7
         Txt_Building_NearArea.Locked = True:                Txt_Building_NearArea.BackColor = &HEBEBE7
         Txt_Building_OtherArea.Locked = True:               Txt_Building_OtherArea.BackColor = &HEBEBE7
         
         Txt_BUILDING_TOTAL_ROOM_TYPEB.Locked = True:               Txt_BUILDING_TOTAL_ROOM_TYPEB.BackColor = &HEBEBE7
         Txt_BUILDING_QTY_B_MONTH.Locked = True:                                   Txt_BUILDING_QTY_B_MONTH.BackColor = &HEBEBE7
         
         Txt_BUILDING_SUMAREA.Locked = True:                                      Txt_BUILDING_SUMAREA.BackColor = &HEBEBE7
         Txt_BUILDING_QTY_C_MONTH.Locked = True:                                         Txt_BUILDING_QTY_C_MONTH.BackColor = &HEBEBE7
         
         Txt_BUILDING_TOTAL_ROOM_TYPED.Locked = True:               Txt_BUILDING_TOTAL_ROOM_TYPED.BackColor = &HEBEBE7
         Txt_BUILDING_QTY_D_DAY.Locked = True:                                         Txt_BUILDING_QTY_D_DAY.BackColor = &HEBEBE7
        
         Chk_Person.Enabled = False:      Chk_Person.Value = Unchecked
         ChkSystem.Enabled = False:      ChkSystem.Value = Unchecked
         
            Cmb_BuildingType.Enabled = False
            combo1.Enabled = False
            
                Chk_Method1.Enabled = False
                Chk_Method2.Enabled = False
                Chk_Method3.Enabled = False
                Chk_Method4.Enabled = False
                Chk_Method1.Value = Unchecked
                Chk_Method2.Value = Unchecked
                Chk_Method3.Value = Unchecked
                Chk_Method4.Value = Unchecked
                
                Txt_StoryRemark.Visible = False
                Label2(107).Visible = False
                Shape3(0).BackColor = &HD6D6D6
                Shape3(1).BackColor = &HD6D6D6
                Shape3(2).BackColor = &HD6D6D6
                Shape3(3).BackColor = &HD6D6D6
                Shape3(4).BackColor = &HD6D6D6
                Shape3(5).BackColor = &HD6D6D6
                Chk_Flag_PayTax0.Enabled = False
                Chk_Flag_PayTax1.Enabled = False
   End If
End If
End Sub

Private Sub WriteHistoryFile(Tmp_BUILDING_ID As String)
Dim sql_txt As String, i As Byte, ROUND_COUNT As String, Temp_Change As String
Dim Query As ADODB.Recordset
Set Query = New ADODB.Recordset
i = 1
Set Query = Globle_Connective.Execute("exec sp_writehistory_building_query '" & Tmp_BUILDING_ID & "','0'", , adCmdUnknown)
'sql_txt = " Select A.* , B.OWNERSHIP_ID  From BUILDINGDATA A , BUILDINGDATA_NOTIC B WHERE  B.BUILDING_ID = A.BUILDING_ID AND A.BUILDING_ID = '" & Tmp_BUILDING_ID & "'"
'Call SET_QUERY(sql_txt, Query)
With Query
                     .MoveFirst
               Do While Not .EOF
'                        sql_txt = Empty
                     If i = 1 Then
                            If Status = "EDIT" Then
                                    Temp_Change = .Fields("BUILDING_CHANGE_ID").Value
                            ElseIf Status = "DEL" Then
                                    Temp_Change = "000"
                            End If
                            ROUND_COUNT = RunAutoNumber("BUILDINGDATASTORY", "ROUND_COUNT", Tmp_BUILDING_ID & "-0001", False, " AND BUILDING_ID = '" & Tmp_BUILDING_ID & "'")
'                            Debug.Print "exec sp_insert_buildingdatastory '" & ROUND_COUNT & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & _
'                                            .Fields("ZONETAX_ID").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_HOME_ID").Value & "','" & .Fields("BUILDING_HOME_NO").Value & "','" & .Fields("BUILDING_TYPE").Value & "','" & _
'                                            .Fields("BUILDING_STATE_DETAILS").Value & "','" & .Fields("BUILDING_STATE_REMARK").Value & "','" & IIf(IsNull(.Fields("BUILDING_DATE").Value), "", CvDate2(.Fields("BUILDING_DATE").Value)) & "','" & IIf(IsNull(.Fields("BUILDING_STATE_DATE").Value), "", CvDate2(.Fields("BUILDING_STATE_DATE").Value)) & "'," & _
'                                            .Fields("BUILDING_FLAG").Value & "," & .Fields("BUILDING_PRICE").Value & "," & .Fields("BUILDING_ROOM").Value & "," & .Fields("BUILDING_FLOOR").Value & "," & .Fields("BUILDING_WIDTH").Value & "," & _
'                                            .Fields("BUILDING_HEIGHT").Value & "," & .Fields("BUILDING_STATUS").Value & "," & .Fields("BUILDING_STATE1").Value & "," & .Fields("BUILDING_STATE2").Value & "," & .Fields("BUILDING_STATE3").Value & "," & _
'                                            .Fields("BUILDING_STATE_PRICE").Value & "," & .Fields("BUILDING_STATE_OTHER_PRICE").Value & "," & .Fields("BUILDING_STATE_YEAR").Value & "," & .Fields("TOTAL_TAX_EXCLUDE").Value & "," & .Fields("RENTYEAR_TOTAL_ALL").Value & ",'" & _
'                                            .Fields("FLAG_PAYTAX").Value & "','" & .Fields("USER_LOGIN").Value & "','" & CvDate2(.Fields("DATE_ACCESS").Value, 1) & "','" & Temp_Change & "','" & Query.Fields("BUILDING_TYPE_ID").Value & "','" & Query.Fields("BUILDING_NAME").Value & "'"
'                            Globle_Connective.Execute "exec sp_insert_buildingdatastory '" & ROUND_COUNT & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & _
'                                            .Fields("ZONETAX_ID").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_HOME_ID").Value & "','" & .Fields("BUILDING_HOME_NO").Value & "','" & .Fields("BUILDING_TYPE").Value & "','" & _
'                                            .Fields("BUILDING_STATE_DETAILS").Value & "','" & .Fields("BUILDING_STATE_REMARK").Value & "','" & IIf(IsNull(.Fields("BUILDING_DATE").Value), "", CvDate2(.Fields("BUILDING_DATE").Value)) & "','" & IIf(IsNull(.Fields("BUILDING_STATE_DATE").Value), "", CvDate2(.Fields("BUILDING_STATE_DATE").Value)) & "'," & _
'                                            .Fields("BUILDING_FLAG").Value & "," & .Fields("BUILDING_PRICE").Value & "," & .Fields("BUILDING_ROOM").Value & "," & .Fields("BUILDING_FLOOR").Value & "," & .Fields("BUILDING_WIDTH").Value & "," & _
'                                            .Fields("BUILDING_HEIGHT").Value & "," & .Fields("BUILDING_STATUS").Value & "," & .Fields("BUILDING_STATE1").Value & "," & .Fields("BUILDING_STATE2").Value & "," & .Fields("BUILDING_STATE3").Value & "," & _
'                                            .Fields("BUILDING_STATE_PRICE").Value & "," & .Fields("BUILDING_STATE_OTHER_PRICE").Value & "," & .Fields("BUILDING_STATE_YEAR").Value & "," & .Fields("TOTAL_TAX_EXCLUDE").Value & "," & .Fields("RENTYEAR_TOTAL_ALL").Value & ",'" & _
'                                            .Fields("FLAG_PAYTAX").Value & "','" & .Fields("USER_LOGIN").Value & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), CvDate2(Date)) & " " & Time & "','" & Temp_Change & "','" & Query.Fields("BUILDING_TYPE_ID").Value & "','" & Query.Fields("BUILDING_NAME").Value & "'", , adCmdUnknown
                            Call SET_Execute2("exec sp_insert_buildingdatastory '" & ROUND_COUNT & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & _
                                            .Fields("ZONETAX_ID").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_HOME_ID").Value & "','" & .Fields("BUILDING_HOME_NO").Value & "','" & .Fields("BUILDING_TYPE").Value & "','" & _
                                            .Fields("BUILDING_STATE_DETAILS").Value & "','" & .Fields("BUILDING_STATE_REMARK").Value & "','" & IIf(IsNull(.Fields("BUILDING_DATE").Value), "", CvDate2(.Fields("BUILDING_DATE").Value)) & "','" & IIf(IsNull(.Fields("BUILDING_STATE_DATE").Value), "", CvDate2(.Fields("BUILDING_STATE_DATE").Value)) & "'," & _
                                            .Fields("BUILDING_FLAG").Value & "," & .Fields("BUILDING_PRICE").Value & "," & .Fields("BUILDING_ROOM").Value & "," & .Fields("BUILDING_FLOOR").Value & "," & .Fields("BUILDING_WIDTH").Value & "," & _
                                            .Fields("BUILDING_HEIGHT").Value & "," & .Fields("BUILDING_STATUS").Value & "," & .Fields("BUILDING_STATE1").Value & "," & .Fields("BUILDING_STATE2").Value & "," & .Fields("BUILDING_STATE3").Value & "," & _
                                            .Fields("BUILDING_STATE_PRICE").Value & "," & .Fields("BUILDING_STATE_OTHER_PRICE").Value & "," & .Fields("BUILDING_STATE_YEAR").Value & "," & .Fields("TOTAL_TAX_EXCLUDE").Value & "," & .Fields("RENTYEAR_TOTAL_ALL").Value & ",'" & _
                                            .Fields("FLAG_PAYTAX").Value & "','" & .Fields("USER_LOGIN").Value & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), CvDate2(Date)) & " " & Time & "','" & Temp_Change & "','" & Query.Fields("BUILDING_TYPE_ID").Value & "','" & Query.Fields("BUILDING_NAME").Value & "'", 8, False)
'                            sql_txt = "INSERT INTO BUILDINGDATASTORY ( ROUND_COUNT,BUILDING_ID,ZONE_BLOCK , ZONETAX_ID , LAND_ID , BUILDING_HOME_ID , BUILDING_HOME_NO, BUILDING_TYPE ," & _
'                                                  " BUILDING_STATE_DETAILS, BUILDING_STATE_REMARK , BUILDING_DATE,BUILDING_STATE_DATE , " & _
'                                                  " BUILDING_FLAG, BUILDING_PRICE,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT,BUILDING_STATUS ," & _
'                                                   "BUILDING_STATE1 ,BUILDING_STATE2,BUILDING_STATE3,BUILDING_STATE_PRICE,BUILDING_STATE_OTHER_PRICE,BUILDING_STATE_YEAR ,TOTAL_TAX_EXCLUDE , RENTYEAR_TOTAL_ALL,BUILDING_STORY,BUILDING_UPDATE,FLAG_PAYTAX )"
'                            sql_txt = sql_txt & " VALUES ('" & ROUND_COUNT & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','"
'                            sql_txt = sql_txt & .Fields("ZONETAX_ID").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_HOME_ID").Value & "','" & .Fields("BUILDING_HOME_NO").Value & "','" & .Fields("BUILDING_TYPE").Value & "','"
'                            sql_txt = sql_txt & .Fields("BUILDING_STATE_DETAILS").Value & "','" & .Fields("BUILDING_STATE_REMARK").Value & "'," & IIf(IsNull(.Fields("BUILDING_DATE").Value), "Null", "'" & .Fields("BUILDING_DATE").Value & "'") & "," & IIf(IsNull(.Fields("BUILDING_STATE_DATE").Value), "Null", "'" & .Fields("BUILDING_STATE_DATE").Value & "'") & ","
'                            sql_txt = sql_txt & .Fields("BUILDING_FLAG").Value & "," & .Fields("BUILDING_PRICE").Value & "," & .Fields("BUILDING_ROOM").Value & "," & .Fields("BUILDING_FLOOR").Value & "," & .Fields("BUILDING_WIDTH").Value & ","
'                            sql_txt = sql_txt & .Fields("BUILDING_HEIGHT").Value & "," & .Fields("BUILDING_STATUS").Value & "," & .Fields("BUILDING_STATE1").Value & "," & .Fields("BUILDING_STATE2").Value & "," & .Fields("BUILDING_STATE3").Value & ","
'                            sql_txt = sql_txt & .Fields("BUILDING_STATE_PRICE").Value & "," & .Fields("BUILDING_STATE_OTHER_PRICE").Value & "," & .Fields("BUILDING_STATE_YEAR").Value & "," & .Fields("TOTAL_TAX_EXCLUDE").Value & "," & .Fields("RENTYEAR_TOTAL_ALL").Value & ",'"
'                            sql_txt = sql_txt & Trim$(Txt_StoryRemark.Text) & "','" & Date & "'," & .Fields("FLAG_PAYTAX").Value & ")"
'                            Call SET_Execute(sql_txt)
                     End If
'                            sql_txt = Empty
'                            Globle_Connective.Execute "exec sp_insert_buildingdata_notic_story '" & RunAutoNumber("BUILDINGDATA_NOTIC_STORY", "BUILDING_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                            ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", , adCmdUnknown
                            Call SET_Execute2("exec sp_insert_buildingdata_notic_story '" & RunAutoNumber("BUILDINGDATA_NOTIC_STORY", "BUILDING_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                            ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", 8, False)
'                            sql_txt = " INSERT INTO BUILDINGDATA_NOTIC_STORY ( BUILDING_NOTIC_ID, ROUND_COUNT,OWNERSHIP_ID ) "
'                            sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("BUILDINGDATA_NOTIC_STORY", "BUILDING_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                            sql_txt = sql_txt & ROUND_COUNT & "','"
'                            sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "')"
'                            Call SET_Execute(sql_txt)
                            .MoveNext
                            i = i + 1
                Loop
                
'                sql_txt = Empty
                Set Query = Globle_Connective.Execute("exec sp_writehistory_building_query '" & Tmp_BUILDING_ID & "','1'", , adCmdUnknown)
'                sql_txt = " Select *  From BUILDINGDATA_APPLY WHERE BUILDING_ID  = '" & Tmp_BUILDING_ID & "'"
'                Call SET_QUERY(sql_txt, Query)
                         .MoveFirst
               Do While Not .EOF
'                        sql_txt = Empty
'                        Globle_Connective.Execute "exec sp_insert_buildingdata_apply_story '" & RunAutoNumber("BUILDINGDATA_APPLY_STORY", "BUILDING_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                    ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", , adCmdUnknown
                        Call SET_Execute2("exec sp_insert_buildingdata_apply_story '" & RunAutoNumber("BUILDINGDATA_APPLY_STORY", "BUILDING_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                    ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", 8, False)
'                        sql_txt = "INSERT INTO BUILDINGDATA_APPLY_STORY ( BUILDING_APPLY_ID,ROUND_COUNT,OWNERSHIP_ID) "
'                        sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("BUILDINGDATA_APPLY_STORY", "BUILDING_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                        sql_txt = sql_txt & ROUND_COUNT & "','"
'                        sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "')"
'                        Call SET_Execute(sql_txt)
                        .MoveNext
                Loop
                
'                 Dim Logic As Boolean   'MAKE FOR insert ����������㹵��ҧ PRD2 ��èѴ���������������¹����͡����Է��� =================================
'                 Logic = False
'                sql_txt = Empty
'                sql_txt = " Select  * From PRD2  WHERE BUILDING_ID  = '" & Tmp_BUILDING_ID & "' ORDER BY PRD2_PAY DESC"
'                Call SET_QUERY(sql_txt, Query)
'                If .RecordCount > 0 Then
'                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                           .MoveFirst
'                            Do While Not .EOF
'                                                              If Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) = Trim$(.Fields("OWNERSHIP_ID").Value) Then
'                                                                  Logic = True
'                                                                  .MoveLast
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                    .MoveNext
'                           Loop
'                                                             If Logic = False Then
'                                                                 .MovePrevious
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "INSERT INTO PRD2 ( BUILDING_ID, OWNERSHIP_ID ,PRD2_YEAR,PRD2_NO,PRD2_DATE , PRD2_NO_STATUS,PRD2_RENTYEAR_TOTAL,PRD2_STATUS,PRD2_PAY" & _
'                                                                                   ",PRD2_REMARK,FLAG_YEAR,PRD2_NO_ACCEPT,PRD2_DATE_ACCEPT,PRD2_BOOK_NUMBER,PRD2_BOOK_NO,PRD2_PAY_DATE,PRD2_TAXINCLUDE)"
'                                                                  sql_txt = sql_txt & "VALUES ('" & .Fields("BUILDING_ID").Value & "','" & Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) & "'," & .Fields("PRD2_YEAR").Value & ",'" & _
'                                                                                   .Fields("PRD2_NO").Value & "'," & IIf(IsNull(.Fields("PRD2_DATE").Value), "Null", "'" & .Fields("PRD2_DATE").Value & "'") & "," & .Fields("PRD2_NO_STATUS").Value & "," & _
'                                                                                   .Fields("PRD2_RENTYEAR_TOTAL").Value & "," & .Fields("PRD2_STATUS").Value & "," & .Fields("PRD2_PAY").Value & ",'" & .Fields("PRD2_REMARK").Value & "'," & .Fields("FLAG_YEAR").Value & ",'" & _
'                                                                                   .Fields("PRD2_NO_ACCEPT").Value & "'," & IIf(IsNull(.Fields("PRD2_DATE_ACCEPT").Value), "Null", "'" & .Fields("PRD2_DATE_ACCEPT").Value & "'") & ",'" & .Fields("PRD2_BOOK_NUMBER").Value & "','" & _
'                                                                                   .Fields("PRD2_BOOK_NO").Value & "'," & IIf(IsNull(.Fields("PRD2_PAY_DATE").Value), "Null", "'" & .Fields("PRD2_PAY_DATE").Value & "'") & "," & .Fields("PRD2_TAXINCLUDE").Value & ")"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                    Next i
'                                    sql_txt = "UPDATE PRD2 SET PRD2_RENTYEAR_TOTAL = " & CDbl(FormatNumber(Lb_GrandTotal_Tax.Caption, 2)) & _
'                                                    " WHERE BUILDING_ID = '" & Tmp_BUILDING_ID & "' AND PRD2_YEAR = " & IIf(Year(Date) < 2500, Year(Date) + 543, Year(Date) - 543) & " AND PRD2_STATUS = 0 AND PRD2_NO_STATUS = 0 "
'                                    Call SET_Execute(sql_txt)
'                           '========================================================
'                             Logic = False
'                                               .MoveFirst
'                                        Do While Not .EOF
'                                                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                                                              If Trim$(.Fields("OWNERSHIP_ID").Value) = Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) Then
'                                                                  Logic = True
'                                                                   i = LstV_OwnerShip.ListItems.Count + 7
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                                    Next i
'                                                             If Logic = False Then
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "DELETE BUILDING_ID  FROM PRD2 WHERE BUILDING_ID = '" & Tmp_BUILDING_ID & "' AND OWNERSHIP_ID = '" & Trim$(.Fields("OWNERSHIP_ID").Value) & "'"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                                                    .MoveNext
'                                        Loop
'                End If
End With
Set Query = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
Dim sql_txt As String
Dim i  As Byte
Dim Rs As ADODB.Recordset
If Lb_GrandTotal_Tax.Caption = "0.00" Or Lb_GrandTotal_Tax.Caption = ".00" Then
        Chk_Flag_PayTax1.Value = Unchecked
        Chk_Flag_PayTax0.Value = Checked
End If
'        Globle_Connective.BeginTrans
        Set Rs = New ADODB.Recordset
        Set Rs = Globle_Connective.Execute("exec sp_max_year_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','','0','',''", , adCmdUnknown)
        If IsNull(Rs.Fields(0).Value) <> True Then
                    Label2(4).Tag = Rs.Fields(0).Value
        Else
                    Label2(4).Tag = vbNullString
        End If
        If STATE = "DEL" Then
                Call WriteHistoryFile(Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text)
        End If
        sql_txt = "exec sp_manage_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Cmb_Zoneblock.Text & "','" & Cmb_Tamlay.Text & "','" & Cmb_Land_ID.Text & "','" & Trim$(Txt_BUILDING_HOME_ID.Text) & "','" & Trim$(Txt_BUILDING_HOME_NO) & "','" & Trim$(Cmb_Building_Type.Text) & "','" & _
                                      Trim$(Txt_BUILDING_STATE_DETAILS.Text) & "','" & Trim$(Txt_BUILDING_STATE_REMARK.Text) & "','" & IIf(IsNull(DTP_BUILDING_DATE) = False, CvDate1(DTP_BUILDING_DATE), "") & "','" & IIf(IsNull(DTP_BUILDING_STATE_DATE) = False, CvDate1(DTP_BUILDING_STATE_DATE), "") & "'," & _
                                      Chk_BUILDING_FLAG1.Value & "," & CCur(Txt_BUILDING_PRICE.Text) & "," & Txt_BUILDING_ROOM.Text & "," & CSng(Txt_BUILDING_FLOOR.Text) & "," & CSng(Txt_BUILDING_WIDTH.Text) & "," & CSng(Txt_BUILDING_HEIGHT.Text) & "," & CHK_BUILDING_STATUS1.Value & "," & _
                                      Chk_BUILDING_STATE1.Value & "," & Chk_BUILDING_STATE2.Value & "," & Chk_BUILDING_STATE3.Value & "," & CCur(Txt_BUILDING_STATE_PRICE.Text) & "," & CCur(Txt_BUILDING_STATE_OTHER_PRICE.Text) & "," & Txt_BUILDING_STATE_YEAR.Text & "," & _
                                      CCur(Lb_GrandTotal_Tax.Caption) & "," & CCur(Txt_RentYear_Total_All.Caption) & "," & Chk_Flag_PayTax1.Value & ",'" & strUser & "','" & CvDate2(Now, 1) & "','" & cb_Building_Change_ID.Text & "','" & Chk_Person.Value & "','" & Cmb_Building_Type_II_ID.Text & "','" & STATE & "','" & Trim$(Txt_Building_Name.Text) & "'"
'        Globle_Connective.Execute sql_txt, , adCmdUnknown
        Call SET_Execute2(sql_txt, 8, False)
'        Call SET_QUERY("SELECT MAX(PRD2_YEAR) FROM PRD2 WHERE BUILDING_ID='" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'", Rs)
        If STATE = "EDIT" Then
'                sql_txt = "INSERT INTO BUILDINGDATA ( BUILDING_ID,ZONE_BLOCK , ZONETAX_ID , LAND_ID , BUILDING_HOME_ID , BUILDING_HOME_NO, BUILDING_TYPE ," & _
'                                      " BUILDING_STATE_DETAILS, BUILDING_STATE_REMARK , BUILDING_DATE,BUILDING_STATE_DATE , " & _
'                                      " BUILDING_FLAG, BUILDING_PRICE,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT,BUILDING_STATUS ," & _
'                                      "BUILDING_STATE1 ,BUILDING_STATE2,BUILDING_STATE3,BUILDING_STATE_PRICE,BUILDING_STATE_OTHER_PRICE,BUILDING_STATE_YEAR ,TOTAL_TAX_EXCLUDE , RENTYEAR_TOTAL_ALL,FLAG_PAYTAX ,USER_LOGIN,DATE_ACCESS)"
'                sql_txt = sql_txt & " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "','" & Cmb_Zoneblock.Text & "','" & Cmb_Tamlay.Text & "','" & Cmb_Land_ID.Text & "','" & Trim$(Txt_BUILDING_HOME_ID.Text) & "','" & Trim$(Txt_BUILDING_HOME_NO) & "','" & Trim$(Cmb_Building_Type.Text) & "','" & _
'                                      Trim$(Txt_BUILDING_STATE_DETAILS.Text) & "','" & Trim$(Txt_BUILDING_STATE_REMARK.Text) & "','" & Format$(DTP_BUILDING_DATE.Value, "dd/mm/yyyy") & "','" & Format$(DTP_BUILDING_STATE_DATE.Value, "dd/mm/yyyy") & "'," & _
'                                      CByte(Chk_BUILDING_FLAG1.Value) & "," & CSng(Txt_BUILDING_PRICE.Text) & "," & CInt(Txt_BUILDING_ROOM.Text) & "," & CSng(Txt_BUILDING_FLOOR.Text) & "," & CSng(Txt_BUILDING_WIDTH.Text) & "," & CSng(Txt_BUILDING_HEIGHT.Text) & "," & CByte(CHK_BUILDING_STATUS1.Value) & "," & _
'                                      CByte(Chk_BUILDING_STATE1.Value) & "," & CByte(Chk_BUILDING_STATE2.Value) & "," & CByte(Chk_BUILDING_STATE3.Value) & "," & CSng(Txt_BUILDING_STATE_PRICE.Text) & "," & CSng(Txt_BUILDING_STATE_OTHER_PRICE.Text) & "," & CByte(Txt_BUILDING_STATE_YEAR.Text) & "," & CSng(Lb_GrandTotal_Tax.Caption) & "," & CSng(Txt_RentYear_Total_All.Caption) & "," & CByte(Chk_Flag_PayTax1.Value) & ",'" & strUser & "','" & Now & "')"
'                Call SET_Execute(sql_txt)
'
'        ElseIf STATE = "EDIT" Then
                Call WriteHistoryFile(Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text)
'                sql_txt = " UPDATE BUILDINGDATA SET ZONETAX_ID = '" & Cmb_Tamlay.Text & "'," & _
'                                " LAND_ID =   '" & Cmb_Land_ID.Text & "'," & _
'                                " BUILDING_HOME_ID =   '" & Trim$(Txt_BUILDING_HOME_ID.Text) & "'," & _
'                                " BUILDING_HOME_NO =   '" & Trim$(Txt_BUILDING_HOME_NO.Text) & "'," & _
'                                " BUILDING_TYPE =   '" & Trim$(Cmb_Building_Type.Text) & "'," & _
'                                " BUILDING_STATE_DETAILS =   '" & Trim$(Txt_BUILDING_STATE_DETAILS.Text) & "'," & _
'                                " BUILDING_STATE_REMARK =   '" & Trim$(Txt_BUILDING_STATE_REMARK.Text) & "'," & _
'                                " BUILDING_DATE =   '" & Format$(DTP_BUILDING_DATE.Value, "dd/mm/yyyy") & "'," & _
'                                " BUILDING_STATE_DATE =   '" & Format$(DTP_BUILDING_STATE_DATE.Value, "dd/mm/yyyy") & "'," & _
'                                " BUILDING_FLAG =   " & CByte(Chk_BUILDING_FLAG1.Value) & "," & _
'                                " BUILDING_PRICE =   " & CSng(Txt_BUILDING_PRICE.Text) & "," & _
'                                " BUILDING_ROOM =   " & CInt(Txt_BUILDING_ROOM.Text) & "," & _
'                                " BUILDING_FLOOR =   " & CSng(Txt_BUILDING_FLOOR.Text) & "," & _
'                                " BUILDING_WIDTH =   " & CSng(Txt_BUILDING_WIDTH.Text) & "," & _
'                                " BUILDING_HEIGHT =   " & CSng(Txt_BUILDING_HEIGHT.Text) & "," & _
'                                " FLAG_ASSESS =   " & CByte(Chk_Person.Value) & "," & _
'                                " BUILDING_STATUS =   " & CByte(CHK_BUILDING_STATUS1.Value) & "," & _
'                                " BUILDING_STATE1 =   " & CByte(Chk_BUILDING_STATE1.Value) & "," & _
'                                " BUILDING_STATE2 =   " & CByte(Chk_BUILDING_STATE2.Value) & "," & _
'                                " BUILDING_STATE3 =   " & CByte(Chk_BUILDING_STATE3.Value) & "," & _
'                                " BUILDING_STATE_PRICE =   " & CSng(Txt_BUILDING_STATE_PRICE.Text) & "," & _
'                                " BUILDING_STATE_OTHER_PRICE =   " & CSng(Txt_BUILDING_STATE_OTHER_PRICE.Text) & "," & _
'                                " BUILDING_STATE_YEAR =   " & CByte(Txt_BUILDING_STATE_YEAR.Text) & "," & _
'                                " RENTYEAR_TOTAL_ALL =   " & CSng(Txt_RentYear_Total_All.Caption) & ","
'                sql_txt = sql_txt & " TOTAL_TAX_EXCLUDE =   " & CSng(Lb_GrandTotal_Tax.Caption) & "," & _
'                                                 " FLAG_PAYTAX  = " & CByte(Chk_Flag_PayTax1.Value) & "," & _
'                                                 " USER_LOGIN = '" & strUser & "'," & _
'                                                 " DATE_ACCESS = '" & Now & "'" & _
'                                                 " WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'        ElseIf STATE = "DEL" Then
'                            sql_txt = "Delete  FROM BUILDINGDATA_IMAGE WHERE BUILDING_ID  = '" & Cmb_Zoneblock.Text & Txt_Building_ID & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATA_NOTIC WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATA_APPLY WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATA_ASSESS  WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATA_RENT  WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATASTORY  WHERE BUILDING_ID  = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete   FROM BUILDINGDATA_NOTIC_STORY  WHERE ROUND_COUNT LIKE '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "%'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete   FROM BUILDINGDATA_APPLY_STORY  WHERE ROUND_COUNT LIKE '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "%'"
'                                            Call SET_Execute(sql_txt)
'                            sql_txt = "Delete  FROM BUILDINGDATA  WHERE BUILDING_ID  = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                            Call SET_Execute(sql_txt)
        End If
                            sql_txt = Empty
                                      
                                      If STATE <> "DEL" Then
'                                                Globle_Connective.Execute "exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','3'", , adCmdUnknown
                                                Call SET_Execute2("exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','3'", 8, False)
'                                                   sql_txt = "DELETE FROM BUILDINGDATA_RENT  WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                                   Call SET_Execute(sql_txt)
'                                                  sql_txt = "DELETE FROM BUILDINGDATA_ASSESS WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                                   Call SET_Execute(sql_txt)
                                                If ChkSystem.Value Then '�ѹ�֡��÷ӻ����Թ����
                                                        If CCur(Lb_RentSumTax.Caption) > 0 Then   '�ó�������
'                                                                Globle_Connective.Execute "exec sp_insert_buildingdata_rent '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Cmb_BuildingType_ID.Text & "'," & CCur(Txt_Rent_Month(1).Text) & "," & _
'                                                                                CCur(Txt_Rent_Month(2).Text) & "," & CCur(Txt_Rent_Month(3).Text) & "," & CCur(Txt_Rent_Month(4).Text) & "," & CCur(Txt_Rent_Month(5).Text) & "," & CCur(Txt_Rent_Month(6).Text) & "," & CCur(Txt_Rent_Month(7).Text) & "," & CCur(Txt_Rent_Month(8).Text) & "," & _
'                                                                                CCur(Txt_Rent_Month(9).Text) & "," & CCur(Txt_Rent_Month(10).Text) & "," & CCur(Txt_Rent_Month(11).Text) & "," & CCur(Txt_Rent_Month(12).Text) & "," & CCur(LB_RentYear_Total2.Caption) & "," & CCur(Lb_RentSumTax.Caption), , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_buildingdata_rent '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Cmb_Tamlay.Text & "','" & Cmb_BuildingType_ID.Text & "'," & CCur(Txt_Rent_Month(1).Text) & "," & _
                                                                                CCur(Txt_Rent_Month(2).Text) & "," & CCur(Txt_Rent_Month(3).Text) & "," & CCur(Txt_Rent_Month(4).Text) & "," & CCur(Txt_Rent_Month(5).Text) & "," & CCur(Txt_Rent_Month(6).Text) & "," & CCur(Txt_Rent_Month(7).Text) & "," & CCur(Txt_Rent_Month(8).Text) & "," & _
                                                                                CCur(Txt_Rent_Month(9).Text) & "," & CCur(Txt_Rent_Month(10).Text) & "," & CCur(Txt_Rent_Month(11).Text) & "," & CCur(Txt_Rent_Month(12).Text) & "," & CCur(LB_RentYear_Total2.Caption) & "," & CCur(Lb_RentSumTax.Caption), 8, False)
'                                                              sql_txt = "INSERT INTO BUILDINGDATA_RENT (BUILDING_ID,BUILDING_SHAPE_ID,RENT_JANUARY,RENT_FEBUARY,RENT_MARCH,RENT_APRIL,RENT_MAY,RENT_JUNE,RENT_JULY,RENT_AUGUST,RENT_SEPTEMBER ,RENT_OCTOBER ,RENT_NOVEMBER,RENT_DECEMBER,RENTYEAR_TOTAL,TAX_EXCLUDE) " & _
'                                                                                    " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "','" & Cmb_BuildingType_ID.Text & "'," & Txt_Rent_Month(1).Text & "," & Txt_Rent_Month(2).Text & "," & Txt_Rent_Month(3).Text & "," & Txt_Rent_Month(4).Text & "," & Txt_Rent_Month(5).Text & "," & Txt_Rent_Month(6).Text & "," & Txt_Rent_Month(7).Text & "," & Txt_Rent_Month(8).Text & "," & _
'                                                                                    Txt_Rent_Month(9).Text & "," & Txt_Rent_Month(10).Text & "," & Txt_Rent_Month(11).Text & "," & Txt_Rent_Month(12).Text & "," & CSng(LB_RentYear_Total2.Caption) & "," & CSng(Lb_RentSumTax.Caption) & ")"
'                                                                                    Call SET_Execute(sql_txt)
                                                        End If
                                                        
                                                        If Chk_Method1.Value Then    '�óշӡ�ä��
'                                                                Globle_Connective.Execute "exec sp_insert_buildingdata_assess1 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',1,'" & Cmb_BuildingType_ID.Text & "'," & Txt_Building_Area.Text & "," & _
'                                                                                Txt_Building_NearArea.Text & "," & Txt_Building_OtherArea.Text & "," & CCur(LB_RentYear_Total3.Caption) & "," & CCur(Lb_UseSumTax1.Caption), , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_buildingdata_assess1 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',1,'" & Cmb_Tamlay.Text & "','" & Cmb_BuildingType_ID.Text & "'," & Txt_Building_Area.Text & "," & _
                                                                                Txt_Building_NearArea.Text & "," & Txt_Building_OtherArea.Text & "," & CCur(LB_RentYear_Total3.Caption) & "," & CCur(Lb_UseSumTax1.Caption) & "," & Txt_Percent.Text, 8, False)
'                                                                               sql_txt = "INSERT INTO BUILDINGDATA_ASSESS (BUILDING_ID, BUILDING_FLAG_ASSESS,BUILDING_SHAPE_ID, BUILDING_AREA,BUILDING_NEARAREA,BUILDING_OTHERAREA ,RENTYEAR_TOTAL,TAX_EXCLUDE )" & _
'                                                                                                     " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "',1,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_Building_Area.Text) & "," & CSng(Txt_Building_NearArea.Text) & "," & CSng(Txt_Building_OtherArea.Text) & "," & _
'                                                                                                     CSng(LB_RentYear_Total3.Caption) & "," & CSng(Lb_UseSumTax1.Caption) & ")"
'                                                                                                      Call SET_Execute(sql_txt)
                                                        End If
                                                        If Chk_Method2.Value Then   '�óշӡ�ä��
'                                                                Globle_Connective.Execute "exec sp_insert_buildingdata_assess2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',2,'" & Cmb_BuildingType_ID.Text & "'," & Txt_BUILDING_TOTAL_ROOM_TYPEB.Text & "," & _
'                                                                                Txt_BUILDING_QTY_B_MONTH.Text & "," & CCur(LB_RentYear_Total4.Caption) & "," & CCur(Lb_UseSumTax2.Caption), , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_buildingdata_assess2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',2,'" & Cmb_BuildingType_ID.Text & "'," & Txt_BUILDING_TOTAL_ROOM_TYPEB.Text & "," & _
                                                                                Txt_BUILDING_QTY_B_MONTH.Text & "," & CCur(LB_RentYear_Total4.Caption) & "," & CCur(Lb_UseSumTax2.Caption), 8, False)
'                                                                                sql_txt = "INSERT INTO BUILDINGDATA_ASSESS(BUILDING_ID,BUILDING_FLAG_ASSESS,BUILDING_SHAPE_ID, BUILDING_TOTAL_ROOM_TYPEB, BUILDING_QTY_B_MONTH,RENTYEAR_TOTAL,TAX_EXCLUDE )" & _
'                                                                                                      " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "',2,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_BUILDING_TOTAL_ROOM_TYPEB.Text) & "," & CSng(Txt_BUILDING_QTY_B_MONTH.Text) & "," & CSng(LB_RentYear_Total4.Caption) & "," & CSng(Lb_UseSumTax2.Caption) & ")"
'                                                                                                      Call SET_Execute(sql_txt)
                                                        End If
                                                        If Chk_Method3.Value Then   '�óշӡ�ä��
'                                                                Globle_Connective.Execute "exec sp_insert_buildingdata_assess3 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',3,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_BUILDING_SUMAREA.Text) & "," & _
'                                                                                Txt_BUILDING_QTY_C_MONTH.Text & "," & CCur(LB_RentYear_Total4.Caption) & "," & CCur(Lb_UseSumTax3.Caption), , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_buildingdata_assess3 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',3,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_BUILDING_SUMAREA.Text) & "," & _
                                                                                Txt_BUILDING_QTY_C_MONTH.Text & "," & CCur(LB_RentYear_Total4.Caption) & "," & CCur(Lb_UseSumTax3.Caption), 8, False)
'                                                                                 sql_txt = "INSERT INTO BUILDINGDATA_ASSESS(BUILDING_ID,BUILDING_FLAG_ASSESS,BUILDING_SHAPE_ID, BUILDING_SUMAREA ,BUILDING_QTY_C_MONTH,RENTYEAR_TOTAL,TAX_EXCLUDE)" & _
'                                                                                                         " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "',3,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_BUILDING_SUMAREA.Text) & "," & CSng(Txt_BUILDING_QTY_C_MONTH.Text) & "," & CSng(LB_RentYear_Total5.Caption) & "," & CSng(Lb_UseSumTax3.Caption) & ")"
'                                                                                                         Call SET_Execute(sql_txt)
                                                        End If
                                                        If Chk_Method4.Value Then   '�óշӡ�ä��
'                                                                Globle_Connective.Execute "exec sp_insert_buildingdata_assess4 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',4,'" & Cmb_BuildingType_ID.Text & "'," & Txt_BUILDING_TOTAL_ROOM_TYPED.Text & "," & _
'                                                                                Txt_BUILDING_QTY_D_DAY.Text & "," & CCur(LB_RentYear_Total6.Caption) & "," & CCur(Lb_UseSumTax4.Caption), , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_buildingdata_assess4 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "',4,'" & Cmb_BuildingType_ID.Text & "'," & Txt_BUILDING_TOTAL_ROOM_TYPED.Text & "," & _
                                                                                Txt_BUILDING_QTY_D_DAY.Text & "," & CCur(LB_RentYear_Total6.Caption) & "," & CCur(Lb_UseSumTax4.Caption), 8, False)
'                                                                                  sql_txt = "INSERT INTO BUILDINGDATA_ASSESS (BUILDING_ID,BUILDING_FLAG_ASSESS,BUILDING_SHAPE_ID, BUILDING_TOTAL_ROOM_TYPED,BUILDING_QTY_D_DAY,RENTYEAR_TOTAL,TAX_EXCLUDE )" & _
'                                                                                                         " VALUES ('" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "',4,'" & Cmb_BuildingType_ID.Text & "'," & CSng(Txt_BUILDING_TOTAL_ROOM_TYPED.Text) & "," & CSng(Txt_BUILDING_QTY_D_DAY.Text) & "," & CSng(LB_RentYear_Total6.Caption) & "," & CSng(Lb_UseSumTax4.Caption) & ")"
'                                                                                                         Call SET_Execute(sql_txt)
                                                         End If
                                             End If
'                                                        Globle_Connective.Execute "exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','0'", , adCmdUnknown
                                                        Call SET_Execute2("exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','0'", 8, False)
'                                                           sql_txt = "Delete FROM BUILDINGDATA_IMAGE WHERE BUILDING_ID  = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                                           Call SET_Execute(sql_txt)
                                                If LstV_Img_Name.ListItems.Count > 0 Then
                                                    For i = 1 To LstV_Img_Name.ListItems.Count
'                                                            Globle_Connective.Execute "exec sp_insert_image '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "###00#") & "','" & LstV_Img_Name.ListItems(i).Text & "','1'", , adCmdUnknown
                                                            Call SET_Execute2("exec sp_insert_image '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "###00#") & "','" & LstV_Img_Name.ListItems(i).Text & "','1'", 8, False)
'                                                           sql_txt = " INSERT INTO BUILDINGDATA_IMAGE ( BUILDING_IMG_ID, BUILDING_ID , BUILDING_IMG_NAME ) "
'                                                           sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "-" & Format$(i, "###00#") & "','"
'                                                           sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "','"
'                                                           sql_txt = sql_txt & LstV_Img_Name.ListItems(i).Text & "')"
'                                                           Call SET_Execute(sql_txt)
                                                           Call CopyFile(LstV_Img_Name.ListItems(i).SubItems(1), iniPath_Picture & "\Zone" & Left$(Cmb_Zoneblock.Text, 2) & "\Block_" & Right$(Cmb_Zoneblock.Text, 1) & "\Building\" & LstV_Img_Name.ListItems(i).Text, 0)
                                                   Next i
                                              End If
                                            If LstV_OwnerShip.ListItems.Count > 0 Then
'                                                     Globle_Connective.Execute "exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','1'", , adCmdUnknown
                                                     Call SET_Execute2("exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','1'", 8, False)
'                                                     sql_txt = "Delete FROM BUILDINGDATA_NOTIC WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                                     Call SET_Execute(sql_txt)
                                                 For i = 1 To LstV_OwnerShip.ListItems.Count
'                                                        Globle_Connective.Execute "exec sp_insert_buildingdata_notic '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "##0#") & "','" & _
'                                                                    Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "','" & _
'                                                                    IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & "'", , adCmdUnknown
                                                        Call SET_Execute2("exec sp_insert_buildingdata_notic '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "##0#") & "','" & _
                                                                    Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "','" & _
                                                                    IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & "'", 8, False)
'                                                        sql_txt = " INSERT INTO BUILDINGDATA_NOTIC ( BUILDING_NOTIC_ID, BUILDING_ID,OWNERSHIP_ID,OWNERSHIP_NOTIC,OWNERSHIP_MAIL ) "
'                                                        sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "-" & Format$(i, "##0#") & "','"
'                                                        sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "','"
'                                                        sql_txt = sql_txt & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'," & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "," & _
'                                                                                         IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & ")"
'                                                        Call SET_Execute(sql_txt)
        ' �礡������¹�����Է���
                                                        If STATE = "EDIT" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" And LstV_OwnerShip.ListItems(i).ListSubItems(4).Text <> str_OwnershipID3 Then
        '                                                                Call SET_QUERY("SELECT MAX(PBT5_YEAR) FROM PBT5 WHERE LAND_ID='" & Temp_LAND_ID & "'", Rs)
                                                                        If Label2(4).Tag <> vbNullString Then
                                                                                Set Rs = Globle_Connective.Execute("exec sp_max_year_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','','3',''," & Label2(4).Tag, , adCmdUnknown)  '�� COUNT_CHANGE
                                                                                If IsNull(Rs.Fields(0).Value) = False Then
                                                                                        Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                Else
                                                                                        Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                End If
                                                                    
                                                                                Select Case Rs.Fields(0).Value '  PBT5_SET_GK
                                                                                        Case 0
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", 8, False)
                                                                                        Case 1
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", 8, False)
                                                                                        Case 2
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", 8, False)
                                                                                End Select
        '                                                                        sql_txt = "UPDATE PBT5 SET FLAG_CHANGE=1 WHERE LAND_ID= '" & Temp_LAND_ID & "' AND PBT5_YEAR=" & Rs.Fields(0).Value
        '                                                                        Call SET_Execute(sql_txt)
                                                                        End If
                                                                End If
                                                        ElseIf STATE = "ADD" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" Then
                                                                        If Chk_Flag_PayTax1.Value = Checked Then  '�ա�û����Թ����
                                                                                If Label2(4).Tag <> vbNullString Then
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','','3',''," & Label2(4).Tag, , adCmdUnknown)  '�� COUNT_CHANGE
                                                                                        If IsNull(Rs.Fields(0).Value) = False Then
                                                                                                Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                        Else
                                                                                                Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                        End If
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','','5'," & Label2(6).Tag & "," & Label2(4).Tag, , adCmdUnknown)  '�� COUNT_CHANGE
                                                                                        If Rs.Fields(1).Value = LstV_OwnerShip.ListItems(i).ListSubItems(4).Text Then
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        Else
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_prd2 '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        End If
                                                                                End If
                                                                        End If
                                                                End If
                                                        End If
                                                 Next i
                                            End If
                                            If LstV_OwnerUse.ListItems.Count > 0 Then
'                                                        Globle_Connective.Execute "exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','2'", , adCmdUnknown
                                                        Call SET_Execute2("exec sp_del_notic_apply_img_buildingdata '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','2'", 8, False)
'                                                     sql_txt = "Delete FROM BUILDINGDATA_APPLY WHERE BUILDING_ID = '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "'"
'                                                     Call SET_Execute(sql_txt)
                                                 For i = 1 To LstV_OwnerUse.ListItems.Count
'                                                            Globle_Connective.Execute "exec sp_insert_buildingdata_apply '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "##0#") & "','" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & _
'                                                                            LstV_OwnerUse.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                            Call SET_Execute2("exec sp_insert_buildingdata_apply '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "-" & Format$(i, "##0#") & "','" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "','" & _
                                                                            LstV_OwnerUse.ListItems(i).ListSubItems(4).Text & "'", 8, False)
'                                                                sql_txt = "INSERT INTO BUILDINGDATA_APPLY( BUILDING_APPLY_ID, BUILDING_ID, OWNERSHIP_ID) "
'                                                                sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "-" & Format$(i, "##0#") & "','"
'                                                                sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_Building_ID.Text & "','"
'                                                                sql_txt = sql_txt & LstV_OwnerUse.ListItems(i).ListSubItems(4).Text & "')"
'                                                               Call SET_Execute(sql_txt)
                                                 Next i
                                            End If
                                     End If
'                                    If IsNull(Rs.Fields(0).Value) = False Then
'                                            Call SET_Execute("UPDATE PRD2 SET PRD2_AMOUNT_ACCEPT=" & CCur(Lb_GrandTotal_Tax.Caption) & " WHERE BUILDING_ID='" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "' AND PRD2_YEAR=" & Rs.Fields(0).Value & " AND FLAG_CHANGE=0 AND PRD2_STATUS=0")
'                                    End If
                                    Call SET_REFRESH
'        Globle_Connective.CommitTrans
        Exit Sub
'        Globle_Connective.RollbackTrans
        Set Rs = Nothing
End Sub

Private Function CheckBeforPost() As Boolean
         CheckBeforPost = True
         Dim i As Byte
     If Len(Cmb_Zoneblock.Text) + Len(Txt_BUILDING_ID.Text) <> 6 Then
                MsgBox "�ô�к������ç���͹���ú 6 ��ѡ !", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
                Exit Function
     End If
     If Trim$(Cmb_Building_Type.Text) = Empty Then
            MsgBox "�ô�к��ѡɳ��ç���͹ !", vbCritical, "�Ӫ��ᨧ !"
           CheckBeforPost = False
          Exit Function
    End If
     
    If Cmb_Land_ID.Text = Empty Then
        MsgBox "�ô�к����ʷ��Թ !", vbCritical, "�Ӫ��ᨧ !"
        CheckBeforPost = False
        Exit Function
    End If
    
    If LstV_OwnerShip.ListItems.Count = 0 Then
         MsgBox "�ô�к� ������Ңͧ�ç���͹ ! ", vbCritical, "�Ӫ��ᨧ !"
         CheckBeforPost = False
         Exit Function
     End If
  
  If LstV_OwnerUse.ListItems.Count = 0 Then
         MsgBox "�ô�к� ���ͼ���ͺ��ͧ�ç���͹ ! ", vbCritical, "�Ӫ��ᨧ !"
         CheckBeforPost = False
         Exit Function
     End If
    
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LstV_OwnerShip.ListItems.Item(i).SubItems(7) = "1" Then
                 CheckBeforPost = True
                 Exit Function
            Else
                 CheckBeforPost = False
           End If
    Next i
                  If CheckBeforPost = False Then
                     MsgBox "�ô�кؼ���ա����Է��줹�á ! ", vbCritical, "�Ӫ��ᨧ "
                     Exit Function
                  End If
                  
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LstV_OwnerShip.ListItems.Item(i).SubItems(8) = Empty Then
                 CheckBeforPost = False
            Else
                 CheckBeforPost = True
                 Exit For
           End If
     Next i
                            If CheckBeforPost = False Then
                                    MsgBox "�ô�кؼ���Ѻ������ !", vbExclamation, "�Ӫ��ᨧ "
                                    Exit Function
                           End If
                  
End Function

Private Sub Btn_Add_Click()
  Call SET_TEXTBOX("ADD", "Perpose")
  Status = "ADD"
  Txt_BUILDING_ID.SetFocus
End Sub

Private Sub Btn_Cancel_Click()
Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If Cmb_Zoneblock.Text = Empty Then Exit Sub
If MsgBox("�׹�ѹ���ź�����ŷ���¹�ç���͹ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
        On Error GoTo ErrHandler
        Globle_Connective.BeginTrans
        Status = "DEL"
        Call SET_DATABASE("DEL")
        Call SET_TEXTBOX("DEL")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Edit_Click()
If Cmb_Zoneblock.Text = Empty Then Exit Sub
Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
Cmb_BuildingType.Tag = Cmb_BuildingType.Text
Cmb_BuildingType_ID.Tag = Cmb_BuildingType_ID.Text
Call Cmb_Tamlay_Click
Cmb_BuildingType.Text = Cmb_BuildingType.Tag
Cmb_BuildingType_ID.Text = Cmb_BuildingType_ID.Tag
Call Cmb_Zoneblock_Click
Call Chk_BUILDING_STATE2_Click
Call Chk_BUILDING_STATE3_Click
End Sub

Private Sub Btn_FullScreen_Click()
   If LstV_Img_Name.ListItems.Count > 0 Then
        GB_ImagePath = LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1)
        Frm_FocusImage.Show vbModal
  End If
End Sub

Private Sub Btn_Post_Click()
If CheckBeforPost = False Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE(Status)
        Call SET_TEXTBOX("POST")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Refresh_Click()
        Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
 With Frm_Search
      .ZOrder
      .Show
        .Shp_Menu(1).BackColor = &HD6D6D6
         .Shp_Menu(2).BackColor = &HD6D6D6
        .Shp_Menu(4).BackColor = &HD6D6D6
        .Shp_Menu(5).BackColor = &HD6D6D6
        .Shp_Menu(3).BackColor = &HB8CFD6
        .SSTab1.Tab = 2

        .optCode.Visible = True
        .optName.Visible = True
        .optCode.Caption = "�����ç���͹"
        .cmd_SendToEdit.Caption = " << ��䢢����� - �ç���͹"
End With
End Sub

Private Sub Btn_SelectImg_Click()
Dim Picture_Path  As String
    With Clone_Form.CommonDialog1
            .DialogTitle = "���͡�ٻ�Ҿ�͡����Է���"
            .CancelError = True
              On Error GoTo ErrHandler
            .InitDir = "C:\"
            .Flags = cdlOFNHideReadOnly
            .Filter = "All Files (*.*)|*.*|Picture Files(*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif"     ' Set filters
            .FilterIndex = 2
            '.Action = 1
            .ShowOpen
             Picture_Path = .FileName
    End With
              If LenB(Trim$(Picture_Path)) <> 0 Then
                 Call ShowPicture(Picture_Path)
           End If
                    Dim i As Byte, File_Name   As String
                    Set itmX = LstV_Img_Name.ListItems.Add()
                       itmX.SmallIcon = Clone_Form.ImageList1.ListImages(2).Index
                       
                       For i = 0 To Len(Picture_Path)
                             If Mid$(Picture_Path, Len(Picture_Path) - i, 1) <> "\" Then
                                 File_Name = File_Name + Mid$(Picture_Path, Len(Picture_Path) - i, 1)
                              Else
                                  File_Name = StrReverse(File_Name)
                                  Exit For
                             End If
                       Next i
                       itmX.Text = File_Name
                       itmX.SubItems(1) = Picture_Path
                       Exit Sub
ErrHandler:
End Sub

Private Sub cb_Building_Change_Click()
        cb_Building_Change_ID.ListIndex = cb_Building_Change.ListIndex
End Sub

Private Sub Chk_Flag_PayTax0_Click()
If Chk_Flag_PayTax0.Value Then
        Chk_Flag_PayTax1.Value = Unchecked
        Lb_GrandTotal_Tax.Caption = "0.00"
Else
        Chk_Flag_PayTax1.Value = Checked
End If
End Sub

Private Sub Chk_Flag_PayTax1_Click()
If Chk_Flag_PayTax1.Value Then
    If Chk_Person.Value = Checked Then Call Txt_RentYear_Total1_Change
   If ChkSystem.Value = Checked Then
        Select Case SSTab2.Tab
                Case 0
                    Call CalOfRent
                Case 1
                    Call CalOfUse(1)
                Case 2
                    Call CalOfUse(2)
                Case 3
                    Call CalOfUse(3)
                Case 4
                    Call CalOfUse(4)
        End Select
   End If
   Chk_Flag_PayTax0.Value = Unchecked
Else
  Chk_Flag_PayTax0.Value = Checked
End If
End Sub

Private Sub Chk_Method1_Click()
        On Error Resume Next
            If Chk_Method1.Value Then
                Txt_Building_Area.Text = Txt_BUILDING_WIDTH * Txt_BUILDING_HEIGHT
                Chk_Method2.Value = Unchecked
                Chk_Method3.Value = Unchecked
                Chk_Method4.Value = Unchecked
            End If
End Sub


Private Sub Chk_Method2_Click()
            If Chk_Method2.Value Then
                Chk_Method1.Value = Unchecked
                Chk_Method3.Value = Unchecked
                Chk_Method4.Value = Unchecked
            End If
End Sub

Private Sub Chk_Method3_Click()
            If Chk_Method3.Value Then
                Chk_Method1.Value = Unchecked
                Chk_Method2.Value = Unchecked
                Chk_Method4.Value = Unchecked
            End If
End Sub

Private Sub Chk_Method4_Click()
            If Chk_Method4.Value Then
                Chk_Method1.Value = Unchecked
                Chk_Method2.Value = Unchecked
                Chk_Method3.Value = Unchecked
            End If
End Sub

Private Sub Cmb_Building_Type_II_Click()
        Cmb_Building_Type_II_ID.ListIndex = Cmb_Building_Type_II.ListIndex
End Sub

Private Sub Cmb_Building_Type_II_KeyPress(KeyAscii As Integer)
Dim strFind As String
Dim lngRet As Long
        
    If KeyAscii = 13 Then
        SendKeys "{Tab}"
    Else
        strFind = Left$(Cmb_Building_Type_II.Text, Cmb_Building_Type_II.SelStart) & Chr$(KeyAscii)
        lngRet = SendMessage(Cmb_Building_Type_II.hWnd, CB_FINDSTRING, -1, ByVal strFind)
        If lngRet <> CB_ERR Then
            Cmb_Building_Type_II.ListIndex = lngRet
            Cmb_Building_Type_II.Text = Cmb_Building_Type_II.List(lngRet)
            Cmb_Building_Type_II.SelStart = Len(strFind)
            Cmb_Building_Type_II.SelLength = Len(Cmb_Building_Type_II.Text)
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub Cmb_Building_Type_KeyPress(KeyAscii As Integer)
Dim strFind As String
Dim lngRet As Long
        
    If KeyAscii = 13 Then
'        Combo1_Click
'        KeyAscii = 0
        SendKeys "{Tab}"
    Else
        strFind = Left$(Cmb_Building_Type.Text, Cmb_Building_Type.SelStart) & Chr$(KeyAscii)
        lngRet = SendMessage(Cmb_Building_Type.hWnd, CB_FINDSTRING, -1, ByVal strFind)
        If lngRet <> CB_ERR Then
'            IgnorarListaClick = True
            Cmb_Building_Type.ListIndex = lngRet
'            IgnorarListaClick = False
            Cmb_Building_Type.Text = Cmb_Building_Type.List(lngRet)
            Cmb_Building_Type.SelStart = Len(strFind)
            Cmb_Building_Type.SelLength = Len(Cmb_Building_Type.Text)
            KeyAscii = 0
'            Data1.RecordSource = "SELECT * FROM Tabla1 WHERE Nombre='" & Cmb_Building_Type_KeyPress.Text & "'"
'            Data1.Refresh
'            Label1.Caption = Data1.Recordset.Fields("DNI")
        End If
    End If
End Sub

Private Sub Cmb_BuildingType_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Cmb_Tamlay_Click()
        If Status <> "EDIT" Then
                LB_TYPE_A_RATE1.Caption = "0.00"
                LB_TYPE_A_RATE2.Caption = "0.00"
                LB_TYPE_A_RATE3.Caption = "0.00"
                LB_TYPE_B_RATE1.Caption = "0.00"
                LB_TYPE_C_RATE1.Caption = "0.00"
                LB_TYPE_D_RATE1.Caption = "0.00"
        End If
        GBQueryBuildingRate.Filter = "ZONETAX_ID = '" & Cmb_Tamlay.Text & "'"
        If ChkSystem.Value = Checked Then Call ChkSystem_Click
        Call Cmb_BuildingType_Click
End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
'        Set QueryZoneBlock = Nothing
        Set QueryBuilding_Rate = Nothing
End Sub

Private Sub Label2_Click(Index As Integer)
 If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
Select Case Index
             Case 29
                        If Chk_BUILDING_STATE2.Value Then
                            Chk_BUILDING_STATE2.Value = Unchecked
                        Else
                           Chk_BUILDING_STATE2.Value = Checked
                        End If
              Case 31
                        If Chk_BUILDING_STATE3.Value Then
                           Chk_BUILDING_STATE3.Value = Unchecked
                        Else
                            Chk_BUILDING_STATE3.Value = Checked
                        End If
              Case 30
                      If Chk_BUILDING_STATE1.Value Then
                          Chk_BUILDING_STATE1.Value = Unchecked
                      Else
                           Chk_BUILDING_STATE1.Value = Checked
                      End If
              Case 54
                      If Chk_Person.Value Then
                          Chk_Person.Value = Unchecked
                      Else
                           Chk_Person.Value = Checked
                      End If
              Case 56
                      If ChkSystem.Value Then
                          ChkSystem.Value = Unchecked
                      Else
                           ChkSystem.Value = Checked
                      End If
              Case 103
                      If Chk_Method1.Value Then
                          Chk_Method1.Value = Unchecked
                      Else
                           Chk_Method1.Value = Checked
                      End If
              Case 104
                      If Chk_Method2.Value Then
                          Chk_Method2.Value = Unchecked
                      Else
                           Chk_Method2.Value = Checked
                      End If
              Case 105
                      If Chk_Method3.Value Then
                          Chk_Method3.Value = Unchecked
                      Else
                           Chk_Method3.Value = Checked
                      End If
              Case 106
                      If Chk_Method4.Value Then
                          Chk_Method4.Value = Unchecked
                      Else
                           Chk_Method4.Value = Checked
                      End If
              Case 75
                      If Chk_Same.Value Then
                          Chk_Same.Value = Unchecked
                      Else
                           Chk_Same.Value = Checked
                      End If
End Select
End Sub

Private Sub Lb_Ownership_Real_ID_Change()
    Dim i As Byte, j As Long
    
    For i = 1 To Len(Lb_Ownership_Real_ID.Caption)
           j = j + TextWidth(Mid$(Lb_Ownership_Real_ID.Caption, i, 1))
    Next i
        If j > 795 Then
                Lb_Ownership_Real_ID.Width = 1725 + (j - 700)
                Shape1(29).Width = 1727 + (j - 698)
        Else
                Lb_Ownership_Real_ID.Width = 1725
                Shape1(29).Width = 1727
        End If
End Sub

Private Sub LB_TYPE_A_AREA_Change()
        If LB_TYPE_A_AREA.Caption = Empty Then LB_TYPE_A_AREA.Caption = "0.00"
End Sub

Private Sub LB_TYPE_A_RATE4_Change()
        If LB_TYPE_A_RATE4.Caption = Empty Then LB_TYPE_A_RATE4.Caption = "0.00"
End Sub

Private Sub Lb_User_Real_ID_Change()
    Dim i As Byte, j As Long
    
    For i = 1 To Len(Lb_User_Real_ID.Caption)
          j = j + TextWidth(Mid$(Lb_User_Real_ID.Caption, i, 1))
     Next i
        If j > 795 Then
           Lb_User_Real_ID.Width = 1725 + (j - 700)
           Shape1(30).Width = 1727 + (j - 698)
        Else
            Lb_User_Real_ID.Width = 1725
            Shape1(30).Width = 1727
        End If
End Sub

Private Sub picPicture_DblClick()
        Call Btn_FullScreen_Click
End Sub

Private Sub Txt_Building_Area_GotFocus()
Txt_Building_Area.SelStart = 0
Txt_Building_Area.SelLength = Len(Txt_Building_Area.Text)
End Sub

Private Sub Txt_Building_Area_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_HOME_ID_GotFocus()
Txt_BUILDING_HOME_ID.SelStart = 0
Txt_BUILDING_HOME_ID.SelLength = Len(Txt_BUILDING_HOME_ID.Text)
End Sub

Private Sub Txt_BUILDING_HOME_NO_GotFocus()
Txt_BUILDING_HOME_NO.SelStart = 0
Txt_BUILDING_HOME_NO.SelLength = Len(Txt_BUILDING_HOME_NO.Text)
End Sub

Private Sub Txt_Building_Name_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Building_NearArea_GotFocus()
Txt_Building_NearArea.SelStart = 0
Txt_Building_NearArea.SelLength = Len(Txt_Building_NearArea.Text)
End Sub

Private Sub Txt_Building_NearArea_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Building_OtherArea_GotFocus()
Txt_Building_OtherArea.SelStart = 0
Txt_Building_OtherArea.SelLength = Len(Txt_Building_OtherArea.Text)
End Sub

Private Sub Txt_Building_OtherArea_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_QTY_B_MONTH_Change()
If Txt_BUILDING_QTY_B_MONTH.Text = Empty Then Txt_BUILDING_QTY_B_MONTH.Text = "0"
 Call CalOfUse(2)
End Sub

Private Sub Chk_BUILDING_FLAG0_Click()
If Chk_BUILDING_FLAG0.Value Then
   Chk_BUILDING_FLAG1.Value = Unchecked
Else
   Chk_BUILDING_FLAG1.Value = Checked
End If
End Sub

Private Sub Chk_BUILDING_STATE2_Click()
 If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
 
 If Chk_BUILDING_STATE2.Value Then
     Txt_BUILDING_STATE_PRICE.Locked = False
     Txt_BUILDING_STATE_OTHER_PRICE.Locked = False
     Txt_BUILDING_STATE_YEAR.Locked = False
     Txt_Building_Name.Locked = False
 Else
     Txt_BUILDING_STATE_PRICE.Text = "0"
     Txt_BUILDING_STATE_OTHER_PRICE.Text = "0"
     Txt_BUILDING_STATE_YEAR.Text = "0"
     
     Txt_BUILDING_STATE_PRICE.Locked = True
     Txt_BUILDING_STATE_OTHER_PRICE.Locked = True
     Txt_BUILDING_STATE_YEAR.Locked = True
     Txt_Building_Name.Locked = True
 End If
 
End Sub

Private Sub Chk_BUILDING_FLAG1_Click()
If Chk_BUILDING_FLAG1.Value Then
   Chk_BUILDING_FLAG0.Value = Unchecked
Else
   Chk_BUILDING_FLAG0.Value = Checked
End If
End Sub

Private Sub Chk_BUILDING_STATE3_Click()
If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
            If Chk_BUILDING_STATE3.Value = Checked Then
                Txt_BUILDING_STATE_DETAILS.Locked = False
            Else
               Txt_BUILDING_STATE_DETAILS.Text = Empty
               Txt_BUILDING_STATE_DETAILS.Locked = True
            End If
End Sub

Private Sub CHK_BUILDING_STATUS0_Click()
If CHK_BUILDING_STATUS0.Value Then
   CHK_BUILDING_STATUS1.Value = Unchecked
Else
   CHK_BUILDING_STATUS1.Value = Checked
End If
End Sub

Private Sub CHK_BUILDING_STATUS1_Click()
If CHK_BUILDING_STATUS1.Value Then
   CHK_BUILDING_STATUS0.Value = Unchecked
Else
   CHK_BUILDING_STATUS0.Value = Checked
End If
End Sub

Private Sub Chk_Person_Click()
Dim i As Byte
If Chk_Person.Value = Checked Then
   Cmb_BuildingType.Locked = True
   combo1.Locked = True
   If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
   ChkSystem.Value = Unchecked
   Chk_Same.Enabled = False
   Chk_Same.Value = Unchecked
   Txt_RentYear_Total1.Locked = False
   Txt_RentSame.Locked = True
   Txt_RentSame.Text = Empty
    For i = 1 To 12
           Txt_Rent_Month(i).Locked = True
           Txt_Rent_Month(i).Text = Empty
    Next i
    
Else
 If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
   ChkSystem.Value = Checked
End If
End Sub

Private Sub Chk_Same_Click()
 If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
Dim i As Byte
If Chk_Same.Value Then
    Txt_RentSame.Locked = False
    For i = 1 To 12
           Txt_Rent_Month(i).Locked = True
    Next i
Else
       Txt_RentSame.Locked = True
       Txt_RentSame.Text = Empty
        For i = 1 To 12
              Txt_Rent_Month(i).Locked = False
              Txt_Rent_Month(i).Text = Empty
        Next i
End If
End Sub

Private Sub ChkSystem_Click()
        If ChkSystem.Value = Checked Then
                Dim i As Byte
                If Chk_Method1.Value <> Checked Then
                        Txt_RentYear_Total1.Text = "0.00"
                        Txt_RentYear_Total_All.Caption = "0"
                End If
                Cmb_BuildingType.Locked = False
                combo1.Locked = False
                If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
                Chk_Person.Value = Unchecked
                Txt_RentYear_Total1.Locked = True
                For i = 1 To 12
                       Txt_Rent_Month(i).Locked = False
                Next i
                Chk_Same.Enabled = True
                QueryBuilding_Rate.Requery
                Cmb_BuildingType.Clear
                Cmb_BuildingType_ID.Clear
                QueryBuilding_Rate.Filter = "ZONETAX_ID ='" & Cmb_Tamlay.Text & "'"
                If QueryBuilding_Rate.RecordCount > 0 Then
                    QueryBuilding_Rate.MoveFirst
                    Do While Not QueryBuilding_Rate.EOF
                            Cmb_BuildingType.AddItem QueryBuilding_Rate.Fields("BUILDING_DETAILS").Value
                            Cmb_BuildingType_ID.AddItem QueryBuilding_Rate.Fields("BUILDING_SHAPE_ID").Value
                            QueryBuilding_Rate.MoveNext
                    Loop
                End If
                If Cmb_BuildingType.ListCount > 0 Then
                        Cmb_BuildingType.ListIndex = 0
                End If
        Else
                If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
                Chk_Person.Value = Checked
                Cmb_BuildingType.Clear
                Cmb_BuildingType_ID.Clear
        End If
End Sub

Private Sub Cmb_BuildingType_Click()
'        Chk_Method1.Value = Unchecked
'        Chk_Method2.Value = Unchecked
'        Chk_Method3.Value = Unchecked
'        Chk_Method4.Value = Unchecked
        Cmb_BuildingType_ID.ListIndex = Cmb_BuildingType.ListIndex
        If Chk_Person.Value Then Exit Sub

        With GBQueryBuildingRate
             If .RecordCount > 0 Then
                    .MoveFirst
                    .Find " BUILDING_SHAPE_ID = '" & Cmb_BuildingType_ID.Text & "'", , adSearchForward
                 If .AbsolutePage > 0 Then
                            LB_TYPE_A_RATE1.Caption = .Fields("TYPE_A_RATE1").Value
                            LB_TYPE_A_RATE2.Caption = .Fields("TYPE_A_RATE2").Value
                            LB_TYPE_A_RATE3.Caption = .Fields("TYPE_A_RATE3").Value
                            LB_TYPE_B_RATE1.Caption = .Fields("TYPE_B_RATE1").Value
                            LB_TYPE_C_RATE1.Caption = .Fields("TYPE_C_RATE1").Value
                            LB_TYPE_D_RATE1.Caption = .Fields("TYPE_D_RATE1").Value
                            LB_TYPE_A_RATE4.Caption = .Fields("TYPE_A_RATE4").Value
                            LB_TYPE_A_AREA.Caption = .Fields("TYPE_A_AREA").Value
               End If
             End If
        End With
        Lb_GrandTotal_Tax.Caption = Format$(CCur(Lb_RentSumTax.Caption), "0.00")
End Sub

Private Sub Cmb_Land_ID_Click()
        With GBQueryLandData
                .Requery
                .Filter = " LAND_ID =  '" & Cmb_Land_ID.Text & "'"
                If .RecordCount > 0 Then
                        .MoveFirst
                        Lb_Tambon.Caption = .Fields("TAMBON_NAME").Value
                        Lb_Village.Caption = .Fields("VILLAGE_NAME").Value
                        Lb_Soi.Caption = .Fields("SOI_NAME").Value
                        Lb_Street.Caption = .Fields("STREET_NAME").Value
                        Lb_Land_Notic.Caption = .Fields("LAND_NOTIC").Value
                        Lb_Type_Land.Caption = Land_Type(.Fields("LAND_TYPE").Value)
                End If
        End With
End Sub

Private Sub Cmb_Land_ID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    'KeyAscii = 0
End Sub

Private Sub Cmb_Tamlay_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Cmb_Zoneblock_Click()
If Status = "ADD" Then
            Lb_Type_Land.Caption = Empty
            Lb_Tambon.Caption = Empty
            Lb_Village.Caption = Empty
            Lb_Soi.Caption = Empty
            Lb_Street.Caption = Empty
            Lb_Land_Notic.Caption = Empty
            Cmb_Land_ID.Clear
End If

            GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
            
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
    Do While Not GBQueryLandData.EOF
                  If GBQueryLandData.Fields("ZONE_BLOCK").Value <> Empty Then
                        Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                  End If
                       GBQueryLandData.MoveNext
    Loop
End If
End Sub

Private Sub Cmb_Zoneblock_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Combo1_Click()
                 If Chk_Person.Value Then Exit Sub
   Select Case combo1.ListIndex
                 Case 0
                                 SSTab2.Tab = 0
                                 Lb_GrandTotal_Tax.Caption = Format$(CDbl(Lb_UseSumTax1.Caption) + CDbl(Lb_RentSumTax.Caption), "0.00")
                 Case 1
                                 SSTab2.Tab = 1
                                 Lb_GrandTotal_Tax.Caption = Format$(CDbl(Lb_UseSumTax1.Caption) + CDbl(Lb_RentSumTax.Caption), "0.00")
                 Case 2
                                SSTab2.Tab = 2
                                Lb_GrandTotal_Tax.Caption = Format$(CDbl(Lb_UseSumTax2.Caption) + CDbl(Lb_RentSumTax.Caption), "0.00")
                 Case 3
                                 SSTab2.Tab = 3
                                 Lb_GrandTotal_Tax.Caption = Format$(CDbl(Lb_UseSumTax3.Caption) + CDbl(Lb_RentSumTax.Caption), "0.00")
                 Case 4
                                SSTab2.Tab = 4
                                Lb_GrandTotal_Tax.Caption = Format$(CDbl(Lb_UseSumTax4.Caption) + CDbl(Lb_RentSumTax.Caption), "0.00")
   End Select
End Sub
Private Sub Form_Activate()
Clone_Form.Picture1.Visible = True
Me.WindowState = vbMaximized
End Sub

Private Sub Form_Initialize()
OpMenu(1).Top = 4920
OpMenu(2).Top = 4920
OpMenu(3).Top = 4920
LstV_OwnerShip.Icons = Clone_Form.ImageList1
LstV_OwnerShip.SmallIcons = Clone_Form.ImageList1

LstV_OwnerUse.Icons = Clone_Form.ImageList1
LstV_OwnerUse.SmallIcons = Clone_Form.ImageList1

LstV_Img_Name.Icons = Clone_Form.ImageList1
LstV_Img_Name.SmallIcons = Clone_Form.ImageList1
End Sub

Private Sub Form_Load()
'    Set QueryZoneBlock = New ADODB.Recordset
    Set QueryBuilding_Rate = New ADODB.Recordset
   Clone_Form.mnuPopup2.Visible = False
    Image1(4).Left = 20: Image1(4).Top = 0
    Image1(5).Left = 20: Image1(5).Top = 0
    Image1(6).Left = 20: Image1(6).Top = 0
    Image1(7).Left = 20: Image1(7).Top = 0
    Image1(8).Left = 20: Image1(8).Top = 0
   Call SET_REFRESH
    Image1(1).Picture = LoadResPicture(999, 0)
    Image1(2).Picture = LoadResPicture(999, 0)
    Image1(3).Picture = LoadResPicture(999, 0)
    Image1(4).Picture = LoadResPicture(998, 0)
     Image1(5).Picture = LoadResPicture(998, 0)
    Image1(6).Picture = LoadResPicture(998, 0)
    Image1(7).Picture = LoadResPicture(998, 0)
    Image1(8).Picture = LoadResPicture(998, 0)
    Chk_BUILDING_FLAG0.Value = Checked
    CHK_BUILDING_STATUS0.Value = Checked
    Set QueryBuilding_Rate = Globle_Connective.Execute("exec sp_QueryBuilding_Rate", , adCmdUnknown)
'    If GBQueryBuildingType.RecordCount > 0 Then
'            GBQueryBuildingType.MoveFirst
'    Do While Not GBQueryBuildingType.EOF
'                  If GBQueryBuildingType.Fields("BUILDING_DETAILS").Value <> Empty Then
'                       Cmb_Building_Type.AddItem GBQueryBuildingType.Fields("BUILDING_DETAILS").Value
'                  End If
'                    GBQueryBuildingType.MoveNext
'    Loop
'End If
End Sub

Private Sub Lb_GrandTotal_Tax_Change()
 If Lb_GrandTotal_Tax.Caption = Empty Then Lb_GrandTotal_Tax.Caption = "0.00"
 If Chk_Flag_PayTax0.Value And Chk_Flag_PayTax1.Value = Unchecked Then Lb_GrandTotal_Tax.Caption = "0.00"
    ' Lb_GrandTotal_Tax.Caption = FormatNumber(CDbl(Lb_GrandTotal_Tax.Caption), 2)
End Sub

Private Sub Lb_RentSumTax_Change()
 If Lb_RentSumTax.Caption = Empty Then Lb_RentSumTax.Caption = "0.00"
End Sub

Private Sub LB_RentYear_Total2_Change()
 If LB_RentYear_Total2.Caption = Empty Then LB_RentYear_Total2.Caption = "0.00"
End Sub

Private Sub LB_RentYear_Total3_Change()
If LB_RentYear_Total3.Caption = Empty Then LB_RentYear_Total3.Caption = "0.00"
End Sub

Private Sub LB_RentYear_Total4_Change()
If LB_RentYear_Total4.Caption = Empty Then LB_RentYear_Total4.Caption = "0.00"
End Sub

Private Sub LB_RentYear_Total5_Change()
If LB_RentYear_Total5.Caption = Empty Then LB_RentYear_Total5.Caption = "0.00"
End Sub

Private Sub LB_RentYear_Total6_Change()
If LB_RentYear_Total6.Caption = Empty Then LB_RentYear_Total6.Caption = "0.00"
End Sub

Private Sub LB_TYPE_A_RATE1_Change()
If LB_TYPE_A_RATE1.Caption = Empty Then LB_TYPE_A_RATE1.Caption = "0.00"
Call CalOfUse(1)
End Sub

Private Sub LB_TYPE_A_RATE1_SUM_Change()
If LB_TYPE_A_RATE1_SUM.Caption = Empty Then LB_TYPE_A_RATE1_SUM.Caption = "0.00"
End Sub

Private Sub LB_TYPE_A_RATE2_Change()
If LB_TYPE_A_RATE2.Caption = Empty Then LB_TYPE_A_RATE2.Caption = "0.00"
Call CalOfUse(1)
End Sub

Private Sub LB_TYPE_A_RATE2_SUM_Change()
If LB_TYPE_A_RATE2_SUM.Caption = Empty Then LB_TYPE_A_RATE2_SUM.Caption = "0.00"
End Sub

Private Sub LB_TYPE_A_RATE3_Change()
If LB_TYPE_A_RATE3.Caption = Empty Then LB_TYPE_A_RATE3.Caption = "0.00"
Call CalOfUse(1)
End Sub

Private Sub LB_TYPE_A_RATE3_SUM_Change()
If LB_TYPE_A_RATE3_SUM.Caption = Empty Then LB_TYPE_A_RATE3_SUM.Caption = "0.00"
End Sub

Private Sub LB_TYPE_B_RATE1_Change()
If LB_TYPE_B_RATE1.Caption = Empty Then LB_TYPE_B_RATE1.Caption = "0.00"
Call CalOfUse(2)
End Sub

Private Sub LB_TYPE_C_RATE1_Change()
If LB_TYPE_C_RATE1.Caption = Empty Then LB_TYPE_C_RATE1.Caption = "0.00"
Call CalOfUse(3)
End Sub

Private Sub LB_TYPE_D_RATE1_Change()
If LB_TYPE_D_RATE1.Caption = Empty Then LB_TYPE_D_RATE1.Caption = "0.00"
End Sub

Private Sub Lb_UseSumTax1_Change()
 If Lb_UseSumTax1.Caption = Empty Then Lb_UseSumTax1.Caption = "0.00"
End Sub

Private Sub Lb_UseSumTax2_Change()
 If Lb_UseSumTax2.Caption = Empty Then Lb_UseSumTax2.Caption = "0.00"
End Sub
Private Sub Lb_UseSumTax3_Change()
 If Lb_UseSumTax3.Caption = Empty Then Lb_UseSumTax3.Caption = "0.00"
End Sub
Private Sub Lb_UseSumTax4_Change()
 If Lb_UseSumTax4.Caption = Empty Then Lb_UseSumTax4.Caption = "0.00"
 Call CalOfUse(4)
End Sub

Private Sub LstV_Img_Name_ItemClick(ByVal Item As MSComctlLib.ListItem)
If LstV_Img_Name.ListItems.Count > 0 Then Call ShowPicture(LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1))
End Sub

Private Sub LstV_Img_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
        Clone_Form.mnuAddName2.Visible = False
        Clone_Form.mnuDelName2.Visible = False
        Clone_Form.mnuSetOwner2.Visible = False
        Clone_Form.mnuSetOwnerMail2.Visible = False
        Clone_Form.mnuDelImg2.Visible = True
        PopupMenu Clone_Form.mnuPopup2, vbPopupMenuLeftAlign, LstV_Img_Name.Left + X, LstV_Img_Name.Top + Me.Top + y
        picPicture.Picture = LoadPicture("")
        picPicture.Width = 2235
        picPicture.Height = 2085
   End If
   End If
End Sub

Private Sub LstV_OwnerShip_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   If LstV_OwnerShip.SortOrder = lvwAscending Then
      LstV_OwnerShip.SortOrder = lvwDescending
   Else
     LstV_OwnerShip.SortOrder = lvwAscending
   End If
    LstV_OwnerShip.SortKey = ColumnHeader.Index - 1
    LstV_OwnerShip.Sorted = True
End Sub
Private Sub mnuSetOwner_Click()
With LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
                            .ListItems.Item(i).SubItems(7) = "0"
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
        End If
End With
End Sub
Private Sub LstV_OwnerShip_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Lb_Owner_Type.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index)
    Lb_Owner_Name.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(1)
    Lb_Owner_Address.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(2)
    Lb_Email.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(3)
    Lb_Ownership_Real_ID.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(5)
End Sub

Private Sub LstV_OwnerShip_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            OrderListView = 1
            Clone_Form.mnuDelImg2.Visible = False
            Clone_Form.mnuAddName2.Visible = True
            Clone_Form.mnuDelName2.Visible = True
            Clone_Form.mnuSetOwner2.Visible = True
            Clone_Form.mnuSetOwnerMail2.Visible = True
           PopupMenu Clone_Form.mnuPopup2, vbPopupMenuLeftAlign, LstV_OwnerShip.Left + X, LstV_OwnerShip.Top + Me.Top + y + SSTab1.Top
   End If
End If
End Sub

Private Sub LstV_OwnerUse_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   With LstV_OwnerUse
   If .SortOrder = lvwAscending Then
      .SortOrder = lvwDescending
   Else
     .SortOrder = lvwAscending
   End If
    .SortKey = ColumnHeader.Index - 1
    .Sorted = True
End With
End Sub

Private Sub LstV_OwnerUse_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Lb_User_Type.Caption = LstV_OwnerUse.ListItems.Item(LstV_OwnerUse.SelectedItem.Index)
    Lb_User_Name.Caption = LstV_OwnerUse.ListItems.Item(LstV_OwnerUse.SelectedItem.Index).SubItems(1)
    Lb_User_Address.Caption = LstV_OwnerUse.ListItems.Item(LstV_OwnerUse.SelectedItem.Index).SubItems(2)
    Lb_User_Email.Caption = LstV_OwnerUse.ListItems.Item(LstV_OwnerUse.SelectedItem.Index).SubItems(3)
    Lb_User_Real_ID.Caption = LstV_OwnerUse.ListItems.Item(LstV_OwnerUse.SelectedItem.Index).SubItems(5)
End Sub

Private Sub LstV_OwnerUse_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            OrderListView = 2
            Clone_Form.mnuDelImg2.Visible = False
            Clone_Form.mnuAddName2.Visible = True
            Clone_Form.mnuDelName2.Visible = True
            Clone_Form.mnuSetOwner2.Visible = False
            Clone_Form.mnuSetOwnerMail2.Visible = False
           PopupMenu Clone_Form.mnuPopup2, vbPopupMenuLeftAlign, LstV_OwnerUse.Left + X, LstV_OwnerUse.Top + Me.Top + y + SSTab1.Top
   End If
   End If
End Sub

Private Sub mnuAddName_Click()
If OrderListView = 1 Then
            Status_InToFrm = "BD_OWN"
            Frm_SearchOwnerShip.Show
End If
If OrderListView = 2 Then
    Status_InToFrm = "BD_APY"
    Frm_SearchOwnerShip.Show
End If
End Sub

Private Sub mnuDelImg_Click()
If LstV_Img_Name.ListItems.Count > 0 Then
    LstV_Img_Name.ListItems.Remove (LstV_Img_Name.SelectedItem.Index)
    picPicture.Cls
    picPicture.Left = 0
    picPicture.Top = 0
End If
End Sub

Private Sub mnuDelName_Click()
If OrderListView = 1 Then
        If LstV_OwnerShip.ListItems.Count > 0 Then
            LstV_OwnerShip.ListItems.Remove (LstV_OwnerShip.SelectedItem.Index)
                          Lb_Ownership_Real_ID.Caption = Empty
                          Lb_Owner_Type.Caption = Empty
                          Lb_Owner_Name.Caption = Empty
                          Lb_Owner_Address.Caption = Empty
                          Lb_Email.Caption = Empty
        End If
End If
If OrderListView = 2 Then
        If LstV_OwnerUse.ListItems.Count > 0 Then
            LstV_OwnerUse.ListItems.Remove (LstV_OwnerUse.SelectedItem.Index)
                          Lb_User_Real_ID.Caption = Empty
                          Lb_User_Type.Caption = Empty
                          Lb_User_Name.Caption = Empty
                          Lb_User_Address.Caption = Empty
                          Lb_User_Email.Caption = Empty
        End If
End If
End Sub

Private Sub mnuSetOwnerMail_Click()
With LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(8) = Empty
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
        End If
End With
End Sub

Private Sub OpMenu_Click(Index As Integer)
 Select Case Index
              Case 1
                    SSTab1.Tab = 0
               Case 2
                    SSTab1.Tab = 1
               Case 3
                    SSTab1.Tab = 2
 End Select
End Sub

Private Sub picParent_DblClick()
Call Btn_FullScreen_Click
End Sub

Private Sub Txt_Building_Area_Change()
   If Txt_Building_Area.Text = Empty Then Txt_Building_Area.Text = "0"
   If ChkSystem Then Call CalOfUse(1)
End Sub

Private Sub Txt_BUILDING_FLOOR_Change()
If Txt_BUILDING_FLOOR.Text = Empty Then Txt_BUILDING_FLOOR.Text = "0"
End Sub

Private Sub Txt_BUILDING_FLOOR_GotFocus()
Txt_BUILDING_FLOOR.SelStart = 0
Txt_BUILDING_FLOOR.SelLength = Len(Txt_BUILDING_FLOOR.Text)
End Sub

Private Sub Txt_BUILDING_FLOOR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
End Sub

Private Sub Txt_BUILDING_HEIGHT_Change()
If Txt_BUILDING_HEIGHT.Text = Empty Then Txt_BUILDING_HEIGHT.Text = "0"
   Txt_Building_Area.Text = Format$(CDbl(Txt_BUILDING_HEIGHT.Text) * CDbl(Txt_BUILDING_WIDTH.Text), "0.00")
   Txt_BUILDING_SUMAREA.Text = Txt_Building_Area.Text
End Sub

Private Sub Txt_BUILDING_HEIGHT_GotFocus()
Txt_BUILDING_HEIGHT.SelStart = 0
Txt_BUILDING_HEIGHT.SelLength = Len(Txt_BUILDING_HEIGHT.Text)
End Sub

Private Sub Txt_BUILDING_HEIGHT_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_HOME_ID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BUILDING_HOME_NO_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Building_ID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Building_ID_Validate(Cancel As Boolean)
    If Status = "ADD" Then
            Cancel = False
            If Len(Txt_BUILDING_ID.Text) = 3 Then
                    GBQueryBuildingData.Requery
                    GBQueryBuildingData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
                    If GBQueryBuildingData.RecordCount > 0 Then
                             GBQueryBuildingData.MoveFirst
                             GBQueryBuildingData.Find " BUILDING_ID  =  '" & Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text & "'"
                            If GBQueryBuildingData.AbsolutePosition > 0 Or Chk_ID_IN_Tax(Cmb_Zoneblock.Text & Txt_BUILDING_ID.Text, 1) = False Then
                                 MsgBox "���ҧ�����ç���͹��Ӣ��������  �ô����¹�����ç���͹���� !", vbCritical, "Warning!"
                                 Cancel = True ' = �͡�����
                            Else
                                  Cancel = False
                            End If
                    End If
            End If
    End If
End Sub

Private Sub Txt_Building_NearArea_Change()
   If Txt_Building_NearArea.Text = Empty Then Txt_Building_NearArea.Text = "0"
      Call CalOfUse(1)
End Sub

Private Sub Txt_Building_OtherArea_Change()
   If Txt_Building_OtherArea.Text = Empty Then Txt_Building_OtherArea.Text = "0"
      Call CalOfUse(1)
End Sub

Private Sub Txt_BUILDING_PRICE_Change()
   If Txt_BUILDING_PRICE.Text = Empty Then Txt_BUILDING_PRICE.Text = "0.00"
End Sub

Private Sub Txt_BUILDING_PRICE_GotFocus()
Txt_BUILDING_PRICE.SelStart = 0
Txt_BUILDING_PRICE.SelLength = Len(Txt_BUILDING_PRICE.Text)
End Sub

Private Sub Txt_BUILDING_PRICE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_QTY_B_MONTH_GotFocus()
Txt_BUILDING_QTY_B_MONTH.SelStart = 0
Txt_BUILDING_QTY_B_MONTH.SelLength = Len(Txt_BUILDING_QTY_B_MONTH.Text)
End Sub

Private Sub Txt_BUILDING_QTY_B_MONTH_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_BUILDING_QTY_C_MONTH_Change()
If Txt_BUILDING_QTY_C_MONTH.Text = Empty Then Txt_BUILDING_QTY_C_MONTH.Text = "0"
 Call CalOfUse(3)
End Sub

Private Sub Txt_BUILDING_QTY_C_MONTH_GotFocus()
Txt_BUILDING_QTY_C_MONTH.SelStart = 0
Txt_BUILDING_QTY_C_MONTH.SelLength = Len(Txt_BUILDING_QTY_C_MONTH.Text)
End Sub


Private Sub Txt_BUILDING_QTY_C_MONTH_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub


Private Sub Txt_BUILDING_QTY_D_DAY_Change()
If Txt_BUILDING_QTY_D_DAY.Text = Empty Then Txt_BUILDING_QTY_D_DAY.Text = "0"
 Call CalOfUse(4)
End Sub

Private Sub Txt_BUILDING_QTY_D_DAY_GotFocus()
Txt_BUILDING_QTY_D_DAY.SelStart = 0
Txt_BUILDING_QTY_D_DAY.SelLength = Len(Txt_BUILDING_QTY_D_DAY.Text)
End Sub
Private Sub Txt_BUILDING_QTY_D_DAY_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub
Private Sub Txt_BUILDING_ROOM_Change()
If Txt_BUILDING_ROOM.Text = Empty Then Txt_BUILDING_ROOM.Text = "0"
End Sub

Private Sub Txt_BUILDING_ROOM_GotFocus()
Txt_BUILDING_ROOM.SelStart = 0
Txt_BUILDING_ROOM.SelLength = Len(Txt_BUILDING_ROOM.Text)
End Sub

Private Sub Txt_BUILDING_ROOM_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_BUILDING_STATE_DETAILS_Change()
If Txt_BUILDING_STATE_DETAILS.Text = Empty Then Txt_BUILDING_STATE_DETAILS.Text = "-"
End Sub

Private Sub Txt_BUILDING_STATE_DETAILS_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BUILDING_STATE_OTHER_PRICE_Change()
  If Txt_BUILDING_STATE_OTHER_PRICE.Text = Empty Then Txt_BUILDING_STATE_OTHER_PRICE.Text = "0.00"
End Sub

Private Sub Txt_BUILDING_STATE_OTHER_PRICE_GotFocus()
Txt_BUILDING_STATE_OTHER_PRICE.SelStart = 0
Txt_BUILDING_STATE_OTHER_PRICE.SelLength = Len(Txt_BUILDING_STATE_OTHER_PRICE.Text)
End Sub

Private Sub Txt_BUILDING_STATE_OTHER_PRICE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_STATE_PRICE_Change()
If Txt_BUILDING_STATE_PRICE.Text = Empty Then Txt_BUILDING_STATE_PRICE.Text = "0.00"
End Sub

Private Sub Txt_BUILDING_STATE_PRICE_GotFocus()
Txt_BUILDING_STATE_PRICE.SelStart = 0
Txt_BUILDING_STATE_PRICE.SelLength = Len(Txt_BUILDING_STATE_PRICE.Text)
End Sub

Private Sub Txt_BUILDING_STATE_PRICE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_STATE_REMARK_Change()
If Txt_BUILDING_STATE_REMARK.Text = Empty Then Txt_BUILDING_STATE_REMARK.Text = "-"
End Sub

Private Sub Txt_BUILDING_STATE_REMARK_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BUILDING_STATE_YEAR_Change()
   If Txt_BUILDING_STATE_YEAR.Text = Empty Then Txt_BUILDING_STATE_YEAR.Text = "0"
End Sub

Private Sub Txt_BUILDING_STATE_YEAR_GotFocus()
Txt_BUILDING_STATE_YEAR.SelStart = 0
Txt_BUILDING_STATE_YEAR.SelLength = Len(Txt_BUILDING_STATE_YEAR.Text)
End Sub

Private Sub Txt_BUILDING_STATE_YEAR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_BUILDING_SUMAREA_Change()
   If Txt_BUILDING_SUMAREA.Text = Empty Then Txt_BUILDING_SUMAREA.Text = "0"
           If ChkSystem Then Call CalOfUse(3)
End Sub

Private Sub Txt_BUILDING_SUMAREA_GotFocus()
Txt_BUILDING_SUMAREA.SelStart = 0
Txt_BUILDING_SUMAREA.SelLength = Len(Txt_BUILDING_SUMAREA.Text)
End Sub


Private Sub Txt_BUILDING_SUMAREA_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub


Private Sub Txt_BUILDING_TOTAL_ROOM_TYPEB_Change()
   If Txt_BUILDING_TOTAL_ROOM_TYPEB.Text = Empty Then Txt_BUILDING_TOTAL_ROOM_TYPEB.Text = "0"
        Call CalOfUse(2)
End Sub

Private Sub Txt_BUILDING_TOTAL_ROOM_TYPEB_GotFocus()
Txt_BUILDING_TOTAL_ROOM_TYPEB.SelStart = 0
Txt_BUILDING_TOTAL_ROOM_TYPEB.SelLength = Len(Txt_BUILDING_TOTAL_ROOM_TYPEB.Text)
End Sub


Private Sub Txt_BUILDING_TOTAL_ROOM_TYPEB_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub


Private Sub Txt_BUILDING_TOTAL_ROOM_TYPED_Change()
   If Txt_BUILDING_TOTAL_ROOM_TYPED.Text = Empty Then Txt_BUILDING_TOTAL_ROOM_TYPED.Text = "0"
           Call CalOfUse(4)
End Sub

Private Sub Txt_BUILDING_TOTAL_ROOM_TYPED_GotFocus()
Txt_BUILDING_TOTAL_ROOM_TYPED.SelStart = 0
Txt_BUILDING_TOTAL_ROOM_TYPED.SelLength = Len(Txt_BUILDING_TOTAL_ROOM_TYPED.Text)
End Sub


Private Sub Txt_BUILDING_TOTAL_ROOM_TYPED_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub


'Private Sub Txt_BUILDING_TYPE_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

Private Sub Txt_BUILDING_WIDTH_Change()
  If Txt_BUILDING_WIDTH.Text = Empty Then Txt_BUILDING_WIDTH.Text = "0"
     Txt_Building_Area.Text = Format$(CDbl(Txt_BUILDING_HEIGHT.Text) * CDbl(Txt_BUILDING_WIDTH.Text), "0.00")
     Txt_BUILDING_SUMAREA.Text = Txt_Building_Area.Text
End Sub

Private Sub Txt_BUILDING_WIDTH_GotFocus()
Txt_BUILDING_WIDTH.SelStart = 0
Txt_BUILDING_WIDTH.SelLength = Len(Txt_BUILDING_WIDTH.Text)
End Sub

Private Sub Txt_BUILDING_WIDTH_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Percent_Change()
        On Error Resume Next
        If CInt(Trim$(Txt_Percent.Text)) > 100 Then Txt_Percent.Text = "100"
        Call CalOfUse(1)
End Sub

Private Sub Txt_Percent_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Rent_Month_Change(Index As Integer)
 If Txt_Rent_Month(Index).Text = Empty Then Txt_Rent_Month(Index).Text = "0.00"
 Call CalOfRent
End Sub

Private Sub Txt_Rent_Month_GotFocus(Index As Integer)
Txt_Rent_Month(Index).SelStart = 0
Txt_Rent_Month(Index).SelLength = Len(Txt_Rent_Month(Index).Text)
End Sub

Private Sub Txt_Rent_Month_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_RentSame_Change()
       Dim i As Byte
       If Txt_RentSame.Text = Empty Then Txt_RentSame.Text = "0.00"
        For i = 1 To 12
              Txt_Rent_Month(i).Text = Txt_RentSame.Text
        Next i
         Call CalOfRent
End Sub

Private Sub Txt_RentSame_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_RentYear_Total_All_Change()
If Txt_RentYear_Total_All.Caption = Empty Then Txt_RentYear_Total_All.Caption = "0.00"
End Sub

Private Sub Txt_RentYear_Total1_Change()
If Txt_RentYear_Total1.Text = Empty Then Txt_RentYear_Total1.Text = "0.00"
If Chk_Person.Value Then Lb_GrandTotal_Tax.Caption = Format$((CDbl(Txt_RentYear_Total1.Text) * 12.5) / 100, "0.00")
Txt_RentYear_Total_All.Caption = Txt_RentYear_Total1.Text
End Sub

Private Sub Txt_RentYear_Total1_GotFocus()
Txt_RentYear_Total1.SelStart = 0
Txt_RentYear_Total1.SelLength = Len(Txt_RentYear_Total1.Text)
End Sub

Private Sub Txt_RentYear_Total1_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_StoryRemark_Change()
If Txt_StoryRemark.Text = Empty Then Txt_StoryRemark.Text = "-"
End Sub
