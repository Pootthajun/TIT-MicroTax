VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Payment 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   ClientHeight    =   9015
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12705
   ControlBox      =   0   'False
   Icon            =   "Frm_Payment.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Payment.frx":151A
   ScaleHeight     =   9015
   ScaleWidth      =   12705
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   3765
      Left            =   120
      TabIndex        =   50
      Top             =   3780
      Visible         =   0   'False
      Width           =   12525
      Begin VB.TextBox Txt_BOOK_NUMBER2 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   7650
         MaxLength       =   8
         TabIndex        =   59
         Top             =   390
         Width           =   1665
      End
      Begin VB.TextBox Txt_BOOK_NO2 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   10560
         MaxLength       =   12
         TabIndex        =   60
         Top             =   390
         Width           =   1905
      End
      Begin VB.TextBox Txt_RoundMoney_Installment 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   405
         Left            =   10560
         TabIndex        =   61
         Text            =   "0.00"
         Top             =   1230
         Width           =   1905
      End
      Begin VB.ComboBox Combo3 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "Frm_Payment.frx":F947
         Left            =   4260
         List            =   "Frm_Payment.frx":F957
         Style           =   2  'Dropdown List
         TabIndex        =   84
         Top             =   3300
         Width           =   1665
      End
      Begin VB.CheckBox Chk_RoundMoney2 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   0
         Left            =   7170
         TabIndex        =   83
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox Txt_Money_Installment 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   405
         Left            =   10560
         TabIndex        =   82
         Text            =   "0.00"
         Top             =   2490
         Width           =   1905
      End
      Begin VB.TextBox Txt_Money_Installment_Pay 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   405
         Left            =   10560
         TabIndex        =   80
         Text            =   "0.00"
         Top             =   3300
         Width           =   1905
      End
      Begin VB.CheckBox Chk_RoundMoney2 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   1
         Left            =   3810
         TabIndex        =   79
         Top             =   3390
         Width           =   315
      End
      Begin VB.ComboBox Combo2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "Frm_Payment.frx":F98D
         Left            =   7650
         List            =   "Frm_Payment.frx":F99D
         Style           =   2  'Dropdown List
         TabIndex        =   62
         Top             =   1230
         Width           =   1695
      End
      Begin VB.TextBox Txt_Summary_Installment 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   405
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   70
         Text            =   "0.00"
         Top             =   810
         Width           =   1905
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Installment 
         Height          =   1245
         Left            =   0
         TabIndex        =   52
         Tag             =   "&H00D6D6D6&"
         Top             =   2010
         Width           =   9105
         _ExtentX        =   16060
         _ExtentY        =   2196
         _Version        =   393216
         BackColor       =   -2147483624
         Rows            =   4
         Cols            =   7
         FixedCols       =   0
         BackColorFixed  =   8421504
         ForeColorFixed  =   16777215
         BackColorSel    =   16777215
         ForeColorSel    =   8079449
         BackColorBkg    =   12632256
         GridColor       =   9806502
         GridColorFixed  =   4210752
         FocusRect       =   0
         ScrollBars      =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   7
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSMask.MaskEdBox Mask_Installment_Start 
         Height          =   405
         Left            =   7650
         TabIndex        =   58
         Top             =   810
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   714
         _Version        =   393216
         AllowPrompt     =   -1  'True
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox Mask_Installment_Pay 
         Height          =   405
         Left            =   10560
         TabIndex        =   73
         Top             =   2070
         Width           =   1905
         _ExtentX        =   3360
         _ExtentY        =   714
         _Version        =   393216
         AllowPrompt     =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ѵ��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Index           =   30
         Left            =   3000
         TabIndex        =   87
         Top             =   3375
         Width           =   810
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ѵ��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Index           =   29
         Left            =   6390
         TabIndex        =   86
         Top             =   1320
         Width           =   810
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���շ����� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   28
         Left            =   9435
         TabIndex        =   85
         Top             =   1320
         Width           =   1065
      End
      Begin VB.Label Lb_MoneyDue_Installment 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   10560
         TabIndex        =   81
         Top             =   2910
         Width           =   1905
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ�Թ�Ǵ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   27
         Left            =   9210
         TabIndex        =   78
         Top             =   2580
         Width           =   1275
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��һ�Ѻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   26
         Left            =   9810
         TabIndex        =   77
         Top             =   3000
         Width           =   675
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ�Թ���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   25
         Left            =   9120
         TabIndex        =   76
         Top             =   3420
         Width           =   1365
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "������� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   23
         Left            =   7020
         TabIndex        =   75
         Top             =   510
         Width           =   540
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   22
         Left            =   9960
         TabIndex        =   74
         Top             =   510
         Width           =   540
      End
      Begin VB.Label Lb_Installment_No 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   990
         TabIndex        =   72
         Top             =   3300
         Width           =   1575
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��Ť������ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   21
         Left            =   9615
         TabIndex        =   71
         Top             =   930
         Width           =   885
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Թ����/Ŵ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   20
         Left            =   3720
         TabIndex        =   69
         Top             =   510
         Width           =   1050
      End
      Begin VB.Label Lb_discount2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   405
         Left            =   4830
         TabIndex        =   68
         Top             =   390
         Width           =   1665
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��һ�Ѻ�����Թ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   19
         Left            =   480
         TabIndex        =   67
         Top             =   1320
         Width           =   1305
      End
      Begin VB.Label Lb_MoneyDue2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   405
         Left            =   1890
         TabIndex        =   66
         Top             =   1230
         Width           =   1665
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��һ�Ѻ���Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   17
         Left            =   540
         TabIndex        =   65
         Top             =   930
         Width           =   1245
      End
      Begin VB.Label Lb_MoneyDue_Perform2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   405
         Left            =   1890
         TabIndex        =   64
         Top             =   810
         Width           =   1665
      End
      Begin VB.Label Lb_Number_Installment 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   405
         Left            =   1890
         TabIndex        =   63
         Top             =   390
         Width           =   1665
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ�駢ͪ����繧Ǵ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   14
         Left            =   5625
         TabIndex        =   57
         Top             =   930
         Width           =   1935
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ������ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   13
         Left            =   9570
         TabIndex        =   56
         Top             =   2160
         Width           =   900
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ǵ��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   12
         Left            =   330
         TabIndex        =   55
         Top             =   3360
         Width           =   585
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ�������繧Ǵ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   9
         Left            =   210
         TabIndex        =   54
         Top             =   510
         Width           =   1590
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00D1ADAD&
         Caption         =   "��¡�çǴ��������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   0
         TabIndex        =   53
         Top             =   1710
         Width           =   12525
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00D1ADAD&
         Caption         =   "��������´����Ѻ���������觨����繧Ǵ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   51
         Top             =   0
         Width           =   12525
      End
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00E0E0E0&
      Height          =   555
      Left            =   6570
      Picture         =   "Frm_Payment.frx":F9D3
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   8490
      Width           =   2775
   End
   Begin VB.CommandButton Btn_Post_PBT5 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�������� �.�.7"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3660
      Picture         =   "Frm_Payment.frx":100BD
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   8490
      Width           =   2925
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3765
      Left            =   120
      TabIndex        =   22
      Top             =   4650
      Width           =   12525
      Begin VB.TextBox Txt_Summary 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   480
         Left            =   10530
         Locked          =   -1  'True
         TabIndex        =   41
         Text            =   "0.00"
         Top             =   1380
         Width           =   1905
      End
      Begin VB.TextBox Txt_BOOK_NUMBER 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   7770
         MaxLength       =   8
         TabIndex        =   1
         Top             =   480
         Width           =   1635
      End
      Begin VB.TextBox Txt_BOOK_NO 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   10530
         MaxLength       =   12
         TabIndex        =   2
         Top             =   480
         Width           =   1905
      End
      Begin VB.TextBox Txt_RoundMoney 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   480
         Left            =   10530
         TabIndex        =   3
         Text            =   "0.00"
         Top             =   1890
         Width           =   1905
      End
      Begin VB.CheckBox Chk_RoundMoney 
         Height          =   195
         Left            =   8340
         TabIndex        =   40
         Top             =   2130
         Width           =   225
      End
      Begin VB.ComboBox Combo1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         ItemData        =   "Frm_Payment.frx":10647
         Left            =   8700
         List            =   "Frm_Payment.frx":10657
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1890
         Width           =   1755
      End
      Begin MSMask.MaskEdBox MaskEdBox1 
         Height          =   465
         Left            =   4920
         TabIndex        =   24
         Top             =   960
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   820
         _Version        =   393216
         BackColor       =   12632256
         ForeColor       =   12582912
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MaskEdBox3 
         Height          =   465
         Left            =   1410
         TabIndex        =   25
         Top             =   960
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   820
         _Version        =   393216
         BackColor       =   12632256
         ForeColor       =   12582912
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MaskEdBox2 
         Height          =   465
         Left            =   7770
         TabIndex        =   0
         Top             =   930
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   820
         _Version        =   393216
         BackColor       =   16777215
         ForeColor       =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00D1ADAD&
         Caption         =   "��¡�ê�������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   6870
         TabIndex        =   49
         Top             =   30
         Width           =   5625
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H005C5C5F&
         BorderColor     =   &H005C5C5F&
         Height          =   3765
         Index           =   2
         Left            =   6840
         Top             =   0
         Width           =   5670
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   24
         Left            =   9810
         TabIndex        =   48
         Top             =   1080
         Width           =   660
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��Ť������ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   16
         Left            =   9570
         TabIndex        =   47
         Top             =   1560
         Width           =   885
      End
      Begin VB.Label Lb_Count 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   10530
         TabIndex        =   46
         Top             =   930
         Width           =   1905
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "������� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   10
         Left            =   7200
         TabIndex        =   45
         Top             =   600
         Width           =   540
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   11
         Left            =   9930
         TabIndex        =   44
         Top             =   600
         Width           =   540
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ѵ��ɤ������ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   2
         Left            =   7050
         TabIndex        =   43
         Top             =   2100
         Width           =   1200
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   3
         Left            =   6960
         TabIndex        =   42
         Top             =   1080
         Width           =   765
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Թ��˹� (�ѹ) :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   6
         Left            =   3450
         TabIndex        =   39
         Top             =   1620
         Width           =   1410
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��һ�Ѻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   7
         Left            =   4185
         TabIndex        =   38
         Top             =   2640
         Width           =   675
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   15
         Left            =   240
         TabIndex        =   37
         Top             =   600
         Width           =   1110
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ����駻����Թ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   18
         Left            =   3375
         TabIndex        =   36
         Top             =   600
         Width           =   1485
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ����Ѻ�駻����Թ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   1
         Left            =   3210
         TabIndex        =   35
         Top             =   1110
         Width           =   1650
      End
      Begin VB.Label Lb_DateOver 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   4920
         TabIndex        =   34
         Top             =   1500
         Width           =   1695
      End
      Begin VB.Label Lb_MoneyDue 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   4920
         TabIndex        =   33
         Top             =   2520
         Width           =   1695
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Թ����/Ŵ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   4
         Left            =   3810
         TabIndex        =   32
         Top             =   3090
         Width           =   1050
      End
      Begin VB.Label Lb_discount 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   4920
         TabIndex        =   31
         Top             =   3030
         Width           =   1695
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   8
         Left            =   300
         TabIndex        =   30
         Top             =   1080
         Width           =   1050
      End
      Begin VB.Label Lb_PBT_NO 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   1410
         TabIndex        =   29
         Top             =   450
         Width           =   1695
      End
      Begin VB.Label Lb_PBT_NO_ACCEPT_OLD 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   4920
         TabIndex        =   28
         Top             =   450
         Width           =   1695
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��һ�Ѻ���Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   5
         Left            =   3615
         TabIndex        =   27
         Top             =   2130
         Width           =   1245
      End
      Begin VB.Label Lb_MoneyDue_Perform 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   14.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   4920
         TabIndex        =   26
         Top             =   2010
         Width           =   1695
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H005C5C5F&
         BorderColor     =   &H005C5C5F&
         Height          =   3765
         Index           =   1
         Left            =   0
         Top             =   0
         Width           =   6840
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00D1ADAD&
         Caption         =   "��������´����Ѻ��������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   23
         Top             =   30
         Width           =   6825
      End
   End
   Begin VB.CheckBox Chk_Installment 
      BackColor       =   &H00C0C0C0&
      Caption         =   "�����繧Ǵ"
      Enabled         =   0   'False
      Height          =   195
      Left            =   300
      TabIndex        =   21
      Top             =   2430
      Visible         =   0   'False
      Width           =   1245
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   7636
      TabIndex        =   20
      Top             =   1590
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196655
      OrigLeft        =   9030
      OrigTop         =   1440
      OrigRight       =   9285
      OrigBottom      =   1935
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.OptionButton Op_PBT5_NO_ACCEPT 
      BackColor       =   &H00808080&
      Caption         =   "�Ţ����駻����Թ"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   6720
      TabIndex        =   16
      Top             =   2070
      Width           =   1605
   End
   Begin VB.OptionButton Op_PBT5_NO 
      BackColor       =   &H00808080&
      Caption         =   "�Ţ������Ẻ"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   5250
      TabIndex        =   15
      Top             =   2070
      Width           =   1455
   End
   Begin VB.TextBox Txt_Ownership_ID 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   360
      Left            =   8460
      MaxLength       =   10
      TabIndex        =   14
      Top             =   2340
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.CommandButton Btn_SearchOwnerShip_PBT5 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   7770
      Picture         =   "Frm_Payment.frx":1068D
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   2370
      Width           =   555
   End
   Begin VB.TextBox Txt_PBT_NO_ACCEPT 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   300
      Left            =   5250
      MaxLength       =   12
      TabIndex        =   7
      Top             =   2370
      Width           =   2505
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   1575
      Left            =   150
      TabIndex        =   5
      Top             =   2700
      Width           =   12465
      _ExtentX        =   21987
      _ExtentY        =   2778
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   10
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   10
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Frame Frame1 
      Height          =   345
      Left            =   9330
      TabIndex        =   17
      Top             =   2340
      Width           =   3285
      Begin VB.OptionButton Option2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "��˹��ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Left            =   1950
         TabIndex        =   19
         Top             =   60
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�������շ�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   60
         Width           =   1785
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   345
         Index           =   1
         Left            =   0
         Top             =   0
         Width           =   3285
      End
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Index           =   2
      Left            =   180
      Top             =   2340
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      Height          =   645
      Index           =   0
      Left            =   5220
      Top             =   2040
      Width           =   3135
   End
   Begin VB.Label Lb_Name_Now 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   345
      Left            =   150
      TabIndex        =   11
      Top             =   4290
      Width           =   12465
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻվط��ѡ�Ҫ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   4890
      TabIndex        =   10
      Top             =   1650
      Width           =   1680
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2548"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6630
      TabIndex        =   9
      Top             =   1590
      Width           =   1005
   End
   Begin VB.Label LB_Menu 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹��з��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Left            =   5910
      TabIndex        =   6
      Top             =   960
      Width           =   1725
   End
End
Attribute VB_Name = "Frm_Payment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Query As ADODB.Recordset
Dim QueryCfgTax As ADODB.Recordset
Dim Rs_Check As ADODB.Recordset
Dim Summary  As Currency
Dim Txt_Formula As String

Private Sub ShowReport(File_Name As String, Formula As String, Output As Boolean, Optional BEGIN_DATE As String, Optional ENDDATE As String)
Call FindPrinterDeFault
With Clone_Form.CTReport
        .Reset
        .Connect = "DSN = Microtax;UID = " & ini_Username & ";PWD = " & ini_Password & ";DSQ = Administration"
        .DiscardSavedData = True
        .SelectionFormula = Formula
        .PrinterDriver = TruePrinter.DriverName
        .PrinterPort = TruePrinter.Port
        .PrinterName = TruePrinter.DeviceName
        .ReportFileName = App.Path & "\Report\" & File_Name
        Select Case LB_Menu.Tag
                     Case "4"
                              .Formulas(0) = "TitleName ='" & BEGIN_DATE & "'"
'                              .Formulas(1) = "Province ='" & iniProvince & "'"
'                     Case "2"
'
        End Select
         .WindowShowPrintSetupBtn = True
         If Output Then
            .Destination = crptToPrinter
         Else
            .Destination = crptToWindow
            .WindowState = crptMaximized
             .WindowShowExportBtn = True
        End If
         .Action = 1
End With
End Sub

Private Function CalculateTax(Money As Single) As Single

Dim DateComp As Long
Dim DateComp2 As Long
Dim TaxInclude As Currency, TaxInclude2 As Currency
Dim Rs_Submit As ADODB.Recordset
Dim Per_Month As Integer
Dim Per_Month2 As Integer
On Error GoTo ErrCal
            Set Rs_Submit = New ADODB.Recordset
            TaxInclude = 0
            TaxInclude2 = 0
            DateComp = 0
            If Chk_Installment.Value = Checked Then
                    If Trim$(Mask_Installment_Start.Text) = "__/__/____" Then Exit Function
                    If Trim$(MaskEdBox1.Text) <> "__/__/____" Then
                            DateComp = DateDiff("d", Format$(CDate(MaskEdBox1.Text), "dd/mm/yyyy"), CDate(Mask_Installment_Start.Text))  ' �ѹ�������Թ�ҡ����Ѻ�Ţ�駻����Թ
                            Select Case LB_Menu.Tag
                                    Case 1, 5 '���ا��ͧ���
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept1.Text) + 1
                                    Case 2, 6 '�ç���͹��з��Թ
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept0.Text)
                                    Case 3, 7 '����
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept2.Text) + 1
                            End Select
                            If ((DateComp + 30) / 30) - Int((DateComp + 30) / 30) > 0 Then
                                    Per_Month = Int(DateComp / 30) + 1
                            Else
                                    Per_Month = Int(DateComp / 30)
                            End If
                    End If
                    DateComp2 = DateDiff("d", CDate("30/04/" & Lb_Year.Caption), CDate(Mask_Installment_Start.Text))  '�ѹ�������Թ��ѧ�ҡ 30 ��.�. ������ա���駻����Թ
                    If DateComp2 / 30 - Int(DateComp2 / 30) > 0 Then
                            Per_Month2 = Int(DateComp2 / 30) + 1
                    Else
                            Per_Month2 = Int(DateComp2 / 30)
                    End If
            Else
                    If Trim$(MaskEdBox2.Text) = "__/__/____" Then Exit Function
                    If Trim$(MaskEdBox1.Text) <> "__/__/____" Then
                            DateComp = DateDiff("d", Format$(CDate(MaskEdBox1.Text), "dd/mm/yyyy"), CDate(MaskEdBox2.Text))  ' �ѹ�������Թ�ҡ����Ѻ�Ţ�駻����Թ
                            Select Case LB_Menu.Tag
                                    Case 1, 5 '���ا��ͧ���
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept1.Text) + 1
                                    Case 2, 6 '�ç���͹��з��Թ
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept0.Text)
                                    Case 3, 7 '����
                                            DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept2.Text) + 1
                            End Select
                            If ((DateComp + 30) / 30) - Int((DateComp + 30) / 30) > 0 Then
                                    Per_Month = Int(DateComp / 30) + 1
                            Else
                                    Per_Month = Int(DateComp / 30)
                            End If
                    End If
                    DateComp2 = DateDiff("d", CDate("30/04/" & Lb_Year.Caption), CDate(MaskEdBox2.Text))  '�ѹ�������Թ��ѧ�ҡ 30 ��.�. ������ա���駻����Թ
                    If DateComp2 / 30 - Int(DateComp2 / 30) > 0 Then
                            Per_Month2 = Int(DateComp2 / 30) + 1
                    Else
                            Per_Month2 = Int(DateComp2 / 30)
                    End If
            End If
            
Select Case LB_Menu.Tag
             Case "1", "5" '���ا��ͧ���
                            Lb_DateOver.Caption = DateComp
                            Call SET_QUERY("SELECT PBT5_DATE,PBT5_NO_ACCEPT,FLAG_RETURN,PBT5_DATE_RETURN,PBT5_SET_GK,PBT5_OWNER_DATE FROM PBT5 WHERE PBT5_NO='" & Lb_PBT_NO.Caption & "' AND PBT5_YEAR=" & Lb_Year.Caption, Rs_Submit)
                                    If Rs_Submit.Fields("PBT5_SET_GK").Value <> 1 Then '*******  �������  *******
                                            With Frm_Index
                                            If DateDiff("d", Format$(Rs_Submit.Fields("PBT5_OWNER_DATE").Value, "dd/mm/yyyy"), Format$(Rs_Submit.Fields("PBT5_DATE").Value, "dd/mm/yyyy")) > 30 Then   '���Ẻ��ѧ�ҡ����Ңͧ�����Է����Թ 30 �ѹ
                                                    TaxInclude2 = TaxInclude2 + (Money * CCur(.Txt_Tax_OutSchedule1.Text)) / 100
                                            End If
                                            If Not IsNull(Rs_Submit.Fields("PBT5_DATE_RETURN").Value) And Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then '   ����ա���駻����Թ
                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                            TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                            Lb_DateOver.Caption = DateComp
                                                    End If
                                            End If
                                            End With
                                    Else
                                            With Frm_Index
                                                    If (CInt(Lb_Year.Caption) - 2545) Mod 4 = 0 Then  ' ����㹻���������������
                                                            If Rs_Submit.Fields(0).Value > CDate("31/01/" & (CInt(Lb_Year.Caption) - (CInt(Lb_Year.Caption) - 2545) Mod 4)) Then   '         ������ѹ�������Ҥ�
                                                                    TaxInclude2 = TaxInclude2 + (Money * CCur(.Txt_Tax_OutSchedule1.Text)) / 100      '�Դ��һ�Ѻ���Ẻ�Թ��˹� 10%
                                                                    If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                            If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                            Lb_DateOver.Caption = DateComp
                                                                                    End If
                                                                            ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                                    If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                                    End If
                                                                            End If
        '                                                            Else            '  ����ա���駻����Թ
        '                                                                    If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
        '                                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
        '                                                                    End If
                                                                    End If
                                                            Else             '  ����������Ҥ�
                                                                    If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                            If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                            Lb_DateOver.Caption = DateComp
                                                                                    End If
                                                                            ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                                    If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                                    End If
                                                                            End If
'                                                                    Else
'                                                                            If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
'                                                                                    TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
'                                                                            End If
                                                                    End If
                                                            End If
                                                    Else    ' �������㹻��������
                                                            If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                    If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                            If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                    TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                    Lb_DateOver.Caption = DateComp
                                                                            End If
                                                                    ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                            If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                    TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                            End If
                                                                    End If
                                                            Else            '  ����ա���駻����Թ
                                                                    If CDate(MaskEdBox2.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                            TaxInclude = TaxInclude + (Money * CSng(.Txt_Tax_OutTime1.Text)) / 100 * Per_Month2
                                                                    End If
                                                            End If
                                                    End If
                                            End With
                                    End If
                                            Lb_MoneyDue_Perform.Caption = FormatNumber(CCur(Lb_MoneyDue_Perform.Caption) + TaxInclude2, 2, vbTrue)
                                            Lb_MoneyDue.Caption = FormatNumber(CCur(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
            Case "3", "7"  '����    *********************************************************************************************************
                                Call SET_QUERY("SELECT DISTINCT PP1_DATE,PP1_DATE_RETURN,PP1_SET_GK,PP1_OWNER_DATE FROM PP1 WHERE PP1_STATUS=0 AND PP1_NO='" & Lb_PBT_NO.Caption & "' AND PP1_YEAR=" & Lb_Year.Caption, Rs_Submit)
                                With Frm_Index
                                        If Rs_Submit.Fields("PP1_SET_GK").Value <> 1 Then '     �������
                                                If Month(Rs_Submit.Fields("PP1_OWNER_DATE").Value) > 3 Then  '�Դ��駻������� �.� - .��.�. ����դ�һ�Ѻ
                                                        If DateDiff("d", Format$(Rs_Submit.Fields("PP1_OWNER_DATE").Value, "dd/mm/yyyy"), Format$(Rs_Submit.Fields("PP1_DATE").Value, "dd/mm/yyyy")) > 15 Then
                                                                TaxInclude2 = TaxInclude2 + (Money * CCur(.Txt_Tax_OutSchedule2.Text)) / 100
                                                        End If
                                                End If
                                                If DateComp < 0 Then DateComp = 0
                                                Lb_DateOver.Caption = DateComp
                                                If Not IsNull(Rs_Submit.Fields("PP1_DATE_RETURN").Value) Then     '   ����ա���駻����Թ
                                                        TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime2.Text)) / 100) * Per_Month
                                                End If
                                        Else
                                                If DateComp < 0 Then DateComp = 0
                                                Lb_DateOver.Caption = DateComp
                                                If Rs_Submit.Fields(0).Value > CDate("31/03/" & Lb_Year.Caption) Then   '   ���Ẻ���ѹ��˹�
                                                        TaxInclude2 = TaxInclude2 + (Money * CCur(.Txt_Tax_OutSchedule2.Text)) / 100
                                                End If
                                                '���Ẻ�ѹ��˹�
                                                If Not IsNull(Rs_Submit.Fields("PP1_DATE_RETURN").Value) Then  '   ����ա���駻����Թ
                                                        If DateComp > 0 Then  '�����Թ 30 �ѹ��ѧ���Ѻ㺵ͺ�Ѻ����駻����Թ
                                                                TaxInclude = TaxInclude + ((Money * CCur(.Txt_Tax_OutTime2.Text)) / 100) * Per_Month
                                                        End If
                                                End If
                                        End If
                                End With
                                Lb_MoneyDue_Perform.Caption = FormatNumber(CCur(Lb_MoneyDue_Perform.Caption) + TaxInclude2, 2, vbTrue)
                                Lb_MoneyDue.Caption = FormatNumber(CCur(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
             Case "2", "6" '�ç���͹��з��Թ    ********************************************************************************************
                            If Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) <> Empty Then
                                    With Frm_Index
                                            Lb_DateOver.Caption = DateComp
                                            If DateComp > 0 Then
                                                  If DateComp > 0 And DateComp <= 30 Then
                                                          TaxInclude = (Money * CCur(.Txt_Due_1Month.Text)) / 100
                                                  End If
                                                  If DateComp > 30 And DateComp <= 60 Then
                                                          TaxInclude = (Money * CCur(.Txt_Due_2Month.Text)) / 100
                                                  End If
                                                  If DateComp > 60 And DateComp <= 90 Then
                                                          TaxInclude = (Money * CCur(.Txt_Due_3Month.Text)) / 100
                                                  End If
                                                  If DateComp > 90 Then
                                                          TaxInclude = (Money * CCur(.Txt_Due_4Month.Text)) / 100
                                                  End If
                                            End If
                                    End With
                            End If
                            Lb_MoneyDue.Caption = FormatNumber(CCur(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
End Select
CalculateTax = Money
Set Rs_Submit = Nothing
Exit Function
ErrCal:
        Set Rs_Submit = Nothing
        MsgBox Err.Description
End Function

Private Function LooP_IN_GK(GK_DATE As Date) As Byte
        If Int(Month(GK_DATE)) = 10 Or Int(Month(GK_DATE)) = 11 Then
            LooP_IN_GK = 1
        End If
        If Int(Month(GK_DATE)) = 12 Or Int(Month(GK_DATE)) = 1 Then
            LooP_IN_GK = 2
        End If
        If Int(Month(GK_DATE)) = 2 Or Int(Month(GK_DATE)) = 3 Then
            LooP_IN_GK = 3
        End If
        If Int(Month(GK_DATE)) = 4 Or Int(Month(GK_DATE)) = 5 Then
            LooP_IN_GK = 4
        End If
        If Int(Month(GK_DATE)) = 6 Or Int(Month(GK_DATE)) = 7 Then
            LooP_IN_GK = 5
        End If
        If Int(Month(GK_DATE)) = 8 Or Int(Month(GK_DATE)) = 9 Then
            LooP_IN_GK = 6
        End If
End Function

Private Sub CheckForPBT5()
On Error Resume Next
Dim i  As Byte, j   As Integer
        For j = 1 To Grid_Result.Rows - 1
                Grid_Result.Row = j
                For i = 0 To Grid_Result.Cols - 1
                        Grid_Result.Col = i
                    If Option1.Value Then
                        Grid_Result.CellBackColor = &HC0C0FF
                        Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = "/"
                     Else
                        Grid_Result.CellBackColor = &H80000018
                        Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                     End If
                  Next i
     Next j
End Sub

Private Function Chk_IN_GK() As Byte
    Dim DateNow As Byte, DateOwn As Byte
    
    Set Query = Globle_Connective.Execute("exec sp_check_in_gk '" & Trim$(Txt_Ownership_ID.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
    If Query.RecordCount > 0 Then
            DateOwn = LooP_IN_GK(Query.Fields(1).Value)
            DateNow = LooP_IN_GK(CDate(MaskEdBox2.Text))
            If DateNow = DateOwn Then
                    Chk_IN_GK = 1
            Else
                    Chk_IN_GK = 2
            End If
    Else
            Chk_IN_GK = 1
    End If
'    Select Case LB_Menu.Tag
'                 Case "1", "5"
'                              Call SET_QUERY("SELECT PBT5_STATUS ,PBT5_PAY_DATE,PBT5_IN_GK FROM PBT5 WHERE PBT5_STATUS = 1 AND PBT5_IN_GK = 1 AND PBT5_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(Txt_Ownership_ID.Text) & "'", Query)
'                              If Query.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Query.Fields("PBT5_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox2.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'                 Case "2", "6"
'                              Call SET_QUERY("SELECT PRD2_STATUS ,PRD2_PAY_DATE ,PRD2_IN_GK FROM PRD2 WHERE PRD2_STATUS = 1 AND PRD2_IN_GK = 1 AND PRD2_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(Txt_Ownership_ID.Text) & "'", Query)
'                              If Query.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Query.Fields("PRD2_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox2.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'                 Case "3", "7"
'                              Call SET_QUERY("SELECT PP1_STATUS ,PP1_PAY_DATE ,PP1_IN_GK FROM PP1 WHERE PP1_STATUS = 1 AND PP1_IN_GK = 1 AND PP1_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(Txt_Ownership_ID.Text) & "'", Query)
'                              If Query.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Query.Fields("PP1_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox2.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'                 Case "4"
'                              Call SET_QUERY("SELECT PBA1_STATUS ,PBA1_PAY_DATE ,PBA1_IN_GK FROM PBA1 WHERE PBA1_STATUS = 1 AND PBA1_IN_GK = 1 AND PBA1_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(Txt_Ownership_ID.Text) & "'", Query)
'                              If Query.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Query.Fields("PBA1_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox2.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'    End Select
End Function

Private Sub Set_Grid_Installment()
        With Grid_Installment
                .FormatString = "^�Ǵ���|^�����ѹ����ѹ���|^�ѹ������|^��һ�Ѻ�Թ�Ǵ|^�ʹ����ͧ����|^�ӹǹ�Թ����|^����"
                .ColWidth(0) = 700
                .ColWidth(1) = 1500
                .ColWidth(2) = 1500
                .ColWidth(3) = 1500: .ColAlignment(3) = 6
                .ColWidth(4) = 1500: .ColAlignment(4) = 6
                .ColWidth(5) = 1500: .ColAlignment(5) = 6
                .ColWidth(6) = 800
        End With
End Sub

Private Sub Set_Grid()
On Error Resume Next
With Grid_Result
If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                .FormatString = "^ʶҹЪ�������|�ѹ����駻����Թ|�����ŧ���Թ|�Ţ����͡����Է���|�Ţ���Թ|^�����|�������|||||||����"
                .ColWidth(0) = 1500
                .ColWidth(1) = 1500
                .ColWidth(2) = 1300
                .ColWidth(3) = 1500
                .ColWidth(4) = 800
                .ColWidth(5) = 4000: .ColAlignment(5) = 2
                .ColWidth(6) = 1100: .ColAlignment(6) = 6 ' REALMONEY
                .ColWidth(7) = 0
                .ColWidth(8) = 0
                .ColWidth(9) = 0
                .ColWidth(10) = 0
                .ColWidth(11) = 0
                .ColWidth(12) = 0
                .ColWidth(13) = 600: .ColAlignment(13) = 5
End If
If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
                .FormatString = "^ʶҹЪ�������|�ѹ����駻����Թ|�����ç���͹|��ҹ�Ţ���|^�����|�������|||||||����"
                .ColWidth(0) = 1400
                .ColWidth(1) = 1500
                .ColWidth(2) = 1300
                .ColWidth(3) = 1100
                .ColWidth(4) = 4900: .ColAlignment(4) = 2
                .ColWidth(5) = 1500: .ColAlignment(5) = 6  'REALMONEY
                .ColWidth(6) = 0
                .ColWidth(7) = 0
                .ColWidth(8) = 0
                .ColWidth(9) = 0
                .ColWidth(10) = 0
                .ColWidth(11) = 0
'                .ColWidth(12) = 0  '�Ţ���Ǵ
                .ColWidth(12) = 600: .ColAlignment(12) = 5
End If
If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                .FormatString = "^ʶҹЪ�������|�ѹ����駻����Թ|���ʻ���|�ӹǹ��ҹ|^�����|�������|||||||����"
                .ColWidth(0) = 1400
                .ColWidth(1) = 1500
                .ColWidth(2) = 1300
                .ColWidth(3) = 1000
                .ColWidth(4) = 5000: .ColAlignment(4) = 2
                .ColWidth(5) = 1500: .ColAlignment(5) = 6  ' REALMONEY
                .ColWidth(6) = 0
                .ColWidth(7) = 0
                .ColWidth(8) = 0
                .ColWidth(9) = 0
                .ColWidth(10) = 0
                .ColWidth(11) = 0  '�Ţ���Ǵ
                .ColWidth(12) = 0 '�Ţ���Ǵ
                .ColWidth(13) = 600: .ColAlignment(13) = 5
End If
If LB_Menu.Tag = "4" Then
                .FormatString = "^ʶҹЪ�������|ʶҹЪ�������||�������|�Ţ���|^�����|||�������||||����"
                 .ColWidth(0) = 1400
                .ColWidth(1) = 1500
                .ColWidth(3) = 1000: .ColAlignment(3) = 4: .ColAlignmentFixed(3) = 4
                .ColWidth(4) = 1000: .ColAlignment(4) = 4: .ColAlignmentFixed(4) = 4
                .ColWidth(5) = 5000: .ColAlignmentFixed(5) = 4
                .ColWidth(8) = 1500: .ColAlignment(8) = 6: .ColAlignmentFixed(8) = 4
                .ColWidth(2) = 0
                .ColWidth(6) = 0
                .ColWidth(7) = 0
                .ColWidth(9) = 0
                .ColWidth(10) = 0
                .ColWidth(11) = 0   ' REALMONEY
                .ColWidth(12) = 0
                .TextArray(13) = "����": .ColWidth(13) = 600: .ColAlignment(13) = 5
End If
End With
End Sub

Private Sub Clear_Grid()
On Error Resume Next
Dim i As Byte
                Grid_Result.Rows = 2
                For i = 0 To Grid_Result.Cols - 1
                        Grid_Result.TextMatrix(1, i) = Empty
                Next i
                Txt_PBT_NO_ACCEPT.Text = Empty
                Lb_Count.Caption = "0"
                Lb_Name_Now.Caption = Empty
                Txt_Summary.Text = "0.00"
                Lb_PBT_NO_ACCEPT_OLD.Caption = vbNullString
                Lb_PBT_NO.Caption = vbNullString
                Lb_discount.Caption = "0.00"
                Chk_RoundMoney.Value = Unchecked
End Sub

Private Sub Clear_Grid_Installment()
On Error Resume Next
                Grid_Installment.Clear
                Grid_Installment.Rows = 4
End Sub

Private Sub CalOfPay()
        On Error Resume Next
                        Dim i As Integer
                        Summary = 0
                        Lb_Count.Caption = "0"
                        Lb_MoneyDue.Caption = "0.00"
                        Lb_MoneyDue_Perform.Caption = "0.00"
                        For i = 1 To Grid_Result.Rows - 1
                                If Grid_Result.TextMatrix(i, 0) = "�ѧ������" And Grid_Result.TextMatrix(i, Grid_Result.Cols - 1) = "/" Then
                                            Lb_Count.Caption = CInt(Lb_Count.Caption) + 1
                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                Grid_Result.TextMatrix(i, 11) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 6)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 6))
                                        End If
                                        If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 5))
                                        End If
                                        If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 5))
                                        End If
                                        If LB_Menu.Tag = "4" Then
                                          Summary = Summary + CCur(Grid_Result.TextMatrix(i, 8))
                                          If Grid_Result.TextMatrix(i, 9) = "1" Then
                                                           Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 4)
                                          End If
                                          If Grid_Result.TextMatrix(i, 9) = "2" Then
                                                          Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 5)
                                          End If
                                     End If
                                             If Grid_Result.TextMatrix(i, 1) <> Empty Then
                                                    If CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) < 2500 Then
                                                        Grid_Result.TextMatrix(i, 1) = Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm") & _
                                                                                                           "/" & CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) + 543
                                                    End If
                                             End If
                               End If
                        Next i
                        If CInt(Lb_MoneyDue.Caption) <> 0 Then Lb_MoneyDue.Caption = FormatNumber(Lb_MoneyDue.Caption, 2, vbTrue)
                        If CInt(Lb_MoneyDue_Perform.Caption) <> 0 Then Lb_MoneyDue_Perform.Caption = FormatNumber(Lb_MoneyDue_Perform.Caption, 2, vbTrue)
                        Txt_Summary.Text = FormatNumber(Summary, 2, vbTrue)   'END OF CALCULATETAX+++++++++++++++++++++++++++++++++++++++++
                        Txt_RoundMoney.Text = Format$(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption), "###,##0.00")
End Sub

Private Sub CalOfPay2()
        On Error Resume Next
                        Dim i As Integer
                        Summary = 0
                        Lb_Count.Caption = "0"
                        Lb_MoneyDue.Caption = "0.00"
                        Lb_MoneyDue_Perform.Caption = "0.00"
                        For i = 1 To Grid_Result.Rows - 1
                                If Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ" And Grid_Result.TextMatrix(i, Grid_Result.Cols - 1) = "/" Then
                                            Lb_Count.Caption = CInt(Lb_Count.Caption) + 1
                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                Grid_Result.TextMatrix(i, 11) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 6)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 6))
                                        End If
                                        If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 5))
                                        End If
                                        If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 5))
                                        End If
                                        If LB_Menu.Tag = "4" Then
                                          Summary = Summary + CCur(Grid_Result.TextMatrix(i, 8))
                                          If Grid_Result.TextMatrix(i, 9) = "1" Then
                                                           Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 4)
                                          End If
                                          If Grid_Result.TextMatrix(i, 9) = "2" Then
                                                          Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 5)
                                          End If
                                     End If
                                             If Grid_Result.TextMatrix(i, 1) <> Empty Then
                                                    If CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) < 2500 Then
                                                        Grid_Result.TextMatrix(i, 1) = Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm") & _
                                                                                                           "/" & CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) + 543
                                                    End If
                                             End If
                               End If
                        Next i
                        If CInt(Lb_MoneyDue.Caption) <> 0 Then Lb_MoneyDue.Caption = FormatNumber(Lb_MoneyDue.Caption, 2, vbTrue)
                        If CInt(Lb_MoneyDue_Perform.Caption) <> 0 Then Lb_MoneyDue_Perform.Caption = FormatNumber(Lb_MoneyDue_Perform.Caption, 2, vbTrue)
                        Txt_Summary.Text = FormatNumber(Summary, 2, vbTrue)   'END OF CALCULATETAX+++++++++++++++++++++++++++++++++++++++++
                        Txt_RoundMoney.Text = Format$(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption), "###,##0.00")
End Sub



Private Sub Btn_Post_PBT5_Click()
        
'        Dim str_SQL As String
        Dim Rs As ADODB.Recordset
        Dim i As Integer, RdMoney As Currency, j As Byte
        On Error GoTo ErrHandler
        Globle_Connective.BeginTrans
        
        Set Rs = New ADODB.Recordset
        If Chk_Installment.Enabled = False Then
                If Grid_Result.TextMatrix(1, 0) = Empty Then Exit Sub
                If Txt_BOOK_NUMBER.Text = Empty Then
                    MsgBox "�ô�к�������� !", vbExclamation, "�Ӫ��ᨧ"
                    Txt_BOOK_NUMBER.SetFocus
                    Globle_Connective.RollbackTrans
                    Exit Sub
                End If
                If Txt_BOOK_NO.Text = Empty Then
                        MsgBox "�ô�к��Ţ��� !", vbExclamation, "�Ӫ��ᨧ"
                        Txt_BOOK_NO.SetFocus
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
                 
                If CInt(Lb_Count.Caption) = 0 Then
                        MsgBox "�ô���͡�к���¡�����ͪ������� !", vbExclamation, "�Ӫ��ᨧ"
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
                If Not IsDate(MaskEdBox2.Text) Then
                        MsgBox "��س�����ѹ��͹�շ���ͧ�ѹ�������١��ͧ����", vbCritical, "��ͼԴ��Ҵ"
                        MaskEdBox2.SetFocus
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
                Select Case CInt(Mid$(MaskEdBox2.Text, 4, 2))
                            Case 10, 11, 12
                                        Lb_Year.Tag = Right$(MaskEdBox2.Text, 4) + 1
                            Case 1, 2, 3, 4, 5, 6, 7, 8, 9
                                        Lb_Year.Tag = Right$(MaskEdBox2.Text, 4)
                End Select
                Set Rs_Check = Globle_Connective.Execute("exec sp_check_payment_book_no '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "'," & CInt(Lb_Year.Tag) & ",'" & _
                            LB_Menu.Tag & "'", , adCmdUnknown)
        
                 If Rs_Check.RecordCount > 0 Then     '�礡�ë�Ӣͧ������� �Ţ���
                        Txt_BOOK_NO.Text = Empty
                        Txt_BOOK_NUMBER.Text = Empty
                        Txt_BOOK_NUMBER.SetFocus
                        MsgBox "�������������������ӡѺ㹰ҹ������ ��س����������������Ţ���������� !!!", vbCritical, "��ͼԴ��Ҵ"
                        Globle_Connective.RollbackTrans
                        Exit Sub
                 End If
                If MsgBox("��ͧ��ê������� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
'                Dim sql_txt As String
                
                
                RdMoney = 0  ' ʵҧ�� ���� �Թ����Ŵ
                RdMoney = CCur(Txt_RoundMoney.Text) - (CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption))
        
        
        '            If chkInstallment.Value = Checked Then
        '                    sql_txt = "INSERT INTO PBT5_INSTALLMENT (PBT5_NO,OWNERSHIP_ID,INSTALLMENT,PBT5_AMOUNT_REAL,DATE_INSTALLMENT) VALUES (" & _
        '                                    Trim$(Lb_PBT_NO.caption) & "," & Trim$(Txt_Ownership_ID.Text) & "," & 1 & "," & Txt_Summary
        ''            Else
        '        Debug.Print "exec sp_update_payment_gk '" & CvDate2(CDate(MaskEdBox2.Text)) & "','" & Lb_Year.Caption & "','" & _
        '                    Trim$(Txt_OwnerShip_ID.Text) & "','" & Lb_Menu.Tag & "'"
                
        '        Globle_Connective.Execute "exec sp_update_payment_gk '" & CvDate2(CDate(MaskEdBox2.Text)) & "','" & Lb_Year.Caption & "','" & _
        '                    Trim$(Txt_Ownership_ID.Text) & "','" & LB_Menu.Tag & "'", , adCmdUnknown
                Set Rs = Globle_Connective.Execute("exec sp_check_first_paytax '" & Txt_Ownership_ID.Text & "','" & Lb_Year.Caption & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
                If Rs.RecordCount < 1 Then
                Call SET_Execute2("exec sp_update_payment_gk '" & CvDate2(CDate(MaskEdBox2.Text)) & "','" & Lb_Year.Caption & "','" & _
                            Trim$(Txt_Ownership_ID.Text) & "','" & LB_Menu.Tag & "'", 8, False)
                End If
        '                    Select Case LB_Menu.Tag
        '                                 Case "1", "5"
        '                                                    sql_txt = "UPDATE PBT5 SET PBT5_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "'" & _
        '                                                                     "  WHERE PBT5_YEAR = " & CInt(Lb_Year.Caption) & _
        '                                                                     " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & _
        '                                                                     " AND PBT5_STATUS = 0 AND PBT5_SET_GK " & IIf(LB_Menu.Tag = "1", "<>", "=") & "2 "
        '                                                                      Call SET_Execute(sql_txt)
        '                                 Case "2", "6"
        '                                                    sql_txt = "UPDATE PRD2 SET PRD2_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "'" & _
        '                                                                     " WHERE PRD2_YEAR = " & CInt(Lb_Year.Caption) & _
        '                                                                     " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & _
        '                                                                     " AND PRD2_STATUS = 0 AND PRD2_SET_GK " & IIf(LB_Menu.Tag = "2", "<>", "=") & "2 "
        '                                                                     Call SET_Execute(sql_txt)
        '                                 Case "3", "7"
        '                                                    sql_txt = "UPDATE PP1 SET PP1_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "'" & _
        '                                                                    " WHERE PP1_YEAR = " & CInt(Lb_Year.Caption) & _
        '                                                                    " AND OWNERSHIP_ID  = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & _
        '                                                                    " AND PP1_STATUS = 0 AND PP1_SET_GK " & IIf(LB_Menu.Tag = "3", "<>", "=") & "2 "
        '                                                                     Call SET_Execute(sql_txt)
        '                                 Case "4"
        '                                                    sql_txt = "UPDATE PBA1 SET PBA1_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "'" & _
        '                                                                     "  WHERE PBA1_YEAR = " & CInt(Lb_Year.Caption) & _
        '                                                                     " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & _
        '                                                                     " AND PBA1_STATUS = 0 "
        '                                                                     Call SET_Execute(sql_txt)
        '                     End Select
                    
                              For i = 1 To Grid_Result.Rows - 1
                                    If Trim$(Grid_Result.TextMatrix(i, 0)) = "�ѧ������" And Trim$(Grid_Result.TextMatrix(i, Grid_Result.Cols - 1)) = "/" Then
        '                                If i > 1 Then RdMoney = 0:     Lb_MoneyDue.Caption = "0.00":     Lb_discount.Caption = "0.00": Lb_MoneyDue_Perform.Caption = "0.00"
                                                    
                                        If LB_Menu.Tag = "4" Then
        '                                        Globle_Connective.Execute "exec sp_update_payment_pba1 '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & CvDate2(CDate(MaskEdBox2.Text)) & "'," & RdMoney & _
        '                                                ",'" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_discount.Caption) & "," & CInt(Lb_Year.Caption) & ",'" & Trim$(Grid_Result.TextMatrix(i, 3)) & "','" & Trim$(Grid_Result.TextMatrix(i, 4)) & "','" & _
        '                                                Trim$(Txt_Ownership_ID.Text) & "'", , adCmdUnknown
                                                Call SET_Execute2("exec sp_update_payment_pba1 '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & CvDate2(CDate(MaskEdBox2.Text)) & "'," & RdMoney & _
                                                        ",'" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_discount.Caption) & "," & CInt(Lb_Year.Caption) & ",'" & Trim$(Grid_Result.TextMatrix(i, 3)) & "','" & Trim$(Grid_Result.TextMatrix(i, 4)) & "','" & _
                                                        Trim$(Txt_Ownership_ID.Text) & "'", 8, False)
                                        Else
                                                If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                        j = 6
                                                Else
                                                        j = 5
                                                End If
                                                Call SET_Execute2("exec sp_update_payment '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ",'" & _
                                                            Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & CvDate2(CDate(MaskEdBox2.Text)) & "'," & Chk_IN_GK & "," & _
                                                            RdMoney & "," & RdMoney & "," & CCur(Trim$(Grid_Result.TextMatrix(i, j))) & "," & CInt(Lb_Year.Caption) & ",'" & Trim$(Txt_Ownership_ID.Text) & "','" & _
                                                            Trim$(Grid_Result.TextMatrix(i, 2)) & "','" & Trim$(Lb_PBT_NO.Caption) & "','" & LB_Menu.Tag & "'", 8, False)
                                        End If
        '                                Select Case LB_Menu.Tag
        '                                             Case "1", "5"
        '                                                                sql_txt = "UPDATE PBT5 SET PBT5_STATUS = 1,PBT5_PAY = 1,PBT5_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
        '                                                                                 ", PBT5_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & " , PBT5_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PBT5_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "', PBT5_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "',PBT5_IN_GK = " & Chk_IN_GK & ",PBT5_SATANGINCLUDE = " & RdMoney & _
        '                                                                                 ", PBT5_DISCOUNT = " & CCur(Lb_discount.Caption) & ",PBT5_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 6))) & _
        '                                                                                 "  WHERE PBT5_YEAR = " & CInt(Lb_Year.Caption) & _
        '                                                                                 " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "' AND LAND_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
        '                                                                                 " AND PBT5_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
        '                                                                                  Call SET_Execute(sql_txt)
        '                                             Case "2", "6"
        '                                                                sql_txt = "UPDATE PRD2 SET PRD2_STATUS =1,PRD2_PAY = 1, PRD2_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
        '                                                                                 ",PRD2_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ",PRD2_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PRD2_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PRD2_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "',PRD2_IN_GK = " & Chk_IN_GK & ",PRD2_SATANGINCLUDE = " & RdMoney & _
        '                                                                                ", PRD2_DISCOUNT = " & CCur(Lb_discount.Caption) & ",PRD2_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 5))) & _
        '                                                                                 " WHERE PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "' AND BUILDING_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
        '                                                                                 " AND PRD2_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
        '                                                                                 Call SET_Execute(sql_txt)
        '                                             Case "3", "7"
        '                                                                sql_txt = "UPDATE PP1 SET  PP1_STATUS = 1 ,PP1_PAY = 1,PP1_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
        '                                                                                ",PP1_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ", PP1_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PP1_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PP1_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "',PP1_IN_GK = " & Chk_IN_GK & ",PP1_SATANGINCLUDE = " & RdMoney & _
        '                                                                                ",PP1_DISCOUNT = " & CCur(Lb_discount.Caption) & ",PP1_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 5))) & _
        '                                                                                " WHERE PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID  = '" & Trim$(Txt_OwnerShip_ID.Text) & "' AND SIGNBORD_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
        '                                                                                " AND PP1_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
        '                                                                                Call SET_Execute(sql_txt)
        '                                            Case "4"
        '                                                                sql_txt = "UPDATE PBA1 SET PBA1_STATUS = 1, PBA1_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PBA1_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PBA1_PAY_DATE = '" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "',PBA1_SATANGINCLUDE = " & RdMoney & _
        '                                                                                 ",PBA1_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & ", PBA1_DISCOUNT = " & CCur(Lb_discount.Caption) & _
        '                                                                                 " WHERE PBA1_YEAR = " & CInt(Lb_Year.Caption) & " AND LICENSE_BOOK = '" & Trim$(Grid_Result.TextMatrix(i, 3)) & "' AND LICENSE_NO = '" & Trim$(Grid_Result.TextMatrix(i, 4)) & "'" & _
        '                                                                                 " AND OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "'"
        '                                                                                  Call SET_Execute(sql_txt)
        '                                  End Select
                                            RdMoney = 0:     Lb_MoneyDue.Caption = "0.00":     Lb_discount.Caption = "0.00": Lb_MoneyDue_Perform.Caption = "0.00"
                                   End If
                            Next i
        '            End If  'chkInstallment
                    Else  '  �����繧Ǵ
                            If Chk_Installment.Value = Checked Then
                                    If Not IsDate(Mask_Installment_Start.Text) Then
                                            MsgBox "��س�����ѹ��͹�շ���ͧ�ѹ�������١��ͧ����", vbCritical, "��ͼԴ��Ҵ"
                                            Mask_Installment_Start.SetFocus
                                            Globle_Connective.RollbackTrans
                                            Exit Sub
                                    End If
                                    If Grid_Installment.TextMatrix(1, Grid_Installment.Cols - 1) = "/" And Not IsDate(Mask_Installment_Pay.Text) Then
                                            MsgBox "��س�����ѹ��͹�շ���ͧ�ѹ�������١��ͧ����", vbCritical, "��ͼԴ��Ҵ"
                                            Mask_Installment_Pay.SetFocus
                                            Globle_Connective.RollbackTrans
                                            Exit Sub
                                    End If
                                    If MsgBox("��ͧ��èѴ����¡�ê������� " & LB_Menu.Caption & " ��������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                                            Globle_Connective.RollbackTrans
                                            Exit Sub
                                    End If
                                    For i = 1 To Grid_Result.Rows - 1
                                            If Trim$(Grid_Result.TextMatrix(i, 0)) = "�ѧ������" And Trim$(Grid_Result.TextMatrix(i, Grid_Result.Cols - 1)) = "/" Then
                                                        Call SET_Execute2("exec sp_set_Installment '" & CvDate2(CDate(Mask_Installment_Start)) & "'," & CInt(Lb_Year.Caption) & ",'" & Trim$(Txt_Ownership_ID.Text) & "','" & _
                                                        Lb_Number_Installment.Caption & "','" & Trim$(Grid_Result.TextMatrix(i, 2)) & "','" & LB_Menu.Tag & "'", 8, False)
                                            End If
                                    Next i
                                    For i = 1 To Grid_Installment.Rows - 1
'                                            Debug.Print "exec sp_create_Installment '" & Lb_Number_Installment.Caption & "'," & Grid_Installment.TextMatrix(i, 0) & ",'" & CvDate3(Grid_Installment.TextMatrix(i, 1)) & "','" & _
'                                                        Grid_Installment.TextMatrix(i, 2) & "','" & Grid_Installment.TextMatrix(i, 3) & "','" & CCur(Txt_Money_Installment_Pay.Text) - (CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption)) & "','" & Grid_Installment.TextMatrix(i, 4) & "','" & _
'                                                        Grid_Installment.TextMatrix(i, 5) & "','" & CCur(Txt_RoundMoney_Installment.Text) - CCur(Txt_Money_Installment_Pay.Text) & "','" & IIf(Grid_Installment.TextMatrix(i, 6) = "/", 1, 0) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'"
                                            Call SET_Execute2("exec sp_create_Installment '" & Lb_Number_Installment.Caption & "'," & Grid_Installment.TextMatrix(i, 0) & ",'" & CvDate3(Grid_Installment.TextMatrix(i, 1)) & "','" & _
                                                        IIf(Len(Trim$(Grid_Installment.TextMatrix(i, 2))) = 0, Null, CvDate3(Grid_Installment.TextMatrix(i, 2))) & "','" & Grid_Installment.TextMatrix(i, 3) & "','" & CCur(Txt_Money_Installment_Pay.Text) - (CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption)) & "','" & Grid_Installment.TextMatrix(i, 4) & "','" & _
                                                        Grid_Installment.TextMatrix(i, 5) & "','" & CCur(Txt_RoundMoney_Installment.Text) - CCur(Txt_Money_Installment_Pay.Text) & "','" & IIf(Grid_Installment.TextMatrix(i, 6) = "/", 1, 0) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", 8, False)
                                    Next i
                            End If
                    End If
                    Call Clear_Grid
                    Call Set_Grid
                    If Txt_BOOK_NUMBER.Text <> Empty Then Txt_BOOK_NUMBER.Tag = Txt_BOOK_NUMBER.Text
                    If Txt_BOOK_NO.Text <> Empty Then Txt_BOOK_NO.Tag = Txt_BOOK_NO.Text
                    Txt_BOOK_NUMBER.Text = Empty
                    Txt_BOOK_NO.Text = Empty
                    Lb_PBT_NO.Caption = Empty
                    Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
                    Lb_DateOver.Caption = "0"
                    Lb_MoneyDue.Caption = "0.00"
                    Lb_MoneyDue_Perform.Caption = "0.00"
                    MaskEdBox1.Text = "__/__/____"
                    MaskEdBox2.Text = "__/__/____"
                    MaskEdBox3.Text = "__/__/____"
                    Lb_Count.Caption = "0"
                    Txt_Summary.Text = "0.00"
                    Lb_Name_Now.Caption = Empty
                    Chk_RoundMoney.Value = Unchecked
                    Option2.Value = True
                    Txt_Ownership_ID.Text = Empty
                    Txt_RoundMoney.Text = "0"
                    Label3(4).Caption = "�Թ����/Ŵ :"
                    Lb_discount.Caption = "0.00"
                    Globle_Connective.CommitTrans
            On Error GoTo ErrHandler2
                    If iniPrint_Payment = 1 Then
                            Call SetFormulaReport
                            Select Case LB_Menu.Tag
                                    Case "1", "5"
                                                Call ShowReport("PBT11.rpt", Txt_Formula, True)
                                    Case "2", "6"
                                                Call ShowReport("PRD12.rpt", Txt_Formula, True)
                                    Case "3", "7"
                                                Call ShowReport("PP7.rpt", Txt_Formula, True)
                                    Case "4"
'                                                str_SQL = "SELECT BUSINESS_TYPE.BUSINESS_ID" & _
'                                                " FROM BUSINESS_TYPE INNER JOIN (CLASS_DETAILS INNER JOIN (PBA1 INNER JOIN LICENSEDATA ON (PBA1.LICENSE_BOOK = LICENSEDATA.LICENSE_BOOK)" & _
'                                                " AND (PBA1.LICENSE_NO = LICENSEDATA.LICENSE_NO)) ON CLASS_DETAILS.CLASS_ID = LICENSEDATA.CLASS_ID) ON BUSINESS_TYPE.BUSINESS_ID = CLASS_DETAILS.BUSINESS_ID" & _
'                                                " WHERE (((PBA1.PBA1_BOOK_NUMBER)='" & Trim$(Txt_BOOK_NUMBER.Tag) & "') AND ((PBA1.PBA1_BOOK_NO)='" & Trim$(Txt_BOOK_NO.Tag) & "') AND ((PBA1.PBA1_YEAR)=" & Lb_Year.Caption & "))"
'                                                Call SET_QUERY(str_SQL, Query)
'                                                If Query.RecordCount > 0 Then
'                                                        Select Case Query.Fields(0).Value
'                                                                Case "001"   '  ���������
'                                                                        Call ShowReport("SOR5.rpt", Txt_Formula, False)
'                                                                Case "002"   '  �Ԩ��÷�����ѹ���µ���آ�Ҿ
'                                                                        Call ShowReport("ORP2.rpt", Txt_Formula, False)
'                                                                Case Else     '  �����
'
'                                                        End Select
'                                                End If
                            End Select
                    End If
Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Exit Sub
ErrHandler2:
'        MsgBox "�ա�úѹ�֡���������º�������� ���Դ��ͼԴ��Ҵ " & Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Reset_Payment()
        Txt_BOOK_NUMBER.Text = Empty
        Txt_BOOK_NO.Text = Empty
        Lb_PBT_NO.Caption = Empty
        Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
        Lb_DateOver.Caption = "0"
        Lb_MoneyDue.Caption = "0.00"
        Lb_MoneyDue_Perform.Caption = "0.00"
        MaskEdBox1.Text = "__/__/____"
        MaskEdBox2.Text = "__/__/____"
        MaskEdBox3.Text = "__/__/____"
        Lb_Count.Caption = "0"
        Txt_Summary.Text = "0.00"
        Lb_Name_Now.Caption = Empty
        Chk_RoundMoney.Value = Unchecked
        Option2.Value = True
        Txt_Ownership_ID.Text = Empty
        Txt_RoundMoney.Text = "0"
        Label3(4).Caption = "�Թ����/Ŵ :"
        Lb_discount.Caption = "0.00"
End Sub

Private Sub reset_Installment()
        Lb_Number_Installment.Caption = vbNullString
        Lb_discount2.Caption = "0.00"
        Lb_MoneyDue2.Caption = "0.00"
        Lb_MoneyDue_Perform2.Caption = "0.00"
        Txt_BOOK_NUMBER2.Text = vbNullString
        Txt_BOOK_NO2.Text = vbNullString
        Mask_Installment_Start.Text = "__/__/____"
        Txt_Summary_Installment.Text = "0.00"
        Txt_RoundMoney_Installment.Text = "0.00"
        Chk_RoundMoney2(0).Value = Unchecked
        Lb_Installment_No.Caption = vbNullString
        Mask_Installment_Pay.Text = "__/__/____"
        Txt_Money_Installment.Text = "0.00"
        Lb_MoneyDue_Installment.Caption = "0.00"
        Txt_Money_Installment_Pay.Text = "0.00"
        Chk_RoundMoney2(1).Value = Unchecked
        Chk_Installment.Value = Unchecked
        Chk_Installment.Enabled = False
        Call Clear_Grid_Installment
        Call Set_Grid_Installment
End Sub

Private Sub SetFormulaReport()
 Dim BEGIN_DATE As String
 Dim LASTDATE As String
 Txt_Formula = Empty
 BEGIN_DATE = MaskEdBox2.Text
 LASTDATE = CStr(Date)
 
'                            Select Case LB_Menu.Tag
'                                         Case "1"
'                                                        Txt_Formula = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_STATUS}=1" & ")"
'                                          Case "2"
'                                                       Txt_Formula = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_STATUS}=1" & ")"
'                                          Case "3"
'                                                       Txt_Formula = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_STATUS}=1" & ")"
'                              End Select
                                          
'                                          Select Case LB_Menu.Tag
'                                                       Case "1"
'                                                                        Txt_Formula = Txt_Formula & " AND {PBT5.PBT5_PAY_DATE} >=  CDATE(" & DatePart("yyyy", CDate(BEGIN_DATE)) & "," & Format(DatePart("m", CDate(BEGIN_DATE)), "0#") & "," & Format(DatePart("d", CDate(BEGIN_DATE)), "0#") & ")" & _
'                                                                                                    " AND  {PBT5.PBT5_PAY_DATE} <=   CDATE(" & DatePart("yyyy", CDate(LASTDATE)) & "," & Format(DatePart("m", CDate(LASTDATE)), "0#") & "," & Format(DatePart("d", CDate(LASTDATE)), "0#") & ")"
'                                                       Case "2"
'                                                                        Txt_Formula = " {PRD2.PRD2_PAY_DATE} >=  CDATE(" & DatePart("yyyy", CDate(BEGIN_DATE)) & "," & Format(DatePart("m", CDate(BEGIN_DATE)), "0#") & "," & Format(DatePart("d", CDate(BEGIN_DATE)), "0#") & ")" & _
'                                                                                            " AND  {PRD2.PRD2_PAY_DATE} <=   CDATE(" & DatePart("yyyy", CDate(LASTDATE)) & "," & Format(DatePart("m", CDate(LASTDATE)), "0#") & "," & Format(DatePart("d", CDate(LASTDATE)), "0#") & ")"
'                                                        Case "3"
'                                                                        Txt_Formula = Txt_Formula & " AND  {PP1.PP1_PAY_DATE} >=  CDATE(" & DatePart("yyyy", CDate(BEGIN_DATE)) & "," & Format(DatePart("m", CDate(BEGIN_DATE)), "0#") & "," & Format(DatePart("d", CDate(BEGIN_DATE)), "0#") & ")" & _
'                                                                                                    " AND  {PP1.PP1_PAY_DATE} <=   CDATE(" & DatePart("yyyy", CDate(LASTDATE)) & "," & Format(DatePart("m", CDate(LASTDATE)), "0#") & "," & Format(DatePart("d", CDate(LASTDATE)), "0#") & ")"
'                                             End Select
                        
'                        If Txt_BOOK_NUMBER.Text <> Empty And Txt_BOOK_NO.Text <> Empty Then
                                                 Select Case LB_Menu.Tag
                                                               Case "1", "5"
                                                                        Txt_Formula = Txt_Formula & "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PBT5.PBT5_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                                                                Case "2", "6"
                                                                        Txt_Formula = Txt_Formula & "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PRD2.PRD2_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                                                                Case "3", "7"
                                                                        Txt_Formula = Txt_Formula & "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PP1.PP1_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                                                                Case "4"
                                                                        Txt_Formula = "({PBA1.PBA1_YEAR} = " & Lb_Year.Caption & " AND {PBA1.PBA1_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PBA1.PBA1_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                                                  End Select
'                         Else
'                                        If Txt_BOOK_NUMBER.Text <> Empty Then
'                                                 Select Case LB_Menu.Tag
'                                                               Case "1"
'                                                                        Txt_Formula = Txt_Formula & " AND ({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Text) & "')"
'                                                                Case "2"
'                                                                        Txt_Formula = Txt_Formula & " AND ({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Text) & "')"
'                                                                Case "3"
'                                                                            Txt_Formula = Txt_Formula & " AND ({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Text) & "')"
'                                                  End Select
'                                        End If
'                                        If Txt_BOOK_NO.Text <> Empty Then
'                                                 Select Case LB_Menu.Tag
'                                                               Case "1"
'                                                                          Txt_Formula = Txt_Formula & " AND {PBT5.PBT5_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Text) & "'"
'                                                                Case "2"
'                                                                        Txt_Formula = Txt_Formula & " AND ({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Text) & "')"
'                                                                Case "3"
'                                                                            Txt_Formula = Txt_Formula & " AND {PP1.PP1_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Text) & "'"
'                                                  End Select
'                                        End If
'                         End If
End Sub

Private Sub Btn_Print_Click()
Dim strSQL As String
Call SetFormulaReport
If Trim$(Txt_BOOK_NUMBER.Tag) = Empty Or Trim$(Txt_BOOK_NO.Tag) = Empty Then Exit Sub
        Select Case LB_Menu.Tag
                    Case "1", "5"
                                Call ShowReport("PBT11.rpt", Txt_Formula, False)
                    Case "2", "6"
                                Call ShowReport("PRD12.rpt", Txt_Formula, False)
                    Case "3", "7"
                                Call ShowReport("PP7.rpt", Txt_Formula, False)
                    Case "4"
                                strSQL = "SELECT BUSINESS_TYPE.BUSINESS_ID,BUSINESS_TYPE.BUSINESS_TYPE" & _
                                                " FROM BUSINESS_TYPE INNER JOIN (CLASS_DETAILS INNER JOIN (PBA1 INNER JOIN LICENSEDATA ON (PBA1.LICENSE_BOOK = LICENSEDATA.LICENSE_BOOK)" & _
                                                " AND (PBA1.LICENSE_NO = LICENSEDATA.LICENSE_NO)) ON CLASS_DETAILS.CLASS_ID = LICENSEDATA.CLASS_ID) ON BUSINESS_TYPE.BUSINESS_ID = CLASS_DETAILS.BUSINESS_ID" & _
                                                " WHERE (((PBA1.PBA1_BOOK_NUMBER)='" & Trim$(Txt_BOOK_NUMBER.Tag) & "') AND ((PBA1.PBA1_BOOK_NO)='" & Trim$(Txt_BOOK_NO.Tag) & "') AND ((PBA1.PBA1_YEAR)=" & Lb_Year.Caption & "))"
                                Call SET_QUERY(strSQL, Query)
                                If Query.RecordCount > 0 Then
                                        Select Case Query.Fields(0).Value
                                                Case "001"   '  ���������
                                                        Call ShowReport("SOR5.rpt", Txt_Formula, False)
                                                Case Else     '  �����
                                                        Call ShowReport("ORP2.rpt", Txt_Formula, False, Query.Fields(1))
                                        End Select
                                End If
        End Select
End Sub

Private Sub Btn_SearchOwnerShip_PBT5_Click()
On Error GoTo ErrSearch
 Dim sql_txt As String
            Lb_MoneyDue.Caption = "0.00"
            Lb_MoneyDue_Perform.Caption = "0.00"
            Select Case LB_Menu.Tag
                     Case "1"
                                 Status_InToFrm = "PBT11"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������պ��ا��ͧ���"
                     Case "2"
                                 Status_InToFrm = "PRD12"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������ç���͹��з��Թ"
                     Case "3"
                                 Status_InToFrm = "PP7"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������ջ���"
                     Case "4"
                                 Status_InToFrm = "PBA1"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������͹حҵ��Сͺ��ä��"
                     Case "5"
                                 Status_InToFrm = "PBT11_OTHER"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������պ��ا��ͧ���(�������)"
                     Case "6"
                                 Status_InToFrm = "PRD12_OTHER"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������ç���͹��з��Թ(�������)"
                     Case "7"
                                 Status_InToFrm = "PP7_OTHER"
                                 Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������ջ���(�������)"
            End Select
            Frm_SerachForProcess.Show vbModal
Debug.Print "exec sp_search_payment '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "','" & Trim$(Txt_Ownership_ID.Text) & "','" & _
            CInt(Lb_Year.Caption) & "','" & CInt(Op_PBT5_NO_ACCEPT.Value) & "','" & LB_Menu.Tag & "'"
Set Query = Globle_Connective.Execute("exec sp_search_payment '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "','" & Trim$(Txt_Ownership_ID.Text) & "','" & _
           CInt(Lb_Year.Caption) & "','" & CInt(Op_PBT5_NO_ACCEPT.Value) & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
If Query.STATE = adStateOpen Then
  If Query.RecordCount > 0 Then
        Set Grid_Result.DataSource = Query
                                  If Query.Fields("DateReturn").Value = Empty Or IsNull(Query.Fields("DateReturn").Value) Then
                                         MaskEdBox1.Text = "__/__/____"
                                  Else
                                         MaskEdBox1.Text = Format$(Query.Fields("DateReturn").Value, "dd/mm/yyyy")
                                  End If
                                  If IsNull(Query.Fields("PERFORM_DATE").Value) Or Query.Fields("PERFORM_DATE").Value = Empty Then
                                         MaskEdBox3.Text = "__/__/____"
                                  Else
                                         MaskEdBox3.Text = Format$(Query.Fields("PERFORM_DATE").Value, "dd/mm/yyyy")
                                  End If
                                 Lb_Count.Caption = Query.RecordCount
                                    
                                Dim i As Integer
                                For i = 1 To Grid_Result.Rows - 1
                                        If Query.Fields("DateReturn").Value = Empty Or IsNull(Query.Fields("DateReturn").Value) Then
                                               Grid_Result.TextMatrix(i, 1) = vbNullString
                                        Else
                                               Grid_Result.TextMatrix(i, 1) = Format$(Query.Fields("DateReturn").Value, "dd/mm/yyyy")
                                        End If
                                        If Grid_Result.TextMatrix(i, 0) = "0" Then
'                                                Grid_Result.TextMatrix(i, 0) = "�ѧ������"
'                                        Else
                                                If Len(Trim$(Grid_Result.TextMatrix(i, 12))) = 0 Then
                                                        Grid_Result.TextMatrix(i, 0) = "�ѧ������"
                                                Else
                                                        Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ"
                                                End If
                                        End If
'                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                        Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 5)
                                        Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
'                                        End If
'                                        If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
'                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
'                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
'                                        End If
'                                        If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
'                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
'                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
'                                        End If
                                        If LB_Menu.Tag = "4" Then
                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 2)
                                        End If
                                        Select Case LB_Menu.Tag
                                                Case "1", "5"
                                                                Grid_Result.TextMatrix(i, 6) = Format$(Grid_Result.TextMatrix(i, 6), "#,##0.00")
                                                Case "2", "6", "3", "7"
                                                                Grid_Result.TextMatrix(i, 5) = Format$(Grid_Result.TextMatrix(i, 5), "#,##0.00")
                                        End Select
                                Next i
                                Call CalOfPay
                                Lb_Name_Now.Caption = Query.Fields("StillName").Value
'                                 If Lb_PBT_NO.Caption = Empty Then
'                                                    '���ҧ�������
'                                                    Select Case LB_Menu.Tag
'                                                                 Case "1", "5"
'                                                                        sql_txt = " SELECT PBT5_NO From PBT5  Where  PBT5_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBT5_NO IS NOT NULL AND PBT5_NO <> '') Group by  PBT5_NO Order by  PBT5_NO "
'                                                                 Case "2", "6"
'                                                                        sql_txt = " SELECT PRD2_NO From PRD2  Where  PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND (PRD2_NO IS NOT NULL AND PRD2_NO <> '') Group by  PRD2_NO Order by  PRD2_NO "
'                                                                 Case "3", "7"
'                                                                        sql_txt = " SELECT PP1_NO From PP1  Where  PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND (PP1_NO IS NOT NULL AND PP1_NO <> '') Group by  PP1_NO Order by  PP1_NO "
''                                                                 Case "4"
''                                                                        sql_txt = " SELECT PBA1_NO From PBA1  Where  PBA1_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBA1_NO IS NOT NULL AND PBA1_NO <> '') Group by  PBA1_NO Order by  PBA1_NO "
'                                                     End Select
'                                                    Call SET_QUERY(sql_txt, Query)
'                                                    If Query.RecordCount > 0 Then
'                                                        Query.MoveLast
'                                                        Select Case LB_Menu.Tag
'                                                                        Case "1", "5"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PBT5_NO").Value, 4)) + 1, "###000#")
'                                                                        Case "2", "6"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PRD2_NO").Value, 4)) + 1, "###000#")
'                                                                        Case "3", "7"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PP1_NO").Value, 4)) + 1, "###000#")
''                                                                         Case "4"
''                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PBA1_NO").Value, 4)) + 1, "###000#")
'                                                        End Select
'                                                                                Lb_PBT_NO.Caption = Lb_PBT_NO.Caption & "/" & Right$(Year(Now), 2)
'                                                    Else
'                                                                                Lb_PBT_NO.Caption = "0001/" & Right$(Year(Now), 2)
'                                                    End If
'                                 End If
'                                 If LB_Menu.Tag = "4" Then Lb_PBT_NO.Caption = Empty
                                 
                                Chk_RoundMoney.Value = Checked
                                Call Set_Grid
  Else
            Call Clear_Grid
  End If
  End If
           Option2.Value = True
Exit Sub
ErrSearch:
            MsgBox Err.Description
End Sub

Private Sub Chk_Installment_Click()
Dim i As Byte
        If Chk_Installment.Value = Checked Then
                Frame2.Visible = False
                Frame3.Visible = True
                With Grid_Installment
                        .Rows = 4
                        .Clear
                        For i = 1 To .Rows - 1
                                .TextMatrix(i, 0) = i
                                .TextMatrix(i, 1) = vbNullString
                                .TextMatrix(i, 2) = vbNullString
                                .TextMatrix(i, 3) = "0.00"
                                .TextMatrix(i, 4) = "0.00"
                                .TextMatrix(i, 5) = "0.00"
                                .TextMatrix(i, 6) = vbNullString
                        Next i
                End With
                Set Query = Globle_Connective.Execute("exec sp_check_first_installment '" & Lb_Year.Caption & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
                If IsNull(Query.Fields(0).Value) = False Then
                        Lb_Number_Installment.Caption = Format$(CInt(Left$(Query.Fields(0).Value, 5)) + 1, "00000") & "/" & Right$(Lb_Year.Caption, 2)
                Else
                        Lb_Number_Installment.Caption = "00001/" & Right$(Lb_Year.Caption, 2)
                End If
                Call Set_Grid_Installment
                Txt_RoundMoney_Installment = Txt_RoundMoney.Text
                Mask_Installment_Start.Enabled = True
                Mask_Installment_Start.SelStart = 0
                Mask_Installment_Start.SetFocus
        Else
                Frame3.Visible = False
                Frame2.Visible = True
                Lb_Installment_No.Caption = vbNullString
                Mask_Installment_Pay.Text = "__/__/____"
                Txt_Money_Installment.Text = "0.00"
                Lb_MoneyDue_Installment.Caption = "0.00"
                Txt_Money_Installment_Pay.Text = "0.00"
                Lb_Number_Installment.Caption = vbNullString
                Chk_RoundMoney2(1).Value = Unchecked
        End If
End Sub

Private Sub Chk_RoundMoney_Click()
   If Chk_RoundMoney.Value Then
            Combo1.Enabled = True
   Else
            Combo1.ListIndex = -1
            Combo1.Enabled = False
            Txt_RoundMoney.Text = Format$(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption), "###,##0.00")
   End If
End Sub

Private Sub Chk_RoundMoney2_Click(Index As Integer)
    If Chk_RoundMoney2(Index).Value Then
            If Index = 0 Then
                    Combo2.Enabled = True
            Else
                    Combo3.Enabled = True
            End If
    Else
            If Index = 0 Then
                    Combo2.ListIndex = -1
                    Combo2.Enabled = False
                    Txt_RoundMoney_Installment.Text = Format$(CCur(Txt_Summary_Installment.Text) + CCur(Lb_MoneyDue2.Caption) + CCur(Lb_MoneyDue_Perform2.Caption), "###,##0.00")
            Else
                    Combo3.ListIndex = -1
                    Combo3.Enabled = False
                    Txt_Money_Installment_Pay.Text = Format$(CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption), "###,##0.00")
            End If
    End If
End Sub

Private Sub Combo1_Click()
On Error GoTo ErrClick
Dim Flag_money  As Single, Temp_Sum As Currency
If CCur(Txt_Summary.Text) = 0 Then Exit Sub
Select Case Combo1.ListIndex
             Case 0
                        Flag_money = 1#
              Case 1
                        Flag_money = 0.25
              Case 2
                        Flag_money = 0.5
              Case 3
                        Flag_money = 0.75
End Select
Temp_Sum = CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption)
            If Chk_RoundMoney.Value Then
                  If Temp_Sum - Int(Temp_Sum) > 0 Then
                        Txt_RoundMoney.Text = FormatNumber(Int(Temp_Sum) + Flag_money, 2)
                   Else
                        Txt_RoundMoney.Text = FormatNumber(Temp_Sum, 2)
                  End If
            End If
                If CCur(Txt_RoundMoney.Text) < CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) Then Txt_RoundMoney.Text = FormatNumber(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption), 2, vbTrue)
                Lb_discount.Caption = FormatNumber(CCur(Txt_RoundMoney.Text) - Summary - CCur(Lb_MoneyDue.Caption) - CCur(Lb_MoneyDue_Perform.Caption), 2, vbTrue)
'                If CCur(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
'End If
Exit Sub
ErrClick:
            MsgBox Err.Description
End Sub

Private Sub combo2_Click()
Dim Flag_money  As Single, Temp_Sum As Currency
If CCur(Txt_Summary_Installment.Text) = 0 Then Exit Sub
Select Case Combo2.ListIndex
             Case 0
                        Flag_money = 1#
              Case 1
                        Flag_money = 0.25
              Case 2
                        Flag_money = 0.5
              Case 3
                        Flag_money = 0.75
End Select
Temp_Sum = CCur(Txt_Summary_Installment.Text) + CCur(Lb_MoneyDue2.Caption) + CCur(Lb_MoneyDue_Perform2.Caption)
            If Chk_RoundMoney2(0).Value Then
                  If Temp_Sum - Int(Temp_Sum) > 0 Then
                        Txt_RoundMoney_Installment.Text = FormatNumber(Int(Temp_Sum) + Flag_money, 2)
                   Else
                        Txt_RoundMoney_Installment.Text = FormatNumber(Temp_Sum, 2)
                  End If
            End If
                If CCur(Txt_RoundMoney_Installment.Text) < CCur(Txt_Summary_Installment.Text) + CCur(Lb_MoneyDue2.Caption) Then Txt_RoundMoney_Installment.Text = FormatNumber(CCur(Txt_Summary_Installment.Text) + CCur(Lb_MoneyDue2.Caption), 2, vbTrue)
                Lb_discount2.Caption = FormatNumber(CCur(Txt_RoundMoney_Installment.Text) - Summary, 2, vbTrue)
'                If CCur(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
'End If
Exit Sub
ErrClick:
            MsgBox Err.Description
End Sub

Private Sub Combo3_Click()
        Dim Flag_money  As Single, Temp_Sum As Currency
        If CCur(Txt_Money_Installment_Pay.Text) = 0 Then Exit Sub
        Select Case Combo3.ListIndex
                     Case 0
                                Flag_money = 1#
                      Case 1
                                Flag_money = 0.25
                      Case 2
                                Flag_money = 0.5
                      Case 3
                                Flag_money = 0.75
        End Select
        Temp_Sum = CCur(Txt_Money_Installment_Pay.Text) + CCur(Lb_MoneyDue_Installment.Caption)
                    If Chk_RoundMoney2(1).Value Then
                          If Temp_Sum - Int(Temp_Sum) > 0 Then
                                Txt_Money_Installment_Pay.Text = FormatNumber(Int(Temp_Sum) + Flag_money, 2)
                           Else
                                Txt_Money_Installment_Pay.Text = FormatNumber(Temp_Sum, 2)
                          End If
                    End If
'                        If CCur(Txt_RoundMoney_Installment.Text) < CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption) Then Txt_Money_Installment_Pay.Text = FormatNumber(CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption), 2, vbTrue)
'                        Lb_discount2.Caption = FormatNumber(CCur(Txt_Money_Installment_Pay.Text) - Summary, 2, vbTrue)
Exit Sub
ErrClick:
            MsgBox Err.Description
End Sub

Private Sub Form_Activate()
      Clone_Form.Picture1.Visible = True
End Sub

Private Sub Form_Load()
        Set Query = New ADODB.Recordset
        Set QueryCfgTax = New ADODB.Recordset
        Set Rs_Check = New ADODB.Recordset
        Lb_Year.Caption = Right$(Date, 4)
        MaskEdBox2.Text = "__/__/____"
        Frame3.Top = 4650
        Frame3.Left = 120
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
        Set QueryCfgTax = Nothing
        Set Rs_Check = Nothing
        Set Frm_Payment = Nothing
End Sub

Private Sub Grid_Installment_DblClick()
        Dim i As Byte, j As Byte
        If Chk_Installment.Enabled = True Then
                If Grid_Installment.TextMatrix(1, Grid_Installment.Cols - 1) <> "/" And Grid_Installment.Row = 1 Then
                        For i = 0 To Grid_Installment.Cols - 1
                                Grid_Installment.Row = 1
                                Grid_Installment.Col = i
                                Grid_Installment.CellBackColor = &HC0C0FF    '  �ժ���
                        Next i
                        Grid_Installment.TextMatrix(1, Grid_Installment.Cols - 1) = "/"
                        Txt_Money_Installment.Text = Grid_Installment.TextMatrix(Grid_Installment.Row, 4)
                        Lb_MoneyDue_Installment.Caption = Grid_Installment.TextMatrix(1, 3)
                        Txt_Money_Installment_Pay.Text = Format$(CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption), "#,##0.00")
                        Mask_Installment_Pay.Enabled = True
                        Mask_Installment_Pay.SetFocus
                Else
                        For i = 0 To Grid_Installment.Cols - 1
                                Grid_Installment.Row = 1
                                Grid_Installment.Col = i
                                Grid_Installment.CellBackColor = &H80000018
                        Next i
                        Grid_Installment.TextMatrix(1, Grid_Installment.Cols - 1) = Empty
                        Mask_Installment_Pay.Text = "__/__/____"
                        Mask_Installment_Pay.Enabled = False
                        Txt_Money_Installment.Text = "0.00"
                        Lb_MoneyDue_Installment.Caption = "0.00"
                        Txt_Money_Installment_Pay.Text = "0.00"
                End If
        Else
                If Grid_Installment.TextMatrix(Grid_Installment.Row, Grid_Installment.Cols - 1) <> "*" Then
                        If Grid_Installment.TextMatrix(Grid_Installment.Row, Grid_Installment.Cols - 1) = Empty And Grid_Installment.TextMatrix(Grid_Installment.Row, 1) <> Empty Then
                                Grid_Installment.Tag = Grid_Installment.Row
                                For j = 1 To 3
                                        For i = 0 To Grid_Installment.Cols - 1
                                                Grid_Installment.Row = j
                                                Grid_Installment.Col = i
                                                Grid_Installment.CellBackColor = &H80000018
                                        Next i
                                        Grid_Installment.TextMatrix(j, 5) = "0.00"
                                        Grid_Installment.TextMatrix(j, 6) = vbNullString
                                Next j
                                Grid_Installment.Row = Grid_Installment.Tag
                                For i = 0 To Grid_Installment.Cols - 1
                                        Grid_Installment.Row = Grid_Installment.Row
                                        Grid_Installment.Col = i
                                        Grid_Installment.CellBackColor = &HC0C0FF    '  �ժ���
                                Next i
                                Grid_Installment.TextMatrix(Grid_Installment.Row, Grid_Installment.Cols - 1) = "/"
                                Txt_Money_Installment.Text = Grid_Installment.TextMatrix(Grid_Installment.Row, 4)
                                Lb_MoneyDue_Installment.Caption = Grid_Installment.TextMatrix(Grid_Installment.Row, 3)
                                Txt_Money_Installment_Pay.Text = Format$(CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption), "#,##0.00")
                                Mask_Installment_Pay.Enabled = True
                                Mask_Installment_Pay.SetFocus
                        Else
                                For i = 0 To Grid_Installment.Cols - 1
                                        Grid_Installment.Row = Grid_Installment.Row
                                        Grid_Installment.Col = i
                                        Grid_Installment.CellBackColor = &H80000018
                                Next i
                                Grid_Installment.TextMatrix(Grid_Installment.Row, Grid_Installment.Cols - 1) = Empty
                                Mask_Installment_Pay.Text = "__/__/____"
                                Mask_Installment_Pay.Enabled = False
                                Txt_Money_Installment.Text = "0.00"
                                Lb_MoneyDue_Installment.Caption = "0.00"
                                Txt_Money_Installment_Pay.Text = "0.00"
                        End If
                        Lb_Installment_No.Caption = Grid_Installment.Row
                End If
        End If
End Sub

Private Sub Grid_Result_DblClick()
        Dim j As Integer
        Dim i As Integer, n As Integer, m As Integer
        If Grid_Result.TextMatrix(1, 0) = Empty Then Exit Sub
        Lb_MoneyDue.Caption = "0.00"
        Lb_DateOver.Caption = "0"
        If Trim$(Grid_Result.TextMatrix(Grid_Result.Row, 0)) = "�ѧ������" Then
                        n = 0
                        m = Grid_Result.Row
                If LB_Menu.Tag = "4" Then
                        Grid_Result.Col = 1
                        If Grid_Result.CellBackColor = &HC0C0FF Then
                                For i = 0 To Grid_Result.Cols - 1
                                        Grid_Result.Row = Grid_Result.Row
                                        Grid_Result.Col = i
                                        Grid_Result.CellBackColor = &H80000018
                                Next i
                                Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                                Summary = 0
                                Txt_Summary.Text = "0.00"
                                Txt_RoundMoney.Text = "0.00"
                                Exit Sub
                        End If
                        For i = 0 To Grid_Result.Rows - 1
                                Grid_Result.Row = i
                                Grid_Result.Col = 1
                                If Grid_Result.CellBackColor = &HC0C0FF Then n = n + 1
                        Next i
                        If n < 1 Then
                                If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                                           For i = 0 To Grid_Result.Cols - 1
                                                   Grid_Result.Row = m   '   Grid_Result.Row
                                                   Grid_Result.Col = i
                                                   Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                                           Next i
                                            Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = "/"
                                Else
                                           For i = 0 To Grid_Result.Cols - 1
                                                   Grid_Result.Row = Grid_Result.Row
                                                   Grid_Result.Col = i
                                                   Grid_Result.CellBackColor = &H80000018
                                           Next i
                                             Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                                End If
                                        Grid_Result.ColAlignment(Grid_Result.Cols - 1) = 5
                                        Call CalOfPay
                        End If
                Else
                        If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                                   For i = 0 To Grid_Result.Cols - 1
                                           Grid_Result.Row = Grid_Result.Row
                                           Grid_Result.Col = i
                                           Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                                   Next i
                                    Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = "/"
                        Else
                                   For i = 0 To Grid_Result.Cols - 1
                                           Grid_Result.Row = Grid_Result.Row
                                           Grid_Result.Col = i
                                           Grid_Result.CellBackColor = &H80000018
                                   Next i
                                     Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                        End If
                                Grid_Result.ColAlignment(Grid_Result.Cols - 1) = 5
                                Call CalOfPay
                End If
                Frame3.Visible = False
                Frame2.Visible = True
                Txt_RoundMoney_Installment.Locked = False
        ElseIf Trim$(Grid_Result.TextMatrix(Grid_Result.Row, 0)) = "�����繧Ǵ" Then
                Lb_Installment_No.Tag = ""
                Chk_Installment.Tag = ""
                Lb_Number_Installment.Caption = Grid_Result.TextMatrix(Grid_Result.Row, 12)
                If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                        For i = 1 To Grid_Result.Rows - 1
                                If Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ" And Grid_Result.TextMatrix(i, 12) = Lb_Number_Installment.Caption Then
                                        For j = 0 To Grid_Result.Cols - 1
                                                Grid_Result.Row = i
                                                Grid_Result.Col = j
                                                Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                                        Next j
                                        Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = "/"
                                End If
                        Next i
                        Set Query = Globle_Connective.Execute("exec sp_search_notice_date '" & Lb_Number_Installment.Caption & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
                        If Query.RecordCount > 0 Then
                                Mask_Installment_Start.Text = Format$(Query.Fields(0).Value, "dd/mm/yyyy")
                                If InStr(1, Mask_Installment_Start.Text, "_", vbTextCompare) > 0 Then Exit Sub
                                If IsDate(Mask_Installment_Start.Text) And Mid$(Mask_Installment_Start.Text, 4, 2) < 13 Then
'                                        If LB_Menu.Tag <> "4" Then Call CheckForPBT5
                                        Call CalOfPay2
                                        Set Query = Globle_Connective.Execute("exec sp_get_Installment '" & Lb_Number_Installment.Caption & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
                                        If Query.RecordCount > 0 Then
                                                For i = 1 To 3
                                                        Grid_Installment.TextMatrix(i, 0) = Query.Fields(0).Value
                                                        Grid_Installment.TextMatrix(i, 1) = Format$(Query.Fields(1).Value, "dd/mm/yyyy")
                                                        Grid_Installment.TextMatrix(i, 2) = IIf(IsNull(Query.Fields(2).Value), "", Query.Fields(2).Value)
                                                        Grid_Installment.TextMatrix(i, 3) = Format$(Query.Fields(3).Value, "##,##0.00")
                                                        Grid_Installment.TextMatrix(i, 4) = Format$(Query.Fields(4).Value, "##,##0.00")
                                                        Grid_Installment.TextMatrix(i, 5) = Format$(Query.Fields(5).Value, "##,##0.00")
                                                        Grid_Installment.TextMatrix(i, 6) = IIf(Query.Fields(6).Value = 0, "", "*")
                                                        If Grid_Installment.TextMatrix(i, 6) = "" And Lb_Installment_No.Tag <> "1" Then
                                                                Lb_Installment_No.Caption = Grid_Installment.TextMatrix(i, 0)
                                                                Lb_Installment_No.Tag = "1"
                                                                For j = 0 To Grid_Installment.Cols - 1
                                                                        Grid_Installment.Row = Grid_Installment.TextMatrix(i, 0)
                                                                        Grid_Installment.Col = j
                                                                        Grid_Installment.CellBackColor = &HC0C0FF    '  �ժ���
                                                                Next j
                                                                Grid_Installment.TextMatrix(Grid_Installment.TextMatrix(i, 0), Grid_Installment.Cols - 1) = "/"
                                                        End If
                                                        Query.MoveNext
                                                Next i
                                                Call Set_Grid_Installment
                                        End If
'                                        Grid_Installment.TextMatrix(1, 1) = Format$(DateAdd("d", 30, Mask_Installment_Start.Text), "dd/mm/yyyy")
'                                        Grid_Installment.TextMatrix(2, 1) = Format$(DateAdd("d", 30, Grid_Installment.TextMatrix(1, 1)), "dd/mm/yyyy")
'                                        Grid_Installment.TextMatrix(3, 1) = Format$(DateAdd("d", 30, Grid_Installment.TextMatrix(2, 1)), "dd/mm/yyyy")
'                                        If Len(Trim$(Txt_RoundMoney_Installment)) > 0 Then
'                                                On Error Resume Next
'                                                Grid_Installment.TextMatrix(1, 4) = Format$(CCur(Txt_RoundMoney_Installment) \ 3, "#,##0.00")
'                                                Grid_Installment.TextMatrix(2, 4) = Grid_Installment.TextMatrix(1, 4)
'                                                Grid_Installment.TextMatrix(3, 4) = Format$(CCur(Txt_RoundMoney_Installment) - (CCur(Grid_Installment.TextMatrix(1, 4) + CCur(Grid_Installment.TextMatrix(2, 4)))), "#,##0.00")
'                                        End If
'                                        Lb_Installment_No = "1"
'                                Else
'                                        MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
'                                        Mask_Installment_Start.SetFocus
                                End If
                        End If
                        Frame3.Visible = True
                        Frame2.Visible = False
                        Txt_RoundMoney_Installment.Locked = True
                        Mask_Installment_Start.Enabled = False
                        Chk_Installment.Enabled = False
                Else
                        For i = 0 To Grid_Result.Rows - 1
                                If Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ" Then
                                        For j = 0 To Grid_Result.Cols - 1
                                                Grid_Result.Row = i
                                                Grid_Result.Col = j
                                                Grid_Result.CellBackColor = &H80000018
                                        Next j
                                        Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                                End If
                        Next i
                        Frame3.Visible = False
                        Frame2.Visible = True
                        Call Reset_Payment
                End If
        '     MsgBox "��������¡�ù������������� !  �������ö�ӡ�ê���������", vbCritical, "MicroTax Software"
        End If
End Sub

Private Sub Lb_DateOver_Change()
  On Error Resume Next
  If CInt(Lb_DateOver.Caption) < 0 Then
        Lb_DateOver.Caption = "0"
  End If
  If LB_Menu.Tag <> "3" Or LB_Menu.Tag <> "5" Then Lb_MoneyDue.Caption = "0.00"
End Sub

Private Sub Lb_discount_Change()
On Error GoTo ErrChange
If Lb_discount.Caption > 0 Then
      Label3(4).Caption = "�Թ���� :"
End If
If Lb_discount.Caption < 0 Then
     Label3(4).Caption = "�ԹŴ :"
End If
If Left$(Trim$(Lb_discount.Caption), 1) <> "+" Then
        If CCur(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
End If
Lb_discount2.Caption = Lb_discount.Caption
Exit Sub
ErrChange:
        MsgBox Err.Description
End Sub

Private Sub Lb_discount2_Change()
On Error GoTo ErrChange
If Lb_discount2.Caption > 0 Then
      Label3(20).Caption = "�Թ���� :"
End If
If Lb_discount2.Caption < 0 Then
     Label3(20).Caption = "�ԹŴ :"
End If
If Left$(Trim$(Lb_discount2.Caption), 1) <> "+" Then
        If CCur(Lb_discount2.Caption) > 0 Then Lb_discount2.Caption = "+" & Lb_discount2.Caption
End If
Exit Sub
ErrChange:
        MsgBox Err.Description
End Sub

Private Sub LB_Menu_Change()
    Lb_PBT_NO.Caption = Empty
    Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
    Lb_DateOver.Caption = "0"
    Lb_MoneyDue.Caption = "0.00"
    MaskEdBox1.Text = "__/__/____"
    MaskEdBox3.Text = "__/__/____"
    Lb_Count.Caption = "0"
    Txt_Summary.Text = "0.00"
    Lb_Name_Now.Caption = Empty
    Txt_Ownership_ID.Text = Empty
    Call Clear_Grid
    Call Set_Grid
End Sub

Private Sub Lb_MoneyDue_Change()
        Lb_MoneyDue2.Caption = Lb_MoneyDue.Caption
End Sub

Private Sub Lb_MoneyDue_Perform_Change()
        Lb_MoneyDue_Perform2.Caption = Lb_MoneyDue_Perform.Caption
End Sub

Private Sub Lb_Year_Change()
        Call Clear_Grid
        Call Set_Grid
End Sub

Private Sub Mask_Installment_Pay_KeyPress(KeyAscii As Integer)
        On Error Resume Next
        If KeyAscii = 13 Then
                Dim i As Byte, j As Byte
                If InStr(1, Mask_Installment_Pay.Text, "_", vbTextCompare) > 0 Then
        '               Txt_Money_Installment_Pay.Text = "0.00"
                        Exit Sub
                End If
                If IsDate(Mask_Installment_Pay.Text) And Mid$(Mask_Installment_Pay.Text, 4, 2) < 13 Then
                        
        '                Lb_Installment_No.Caption = vbNullString
                        For i = 1 To 3
                                For j = 0 To Grid_Installment.Cols - 1
                                        Grid_Installment.Row = i
                                        Grid_Installment.Col = j
                                        Grid_Installment.CellBackColor = &H80000018
                                Next j
                                Grid_Installment.TextMatrix(Grid_Installment.Row, Grid_Installment.Cols - 1) = Empty
                        Next i
                        For i = 1 To 3
                                If (CDate(Mask_Installment_Pay.Text) < CDate(Grid_Installment.TextMatrix(i + 1, 1))) Then '����ѹ�����й��¡����ѹ����˹�������
                                        For j = 0 To Grid_Installment.Cols - 1
                                                Grid_Installment.Row = i
                                                Grid_Installment.Col = j
                                                Grid_Installment.CellBackColor = &HC0C0FF    '  �ժ���
                                        Next j
                                        Grid_Installment.TextMatrix(i, 5) = Grid_Installment.TextMatrix(i, 4)
                                        Grid_Installment.TextMatrix(i, Grid_Installment.Cols - 1) = "/"
                                        Lb_Installment_No.Caption = i
                                        Exit For
                                End If
                        Next i
                Else
                        MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                        Mask_Installment_Pay.SetFocus
                End If
        End If
End Sub

Private Sub Mask_Installment_Pay_LostFocus()
        Dim i As Byte
        If InStr(1, Mask_Installment_Pay.Text, "_", vbTextCompare) > 0 Then
'                Txt_Money_Installment_Pay.Text = "0.00"
                Exit Sub
        End If
        If IsDate(Mask_Installment_Pay.Text) And Mid$(Mask_Installment_Pay.Text, 4, 2) < 13 Then
'                Lb_Installment_No.Caption = vbNullString
'                For i = 1 To 3
'                        If Grid_Installment.TextMatrix(i, 2) = Empty Then
'                                If Lb_Installment_No.Caption = vbNullString Then
'                                        Lb_Installment_No.Caption = Grid_Installment.TextMatrix(i, 0)
'                                        Txt_Money_Installment.Text = Grid_Installment.TextMatrix(i, 4)
'                                        Lb_MoneyDue_Installment.Caption = Grid_Installment.TextMatrix(i, 3)
'                                        Txt_Money_Installment_Pay.Text = Format$(CCur(Txt_Money_Installment.Text) + CCur(Lb_MoneyDue_Installment.Caption), "#,##0.00")
'                                End If
'                        End If
'                Next i
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                Mask_Installment_Pay.SetFocus
        End If
End Sub

Private Sub Mask_Installment_Start_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Call Mask_Installment_Start_LostFocus
        End If
End Sub

Private Sub Mask_Installment_Start_LostFocus()
        If InStr(1, Mask_Installment_Start.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(Mask_Installment_Start.Text) And Mid$(Mask_Installment_Start.Text, 4, 2) < 13 Then
'                If LB_Menu.Tag <> "4" Then Call CheckForPBT5
                MaskEdBox2.Text = Mask_Installment_Start.Text
                Call CalOfPay
                Grid_Installment.TextMatrix(1, 1) = Format$(DateAdd("d", 30, Mask_Installment_Start.Text), "dd/mm/yyyy")
                Grid_Installment.TextMatrix(2, 1) = Format$(DateAdd("d", 30, Grid_Installment.TextMatrix(1, 1)), "dd/mm/yyyy")
                Grid_Installment.TextMatrix(3, 1) = Format$(DateAdd("d", 30, Grid_Installment.TextMatrix(2, 1)), "dd/mm/yyyy")
                If Len(Trim$(Txt_RoundMoney_Installment)) > 0 Then
                        On Error Resume Next
                        Grid_Installment.TextMatrix(1, 4) = Format$(CCur(Txt_RoundMoney_Installment) \ 3, "#,##0.00")
                        Grid_Installment.TextMatrix(2, 4) = Grid_Installment.TextMatrix(1, 4)
                        Grid_Installment.TextMatrix(3, 4) = Format$(CCur(Txt_RoundMoney_Installment) - (CCur(Grid_Installment.TextMatrix(1, 4) + CCur(Grid_Installment.TextMatrix(2, 4)))), "#,##0.00")
                End If
                Lb_Installment_No = "1"
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                Mask_Installment_Start.SetFocus
        End If
End Sub

Private Sub MaskEdBox1_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub MaskEdBox2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub MaskEdBox2_LostFocus()
        If InStr(1, MaskEdBox2.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(MaskEdBox2.Text) And Mid$(MaskEdBox2.Text, 4, 2) < 13 Then
                If LB_Menu.Tag <> "4" Then Call CheckForPBT5
                Call CalOfPay
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox2.SetFocus
        End If
End Sub

Private Sub Op_PBT5_NO_ACCEPT_Click()
        Call Clear_Grid
'Txt_PBT_NO_ACCEPT_OLD.Locked = True
End Sub

Private Sub Op_PBT5_NO_Click()
        Call Clear_Grid
'Txt_PBT_NO_ACCEPT_OLD.Locked = False
End Sub

Private Sub Option1_Click()
        On Error Resume Next
         If Grid_Result.TextMatrix(1, 0) = Empty Or LB_Menu.Tag = "4" Then Exit Sub
        Call CheckForPBT5
        Call CalOfPay
        MaskEdBox2.SelStart = 0
        MaskEdBox2.SetFocus
End Sub

Private Sub Option2_Click()
         If Grid_Result.TextMatrix(1, 0) = Empty Or LB_Menu.Tag = "4" Then Exit Sub
'         chkInstallment.Enabled = False
        Call CheckForPBT5
        Summary = 0
        Lb_Count.Caption = "0"
        Txt_Summary.Text = "0.00"
        Txt_RoundMoney.Text = "0.00"
End Sub

Private Sub Txt_BOOK_NO_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BOOK_NO_LostFocus()
        Txt_BOOK_NO.Tag = Txt_BOOK_NO.Text
End Sub

Private Sub Txt_BOOK_NUMBER_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BOOK_NUMBER_LostFocus()
        Txt_BOOK_NUMBER.Tag = Txt_BOOK_NUMBER.Text
End Sub

Private Sub Txt_Money_Installment_Pay_Change()
        On Error Resume Next
        Grid_Installment.TextMatrix(Lb_Installment_No.Caption, 5) = Txt_Money_Installment_Pay.Text
End Sub

Private Sub Txt_Money_Installment_Pay_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
        If FindText(Txt_Money_Installment_Pay.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
End Sub

Private Sub Txt_Money_Installment_Pay_LostFocus()
        Txt_Money_Installment_Pay.BackColor = &HC0C0C0
        If Trim$(Txt_Money_Installment_Pay.Text) = "." Then Txt_Money_Installment_Pay.Text = "0.00"
End Sub

Private Sub Txt_PBT_NO_ACCEPT_Change()
 If Grid_Result.TextMatrix(1, 1) <> Empty Or Trim$(Txt_PBT_NO_ACCEPT) = Empty Then
    Grid_Result.Rows = 2
    Grid_Result.Clear
    Call Set_Grid
    Call reset_Installment
    Lb_PBT_NO.Caption = Empty
    Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
    Lb_DateOver.Caption = "0"
    Lb_MoneyDue.Caption = "0.00"
    Lb_discount.Caption = "0.00"
    MaskEdBox1.Text = "__/__/____"
    MaskEdBox2.Text = "__/__/____"
    MaskEdBox3.Text = "__/__/____"
    Lb_Count.Caption = "0"
    Summary = 0
    Txt_Summary.Text = "0.00"
    Txt_BOOK_NO.Text = Empty
    Txt_BOOK_NUMBER.Text = Empty
    Lb_Name_Now.Caption = Empty
    Label3(4).Caption = "�Թ����/Ŵ :"
End If
End Sub

Private Sub Txt_PBT_NO_ACCEPT_DblClick()
        Txt_PBT_NO_ACCEPT.Text = Empty
End Sub

Private Sub Txt_PBT_NO_ACCEPT_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 And LenB(Trim$(Txt_PBT_NO_ACCEPT.Text)) <> 0 Then
            Call Btn_SearchOwnerShip_PBT5_Click
        End If
End Sub


Private Sub Txt_PBT_NO_ACCEPT_OLD_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_PBT_NO_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_RoundMoney_Change()
        On Error Resume Next
        Txt_RoundMoney_Installment.Text = Txt_RoundMoney.Text
        If Txt_RoundMoney.Text = "" Then Txt_RoundMoney.Text = 0
'        If Lb_MoneyDue_Perform = "" Then Lb_MoneyDue_Perform.Caption = "0"
                    Lb_discount.Caption = FormatNumber(CCur(Txt_RoundMoney.Text) - (Summary + CCur(Lb_MoneyDue.Caption)), 2, vbTrue)
'                    If CCur(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
'                    Txt_Summary.Text = FormatNumber((Txt_RoundMoney.Text) - (CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption)), 2, vbTrue)
        If CCur(Val(Txt_RoundMoney.Text)) = 0 Then
                Chk_Installment.Enabled = False
                Chk_Installment.Value = 0
                Frame2.Visible = True
                Frame3.Visible = False
                Exit Sub
        End If
        Select Case LB_Menu.Tag
                Case "1"
                        If CCur(Val(Txt_RoundMoney.Text)) > 10 And Len(Trim$(Txt_PBT_NO_ACCEPT)) > 0 Then
                                Chk_Installment.Enabled = True
                        Else
                                Chk_Installment.Enabled = False
                        End If
                Case "2"
                        If CCur(Val(Txt_RoundMoney.Text)) > 3000 And Len(Trim$(Txt_PBT_NO_ACCEPT)) > 0 Then
                                Chk_Installment.Enabled = True
                        Else
                                Chk_Installment.Enabled = False
                        End If
                Case "3"
                        If CCur(Val(Txt_RoundMoney.Text)) > 4000 And Len(Trim$(Txt_PBT_NO_ACCEPT)) > 0 Then
                                Chk_Installment.Enabled = True
                        Else
                                Chk_Installment.Enabled = False
                        End If
        End Select
        Txt_RoundMoney_Installment.Text = Txt_RoundMoney.Text
'        If Chk_RoundMoney.Value Then
'                  Call Combo1_Click
'        Else
'                Txt_Summary.Text = Format$(CCur(Txt_RoundMoney.Text) - CCur(Lb_MoneyDue.Caption), "###,##0.00")
'        End If
'        If Option1.Value = True Then
'                Select Case LB_Menu.Tag
'                        Case "1" '���ا��ͧ���
'                                If CDbl(Txt_Summary) >= 3000 Then
'                                        chkInstallment.Enabled = True
'                                End If
'                        Case "2" '�ç���͹��з��Թ
'                                If CDbl(Txt_Summary) >= 9000 Then
'                                        chkInstallment.Enabled = True
'                                End If
'                        Case "3" '����
'                                If CDbl(Txt_Summary) >= 3000 Then
'                                        chkInstallment.Enabled = True
'                                End If
'                End Select
'        End If
End Sub

Private Sub Txt_RoundMoney_GotFocus()
        Txt_RoundMoney.BackColor = &H80000005
        Txt_RoundMoney.SelStart = 0
        Txt_RoundMoney.SelLength = Len(Txt_RoundMoney.Text)
End Sub

Private Sub Txt_RoundMoney_Installment_Change()
        If Frame3.Visible = True Then
                Call Mask_Installment_Start_LostFocus
        End If
End Sub

Private Sub Txt_RoundMoney_Installment_GotFocus()
        Txt_RoundMoney_Installment.BackColor = &H80000005
        Txt_RoundMoney_Installment.SelStart = 0
        Txt_RoundMoney_Installment.SelLength = Len(Txt_RoundMoney_Installment.Text)
End Sub

Private Sub Txt_RoundMoney_Installment_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
        If FindText(Txt_RoundMoney_Installment.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
End Sub

Private Sub Txt_RoundMoney_Installment_LostFocus()
        If Chk_Installment.Enabled = True Then
                Call Mask_Installment_Start_LostFocus
        End If
End Sub

Private Sub Txt_RoundMoney_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
        If FindText(Txt_RoundMoney.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
End Sub

Private Sub Txt_RoundMoney_LostFocus()
        Txt_RoundMoney.BackColor = &HC0C0C0
        If Trim$(Txt_RoundMoney.Text) = "." Then Txt_RoundMoney.Text = "0"
End Sub

Private Sub Txt_Summary_Change()
        On Error Resume Next
        If Txt_Summary.Text = Empty Then Txt_Summary.Text = 0
        Txt_Summary_Installment.Text = Txt_Summary.Text
'
'                    Lb_discount.Caption = Format$(CDbl(Txt_Summary.Text) - Summary, "###,###,##0.00")
'                    If CDbl(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
'                    Txt_RoundMoney.Text = FormatNumber(CDbl(Txt_Summary.Text) + CDbl(Lb_MoneyDue.Caption), 2)
'
'        If CSng(Val(Txt_Summary.Text)) = 0 Then Exit Sub
'
'        If Chk_RoundMoney.Value Then
'                  Call Combo1_Click
'        Else
'                  Txt_RoundMoney.Text = FormatNumber(CSng(Txt_Summary.Text) + CSng(Lb_MoneyDue.Caption), 2)
'        End If
End Sub

Private Sub UpDown1_Change()
        If Txt_PBT_NO_ACCEPT.Text = Empty Then Exit Sub
End Sub
