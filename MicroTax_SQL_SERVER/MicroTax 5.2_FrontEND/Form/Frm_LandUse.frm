VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_LandUse 
   ClientHeight    =   9105
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12870
   ControlBox      =   0   'False
   Icon            =   "Frm_LandUse.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   Picture         =   "Frm_LandUse.frx":151A
   ScaleHeight     =   9105
   ScaleWidth      =   12870
   WindowState     =   2  'Maximized
   Begin VB.TextBox Txt_SumTax 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   10860
      TabIndex        =   125
      Text            =   "0.00"
      Top             =   6600
      Width           =   1455
   End
   Begin VB.ComboBox cb_Land_Change_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   420
      Style           =   2  'Dropdown List
      TabIndex        =   123
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ComboBox cb_Land_Change 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4140
      Style           =   2  'Dropdown List
      TabIndex        =   122
      Top             =   900
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.ComboBox Cmb_LandScript 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":1937B
      Left            =   9900
      List            =   "Frm_LandUse.frx":19385
      Style           =   2  'Dropdown List
      TabIndex        =   119
      Top             =   5400
      Width           =   2445
   End
   Begin VB.TextBox Txt_B_Va 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7410
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   15
      Text            =   "0"
      Top             =   2640
      Width           =   465
   End
   Begin VB.TextBox Txt_B_Pot 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   6450
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   14
      Text            =   "0"
      Top             =   2640
      Width           =   435
   End
   Begin VB.TextBox Txt_B_Rai 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   5610
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   13
      Text            =   "0"
      Top             =   2640
      Width           =   435
   End
   Begin VB.TextBox Txt_A_Va 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7410
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   12
      Text            =   "0"
      Top             =   1800
      Width           =   465
   End
   Begin VB.TextBox Txt_A_Pot 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   6450
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   11
      Text            =   "0"
      Top             =   1800
      Width           =   435
   End
   Begin VB.TextBox Txt_A_Rai 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   5610
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   10
      Text            =   "0"
      Top             =   1800
      Width           =   435
   End
   Begin VB.ComboBox Cmb_LM_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":193AD
      Left            =   6810
      List            =   "Frm_LandUse.frx":193AF
      Style           =   2  'Dropdown List
      TabIndex        =   115
      Top             =   5490
      Visible         =   0   'False
      Width           =   1260
   End
   Begin VB.CheckBox Chk_Flag_PayTax0 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   10470
      TabIndex        =   113
      Top             =   4200
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_PayTax1 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   9090
      TabIndex        =   111
      Top             =   4200
      Width           =   195
   End
   Begin VB.TextBox Txt_StoryRemark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4140
      TabIndex        =   109
      Text            =   "-"
      Top             =   900
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.PictureBox picParent 
      BackColor       =   &H006E6B68&
      Height          =   2145
      Left            =   10560
      ScaleHeight     =   2085
      ScaleWidth      =   2235
      TabIndex        =   106
      Top             =   1260
      Width           =   2295
      Begin VB.PictureBox picPicture 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   2085
         Left            =   0
         MouseIcon       =   "Frm_LandUse.frx":193B1
         ScaleHeight     =   2085
         ScaleWidth      =   2235
         TabIndex        =   107
         Top             =   0
         Width           =   2235
      End
      Begin VB.Image ImgSize 
         Height          =   1305
         Left            =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.ComboBox Cmb_Street_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":196BB
      Left            =   1740
      List            =   "Frm_LandUse.frx":196BD
      TabIndex        =   101
      Top             =   30
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.ComboBox Cmb_Village_ID 
      Height          =   315
      Left            =   1170
      TabIndex        =   100
      Top             =   60
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.ComboBox Cmb_Tambon_ID 
      Height          =   315
      Left            =   450
      TabIndex        =   99
      Top             =   60
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.CommandButton Btn_Add_OwnerUse 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   12360
      Picture         =   "Frm_LandUse.frx":196BF
      Style           =   1  'Graphical
      TabIndex        =   92
      Top             =   7290
      Width           =   405
   End
   Begin VB.TextBox Txt_Remark 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7620
      Locked          =   -1  'True
      TabIndex        =   26
      Text            =   "-"
      Top             =   6960
      Width           =   4695
   End
   Begin VB.TextBox Txt_LandData_Year 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   10860
      Locked          =   -1  'True
      MaxLength       =   9
      TabIndex        =   22
      Text            =   "0.00"
      Top             =   6240
      Width           =   1455
   End
   Begin VB.TextBox Txt_Land_Price 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   1500
      Locked          =   -1  'True
      TabIndex        =   20
      Text            =   "0.00"
      Top             =   4650
      Width           =   1155
   End
   Begin VB.CheckBox Chk_Flag_StreetN 
      Caption         =   "Check6"
      Height          =   195
      Left            =   11640
      TabIndex        =   84
      Top             =   3750
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_StreetY 
      Caption         =   "Check5"
      Height          =   195
      Left            =   10470
      TabIndex        =   83
      Top             =   3750
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_Apply0 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Left            =   7590
      TabIndex        =   78
      Top             =   6270
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_Apply1 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Left            =   8550
      TabIndex        =   77
      Top             =   6270
      Width           =   195
   End
   Begin VB.TextBox Txt_Apply_Va 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   9750
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   25
      Text            =   "0"
      Top             =   6600
      Width           =   465
   End
   Begin VB.TextBox Txt_Apply_Pot 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8640
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   24
      Text            =   "0"
      Top             =   6600
      Width           =   435
   End
   Begin VB.TextBox Txt_Apply_Rai 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7620
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   23
      Text            =   "0"
      Top             =   6600
      Width           =   435
   End
   Begin VB.ComboBox Cmb_LandUse 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":19DA9
      Left            =   7560
      List            =   "Frm_LandUse.frx":19DAB
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   5790
      Width           =   4785
   End
   Begin VB.CheckBox Chk_Flag_Assess1 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   6390
      TabIndex        =   67
      Top             =   4710
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_Assess0 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   3570
      TabIndex        =   66
      Top             =   4710
      Value           =   1  'Checked
      Width           =   210
   End
   Begin VB.ComboBox Cmb_Soi 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":19DAD
      Left            =   8340
      List            =   "Frm_LandUse.frx":19DAF
      TabIndex        =   19
      Top             =   3630
      Width           =   1845
   End
   Begin VB.ComboBox Cmb_Street 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":19DB1
      Left            =   6000
      List            =   "Frm_LandUse.frx":19DB3
      TabIndex        =   18
      Top             =   3630
      Width           =   1785
   End
   Begin VB.ComboBox Cmb_Village 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":19DB5
      Left            =   3780
      List            =   "Frm_LandUse.frx":19DB7
      TabIndex        =   17
      Top             =   3630
      Width           =   1665
   End
   Begin VB.ComboBox Cmb_Tambon 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":19DB9
      Left            =   720
      List            =   "Frm_LandUse.frx":19DBB
      TabIndex        =   16
      Top             =   3630
      Width           =   1965
   End
   Begin VB.CommandButton Btn_FullScreen 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   285
      Left            =   10200
      Picture         =   "Frm_LandUse.frx":19DBD
      Style           =   1  'Graphical
      TabIndex        =   51
      Top             =   3120
      Width           =   345
   End
   Begin VB.CommandButton Btn_SelectImg 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_LandUse.frx":1A347
      Style           =   1  'Graphical
      TabIndex        =   50
      Top             =   2820
      Width           =   345
   End
   Begin VB.TextBox Txt_Land_Rawang 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3840
      Locked          =   -1  'True
      MaxLength       =   15
      TabIndex        =   9
      Top             =   2640
      Width           =   1485
   End
   Begin VB.TextBox Txt_Land_Survey 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1200
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   8
      Top             =   2640
      Width           =   1455
   End
   Begin VB.TextBox Txt_Land_Number 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3840
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   7
      Top             =   2250
      Width           =   1485
   End
   Begin VB.TextBox Txt_Land_Notic 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1200
      Locked          =   -1  'True
      MaxLength       =   10
      TabIndex        =   6
      Top             =   2250
      Width           =   1455
   End
   Begin VB.TextBox Txt_Land_ID2 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2940
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   3
      Top             =   1380
      Width           =   435
   End
   Begin VB.TextBox Txt_Land_ID1 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2220
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   2
      Top             =   1380
      Width           =   435
   End
   Begin VB.ComboBox Cmb_Tamlay 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":1A8D1
      Left            =   3780
      List            =   "Frm_LandUse.frx":1A911
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1800
      Width           =   1575
   End
   Begin VB.ComboBox CMB_TYPE_LAND 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":1A965
      Left            =   1140
      List            =   "Frm_LandUse.frx":1A984
      TabIndex        =   4
      Top             =   1800
      Width           =   1575
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1140
      TabIndex        =   1
      Top             =   1350
      Width           =   1000
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":1A9CA
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_LandUse.frx":1B231
      Style           =   1  'Graphical
      TabIndex        =   34
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":1DB28
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_LandUse.frx":1E3BF
      Style           =   1  'Graphical
      TabIndex        =   33
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_LandUse.frx":20CA4
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_LandUse.frx":21507
      Style           =   1  'Graphical
      TabIndex        =   32
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":23F2C
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_LandUse.frx":247DE
      Style           =   1  'Graphical
      TabIndex        =   31
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":27219
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_LandUse.frx":27B08
      Style           =   1  'Graphical
      TabIndex        =   30
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":2A663
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_LandUse.frx":2AE91
      Style           =   1  'Graphical
      TabIndex        =   29
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_LandUse.frx":2D71B
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_LandUse.frx":2DF84
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin MSComCtl2.DTPicker DTP_Start_Day 
      Height          =   330
      Left            =   8220
      TabIndex        =   27
      Top             =   7290
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_LandUse.frx":3077C
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   76349441
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComCtl2.DTPicker DTP_End_Day 
      Height          =   330
      Left            =   10830
      TabIndex        =   28
      Top             =   7290
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_LandUse.frx":30A96
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   76349441
      CurrentDate     =   38352
      MaxDate         =   94234
      MinDate         =   20821
   End
   Begin MSComctlLib.ListView LstV_OwnerUse 
      Height          =   1365
      Left            =   6630
      TabIndex        =   103
      Top             =   7680
      Width           =   6165
      _ExtentX        =   10874
      _ExtentY        =   2408
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   16
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "������"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "�ѡɳ�"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "���"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "�ҹ"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "��"
         Object.Width           =   917
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "��Ť������"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "�ѹ��������"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "�ѹ����ش"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "��������»�"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "�����˵�"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "���ͼ�����"
         Object.Width           =   4764
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "ownership_id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "LandUse_Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "LAND_FLAG_APPLY"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "��鹷�����͡"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView LstV_OwnerShip 
      Height          =   1365
      Left            =   120
      TabIndex        =   104
      Top             =   7680
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   2408
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      MousePointer    =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "��������Ңͧ"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "������Ңͧ�����Է���"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "�������"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Email"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "��˹��Է��"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "�Ѻ������"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LstV_Img_Name 
      Height          =   2115
      Left            =   8550
      TabIndex        =   105
      Top             =   1290
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   3731
      View            =   3
      Arrange         =   2
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�ٻ�͡����Է���"
         Object.Width           =   2752
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.ComboBox CMB_SOI_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_LandUse.frx":30DB0
      Left            =   2520
      List            =   "Frm_LandUse.frx":30DB2
      TabIndex        =   102
      Top             =   30
      Visible         =   0   'False
      Width           =   480
   End
   Begin MSComCtl2.DTPicker DTP_START_OWNER 
      Height          =   330
      Left            =   4590
      TabIndex        =   120
      Top             =   5550
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_LandUse.frx":30DB4
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   76349441
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin VB.Label Lb_Land_SumTax 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   10860
      TabIndex        =   124
      Top             =   4650
      Width           =   1455
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ����͡����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   57
      Left            =   4650
      TabIndex        =   121
      Top             =   5250
      Width           =   1380
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���͡��鹷��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   56
      Left            =   9060
      TabIndex        =   118
      Top             =   5520
      Width           =   765
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   36
      Left            =   10830
      Top             =   6570
      Width           =   1515
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   21
      Left            =   5580
      Top             =   2610
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   22
      Left            =   6420
      Top             =   2610
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   23
      Left            =   7380
      Top             =   2610
      Width           =   525
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   5
      Left            =   6420
      Top             =   1770
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   6
      Left            =   5580
      Top             =   1770
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   19
      Left            =   7380
      Top             =   1770
      Width           =   525
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ͷ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   2
      Left            =   6810
      TabIndex        =   117
      Top             =   1380
      Width           =   420
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ŵ���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   6660
      TabIndex        =   116
      Top             =   2220
      Width           =   690
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   55
      Left            =   10770
      TabIndex        =   114
      Top             =   4200
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   54
      Left            =   9390
      TabIndex        =   112
      Top             =   4200
      Width           =   960
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ê�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   53
      Left            =   7230
      TabIndex        =   110
      Top             =   4200
      Width           =   1050
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*���˵ػ�Ѻ����¹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   28
      Left            =   2520
      TabIndex        =   108
      Top             =   960
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Lb_Ownership_Real_ID 
      Alignment       =   2  'Center
      BackColor       =   &H0099AA88&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   98
      Top             =   5550
      Width           =   1725
   End
   Begin VB.Label Lb_Email 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   97
      Top             =   7230
      Width           =   4305
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   35
      Left            =   1770
      Top             =   6030
      Width           =   4365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������Ңͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   52
      Left            =   330
      TabIndex        =   93
      Top             =   6030
      Width           =   1230
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   4005
      Index           =   33
      Left            =   6570
      Top             =   5100
      Width           =   6285
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   34
      Left            =   7590
      Top             =   6930
      Width           =   4755
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   51
      Left            =   6720
      TabIndex        =   91
      Top             =   6990
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����ش"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   50
      Left            =   10230
      TabIndex        =   90
      Top             =   7380
      Width           =   495
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ���  �������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   49
      Left            =   7140
      TabIndex        =   89
      Top             =   7380
      Width           =   1005
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������ª����Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   35
      Left            =   6930
      TabIndex        =   59
      Top             =   5220
      Width           =   1650
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   6
      Left            =   6630
      Top             =   5160
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   3915
      Index           =   32
      Left            =   6600
      Top             =   5160
      Width           =   6225
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   48
      Left            =   12390
      TabIndex        =   88
      Top             =   6240
      Width           =   345
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   31
      Left            =   10830
      Top             =   6210
      Width           =   1515
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������»�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   47
      Left            =   9795
      TabIndex        =   87
      Top             =   6270
      Width           =   915
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   46
      Left            =   2850
      TabIndex        =   86
      Top             =   4680
      Width           =   345
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   30
      Left            =   1470
      Top             =   4620
      Width           =   1215
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥһҹ��ҧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   45
      Left            =   210
      TabIndex        =   85
      Top             =   4680
      Width           =   1140
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   44
      Left            =   12390
      TabIndex        =   82
      Top             =   6570
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   43
      Left            =   10350
      TabIndex        =   81
      Top             =   6630
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   42
      Left            =   8940
      TabIndex        =   80
      Top             =   6270
      Width           =   495
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   41
      Left            =   7920
      TabIndex        =   79
      Top             =   6270
      Width           =   480
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѡɳ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   40
      Left            =   6900
      TabIndex        =   76
      Top             =   6270
      Width           =   585
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   39
      Left            =   7260
      TabIndex        =   75
      Top             =   6630
      Width           =   225
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   38
      Left            =   8190
      TabIndex        =   74
      Top             =   6630
      Width           =   345
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��.��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   37
      Left            =   9195
      TabIndex        =   73
      Top             =   6630
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   27
      Left            =   8610
      Top             =   6570
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   26
      Left            =   7590
      Top             =   6570
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   25
      Left            =   9720
      Top             =   6570
      Width           =   525
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   20
      Left            =   1770
      Top             =   7200
      Width           =   4365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   36
      Left            =   1020
      TabIndex        =   72
      Top             =   7290
      Width           =   600
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   24
      Left            =   1260
      TabIndex        =   71
      Top             =   6900
      Width           =   345
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   17
      Left            =   1770
      Top             =   6420
      Width           =   4365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ңͧ�����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   23
      Left            =   210
      TabIndex        =   70
      Top             =   6480
      Width           =   1380
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   16
      Left            =   1770
      Top             =   6810
      Width           =   4365
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00756E60&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   315
      Index           =   15
      Left            =   1800
      Top             =   5610
      Width           =   1725
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������Ңͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   22
      Left            =   615
      TabIndex        =   69
      Top             =   5610
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ңͧ�����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   20
      Left            =   510
      TabIndex        =   68
      Top             =   5220
      Width           =   1380
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   14
      Left            =   10830
      Top             =   4620
      Width           =   1515
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   645
      Index           =   13
      Left            =   60
      Top             =   4470
      Width           =   12795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   585
      Index           =   12
      Left            =   90
      Top             =   4500
      Width           =   12735
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   615
      Index           =   11
      Left            =   90
      Top             =   3480
      Width           =   12735
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   675
      Index           =   9
      Left            =   60
      Top             =   3450
      Width           =   12795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1755
      Index           =   8
      Left            =   60
      Top             =   1290
      Width           =   5415
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ�¡�������ª��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   19
      Left            =   3900
      TabIndex        =   65
      Top             =   4680
      Width           =   2220
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   18
      Left            =   6840
      TabIndex        =   64
      Top             =   5910
      Width           =   645
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   17
      Left            =   12420
      TabIndex        =   63
      Top             =   4680
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���շ������Թ������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   9000
      TabIndex        =   62
      Top             =   4680
      Width           =   1710
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ����Ҿ�ѡ�ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   6720
      TabIndex        =   61
      Top             =   4680
      Width           =   1875
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��û����Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   14
      Left            =   510
      TabIndex        =   60
      Top             =   4200
      Width           =   1260
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1815
      Index           =   24
      Left            =   30
      Top             =   1260
      Width           =   5475
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Դ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   34
      Left            =   11910
      TabIndex        =   58
      Top             =   3750
      Width           =   795
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   33
      Left            =   7950
      TabIndex        =   57
      Top             =   3750
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   32
      Left            =   5580
      TabIndex        =   56
      Top             =   3750
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� (�����)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   31
      Left            =   2760
      TabIndex        =   55
      Top             =   3750
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   30
      Left            =   210
      TabIndex        =   54
      Top             =   3750
      Width           =   435
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Դ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   21
      Left            =   10770
      TabIndex        =   53
      Top             =   3750
      Width           =   585
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������´����駷��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   29
      Left            =   360
      TabIndex        =   52
      Top             =   3180
      Width           =   1635
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   27
      Left            =   6150
      TabIndex        =   49
      Top             =   2670
      Width           =   225
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   26
      Left            =   6990
      TabIndex        =   48
      Top             =   2670
      Width           =   345
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��.��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   25
      Left            =   7950
      TabIndex        =   47
      Top             =   2670
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   18
      Left            =   3810
      Top             =   2610
      Width           =   1545
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������´�͡����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   240
      TabIndex        =   38
      Top             =   990
      Width           =   1845
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�͡����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   150
      TabIndex        =   46
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��.��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   7950
      TabIndex        =   45
      Top             =   1830
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   12
      Left            =   6990
      TabIndex        =   44
      Top             =   1830
      Width           =   345
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   6150
      TabIndex        =   43
      Top             =   1830
      Width           =   225
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   2
      Left            =   1170
      Top             =   2610
      Width           =   1515
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   1
      Left            =   3810
      Top             =   2220
      Width           =   1545
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   1170
      Top             =   2220
      Width           =   1515
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ŷ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   11
      Left            =   2910
      TabIndex        =   42
      Top             =   1890
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "˹�����Ǩ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   10
      Left            =   150
      TabIndex        =   41
      Top             =   2670
      Width           =   885
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ���ҧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   2880
      TabIndex        =   40
      Top             =   2670
      Width           =   825
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   150
      TabIndex        =   39
      Top             =   1890
      Width           =   675
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ���Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   7
      Left            =   2910
      TabIndex        =   37
      Top             =   2280
      Width           =   675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   2760
      TabIndex        =   36
      Top             =   1410
      Width           =   120
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   7
      Left            =   2910
      Top             =   1350
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   10
      Left            =   2190
      Top             =   1350
      Width           =   495
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   150
      TabIndex        =   35
      Top             =   1500
      Width           =   735
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   4
      Left            =   30
      Top             =   930
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   2
      Left            =   5550
      Top             =   2190
      Width           =   2925
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   5
      Left            =   60
      Top             =   3120
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   0
      Left            =   60
      Top             =   4140
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   7
      Left            =   90
      Top             =   5160
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   3915
      Index           =   29
      Left            =   90
      Top             =   5160
      Width           =   6465
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   4005
      Index           =   28
      Left            =   60
      Top             =   5100
      Width           =   6525
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   1
      Left            =   5550
      Top             =   1320
      Width           =   2925
   End
   Begin VB.Label Lb_Owner_Name 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   95
      Top             =   6450
      Width           =   4305
   End
   Begin VB.Label Lb_Owner_Type 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   94
      Top             =   6060
      Width           =   4305
   End
   Begin VB.Label Lb_Owner_Address 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   96
      Top             =   6840
      Width           =   4305
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   3
      Left            =   6630
      Top             =   4140
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1755
      Index           =   3
      Left            =   5520
      Top             =   1290
      Width           =   2985
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1815
      Index           =   4
      Left            =   5490
      Top             =   1260
      Width           =   3045
   End
End
Attribute VB_Name = "Frm_LandUse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim itmX As ListItem
Dim Lst_Tamlay_Edit As Byte
Dim Status As String
Dim QueryLandUseRate As Recordset

Private Sub CalculateTax()
If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
If CSng(Txt_Apply_Rai.Text) + CSng(Txt_Apply_Pot.Text) + CSng(Txt_Apply_Va.Text) = 0 Then
    Txt_SumTax.Text = "0.00"
    Exit Sub
End If

If Cmb_Tamlay.Text = Empty Then Exit Sub
If Cmb_LandUse.Text = Empty Then Exit Sub

Dim DimentionC As Single
                   DimentionC = (CSng(Txt_Apply_Rai.Text) * 1600) + (CSng(Txt_Apply_Pot.Text) * 400) + (CSng(Val(Txt_Apply_Va.Text)) * 4)  '�ŧ�� ��.�
          If Cmb_LandScript.ListIndex = 0 Then ' Or Trim$(Cmb_LM_ID.Text) = "LM-04"   Ŵ���͹���Դ�Թ�����ҡó�� �
                    With GBQueryLandUse
                                If Chk_Flag_Apply0.Value Then  '  �����ͧ
                                    If .Fields("TYPE_USE_PRICE").Value = 1 Then
                                        Txt_SumTax.Text = FormatNumber((.Fields("LANDUSE_PRICE").Value / 1600) * DimentionC, 2)
                                    Else
                                       Txt_SumTax.Text = FormatNumber((((GBQueryZoneTax.Fields("TAX_RATE").Value * .Fields("LANDUSE_PRICE").Value) / 100) / 1600) * DimentionC, 2)
                                    End If
                                Else '������
                                    If .Fields("TYPE_USE_PRICE").Value = 1 Then '�ѵ�Һҷ
                                        Txt_SumTax.Text = FormatNumber((.Fields("LANDUSE_RATE").Value / 1600) * DimentionC, 2)  '�����շ�����������ͷ���������ª��
                                    Else '�ѵ��%
                                        Txt_SumTax.Text = FormatNumber((((GBQueryZoneTax.Fields("TAX_RATE").Value * .Fields("LANDUSE_RATE").Value) / 100) / 1600) * DimentionC, 2)
                                    End If
                                End If
                    End With
            Else
                        Txt_SumTax.Text = "0.00"
           End If
      
        If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) And Trim$(Cmb_LM_ID.Text) <> "LM-04" Then
              Txt_SumTax.Text = "0.00"
        End If
End Sub

Public Sub CalSumTax()
If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub
Dim DimentionC As Currency, i As Byte

'If Chk_Flag_Assess0.Value = Checked Then
        For i = 1 To LstV_OwnerUse.ListItems.Count
                DimentionC = DimentionC + CCur(LstV_OwnerUse.ListItems.Item(i).SubItems(5))
        Next i
        Lb_Land_SumTax.Caption = FormatNumber(DimentionC, 2, vbTrue)
'End If
End Sub

Private Sub ShowPicture(Path_Picture As String)
On Error GoTo HldNoPic
Dim ShrinkScale As Single
                
                ImgSize.Stretch = False
                ImgSize.Picture = LoadPicture(Path_Picture)
                originalWidth = ImgSize.Width
                originalHeight = ImgSize.Height
                sWidth = ImgSize.Width
                sHeight = ImgSize.Height
                picPicture.AutoSize = True
                picPicture.Picture = LoadPicture(Path_Picture)
                
                picPicture.Left = 0
                picPicture.Top = 0
                        
                ImgSize.Stretch = True
                ImgSize.Width = sWidth
                ImgSize.Height = sHeight
        
                sWidth = picParent.Width
                ShrinkScale = picParent.Width / ImgSize.Width
                sHeight = CSng(sHeight * ShrinkScale)
        
        If sHeight > picParent.Height Then
                ShrinkScale = picParent.Height / sHeight
                sWidth = CSng(sWidth * ShrinkScale)
                sHeight = CSng(sHeight * ShrinkScale) - 1
        End If
                ImgSize.Stretch = False
                  Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
                  Exit Sub
HldNoPic:
            picPicture.Picture = LoadPicture("")
            ImgSize.Picture = LoadPicture("")
End Sub

Private Sub SizeImage(picBox As PictureBox, sizePic As Picture, sizeWidth As Single, sizeHeight As Single)
On Error Resume Next
        picBox.Picture = LoadPicture("")
        picBox.Width = sizeWidth
        picBox.Height = sizeHeight
        picBox.AutoRedraw = True
        picBox.PaintPicture sizePic, 0, 0, sizeWidth, sizeHeight
        picBox.Picture = picBox.Image
        picBox.AutoRedraw = False
        picBox.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

Private Sub SET_REFRESH()
   GBQueryLandData.Requery
   GBQueryZoneTax.Requery
   GBQueryVillage.Requery
   GBQueryLandUse.Requery
   GBQueryStreet_Name.Requery
   GBQuerySoi.Requery
   GBQueryDuePayTax.Requery
   
          Cmb_LandScript.ListIndex = -1
          Cmb_Zoneblock.Clear
          GBQueryZone.Filter = "ZONE_ID <> '' "
If GBQueryZone.RecordCount > 0 Then
            GBQueryZone.MoveFirst
    Do While Not GBQueryZone.EOF
                  If GBQueryZone.Fields("ZONE_BLOCK").Value <> Empty Then
                    Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
    Loop
End If
If sFlag = False Then
       Cmb_Tambon_ID.Clear
       Cmb_Tambon.Clear
       GBQueryTambon.Filter = " AMPHOE_ID = '" & iniAmphoe_Id & "'"
            If GBQueryTambon.RecordCount > 0 Then
                      GBQueryTambon.MoveFirst
                        Do While Not GBQueryTambon.EOF
                                    Cmb_Tambon.AddItem GBQueryTambon.Fields("Tambon_Name").Value
                                    Cmb_Tambon_ID.AddItem GBQueryTambon.Fields("Tambon_ID").Value
                                    GBQueryTambon.MoveNext
                        Loop
            End If
 End If
        Cmb_Village.Clear
        Cmb_Village_ID.Clear
        GBQueryVillage.Filter = " Tambon_Id = '" & Cmb_Tambon_ID.Text & "'"
        If GBQueryVillage.RecordCount > 0 Then
                   GBQueryVillage.MoveFirst
           Do While Not GBQueryVillage.EOF
                         If GBQueryVillage.Fields("Village_ID").Value <> Empty Then
                             Cmb_Village.AddItem GBQueryVillage.Fields("Village_Name").Value
                             Cmb_Village_ID.AddItem GBQueryVillage.Fields("Village_ID").Value
                         End If
                           GBQueryVillage.MoveNext
           Loop
                          Cmb_Village.ListIndex = 0
        Else
             GBQueryVillage.Filter = " AMPHOE_ID = '" & iniAmphoe & "'"
        End If
   
         Cmb_Street_ID.Clear
         Cmb_Street.Clear
         GBQueryStreet_Name.Filter = "STREET_ID <> '' "
If GBQueryStreet_Name.RecordCount > 0 Then
            GBQueryStreet_Name.MoveFirst
    Do While Not GBQueryStreet_Name.EOF
                  If GBQueryStreet_Name.Fields("STREET_ID").Value <> Empty Then
                    Cmb_Street.AddItem GBQueryStreet_Name.Fields("STREET_NAME").Value
                    Cmb_Street_ID.AddItem GBQueryStreet_Name.Fields("STREET_ID").Value
                  End If
                    GBQueryStreet_Name.MoveNext
    Loop
End If
         Cmb_Tamlay.Clear
         'GBQueryZoneTax.Filter = "STREET_ID <> '' "
If GBQueryZoneTax.RecordCount > 0 Then
            GBQueryZoneTax.MoveFirst
    Do While Not GBQueryZoneTax.EOF
                  If GBQueryZoneTax.Fields("ZONETAX_ID").Value <> Empty Then
                       Cmb_Tamlay.AddItem GBQueryZoneTax.Fields("ZONETAX_ID").Value
                  End If
                    GBQueryZoneTax.MoveNext
    Loop
End If

             GBQueryDuePayTax.MoveFirst
             GBQueryDuePayTax.Find " Type_Rate = 1 ", , adSearchForward
             Txt_B_Rai.Text = GBQueryDuePayTax.Fields("Subside_Rai").Value
             Txt_B_Pot.Text = GBQueryDuePayTax.Fields("Subside_Ngan").Value
             Txt_B_Va.Text = GBQueryDuePayTax.Fields("Subside_Va").Value
End Sub

Private Sub WriteHistoryFile(LAND_ID As String)
Dim sql_txt As String, i As Byte, ROUND_COUNT As String, Temp_Change As String
Dim Query As ADODB.Recordset
Set Query = New ADODB.Recordset
i = 1
'sql_txt = " Select A.* , B.OWNERSHIP_ID  From LANDDATA A , LANDDATA_NOTIC B WHERE  B.LAND_ID = A.LAND_ID AND A.LAND_ID = '" & LAND_ID & "'"
Set Query = Globle_Connective.Execute("exec sp_writehistory_land_query '" & LAND_ID & "','0'", , adCmdUnknown)
'Call SET_QUERY(sql_txt, Query)
With Query
                .MoveFirst
               Do While Not .EOF
'                        sql_txt = Empty
                     If i = 1 Then
                                    If Status = "EDIT" Then
                                            Temp_Change = .Fields("LAND_CHANGE_ID").Value
                                    ElseIf Status = "DEL" Then
                                            Temp_Change = "000"
                                    End If
                                    ROUND_COUNT = RunAutoNumber("LANDDATASTORY", "ROUND_COUNT", LAND_ID & "-0001", False, " AND LAND_ID = '" & LAND_ID & "'")
'                                    Debug.Print "exec sp_insert_landdatastory '" & ROUND_COUNT & "','" & .Fields("LAND_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & .Fields("ZONETAX_ID").Value & "','" & _
'                                                .Fields("LAND_NUMBER").Value & "','" & .Fields("LAND_RAWANG").Value & "','" & .Fields("LAND_SURVEY").Value & "','" & .Fields("LAND_NOTIC").Value & "','" & .Fields("TAMBON_ID").Value & "','" & .Fields("VILLAGE_ID").Value & "','" & _
'                                                .Fields("STREET_ID").Value & "','" & .Fields("SOI_ID").Value & "'," & .Fields("LAND_TYPE").Value & "," & .Fields("LAND_A_RAI").Value & "," & .Fields("LAND_A_POT").Value & "," & .Fields("LAND_A_VA").Value & "," & .Fields("LAND_B_RAI").Value & "," & _
'                                                .Fields("LAND_B_POT").Value & "," & .Fields("LAND_B_VA").Value & "," & .Fields("FLAG_STREET").Value & "," & .Fields("FLAG_ASSESS").Value & "," & .Fields("LAND_PRICE").Value & "," & .Fields("LAND_SUMTAX").Value & "," & .Fields("FLAG_PAYTAX").Value & ",'" & _
'                                                IIf(IsNull(.Fields("LAND_DATE").Value) = False, CvDate2(.Fields("LAND_DATE").Value), "") & "','" & IIf(IsNull(.Fields("USER_LOGIN").Value) = True, "", .Fields("USER_LOGIN").Value) & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), "") & " " & Time & "','" & Temp_Change & "'"
'                                    Globle_Connective.Execute "exec sp_insert_landdatastory '" & ROUND_COUNT & "','" & .Fields("LAND_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & .Fields("ZONETAX_ID").Value & "','" & _
'                                                .Fields("LAND_NUMBER").Value & "','" & .Fields("LAND_RAWANG").Value & "','" & .Fields("LAND_SURVEY").Value & "','" & .Fields("LAND_NOTIC").Value & "','" & .Fields("TAMBON_ID").Value & "','" & .Fields("VILLAGE_ID").Value & "','" & _
'                                                .Fields("STREET_ID").Value & "','" & .Fields("SOI_ID").Value & "'," & .Fields("LAND_TYPE").Value & "," & .Fields("LAND_A_RAI").Value & "," & .Fields("LAND_A_POT").Value & "," & .Fields("LAND_A_VA").Value & "," & .Fields("LAND_B_RAI").Value & "," & _
'                                                .Fields("LAND_B_POT").Value & "," & .Fields("LAND_B_VA").Value & "," & .Fields("FLAG_STREET").Value & "," & .Fields("FLAG_ASSESS").Value & "," & .Fields("LAND_PRICE").Value & "," & .Fields("LAND_SUMTAX").Value & "," & .Fields("FLAG_PAYTAX").Value & ",'" & _
'                                                IIf(IsNull(.Fields("LAND_DATE").Value) = False, CvDate2(.Fields("LAND_DATE").Value), "") & "','" & .Fields("USER_LOGIN").Value & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), CvDate2(Date)) & " " & Time & "','" & Temp_Change & "'", , adCmdUnknown
                                    Call SET_Execute2("exec sp_insert_landdatastory '" & ROUND_COUNT & "','" & .Fields("LAND_ID").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & .Fields("ZONETAX_ID").Value & "','" & _
                                                .Fields("LAND_NUMBER").Value & "','" & .Fields("LAND_RAWANG").Value & "','" & .Fields("LAND_SURVEY").Value & "','" & .Fields("LAND_NOTIC").Value & "','" & .Fields("TAMBON_ID").Value & "','" & .Fields("VILLAGE_ID").Value & "','" & _
                                                .Fields("STREET_ID").Value & "','" & .Fields("SOI_ID").Value & "'," & .Fields("LAND_TYPE").Value & "," & .Fields("LAND_A_RAI").Value & "," & .Fields("LAND_A_POT").Value & "," & .Fields("LAND_A_VA").Value & "," & .Fields("LAND_B_RAI").Value & "," & _
                                                .Fields("LAND_B_POT").Value & "," & .Fields("LAND_B_VA").Value & "," & .Fields("FLAG_STREET").Value & "," & .Fields("FLAG_ASSESS").Value & "," & .Fields("LAND_PRICE").Value & "," & .Fields("LAND_SUMTAX").Value & "," & .Fields("FLAG_PAYTAX").Value & ",'" & _
                                                IIf(IsNull(.Fields("LAND_DATE").Value) = False, CvDate2(.Fields("LAND_DATE").Value), "") & "','" & .Fields("USER_LOGIN").Value & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), CvDate2(Date)) & " " & Time & "','" & Temp_Change & "'", 8, False)
'                                    sql_txt = "INSERT INTO LANDDATASTORY ( ROUND_COUNT,LAND_ID,ZONE_BLOCK , ZONETAX_ID ,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC, TAMBON_ID,VILLAGE_ID,STREET_ID,SOI_ID,LAND_TYPE ,LAND_A_RAI, " & _
'                                                    " LAND_A_POT , LAND_A_VA ,LAND_B_RAI ,LAND_B_POT , LAND_B_VA , FLAG_STREET , FLAG_ASSESS , LAND_PRICE , LAND_SUMTAX ,LAND_REMARK,LAND_UPDATE ,FLAG_PAYTAX )"
'                                    sql_txt = sql_txt & " VALUES ('" & ROUND_COUNT & "','" & .Fields("LAND_ID").Value & "','"
'                                    sql_txt = sql_txt & .Fields("ZONE_BLOCK").Value & "','" & .Fields("ZONETAX_ID").Value & "','" & .Fields("LAND_NUMBER").Value & "','" & .Fields("LAND_RAWANG").Value & "','"
'                                    sql_txt = sql_txt & .Fields("LAND_SURVEY").Value & "','" & .Fields("LAND_NOTIC").Value & "','" & .Fields("TAMBON_ID").Value & "','" & .Fields("VILLAGE_ID").Value & "','" & .Fields("STREET_ID").Value & "','" & .Fields("SOI_ID").Value & "',"
'                                    sql_txt = sql_txt & .Fields("LAND_TYPE").Value & "," & .Fields("LAND_A_RAI").Value & "," & .Fields("LAND_A_POT").Value & "," & .Fields("LAND_A_VA").Value & ","
'                                    sql_txt = sql_txt & .Fields("LAND_B_RAI").Value & "," & .Fields("LAND_B_POT").Value & "," & .Fields("LAND_B_VA").Value & ","
'                                    sql_txt = sql_txt & .Fields("FLAG_STREET").Value & "," & .Fields("FLAG_ASSESS").Value & "," & .Fields("LAND_PRICE").Value & "," & .Fields("LAND_SUMTAX").Value & ",'" & Trim$(Txt_StoryRemark.Text) & "','" & Date & "'," & .Fields("FLAG_PAYTAX").Value & ")"
'                                    Call SET_Execute(sql_txt)
                        i = 0
                     End If
'                           sql_txt = Empty
'                                    Globle_Connective.Execute "exec sp_insert_landdata_notic_story '" & RunAutoNumber("LANDDATA_NOTIC_STORY", "LAND_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                                ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", , adCmdUnknown
                                    Call SET_Execute2("exec sp_insert_landdata_notic_story '" & RunAutoNumber("LANDDATA_NOTIC_STORY", "LAND_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                                ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", 8, False)
'                                    sql_txt = " INSERT INTO LANDDATA_NOTIC_STORY ( LAND_NOTIC_ID, ROUND_COUNT,OWNERSHIP_ID ) "
'                                    sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("LANDDATA_NOTIC_STORY", "LAND_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                                    sql_txt = sql_txt & ROUND_COUNT & "','"
'                                    sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "')"
'                                    Call SET_Execute(sql_txt)
                        Query.MoveNext
'                        i = i + 1
                Loop
                
'                sql_txt = Empty
'                sql_txt = " Select *  From LANDDATA_APPLY WHERE LAND_ID = '" & LAND_ID & "'"
                Set Query = Globle_Connective.Execute("exec sp_writehistory_land_query '" & LAND_ID & "','1'", , adCmdUnknown)
'                Call SET_QUERY(sql_txt, Query)
                Query.MoveFirst
               Do While Not Query.EOF
'                        Globle_Connective.Execute "exec sp_insert_landdata_apply_story '" & RunAutoNumber("LANDDATA_APPLY_STORY", "LAND_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                    ROUND_COUNT & "','" & Query.Fields("OWNERSHIP_ID").Value & "','" & Query.Fields("REMARK").Value & "','" & CvDate2(Query.Fields("START_DATE").Value) & "','" & CvDate2(Query.Fields("END_DATE").Value) & "'," & _
'                                    Query.Fields("APPLY_YEAR").Value & "," & Query.Fields("Apply_Rai").Value & "," & Query.Fields("APPLY_POT").Value & "," & Query.Fields("Apply_Va").Value & "," & Query.Fields("APPLY_SUMRATE").Value & ",'" & _
'                                    Query.Fields("LM_ID").Value & "'," & IIf(IsNull(Query.Fields("LAND_FLAG_APPLY").Value), 0, Query.Fields("LAND_FLAG_APPLY").Value) & "," & Query.Fields("LAND_OPTION").Value & ",'" & Query.Fields("ZONETAX_ID").Value & "'", , adCmdUnknown
                        Call SET_Execute2("exec sp_insert_landdata_apply_story '" & RunAutoNumber("LANDDATA_APPLY_STORY", "LAND_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                    ROUND_COUNT & "','" & Query.Fields("OWNERSHIP_ID").Value & "','" & Query.Fields("REMARK").Value & "','" & CvDate2(Query.Fields("START_DATE").Value) & "','" & CvDate2(Query.Fields("END_DATE").Value) & "'," & _
                                    Query.Fields("APPLY_YEAR").Value & "," & Query.Fields("Apply_Rai").Value & "," & Query.Fields("APPLY_POT").Value & "," & Query.Fields("Apply_Va").Value & "," & Query.Fields("APPLY_SUMRATE").Value & ",'" & _
                                    Query.Fields("LM_ID").Value & "'," & IIf(IsNull(Query.Fields("LAND_FLAG_APPLY").Value), 0, Query.Fields("LAND_FLAG_APPLY").Value) & "," & Query.Fields("LAND_OPTION").Value & ",'" & Query.Fields("ZONETAX_ID").Value & "'", 8, False)
'                        sql_txt = Empty
'                        sql_txt = "INSERT INTO LANDDATA_APPLY_STORY ( LAND_APPLY_ID,ROUND_COUNT,OWNERSHIP_ID,REMARK,START_DATE,END_DATE,APPLY_YEAR ,APPLY_RAI ,APPLY_POT,APPLY_VA , APPLY_SUMRATE,LM_ID,LAND_FLAG_APPLY,LAND_OPTION) "
'                        sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("LANDDATA_APPLY_STORY", "LAND_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                        sql_txt = sql_txt & ROUND_COUNT & "','"
'                        sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "','" & .Fields("REMARK").Value & "','" & .Fields("START_DATE").Value & "','" & .Fields("END_DATE").Value & "',"
'                        sql_txt = sql_txt & .Fields("APPLY_YEAR").Value & "," & .Fields("Apply_Rai").Value & "," & .Fields("APPLY_POT").Value & "," & .Fields("Apply_Va").Value & "," & .Fields("APPLY_SUMRATE").Value & ",'"
'                        sql_txt = sql_txt & .Fields("LM_ID").Value & "'," & IIf(IsNull(.Fields("LAND_FLAG_APPLY").Value), 0, .Fields("LAND_FLAG_APPLY").Value) & "," & .Fields("LAND_OPTION").Value & ")"
'                        Call SET_Execute(sql_txt)
                        Query.MoveNext
                Loop
                 
'                Dim Logic As Boolean   'MAKE FOR insert ����������㹵��ҧ PBT5 ��èѴ���������������¹����͡����Է��� =================================
'                Logic = False
'                sql_txt = Empty
'                sql_txt = " Select  * From PBT5 WHERE LAND_ID = '" & LAND_ID & "' ORDER BY PBT5_PAY DESC"
'                Call SET_QUERY(sql_txt, Query)
'                If .RecordCount > 0 Then
'                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                           .MoveFirst
'                            Do While Not .EOF
'                                                              If Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) = Trim$(.Fields("OWNERSHIP_ID").Value) Then
'                                                                  Logic = True
'                                                                  .MoveLast
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                    .MoveNext
'                           Loop
'                                                             If Logic = False Then
'                                                                 .MovePrevious
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "INSERT INTO PBT5 ( LAND_ID, OWNERSHIP_ID ,PBT5_YEAR,PBT5_NO,PBT5_DATE , PBT5_NO_STATUS,PBT5_AMOUNT,PBT5_STATUS,PBT5_PAY" & _
'                                                                                   ",PBT5_REMARK,FLAG_YEAR,PBT5_NO_ACCEPT,PBT5_DATE_ACCEPT,PBT5_BOOK_NUMBER,PBT5_BOOK_NO,PBT5_PAY_DATE,PBT5_TAXINCLUDE)"
'                                                                  sql_txt = sql_txt & "VALUES ('" & .Fields("LAND_ID").Value & "','" & Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) & "'," & .Fields("PBT5_YEAR").Value & ",'" & _
'                                                                                   .Fields("PBT5_NO").Value & "'," & IIf(IsNull(.Fields("PBT5_DATE").Value), "Null", "'" & .Fields("PBT5_DATE").Value & "'") & "," & .Fields("PBT5_NO_STATUS").Value & "," & _
'                                                                                   .Fields("PBT5_AMOUNT").Value & "," & .Fields("PBT5_STATUS").Value & "," & .Fields("PBT5_PAY").Value & ",'" & .Fields("PBT5_REMARK").Value & "'," & .Fields("FLAG_YEAR").Value & ",'" & _
'                                                                                   .Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(.Fields("PBT5_DATE_ACCEPT").Value), "Null", "'" & .Fields("PBT5_DATE_ACCEPT").Value & "'") & ",'" & .Fields("PBT5_BOOK_NUMBER").Value & "','" & _
'                                                                                   .Fields("PBT5_BOOK_NO").Value & "'," & IIf(IsNull(.Fields("PBT5_PAY_DATE").Value), "Null", "'" & .Fields("PBT5_PAY_DATE").Value & "'") & "," & .Fields("PBT5_TAXINCLUDE").Value & ")"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                    Next i
'                                    sql_txt = "UPDATE PBT5 SET PBT5_AMOUNT = " & CDbl(FormatNumber(Txt_Land_SumTax.Text, 2)) & _
'                                                    " WHERE LAND_ID = '" & LAND_ID & "' AND PBT5_YEAR = " & IIf(Year(Date) < 2500, Year(Date) + 543, Year(Date) - 543) & " AND PBT5_STATUS = 0 AND PBT5_NO_STATUS = 0 "
'                                    Call SET_Execute(sql_txt)
'                           '========================================================
'                             Logic = False
'                                               .MoveFirst
'                                        Do While Not .EOF
'                                                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                                                              If Trim$(.Fields("OWNERSHIP_ID").Value) = Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) Then
'                                                                  Logic = True
'                                                                   i = LstV_OwnerShip.ListItems.Count + 7
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                                    Next i
'                                                             If Logic = False Then
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "DELETE FROM PBT5 WHERE LAND_ID = '" & LAND_ID & "' AND OWNERSHIP_ID = '" & Trim$(.Fields("OWNERSHIP_ID").Value) & "'"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                                                    .MoveNext
'                                        Loop
'                End If
End With
Set Query = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
Dim sql_txt As String, Temp_LAND_ID As String
Dim i  As Byte
Dim Rs As ADODB.Recordset

Set Rs = New ADODB.Recordset
If Txt_Land_ID2.Text = Empty Then
        Temp_LAND_ID = Cmb_Zoneblock.Text & Txt_Land_ID1.Text
Else
        Temp_LAND_ID = Cmb_Zoneblock.Text & Txt_Land_ID1.Text & "/" & Txt_Land_ID2.Text
End If
If Lb_Land_SumTax.Caption = "0.00" Or Lb_Land_SumTax.Caption = ".00" Then
        Chk_Flag_PayTax1.Value = Unchecked
        Chk_Flag_PayTax0.Value = Checked
End If
Set Rs = Globle_Connective.Execute("exec sp_max_year_pbt5 '" & Temp_LAND_ID & "','','0','',''", , adCmdUnknown)   '�һ�����ش�  GK1
If IsNull(Rs.Fields(0).Value) <> True Then
            Label2(4).Tag = Rs.Fields(0).Value
Else
            Label2(4).Tag = vbNullString
End If
If STATE = "DEL" Then
        Call WriteHistoryFile(Temp_LAND_ID)
End If
        sql_txt = "exec sp_manage_landdata '" & Temp_LAND_ID & "','" & Cmb_Zoneblock.Text & "','" & Cmb_Tamlay.Text & "','" & Trim$(Txt_Land_Number.Text) & "','" & Trim$(Txt_Land_Rawang.Text) & "','" & _
                        Trim$(Txt_Land_Survey.Text) & "','" & Trim$(Txt_Land_Notic.Text) & "','" & Trim$(Cmb_Tambon_ID.Text) & "','" & Trim$(Cmb_Village_ID.Text) & "','" & Trim$(Cmb_Street_ID.Text) & "','" & Trim$(CMB_SOI_ID.Text) & "','" & _
                        CByte(CMB_TYPE_LAND.ListIndex + 1) & "'," & CLng(Txt_A_Rai.Text) & "," & CByte(Txt_A_Pot.Text) & "," & CSng(Txt_A_Va.Text) & "," & _
                        CLng(Txt_B_Rai.Text) & "," & CByte(Txt_B_Pot.Text) & "," & CSng(Txt_B_Va.Text) & ",'" & _
                        CByte(Chk_Flag_StreetY.Value) & "','" & CByte(Chk_Flag_Assess1.Value) & "'," & CCur(Txt_Land_Price.Text) & _
                        "," & CCur(Lb_Land_SumTax.Caption) & ",'" & CByte(Chk_Flag_PayTax1.Value) & "','" & _
                        IIf(IsNull(DTP_START_OWNER) = False, CvDate1(DTP_START_OWNER), "") & "','" & strUser & _
                        "','" & CvDate2(Now, 1) & " " & Time & "','" & cb_Land_Change_ID.Text & "','" & STATE & "'"
'                              IIf(IsNull(DTPicker1.Value) = False, CvDate(DTPicker1), "")
'        Globle_Connective.Execute sql_txt, , adCmdUnknown
        Call SET_Execute2(sql_txt, 8, False)
                        If STATE = "EDIT" Then
'                                sql_txt = "INSERT INTO LANDDATA ( LAND_ID,ZONE_BLOCK , ZONETAX_ID ,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC, TAMBON_ID,VILLAGE_ID,STREET_ID,SOI_ID,LAND_TYPE ,LAND_A_RAI, " & _
'                                                  " LAND_A_POT , LAND_A_VA ,LAND_B_RAI ,LAND_B_POT , LAND_B_VA , FLAG_STREET , FLAG_ASSESS , LAND_PRICE , LAND_SUMTAX ,FLAG_PAYTAX,LAND_DATE)"
'                                 sql_txt = sql_txt & " VALUES ('" & Temp_LAND_ID & "','"
'                                 sql_txt = sql_txt & Cmb_Zoneblock.Text & "','" & Cmb_Tamlay.Text & "','" & Trim$(Txt_Land_Number.Text) & "','" & Trim$(Txt_Land_Rawang.Text) & "','"
'                                 sql_txt = sql_txt & Trim$(Txt_Land_Survey.Text) & "','" & Trim$(Txt_Land_Notic.Text) & "','" & Trim$(Cmb_Tambon_ID.Text) & "','" & Trim$(Cmb_Village_ID.Text) & "','" & Trim$(Cmb_Street_ID.Text) & "','" & Trim$(CMB_SOI_ID.Text) & "',"
'                                 sql_txt = sql_txt & CByte(CMB_TYPE_LAND.ListIndex + 1) & "," & CLng(Txt_A_Rai.Text) & "," & CByte(Txt_A_Pot.Text) & "," & CSng(Txt_A_Va.Text) & ","
'                                 sql_txt = sql_txt & CLng(Txt_B_Rai.Text) & "," & CByte(Txt_B_Pot.Text) & "," & CSng(Txt_B_Va.Text) & ","
'                                 sql_txt = sql_txt & CByte(Chk_Flag_StreetY.Value) & "," & CByte(Chk_Flag_Assess1.Value) & "," & CSng(Txt_Land_Price.Text) & "," & CSng(Txt_Land_SumTax.Text) & "," & CByte(Chk_Flag_PayTax1.Value) & ",#" & Format$(DTP_START_OWNER.Month, "00") & "/" & Format$(DTP_START_OWNER.Day, "00") & "/" & DTP_START_OWNER.Year & "#)"
'                                 Call SET_Execute(sql_txt)
'                        ElseIf STATE = "EDIT" Then
                                
                                Call WriteHistoryFile(Temp_LAND_ID)
                                             
'                                sql_txt = "UPDATE LANDDATA SET ZONETAX_ID = '" & Cmb_Tamlay.Text & "'," & _
'                                                       " LAND_NUMBER = '" & Trim$(Txt_Land_Number.Text) & "'," & _
'                                                       " LAND_RAWANG = '" & Trim$(Txt_Land_Rawang.Text) & "'," & _
'                                                       " LAND_SURVEY= '" & Trim$(Txt_Land_Survey.Text) & "'," & _
'                                                       " LAND_NOTIC = '" & Trim$(Txt_Land_Notic.Text) & "'," & _
'                                                       " TAMBON_ID = '" & Trim$(Cmb_Tambon_ID.Text) & "'," & _
'                                                       " VILLAGE_ID = '" & Trim$(Cmb_Village_ID.Text) & "'," & _
'                                                       " STREET_ID = '" & Trim$(Cmb_Street_ID.Text) & "'," & _
'                                                       " SOI_ID = '" & Trim$(CMB_SOI_ID.Text) & "'," & _
'                                                       " Land_Type = " & CByte(CMB_TYPE_LAND.ListIndex + 1) & "," & _
'                                                       " LAND_A_RAI  = " & CSng(Txt_A_Rai.Text) & "," & _
'                                                       " LAND_A_POT = " & CSng(Txt_A_Pot.Text) & "," & _
'                                                       " LAND_A_VA = " & CSng(Txt_A_Va.Text) & "," & _
'                                                       " LAND_B_RAI = " & CSng(Txt_B_Rai.Text) & "," & _
'                                                       " LAND_B_POT = " & CSng(Txt_B_Pot.Text) & "," & _
'                                                       " LAND_B_VA = " & CSng(Txt_B_Va.Text) & "," & _
'                                                       " FLAG_STREET  = " & CByte(Chk_Flag_StreetY.Value) & "," & _
'                                                       " FLAG_ASSESS = " & CByte(Chk_Flag_Assess1.Value) & "," & _
'                                                       " LAND_PRICE = " & CSng(Txt_Land_Price.Text) & "," & _
'                                                       " LAND_SUMTAX = " & CSng(Txt_Land_SumTax.Text) & "," & _
'                                                       " FLAG_PAYTAX  = " & CByte(Chk_Flag_PayTax1.Value) & "," & _
'                                                       " LAND_DATE = #" & Format$(DTP_START_OWNER.Month, "00") & "/" & Format$(DTP_START_OWNER.Day, "00") & "/" & DTP_START_OWNER.Year & "#" & _
'                                                       " WHERE LAND_ID = '" & Temp_LAND_ID & "'"
'                                                        Call SET_Execute(sql_txt)
'                        ElseIf STATE = "DEL" Then
'                                 sql_txt = "Delete FROM LANDDATA_IMAGE WHERE LAND_ID  = '" & Temp_LAND_ID & "'"
'                                                Call SET_Execute(sql_txt)
'                                sql_txt = "Delete FROM LANDDATA_NOTIC WHERE LAND_ID = '" & Temp_LAND_ID & "'"
'                                                Call SET_Execute(sql_txt)
'                                sql_txt = "Delete FROM LANDDATA_APPLY WHERE LAND_ID = '" & Temp_LAND_ID & "'"
'                                                Call SET_Execute(sql_txt)
'                                 sql_txt = "Delete FROM LANDDATASTORY  WHERE LAND_ID  = '" & Temp_LAND_ID & "'"
'                                                Call SET_Execute(sql_txt)
'                                 sql_txt = "Delete FROM LANDDATA_NOTIC_STORY  WHERE ROUND_COUNT LIKE '" & Temp_LAND_ID & "%'"
'                                                Call SET_Execute(sql_txt)
'                                 sql_txt = "Delete FROM LANDDATA_APPLY_STORY  WHERE ROUND_COUNT LIKE '" & Temp_LAND_ID & "%'"
'                                                Call SET_Execute(sql_txt)
'                                sql_txt = "Delete FROM LANDDATA  WHERE LAND_ID  = '" & Temp_LAND_ID & "'"
'                                                     Call SET_Execute(sql_txt)
                        End If
                                    sql_txt = Empty
                                      If STATE <> "DEL" Then
                                            If LstV_Img_Name.ListItems.Count > 0 Then
'                                                    Globle_Connective.Execute "exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','0'", , adCmdUnknown
                                                    Call SET_Execute2("exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','0'", 8, False)
'                                                sql_txt = "Delete FROM LANDDATA_IMAGE WHERE LAND_ID  = '" & Temp_LAND_ID & "'"
'                                                Call SET_Execute(sql_txt)
                                                
                                                    For i = 1 To LstV_Img_Name.ListItems.Count
'                                                            Globle_Connective.Execute "exec sp_insert_image '" & Temp_LAND_ID & "','" & Temp_LAND_ID & "-" & Format$(i, "###00#") & "','" & LstV_Img_Name.ListItems(i).Text & "','0'", , adCmdUnknown
                                                            Call SET_Execute2("exec sp_insert_image '" & Temp_LAND_ID & "','" & Temp_LAND_ID & "-" & Format$(i, "###00#") & "','" & LstV_Img_Name.ListItems(i).Text & "','0'", 8, False)
'                                                           sql_txt = " INSERT INTO LANDDATA_IMAGE ( LAND_IMG_ID, LAND_ID ,LAND_IMG_NAME ) "
'                                                           sql_txt = sql_txt & " VALUES ( '" & Temp_LAND_ID & "-" & Format$(i, "###00#") & "','"
'                                                           sql_txt = sql_txt & Temp_LAND_ID & "','"
'                                                           sql_txt = sql_txt & LstV_Img_Name.ListItems(i).Text & "')"
'                                                            Call SET_Execute(sql_txt)
                                                            Call CopyFile(LstV_Img_Name.ListItems(i).SubItems(1), iniPath_Picture & "\Zone" & Left$(Cmb_Zoneblock.Text, 2) & "\Block_" & Right$(Cmb_Zoneblock.Text, 1) & "\Land\" & LstV_Img_Name.ListItems(i).Text, 0)
                                                   Next i
                                            End If
                                            If LstV_OwnerShip.ListItems.Count > 0 Then
'                                                Globle_Connective.Execute "exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','1'", , adCmdUnknown
                                                Call SET_Execute2("exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','1'", 8, False)
'                                                     sql_txt = "Delete FROM LANDDATA_NOTIC WHERE LAND_ID = '" & Temp_LAND_ID & "'"
'                                                     Call SET_Execute(sql_txt)
                                                For i = 1 To LstV_OwnerShip.ListItems.Count
'                                                        Globle_Connective.Execute "exec sp_insert_landdata_notic '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','" & Temp_LAND_ID & "','" & _
'                                                                                                                LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "','" & _
'                                                                                                                IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & "'", , adCmdUnknown
                                                        Call SET_Execute2("exec sp_insert_landdata_notic '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','" & Temp_LAND_ID & "','" & _
                                                                                                                LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "','" & _
                                                                                                                IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & "'", 8, False)
'                                                        sql_txt = " INSERT INTO LANDDATA_NOTIC ( LAND_NOTIC_ID, LAND_ID,OWNERSHIP_ID,OWNERSHIP_NOTIC,OWNERSHIP_MAIL) "
'                                                        sql_txt = sql_txt & " VALUES ( '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','"
'                                                        sql_txt = sql_txt & Temp_LAND_ID & "','"
'                                                        sql_txt = sql_txt & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'," & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "," & _
'                                                                                         IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & ")"
'                                                        Call SET_Execute(sql_txt)
          ' �礡������¹�����Է���
                                                        If STATE = "EDIT" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" And LstV_OwnerShip.ListItems(i).ListSubItems(4).Text <> str_OwnershipID Then
        '                                                                Call SET_QUERY("SELECT MAX(PBT5_YEAR) FROM PBT5 WHERE LAND_ID='" & Temp_LAND_ID & "'", Rs)
                                                                        If Label2(4).Tag <> vbNullString Then
                                                                                Set Rs = Globle_Connective.Execute("exec sp_max_year_pbt5 '" & Temp_LAND_ID & "','','3',''," & Label2(4).Tag, , adCmdUnknown) '�� COUNT_CHANGE
                                                                                If IsNull(Rs.Fields(0).Value) = False Then
                                                                                        Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                Else
                                                                                        Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                End If
                                                                    
                                                                                Select Case Rs.Fields(0).Value '  PBT5_SET_GK
                                                                                        Case 0
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", 8, False)
                                                                                        Case 1
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", 8, False)
                                                                                        Case 2
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", 8, False)
                                                                                End Select
        '                                                                        sql_txt = "UPDATE PBT5 SET FLAG_CHANGE=1 WHERE LAND_ID= '" & Temp_LAND_ID & "' AND PBT5_YEAR=" & Rs.Fields(0).Value
        '                                                                        Call SET_Execute(sql_txt)
                                                                        End If
                                                                End If
                                                        ElseIf STATE = "ADD" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" Then
                                                                        If Chk_Flag_PayTax1.Value = Checked Then  '�ա�û����Թ����
                                                                                If Label2(4).Tag <> vbNullString Then
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_pbt5 '" & Temp_LAND_ID & "','','3',''," & Label2(4).Tag, , adCmdUnknown) '�� COUNT_CHANGE
                                                                                        If IsNull(Rs.Fields(0).Value) = False Then
                                                                                                Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                        Else
                                                                                                Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                        End If
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_pbt5 '" & Temp_LAND_ID & "','','5'," & Label2(6).Tag & "," & Label2(4).Tag, , adCmdUnknown) '�� COUNT_CHANGE
                                                                                        If Rs.Fields(1).Value = LstV_OwnerShip.ListItems(i).ListSubItems(4).Text Then
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        Else
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pbt5 '" & Temp_LAND_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        End If
                                                                                End If
                                                                        End If
                                                                End If
                                                        End If
                                                Next i
                                            End If
                                            If LstV_OwnerUse.ListItems.Count > 0 Then
'                                                     Globle_Connective.Execute "exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','2'", , adCmdUnknown
                                                     Call SET_Execute2("exec sp_del_notic_apply_img_landdata '" & Temp_LAND_ID & "','2'", 8, False)
'                                                     sql_txt = "Delete FROM LANDDATA_APPLY WHERE LAND_ID = '" & Temp_LAND_ID & "'"
'                                                     Call SET_Execute(sql_txt)
                                                     With LstV_OwnerUse
                                                        For i = 1 To LstV_OwnerUse.ListItems.Count
'                                                                Globle_Connective.Execute "exec sp_insert_landdata_apply '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','" & _
'                                                                                                                        Temp_LAND_ID & "','" & .ListItems(i).ListSubItems(11).Text & "','" & .ListItems(i).ListSubItems(9).Text & "','" & CvDate2(CDate(.ListItems(i).ListSubItems(6).Text)) & "','" & CvDate2(CDate(.ListItems(i).ListSubItems(7).Text)) & "'," & _
'                                                                                                                        .ListItems(i).ListSubItems(8).Text & "," & .ListItems(i).ListSubItems(2).Text & "," & .ListItems(i).ListSubItems(3).Text & "," & .ListItems(i).ListSubItems(4).Text & "," & CSng(.ListItems(i).ListSubItems(5).Text) & ",'" & _
'                                                                                                                        .ListItems(i).ListSubItems(12).Text & "'," & .ListItems(i).ListSubItems(13).Text & "," & .ListItems(i).ListSubItems(15).Text & ",'" & Cmb_Tamlay.Text & "'", , adCmdUnknown
                                                                Call SET_Execute2("exec sp_insert_landdata_apply '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','" & _
                                                                                                                        Temp_LAND_ID & "','" & .ListItems(i).ListSubItems(11).Text & "','" & .ListItems(i).ListSubItems(9).Text & "','" & CvDate2(CDate(.ListItems(i).ListSubItems(6).Text)) & "','" & CvDate2(CDate(.ListItems(i).ListSubItems(7).Text)) & "'," & _
                                                                                                                        .ListItems(i).ListSubItems(8).Text & "," & .ListItems(i).ListSubItems(2).Text & "," & .ListItems(i).ListSubItems(3).Text & "," & .ListItems(i).ListSubItems(4).Text & "," & CSng(.ListItems(i).ListSubItems(5).Text) & ",'" & _
                                                                                                                        .ListItems(i).ListSubItems(12).Text & "'," & .ListItems(i).ListSubItems(13).Text & "," & .ListItems(i).ListSubItems(15).Text & ",'" & Cmb_Tamlay.Text & "'", 8, False)
'                                                                sql_txt = "INSERT INTO LANDDATA_APPLY( LAND_APPLY_ID,LAND_ID,OWNERSHIP_ID,REMARK,START_DATE,END_DATE,APPLY_YEAR ,APPLY_RAI ,APPLY_POT,APPLY_VA , APPLY_SUMRATE,LM_ID,LAND_FLAG_APPLY,LAND_OPTION) "
'                                                                sql_txt = sql_txt & " VALUES ( '" & Temp_LAND_ID & "-" & Format$(i, "##0#") & "','"
'                                                                sql_txt = sql_txt & Temp_LAND_ID & "','"
'                                                                sql_txt = sql_txt & .ListItems(i).ListSubItems(11).Text & "','" & .ListItems(i).ListSubItems(9).Text & "','" & .ListItems(i).ListSubItems(6).Text & "','" & .ListItems(i).ListSubItems(7).Text & "',"
'                                                                sql_txt = sql_txt & .ListItems(i).ListSubItems(8).Text & "," & .ListItems(i).ListSubItems(2).Text & "," & .ListItems(i).ListSubItems(3).Text & "," & .ListItems(i).ListSubItems(4).Text & "," & CDbl(.ListItems(i).ListSubItems(5).Text) & ",'"
'                                                                sql_txt = sql_txt & .ListItems(i).ListSubItems(12).Text & "'," & .ListItems(i).ListSubItems(13).Text & "," & .ListItems(i).ListSubItems(15).Text & ")"
'                                                                Call SET_Execute(sql_txt)
                                                        Next i
                                                    End With
                                            End If
                                     End If
'                                     If IsNull(Rs.Fields(0).Value) = False Then
'                                            Call SET_Execute("UPDATE PBT5 SET PBT5_AMOUNT_ACCEPT=" & Txt_Land_SumTax.Text & " WHERE LAND_ID='" & Temp_LAND_ID & "' AND PBT5_YEAR=" & Rs.Fields(0).Value & " AND FLAG_CHANGE=0 AND PBT5_STATUS=0")
'                                    End If
End Sub

Private Function CheckBeforPost() As Boolean
    CheckBeforPost = True
    If Len(Cmb_Zoneblock.Text) + Len(Txt_Land_ID1.Text) <> 6 Then
            MsgBox "�ô�к����ʷ��Թ���ҧ��� 6 ��ѡ !", vbCritical, "�Ӫ��ᨧ !"
            CheckBeforPost = False
            Exit Function
    End If
     
    If Status = "ADD" Then
            GBQueryLandData.Requery
            GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
            If GBQueryLandData.RecordCount > 0 Then
                    GBQueryLandData.MoveFirst
                    If LenB(Txt_Land_ID2.Text) <> 0 Then
                            GBQueryLandData.Find " LAND_ID  =  '" & Cmb_Zoneblock.Text & Txt_Land_ID1.Text & "/" & Txt_Land_ID2.Text & "'"
                            Shape3(1).Tag = Cmb_Zoneblock.Text & Txt_Land_ID1.Text & "/" & Txt_Land_ID2.Text
                    Else
                            GBQueryLandData.Find " LAND_ID  =  '" & Cmb_Zoneblock.Text & Txt_Land_ID1.Text & "'"
                            Shape3(1).Tag = Cmb_Zoneblock.Text & Txt_Land_ID1.Text
                    End If
                    If GBQueryLandData.AbsolutePosition > 0 Or Chk_ID_IN_Tax(Shape3(1).Tag, 0) = False Then
                            MsgBox "���ҧ���ʷ��Թ��Ӣ��������  �ô����¹���ʷ��Թ���� !", vbCritical, "Warning!"
                            Txt_Land_ID1.SetFocus
                            CheckBeforPost = False
                            Exit Function
                    End If
            End If
    End If
     
    If CMB_TYPE_LAND.Text = Empty Then
        MsgBox "�ô�кػ������͡��÷��Թ !", vbCritical, "�Ӫ��ᨧ !"
        CheckBeforPost = False
        Exit Function
    End If
    
    If Cmb_Tambon.Text = Empty Then
                MsgBox "�ô�к� �Ӻ� ", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
                Exit Function
   End If
    If Cmb_Village.Text = Empty Then
                MsgBox "�ô�к� �����ҹ", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
                Exit Function
   End If
   
   If Cmb_Street.Text = Empty Then
                MsgBox "�ô�к� ���", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
                Exit Function
   End If
     
    If LstV_OwnerShip.ListItems.Count = 0 Then
        MsgBox "�ô�к� ������Ңͧ�����Է��� ", vbCritical, "�Ӫ��ᨧ !"
        CheckBeforPost = False
        Exit Function
    End If
    If LstV_OwnerUse.ListItems.Count = 0 Then
             MsgBox "�ô�к� ���ͼ�������ª�� ", vbCritical, "�Ӫ��ᨧ !"
             CheckBeforPost = False
             Exit Function
    End If
   
   Dim i As Byte
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LstV_OwnerShip.ListItems.Item(i).SubItems(7) = "1" Then
                 CheckBeforPost = True
                 Exit For
            Else
                 CheckBeforPost = False
           End If
     Next i
    If CheckBeforPost = False Then
               MsgBox "�ô�кؼ���ա����Է��줹�á !", vbExclamation, "�Ӫ��ᨧ "
               Exit Function
    End If
                          
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LstV_OwnerShip.ListItems.Item(i).SubItems(8) = Empty Then
                 CheckBeforPost = False
            Else
                 CheckBeforPost = True
                 Exit For
           End If
    Next i
                    If CheckBeforPost = False Then
                            MsgBox "�ô�кؼ���Ѻ������ !", vbExclamation, "�Ӫ��ᨧ "
                            Exit Function
                    End If
                     
                    For i = 1 To LstV_OwnerUse.ListItems.Count
                            If LstV_OwnerUse.ListItems(i).ListSubItems(11).Text = Empty Then
                                 MsgBox "�ô�кت��ͼ�������ª����Թ���ú�ӹǹ ", vbExclamation, "�Ӫ��ᨧ !"
                                 CheckBeforPost = False
                                 Exit Function
                            End If
                    Next i
            If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) Then
                    MsgBox "��鹷��Ŵ���͹�վ�鹷���ҡ�������ͷ���ԧ ��س���������١��ͧ���� !!!", vbExclamation, "�Ӫ��ᨧ !"
                    CheckBeforPost = False
                    Exit Function
            End If
      CheckBeforPost = CheckTotalLand("AllLand", "PassToPost")
 End Function
 
Private Function CheckTotalLand(StateCheck As String, Optional StatePass As String) As Boolean
Dim DimentionC As Single
Dim Apply_Rai As Single, Apply_Pot As Single, Apply_Va As Single
Dim Summary As Single, Temp As Single, i As Byte

        DimentionC = 0
        
   If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) Then
          Summary = (CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Val(Txt_A_Va.Text)) * 4)
   Else
        If StateCheck = "TrueLand" Or StateCheck = "AllLand" And (CSng(Txt_B_Rai.Text) + CSng(Txt_B_Pot.Text) + CSng(Val(Txt_B_Va.Text)) = 0) Then
           Summary = ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) - ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4))
        End If
        If StateCheck = "FalseLand" And (CSng(Txt_B_Rai.Text) + CSng(Txt_B_Pot.Text) + CSng(Val(Txt_B_Va.Text)) > 0) Then
            Summary = (CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)
        End If
        If StateCheck = "AllLand" And (CSng(Txt_B_Rai.Text) + CSng(Txt_B_Pot.Text) + CSng(Val(Txt_B_Va.Text)) > 0) Then
            Summary = (CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Val(Txt_A_Va.Text)) * 4)
        End If
   End If
  Dim Rai As Integer, Ngang As Integer, Va As Single
  If LstV_OwnerUse.ListItems.Count > 0 Then '���͡��������������
            For i = 1 To LstV_OwnerUse.ListItems.Count
                                If StateCheck = "TrueLand" And LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "1" Then
                                        Rai = Rai + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(2))
                                        Ngang = Ngang + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(3))
                                        Va = Va + CSng(LstV_OwnerUse.ListItems.Item(i).SubItems(4))
                                End If
                                If StateCheck = "FalseLand" And LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "0" Then
                                        Rai = Rai + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(2))
                                        Ngang = Ngang + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(3))
                                        Va = Va + CSng(LstV_OwnerUse.ListItems.Item(i).SubItems(4))
                                End If
                                If StateCheck = "AllLand" Then '�óվ�鹷���ԧ���¡��Ҿ�鹷��Ŵ���͹��ͧ���������㹵��ҧ����º
                                        Rai = Rai + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(2))
                                        Ngang = Ngang + CInt(LstV_OwnerUse.ListItems.Item(i).SubItems(3))
                                        Va = Va + CSng(LstV_OwnerUse.ListItems.Item(i).SubItems(4))
                                End If
          Next i
          DimentionC = DimentionC + (CSng(Rai) * 1600 + CSng(Ngang) * 400 + CSng(FormatNumber(Va, 2) * 4))
  End If

                            Summary = Summary - DimentionC
                                          If Summary >= 1600 Then
                                                Apply_Rai = Summary / 1600
                                                  Temp = Apply_Rai - Int(Apply_Rai)
                                                   Summary = Temp * 1600
                                          End If
                                          If Summary >= 400 Then
                                                   Apply_Pot = Summary / 400
                                                   Temp = Apply_Pot - Int(Apply_Pot)
                                                   Summary = Temp * 400
                                          End If
                                                  Apply_Va = Summary / 4

                If Int(Apply_Rai) > 0 Or Int(Apply_Pot) > 0 Or Apply_Va > 0 Then
                        If StateCheck = "TrueLand" Then
                              MsgBox "�վ�鹷�������ª���ԧ����  " & Int(Apply_Rai) & "  ���  " & Int(Apply_Pot) & "  �ҹ  " & FormatNumber(Apply_Va, 2) & "  ��.��", vbExclamation, "�Ӫ��ᨧ !"
                        End If
                        If StateCheck = "FalseLand" Then
                                MsgBox "�վ�鹷��Ŵ���͹��ԧ����  " & Int(Apply_Rai) & "  ���  " & Int(Apply_Pot) & "  �ҹ  " & FormatNumber(Apply_Va, 2) & "  ��.��", vbExclamation, "�Ӫ��ᨧ !"
                        End If
                        If StateCheck = "AllLand" Then
                              MsgBox "�ѧ��������ͷ���ѧ������͡����  " & Int(Apply_Rai) & "  ���  " & Int(Apply_Pot) & "  �ҹ  " & Format$(FormatNumber(Apply_Va, 2), "##0.00##") & "  ��.��", vbExclamation, "�Ӫ��ᨧ !"
                        End If
                        CheckTotalLand = False
                        Exit Function
                Else
                         If StatePass = Empty Then
                                 MsgBox "�Ѵ��ǹ���ͷ�������ª���������", vbExclamation, "�Ӫ��ᨧ !"
                        Else
                                  CheckTotalLand = True
                        End If
                End If
End Function

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
    Btn_Search.Enabled = False:     'Btn_Refresh.Enabled = False
    Btn_Add_OwnerUse.Enabled = True: Btn_SelectImg.Enabled = True: Btn_FullScreen.Enabled = True
    Cmb_Zoneblock.Enabled = True
    CMB_TYPE_LAND.Enabled = True
    Cmb_Tamlay.Enabled = True
    
    Chk_Flag_Assess0.Enabled = True
    Chk_Flag_Assess1.Enabled = True
    
     Chk_Flag_PayTax0.Enabled = True
     Chk_Flag_PayTax1.Enabled = True
    DTP_START_OWNER.Enabled = True
    
     Txt_Land_ID1.Locked = False:                   Txt_Land_ID1.BackColor = &H80000005
     Txt_Land_ID2.Locked = False:                     Txt_Land_ID2.BackColor = &H80000005
     
     Txt_Land_Notic.Locked = False:                Txt_Land_Notic.BackColor = &H80000005
    Txt_Land_Number.Locked = False:            Txt_Land_Number.BackColor = &H80000005
    Txt_Land_Survey.Locked = False:                Txt_Land_Survey.BackColor = &H80000005
    Txt_Land_Rawang.Locked = False:             Txt_Land_Rawang.BackColor = &H80000005
    
    Txt_A_Rai.Locked = False:                             Txt_A_Rai.BackColor = &H80000005
    Txt_A_Pot.Locked = False:                             Txt_A_Pot.BackColor = &H80000005
    Txt_A_Va.Locked = False:                               Txt_A_Va.BackColor = &H80000005
    Txt_B_Rai.Locked = False:                             Txt_B_Rai.BackColor = &H80000005
    Txt_B_Pot.Locked = False:                             Txt_B_Pot.BackColor = &H80000005
    Txt_B_Va.Locked = False:                               Txt_B_Va.BackColor = &H80000005
    
     Txt_Land_Price.Locked = False:                   Txt_Land_Price.BackColor = &H80000005
   If Chk_Flag_Assess1.Value Then Txt_SumTax.Locked = False: Txt_SumTax.BackColor = &H80000005
    
    Txt_LandData_Year.Locked = False:            Txt_LandData_Year.BackColor = &H80000005
    Txt_Apply_Rai.Locked = False:                       Txt_Apply_Rai.BackColor = &H80000005
    Txt_Apply_Pot.Locked = False:                       Txt_Apply_Pot.BackColor = &H80000005
    Txt_Apply_Va.Locked = False:                         Txt_Apply_Va.BackColor = &H80000005
    txt_Remark.Locked = False:                            txt_Remark.BackColor = &H80000005
      Cmb_LandScript.Enabled = True
      Cmb_LandUse.Enabled = True
        If STATE = "EDIT" Then
            Cmb_Zoneblock.Enabled = False
            Txt_Land_ID1.Locked = True: Txt_Land_ID1.BackColor = &HEBEBE7
            Txt_Land_ID2.Locked = True: Txt_Land_ID2.BackColor = &HEBEBE7
            
            Shape3(3).BackColor = &HC0C000
            Shape3(4).BackColor = &HC0C000
            Shape3(5).BackColor = &HC0C000
            Shape3(0).BackColor = &HC0C000
            Shape3(1).BackColor = &HC0C000
            Shape3(2).BackColor = &HC0C000
             Shape3(7).BackColor = &HC0C000
             Shape3(6).BackColor = &HC0C000
             Label2(28).Visible = True
'             Txt_StoryRemark.Visible = True
'            Cmb_LandUse.Clear
'            Cmb_LM_ID.Clear
            cb_Land_Change_ID.Clear
            cb_Land_Change.Clear
            GBQueryLandChange.Requery
            If GBQueryLandChange.RecordCount > 0 Then
                GBQueryLandChange.MoveFirst
                Do While Not GBQueryLandChange.EOF
                      cb_Land_Change_ID.AddItem GBQueryLandChange.Fields("LAND_CHANGE_ID").Value
                      cb_Land_Change.AddItem GBQueryLandChange.Fields("LAND_CHANGE_DETAILS").Value
                      GBQueryLandChange.MoveNext
                Loop
            End If
            cb_Land_Change.Visible = True
            cb_Land_Change.ListIndex = 0
            cb_Land_Change_ID.ListIndex = 0
       End If
    Lst_Tamlay_Edit = Abs(Cmb_Tamlay.ListIndex)
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                         Status = Empty
                         Txt_Land_ID1.Text = Empty
                         Txt_Land_ID2.Text = Empty
                         Txt_Land_Notic.Text = Empty
                        Txt_Land_Number.Text = Empty
                        Txt_Land_Survey.Text = Empty
                        Txt_Land_Rawang.Text = Empty
                        
                        Txt_A_Rai.Text = Empty
                        Txt_A_Pot.Text = Empty
                        Txt_A_Va.Text = Empty
                        Txt_B_Rai.Text = Empty
                        Txt_B_Pot.Text = Empty
                        Txt_B_Va.Text = Empty
                        
                        Txt_Land_Price.Text = Empty
                        Lb_Land_SumTax.Caption = Empty
                        
                        Txt_LandData_Year.Text = Empty
                        Txt_Apply_Rai.Text = Empty
                        Txt_Apply_Pot.Text = Empty
                        Txt_Apply_Va.Text = Empty
                        txt_Remark.Text = Empty
                                            
                          
                          Txt_SumTax.Text = Empty
                          Lb_Ownership_Real_ID.Caption = Empty
                          Lb_Owner_Type.Caption = Empty
                          Lb_Owner_Name.Caption = Empty
                          Lb_Owner_Address.Caption = Empty
                          Lb_Email.Caption = Empty
                          
                          LstV_Img_Name.ListItems.Clear
                          LstV_OwnerShip.ListItems.Clear
                          LstV_OwnerUse.ListItems.Clear
                          Cmb_LandUse.ListIndex = -1
                          DTP_Start_Day.Value = Now
                          DTP_End_Day.Value = Now
                          DTP_START_OWNER.Value = Now
                         picPicture.Picture = LoadPicture("")
                        picPicture.Width = 2235
                        picPicture.Height = 2085
                        picPicture.Left = 0
                        
            Shape3(3).BackColor = &HD6D6D6
            Shape3(4).BackColor = &HD6D6D6
            Shape3(5).BackColor = &HD6D6D6
            Shape3(0).BackColor = &HD6D6D6
            Shape3(1).BackColor = &HD6D6D6
            Shape3(2).BackColor = &HD6D6D6
            Shape3(7).BackColor = &HD6D6D6
            Shape3(6).BackColor = &HD6D6D6
            Label2(28).Visible = False
'             Txt_StoryRemark.Visible = False
             cb_Land_Change.Visible = False
            Label2(4).Tag = vbNullString
            Label2(6).Tag = vbNullString
    If STATE <> "ADD" Then
        Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
        Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
        Btn_Search.Enabled = True:      'Btn_Refresh.Enabled = True
        Btn_Add_OwnerUse.Enabled = False:  Btn_SelectImg.Enabled = False: Btn_FullScreen.Enabled = False
        
     Txt_Land_ID1.Locked = True:                   Txt_Land_ID1.BackColor = &HEBEBE7
     Txt_Land_ID2.Locked = True:                     Txt_Land_ID2.BackColor = &HEBEBE7
     
     Txt_Land_Notic.Locked = True:                Txt_Land_Notic.BackColor = &HEBEBE7
    Txt_Land_Number.Locked = True:            Txt_Land_Number.BackColor = &HEBEBE7
    Txt_Land_Survey.Locked = True:                Txt_Land_Survey.BackColor = &HEBEBE7
    Txt_Land_Rawang.Locked = True:             Txt_Land_Rawang.BackColor = &HEBEBE7
    DTP_START_OWNER.Enabled = False
    
    Txt_A_Rai.Locked = True:                             Txt_A_Rai.BackColor = &HEBEBE7
    Txt_A_Pot.Locked = True:                             Txt_A_Pot.BackColor = &HEBEBE7
    Txt_A_Va.Locked = True:                               Txt_A_Va.BackColor = &HEBEBE7
    Txt_B_Rai.Locked = True:                             Txt_B_Rai.BackColor = &HEBEBE7
    Txt_B_Pot.Locked = True:                             Txt_B_Pot.BackColor = &HEBEBE7
    Txt_B_Va.Locked = True:                               Txt_B_Va.BackColor = &HEBEBE7
    
    Txt_Land_Price.Locked = True:                   Txt_Land_Price.BackColor = &HEBEBE7
    Txt_SumTax.Locked = True:              Txt_SumTax.BackColor = &HEBEBE7
    
    Txt_LandData_Year.Locked = True:            Txt_LandData_Year.BackColor = &HEBEBE7
    Txt_Apply_Rai.Locked = True:                       Txt_Apply_Rai.BackColor = &HEBEBE7
    Txt_Apply_Pot.Locked = True:                       Txt_Apply_Pot.BackColor = &HEBEBE7
    Txt_Apply_Va.Locked = True:                         Txt_Apply_Va.BackColor = &HEBEBE7
    txt_Remark.Locked = True:                            txt_Remark.BackColor = &HEBEBE7
    
    Cmb_Zoneblock.Enabled = False
    CMB_TYPE_LAND.Enabled = False
    Cmb_Tamlay.Enabled = False
    
    Chk_Flag_Assess0.Enabled = False
    Chk_Flag_Assess1.Enabled = False
     Chk_Flag_PayTax0.Enabled = False
     Chk_Flag_PayTax1.Enabled = False
     
     Cmb_LandScript.Enabled = False
     Cmb_LandUse.Enabled = False
     Cmb_LandScript.ListIndex = -1
   End If
End If
       If STATE = "ADD" Then
             GBQueryDuePayTax.MoveFirst
             GBQueryDuePayTax.Find " Type_Rate = 1 ", , adSearchForward
             Txt_B_Rai.Text = GBQueryDuePayTax.Fields("Subside_Rai").Value
             Txt_B_Pot.Text = GBQueryDuePayTax.Fields("Subside_Ngan").Value
             Txt_B_Va.Text = GBQueryDuePayTax.Fields("Subside_Va").Value
        End If
End Sub
Private Sub Btn_Add_Click()
'               If TProduct = "DEMO" Then
'                   If SumCheckLand >= 500 Then
'                      MsgBox "Microtax Demo Version Limit  to 500 record maximum", vbExclamation, "Microtax"
'                      Exit Sub
'                   End If
'               End If
  Call SET_TEXTBOX("ADD", "Perpose")
  Status = "ADD"
End Sub

Private Function CheckOptionLand(StateCheck As String, StateLand As String, PatternLand As Double) As Boolean
' StateCheck = "Duplicate"  >>  �礡�ë�Ӣͧ����
' StateCheck = "Overload"  >>  �������ͷ������Թ�������
' StateLand   = "TrueLand"  >>   �����ǹ-���ͷ�����ԧ
' StateLand   = "FalseLand"  >>  �����ǹ-���ͷ��Ŵ���͹
Dim i As Byte
Dim LandCompare As Single

  If LstV_OwnerUse.ListItems.Count > 0 Then '���͡��������������
            For i = 1 To LstV_OwnerUse.ListItems.Count
                   If StateCheck = "Duplicate" Then
                                If StateLand = "TrueLand" Then
                                            If Cmb_LM_ID.Text = LstV_OwnerUse.ListItems.Item(i).SubItems(12) And LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "1" Then
                                                    MsgBox "��������������ª�� " & Cmb_LandUse.Text & " ������ !", vbInformation, "�Ӫ��ᨧ !"
                                                    CheckOptionLand = True
                                                    Exit Function
                                            End If
                                Else
                                            If Cmb_LM_ID.Text = LstV_OwnerUse.ListItems.Item(i).SubItems(12) And LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "0" Then
                                                    MsgBox "��������������ª�� " & Cmb_LandUse.Text & " ������ !", vbInformation, "�Ӫ��ᨧ !"
                                                    CheckOptionLand = True
                                                    Exit Function
                                            End If
                                End If
                   End If
                   
                   If StateCheck = "Overload" Then
                       If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) Then
                              StateLand = Empty
                              LandCompare = LandCompare + (CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(2) * 1600)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(3) * 400)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(4) * 4)))
                                        If LandCompare + ((CSng(Txt_Apply_Rai.Text) * 1600) + (CSng(Txt_Apply_Pot.Text) * 400) + (CSng(Val(Txt_Apply_Va.Text)) * 4)) > PatternLand Then
                                                                Call CheckTotalLand("AllLand")
                                                                CheckOptionLand = True
                                                                Exit Function
                                        End If
                       End If
                         If StateLand = "TrueLand" Then
                                 If LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "1" Then
                                           LandCompare = LandCompare + (CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(2) * 1600)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(3) * 400)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(4) * 4)))
                                                    If LandCompare + ((CSng(Txt_Apply_Rai.Text) * 1600) + (CSng(Txt_Apply_Pot.Text) * 400) + (CSng(Val(Txt_Apply_Va.Text)) * 4)) > PatternLand Then
                                                                'MsgBox "��͡���ͷ���������ª���Թ��鹷���ԧ !", vbInformation, "�Ӫ��ᨧ !"
                                                                Call CheckTotalLand("TrueLand")
                                                                CheckOptionLand = True
                                                                Exit Function
                                                    End If
                                  End If
                          End If
                          If StateLand = "FalseLand" Then
                                 If LstV_OwnerUse.ListItems.Item(i).SubItems(15) = "0" Then
                                          LandCompare = LandCompare + (CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(2) * 1600)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(3) * 400)) + CSng((LstV_OwnerUse.ListItems.Item(i).SubItems(4) * 4)))
                                                      If LandCompare + ((CSng(Txt_Apply_Rai.Text) * 1600) + (CSng(Txt_Apply_Pot.Text) * 400) + (CSng(Val(Txt_Apply_Va.Text)) * 4)) > PatternLand Then
                                                                'MsgBox "��͡���ͷ��Ŵ���͹�Թ��鹷���ԧ !", vbInformation, "�Ӫ��ᨧ !"
                                                                Call CheckTotalLand("FalseLand")
                                                                CheckOptionLand = True
                                                                Exit Function
                                                      End If
                                  End If
                          End If
                   End If
            Next i
  Else
      CheckOptionLand = False
 End If
End Function

Private Sub Btn_Add_OwnerUse_Click()
If Cmb_Tamlay.Text = Empty Then
   MsgBox "�ô�кط��ŷ��Թ !", vbInformation, "�Ӫ��ᨧ !"
   Exit Sub
End If
If Cmb_LandScript.Text = Empty Then
   MsgBox "�ô�кؾ�鹷�������ª�� !", vbInformation, "�Ӫ��ᨧ !"
   Exit Sub
End If
If Cmb_LandUse.ListIndex < 0 Then
    MsgBox "�ô���͡��������������ª�� !", vbInformation, "�Ӫ��ᨧ !"
    Exit Sub
End If
        If Trim$(Txt_Apply_Va.Text) = "." Then Txt_Apply_Va.Text = "0"
        If CSng(Txt_Apply_Rai.Text) + CSng(Txt_Apply_Pot.Text) + CSng(Txt_Apply_Va.Text) <= 0 Then Exit Sub
        If CSng(Txt_A_Rai.Text) + CSng(Txt_A_Pot.Text) + CSng(Txt_A_Va.Text) <= 0 Then Exit Sub
  
  Dim TrueLandSum As Double
  
   If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) Then
       TrueLandSum = (CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Val(Txt_A_Va.Text)) * 4)
   Else
          If Cmb_LandScript.ListIndex = 0 Then
             TrueLandSum = ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) - ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4))
          End If
          If Cmb_LandScript.ListIndex = 1 Then
                 TrueLandSum = FormatNumber((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4), 2)
          End If
   End If
   TrueLandSum = FormatNumber(TrueLandSum, 2, vbTrue)
     If TrueLandSum < FormatNumber((CSng(Txt_Apply_Rai.Text) * 1600) + (CSng(Txt_Apply_Pot.Text) * 400) + (CSng(Val(Txt_Apply_Va.Text)) * 4), 2) Then
          If ((CSng(Txt_B_Rai.Text) * 1600) + (CSng(Txt_B_Pot.Text) * 400) + (CSng(Val(Txt_B_Va.Text)) * 4)) > ((CSng(Txt_A_Rai.Text) * 1600) + (CSng(Txt_A_Pot.Text) * 400) + (CSng(Txt_A_Va.Text) * 4)) Then
             Call CheckTotalLand("AllLand")
             Exit Sub
          End If
          If Cmb_LandScript.ListIndex = 0 Then
             Call CheckTotalLand("TrueLand")
          Else
             Call CheckTotalLand("FalseLand")
          End If
               Exit Sub
     End If
           
           If Cmb_LandScript.ListIndex = 0 Then '��ǹ���ͷ���ԧ
                     If Not CheckOptionLand("Duplicate", "TrueLand", TrueLandSum) Then
                            If Not CheckOptionLand("Overload", "TrueLand", TrueLandSum) Then
                             Else
                                       Exit Sub
                             End If
                      Else
                            Exit Sub
                     End If
           Else
                     If Not CheckOptionLand("Duplicate", "FalseLand", TrueLandSum) Then
                            If Not CheckOptionLand("Overload", "FalseLand", TrueLandSum) Then
                            Else
                                    Exit Sub
                            End If
                     Else
                             Exit Sub
                     End If
           End If
          
                   Call CalculateTax
            
                    Set itmX = LstV_OwnerUse.ListItems.Add()
                    itmX.SmallIcon = Clone_Form.ImageList1.ListImages(3).Index
                    itmX.Text = Cmb_LandUse.Text
                    If Chk_Flag_Apply0.Value Then
                       itmX.SubItems(1) = "�����ͧ"
                    Else
                       itmX.SubItems(1) = "������"
                    End If
                      itmX.SubItems(2) = Txt_Apply_Rai.Text             '���
                      itmX.SubItems(3) = Txt_Apply_Pot.Text             '�ҹ
                      itmX.SubItems(4) = Txt_Apply_Va.Text              '��
                      itmX.SubItems(5) = Txt_SumTax.Text          '��Ť��
                      itmX.SubItems(6) = Format$(DTP_Start_Day.Value, "dd/mm/yyyy")     ' �ѹ��������
                      itmX.SubItems(7) = Format$(DTP_End_Day.Value, "dd/mm/yyyy")      ' �ѹ����ش
                      itmX.SubItems(8) = Txt_LandData_Year.Text              '��������»�
                      itmX.SubItems(9) = Trim$(txt_Remark.Text)                '�����˵�
                      itmX.SubItems(12) = Cmb_LM_ID.Text                          'LM_Id
                      itmX.SubItems(13) = CByte(Chk_Flag_Apply1.Value)                               'Land_Flag_Apply
                      itmX.SubItems(14) = Cmb_LandScript.Text
                      If Cmb_LandScript.ListIndex = 0 Then
                             itmX.SubItems(15) = "1"
                      Else
                              itmX.SubItems(15) = "0"
                      End If
                      If LstV_OwnerShip.ListItems.Count > 0 Then '�������ª��ͽ�觫���
                          itmX.SubItems(10) = LstV_OwnerShip.ListItems.Item(1).SubItems(1) '���ͼ�����
                          itmX.SubItems(11) = LstV_OwnerShip.ListItems.Item(1).SubItems(4)  ' ���ʼ�����
                      End If
                         Call CalSumTax
                         Txt_LandData_Year.Text = "0.00"
                         txt_Remark.Text = "-"
                         Txt_SumTax.Text = "0.00"
                         Txt_Apply_Rai.Text = "0"
                         Txt_Apply_Pot.Text = "0"
                         Txt_Apply_Va.Text = "0"
End Sub

Private Sub Btn_Cancel_Click()
Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_OwnerUse_Click()
If LstV_Img_Name.ListItems.Count > 0 Then LstV_Img_Name.ListItems.Remove (LstV_Img_Name.SelectedItem.Index) '9888888888888888
End Sub

Private Sub Btn_Del_Click()
If Cmb_Zoneblock.Text = Empty Then Exit Sub
If MsgBox("�׹�ѹ���ź�����ŷ���¹���Թ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
        On Error GoTo ErrHandler
        Status = "DEL"
        Globle_Connective.BeginTrans
        Call SET_DATABASE("DEL")
        Call SET_TEXTBOX("DEL")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Edit_Click()
If Cmb_Zoneblock.Text = Empty Then Exit Sub
Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
End Sub

Private Sub combo2_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Btn_FullScreen_Click()
   If LstV_Img_Name.ListItems.Count > 0 Then
   GB_ImagePath = LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1)
  Frm_FocusImage.Show vbModal
  End If
End Sub

Private Sub Btn_Post_Click()
If CheckBeforPost = False Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        sFlag = True
        Call SET_DATABASE(Status)
        Call SET_TEXTBOX("POST")
        Globle_Connective.CommitTrans
        Call SET_REFRESH
               'MsgBox "�ѹ�֡���������л�Ѻ����¹����¹���Թ����ó�", vbInformation, "�Ӫ��ᨧ"

Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Refresh_Click()
   Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
 With Frm_Search
'      .ZOrder
      .Show
        .Shp_Menu(1).BackColor = &HD6D6D6
        .Shp_Menu(3).BackColor = &HD6D6D6
        .Shp_Menu(4).BackColor = &HD6D6D6
        .Shp_Menu(5).BackColor = &HD6D6D6
        .Shp_Menu(2).BackColor = &HB8CFD6
        .SSTab1.Tab = 1
        .optCode.Visible = True
        .optName.Visible = True
        .optCode.Caption = "���ʷ��Թ"
        .cmd_SendToEdit.Caption = " << ��䢢����� - ���Թ"
End With
End Sub

Private Sub cb_Land_Change_Click()
        cb_Land_Change_ID.ListIndex = cb_Land_Change.ListIndex
End Sub

Private Sub Chk_Flag_Apply0_Click()
If Chk_Flag_Apply0.Value Then
   Chk_Flag_Apply1.Value = Unchecked
   Txt_LandData_Year.Locked = True
   Call CalculateTax
   Txt_LandData_Year.Text = Empty
Else
  Chk_Flag_Apply1.Value = Checked
End If
End Sub

Private Sub Chk_Flag_Apply1_Click()
If Chk_Flag_Apply1.Value Then
   Chk_Flag_Apply0.Value = Unchecked
        If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
          Txt_LandData_Year.Locked = False
      End If
      Call CalculateTax
Else
  Chk_Flag_Apply0.Value = Checked
  Txt_LandData_Year.Text = Empty
End If
End Sub

Private Sub Chk_Flag_Assess0_Click()
If Chk_Flag_Assess0.Value Then
   Chk_Flag_Assess1.Value = Unchecked
   Txt_SumTax.Locked = True
   Txt_SumTax.Text = Empty
   Txt_SumTax.BackColor = &HEBEBE7
   Call CalculateTax
Else
   Chk_Flag_Assess1.Value = Checked
End If
End Sub

Private Sub Chk_Flag_Assess1_Click()
If Chk_Flag_Assess1.Value Then
        Chk_Flag_Assess0.Value = Unchecked
        If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
                Txt_SumTax.Locked = False
'                Txt_SumTax.Text = "0.00"
                Txt_SumTax.BackColor = &HFFFFFF
                Txt_SumTax.SetFocus
                Txt_SumTax.SelStart = 0
                Txt_SumTax.SelLength = Len(Txt_SumTax.Text)
        End If
Else
        Chk_Flag_Assess0.Value = Checked
End If
End Sub

Private Sub Chk_Flag_PayTax0_Click()
If Chk_Flag_PayTax0.Value Then
   Chk_Flag_PayTax1.Value = Unchecked
   Lb_Land_SumTax.Caption = "0.00"
   Txt_SumTax.Text = "0.00"
Else
  Chk_Flag_PayTax1.Value = Checked
End If
End Sub

Private Sub Chk_Flag_PayTax1_Click()
If Chk_Flag_PayTax1.Value Then
        Chk_Flag_PayTax0.Value = Unchecked
        Call CalculateTax
        Call CalSumTax
Else
        Chk_Flag_PayTax0.Value = Checked
End If
End Sub

Private Sub Chk_Flag_StreetN_Click()
        If Chk_Flag_StreetN.Value Then
           Chk_Flag_StreetY.Value = Unchecked
        Else
          Chk_Flag_StreetY.Value = Checked
        End If
End Sub

Private Sub Chk_Flag_StreetY_Click()
        If Chk_Flag_StreetY.Value Then
           Chk_Flag_StreetN.Value = Unchecked
        Else
          Chk_Flag_StreetN.Value = Checked
        End If
End Sub

Private Sub Cmb_LandScript_Click()
        Call CalculateTax
End Sub

Private Sub Cmb_LandUse_Click()
        Cmb_LM_ID.ListIndex = Cmb_LandUse.ListIndex
        GBQueryLandUse.MoveFirst
        GBQueryLandUse.Find " LM_ID = '" & Cmb_LM_ID.Text & "'", , adSearchForward
        Call CalculateTax
If Cmb_LandUse.ListIndex > 3 Then
    txt_Remark.Text = Cmb_LandUse.Text
Else
    txt_Remark.Locked = False
    txt_Remark.Text = "-"
End If
End Sub

Private Sub Cmb_LandUse_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = 0
End Sub

Private Sub Cmb_Soi_Change()
       If Cmb_Street_ID.Text <> Empty And LenB(Trim$(Cmb_Soi.Text)) = 0 Then
            CMB_SOI_ID.Text = Cmb_Street_ID.Text & "-000"
       End If
End Sub

Private Sub Cmb_Soi_Click()
  CMB_SOI_ID.ListIndex = Cmb_Soi.ListIndex
End Sub

Private Sub Cmb_Soi_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
     KeyAscii = 0
End Sub

Private Sub Cmb_Street_Click()
 Cmb_Street_ID.ListIndex = Cmb_Street.ListIndex
 Cmb_Soi.Clear
 CMB_SOI_ID.Clear
 GBQuerySoi.Filter = " STREET_ID = '" & Cmb_Street_ID.Text & "'"
 If GBQuerySoi.RecordCount > 0 Then
            GBQuerySoi.MoveFirst
    Do While Not GBQuerySoi.EOF
                  If GBQuerySoi.Fields("SOI_ID").Value <> Empty Then
                      Cmb_Soi.AddItem GBQuerySoi.Fields("SOI_Name").Value
                      CMB_SOI_ID.AddItem GBQuerySoi.Fields("SOI_ID").Value
                  End If
                    GBQuerySoi.MoveNext
    Loop
    Cmb_Soi.ListIndex = 0
Else
              CMB_SOI_ID.Text = Cmb_Street_ID.Text & "-000"
End If
End Sub
Private Sub Cmb_Street_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    'KeyAscii = 0
End Sub

Private Sub Cmb_Tambon_Click()
 Cmb_Tambon_ID.ListIndex = Cmb_Tambon.ListIndex
 
 Cmb_Village.Clear
 Cmb_Village_ID.Clear
 GBQueryVillage.Filter = " Tambon_Id = '" & Cmb_Tambon_ID.Text & "'"
 If GBQueryVillage.RecordCount > 0 Then
            GBQueryVillage.MoveFirst
    Do While Not GBQueryVillage.EOF
                  If GBQueryVillage.Fields("Village_ID").Value <> Empty Then
                      Cmb_Village.AddItem GBQueryVillage.Fields("Village_Name").Value
                      Cmb_Village_ID.AddItem GBQueryVillage.Fields("Village_ID").Value
                  End If
                    GBQueryVillage.MoveNext
    Loop
                   Cmb_Village.ListIndex = 0
Else
      GBQueryVillage.Filter = " AMPHOE_ID = '" & iniAmphoe & "'"
End If
End Sub

Private Sub Cmb_Tambon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
      KeyAscii = 0
End Sub

Private Sub Cmb_Tamlay_Click()
If Cmb_Tamlay.Text <> Empty Then
        GBQueryZoneTax.MoveFirst
        GBQueryZoneTax.Find " ZONETAX_ID = " & Cmb_Tamlay.Text, , adSearchForward
        GBQueryLandUse.Filter = " ZONETAX_ID = '" & Cmb_Tamlay.Text & "'"
       
       Call SET_QUERY("SELECT A.*,B.LM_Details , PRICE_RATE From LANDUSE A , LAND_EMPLOYMENT B ,ZONE_TAX C WHERE B.LM_ID = A.LM_ID AND A.ZONETAX_ID = C.ZONETAX_ID AND A.ZONETAX_ID = '" & Cmb_Tamlay.Text & "' ORDER BY A.LM_ID", QueryLandUseRate)
       Cmb_LandUse.Clear
       Cmb_LM_ID.Clear
       If QueryLandUseRate.RecordCount > 0 Then
               QueryLandUseRate.MoveFirst
                Txt_Land_Price.Text = QueryLandUseRate.Fields("PRICE_RATE").Value
           Do While Not QueryLandUseRate.EOF
                 Cmb_LM_ID.AddItem QueryLandUseRate.Fields("LM_ID").Value
                 Cmb_LandUse.AddItem QueryLandUseRate.Fields("LM_DETAILS").Value
                 QueryLandUseRate.MoveNext
           Loop
       End If
End If
        
        If Status = "EDIT" And Cmb_Tamlay.ListIndex <> Lst_Tamlay_Edit Then
           If MsgBox("��û�Ѻ����¹������������ ��ͧ�ӡ�����ҧ��������ª����Թ���� !", vbYesNo + vbExclamation, "����͹") = vbYes Then
                     LstV_OwnerUse.ListItems.Clear
                     If Chk_Flag_Assess0.Value Then Lb_Land_SumTax.Caption = Empty
           Else
                     Cmb_Tamlay.ListIndex = Lst_Tamlay_Edit
           End If
        End If
         If Status = "ADD" Then
           If LstV_OwnerUse.ListItems.Count = 0 Then
                     Lst_Tamlay_Edit = Cmb_Tamlay.ListIndex
            Else
                If Cmb_Tamlay.ListIndex <> Lst_Tamlay_Edit Then
                 If MsgBox("��û�Ѻ����¹���� ��ͧ�ӡ�����ҧ��������ª����Թ���� !", vbYesNo + vbExclamation, "����͹") = vbYes Then
                     LstV_OwnerUse.ListItems.Clear
                    If Chk_Flag_Assess0.Value Then Lb_Land_SumTax.Caption = Empty
                Else
                     Cmb_Tamlay.ListIndex = Lst_Tamlay_Edit
                End If
                End If
          End If
         End If
End Sub

Private Sub Cmb_Tamlay_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub CMB_TYPE_LAND_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
      KeyAscii = 0
End Sub

Private Sub Cmb_Village_Click()
     Cmb_Village_ID.ListIndex = Cmb_Village.ListIndex
End Sub

Private Sub Cmb_Village_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = 0
End Sub

Private Sub Cmb_Zoneblock_Change()
If Len(Cmb_Zoneblock.Text) > 3 Then Cmb_Zoneblock.ListIndex = 0
End Sub

Private Sub Cmb_Zoneblock_Click()
 Txt_Land_ID1.Text = Empty
 Txt_Land_ID2.Text = Empty
End Sub

Private Sub Cmb_Zoneblock_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = AutoFind(Cmb_Zoneblock, KeyAscii, False)
End Sub

Private Sub Btn_SelectImg_Click()
Dim Picture_Path  As String
    With Clone_Form.CommonDialog1
            .DialogTitle = "���͡�ٻ�Ҿ�͡����Է���"
            .CancelError = True
              On Error GoTo ErrHandler
            .InitDir = "C:\"
            .Flags = cdlOFNHideReadOnly
            .Filter = "All Files (*.*)|*.*|Picture Files(*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif"     ' Set filters
            .FilterIndex = 2
            '.Action = 1
            .ShowOpen
             Picture_Path = .FileName
    End With
              If LenB(Trim$(Picture_Path)) <> 0 Then
                 Call ShowPicture(Picture_Path)
           End If
                    Dim i As Byte, File_Name   As String
                Set itmX = LstV_Img_Name.ListItems.Add()
                       itmX.SmallIcon = Clone_Form.ImageList1.ListImages(2).Index
                       
                       For i = 0 To Len(Picture_Path)
                             If Mid$(Picture_Path, Len(Picture_Path) - i, 1) <> "\" Then
                                 File_Name = File_Name + Mid$(Picture_Path, Len(Picture_Path) - i, 1)
                              Else
                                  File_Name = StrReverse(File_Name)
                                  Exit For
                             End If
                       Next i
                       itmX.Text = File_Name
                       itmX.SubItems(1) = Picture_Path
                       Exit Sub
ErrHandler:
'  picPicture.Cls
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
        Me.WindowState = vbMaximized
End Sub

Private Sub Form_Initialize()
LstV_OwnerShip.Icons = Clone_Form.ImageList1
LstV_OwnerShip.SmallIcons = Clone_Form.ImageList1

LstV_OwnerUse.Icons = Clone_Form.ImageList1
LstV_OwnerUse.SmallIcons = Clone_Form.ImageList1

LstV_Img_Name.Icons = Clone_Form.ImageList1
LstV_Img_Name.SmallIcons = Clone_Form.ImageList1
End Sub

Private Sub Form_Load()
Set QueryLandUseRate = New ADODB.Recordset
Call SET_QUERY("SELECT A.*,B.LM_Details From LANDUSE A , LAND_EMPLOYMENT B WHERE B.LM_ID = A.LM_ID", QueryLandUseRate)
Clone_Form.mnuPopup.Visible = False
sFlag = False
Call SET_REFRESH
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set QueryLandUseRate = Nothing
Set itmX = Nothing
End Sub


Private Sub Label2_Click(Index As Integer)
Select Case Index
             Case 21
                       Chk_Flag_StreetY.Value = Checked
             Case 34
                        Chk_Flag_StreetN.Value = Checked
             Case 54
                         Chk_Flag_PayTax1.Value = Checked
             Case 55
                      Chk_Flag_PayTax0.Value = Checked
End Select
End Sub

Private Sub Lb_Ownership_Real_ID_Change()
    Dim i As Byte, j As Long
    
    For i = 1 To Len(Lb_Ownership_Real_ID.Caption)
          j = j + TextWidth(Mid$(Lb_Ownership_Real_ID.Caption, i, 1))
     Next i
        If j > 795 Then
           Lb_Ownership_Real_ID.Width = 1725 + (j - 700)
           Shape1(15).Width = 1727 + (j - 698)
        Else
            Lb_Ownership_Real_ID.Width = 1725
            Shape1(15).Width = 1727
        End If
End Sub

Private Sub LstV_Img_Name_ItemClick(ByVal Item As MSComctlLib.ListItem)
If LstV_Img_Name.ListItems.Count > 0 Then Call ShowPicture(LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1))
End Sub

Private Sub LstV_Img_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
        Clone_Form.mnuAddName.Visible = False
        Clone_Form.mnuDelName.Visible = False
        Clone_Form.mnuSetOwner.Visible = False
        Clone_Form.mnuSetOwnerMail.Visible = False
        Clone_Form.mnuDelImg.Visible = True
        PopupMenu Clone_Form.mnuPopup, vbPopupMenuLeftAlign, LstV_Img_Name.Left + X, LstV_Img_Name.Top + Me.Top + y
        picPicture.Picture = LoadPicture("")
        picPicture.Width = 2235
        picPicture.Height = 2085
   End If
   End If
End Sub

Private Sub LstV_OwnerShip_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   If LstV_OwnerShip.SortOrder = lvwAscending Then
      LstV_OwnerShip.SortOrder = lvwDescending
   Else
     LstV_OwnerShip.SortOrder = lvwAscending
   End If
    LstV_OwnerShip.SortKey = ColumnHeader.Index - 1
    LstV_OwnerShip.Sorted = True
End Sub

Private Sub LstV_OwnerShip_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Lb_Owner_Type.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index)
    Lb_Owner_Name.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(1)
    Lb_Owner_Address.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(2)
    Lb_Email.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(3)
    Lb_Ownership_Real_ID.Caption = LstV_OwnerShip.ListItems.Item(LstV_OwnerShip.SelectedItem.Index).SubItems(5)
End Sub

Private Sub LstV_OwnerShip_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            OrderListView = 1
            Clone_Form.mnuAddName.Caption = "+ ������ª���"
            Clone_Form.mnuDelImg.Visible = False
            Clone_Form.mnuAddName.Visible = True
            Clone_Form.mnuDelName.Visible = True
            Clone_Form.mnuSetOwner.Visible = True
            Clone_Form.mnuSetOwnerMail.Visible = True
        PopupMenu Clone_Form.mnuPopup, vbPopupMenuLeftAlign, LstV_OwnerShip.Left + X, LstV_OwnerShip.Top + Me.Top + y
   End If
   End If
End Sub

Private Sub LstV_OwnerUse_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   If LstV_OwnerUse.SortOrder = lvwAscending Then
      LstV_OwnerUse.SortOrder = lvwDescending
   Else
     LstV_OwnerUse.SortOrder = lvwAscending
   End If
    LstV_OwnerUse.SortKey = ColumnHeader.Index - 1
    LstV_OwnerUse.Sorted = True
End Sub

Private Sub LstV_OwnerUse_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
  If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
     If LstV_OwnerUse.ListItems.Count > 0 Then
            OrderListView = 2
            Clone_Form.mnuAddName.Caption = "+ ����¹���ͼ�����"
            Clone_Form.mnuDelImg.Visible = False
            Clone_Form.mnuAddName.Visible = True
            Clone_Form.mnuDelName.Visible = True
            PopupMenu Clone_Form.mnuPopup, vbPopupMenuLeftAlign, LstV_OwnerUse.Left + X, LstV_OwnerUse.Top + Me.Top + y
     End If
  End If
End If
End Sub

Private Sub mnuDelImg_Click()
If LstV_Img_Name.ListItems.Count > 0 Then
    LstV_Img_Name.ListItems.Remove (LstV_Img_Name.SelectedItem.Index)
    picPicture.Cls
End If
End Sub

Private Sub mnuAddName_Click()
If OrderListView = 1 Then
            Status_InToFrm = "LN_USE"
            Frm_SearchOwnerShip.Show
End If
If OrderListView = 2 Then
    Status_InToFrm = "LN_APY"
    Frm_SearchOwnerShip.Show
End If
End Sub

Private Sub mnuDelName_Click()
If OrderListView = 1 Then
        If LstV_OwnerShip.ListItems.Count > 0 Then
            LstV_OwnerShip.ListItems.Remove (LstV_OwnerShip.SelectedItem.Index)
                           Lb_Ownership_Real_ID.Caption = Empty
                          Lb_Owner_Type.Caption = Empty
                          Lb_Owner_Name.Caption = Empty
                          Lb_Owner_Address.Caption = Empty
                          Lb_Email.Caption = Empty
        End If
End If
If OrderListView = 2 Then
        If LstV_OwnerUse.ListItems.Count > 0 Then
            LstV_OwnerUse.ListItems.Remove (LstV_OwnerUse.SelectedItem.Index)
            Call CalSumTax
        End If
End If
End Sub

Private Sub mnuSetOwner_Click()
With LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
                            .ListItems.Item(i).SubItems(7) = "0"
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
        End If
End With
End Sub

Private Sub mnuSetOwnerMail_Click()
With LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(8) = Empty
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
        End If
End With
End Sub

Private Sub picParent_DblClick()
Call Btn_FullScreen_Click
End Sub

Private Sub picPicture_DblClick()
Call Btn_FullScreen_Click
End Sub

Private Sub Txt_A_Pot_Change()
If Txt_A_Pot.Text = Empty Then Txt_A_Pot.Text = "0"
End Sub

Private Sub Txt_A_Pot_GotFocus()
        Txt_A_Pot.SelStart = 0
        Txt_A_Pot.SelLength = Len(Txt_A_Pot.Text)
End Sub

Private Sub Txt_A_Pot_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123")
End Sub
Private Sub Txt_A_Rai_Change()
If Txt_A_Rai.Text = Empty Then Txt_A_Rai.Text = "0"
End Sub

Private Sub Txt_A_Rai_GotFocus()
        Txt_A_Rai.SelStart = 0
        Txt_A_Rai.SelLength = Len(Txt_A_Rai.Text)
End Sub

Private Sub Txt_A_Rai_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
 KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_A_Va_Change()
If Txt_A_Va.Text = Empty Then Txt_A_Va.Text = "0"
     If CSng(Val(Txt_A_Va.Text)) > 99.99 Then
        Txt_A_Va.Text = "0"
     End If
End Sub

Private Sub Txt_A_Va_GotFocus()
        Txt_A_Va.SelStart = 0
        Txt_A_Va.SelLength = Len(Txt_A_Va.Text)
End Sub

Private Sub Txt_A_Va_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Apply_Pot_Change()
If Txt_Apply_Pot.Text = Empty Then Txt_Apply_Pot.Text = "0"
        Call CalculateTax
End Sub

Private Sub Txt_Apply_Pot_GotFocus()
        Txt_Apply_Pot.SelStart = 0
        Txt_Apply_Pot.SelLength = Len(Txt_Apply_Pot.Text)
End Sub

Private Sub Txt_Apply_Pot_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123")
End Sub

Private Sub Txt_Apply_Rai_Change()
If Txt_Apply_Rai.Text = Empty Then Txt_Apply_Rai.Text = "0"
Call CalculateTax
End Sub

Private Sub Txt_Apply_Rai_GotFocus()
       Txt_Apply_Rai.SelStart = 0
        Txt_Apply_Rai.SelLength = Len(Txt_Apply_Rai.Text)
End Sub

Private Sub Txt_Apply_Rai_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
 KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Apply_Va_Change()
If Txt_Apply_Va.Text = Empty Then Txt_Apply_Va.Text = "0"
    If CSng(Val(Txt_Apply_Va.Text)) > 99.99 Then
        Txt_Apply_Va.Text = "0"
     End If
If Trim$(Txt_Apply_Va.Text) <> "." Then Call CalculateTax
End Sub

Private Sub Txt_Apply_Va_GotFocus()
        Txt_Apply_Va.SelStart = 0
        Txt_Apply_Va.SelLength = Len(Txt_Apply_Va.Text)
End Sub

Private Sub Txt_Apply_Va_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
    If FindText(Txt_Apply_Va.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
End Sub

Private Sub Txt_B_Pot_Change()
If LenB(Trim$(Txt_B_Pot.Text)) = 0 Then Txt_B_Pot.Text = "0"
End Sub

Private Sub Txt_B_Pot_GotFocus()
        Txt_B_Pot.SelStart = 0
        Txt_B_Pot.SelLength = Len(Txt_B_Pot.Text)
End Sub

Private Sub Txt_B_Pot_KeyPress(KeyAscii As Integer)
 If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, "0123")
End Sub

Private Sub Txt_B_Rai_Change()
  If LenB(Trim$(Txt_B_Rai.Text)) = 0 Then Txt_B_Rai.Text = "0"
End Sub

Private Sub Txt_B_Rai_GotFocus()
        Txt_B_Rai.SelStart = 0
        Txt_B_Rai.SelLength = Len(Txt_B_Rai.Text)
End Sub

Private Sub Txt_B_Rai_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then SendKeys "{Tab}"
 KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_B_Va_Change()
  If Txt_B_Va.Text = Empty Then Txt_B_Va.Text = "0"
       If CSng(Val(Txt_B_Va.Text)) > 99.99 Then
        Txt_B_Va.Text = "0"
     End If
End Sub

Private Sub Txt_B_Va_GotFocus()
        Txt_B_Va.SelStart = 0
        Txt_B_Va.SelLength = Len(Txt_B_Va.Text)
End Sub

Private Sub Txt_B_Va_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Land_ID1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Land_ID1_LostFocus()
   Txt_Land_ID1.Text = Format$(Txt_Land_ID1.Text, "###000")
End Sub

Private Sub Txt_Land_ID2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Land_ID2_LostFocus()
  Txt_Land_ID2.Text = Format$(Txt_Land_ID2.Text, "###000")
End Sub

Private Sub Txt_Land_Notic_GotFocus()
        Txt_Land_Notic.SelStart = 0
        Txt_Land_Notic.SelLength = Len(Txt_Land_Notic.Text)
End Sub

Private Sub Txt_Land_Notic_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Land_Number_GotFocus()
        Txt_Land_Number.SelStart = 0
        Txt_Land_Number.SelLength = Len(Txt_Land_Number.Text)
End Sub

Private Sub Txt_Land_Number_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Land_Price_Change()
  If Txt_Land_Price.Text = Empty Then Txt_Land_Price.Text = "0.00"
End Sub

Private Sub Txt_Land_Price_GotFocus()
        Txt_Land_Price.SelStart = 0
        Txt_Land_Price.SelLength = Len(Txt_Land_Price.Text)
End Sub

Private Sub Txt_Land_Price_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
 KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Land_Price_LostFocus()
        If Trim$(Txt_Land_Price.Text) = "." Then
                Txt_Land_Price.Text = "0.00"
        End If
        Txt_Land_Price.Text = Format$(Txt_Land_Price.Text, "###,##0.00")
End Sub

Private Sub Txt_Land_Rawang_GotFocus()
        Txt_Land_Rawang.SelStart = 0
        Txt_Land_Rawang.SelLength = Len(Txt_Land_Rawang.Text)
End Sub

Private Sub Txt_Land_Rawang_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Land_Survey_GotFocus()
        Txt_Land_Survey.SelStart = 0
        Txt_Land_Survey.SelLength = Len(Txt_Land_Survey.Text)
End Sub

Private Sub Txt_Land_Survey_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_LandData_Year_Change()
If Txt_LandData_Year.Text = Empty Then Txt_LandData_Year.Text = "0.00"
End Sub

Private Sub Txt_LandData_Year_GotFocus()
        Txt_LandData_Year.SelStart = 0
        Txt_LandData_Year.SelLength = Len(Txt_LandData_Year.Text)
End Sub

Private Sub Txt_LandData_Year_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
 KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_LandData_Year_LostFocus()
        If Trim$(Txt_LandData_Year.Text) = "." Then
                Txt_LandData_Year.Text = "0.00"
        End If
        Txt_LandData_Year.Text = Format$(Txt_LandData_Year.Text, "###,##0.00")
End Sub

Private Sub Txt_Remark_Change()
        If txt_Remark.Text = Empty Then txt_Remark.Text = "-"
End Sub

Private Sub Txt_StoryRemark_Change()
        If Txt_StoryRemark.Text = Empty Then Txt_StoryRemark.Text = "-"
End Sub

Private Sub Txt_SumTax_Change()
        If Txt_SumTax.Text = Empty Then Txt_SumTax.Text = "0.00"
        If Chk_Flag_PayTax0.Value Then Txt_SumTax.Text = "0.00"
        If Txt_SumTax.Text = Empty Or (Chk_Flag_PayTax0.Value = Checked And Chk_Flag_PayTax1.Value = Unchecked) Then Txt_SumTax.Text = "0.00"
  'If csng(Txt_SumTax.text) - Int(Txt_SumTax.text) > 0 Then Txt_SumTax.text = Int(Txt_SumTax.text) + 1  ����������繨ӹǹ�������������� �ͧ�Ѵ�ʶ�
'        Txt_SumTax.Text = Format$(Txt_SumTax.Text, "0.00")
End Sub

Private Sub Txt_SumTax_GotFocus()
        Txt_SumTax.SelStart = 0
        Txt_SumTax.SelLength = Len(Txt_SumTax.Text)
End Sub

Private Sub Txt_SumTax_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_SumTax_LostFocus()
        If Trim$(Txt_SumTax.Text) = "." Then
                Txt_SumTax.Text = "0.00"
        End If
        Txt_SumTax.Text = Format$(Txt_SumTax.Text, "###,##0.00")
End Sub
