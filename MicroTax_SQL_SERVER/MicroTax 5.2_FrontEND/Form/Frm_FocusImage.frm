VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_FocusImage 
   BackColor       =   &H00EFEFEF&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   8280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11895
   Icon            =   "Frm_FocusImage.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8280
   ScaleWidth      =   11895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Btn_Full 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEFEF&
      Height          =   375
      Left            =   11340
      Picture         =   "Frm_FocusImage.frx":151A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "��Ҵ�Ҿ��ԧ"
      Top             =   1110
      Width           =   555
   End
   Begin VB.CommandButton Btn_Fit 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEFEF&
      Height          =   375
      Left            =   11340
      Picture         =   "Frm_FocusImage.frx":1C04
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "��Ҵ�Ҿ�����ͺ"
      Top             =   750
      Width           =   555
   End
   Begin VB.CommandButton Btn_ZoomOut 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEFEF&
      Height          =   375
      Left            =   11340
      Picture         =   "Frm_FocusImage.frx":22EE
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Ŵ��Ҵ�Ҿ"
      Top             =   390
      Width           =   555
   End
   Begin VB.CommandButton Btn_Zoomin 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEFEF&
      Height          =   375
      Left            =   11340
      Picture         =   "Frm_FocusImage.frx":29D8
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "�����Ҿ"
      Top             =   30
      Width           =   555
   End
   Begin VB.PictureBox picParent 
      BackColor       =   &H00000000&
      Height          =   7995
      Left            =   0
      Picture         =   "Frm_FocusImage.frx":30C2
      ScaleHeight     =   7935
      ScaleWidth      =   10965
      TabIndex        =   0
      Top             =   0
      Width           =   11025
      Begin VB.PictureBox picPicture 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   8655
         Left            =   750
         MouseIcon       =   "Frm_FocusImage.frx":24B4B
         ScaleHeight     =   8655
         ScaleWidth      =   11265
         TabIndex        =   1
         Top             =   -210
         Width           =   11265
      End
      Begin VB.Image ImgSize 
         Height          =   1305
         Left            =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin MSComCtl2.FlatScrollBar HScrollImage 
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   7995
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   503
      _Version        =   393216
      Appearance      =   2
      Arrows          =   65536
      Orientation     =   1245185
   End
   Begin MSComCtl2.FlatScrollBar VScrollImage 
      Height          =   7965
      Left            =   11040
      TabIndex        =   7
      Top             =   30
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   14049
      _Version        =   393216
      Appearance      =   2
      Orientation     =   1245184
   End
End
Attribute VB_Name = "Frm_FocusImage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim Logic As Boolean, Moving As Boolean
'Dim fso As New FileSystemObject
Dim StartX As Long, StartY As Long

Private Sub SizeImage(picBox As PictureBox, sizePic As Picture, sizeWidth As Single, sizeHeight As Single)
On Error Resume Next
        picBox.Picture = LoadPicture("")
        picBox.Width = sizeWidth
        picBox.Height = sizeHeight
        picBox.AutoRedraw = True
        picBox.PaintPicture sizePic, 0, 0, sizeWidth, sizeHeight
        picBox.Picture = picBox.Image
        picBox.AutoRedraw = False
        DoEvents
        picBox.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

Private Sub Scale_Picture()
'*********&&&&&&&&&########
If picPicture.Height > picParent.Height Then
    VScrollImage.max = picParent.Height - picPicture.Height
    VScrollImage.LargeChange = Abs(picParent.Height - picPicture.Height) / 25
Else
     VScrollImage.max = 0
End If
 If picPicture.Width > picParent.Width Then
    HScrollImage.max = picParent.Width - picPicture.Width
    HScrollImage.LargeChange = Abs(picParent.Width - picPicture.Width) / 25
 Else
  HScrollImage.max = 0
 End If
 VScrollImage.Value = 0
 HScrollImage.Value = 0
 VScrollImage.SmallChange = 1000
 HScrollImage.SmallChange = 1000
End Sub
Private Sub Btn_Fit_Click()
On Error Resume Next
        Dim ShrinkScale As Single
        
        sHeight = originalHeight
        sWidth = originalWidth
        ImgSize.Stretch = True
        ImgSize.Width = sWidth
        ImgSize.Height = sHeight
        
        sWidth = picParent.Width
        ShrinkScale = picParent.Width / ImgSize.Width
        sHeight = CSng(sHeight * ShrinkScale)
        
        If sHeight > picParent.Height Then
                ShrinkScale = picParent.Height / sHeight
                sWidth = CSng(sWidth * ShrinkScale)
                sHeight = CSng(sHeight * ShrinkScale) - 1
        End If
                ImgSize.Stretch = False
        Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
        Call Scale_Picture
                 picPicture.Top = 0
                 picParent.SetFocus
End Sub

Private Sub Btn_Full_Click()
On Error Resume Next
        sHeight = originalHeight
        sWidth = originalWidth
        ImgSize.Stretch = True
        ImgSize.Width = sWidth
        ImgSize.Height = sHeight
        ImgSize.Stretch = False
        
        Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
        Call Scale_Picture
                picPicture.Top = 0
                 picParent.SetFocus
End Sub

Private Sub Btn_Zoomin_Click()
On Error Resume Next
If sHeight >= 29000 Then Exit Sub
        sHeight = picPicture.Height + (picPicture.Height * 0.15)
        sWidth = picPicture.Width + (picPicture.Width * 0.15)
        ImgSize.Stretch = True
        ImgSize.Width = sWidth
        ImgSize.Height = sHeight
        ImgSize.Stretch = False
        
        Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
        Call Scale_Picture
         picPicture.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
         picPicture.Top = (picParent.ScaleHeight / 2) - (picPicture.ScaleHeight / 2)
         picParent.SetFocus
End Sub

Private Sub Btn_ZoomOut_Click()
On Error Resume Next
If sHeight <= 500 Then Exit Sub
        sHeight = picPicture.Height - (picPicture.Height * 0.15)
        sWidth = picPicture.Width - (picPicture.Width * 0.15)
        ImgSize.Stretch = True
        ImgSize.Width = sWidth
        ImgSize.Height = sHeight
        ImgSize.Stretch = False
        
        Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
        Call Scale_Picture
         picPicture.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
         picPicture.Top = (picParent.ScaleHeight / 2) - (picPicture.ScaleHeight / 2)
         picParent.SetFocus
End Sub

Private Sub Form_Activate()
Me.Top = 2100
End Sub

Private Sub Form_Load()
  On Error Resume Next
          If LenB(Trim$(GB_ImagePath)) <> 0 Then
                ImgSize.Stretch = False
                ImgSize.Picture = LoadPicture(GB_ImagePath)
                originalWidth = ImgSize.Width
                originalHeight = ImgSize.Height
                sWidth = ImgSize.Width
                sHeight = ImgSize.Height
                picPicture.AutoSize = True
                picPicture.Picture = LoadPicture(GB_ImagePath)
                picPicture.Left = 0
                picPicture.Top = 0
                If picPicture.Width > picParent.Width Or picPicture.Height > picParent.Height Then
                        Call Btn_Fit_Click
                End If
                      Call Scale_Picture
          End If
             picPicture.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

Private Sub HScrollImage_Change()
 picPicture.Left = HScrollImage.Value
End Sub

Private Sub HScrollImage_Scroll()
HScrollImage_Change
End Sub

Private Sub picPicture_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

    If Button = vbLeftButton Or Button = vbKeyMButton Then
        picPicture.MousePointer = 99
        StartX = X
        StartY = y
        Moving = True
    Else
        Exit Sub
    End If
End Sub

Private Sub picPicture_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
    If Moving Then
        picPicture.Move picPicture.Left + X - StartX, picPicture.Top + y - StartY
    End If
End Sub

Private Sub picPicture_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
      Moving = False
        picPicture.MousePointer = 0
End Sub

Private Sub VScrollImage_Change()
 picPicture.Top = VScrollImage.Value
End Sub

Private Sub VScrollImage_Scroll()
Call VScrollImage_Change
End Sub
