VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_SearchOwnerShip 
   BackColor       =   &H00E3DCD7&
   Caption         =   ":: Microtax Software"
   ClientHeight    =   4710
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   7125
   ControlBox      =   0   'False
   Icon            =   "Frm_SearchOwnerShip.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   Picture         =   "Frm_SearchOwnerShip.frx":151A
   ScaleHeight     =   314
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   475
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Clear."
      Height          =   315
      Left            =   6240
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1620
      Width           =   705
   End
   Begin VB.TextBox Txt_Surname 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   2070
      TabIndex        =   6
      Top             =   1620
      Width           =   3495
   End
   Begin VB.TextBox Txt_Name 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   2070
      TabIndex        =   5
      Top             =   1140
      Width           =   3495
   End
   Begin VB.ComboBox Cmb_Type 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_SearchOwnerShip.frx":CDFF
      Left            =   2760
      List            =   "Frm_SearchOwnerShip.frx":CE1E
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   630
      Width           =   2025
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_OWNERSHIP 
      Height          =   2445
      Left            =   90
      TabIndex        =   0
      Top             =   2100
      Width           =   6915
      _ExtentX        =   12197
      _ExtentY        =   4313
      _Version        =   393216
      BackColor       =   16777215
      Rows            =   3
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   15790320
      BackColorSel    =   14073244
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   15461351
      GridColorFixed  =   15856113
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      Height          =   405
      Left            =   6510
      Shape           =   3  'Circle
      Top             =   90
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Image Image1 
      Height          =   405
      Left            =   6570
      Picture         =   "Frm_SearchOwnerShip.frx":CE77
      Top             =   90
      Width           =   405
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   2040
      Top             =   1590
      Width           =   3555
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   1
      Left            =   2040
      Top             =   1110
      Width           =   3555
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   2040
      TabIndex        =   4
      Top             =   750
      Width           =   645
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʡ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   0
      Left            =   1125
      TabIndex        =   2
      Top             =   1650
      Width           =   660
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   1560
      TabIndex        =   1
      Top             =   1200
      Width           =   225
   End
End
Attribute VB_Name = "Frm_SearchOwnerShip"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Query As ADODB.Recordset

Public Sub SearchName()
        On Error GoTo ErrSearch
   Dim strTemp_Year As String
        Select Case Chk_Ownership
                Case 11, 1, 12
                            strTemp_Year = Frm_InformPayTax.txtYear.Text
                Case 2, 3, 4, 5
                            strTemp_Year = Frm_Print_Warn.txtYear.Text
                Case Else
                            strTemp_Year = Empty
        End Select
'        Debug.Print "exec sp_search_ownership " & Chk_Ownership & ",'" & Txt_Name.Text & "','" & Txt_Surname.Text & "','" & strTemp_Year & "','" & Cmb_Type.ListIndex + 1 & "'"
        Set Query = Globle_Connective.Execute("exec sp_search_ownership " & Chk_Ownership & ",'" & Txt_Name.Text & "','" & Txt_Surname.Text & "','" & strTemp_Year & "','" & Cmb_Type.ListIndex + 1 & "'", , adCmdUnknown)
'            Select Case Chk_Ownership
'                    Case 11   ' �������
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME,EMAIL ,OWNERSHIP_ID,OWNER_TYPE,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE " & _
'                            " (Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=0 AND PBT5_STATUS=0 AND PBT5_YEAR=" & Frm_InformPayTax.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_SET_GK=0 AND PRD2_STATUS=0 AND PRD2_YEAR=" & Frm_InformPayTax.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_SET_GK=0 AND PP1_STATUS=0 AND PP1_YEAR=" & Frm_InformPayTax.txtYear.Text & " ))" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 1    '�.�.1
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME,EMAIL,OWNERSHIP_ID,OWNER_TYPE,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE " & _
'                            " (Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND PBT5_YEAR=" & Frm_InformPayTax.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND PRD2_YEAR=" & Frm_InformPayTax.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS=0 AND PP1_YEAR=" & Frm_InformPayTax.txtYear.Text & " ))" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 2
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME,EMAIL,OWNERSHIP_ID,OWNER_TYPE,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE " & _
'                            " (Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND PBT5_YEAR=" & Frm_Print_Warn.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND PRD2_YEAR=" & Frm_Print_Warn.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS=0 AND PP1_YEAR=" & Frm_Print_Warn.txtYear.Text & " )" & _
'                            " OR Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID AND PBA1_SET_GK=1 AND PBA1_STATUS=0 AND PBA1_YEAR=" & Frm_Print_Warn.txtYear.Text & " ))" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 3
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME,EMAIL,OWNERSHIP_ID,OWNER_TYPE,OWNERSHIP_REAL_ID" & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_YEAR=" & Frm_Print_Warn.txtYear.Text & " AND PBT5_STATUS=0" & " )" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 4
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME,EMAIL,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID" & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM PRD2 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PRD2_YEAR=" & Frm_Print_Warn.txtYear.Text & " AND PRD2_STATUS=0" & " )" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 5
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID" & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM PP1 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PP1_YEAR=" & Frm_Print_Warn.txtYear.Text & " AND PP1_STATUS=0" & " )" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 6
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID" & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM LANDDATA_NOTIC B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID)" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 7
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID" & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM BUILDINGDATA_NOTIC B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID)" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 8
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE" & _
'                            " Exists (SELECT *  FROM SIGNBORDDATA_NOTIC B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID)" & _
'                            " OR Exists (SELECT *  FROM LICENSEDATA C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID)" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 9
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE " & _
'                            " (Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_STATUS=1)" & _
'                            " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_STATUS=1))" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case 10
'                            sql_txt = "SELECT PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID " & _
'                            " FROM OWNERSHIP AS A WHERE " & _
'                            " (Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_STATUS=1)" & _
'                            " OR Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID AND PBA1_STATUS=1))" & _
'                            " AND  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'                    Case Else
'                            sql_txt = " SELECT  PRENAME & '  ' &  OWNER_NAME & '   ' & OWNER_SURNAME , ADD_HOME & '  ����.' & ADD_MOO & '  �.' & ADD_SOI & '  �.' & ADD_ROAD & '  �.' & TAMBON_NAME & '  ' & AMPHOE_NAME & '  ' &  PROVINCE_NAME   , EMAIL ,OWNERSHIP_ID,OWNER_TYPE ,OWNERSHIP_REAL_ID " & _
'                                " FROM  OWNERSHIP A  " & _
'                                " Where  OWNER_TYPE = " & Cmb_Type.ListIndex + 1
'           End Select
'           If Txt_Surname.Text <> Empty And Txt_Name.Text <> Empty Then
'                    sql_txt = sql_txt & "  AND ( OWNER_SURNAME LIKE '" & Txt_Surname.Text & "%' and OWNER_NAME LIKE '" & Txt_Name.Text & "%')"
'           ElseIf Txt_Surname.Text <> Empty And Txt_Name.Text = Empty Then
'                    sql_txt = sql_txt & "  AND (OWNER_SURNAME LIKE '" & Txt_Surname.Text & "%' ) "
'           ElseIf Txt_Surname.Text = Empty And Txt_Name.Text <> Empty Then
'                    sql_txt = sql_txt & "  AND (OWNER_NAME LIKE '" & Txt_Name.Text & "%' )"
'           End If
'                    sql_txt = sql_txt & "  ORDER BY OWNER_NAME "
'           Call SET_QUERY(sql_txt, Query)
           DoEvents
            If Query.RecordCount > 0 Then
                    Set GRID_OWNERSHIP.DataSource = Query
            Else
                    GRID_OWNERSHIP.Clear
                    GRID_OWNERSHIP.Rows = 2
            End If
            Call Set_Grid
            Exit Sub
ErrSearch:
            MsgBox Err.Description
End Sub

Private Sub Set_Grid()
With GRID_OWNERSHIP
            .FormatString = "^��ª��� - ������|^�������|^Email"
            .ColWidth(0) = 2700: .ColAlignment(0) = 2
            .ColWidth(1) = 6500: .ColAlignment(1) = 2
            .ColWidth(2) = 1500: .ColAlignment(2) = 2
            .ColWidth(3) = 0
            .ColWidth(4) = 0
            .ColWidth(5) = 0
End With
End Sub

Private Sub Cmb_Type_Click()
If Cmb_Type.ListIndex = 0 Then
        Txt_Surname.Locked = False
        Txt_Surname.BackColor = &HFFFFFF
Else
    Txt_Surname.Text = Empty
     Txt_Surname.Locked = True
     Txt_Surname.BackColor = &HEBEBE7
End If
End Sub

Private Sub Command1_Click()
     Txt_Surname.Text = Empty
     Txt_Name.Text = Empty
     Txt_Name.SetFocus
End Sub

Private Sub Form_Activate()
Txt_Name.SetFocus
End Sub

Private Sub Form_Load()
Set Query = New ADODB.Recordset
Cmb_Type.ListIndex = 0
Call Set_Grid
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
Shape2.Visible = False
End Sub

Private Sub Form_Resize()
 If Me.WindowState = vbMaximized Or Me.WindowState = vbMinimized Then Me.WindowState = vbManual
    Me.Height = 5115
    Me.Width = 7245
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set Frm_SearchOwnerShip = Nothing
  Set Query = Nothing
End Sub

Private Sub GRID_OWNERSHIP_DblClick()
  If GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0) = Empty Then Exit Sub
     Dim i  As Byte
  If Status_InToFrm = "LN_USE" Then  ' "SG_OWN"
            With Frm_LandUse
                                            For i = 1 To .LstV_OwnerShip.ListItems.Count
                                                    If Trim$(.LstV_OwnerShip.ListItems.Item(i).SubItems(4)) = Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)) Then
                                                       MsgBox "���ͼ���͡����Է����� �ô�к����� !", vbCritical, "Microtax Software"
                                                       Exit Sub
                                                    End If
                                            Next i
            Set itmX = .LstV_OwnerShip.ListItems.Add()
            'itmX.Icon = Clone_Form.ImageList1.ListImages(1).Index
             itmX.SmallIcon = Clone_Form.ImageList1.ListImages(1).Index
            itmX.Text = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
            itmX.SubItems(1) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
            itmX.SubItems(2) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
            itmX.SubItems(3) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 2)
            itmX.SubItems(4) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            itmX.SubItems(5) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 5)
                With .LstV_OwnerShip
                          If .ListItems.Count = 1 Then
                                            .ListItems.Item(1).SubItems(6) = "�����Է��줹�á"
                                            .ListItems.Item(1).SubItems(7) = "1"
                                            .ListItems.Item(1).SubItems(8) = "����Ѻ������"
                           ElseIf .ListItems.Count > 1 Then
                                                      itmX.SubItems(6) = "�����Է�������"
                                                      itmX.SubItems(7) = "0"
                                                      itmX.SubItems(8) = Empty
                        End If
                End With
            End With
  End If
    If Status_InToFrm = "LN_APY" Then
          With Frm_LandUse.LstV_OwnerUse
                   .ListItems.Item(.SelectedItem.Index).SubItems(10) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
                   .ListItems.Item(.SelectedItem.Index).SubItems(11) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
          End With
    End If
    
    If Status_InToFrm = "SG_OWN" Then
        With Frm_SignBord
                                            For i = 1 To .LstV_OwnerShip.ListItems.Count
                                                    If Trim$(.LstV_OwnerShip.ListItems.Item(i).SubItems(4)) = Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)) Then
                                                       MsgBox "���ͼ���͡����Է����� �ô�к����� !", vbCritical, "Microtax Software"
                                                       Exit Sub
                                                    End If
                                            Next i
            Set itmX = .LstV_OwnerShip.ListItems.Add()
            'itmX.Icon = Clone_Form.ImageList1.ListImages(1).Index
             itmX.SmallIcon = Clone_Form.ImageList1.ListImages(1).Index
            itmX.Text = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
            itmX.SubItems(1) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
            itmX.SubItems(2) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
            itmX.SubItems(3) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 2)
            itmX.SubItems(4) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            itmX.SubItems(5) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 5)
                With .LstV_OwnerShip
                          If .ListItems.Count = 1 Then
                                            .ListItems.Item(1).SubItems(6) = "�����Է��줹�á"
                                            .ListItems.Item(1).SubItems(7) = "1"
                                            .ListItems.Item(1).SubItems(8) = "����Ѻ������"
                           ElseIf .ListItems.Count > 1 Then
                                                      itmX.SubItems(6) = "�����Է�������"
                                                      itmX.SubItems(7) = "0"
                                                      itmX.SubItems(8) = Empty
                        End If
                End With
      End With
End If
  If Status_InToFrm = "SG_APY" Then
     With Frm_SignBord.LstV_OwnerUse
                   .ListItems.Item(.SelectedItem.Index).SubItems(1) = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
                   .ListItems.Item(.SelectedItem.Index).SubItems(2) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
                   .ListItems.Item(.SelectedItem.Index).SubItems(3) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
                   .ListItems.Item(.SelectedItem.Index).SubItems(4) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
                   .ListItems.Item(.SelectedItem.Index).SubItems(5) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4)
     End With
  End If
  
    If Status_InToFrm = "BD_OWN" Then
       With Frm_Building
                                            For i = 1 To .LstV_OwnerShip.ListItems.Count
                                                    If Trim$(.LstV_OwnerShip.ListItems.Item(i).SubItems(4)) = Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)) Then
                                                       MsgBox "���ͼ���͡����Է����� �ô�к����� !", vbCritical, "Microtax Software"
                                                       Exit Sub
                                                    End If
                                            Next i
            Set itmX = .LstV_OwnerShip.ListItems.Add()
            'itmX.Icon = Clone_Form.ImageList1.ListImages(1).Index
             itmX.SmallIcon = Clone_Form.ImageList1.ListImages(1).Index
            itmX.Text = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
            itmX.SubItems(1) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
            itmX.SubItems(2) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
            itmX.SubItems(3) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 2)
            itmX.SubItems(4) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            itmX.SubItems(5) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 5)
                With .LstV_OwnerShip
                          If .ListItems.Count = 1 Then
                                            .ListItems.Item(1).SubItems(6) = "�����Է��줹�á"
                                            .ListItems.Item(1).SubItems(7) = "1"
                                            .ListItems.Item(1).SubItems(8) = "����Ѻ������"
                           ElseIf .ListItems.Count > 1 Then
                                                      itmX.SubItems(6) = "�����Է�������"
                                                      itmX.SubItems(7) = "0"
                                                      itmX.SubItems(8) = Empty
                        End If
                End With
       End With
    End If
  
  If Status_InToFrm = "BD_APY" Then
     With Frm_Building
            Set itmX = .LstV_OwnerUse.ListItems.Add()
            'itmX.Icon = Clone_Form.ImageList1.ListImages(1).Index
             itmX.SmallIcon = Clone_Form.ImageList1.ListImages(1).Index
            itmX.Text = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
            itmX.SubItems(1) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
            itmX.SubItems(2) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
            itmX.SubItems(3) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 2)
            itmX.SubItems(4) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            itmX.SubItems(5) = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 5)
     End With
End If

  If Status_InToFrm = "LICENSE" Then
       With Frm_License
                .Lb_Owner_ID.Caption = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
                .Lb_Owner_Name.Caption = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
                .Lb_Owner_Add.Caption = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)
                .Lb_Owner_Type.Caption = Owner_Type(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 4))
       End With
       
  End If
    If Status_InToFrm = "OW_SHP" Then
       Frm_Ownership.Op_Type(Cmb_Type.ListIndex + 1).Value = True
       GBQueryOwnerShip.Filter = "OWNER_TYPE = " & Cmb_Type.ListIndex + 1 & " AND OWNERSHIP_ID= '" & GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3) & "'"
       Call Frm_Ownership.Set_Grid
       Frm_Ownership.Call_Grid_Click
    End If
    
    If Status_InToFrm = "WARN" Then
            With Frm_Print_Warn
                    .Text1(2).Text = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
                    .Text1(2).Tag = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            End With
            Frm_SearchOwnerShip.Hide
            Txt_Name.SelStart = 0
            Txt_Name.SelLength = Len(Trim$(Txt_Name.Text))
            Txt_Surname.Text = Empty
    End If
    
    If Status_InToFrm = "INFORM_TAX" Then
            With Frm_InformPayTax
                   .Text1(2).Text = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0)
                   .Text1(2).Tag = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 3)
            End With
            Frm_SearchOwnerShip.Hide
            Txt_Name.SelStart = 0
            Txt_Name.SelLength = Len(Trim$(Txt_Name.Text))
            Txt_Surname.Text = Empty
    End If
    
    If Status_InToFrm = "PT4" Then
        Frm_Print.Cmb_Type.ListIndex = Cmb_Type.ListIndex
        Frm_Print.DataCombo1.Text = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, GRID_OWNERSHIP.Cols - 1)
        Frm_SearchOwnerShip.Hide
    End If
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Shape2.Visible = False
Image1.Top = Image1.Top + 2
End Sub

Private Sub Image1_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
Shape2.Visible = True
End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
Image1.Top = 6
  Me.Visible = False
End Sub

Private Sub Txt_Name_Change()
' Call KILL_SINGLEQOUTE(Txt_Name)
End Sub

Private Sub Txt_Name_DblClick()
     Txt_Name.Text = Empty
End Sub

Private Sub Txt_Name_KeyPress(KeyAscii As Integer)
If KeyAscii = 39 Then
        KeyAscii = 0
        Exit Sub
End If
If KeyAscii = 13 Then
Call Txt_Surname_KeyPress(KeyAscii)
End If
End Sub

Private Sub Txt_Surname_Change()
'If KeyAscii = 39 Then
'        KeyAscii = 0
'        Exit Sub
'End If
' Call KILL_SINGLEQOUTE(Txt_Surname)
End Sub

Private Sub Txt_Surname_DblClick()
     Txt_Surname.Text = Empty
End Sub

Private Sub Txt_Surname_KeyPress(KeyAscii As Integer)
 If KeyAscii = 13 Then
   Call SearchName
End If
End Sub
