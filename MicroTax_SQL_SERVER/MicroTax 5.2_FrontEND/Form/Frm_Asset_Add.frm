VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Asset_Add 
   ClientHeight    =   8895
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   13230
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Asset_Add.frx":0000
   ScaleHeight     =   8895
   ScaleWidth      =   13230
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdFind 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   9000
      Picture         =   "Frm_Asset_Add.frx":B17C
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   2940
      Width           =   615
   End
   Begin VB.CommandButton cmd_Save 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�Ѵ����¡�� "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   4350
      Picture         =   "Frm_Asset_Add.frx":B706
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   8280
      Width           =   2355
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00E0E0E0&
      Height          =   555
      Left            =   6690
      Picture         =   "Frm_Asset_Add.frx":BC90
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   8280
      Width           =   2205
   End
   Begin VB.TextBox Txt_BOOK_NUMBER 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   9420
      MaxLength       =   8
      TabIndex        =   6
      Top             =   6450
      Width           =   1905
   End
   Begin VB.TextBox Txt_BOOK_NO 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   6660
      MaxLength       =   12
      TabIndex        =   5
      Top             =   6420
      Width           =   1635
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Clear."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   9780
      MaskColor       =   &H00404000&
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2940
      Width           =   615
   End
   Begin VB.TextBox txt_Remark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2970
      TabIndex        =   8
      Top             =   7590
      Width           =   8355
   End
   Begin VB.TextBox Txt_Summary 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   480
      Left            =   9420
      TabIndex        =   7
      Text            =   "0.00"
      Top             =   6930
      Width           =   1905
   End
   Begin VB.ComboBox Cmb_StoryAll 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Asset_Add.frx":C37A
      Left            =   5040
      List            =   "Frm_Asset_Add.frx":C38A
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1800
      Width           =   3795
   End
   Begin VB.TextBox txtSurName 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0EAED&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   5070
      TabIndex        =   3
      Top             =   3000
      Width           =   3735
   End
   Begin VB.TextBox txtName 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0EAED&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5070
      TabIndex        =   2
      Top             =   2640
      Width           =   3735
   End
   Begin VB.ComboBox cmb_OwnerType 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Asset_Add.frx":C3DE
      Left            =   5040
      List            =   "Frm_Asset_Add.frx":C400
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2220
      Width           =   3795
   End
   Begin MSMask.MaskEdBox MaskEdBox2 
      Height          =   465
      Left            =   6660
      TabIndex        =   4
      Top             =   6930
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   820
      _Version        =   393216
      BackColor       =   16777215
      ForeColor       =   0
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   2385
      Left            =   1860
      TabIndex        =   18
      Top             =   3390
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   4207
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   16777152
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   7740
      TabIndex        =   23
      Top             =   1410
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196621
      OrigLeft        =   7815
      OrigTop         =   930
      OrigRight       =   8070
      OrigBottom      =   1260
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65537
      Enabled         =   -1  'True
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6990
      TabIndex        =   25
      Top             =   1410
      Width           =   750
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻվط��ѡ�Ҫ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   5250
      TabIndex        =   24
      Top             =   1470
      Width           =   1680
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   10
      Left            =   6090
      TabIndex        =   22
      Top             =   6600
      Width           =   540
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ��� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   11
      Left            =   8820
      TabIndex        =   21
      Top             =   6630
      Width           =   540
   End
   Begin VB.Label lb_Asset_Type 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   300
      Left            =   2040
      TabIndex        =   19
      Top             =   6540
      Width           =   1995
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   5850
      TabIndex        =   15
      Top             =   7110
      Width           =   765
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H004A6873&
      Height          =   315
      Index           =   1
      Left            =   5040
      Top             =   2970
      Width           =   3795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H004A6873&
      Height          =   315
      Index           =   125
      Left            =   5040
      Top             =   2610
      Width           =   3795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H005C5C5F&
      BorderColor     =   &H005C5C5F&
      Height          =   1935
      Index           =   2
      Left            =   1890
      Top             =   6240
      Width           =   9750
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Index           =   5
      Left            =   2010
      TabIndex        =   17
      Top             =   7650
      Width           =   840
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Ť������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   16
      Left            =   8460
      TabIndex        =   16
      Top             =   7080
      Width           =   885
   End
   Begin VB.Label lb_name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1860
      TabIndex        =   14
      Top             =   5820
      Width           =   9795
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00C0C0C0&
      Height          =   1995
      Index           =   0
      Left            =   1860
      Top             =   6210
      Width           =   9795
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   3
      Left            =   3870
      TabIndex        =   13
      Top             =   1890
      Width           =   1095
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ʡ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   2
      Left            =   4530
      TabIndex        =   12
      Top             =   3060
      Width           =   435
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   1
      Left            =   4620
      TabIndex        =   11
      Top             =   2700
      Width           =   345
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   6
      Left            =   4200
      TabIndex        =   10
      Top             =   2280
      Width           =   765
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Ǵ�����Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   0
      Left            =   5820
      TabIndex        =   9
      Top             =   300
      Width           =   1845
   End
End
Attribute VB_Name = "Frm_Asset_Add"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Rs As ADODB.Recordset
Dim tempArray(8)

Private Sub Btn_Print_Click()
        fPrint = 0
        Frm_Print_Asset_Extend.Show vbModal
End Sub

Private Sub cmd_Save_Click()
Dim strSQL As String
On Error GoTo ErrSave
Globle_Connective.BeginTrans
If MsgBox("��ͧ��úѹ�֡�������Թ������������� ?", vbYesNo, "�׹�ѹ") = vbYes Then
        Set Rs = Globle_Connective.Execute("exec sp_chk_key_assetdata_extend '" & Trim$(Txt_BOOK_NO.Text) & "','" & Trim$(Txt_BOOK_NUMBER.Text) & "'," & lb_name.Tag & "," & Lb_Year, , adCmdUnknown)
        If Rs.RecordCount > 0 Then
                MsgBox "�������,�Ţ�����㹻վ.�." & Lb_Year.Caption, vbExclamation, "�����żԴ��Ҵ"
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        Globle_Connective.Execute "exec sp_insert_assetdata_extend '" & Trim$(Txt_BOOK_NO.Text) & "','" & Trim$(Txt_BOOK_NUMBER.Text) & "'," & Lb_Year.Caption & _
        ",'" & tempArray(0) & "','" & tempArray(5) & "','" & tempArray(3) & "','" & tempArray(4) & "'," & tempArray(2) & "," & lb_name.Tag & "," & CCur(Txt_Summary.Text) & ",'" & CvDate2(CDate(MaskEdBox2.Text)) & "','" & _
        strUser & "','" & CvDate2(Now, 1) & " " & Time & "','" & Trim$(txt_Remark.Text) & "'", , adCmdUnknown
'        strSQL = "INSERT INTO ASSETDATA_ADD (OWNERSHIP_ID,PRENAME,OWNER_NAME,OWNER_SURNAME,OWNER_TYPE,TAX_TYPE,TAX_MONEY,PAY_DATE,USER_LOGIN,DATE_ACCESS,ASSET_REMARK) VALUES ('" & _
'       tempArray(0) & "','" & tempArray(5) & "','" & tempArray(3) & "','" & tempArray(4) & "'," & tempArray(2) & "," & lb_name.Tag & "," & CCur(Txt_Summary.Text) & ",'" & Format$(CDate(MaskEdBox2.Text), "dd/mm/yyyy") & "','" & _
'        strUser & "','" & Now & "','" & Trim$(txt_Remark.Text) & "')"
        Globle_Connective.CommitTrans
        txt_Remark.Text = vbNullString
'        MaskEdBox2.Text = "__/__/____"
        Txt_Summary.Text = "0.00"
        Txt_BOOK_NO.Text = vbNullString
        Txt_BOOK_NUMBER.Text = vbNullString
        Grid_Result.Rows = 2
        Grid_Result.Clear
        lb_name.Caption = vbNullString
        lb_Asset_Type.Caption = vbNullString
        Call SetGrid
Else
        Globle_Connective.RollbackTrans
End If
Exit Sub
ErrSave:
        Globle_Connective.RollbackTrans
        MsgBox Err.Description
End Sub

Private Sub cmdFind_Click()
        On Error GoTo ErrFind
        
        Dim strSQL As String
        Me.MousePointer = 11
        lb_Asset_Type.Caption = vbNullString
        If Cmb_StoryAll.ListIndex > 0 Then
'                Debug.Print "exec sp_find_asset_add '" & cmb_OwnerType.ListIndex & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "'," & CInt(Cmb_StoryAll.ListIndex)
                Set Rs = Globle_Connective.Execute("exec sp_find_asset_add '" & cmb_OwnerType.ListIndex & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "'," & CInt(Cmb_StoryAll.ListIndex), , adCmdUnknown)
                If Rs.RecordCount > 0 Then
                        Set Grid_Result.DataSource = Rs
                End If
                lb_name.Tag = Cmb_StoryAll.ListIndex
                Call SetGrid
        End If
        Me.MousePointer = 0
        Exit Sub
ErrFind:
        MsgBox Err.Description
        Me.MousePointer = 0
End Sub

Private Sub Command1_Click()
        txtName.Text = vbNullString: txtName.BackColor = &HE0EAED
        txtSurName.Text = vbNullString: txtSurName.BackColor = &HE0EAED
        Txt_BOOK_NO.Text = vbNullString
        Txt_BOOK_NUMBER.Text = vbNullString
        Txt_Summary.Text = "0.00"
        MaskEdBox2.Text = "__/__/____"
        cmb_OwnerType.ListIndex = 0
        Cmb_StoryAll.ListIndex = 0
        Grid_Result.Rows = 2
        Grid_Result.Clear
        Call SetGrid
End Sub

Private Sub Form_Load()
        Set Rs = New ADODB.Recordset
        Call SetGrid
        Lb_Year.Caption = Right$(Date, 4)
        cmb_OwnerType.ListIndex = 0
        Cmb_StoryAll.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Rs = Nothing
        Set Frm_Asset_Add = Nothing
End Sub

Private Sub Grid_Result_Click()
        
        Dim i As Integer
        
        If Grid_Result.TextMatrix(1, 0) = Empty Then Exit Sub
        lb_name.Caption = Grid_Result.TextMatrix(Grid_Result.Row, 1)
        For i = 0 To 5
                tempArray(i) = Grid_Result.TextMatrix(Grid_Result.Row, i)
        Next i
        Select Case lb_name.Tag
                Case "1"
                        lb_Asset_Type.Caption = "���պ��ا��ͧ���"
                Case "2"
                        lb_Asset_Type.Caption = "�����ç���͹��з��Թ"
                Case "3"
                        lb_Asset_Type.Caption = "���ջ���"
        End Select
        MaskEdBox2.SetFocus
End Sub

Private Sub SetGrid()
        With Grid_Result
                .FormatString = "|����|"
                .ColWidth(0) = 0
                .ColWidth(1) = 9000: .ColAlignmentFixed(1) = 4
                .ColWidth(2) = 0
                .ColWidth(3) = 0
                .ColWidth(4) = 0
                .ColWidth(5) = 0
                .ColWidth(6) = 0
                .ColWidth(7) = 0
        End With
End Sub

Private Sub MaskEdBox2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub MaskEdBox2_LostFocus()
        If InStr(1, MaskEdBox2.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(MaskEdBox2.Text) And Mid$(MaskEdBox2.Text, 4, 2) < 13 Then
             
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox2.SetFocus
        End If
End Sub

Private Sub Txt_BOOK_NO_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_BOOK_NUMBER_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
If KeyAscii = 13 Then SendKeys "{Tab}"
Txt_Summary.SelLength = Len(Txt_Summary.Text)
End Sub

Private Sub Txt_Summary_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
        If FindText(Txt_Summary.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Summary_LostFocus()
        Txt_Summary.Text = Format$(Txt_Summary.Text, "##,##0.00")
End Sub

Private Sub txtName_GotFocus()
        txtName.BackColor = &HFFFFFF
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdFind_Click
End Sub

Private Sub txtName_LostFocus()
        txtName.BackColor = &HE0EAED
End Sub

Private Sub txtSurName_GotFocus()
        txtSurName.BackColor = &HFFFFFF
End Sub

Private Sub txtSurName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdFind_Click
End Sub

Private Sub txtSurName_LostFocus()
        txtSurName.BackColor = &HE0EAED
End Sub


