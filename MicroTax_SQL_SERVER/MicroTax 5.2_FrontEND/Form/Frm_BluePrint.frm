VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Blueprint 
   BackColor       =   &H00808080&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "����������Ҥ������ç���͹"
   ClientHeight    =   3075
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5115
   Icon            =   "Frm_BluePrint.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   5115
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 2 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   1
      Left            =   30
      TabIndex        =   37
      Top             =   3090
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY2 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   5
         Text            =   "0"
         Top             =   1260
         Width           =   915
      End
      Begin VB.TextBox txtX2 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   3
         Text            =   "0"
         Top             =   465
         Width           =   915
      End
      Begin VB.TextBox txtX2 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   4
         Text            =   "0"
         Top             =   795
         Width           =   915
      End
      Begin VB.TextBox txtY2 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   6
         Text            =   "0"
         Top             =   1590
         Width           =   915
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   11
         Left            =   -15
         TabIndex        =   45
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   10
         Left            =   1530
         TabIndex        =   44
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   9
         Left            =   0
         TabIndex        =   43
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   8
         Left            =   1530
         TabIndex        =   42
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   7
         Left            =   0
         TabIndex        =   41
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   6
         Left            =   -15
         TabIndex        =   40
         Top             =   1320
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   5
         Left            =   1530
         TabIndex        =   39
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   4
         Left            =   1530
         TabIndex        =   38
         Top             =   1320
         Width           =   420
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "���ҧ"
      Height          =   315
      Left            =   3315
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   2730
      Width           =   885
   End
   Begin VB.CommandButton Command1 
      Caption         =   "�͡"
      Height          =   315
      Left            =   4200
      TabIndex        =   112
      Top             =   2730
      Width           =   885
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 8 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   7
      Left            =   3150
      TabIndex        =   99
      Top             =   3270
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY8 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   103
         Text            =   "0"
         Top             =   1350
         Width           =   915
      End
      Begin VB.TextBox txtX8 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   102
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtX8 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   101
         Text            =   "0"
         Top             =   765
         Width           =   915
      End
      Begin VB.TextBox txtY8 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   100
         Text            =   "0"
         Top             =   1665
         Width           =   915
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   67
         Left            =   1530
         TabIndex        =   111
         Top             =   1410
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   66
         Left            =   1530
         TabIndex        =   110
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   65
         Left            =   -15
         TabIndex        =   109
         Top             =   1410
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   64
         Left            =   0
         TabIndex        =   108
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   63
         Left            =   1530
         TabIndex        =   107
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   62
         Left            =   0
         TabIndex        =   106
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   29
         Left            =   1530
         TabIndex        =   105
         Top             =   1755
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   28
         Left            =   -15
         TabIndex        =   104
         Top             =   1755
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 7 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   6
      Left            =   2430
      TabIndex        =   86
      Top             =   3090
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY7 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   30
         Text            =   "0"
         Top             =   1860
         Width           =   915
      End
      Begin VB.TextBox txtX7 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   27
         Text            =   "0"
         Top             =   765
         Width           =   915
      End
      Begin VB.TextBox txtX7 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   26
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtY7 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   29
         Text            =   "0"
         Top             =   1560
         Width           =   915
      End
      Begin VB.TextBox txtX7 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   2
         Left            =   510
         TabIndex        =   28
         Text            =   "0"
         Top             =   1080
         Width           =   915
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   61
         Left            =   -15
         TabIndex        =   96
         Top             =   2010
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   60
         Left            =   1530
         TabIndex        =   95
         Top             =   1965
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   59
         Left            =   0
         TabIndex        =   94
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   58
         Left            =   1530
         TabIndex        =   93
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   57
         Left            =   0
         TabIndex        =   92
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   56
         Left            =   -15
         TabIndex        =   91
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   55
         Left            =   1530
         TabIndex        =   90
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   54
         Left            =   1530
         TabIndex        =   89
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X3 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   53
         Left            =   0
         TabIndex        =   88
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   52
         Left            =   1530
         TabIndex        =   87
         Top             =   1230
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 6 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   5
      Left            =   1740
      TabIndex        =   75
      Top             =   3090
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   2
         Left            =   510
         TabIndex        =   25
         Text            =   "0"
         Top             =   2205
         Width           =   915
      End
      Begin VB.TextBox txtY6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   24
         Text            =   "0"
         Top             =   1905
         Width           =   915
      End
      Begin VB.TextBox txtX6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   510
         TabIndex        =   21
         Text            =   "0"
         Top             =   765
         Width           =   915
      End
      Begin VB.TextBox txtX6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   20
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtY6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   510
         TabIndex        =   23
         Text            =   "0"
         Top             =   1605
         Width           =   915
      End
      Begin VB.TextBox txtX6 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   2
         Left            =   510
         TabIndex        =   22
         Text            =   "0"
         Top             =   1080
         Width           =   915
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   23
         Left            =   -15
         TabIndex        =   98
         Top             =   2355
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   22
         Left            =   1530
         TabIndex        =   97
         Top             =   2355
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   51
         Left            =   -15
         TabIndex        =   85
         Top             =   2010
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   50
         Left            =   1530
         TabIndex        =   84
         Top             =   2010
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   49
         Left            =   0
         TabIndex        =   83
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   48
         Left            =   1530
         TabIndex        =   82
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   47
         Left            =   0
         TabIndex        =   81
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   46
         Left            =   -15
         TabIndex        =   80
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   45
         Left            =   1530
         TabIndex        =   79
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   44
         Left            =   1530
         TabIndex        =   78
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X3 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   43
         Left            =   0
         TabIndex        =   77
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   42
         Left            =   1530
         TabIndex        =   76
         Top             =   1230
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 5 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   4
      Left            =   1050
      TabIndex        =   64
      Top             =   3060
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY5 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   465
         TabIndex        =   19
         Text            =   "0"
         Top             =   1905
         Width           =   915
      End
      Begin VB.TextBox txtX5 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   465
         TabIndex        =   16
         Text            =   "0"
         Top             =   795
         Width           =   915
      End
      Begin VB.TextBox txtX5 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   465
         TabIndex        =   15
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtY5 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   465
         TabIndex        =   18
         Text            =   "0"
         Top             =   1605
         Width           =   915
      End
      Begin VB.TextBox txtX5 
         Alignment       =   1  'Right Justify
         Height          =   300
         Index           =   2
         Left            =   465
         TabIndex        =   17
         Text            =   "0"
         Top             =   1140
         Width           =   915
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   41
         Left            =   -15
         TabIndex        =   74
         Top             =   2010
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   40
         Left            =   1485
         TabIndex        =   73
         Top             =   2010
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   39
         Left            =   0
         TabIndex        =   72
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   38
         Left            =   1485
         TabIndex        =   71
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   37
         Left            =   0
         TabIndex        =   70
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   36
         Left            =   -15
         TabIndex        =   69
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   35
         Left            =   1485
         TabIndex        =   68
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   34
         Left            =   1485
         TabIndex        =   67
         Top             =   1665
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X3 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   33
         Left            =   0
         TabIndex        =   66
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   32
         Left            =   1485
         TabIndex        =   65
         Top             =   1230
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 4 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   3
      Left            =   780
      TabIndex        =   57
      Top             =   3420
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtY4 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   1
         Left            =   510
         TabIndex        =   14
         Text            =   "0"
         Top             =   1380
         Width           =   915
      End
      Begin VB.TextBox txtX4 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   0
         Left            =   510
         TabIndex        =   12
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtY4 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   0
         Left            =   510
         TabIndex        =   13
         Text            =   "0"
         Top             =   1080
         Width           =   915
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   31
         Left            =   -15
         TabIndex        =   63
         Top             =   1485
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   30
         Left            =   1530
         TabIndex        =   62
         Top             =   1485
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   27
         Left            =   0
         TabIndex        =   61
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   26
         Left            =   -15
         TabIndex        =   60
         Top             =   1140
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   25
         Left            =   1530
         TabIndex        =   59
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   24
         Left            =   1530
         TabIndex        =   58
         Top             =   1140
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 3 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2655
      Index           =   2
      Left            =   390
      TabIndex        =   46
      Top             =   3270
      Visible         =   0   'False
      Width           =   1815
      Begin VB.TextBox txtX3 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   2
         Left            =   510
         TabIndex        =   9
         Text            =   "0"
         Top             =   1080
         Width           =   915
      End
      Begin VB.TextBox txtY3 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   0
         Left            =   510
         TabIndex        =   10
         Text            =   "0"
         Top             =   1650
         Width           =   915
      End
      Begin VB.TextBox txtX3 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   0
         Left            =   510
         TabIndex        =   7
         Text            =   "0"
         Top             =   450
         Width           =   915
      End
      Begin VB.TextBox txtX3 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   1
         Left            =   510
         TabIndex        =   8
         Text            =   "0"
         Top             =   765
         Width           =   915
      End
      Begin VB.TextBox txtY3 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   300
         Index           =   1
         Left            =   510
         TabIndex        =   11
         Text            =   "0"
         Top             =   1950
         Width           =   915
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   21
         Left            =   1530
         TabIndex        =   56
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X3 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   20
         Left            =   0
         TabIndex        =   55
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   19
         Left            =   1530
         TabIndex        =   54
         Top             =   1710
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   18
         Left            =   1530
         TabIndex        =   53
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   17
         Left            =   -15
         TabIndex        =   52
         Top             =   1710
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   16
         Left            =   0
         TabIndex        =   51
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   15
         Left            =   1530
         TabIndex        =   50
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   14
         Left            =   0
         TabIndex        =   49
         Top             =   870
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   13
         Left            =   1530
         TabIndex        =   48
         Top             =   2055
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y2 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   12
         Left            =   -15
         TabIndex        =   47
         Top             =   2055
         Width           =   420
      End
   End
   Begin VB.Frame fraParameter 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   " Type 1 "
      ForeColor       =   &H00FFFFFF&
      Height          =   2715
      Index           =   0
      Left            =   3285
      TabIndex        =   31
      Top             =   0
      Width           =   1815
      Begin VB.TextBox txtY1 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   0
         Left            =   465
         TabIndex        =   2
         Text            =   "0"
         Top             =   765
         Width           =   870
      End
      Begin VB.TextBox txtX1 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H005C5C5F&
         Height          =   330
         Index           =   0
         Left            =   465
         TabIndex        =   1
         Text            =   "0"
         Top             =   450
         Width           =   870
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   3
         Left            =   1440
         TabIndex        =   35
         Top             =   855
         Width           =   420
      End
      Begin VB.Label lblDim 
         BackStyle       =   0  'Transparent
         Caption         =   "m."
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   2
         Left            =   1440
         TabIndex        =   34
         Top             =   510
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Y1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   1
         Left            =   -15
         TabIndex        =   33
         Top             =   855
         Width           =   420
      End
      Begin VB.Label lblDim 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "X1 :"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   0
         Left            =   0
         TabIndex        =   32
         Top             =   510
         Width           =   420
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   10980
      Top             =   270
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   150
      ImageHeight     =   130
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":151A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":180C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":1B70
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":1F39
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":229D
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":262F
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":29C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_BluePrint.frx":2CF8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvwTemplate 
      Height          =   2685
      Left            =   0
      TabIndex        =   0
      Top             =   15
      Width           =   3270
      _ExtentX        =   5768
      _ExtentY        =   4736
      Arrange         =   2
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      OLEDragMode     =   1
      PictureAlignment=   5
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   33023
      BackColor       =   0
      BorderStyle     =   1
      Appearance      =   1
      OLEDragMode     =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "Frm_Blueprint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
On Error GoTo errHandle
Dim theExtent$
Dim xSMin#, ySMin#, zSMin#, xSMax#, ySMax#, zSMax#, X#, y#

With Frm_Map.Sis1
    theExtent = .GetDisplayExtent
    .SplitExtent xSMin#, ySMin#, zSMin#, xSMax#, ySMax#, zSMax#, theExtent$
End With
X# = (xSMin# + xSMax#) / 2
y# = (ySMin# + ySMax#) / 2

Select Case lvwTemplate.SelectedItem.Index
Case 1
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX1(0)), y#, 0#
    .LineTo X# + CDbl(txtX1(0)), y# + CDbl(txtY1(0)), 0#
    .LineTo X#, y# + CDbl(txtY1(0)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With

Case 2
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX2(0)) + CDbl(txtX2(1)), y#, 0#
    .LineTo X# + CDbl(txtX2(0)) + CDbl(txtX2(1)), y# + CDbl(txtY2(1)), 0#
    .LineTo X# + CDbl(txtX2(0)), y# + CDbl(txtY2(1)), 0#
    .LineTo X# + CDbl(txtX2(0)), y# + CDbl(txtY2(0)) + CDbl(txtY2(1)), 0#
    .LineTo X#, y# + CDbl(txtY2(0)) + CDbl(txtY2(1)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 3
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX3(0)) + CDbl(txtX3(1)) + CDbl(txtX3(2)), y#, 0#
    .LineTo X# + CDbl(txtX3(0)) + CDbl(txtX3(1)) + CDbl(txtX3(2)), y# + CDbl(txtY3(0)) + CDbl(txtY3(1)), 0#
    .LineTo X# + CDbl(txtX3(0)) + CDbl(txtX3(1)), y# + CDbl(txtY3(0)) + CDbl(txtY3(1)), 0#
    .LineTo X# + CDbl(txtX3(0)) + CDbl(txtX3(1)), y# + CDbl(txtY3(1)), 0#
    .LineTo X# + CDbl(txtX3(0)), y# + CDbl(txtY3(1)), 0#
    .LineTo X# + CDbl(txtX3(0)), y# + CDbl(txtY3(0)) + CDbl(txtY3(1)), 0#
    .LineTo X#, y# + CDbl(txtY3(0)) + CDbl(txtY3(1)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 4
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX4(0)), y#, 0#
    .LineTo X# + CDbl(txtX4(0)), y# + CDbl(txtY4(1)), 0#
    .LineTo X#, y# + CDbl(txtY4(0)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 5
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX5(0)) + CDbl(txtX5(1)) + CDbl(txtX5(2)), y#, 0#
    .LineTo X# + CDbl(txtX5(0)) + CDbl(txtX5(1)) + CDbl(txtX5(2)), y# + CDbl(txtY5(1)), 0#
    .LineTo X# + CDbl(txtX5(0)) + CDbl(txtX5(1)), y# + CDbl(txtY5(1)), 0#
    .LineTo X# + CDbl(txtX5(0)) + CDbl(txtX5(1)), y# + CDbl(txtY5(0)) + CDbl(txtY5(1)), 0#
    .LineTo X# + CDbl(txtX5(0)), y# + CDbl(txtY5(0)) + CDbl(txtY5(1)), 0#
    .LineTo X# + CDbl(txtX5(0)), y# + CDbl(txtY5(1)), 0#
    .LineTo X#, y# + CDbl(txtY5(1)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 6
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX6(0)), y#, 0#
    .LineTo X# + CDbl(txtX6(0)), y# - CDbl(txtY6(2)), 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)), y# - CDbl(txtY6(2)), 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)), y#, 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)) + CDbl(txtX6(2)), y#, 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)) + CDbl(txtX6(2)), y# + CDbl(txtY6(1)), 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)), y# + CDbl(txtY6(1)), 0#
    .LineTo X# + CDbl(txtX6(0)) + CDbl(txtX6(1)), y# + CDbl(txtY6(0)) + CDbl(txtY6(1)), 0#
    .LineTo X# + CDbl(txtX6(0)), y# + CDbl(txtY6(0)) + CDbl(txtY6(1)), 0#
    .LineTo X# + CDbl(txtX6(0)), y# + CDbl(txtY6(1)), 0#
    .LineTo X#, y# + CDbl(txtY6(1)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 7
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX7(0)), y#, 0#
    .LineTo X# + CDbl(txtX7(0)), y# - CDbl(txtY7(1)), 0#
    .LineTo X# + CDbl(txtX7(0)) + CDbl(txtX7(1)) + CDbl(txtX7(2)), y# - CDbl(txtY7(1)), 0#
    .LineTo X# + CDbl(txtX7(0)) + CDbl(txtX7(1)) + CDbl(txtX7(2)), y#, 0#
    .LineTo X# + CDbl(txtX7(0)) + CDbl(txtX7(1)), y#, 0#
    .LineTo X# + CDbl(txtX7(0)) + CDbl(txtX7(1)), y# + CDbl(txtY7(0)), 0#
    .LineTo X#, y# + CDbl(txtY7(0)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
Case 8
With Frm_Map.Sis1
    .MoveTo X#, y#, 0#
    .LineTo X# + CDbl(txtX8(1)), y#, 0#
    .LineTo X# + CDbl(txtX8(1)), y# + CDbl(txtY8(1)), 0#
    .LineTo X# + CDbl(txtX8(0)), y# + CDbl(txtY8(0)), 0#
    .LineTo X#, y# + CDbl(txtY8(0)), 0#
    .LineTo X#, y#, 0#
    .UpdateItem
    .AddToList "Area"
    .CreateAreaFromLines "Area", True, SIS_AREA_ONE_TO_ONE
    .EmptyList "Area"
End With
End Select
Unload Me
Exit Sub

errHandle:
If Err.Number = 13 Then
    MsgBox "�ô�к��Ţ���١��ͧ", vbOKOnly + vbInformation, "����͹ !"
    SendKeys "{TAB}"
Else
    MsgBox Err.Number & "-" & Err.Description
End If
End Sub

Private Sub Command1_Click()
        Unload Me
End Sub

Private Sub Form_Load()
        With lvwTemplate
            .ListItems.Add , , "Type 1", 1, 1
            .ListItems.Add , , "Type 2", 2, 2
            .ListItems.Add , , "Type 3", 3, 3
            .ListItems.Add , , "Type 4", 4, 4
            .ListItems.Add , , "Type 5", 5, 5
            .ListItems.Add , , "Type 6", 6, 6
            .ListItems.Add , , "Type 7", 7, 7
            .ListItems.Add , , "Type 8", 8, 8
        End With
        Dim i As Byte
        For i = 0 To 7
               fraParameter(i).Left = 3285
               fraParameter(i).Top = 0
        Next i
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Frm_Blueprint = Nothing
End Sub

Private Sub lvwTemplate_ItemClick(ByVal Item As MSComctlLib.ListItem)
Dim ctl As Control
For Each ctl In Me.Controls
    If TypeOf ctl Is Frame Then
            ctl.Visible = False
    End If
Next
Select Case Item.Index
        Case 1
                fraParameter(0).Visible = True
        Case 2
                fraParameter(1).Visible = True
        Case 3
                fraParameter(2).Visible = True
        Case 4
                fraParameter(3).Visible = True
        Case 5
                fraParameter(4).Visible = True
        Case 6
                fraParameter(5).Visible = True
        Case 7
                fraParameter(6).Visible = True
        Case 8
                fraParameter(7).Visible = True
End Select
End Sub

Private Sub txtX1_Change(Index As Integer)
        If txtX1(Index).Text = Empty Then txtX1(Index).Text = "0"
End Sub

Private Sub txtX1_GotFocus(Index As Integer)

With txtX1(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim Character As String

Character = "0123456789."
KeyAscii = Asc(Chr(KeyAscii))
If KeyAscii > 26 Then
    If InStr(Character, Chr(KeyAscii)) = 0 Then
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtX2_GotFocus(Index As Integer)
With txtX2(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX2_KeyPress(Index As Integer, KeyAscii As Integer)
Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtX3_GotFocus(Index As Integer)
With txtX3(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX3_KeyPress(Index As Integer, KeyAscii As Integer)
Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtX4_GotFocus(Index As Integer)
With txtX4(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX4_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtX5_GotFocus(Index As Integer)
With txtX5(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX5_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtX6_GotFocus(Index As Integer)
        With txtX6(Index)
            .SelStart = 0
            .SelLength = Len(.Text)
        End With
End Sub

Private Sub txtX6_KeyPress(Index As Integer, KeyAscii As Integer)
Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtX7_GotFocus(Index As Integer)
With txtX7(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtX7_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY1_GotFocus(Index As Integer)
With txtY1(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY1_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY2_GotFocus(Index As Integer)
With txtY2(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY2_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY3_GotFocus(Index As Integer)
With txtY3(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY3_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY4_GotFocus(Index As Integer)
With txtY4(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY4_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY5_GotFocus(Index As Integer)
With txtY5(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY5_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY6_GotFocus(Index As Integer)
With txtY6(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY6_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

Private Sub txtY7_GotFocus(Index As Integer)
With txtY7(Index)
    .SelStart = 0
    .SelLength = Len(.Text)
End With
End Sub

Private Sub txtY7_KeyPress(Index As Integer, KeyAscii As Integer)
        Call txtX1_KeyPress(Index, KeyAscii)
End Sub

