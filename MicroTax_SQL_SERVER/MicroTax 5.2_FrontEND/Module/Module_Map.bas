Attribute VB_Name = "DMod_Map"
' Global Constants In VB  For Pass ByValue To Libraly  Cadcorp Component  To Know

' Horizontal Text alignment.
Global Const SIS_LEFT = 0
Global Const SIS_RIGHT = 2
Global Const SIS_CENTRE = 6

' Vertical Text alignment.
Global Const SIS_TOP = 0
Global Const SIS_BOTTOM = 8
Global Const SIS_BASELINE = 24
Global Const SIS_MIDDLE = 72

' Horizontal and Vertical Text alignment.
Global Const SIS_TOP_LEFT = 0
Global Const SIS_TOP_RIGHT = 2
Global Const SIS_TOP_CENTRE = 6
Global Const SIS_BOTTOM_LEFT = 8
Global Const SIS_BOTTOM_RIGHT = 10
Global Const SIS_BOTTOM_CENTRE = 14
Global Const SIS_BASE_LEFT = 24
Global Const SIS_BASE_RIGHT = 26
Global Const SIS_BASE_CENTRE = 30
Global Const SIS_MIDDLE_LEFT = 72
Global Const SIS_MIDDLE_RIGHT = 74
Global Const SIS_MIDDLE_CENTRE = 78

' Menu commands.
Global Const SIS_COM_ALL = 0
Global Const SIS_COM_ADD = 1
Global Const SIS_COM_REMOVE = 2
Global Const SIS_COM_NONE = 3

' Overlay status.
Global Const SIS_INVISIBLE = 0
Global Const SIS_VISIBLE = 1
Global Const SIS_HITTABLE = 2
Global Const SIS_EDITABLE = 3

' Redraw.
Global Const SIS_CURRENTWINDOW = 0
Global Const SIS_CURRENTSWD = 1
Global Const SIS_ALLWINDOWS = 2
Global Const SIS_QUICK_REDRAW = 32

' Attribute filters.
Global Const SIS_FILTERRESET = 0
Global Const SIS_FILTERADD = 1
Global Const SIS_FILTERREMOVE = 2

' Argument entry.
Global Const SIS_ARG_ESCAPE = 0
Global Const SIS_ARG_ENTER = 1
Global Const SIS_ARG_BACKSPACE = 2
Global Const SIS_ARG_POSITION = 3

' Save & Exit.
Global Const SIS_NOSAVE = 0
Global Const SIS_SAVE = 1
Global Const SIS_PROMPTSAVE = 2

' Save Bitmap formats.
Global Const SIS_SAVEBMP_BMP = 0
Global Const SIS_SAVEBMP_JPG = 1
Global Const SIS_SAVEBMP_DITHERBMP = 2
Global Const SIS_SAVEBMP_PNG = 3
Global Const SIS_SAVEBMP_TIFF = 4
Global Const SIS_SAVEBMP_TIFF_PACKBITS = 5
Global Const SIS_SAVEBMP_TIFF_GP4 = 6

' Class filters.
Global Const SIS_CLASSEXCLUDE = 0
Global Const SIS_CLASSINCLUDE = 1
Global Const SIS_OPENBRANCH = 2

' Units.
Global Const SIS_LENGTHDIM = 1
Global Const SIS_AREADIM = 2
Global Const SIS_VOLUMEDIM = 3

' Boolean operations.
Global Const SIS_BOOLEAN_AND = 1
Global Const SIS_BOOLEAN_OR = 2
Global Const SIS_BOOLEAN_XOR = 3
Global Const SIS_BOOLEAN_DIFF = 4
Global Const SIS_REVERSE = 64
Global Const SIS_FORCE_POSITIVE = 65

' Deprecated scanning tests.
Global Const SIS_TEST_NOCHANGE = -1
Global Const SIS_TEST_DISABLE = 0
Global Const SIS_TEST_INSIDE = 1
Global Const SIS_TEST_CROSSING = 2
Global Const SIS_TEST_CONTAINS = 3

' Geometry tests.
Global Const SIS_GT_EQUAL = 0
Global Const SIS_GT_DISJOINT = 1
Global Const SIS_GT_INTERSECT = 2
Global Const SIS_GT_TOUCH = 3
Global Const SIS_GT_CROSS = 4
Global Const SIS_GT_CROSSBY = 5
Global Const SIS_GT_WITHIN = 6
Global Const SIS_GT_CONTAIN = 7
Global Const SIS_GT_OVERLAP = 8

' Geometry testing modes.
Global Const SIS_GM_ORIGIN = 0
Global Const SIS_GM_EXTENTS = 1
Global Const SIS_GM_GEOMETRY = 2

' Property object types.
Global Const SIS_OT_CURITEM = 0
Global Const SIS_OT_DEFITEM = 1
Global Const SIS_OT_SYSTEM = 2
Global Const SIS_OT_WINDOW = 4
Global Const SIS_OT_OVERLAY = 5
Global Const SIS_OT_DATASET = 6
Global Const SIS_OT_OPTION = 7
Global Const SIS_OT_NOL = 8
Global Const SIS_OT_PRINTER = 9
Global Const SIS_OT_FTABLE = 10
Global Const SIS_OT_SCHEMA = 11
Global Const SIS_OT_SCHEMACOLUMN = 12
Global Const SIS_OT_THEME = 13
Global Const SIS_OT_THEMECOMPONENT = 14

' Cadcorp SIS Control Licence Levels.
Global Const SIS_LEVEL_UNLICENSED = 0
Global Const SIS_LEVEL_MANAGER = 1
Global Const SIS_LEVEL_MODELLER = 2
Global Const SIS_LEVEL_VIEWER = 3

' General constants.
Global Const SIS_OVERRIDE = 1
Global Const SIS_WRITEABLE = 0
Global Const SIS_READONLY = -1

' Rubber sheet methods.
Global Const SIS_RUBBER_BEST_FIT = 0
Global Const SIS_RUBBER_LINEAR_PATCH = 1
Global Const SIS_RUBBER_INVERSE_SQUARE = 2

' 3D manipulation modes.
Global Const SIS_3DMODE_CRUISE = 0
Global Const SIS_3DMODE_EYE = 1
Global Const SIS_3DMODE_MODEL = 2
Global Const SIS_3DMODE_PAN = 3
Global Const SIS_3DMODE_ZOOM = 4

' Line segment shapes.
Global Const SIS_LINE_STRAIGHT = 0
Global Const SIS_LINE_BULGE = 1
Global Const SIS_LINE_BEZIER = 2

' Feature filters.
Global Const SIS_FEATUREEXCLUDE = 0
Global Const SIS_FEATUREINCLUDE = 1
Global Const SIS_FEATURECASCADE = 2

' Blob formats.
Global Const SIS_BLOB_SIS = 0
Global Const SIS_BLOB_OGIS_WKB = 1
Global Const SIS_BLOB_OGIS_WKT = 2

' Creating Area items.
Global Const SIS_AREA_ONE_TO_ONE = 0
Global Const SIS_AREA_MANY_TO_ONE = 1
Global Const SIS_AREA_DISJOINT = 2

' Rubber-banding.
Global Const SIS_RUBBERBAND_NONE = 0
Global Const SIS_RUBBERBAND_LINE = 1
Global Const SIS_RUBBERBAND_CIRCLE = 2
Global Const SIS_RUBBERBAND_RECT = 3
Global Const SIS_RUBBERBAND_ITEMS = 4
Global Const SIS_RUBBERBAND_FIX_CIRCLE = 5
Global Const SIS_RUBBERBAND_FIX_RECT = 6

' Scatter Grid creation modes.
Global Const SIS_SCATTER_GRID_INTERPOLATE = 0
Global Const SIS_SCATTER_GRID_CLOSEST = 1
Global Const SIS_SCATTER_GRID_SUM = 2
Global Const SIS_SCATTER_GRID_COUNT = 3

' Cleaning Line items.
Global Const SIS_CLEAN_LINE_NONE = 0
Global Const SIS_CLEAN_LINE_REMOVE_0 = 1
Global Const SIS_CLEAN_LINE_REMOVE_180 = 2
Global Const SIS_CLEAN_LINE_REMOVE_SELF = 4

' Cleaning Topological items.
Global Const SIS_CLEAN_TOPO_NONE = 0
Global Const SIS_CLEAN_TOPO_REMOVE_DANGLING = 1
Global Const SIS_CLEAN_TOPO_FIX_UNDER_OVER = 2
Global Const SIS_CLEAN_TOPO_REMOVE_SEEDS = 4

' Formula calculations.
Global Const SIS_CALCULATE_COUNT = 0
Global Const SIS_CALCULATE_SUM = 1
Global Const SIS_CALCULATE_AVERAGE = 2

' Axes types.
Global Const SIS_AXES_CARTESIAN = 0
Global Const SIS_AXES_SPHERICAL = 1

' Co-ordinate units.

' Type:
Global Const SIS_UNIT_ANGLE = 0
Global Const SIS_UNIT_LINEAR = 1
Global Const SIS_UNIT_AREA = 2
Global Const SIS_UNIT_VOLUME = 3

' Angle:
Global Const SIS_UNITA_DEGREES = 0
Global Const SIS_UNITA_RADIANS = 1
Global Const SIS_UNITA_DMS = 2
Global Const SIS_UNITA_GRADIANS = 3

' Linear:
Global Const SIS_UNIT1_M = 0
Global Const SIS_UNIT1_MM = 1
Global Const SIS_UNIT1_CM = 2
Global Const SIS_UNIT1_KM = 3
Global Const SIS_UNIT1_FEET = 4
Global Const SIS_UNIT1_INCHES = 5
Global Const SIS_UNIT1_IMPERIAL = 6
Global Const SIS_UNIT1_YARD = 7
Global Const SIS_UNIT1_FATHOM = 8
Global Const SIS_UNIT1_MILE = 9
Global Const SIS_UNIT1_NAUTMILE = 10

' Area:
Global Const SIS_UNIT2_M = 0
Global Const SIS_UNIT2_MM = 1
Global Const SIS_UNIT2_CM = 2
Global Const SIS_UNIT2_KM = 3
Global Const SIS_UNIT2_FEET = 4
Global Const SIS_UNIT2_INCHES = 5
Global Const SIS_UNIT2_YARDS = 6
Global Const SIS_UNIT2_ACRE = 7
Global Const SIS_UNIT2_HECTARE = 8
Global Const SIS_UNIT2_TUBO = 9
Global Const SIS_UNIT2_MILE = 10
Global Const SIS_UNIT2_NAUTMILE = 11

' Volume:
Global Const SIS_UNIT3_M = 0
Global Const SIS_UNIT3_MM = 1
Global Const SIS_UNIT3_CM = 2
Global Const SIS_UNIT3_LITRE = 3
Global Const SIS_UNIT3_FEET = 4
Global Const SIS_UNIT3_INCHES = 5
Global Const SIS_UNIT3_YARDS = 6
Global Const SIS_UNIT3_GALLON_IMP = 7
Global Const SIS_UNIT3_GALLON_US = 8

' Theme scaling functions:
Global Const SIS_FUNCTION_CONSTANT = 0
Global Const SIS_FUNCTION_SQUAREROOT = 1
Global Const SIS_FUNCTION_LINEAR = 2
Global Const SIS_FUNCTION_LOG10 = 3

' Printer colour capabilities:
Global Const SIS_PRINTCAPS_QUERY = 0
Global Const SIS_PRINTCAPS_MONO = 1
Global Const SIS_PRINTCAPS_COLOUR = 2

' Theme annotation alignment:
Global Const SIS_ALIGN_BOTTOM_LEFT = 0
Global Const SIS_ALIGN_BOTTOM_CENTRE = 1
Global Const SIS_ALIGN_BOTTOM_RIGHT = 2
Global Const SIS_ALIGN_MIDDLE_LEFT = 3
Global Const SIS_ALIGN_MIDDLE_CENTRE = 4
Global Const SIS_ALIGN_MIDDLE_RIGHT = 5
Global Const SIS_ALIGN_TOP_LEFT = 6
Global Const SIS_ALIGN_TOP_CENTRE = 7
Global Const SIS_ALIGN_TOP_RIGHT = 8

' NOL types:
Global Const SIS_NOL_FILE = 0
Global Const SIS_NOL_STANDARD = 16
Global Const SIS_NOL_TEMPORARY = 32
Global Const SIS_NOL_WORKSPACE = 64

' Graticule span types:
Global Const SIS_GRATICULE_NONE = 0
Global Const SIS_GRATICULE_GRID = 1
Global Const SIS_GRATICULE_CROSSHAIR = 2
Global Const SIS_GRATICULE_CROSSHAIR_TEXT = 3

' Deprecated constants:
Global Const SIS_ERROR_NO_CURRENT_LAYER = 5
Global Const SIS_LEVEL_MAPPER = 1
Global Const SIS_LEVEL_EDITOR = 2

' Label Theme placement options:
Global Const SIS_LABEL_START = 0
Global Const SIS_LABEL_MIDDLE = 1
Global Const SIS_LABEL_END = 2
Global Const SIS_LABEL_ALONG = 3

' Index Dataset flags.
Global Const SIS_INDEX_OUTLINES = 1
Global Const SIS_INDEX_LABELS = 2
Global Const SIS_INDEX_PYRAMID = 4

' Error codes.
Global Const SIS_ERROR_OK = 0
Global Const SIS_ERROR_SYNTAX = 1
Global Const SIS_ERROR_ITEM_NOT_FOUND = 2
Global Const SIS_ERROR_ITEM_GEOM_NOT_OWNED = 3
Global Const SIS_ERROR_NO_ITEM_OPEN = 4
Global Const SIS_ERROR_NO_CURRENT_DATASET = 5
Global Const SIS_ERROR_NO_CURRENT_WINDOW = 6
Global Const SIS_ERROR_NO_POSITION = 7
Global Const SIS_ERROR_INVALID_HWND = 8
Global Const SIS_ERROR_UNSOLICITED = 9
Global Const SIS_ERROR_NO_GROUP_OPEN = 10
Global Const SIS_ERROR_BAD_INDEX = 11
Global Const SIS_ERROR_INVALID_ANGLE = 12
Global Const SIS_ERROR_BAD_ITEMSCAN_STATUS = 13
Global Const SIS_ERROR_NO_ITEMSCAN_RUNNING = 14
Global Const SIS_ERROR_FILTER_NOT_FOUND = 15
Global Const SIS_ERROR_CLASS_NOT_FOUND = 16
Global Const SIS_ERROR_INVALID_PROPERTY = 17
Global Const SIS_ERROR_USER_CANCEL = 18
Global Const SIS_ERROR_FAILED = 19
Global Const SIS_ERROR_INVALID_DATASET = 20
Global Const SIS_ERROR_INVALID_OVERLAY = 21
Global Const SIS_ERROR_UNDEFINED_OVERRIDE = 22
Global Const SIS_ERROR_INVALID_NAME = 23
Global Const SIS_ERROR_INVALID_STATUS = 24
Global Const SIS_ERROR_FILE_ACCESS = 25
Global Const SIS_ERROR_BAD_FILTER_TYPE = 26
Global Const SIS_ERROR_INVALID_POSITION = 27
Global Const SIS_ERROR_INVALID_EXTENT = 28
Global Const SIS_ERROR_INVALID_PARAM = 29
Global Const SIS_ERROR_INVALID_LIST = 30
Global Const SIS_ERROR_COMMAND_FAILED = 31
Global Const SIS_ERROR_CORRUPT_MESSAGE = 32
Global Const SIS_ERROR_LOCUS_NOT_FOUND = 33
Global Const SIS_ERROR_BUSY = 34
Global Const SIS_ERROR_LIBRARY = 35
Global Const SIS_ERROR_DATASET_MODIFIED = 36
Global Const SIS_ERROR_BAD_DATASET_TYPE = 37
Global Const SIS_ERROR_ITEM_READONLY = 38
Global Const SIS_ERROR_GROUP_ALREADY_PLACED = 39
Global Const SIS_ERROR_GROUP_EMPTY = 40
Global Const SIS_ERROR_NO_EDITABLE_ITEMS = 41
Global Const SIS_ERROR_NO_SELECTION = 42
Global Const SIS_ERROR_LIST_NOT_FOUND = 43
Global Const SIS_ERROR_BAD_ITEM_TYPE = 44
Global Const SIS_ERROR_EXTREME_PARAMETER = 45
Global Const SIS_ERROR_NOL_NOT_FOUND = 46
Global Const SIS_ERROR_EMPTY_RESULT = 47
Global Const SIS_ERROR_ITEM_NOT_IN_SWD = 48
Global Const SIS_ERROR_LIST_EMPTY = 49
Global Const SIS_ERROR_INSUFFICIENT_ITEM_ACCESS = 50
Global Const SIS_ERROR_PARTIAL_SUCCESS = 51
Global Const SIS_ERROR_OUT_OF_MEMORY = 52
Global Const SIS_ERROR_INCONSISTENT_PARAMS = 53
Global Const SIS_ERROR_REMOVE_DEFAULT_NOL = 54
Global Const SIS_ERROR_NO_COMPOSED_WINDOW = 55
Global Const SIS_ERROR_NOL_NOT_SUITABLE = 56
Global Const SIS_ERROR_METHOD_NOT_IN_LEVEL = 57
Global Const SIS_ERROR_COMMAND_NOT_IN_LEVEL = 58
Global Const SIS_ERROR_DIFFERENT_DATASETS = 59
Global Const SIS_ERROR_NOT_DATA_ENABLED = 60
Global Const SIS_ERROR_TEMPLATE_NO_DATASETS = 61
Global Const SIS_ERROR_TEMPLATE_NO_PHOTO = 62
Global Const SIS_ERROR_TEMPLATE_PHOTO_NOT_EMPTY = 63
Global Const SIS_ERROR_BAD_RESAMPLE_METHOD = 64
Global Const SIS_ERROR_INVALID_FORMULA = 65
Global Const SIS_ERROR_SCHEMA_NOT_FOUND = 66
Global Const SIS_ERROR_THEME_NOT_FOUND = 67
Global Const SIS_ERROR_SEED_NOT_FOUND = 68
Global Const SIS_ERROR_NAMER_NOT_FOUND = 69
Global Const SIS_ERROR_COMMAND_NOT_FOUND = 70
Global Const SIS_ERROR_PROJECTION_NOT_FOUND = 71
Global Const SIS_ERROR_DATUM_NOT_FOUND = 72
Global Const SIS_ERROR_FTABLE_NOT_FOUND = 73
Global Const SIS_ERROR_PTEMP_NOT_FOUND = 74
Global Const SIS_ERROR_TABLE_NOT_FOUND = 75
Global Const SIS_ERROR_NO_FTABLE_LOADED = 76
Global Const SIS_ERROR_NOL_MODIFIED = 77
Global Const SIS_ERROR_BRUSH_NOT_FOUND = 78
Global Const SIS_ERROR_COLOURSET_NOT_FOUND = 79
Global Const SIS_ERROR_PEN_NOT_FOUND = 80
Global Const SIS_ERROR_NO_SCHEMA_LOADED = 81
Global Const SIS_ERROR_VIEW_NOT_FOUND = 82
Global Const SIS_ERROR_NO_THEME_LOADED = 83
Global Const SIS_ERROR_NO_LONGER_SUPPORTED = 84

