Attribute VB_Name = "Dev1DMod_Libraly"
'------------------------------------------------------------------------------------------------------------------------------------------------------------------
'////////////////////////////////////////////////////////////////////////////////////////////////////////////
' The Dev1DMod_Libraly As Info Research Developer Team1 Data Module Funtion and Procedure Of Libraly
'///////////////////////////////////////////////////////////////////////////////////////////////////////////
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------
Private Enum Tflag
              Numberic
              Character
              NON
End Enum
Private Type NUMS
           n As Byte
       Flag As Tflag
End Type

Dim num() As NUMS
Dim strRes() As String
Dim str1()      As String
Dim numTemp()   As NUMS
Dim num_length As Integer

Public Globle_Connective As ADODB.Connection
Public Globle_ExecuteQuery As ADODB.Command
Public Clone_Form As FrmMain
Public Num_Time As Byte
Public Assets_Type As String
Public Chk_Ownership As Byte  '1=Frm_InformPayTax; ,2,3,4,5=Frm_Print_Warn; 0=default
Public str_OwnershipID As String
Public str_OwnershipID2 As String
Public str_OwnershipID3 As String
Public strUser As String

Private Sub Main()
'***********The Application Start Form Must Use Procedure Sub Main Because Form Not  First Load in Memory****************
        If App.PrevInstance Then End
        Frm_Logo.Show
        Frm_Logo.Refresh
  Call SetPathFile    'Read ini File
'
  Call SET_CONNECTION
  Call Get_Table
'  Call Get_Query
  
 Set Clone_Form = New FrmMain   'YouFirstForm
               Load Clone_Form
'               Load Frm_Index
               Load Frm_Ownership
'               Load Frm_PrintSummary
'               Load Frm_SetPrint
               Chk_Ownership = 0
            If (TProduct = "DEMO" And TCampaign = "TGIS") Or (TProduct = "VIEWER") Or (TProduct = "MANAGER") Or (TProduct = "MODELLER") Then
                   Load Frm_Map
            End If
            Unload Frm_Logo
'            With GBQueryDuePayTax
'                      .Requery
'                      .MoveFirst
'                   Do While Not .EOF
'                         If .Fields("Type_Rate").Value = 0 Then
'                                        Frm_Index.Txt_Due_Accept0.Text = .Fields("Due_Accept").Value
'                                        Frm_Index.Txt_Due_1Month.Text = .Fields("Due_1Month").Value
'                                        Frm_Index.Txt_Due_2Month.Text = .Fields("Due_2Month").Value
'                                        Frm_Index.Txt_Due_3Month.Text = .Fields("Due_3Month").Value
'                                        Frm_Index.Txt_Due_4Month.Text = .Fields("Due_4Month").Value
'                                        Frm_Index.Txt_Due_Due_Appeal0.Text = .Fields("Due_Appeal").Value
'                                        Frm_Index.Txt_Tax_YPerM.Text = .Fields("Tax_YPerM").Value
'                                        Frm_Index.Tax_MPerD.Text = .Fields("Tax_MPerD").Value
'                                        Frm_Index.Txt_Tax_Machine.Text = .Fields("Tax_Machine").Value
'                         End If
'                          If .Fields("Type_Rate").Value = 1 Then
'                                        Frm_Index.Txt_Due_Accept1.Text = .Fields("Due_Accept").Value
'                                        Frm_Index.Txt_Tax_OutSchedule1.Text = .Fields("Tax_OutSchedule").Value
'                                        Frm_Index.Txt_Tax_NotFully.Text = .Fields("Tax_NotFully").Value
'                                        Frm_Index.Txt_Tax_OutTime1.Text = .Fields("Tax_OutTime").Value
'                                        Frm_Index.Txt_Due_Due_Appeal1.Text = .Fields("Due_Appeal").Value
'                                        Frm_Index.Txt_B_Rai.Text = .Fields("Subside_Rai").Value
'                                        Frm_Index.Txt_B_Pot.Text = .Fields("Subside_Ngan").Value
'                                        Frm_Index.Txt_B_Va.Text = .Fields("Subside_Va").Value
'                         End If
'                         If .Fields("Type_Rate").Value = 2 Then
'                                        Frm_Index.Txt_Due_Accept2.Text = .Fields("Due_Accept").Value
'                                        Frm_Index.Txt_Tax_OutSchedule2.Text = .Fields("Tax_OutSchedule").Value
'                                        Frm_Index.Txt_Tax_NotFully2.Text = .Fields("Tax_NotFully").Value
'                                        Frm_Index.Txt_Tax_OutTime2.Text = .Fields("Tax_OutTime").Value
'                                        Frm_Index.Txt_Due_Due_Appeal2.Text = .Fields("Due_Appeal").Value
'                         End If
'                            .MoveNext
'                   Loop
'            End With
            Clone_Form.Show
            Frm_Login.Show vbModal
End Sub

Public Sub SET_CONNECTION()
 On Error GoTo Err_Handler
         Acc_Path = True
 Set Globle_Connective = New ADODB.Connection
  With Globle_Connective
          If .STATE = adStateOpen Then .Close
                .ConnectionString = "Provider=SQLNCLI;Server=" & ini_Servername & ";Database=" & ini_Database & ";UID=" & ini_Username & ";PWD=" & ini_Password & ";"
                .ConnectionTimeout = 20
                .CursorLocation = adUseClient
                .Open
  End With
  Set Globle_ExecuteQuery = New ADODB.Command
  Exit Sub
Err_Handler:
      Acc_Path = False
      MsgBox "�������ö�Դ��Ͱҹ������ MicroTax ���ô��駤�� SQL Server ���١��ͧ���� !", vbCritical, "����͹�к�"
      Frm_Config.SSTab1.Tab = 3
      Frm_Config.Show vbModal
      '***********OpenConnection Of Database ****************
End Sub

Public Sub SET_Execute(sql_txt As String)
            With Globle_ExecuteQuery
                     .ActiveConnection = Globle_Connective
                     .CommandTimeout = 180
                     .CommandType = adCmdText
                     .CommandText = sql_txt
                     .Execute
             End With
End Sub

Public Sub SET_Execute2(sql_txt As String, cmdType As Integer, flag_prepare As Boolean) 'adCmdText =1,adCmdStoredProc = 4,adCmdUnknown = 8,adCmdUnspecified = -1
            With Globle_ExecuteQuery
                     .ActiveConnection = Globle_Connective
                     .CommandTimeout = 120
                     .CommandType = cmdType
                     .CommandText = sql_txt
                     .Prepared = flag_prepare
                     .Execute
             End With
End Sub

Public Function Command_ExecuteToRowReturn(sql_txt As String, cmdType As Integer, flag_prepare As Boolean) As ADODB.Recordset 'adCmdText =1,adCmdStoredProc = 4,adCmdUnknown = 8,adCmdUnspecified = -1
            With Globle_ExecuteQuery
                     .ActiveConnection = Globle_Connective
                     .CommandTimeout = 120
                     .CommandType = cmdType
                     .CommandText = sql_txt
                     .Prepared = flag_prepare
                     .Execute
             End With
End Function

Public Sub SET_QUERY(sql_txt As String, Query As ADODB.Recordset)
    With Query
        If .STATE = adStateOpen Then .Close
                              .ActiveConnection = Globle_Connective
                               .CursorType = adOpenForwardOnly
                               .CursorLocation = adUseClient ' .CursorLocation = adUseServer
                               .LockType = adLockOptimistic '.LockType = adLockBatchOptimistic
                              .Open sql_txt
    End With
End Sub

Public Function ReleaseObject(X As Single, y As Single, obj_picturebox As PictureBox) As Boolean
  With obj_picturebox
         If (X < 0) Or (y < 0) Or (X > .Width) Or (y > .Height) Then
             ReleaseCapture
             ReleaseObject = True
         Else
             SetCapture .hWnd
             ReleaseObject = False
         End If
  End With
'***********Example  EventMouseMove****************
                 ' If ReleaseObject(X, Y, Picture) Then
                 '      Picture.Picture =    LoadResPicture(101, 0)
                 'Else
                 '    Picture21.Picture = LoadResPicture(102, 0)
                 ' End If
  End Function

Public Sub DETERMINE_FORM(Frm_MidiName As String, Use_State As Boolean)
Dim CloneToFrm As Form
       For Each CloneToFrm In Forms
              If CloneToFrm.Name <> Frm_MidiName Then
                          If Use_State Then
                             Unload CloneToFrm
                             Set CloneToFrm = Nothing
                          Else
                              Select Case CloneToFrm.Name
                                           Case "Frm_AcceptTax", "Frm_Payment", "Frm_PayTax", "Frm_PreformTax", "Frm_ChangeOwner"
                                                       Unload CloneToFrm
                                                       Set CloneToFrm = Nothing
                                           Case Else
                                                      CloneToFrm.Hide
                              End Select
                         End If
              End If
        Next
        '***********Example  EventOnLoadChildForm****************
                           'DETERMINE_FORM( MASTER , TRUE)
End Sub

Public Sub KILL_DOT(TXTBOX As TextBox)
Dim Logic As Boolean
Dim str, CTR  As String
Dim i As Byte
      
 Logic = False
 For i = 1 To Len(TXTBOX.Text)
   CTR = Mid(TXTBOX.Text, i, 1)
       If CTR = "." Then
            If Logic = False Then
               Logic = True
            Else
                CTR = vbNullString
            End If
       End If
     str = str + CTR
 Next i
    TXTBOX.Text = str
    TXTBOX.SelStart = Len(TXTBOX.Text)
        '***********Example  EventOnChange****************
                             'KILL_DOT (Text1)
  End Sub
  
Public Sub KILL_SINGLEQOUTE(TXTBOX As TextBox)
Dim Buffer_Txt As String, Txt_Compare As String
Dim i As Byte

If LenB(Trim$(TXTBOX.Text)) <> 0 Then
         For i = 1 To Len(TXTBOX.Text)
                Txt_Compare = Mid(TXTBOX.Text, i, 1)
                If Txt_Compare <> "'" Then
                    Buffer_Txt = Buffer_Txt + Txt_Compare
                Else
                   Buffer_Txt = Buffer_Txt + ""
                End If
          Next i
                        TXTBOX.Text = Buffer_Txt
                        TXTBOX.SelStart = Len(TXTBOX.Text)
End If
        '***********Example  EventOnChange****************
                             'KILL_SINGLEQOUTE(Text1)
End Sub

Function KeyCharecter(NAscii As Integer, ScriptCharacter As String) As Byte
       If NAscii > 26 Or NAscii = 8 Or NAscii = 13 Then
            If InStr(ScriptCharacter, Chr(NAscii)) = 0 Then
               If NAscii = 8 Then
                   KeyCharecter = 8
                   Exit Function
               End If
               If NAscii = 13 Then
                   KeyCharecter = 13
                   Exit Function
               End If
                KeyCharecter = 0
           Else
                 KeyCharecter = NAscii
          End If
        End If
 '***********Example  EventOnKeypress****************
                    'KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Function

' BEGIN SCOPE OF RUNAUTONUMBER '###############################################################################################
    Private Function RevertString(str As String) As String                                                                                                                                                                                                 '##
   Dim i As Integer, lent       As Integer, max    As Integer                                                                                                                                                                                                 '##
     lent = Len(str)                                                                                                                                                                                                                                                                     '##
     max = lent - 1                                                                                                                                                                                                                                                                      '##
    ReDim strRes(lent)                                                                                                                                                                                                                                                              '##
     For i = 0 To max                                                                                                                                                                                                                                                                  '##
         strRes(lent - i) = str1(i + 1)                                                                                                                                                                                                                                             '##
         bufstr = bufstr & strRes(lent - i)                                                                                                                                                                                                                                     '##
   Next i                                                                                                                                                                                                                                                                                        '##
        RevertString = StrReverse(bufstr)                                                                                                                                                                                                                                  '##
End Function
'##
Private Function myConvert2Str() As String
Dim i As Integer, lent As Integer                                                                                                                                                                                                                                             '##
Dim str2 As String                                                                                                                                                                                                                                                                     '##
       ReDim str1(num_length)                                                                                                                                                                                                                                                  '##
       For i = 1 To num_length                                                                                                                                                                                                                                                    '##
             str1(i) = Chr(num(i - 1).n)                                                                                                                                                                                                                                            '##
             str2 = str2 & str1(i)                                                                                                                                                                                                                                                         '##
       Next i                                                                                                                                                                                                                                                                                      '##
         myConvert2Str = RevertString(str2)                                                                                                                                                                                                                               '##
End Function                                                                                                                                                                                                                                                                               '##

Private Sub myConvert(str As String, Flag As Integer)
Dim i As Integer, lent As Integer, str1  As String                                                                                                                                                                                                                   '##
       lent = Len(str):          num_length = lent                                                                                                                                                                                                                           '##
       ReDim num(lent)                                                                                                                                                                                                                                                                  '##
       For i = 0 To lent - 1                                                                                                                                                                                                                                                               '##
                 num(i).n = Asc(Mid(str, (lent - i), 1))                                                                                                                                                                                                                        '##
             If (num(i).n >= 65) And (num(i).n <= 90) Then                                                                                                                                                                                                         '##
                      If Flag = 1 Then                                                                                                                                                                                                                                                       '##
                                num(i).Flag = Character                                                                                                                                                                                                                              '##
                      ElseIf Flag = 2 Then                                                                                                                                                                                                                                                '##
                                num(i).Flag = NON                                                                                                                                                                                                                                        '##
                       End If                                                                                                                                                                                                                                                                        '##
             ElseIf (num(i).n >= 48) And (num(i).n <= 57) Then                                                                                                                                                                                                  '##
                       num(i).Flag = Number                                                                                                                                                                                                                                           '##
             Else                                                                                                                                                                                                                                                                                     '##
                       num(i).Flag = NON                                                                                                                                                                                                                                                 '##
        End If                                                                                                                                                                                                                                                                                       '##
    Next i                                                                                                                                                                                                                                                                                            '##
End Sub                                                                                                                                                                                                                                                                                          '##

Private Function incAuto(i As Integer, lent As Integer) As String
Dim iFlag As Byte, j As Integer                                                                                                                                                                                                                                                    '##
      iFlag = 1                                                                                                                                                                                                                                                                                    '##
       If (num(i).Flag = Number) Or (num(i).Flag = Character) Then                                                                                                                                                                                       '##
               num(i).n = num(i).n + 1                                                                                                                                                                                                                                                 '##
                        If num(i).Flag = Character Then                                                                                                                                                                                                                          '##
                               If num(i).n > Asc("Z") Then                                                                                                                                                                                                                            '##
                                     num(i).n = Asc("A")                                                                                                                                                                                                                                    '##
                                     Call incAuto(i + 1, lent)                                                                                                                                                                                                                             '##
                               End If                                                                                                                                                                                                                                                                  '##
                        End If                                                                                                                                                                                                                                                                         '##
        End If                                                                                                                                                                                                                                                                                         '##
     If num(i).Flag = Number Then                                                                                                                                                                                                                                                  '##
             If num(i).n > Asc("9") Then                                                                                                                                                                                                                                                '##
                   If iFlag = 1 Then                                                                                                                                                                                                                                                            '##
                       num(i).n = Asc("0")                                                                                                                                                                                                                                                   '##
                       Call incAuto(i + 1, lent)                                                                                                                                                                                                                                            '##
                   End If                                                                                                                                                                                                                                                                               '##
           End If                                                                                                                                                                                                                                                                                       '##
     End If                                                                                                                                                                                                                                                                                             '##
     If num(i).Flag = NON Then                                                                                                                                                                                                                                                         '##
                 Call incAuto(i + 1, lent)                                                                                                                                                                                                                                                  '##
      End If                                                                                                                                                                                                                                                                                            '##
             incAuto = myConvert2Str                                                                                                                                                                                                                                                   '##
End Function                                                                                                                                                                                                                                                                                     '##

Public Function RunAutoNumber(TableName As String, FieldName As String, PatternField As String, AddPrefix As Boolean, Optional Condition As String) As String
Dim QueryRunAutoNumber As ADODB.Recordset                                                                                                                                                                                                           '##
Dim Buffer_UnderRun As String
                              Set QueryRunAutoNumber = New ADODB.Recordset
'                              Debug.Print "exec sp_search_runnumber " & FieldName & "," & TableName & ",""" & Condition & """"
                              Set QueryRunAutoNumber = Globle_Connective.Execute("exec sp_search_runnumber " & FieldName & "," & TableName & ",""" & Condition & """", , adCmdUnknown)
                              With QueryRunAutoNumber                                                                                                                                                                                                                               '##
'                                        If .STATE = adStateOpen Then .Close                                                                                                                                                                                                   '##
'                                            .ActiveConnection = Globle_Connective                                                                                                                                                                                          '##
'                                            .CursorType = adOpenForwardOnly                                                                                                                                                                                                  '##
'                                            .CursorLocation = adUseClient
'                                            .LockType = adLockReadOnly
'                                            .Open "SELECT  " & FieldName & " From " & TableName & " WITH(TABLOCK) WHERE " & FieldName & " Is Not Null " & Condition & " ORDER BY " & FieldName & " ASC "                                                                                          '##
                                          If .RecordCount = 0 Then                                                                                                                                                                                                                         '##
                                                        RunAutoNumber = PatternField                                                                                                                                                                                            '##
                                          Else                                                                                                                                                                                                                                                              '##
                                                    .MoveLast
                                                    If Len(.Fields(FieldName).Value) = Len(PatternField) Then
                                                           If AddPrefix Then
                                                                Buffer_UnderRun = .Fields(FieldName).Value & "/" & "00"
                                                           Else
                                                                Buffer_UnderRun = .Fields(FieldName).Value
                                                           End If
                                                    Else
                                                            Buffer_UnderRun = .Fields(FieldName).Value
                                                    End If
                                                    Call myConvert(Buffer_UnderRun, 1)                                                                                                                                                                              '##
                                                    RunAutoNumber = (incAuto(0, 4 - 1))                                                                                                                                                                                   '##
                                          End If                                                                                                                                                                                                                                                            '##
                            End With                                                                                                                                                                                                                                                                    '##
          Set QueryRunAutoNumber = Nothing                                                                                                                                                                                                                                   '##                                                                                                                                                                                                                         '*
          Erase num                                                                                                                                                                                                                                                                                    '##
          Erase strRes                                                                                                                                                                                                                                                                                 '##
          Erase str1                                                                                                                                                                                                                                                                                      '##
          Erase numTemp                                                                                                                                                                                                                                                                         '##

          '***********Example To Use ****************
           ' TEXT1.TEXT =  RunAutoNumber("IN_Manufacture_Order", "INMAN_No", "0000000000001")
End Function                                                                                                                                                                                                                                                                                          '##
' END SCOPE OF RUNAUTONUMBER '########################################################################################################

Public Function AutoFind(ByRef cboCurrent As ComboBox, ByVal KeyAscii As Integer, Optional ByVal LimitToList As Boolean = False)
Dim lCB As Long
Dim sFindString As String

On Error GoTo Err_Handler
    If KeyAscii = 8 Then
        If cboCurrent.SelStart <= 1 Then
            cboCurrent = vbNullString
            AutoFind = 0
            Exit Function
        End If
        If cboCurrent.SelLength = 0 Then
             sFindString = (Left(cboCurrent, Len(cboCurrent) - 1))
        Else
            sFindString = Left$(cboCurrent.Text, cboCurrent.SelStart - 1)
        End If
    Else
        If cboCurrent.SelLength = 0 Then
            sFindString = (cboCurrent.Text & Chr$(KeyAscii))
        Else
            sFindString = Left$(cboCurrent.Text, cboCurrent.SelStart) & Chr$(KeyAscii)
        End If
    End If
            lCB = SendMessage(cboCurrent.hWnd, CB_FINDSTRING, -1, ByVal sFindString)

    If lCB <> CB_ERR Then
            cboCurrent.ListIndex = lCB
            cboCurrent.SelStart = Len(sFindString)
            cboCurrent.SelLength = Len(cboCurrent.Text) - cboCurrent.SelStart
            AutoFind = 0
    Else
        If LimitToList = True Then
            AutoFind = 0
        Else
            AutoFind = KeyAscii
        End If
    End If
Err_Handler:
End Function

Function GetPath(PathString As String, STATE As String) As String
    On Error Resume Next
Dim i As Byte
Dim File_Name As String
If UCase(STATE) = "GETPATH" Then
                   For i = 0 To Len(PathString)
                                If Mid(PathString, Len(PathString) - i, 1) = "\" Then
                                   Exit For
                                End If
                  Next i
                      GetPath = Mid(PathString, 1, Len(PathString) - i)
End If
If UCase(STATE) = "GETFILE" Then
                                    File_Name = Empty
                For i = 0 To Len(PathString)
                                If Mid(PathString, Len(PathString) - i, 1) <> "\" Then
                                           File_Name = File_Name & Mid(PathString, Len(PathString) - i, 1)
                                Else
                                          Exit For
                                End If
                 Next i
                                      GetPath = StrReverse(File_Name)
End If
End Function


