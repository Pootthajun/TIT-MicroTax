VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Payment_Lot 
   Caption         =   "�觪����繧Ǵ"
   ClientHeight    =   8655
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12735
   ControlBox      =   0   'False
   Icon            =   "Frm_Payment_Lot.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   Picture         =   "Frm_Payment_Lot.frx":151A
   ScaleHeight     =   8655
   ScaleWidth      =   12735
   WindowState     =   2  'Maximized
   Begin VB.TextBox Txt_PBT_NO_ACCEPT 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   300
      Left            =   5250
      MaxLength       =   12
      TabIndex        =   19
      Top             =   2430
      Width           =   2505
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   7770
      Picture         =   "Frm_Payment_Lot.frx":18EEC4
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   2430
      Width           =   555
   End
   Begin VB.OptionButton Op_PBT5_NO 
      BackColor       =   &H00808080&
      Caption         =   "�Ţ������Ẻ"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   5250
      TabIndex        =   17
      Top             =   2130
      Width           =   1455
   End
   Begin VB.OptionButton Op_PBT5_NO_ACCEPT 
      BackColor       =   &H00808080&
      Caption         =   "�Ţ����駻����Թ"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   6720
      TabIndex        =   16
      Top             =   2130
      Width           =   1605
   End
   Begin VB.Frame Frame1 
      Height          =   345
      Left            =   9300
      TabIndex        =   10
      Top             =   2400
      Width           =   3285
      Begin VB.OptionButton Option1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�������շ�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Left            =   120
         TabIndex        =   12
         Top             =   60
         Width           =   1785
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "��˹��ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Left            =   1950
         TabIndex        =   11
         Top             =   60
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   345
         Index           =   1
         Left            =   0
         Top             =   0
         Width           =   3285
      End
   End
   Begin VB.TextBox Text3 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   480
      Left            =   7560
      TabIndex        =   4
      Text            =   "0.00"
      Top             =   7200
      Width           =   1905
   End
   Begin VB.TextBox Text2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   480
      Left            =   7560
      TabIndex        =   3
      Text            =   "0.00"
      Top             =   6720
      Width           =   1905
   End
   Begin VB.TextBox Text1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   480
      Left            =   7560
      TabIndex        =   2
      Text            =   "0.00"
      Top             =   6240
      Width           =   1905
   End
   Begin VB.TextBox Txt_TotalMoney 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   480
      Left            =   10650
      TabIndex        =   0
      Text            =   "0.00"
      Top             =   7680
      Width           =   1905
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   2925
      Left            =   120
      TabIndex        =   9
      Top             =   2760
      Width           =   12465
      _ExtentX        =   21987
      _ExtentY        =   5159
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   9
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   9
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   7680
      TabIndex        =   13
      Top             =   1620
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196622
      OrigLeft        =   7635
      OrigTop         =   1620
      OrigRight       =   7890
      OrigBottom      =   1950
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      Height          =   645
      Index           =   0
      Left            =   5220
      Top             =   2100
      Width           =   3135
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2548"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6630
      TabIndex        =   15
      Top             =   1620
      Width           =   1050
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻվط��ѡ�Ҫ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   4890
      TabIndex        =   14
      Top             =   1680
      Width           =   1680
   End
   Begin VB.Label Lb_Name_Now 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   345
      Left            =   120
      TabIndex        =   8
      Top             =   5700
      Width           =   12465
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ǵ��� 1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   8820
      TabIndex        =   7
      Top             =   5190
      Width           =   645
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ǵ��� 2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   6810
      TabIndex        =   6
      Top             =   6900
      Width           =   645
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ǵ��� 3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   6810
      TabIndex        =   5
      Top             =   7380
      Width           =   645
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Ť������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   16
      Left            =   9660
      TabIndex        =   1
      Top             =   7800
      Width           =   885
   End
End
Attribute VB_Name = "Frm_Payment_Lot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset
Dim QueryCfgTax As ADODB.Recordset
Dim Rs_Check As ADODB.Recordset
Dim Summary  As Currency
Dim Txt_Formula As String

Private Sub Command1_Click()
On Error GoTo ErrSearch
 Dim sql_txt As String
            Lb_MoneyDue.Caption = "0.00"
            Lb_MoneyDue_Perform.Caption = "0.00"
                    Select Case LB_Menu.Tag
                            Case "1"
                                        Status_InToFrm = "PBT11_INSTALMENT"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������պ��ا��ͧ���"
                            Case "2"
                                        Status_InToFrm = "PRD12_INSTALMENT"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������ç���͹��з��Թ"
                            Case "3"
                                        Status_InToFrm = "PP7_INSTALMENT"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������ջ���"
'                            Case "4"
'                                        Status_InToFrm = "PBA1"
'                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������͹حҵ��Сͺ��ä��"
                            Case "5"
                                        Status_InToFrm = "PBT11_OTHER"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������պ��ا��ͧ���(�������)"
                            Case "6"
                                        Status_InToFrm = "PRD12_OTHER"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է�����������ç���͹��з��Թ(�������)"
                            Case "7"
                                        Status_InToFrm = "PP7_OTHER"
                                        Frm_SerachForProcess.Caption = " �鹢����� - ������Է���������ջ���(�������)"
                   End Select
                   Frm_SerachForProcess.Show vbModal
 Set Query = Globle_Connective.Execute("exec sp_search_payment '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "','" & Trim$(Txt_Ownership_ID.Text) & "','" & _
            CInt(Lb_Year.Caption) & "','" & CInt(Op_PBT5_NO_ACCEPT.Value) & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
' Dim ConditionFilter As String
'    If LenB(Trim$(Txt_PBT_NO_ACCEPT.Text)) <> 0 Then
'        Select Case LB_Menu.Tag
'                     Case "1", "5"
'                             If Op_PBT5_NO_ACCEPT.Value Then
'                               ConditionFilter = " AND PBT5_NO_ACCEPT = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PBT5_NO_ACCEPT "
'                             Else
'                               ConditionFilter = " AND PBT5_NO = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PBT5_NO "
'                             End If
'                     Case "2", "6"
'                             If Op_PBT5_NO_ACCEPT.Value Then
'                               ConditionFilter = " AND PRD2_NO_ACCEPT = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PRD2_NO_ACCEPT "
'                             Else
'                               ConditionFilter = " AND PRD2_NO = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PRD2_NO "
'                             End If
'                     Case "3", "7"
'                             If Op_PBT5_NO_ACCEPT.Value Then
'                               ConditionFilter = " AND PP1_NO_ACCEPT = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PP1_NO_ACCEPT "
'                             Else
'                               ConditionFilter = " AND PP1_NO = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'" & " ORDER BY A.PP1_NO "
'                             End If
'                     Case "4"
'                             If Op_PBT5_NO_ACCEPT.Value Then
'                                ConditionFilter = " AND LICENSE_BOOK = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'"
'                            Else
'                               ConditionFilter = " AND LICENSE_NO = '" & Trim$(Txt_PBT_NO_ACCEPT.Text) & "'"
'                            End If
'        End Select
'    Else
'            Select Case LB_Menu.Tag
'                     Case "1", "5"
'                               ConditionFilter = " AND B.OWNERSHIP_ID = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & " ORDER BY A.PBT5_DATE "
'                     Case "2", "6"
'                               ConditionFilter = " AND  B.OWNERSHIP_ID  = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & " ORDER BY A.PRD2_DATE "
'                     Case "3", "7"
'                               ConditionFilter = " AND  B.OWNERSHIP_ID  = '" & Trim$(Txt_OwnerShip_ID.Text) & "'" & " ORDER BY A.PP1_DATE "
'                     Case "4"
'                               ConditionFilter = " AND  B.OWNERSHIP_ID  = '" & Trim$(Txt_OwnerShip_ID.Text) & "'"
'            End Select
'    End If
'
'        Select Case LB_Menu.Tag
'                     Case "1", "5"
'                            sql_txt = " SELECT PBT5_STATUS,Format(PBT5_DATE_RETURN,'dd/mm/yyyy') As DateReturn, A.LAND_ID, LAND_NOTIC, LAND_NUMBER, '�-' & E.SOI_NAME & ' �.' & F.STREET_NAME & ' �.' & D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME, Format(A.PBT5_AMOUNT_ACCEPT,'###,###,###0.00##'), B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName ,  A.OWNERSHIP_ID,PBT5_NO,PBT5_NO_ACCEPT,PBT5_DATE AS PERFORM_DATE,'' AS PAYMENT"
'                            sql_txt = sql_txt & " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN (LANDDATA C RIGHT JOIN (OWNERSHIP B RIGHT JOIN PBT5 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) ON C.LAND_ID = A.LAND_ID) ON E.SOI_ID = C.SOI_ID) ON F.STREET_ID = C.STREET_ID) ON D.TAMBON_ID = C.TAMBON_ID" & _
'                                                              " WHERE PBT5_STATUS = 0 AND  PBT5_NO_STATUS = 1 AND PBT5_YEAR =  " & CInt(Lb_Year.Caption)
'                     Case "2", "6"
'                                        sql_txt = " SELECT PRD2_STATUS,Format(A.PRD2_DATE_RETURN,'dd/mm/yyyy')  As DateReturn, A.BUILDING_ID , BUILDING_HOME_NO,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME ,Format(A.PRD2_AMOUNT_ACCEPT,'###,###,###0.00##') , B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName,H.LAND_ID,PRD2_NO,PRD2_NO_ACCEPT,PRD2_DATE AS PERFORM_DATE,'' AS PAYMENT"
'                                        sql_txt = sql_txt & " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PRD2 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN (LANDDATA H RIGHT JOIN BUILDINGDATA C ON H.LAND_ID = C.LAND_ID) ON A.BUILDING_ID = C.BUILDING_ID) ON E.SOI_ID = H.SOI_ID) ON F.STREET_ID = H.STREET_ID) ON D.TAMBON_ID = H.TAMBON_ID"
'                                        sql_txt = sql_txt & " WHERE PRD2_STATUS = 0 AND PRD2_NO_STATUS = 1  And PRD2_YEAR =  " & CInt(Lb_Year.Caption)
'                     Case "3", "7"
'                                        sql_txt = " SELECT PP1_STATUS,Format(A.PP1_DATE_RETURN,'dd/mm/yyyy') As DateReturn , A.SIGNBORD_ID , SIGNBORD_ROUND,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME , Format(A.PP1_AMOUNT_ACCEPT,'###,###,###0.00##') , B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName,H.LAND_ID,PP1_NO,PP1_NO_ACCEPT,PP1_DATE AS PERFORM_DATE,'' AS PAYMENT"
'                                        sql_txt = sql_txt & " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN (LANDDATA H RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PP1 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN SIGNBORDDATA C ON A.SIGNBORD_ID = C.SIGNBORD_ID) ON H.LAND_ID = C.LAND_ID) ON E.SOI_ID = H.SOI_ID) ON F.STREET_ID = H.STREET_ID) ON D.TAMBON_ID = H.TAMBON_ID"
'                                        sql_txt = sql_txt & " WHERE PP1_STATUS = 0 AND PP1_NO_STATUS = 1  AND PP1_YEAR =  " & CInt(Lb_Year.Caption)
'                     Case "4"
'                                        sql_txt = "SELECT PBA1_STATUS,PBA1_DATE AS PERFORM_DATE,Format(A.PBA1_DATE_RETURN,'dd/mm/yyyy') As DateReturn , A.LICENSE_BOOK ,A.LICENSE_NO"
'                                        sql_txt = sql_txt & ", (SELECT  '�-' &  E2.SOI_NAME &  ' �.'  &  F2.STREET_NAME & ' �.' &  D2.TAMBON_NAME &  ' �.' & G2.AMPHOE_NAME "
'                                        sql_txt = sql_txt & " FROM LANDDATA A2 , LICENSEDATA B2  , TAMBON D2 , SOI E2 , STREET F2 , AMPHOE G2 ,PBA1 H2"
'                                        sql_txt = sql_txt & " Where B2.LAND_ID = A2.LAND_ID And A2.TAMBON_ID = D2.TAMBON_ID And E2.SOI_ID = A2.SOI_ID And F2.STREET_ID = A2.STREET_ID "
'                                        sql_txt = sql_txt & " AND G2.AMPHOE_ID = D2.AMPHOE_ID AND H2.LICENSE_BOOK = B2.LICENSE_BOOK  AND H2.LICENSE_NO = B2.LICENSE_NO "
'                                        sql_txt = sql_txt & " AND STATUS_LINK =1 AND C.LICENSE_BOOK = B2.LICENSE_BOOK AND C.LICENSE_NO = B2.LICENSE_NO) "
'                                        sql_txt = sql_txt & ", (SELECT  '�-' &  E2.SOI_NAME &  ' �.'  &  F2.STREET_NAME & ' �.' &  D2.TAMBON_NAME &  ' �.' & G2.AMPHOE_NAME "
'                                        sql_txt = sql_txt & "FROM LANDDATA A2 , LICENSEDATA B2 ,BUILDINGDATA C2 , TAMBON D2 , SOI E2 , STREET F2 , AMPHOE G2 ,PBA1 H2 "
'                                        sql_txt = sql_txt & " Where B2.Building_id = C2.Building_id And C2.LAND_ID = A2.LAND_ID And A2.TAMBON_ID = D2.TAMBON_ID And E2.SOI_ID = A2.SOI_ID And F2.STREET_ID = A2.STREET_ID "
'                                        sql_txt = sql_txt & " AND H2.LICENSE_BOOK = B2.LICENSE_BOOK  AND H2.LICENSE_NO = B2.LICENSE_NO "
'                                        sql_txt = sql_txt & " AND G2.AMPHOE_ID = D2.AMPHOE_ID AND STATUS_LINK =2 AND C.LICENSE_BOOK = B2.LICENSE_BOOK AND C.LICENSE_NO = B2.LICENSE_NO) "
'                                        sql_txt = sql_txt & " ,'',PBA1_AMOUNT,STATUS_LINK"
'                                        sql_txt = sql_txt & " ,B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName ,PBA1_NO,PBA1_NO_ACCEPT ,'' AS PAYMENT "
'                                        sql_txt = sql_txt & " FROM PBA1 A,  OWNERSHIP B, LICENSEDATA C "
'                                        sql_txt = sql_txt & " Where  C.LICENSE_BOOK = A.LICENSE_BOOK  AND C.LICENSE_NO = A.LICENSE_NO AND PBA1_STATUS = 0 " 'AND PBA1_NO_STATUS =1
'                                        sql_txt = sql_txt & " AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBA1_YEAR =  " & CInt(Lb_Year.Caption)
'          End Select
'          sql_txt = sql_txt & ConditionFilter
'  Call SET_QUERY(sql_txt, Query)
If Query.STATE = adStateOpen Then
    If Query.RecordCount > 0 Then
        Set Grid_Result.DataSource = Query
                                  If Query.Fields("DateReturn").Value = Empty Or IsNull(Query.Fields("DateReturn").Value) Then
                                         MaskEdBox1.Text = "__/__/____"
                                  Else
                                         MaskEdBox1.Text = Format$(Query.Fields("DateReturn").Value, "dd/mm/yyyy")
                                  End If
                                  If IsNull(Query.Fields("PERFORM_DATE").Value) Or Query.Fields("PERFORM_DATE").Value = Empty Then
                                         MaskEdBox3.Text = "__/__/____"
                                  Else
                                         MaskEdBox3.Text = Format$(Query.Fields("PERFORM_DATE").Value, "dd/mm/yyyy")
                                  End If
                                 Lb_Count.Caption = Query.RecordCount
                                    
                                Dim i As Integer
                                For i = 1 To Grid_Result.Rows - 1
                                        If Query.Fields("DateReturn").Value = Empty Or IsNull(Query.Fields("DateReturn").Value) Then
                                               Grid_Result.TextMatrix(i, 1) = vbNullString
                                        Else
                                               Grid_Result.TextMatrix(i, 1) = Format$(Query.Fields("DateReturn").Value, "dd/mm/yyyy")
                                        End If
                                        If Grid_Result.TextMatrix(i, 0) <> "0" Then
                                                Grid_Result.TextMatrix(i, 0) = "����"
                                        Else
                                                Grid_Result.TextMatrix(i, 0) = "�ѧ������"
                                        End If
                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                        End If
                                        If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                        End If
                                        If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 4)
                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                        End If
                                        If LB_Menu.Tag = "4" Then
                                                Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 2)
                                        End If
                                        Select Case LB_Menu.Tag
                                                Case "1", "5"
                                                                Grid_Result.TextMatrix(i, 6) = Format$(Grid_Result.TextMatrix(i, 6), "#,##0.00")
                                                Case "2", "6", "3", "7"
                                                                Grid_Result.TextMatrix(i, 5) = Format$(Grid_Result.TextMatrix(i, 5), "#,##0.00")
                                        End Select
                                        
                                        
                                Next i
'                                Call CalOfPay
                                Lb_Name_Now.Caption = Query.Fields("StillName").Value
'                                 If Lb_PBT_NO.Caption = Empty Then
'                                                    '���ҧ�������
'                                                    Select Case LB_Menu.Tag
'                                                                 Case "1", "5"
'                                                                        sql_txt = " SELECT PBT5_NO From PBT5  Where  PBT5_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBT5_NO IS NOT NULL AND PBT5_NO <> '') Group by  PBT5_NO Order by  PBT5_NO "
'                                                                 Case "2", "6"
'                                                                        sql_txt = " SELECT PRD2_NO From PRD2  Where  PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND (PRD2_NO IS NOT NULL AND PRD2_NO <> '') Group by  PRD2_NO Order by  PRD2_NO "
'                                                                 Case "3", "7"
'                                                                        sql_txt = " SELECT PP1_NO From PP1  Where  PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND (PP1_NO IS NOT NULL AND PP1_NO <> '') Group by  PP1_NO Order by  PP1_NO "
''                                                                 Case "4"
''                                                                        sql_txt = " SELECT PBA1_NO From PBA1  Where  PBA1_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBA1_NO IS NOT NULL AND PBA1_NO <> '') Group by  PBA1_NO Order by  PBA1_NO "
'                                                     End Select
'                                                    Call SET_QUERY(sql_txt, Query)
'                                                    If Query.RecordCount > 0 Then
'                                                        Query.MoveLast
'                                                        Select Case LB_Menu.Tag
'                                                                        Case "1", "5"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PBT5_NO").Value, 4)) + 1, "###000#")
'                                                                        Case "2", "6"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PRD2_NO").Value, 4)) + 1, "###000#")
'                                                                        Case "3", "7"
'                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PP1_NO").Value, 4)) + 1, "###000#")
''                                                                         Case "4"
''                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Query.Fields("PBA1_NO").Value, 4)) + 1, "###000#")
'                                                        End Select
'                                                                                Lb_PBT_NO.Caption = Lb_PBT_NO.Caption & "/" & Right$(Year(Now), 2)
'                                                    Else
'                                                                                Lb_PBT_NO.Caption = "0001/" & Right$(Year(Now), 2)
'                                                    End If
'                                 End If
'                                 If LB_Menu.Tag = "4" Then Lb_PBT_NO.Caption = Empty
                                 
                                Chk_RoundMoney.Value = Checked
'                                Call Set_Grid
  Else
'            Call Clear_Grid
  End If
  End If
           Option2.Value = True
            Exit Sub
ErrSearch:
            MsgBox Err.Description
End Sub

Private Sub Form_Load()
        Set Query = New ADODB.Recordset
        Set QueryCfgTax = New ADODB.Recordset
        Set Rs_Check = New ADODB.Recordset
        Lb_Year.Caption = Right$(Date, 4)
'        MaskEdBox2.Text = "__/__/____"
End Sub
