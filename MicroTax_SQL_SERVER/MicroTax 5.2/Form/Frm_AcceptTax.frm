VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_AcceptTax 
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12300
   ControlBox      =   0   'False
   Icon            =   "Frm_AcceptTax.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_AcceptTax.frx":151A
   ScaleHeight     =   9000
   ScaleWidth      =   12300
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkAppeal_No 
      Enabled         =   0   'False
      Height          =   195
      Left            =   2370
      MaskColor       =   &H00000000&
      Picture         =   "Frm_AcceptTax.frx":FA96
      TabIndex        =   30
      Top             =   2550
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.CheckBox chkAppeal_Ok 
      Enabled         =   0   'False
      Height          =   195
      Left            =   1230
      MaskColor       =   &H00000000&
      Picture         =   "Frm_AcceptTax.frx":19D440
      TabIndex        =   29
      Top             =   2550
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.CheckBox chkAppeal 
      Height          =   195
      Left            =   1230
      MaskColor       =   &H00000000&
      Picture         =   "Frm_AcceptTax.frx":32ADEA
      TabIndex        =   26
      Top             =   1710
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.CommandButton cmd_Search_No_Accept 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   6210
      Picture         =   "Frm_AcceptTax.frx":4B8794
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   7710
      Width           =   555
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   7591
      TabIndex        =   23
      Top             =   1650
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196625
      OrigLeft        =   7860
      OrigTop         =   1650
      OrigRight       =   8115
      OrigBottom      =   1995
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      Wrap            =   -1  'True
      BuddyProperty   =   65537
      Enabled         =   -1  'True
   End
   Begin MSMask.MaskEdBox MaskEdBox1 
      Height          =   345
      Left            =   8460
      TabIndex        =   22
      Top             =   7710
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   609
      _Version        =   393216
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Mask            =   "99/99/9999"
      PromptChar      =   "_"
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004040&
      Height          =   360
      Left            =   4530
      MaxLength       =   7
      TabIndex        =   20
      Top             =   7710
      Width           =   1665
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�Ѵ�ӡ���Ѻ�駻����Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   5790
      Picture         =   "Frm_AcceptTax.frx":4B8D1E
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   8400
      Width           =   2355
   End
   Begin VB.TextBox Txt_PBT_NO 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   360
      Left            =   6570
      MaxLength       =   7
      TabIndex        =   5
      Top             =   2250
      Width           =   1275
   End
   Begin VB.CommandButton Btn_SearchOwnerShip_PBT5 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   7890
      Picture         =   "Frm_AcceptTax.frx":4B92A8
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2280
      Width           =   555
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   360
      Left            =   2640
      MaxLength       =   7
      TabIndex        =   3
      Top             =   5940
      Width           =   1665
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00E0E0E0&
      Height          =   555
      Left            =   6960
      Picture         =   "Frm_AcceptTax.frx":4B9832
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6510
      Width           =   2145
   End
   Begin VB.CommandButton Btn_Post_PBT5 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�Ѵ����¡�� �.�.�.9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   4620
      Picture         =   "Frm_AcceptTax.frx":4B9F1C
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6510
      Width           =   2355
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result_PBT9 
      Height          =   3045
      Left            =   1050
      TabIndex        =   6
      Top             =   2850
      Width           =   11085
      _ExtentX        =   19553
      _ExtentY        =   5371
      _Version        =   393216
      BackColor       =   14737632
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   9884603
      ForeColorSel    =   4210752
      BackColorBkg    =   12632256
      GridColor       =   8421504
      GridColorFixed  =   4210752
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSMask.MaskEdBox MaskEdBox2 
      Height          =   345
      Left            =   1800
      TabIndex        =   28
      Top             =   2100
      Visible         =   0   'False
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   609
      _Version        =   393216
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Mask            =   "99/99/9999"
      PromptChar      =   "_"
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ��� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   12
      Left            =   1230
      TabIndex        =   33
      Top             =   2160
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�׹������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   11
      Left            =   2700
      TabIndex        =   32
      Top             =   2520
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Ŵ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   10
      Left            =   1560
      TabIndex        =   31
      Top             =   2520
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�������ط��� "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   5
      Left            =   1515
      TabIndex        =   27
      Top             =   1650
      Visible         =   0   'False
      Width           =   1080
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   825
      Index           =   0
      Left            =   1050
      Top             =   2010
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label Lb_OwnerShip_ID 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   255
      Left            =   8400
      TabIndex        =   25
      Top             =   900
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ����Ѻ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   9
      Left            =   7320
      TabIndex        =   21
      Top             =   7770
      Width           =   1020
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ѻ��駻����Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   8
      Left            =   2820
      TabIndex        =   18
      Top             =   7800
      Width           =   1500
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�Ѻ�Ţ����駻����Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Index           =   6
      Left            =   1020
      TabIndex        =   17
      Top             =   7170
      Width           =   11115
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0FFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   1155
      Left            =   1020
      Top             =   7170
      Width           =   11115
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   7
      Left            =   11460
      TabIndex        =   16
      Top             =   2580
      Width           =   510
   End
   Begin VB.Label LB_Sum 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Left            =   10350
      TabIndex        =   15
      Top             =   2580
      Width           =   1005
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   4
      Left            =   9720
      TabIndex        =   14
      Top             =   2580
      Width           =   510
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2548"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6570
      TabIndex        =   13
      Top             =   1650
      Width           =   1020
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻվط��ѡ�Ҫ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   4830
      TabIndex        =   12
      Top             =   1710
      Width           =   1680
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ������Ẻ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   5400
      TabIndex        =   11
      Top             =   2340
      Width           =   1110
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ����駻����Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   1110
      TabIndex        =   10
      Top             =   6030
      Width           =   1485
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥһ����Թ :                                �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   8700
      TabIndex        =   9
      Top             =   6030
      Width           =   3420
   End
   Begin VB.Label Lb_money 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000000&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   345
      Left            =   9960
      TabIndex        =   8
      Top             =   5940
      Width           =   1665
   End
   Begin VB.Label Lb_name 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Left            =   4410
      TabIndex        =   7
      Top             =   6030
      Width           =   1080
   End
   Begin VB.Label LB_Menu 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���պ��ا��ͧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6150
      TabIndex        =   0
      Top             =   960
      Width           =   1245
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Index           =   1
      Left            =   9540
      Top             =   2490
      Width           =   2595
   End
End
Attribute VB_Name = "Frm_AcceptTax"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset
Dim Query2 As ADODB.Recordset
Dim strTemp As String
Dim strTemp2 As String

Private Sub ShowReport(File_Name As String, Formula As String, Optional BEGIN_DATE As String, Optional ENDDATE As String)
Call FindPrinterDeFault
With Clone_Form.CTReport
        .Reset
        .Connect = "DSN = Microtax;UID = " & ini_Username & ";PWD = " & ini_Password & ";DSQ = Administration"
        .DiscardSavedData = True
        .SelectionFormula = Formula
        .PrinterDriver = TruePrinter.DriverName
        .PrinterPort = TruePrinter.Port
        .PrinterName = TruePrinter.DeviceName
        Select Case LB_Menu.Tag
                Case "1"
                          
                Case "2"
                          .Formulas(0) = "Tambon ='" & iniTambon & "'"
                Case "3"
                         
        End Select
         .ReportFileName = App.Path & "\Report\" & File_Name
         .Destination = crptToPrinter
         .Action = 1
End With
End Sub

Private Sub Clear_Grid()
On Error Resume Next
Dim i As Byte
               Grid_Result_PBT9.Rows = 2
               For i = 0 To Grid_Result_PBT9.Cols - 1
                       Grid_Result_PBT9.TextMatrix(1, i) = vbNullString
               Next i
                   Txt_PBT_NO.Text = vbNullString
                   Text1.Text = vbNullString
                   Text2.Text = vbNullString
                   Lb_money.Caption = "0.00"
                   Lb_Name.Caption = "���ͼ���������� :"
End Sub

Private Sub Set_Grid()
            With Grid_Result_PBT9
                    .FormatString = "�Ţ������Ẻ|���ʷ��Թ||�Ҥ�����|�ѹ������Ẻ|ʶҹз�Ѿ���Թ"
                    .ColWidth(0) = 1500: .ColAlignmentFixed(0) = 4
                    .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4
                    Select Case LB_Menu.Tag
                            Case "1", "5"
                                    .TextArray(2) = "�Ţ�͡����Է���"
                            Case "2", "6"
                                    .TextArray(2) = "��ҹ�Ţ���"
                            Case "3", "7"
                                    .TextArray(2) = "��ҹ�Ţ���"
                    End Select
                    .ColWidth(2) = 1500: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                    .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4: .ColAlignment(3) = 7
                    .ColWidth(4) = 2000: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                    .ColWidth(5) = 2200
                    .ColWidth(6) = 0
            End With
'            chkAppeal.Value = 0
End Sub

Private Sub Btn_Post_PBT5_Click()
'        Dim Sqlstring As String
        Dim Txt_Formula As String
        On Error GoTo ErrPost
        
        Globle_Connective.BeginTrans
        If Grid_Result_PBT9.TextMatrix(1, 0) = Empty Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If MsgBox("�Ѵ����¡�� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If Trim$(Text1.Text) = Empty Then
                MsgBox "�ô����Ţ����Ѻ�駻����Թ !", vbExclamation, "����͹"
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
'        If IsDate(MaskEdBox2.Text) = False And chkAppeal.Value = Checked Then
'                    MaskEdBox2.Text = "__/__/____"
'                    MsgBox "�ѹ��͹�����١��ͧ ��س�����ѹ��͹�����١��ͧ����", vbCritical, "����͹"
'                    MaskEdBox2.SetFocus
'                    Globle_Connective.RollbackTrans
'                    Exit Sub
'        End If
        Set Query = Globle_Connective.Execute("exec sp_check_no_accept '" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
'        Select Case LB_Menu.Tag   '�礡�ë�Ӣͧ�Ţ����駻����Թ
'                     Case "1", "5"
'                                  Sqlstring = "SELECT  PBT5_NO_ACCEPT FROM PBT5 WHERE PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3 & " AND PBT5_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'                     Case "2", "6"
'                                  Sqlstring = "SELECT  PRD2_NO_ACCEPT FROM PRD2 WHERE  PRD2_YEAR = " & Lb_Year.Caption & " AND PRD2_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'                     Case "3", "7"
'                                  Sqlstring = "SELECT  PP1_NO_ACCEPT FROM PP1 WHERE  PP1_YEAR = " & Lb_Year.Caption & " AND PP1_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
''                     Case "4"
''                                  Sqlstring = "SELECT  PBA1_NO_ACCEPT FROM PBA1 WHERE PBA1_YEAR = " & Lb_Year.Caption & " AND PBA1_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'        End Select
'        Call SET_QUERY(Sqlstring, Query)
        If Query.RecordCount > 0 Then   '�礡�ë�Ӣͧ�Ţ����駻����Թ
                MsgBox "�Ţ����駻����Թ " & Trim$(Text1.Text) & " ���  �ô�к��Ţ����駻����Թ����", vbExclamation, "����͹ !"
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
'        Globle_Connective.Execute "exec sp_update_accept '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "','" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
'                        CvDate2(Date) & "','" & LB_Menu.Tag & "'", , adCmdUnknown
        Call SET_Execute2("exec sp_update_accept '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "','" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
                        CvDate2(Date) & "','" & LB_Menu.Tag & "'", 8, False)
'         Select Case LB_Menu.Tag
'                      Case "1", "5"
'                             Sqlstring = "UPDATE PBT5 SET PBT5_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PBT5_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PBT5_YEAR = " & CInt(Lb_Year.Caption) & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'                             SET_Execute (Sqlstring)
'                             Sqlstring = "UPDATE PBT5 SET PBT5_NO_ACCEPT = '" & Trim$(Text1.Text) & "'" & _
'                                                  " Where PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3 & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'                      Case "2", "6"
'                             Sqlstring = "UPDATE PRD2 SET PRD2_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PRD2_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PRD2_YEAR = " & CInt(Lb_Year.Caption) & " And PRD2_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'                      Case "3", "7"
'                             Sqlstring = "UPDATE PP1 SET PP1_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PP1_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PP1_YEAR = " & CInt(Lb_Year.Caption) & " And PP1_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
''                      Case "4"
''                             Sqlstring = "UPDATE PBA1 SET PBA1_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PBA1_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
''                                                  " Where PBA1_YEAR = " & CInt(Lb_Year.Caption) & " And PBA1_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'         End Select
'                    SET_Execute (Sqlstring)
'                    For i = 1 To Grid_Result_PBT9.Rows - 1
'                            Globle_Connective.Execute "exec sp_update_accept_tax " & Grid_Result_PBT9.TextMatrix(i, 3) & "," & CInt(Lb_Year.Caption) & ",'" & Grid_Result_PBT9.TextMatrix(i, 0) & "','" & _
'                                        Grid_Result_PBT9.TextMatrix(i, IIf(LB_Menu.Tag = "1" Or LB_Menu.Tag = "5", 1, 6)) & "','" & LB_Menu.Tag & "'," & chkAppeal.Value & "," & chkAppeal_Ok.Value & ",'" & CvDate3(MaskEdBox2.Text) & "'", , adCmdUnknown
'                            Select Case LB_Menu.Tag    '  ��Ѻ��ا����
'                                    Case "1", "5"
'                                            Sqlstring = "UPDATE PBT5 SET PBT5_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PBT5_YEAR =" & CInt(Lb_Year.Caption) & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And LAND_ID='" & Grid_Result_PBT9.TextMatrix(i, 1) & "'"
'                                    Case "2", "6"
'                                            Sqlstring = "UPDATE PRD2 SET PRD2_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PRD2_YEAR =" & CInt(Lb_Year.Caption) & " And PRD2_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And BUILDING_ID='" & Grid_Result_PBT9.TextMatrix(i, 6) & "'"
'                                    Case "3", "7"
'                                            Sqlstring = "UPDATE PP1 SET PP1_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PP1_YEAR =" & CInt(Lb_Year.Caption) & " And PP1_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And SIGNBORD_ID='" & Grid_Result_PBT9.TextMatrix(i, 6) & "'"
'                            End Select
'                            SET_Execute (Sqlstring)
'                    Next i
                    Call Clear_Grid
'                    Call SEARCH_PBT9
                    Call Set_Grid
                    Lb_OwnerShip_ID.Caption = Empty
                    Globle_Connective.CommitTrans
        On Error GoTo ErrPost2
        If iniPrint_Accept = 1 Then
                    Select Case LB_Menu.Tag
                                Case "1"
                                        Txt_Formula = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                         Call ShowReport("PBT9.rpt", Txt_Formula)
                                Case "2"
                                        Txt_Formula = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReport("PRD8.rpt", Txt_Formula)
                                Case "3"
                                        Txt_Formula = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReport("PP3.rpt", Txt_Formula)
                                Case "5"
                                        Txt_Formula = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                         Call ShowReport("PBT9_OTHER.rpt", Txt_Formula)
                                Case "6"
                                        Txt_Formula = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReport("PRD8_OTHER.rpt", Txt_Formula)
                                Case "7"
                                        Txt_Formula = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReport("PP3_OTHER.rpt", Txt_Formula)
                    End Select
        End If
Exit Sub
ErrPost:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Exit Sub
ErrPost2:
End Sub

Private Sub Btn_Print_Click()
          Frm_Print_AcceptTax.LB_Menu.Tag = Me.LB_Menu.Tag
          Frm_Print_AcceptTax.LB_Menu.Caption = Me.LB_Menu.Caption
          Frm_Print_AcceptTax.Show vbModal
End Sub

Private Sub Btn_SearchOwnerShip_PBT5_Click()
'Call SEARCH_PBT9
'                    If Query.RecordCount > 0 Then
'                       Query.MoveFirst
'                       If Txt_PBT_NO.Text = Empty Then
'        Dim sql_txt As String
        
        Select Case LB_Menu.Tag
                Case "1"
                            Status_InToFrm = "PBT9"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�����պ��ا��ͧ���"
                Case "2"
                            Status_InToFrm = "PRD8"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�������ç���͹��з��Թ"
                Case "3"
                            Status_InToFrm = "PP3"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�����ջ���"
                Case "5"
                            Status_InToFrm = "PBT9_OTHER"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�����պ��ا��ͧ���(�������)"
                Case "6"
                            Status_InToFrm = "PRD8_OTHER"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�������ç���͹��з��Թ(�������)"
                Case "7"
                            Status_InToFrm = "PP3_OTHER"
                            Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�����ջ���(�������)"
        End Select
        Frm_SerachForProcess.Show vbModal
'                      End If
        If Lb_OwnerShip_ID.Caption <> Empty Then
        
               Set Query = Globle_Connective.Execute("exec sp_search_accept_ownership '" & Lb_OwnerShip_ID.Caption & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
'               Select Case LB_Menu.Tag
'                           Case "1", "5"
'                                    sql_txt = "SELECT PBT5_NO,PBT5.LAND_ID,LAND_NOTIC,Format(PBT5_AMOUNT,'0.00'),Cstr(PBT5_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','����¹�����Է���') AS STATE_OWNER" & _
'                                    " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID" & _
'                                    " WHERE PBT5_NO_STATUS=1 AND PBT5_YEAR=" & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID='" & Lb_OwnerShip_ID.Caption & "'" & " AND PBT5_SET_GK" & IIf(LB_Menu.Tag = "1", "<>", "=") & "2"
''                                                   Query.Find "PBT5_NO = '" & Trim$(Txt_PBT_NO.Text) & "'", , adSearchForward
'                           Case "2", "6"
'                                    sql_txt = "SELECT PRD2_NO,BUILDINGDATA.LAND_ID,BUILDING_HOME_NO,Format(PRD2_RENTYEAR_TOTAL,'0.00'),Cstr(PRD2_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','����¹�����Է���') AS STATE_OWNER,PRD2.BUILDING_ID" & _
'                                    " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID = BUILDINGDATA.BUILDING_ID" & _
'                                    " WHERE PRD2_NO_STATUS=1 AND PRD2_YEAR=" & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID='" & Lb_OwnerShip_ID.Caption & "'" & " AND PRD2_SET_GK" & IIf(LB_Menu.Tag = "2", "<>", "=") & "2"
''                                                     Query.Find "PRD2_NO = '" & Trim$(Txt_PBT_NO.Text) & "'", , adSearchForward
'                           Case "3", "7"
'                                    sql_txt = "SELECT PP1_NO,SIGNBORDDATA.LAND_ID,BUILDING_NO,Format(PP1_AMOUNT,'0.00'),Cstr(PP1_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','�����Է�������') AS STATE_OWNER,PP1.SIGNBORD_ID " & _
'                                    " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID = SIGNBORDDATA.SIGNBORD_ID " & _
'                                    " WHERE PP1_NO_STATUS=1 AND PP1_YEAR=" & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID='" & Lb_OwnerShip_ID.Caption & "'" & " AND PP1_SET_GK" & IIf(LB_Menu.Tag = "3", "<>", "=") & "2"
''                                                     Query.Find "PP1_NO = '" & Trim$(Txt_PBT_NO.Text) & "'", , adSearchForward
'               End Select
'               Call SET_QUERY(sql_txt, Query)
               If Query.RecordCount > 0 Then
'                    Select Case LB_Menu.Tag
'                            Case "1"
                        Set Grid_Result_PBT9.DataSource = Query
'                            Case "2"
'                                    Set Grid_Result_PBT9.DataSource = Query
'                            Case "3"
'                                    Set Grid_Result_PBT9.DataSource = Query

'                    End Select
                    For i = 1 To Grid_Result_PBT9.Rows - 1
    '                       If Grid_Result_PBT9.TextMatrix(i, Grid_Result_PBT9.Cols - 1) = "/" Then
                                Lb_money.Caption = CCur(Lb_money.Caption) + CCur(Grid_Result_PBT9.TextMatrix(i, 3))
                                Grid_Result_PBT9.TextMatrix(i, 4) = Format$(Query.Fields(4).Value, "dd/mm/yyyy")
    '                            lb_Count.Caption = CInt(lb_Count.Caption) + 1
    '                       End If
                    Next i
                    Lb_money.Caption = FormatNumber(CCur(Lb_money.Caption), 2, vbTrue)
               End If
               LB_Sum.Caption = Query.RecordCount
'               If Query.AbsolutePosition > 0 Then
'                       Grid_Result_PBT9.TopRow = Query.AbsolutePosition
'                       Grid_Result_PBT9.Row = Query.AbsolutePosition
'               Else
'                        MsgBox "��辺�Ţ������Ẻ " & Txt_PBT_NO.Text & " !", vbCritical, "����͹"
'                        Exit Sub
'               End If
               Grid_Result_PBT9.Col = 0
               Grid_Result_PBT9.ColSel = Grid_Result_PBT9.Cols - 1
               Grid_Result_PBT9.SetFocus
               Lb_money.Caption = "0.00"
               
'               Call Grid_Result_PBT9_RowColChange
        End If
        Call Set_Grid
        Call Grid_Result_PBT9_LeaveCell
'            End If
End Sub

'Private Sub chkAppeal_Click()
'        If chkAppeal.Value = 0 Then
'                chkAppeal_No.Enabled = False
'                chkAppeal_Ok.Enabled = False
'                chkAppeal_No.Value = 0
'                chkAppeal_Ok.Value = 0
'                Label3(10).ForeColor = &HFFFFFF
'                Label3(11).ForeColor = &HFFFFFF
'                Label3(10).Enabled = False
'                Label3(11).Enabled = False
'                MaskEdBox2.Enabled = False
'                MaskEdBox2.Text = "__/__/____"
'        Else
'                chkAppeal_Ok.Enabled = True
'                chkAppeal_Ok.Value = 1
'                chkAppeal_No.Enabled = True
'                Label3(10).Enabled = True
'                Label3(11).Enabled = True
'                MaskEdBox2.Enabled = True
'                MaskEdBox2.Text = "__/__/____"
'                MaskEdBox2.SetFocus
'        End If
'End Sub

'Private Sub chkAppeal_No_Click()
'        If chkAppeal_No.Value = Checked Then
'                chkAppeal_Ok.Value = Unchecked
'                Label3(10).ForeColor = &HFFFFFF
'                Label3(11).ForeColor = &H80FFFF
'        ElseIf chkAppeal_No.Enabled = True Then
'                chkAppeal_Ok.Value = Checked
'        End If
'End Sub

'Private Sub chkAppeal_ok_Click()
'        If chkAppeal_Ok.Value = Checked Then
'                chkAppeal_No.Value = Unchecked
'                Label3(10).ForeColor = &H80FFFF
'                Label3(11).ForeColor = &HFFFFFF   '&HFF&
'        ElseIf chkAppeal_Ok.Enabled = True Then
'                chkAppeal_No.Value = Checked
'        End If
'End Sub

Private Sub cmd_Search_No_Accept_Click()
        Select Case LB_Menu.Tag
                Case "1"
                         Status_InToFrm = "PBT9_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���պ��ا��ͧ���"
                Case "2"
                         Status_InToFrm = "PRD8_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ�����ç���͹��з��Թ"
                Case "3"
                         Status_InToFrm = "PP3_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���ջ���"
                Case "5"
                         Status_InToFrm = "PBT9_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���պ��ا��ͧ���"
                Case "6"
                         Status_InToFrm = "PRD8_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ�����ç���͹��з��Թ"
                Case "7"
                         Status_InToFrm = "PP3_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���ջ���"
'                Case "4"
'                         Status_InToFrm = "PBA2_ACCEPT"
'                          Frm_SerachForProcess.Caption = " �鹢����� - ������Է�������¡�������͹حҵ��Сͺ��ä��"
        End Select
        Frm_SerachForProcess.Show vbModal
        MaskEdBox1.SelStart = 0
        MaskEdBox1.SetFocus
End Sub

Private Sub Command1_Click()
        On Error GoTo Err_Update
        Globle_Connective.BeginTrans
        If Text2.Text = Empty Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If InStr(1, MaskEdBox1.Text, "_") > 0 Then
              MsgBox "�ô��͡�ѹ���-�Ѻ�������Ţ�����Թ", vbCritical, "Microtax Software"
              Globle_Connective.RollbackTrans
              Exit Sub
        End If
                Set Query2 = Globle_Connective.Execute("exec sp_search_accept_no '" & Trim$(Text2.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
'                    Select Case LB_Menu.Tag
'                                 Case "1", "5"
'                                        Call SET_QUERY("SELECT PBT5_NO_ACCEPT FROM PBT5 WHERE PBT5_NO_ACCEPT <> '' AND PBT5_NO_ACCEPT = '" & Text2.Text & "'" & _
'                                                     " AND PBT5_STATUS = 0 AND FLAG_RETURN = 0 AND PBT5_YEAR = " & Lb_Year.Caption, Query2)
'                                Case "2", "6"
'                                        Call SET_QUERY("SELECT PRD2_NO_ACCEPT FROM PRD2 WHERE PRD2_NO_ACCEPT <> '' AND PRD2_NO_ACCEPT = '" & Text2.Text & "'" & _
'                                                     " AND PRD2_STATUS = 0 AND FLAG_RETURN = 0 AND PRD2_YEAR = " & Lb_Year.Caption, Query2)
'                                Case "3", "7"
'                                        Call SET_QUERY("SELECT PP1_NO_ACCEPT FROM PP1 WHERE PP1_NO_ACCEPT <> '' AND PP1_NO_ACCEPT = '" & Text2.Text & "'" & _
'                                                     " AND PP1_STATUS = 0 AND FLAG_RETURN = 0 AND PP1_YEAR = " & Lb_Year.Caption, Query2)
''                                Case "4"
''                                        Call SET_QUERY("SELECT PBA1_NO_ACCEPT FROM PBA1  WHERE PBA1_NO_ACCEPT <> '' AND PBA1_NO_ACCEPT = '" & Text2.Text & "'" & _
''                                                     " AND PBA1_STATUS = 0 AND FLAG_RETURN = 0 AND PBA1_YEAR = " & Lb_Year.Caption, Query2)
'                  End Select
        If Query2.RecordCount > 0 Then
'                    Debug.Print "exec sp_update_accept_datereturn '" & Trim$(Text2.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(MaskEdBox1.Text) & "','" & LB_Menu.Tag & "'"
                    Globle_Connective.Execute "exec sp_update_accept_datereturn '" & Trim$(Text2.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(MaskEdBox1.Text) & "','" & LB_Menu.Tag & "'", , adCmdUnknown
                    Call SET_Execute2("exec sp_update_accept_datereturn '" & Trim$(Text2.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(MaskEdBox1.Text) & "','" & LB_Menu.Tag & "'", 8, False)
'                            Select Case LB_Menu.Tag
'                                         Case "1", "5"
''                                                SET_Execute ("UPDATE PBT5 SET FLAG_RETURN = 1 , PBT5_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
''                                                                           " WHERE PBT5_NO_ACCEPT = '" & Text2.Text & "'" & " AND PBT5_YEAR = " & Lb_Year.Caption)
'                                                SET_Execute ("UPDATE PBT5 SET FLAG_RETURN = 1, PBT5_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
'                                                                           " WHERE PBT5_NO_ACCEPT = '" & Text2.Text & "'" & " AND PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3)
'                                         Case "2", "6"
'                                                SET_Execute ("UPDATE PRD2 SET FLAG_RETURN = 1 , PRD2_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
'                                                                           " WHERE PRD2_NO_ACCEPT = '" & Text2.Text & "'" & " AND PRD2_YEAR = " & Lb_Year.Caption)
'                                         Case "3", "7"
'                                                SET_Execute ("UPDATE PP1 SET FLAG_RETURN = 1 , PP1_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
'                                                                           " WHERE PP1_NO_ACCEPT = '" & Text2.Text & "'" & " AND PP1_YEAR = " & Lb_Year.Caption)
''                                         Case "4"
''                                                SET_Execute ("UPDATE PBA1 SET FLAG_RETURN = 1 , PBA1_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
''                                                                           " WHERE PBA1_NO_ACCEPT = '" & Text2.Text & "'" & " AND PBA1_YEAR = " & Lb_Year.Caption)
'                             End Select
                            MsgBox "�ӡ�õͺ�Ѻ�����Ţ�����Թ : " & Text2.Text & " ŧ㹢���������ó�", vbOKOnly, "Microtax Software"
                            Text2.Text = Empty
                            MaskEdBox1.Text = "__/__/____"
        Else
                Globle_Connective.RollbackTrans
                MsgBox "��辺�����Ţ�����Թ : " & Text2.Text & " 㹰ҹ������", vbCritical, "Microtax Software"
                Exit Sub
        End If
        Globle_Connective.CommitTrans
        Exit Sub
Err_Update:
        Globle_Connective.RollbackTrans
        MsgBox Err.Description
End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
End Sub

Private Sub Form_Load()
        LB_Menu.Tag = Assets_Type
        Set Query = New ADODB.Recordset
        Set Query2 = New ADODB.Recordset
        Lb_Year.Caption = Right$(Date, 4)
End Sub

'Private Sub SEARCH_PBT9()
'Dim sql_txt As String
'Select Case LB_Menu.Tag
'            Case "1"
'                    sql_txt = " SELECT A.PBT5_NO,B.PRENAME & B.OWNER_NAME & '  '  & B.OWNER_SURNAME, Count (A.LAND_ID), Format(Sum(A.PBT5_AMOUNT),'#,###,###0.00#'), Format(PBT5_DATE,'dd/mm/yyyy') " & _
'                                           " FROM PBT5 AS A, OWNERSHIP AS B, LANDDATA AS C " & _
'                                           " Where B.OWNERSHIP_ID = A.OWNERSHIP_ID And C.LAND_ID = A.LAND_ID " & _
'                                           " AND PBT5_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBT5_NO_ACCEPT = '' OR PBT5_NO_ACCEPT IS NULL) AND PBT5_STATUS=0 AND FLAG_RETURN= 0" & _
'                                           " AND PBT5_NO_STATUS=1" & _
'                                           " GROUP BY A.PBT5_NO,PBT5_DATE,B.PRENAME,B.OWNER_NAME,B.OWNER_SURNAME "
'             Case "2"
'                    sql_txt = " SELECT A.PRD2_NO,B.PRENAME & B.OWNER_NAME & '  '  & B.OWNER_SURNAME, Count (A.BUILDING_ID), Format(Sum(PRD2_RENTYEAR_TOTAL),'#,###,###0.00#'), Format(PRD2_DATE,'dd/mm/yyyy') " & _
'                                            " FROM PRD2 AS A, OWNERSHIP AS B, BUILDINGDATA  AS C " & _
'                                            " Where B.OWNERSHIP_ID = A.OWNERSHIP_ID And C.BUILDING_ID = A.BUILDING_ID " & _
'                                            " AND (A.PRD2_NO_ACCEPT = '' OR A.PRD2_NO_ACCEPT IS NULL) AND PRD2_STATUS=0 AND FLAG_RETURN= 0 AND PRD2_YEAR = " & CInt(Lb_Year.Caption) & _
'                                            " AND PRD2_NO_STATUS=1" & _
'                                            " GROUP BY A.PRD2_NO,PRD2_DATE,B.PRENAME,B.OWNER_NAME,B.OWNER_SURNAME "
'             Case "3"
'                    sql_txt = " SELECT A.PP1_NO,B.PRENAME & B.OWNER_NAME & '  '  & B.OWNER_SURNAME, Count (A.SIGNBORD_ID), Format(Sum(A.PP1_AMOUNT),'#,###,###0.00#'), Format(PP1_DATE,'dd/mm/yyyy') " & _
'                                            " FROM PP1 AS A, OWNERSHIP AS B, SIGNBORDDATA AS C " & _
'                                            " Where B.OWNERSHIP_ID = A.OWNERSHIP_ID And C.SIGNBORD_ID = A.SIGNBORD_ID " & _
'                                            " AND (A.PP1_NO_ACCEPT = '' OR A.PP1_NO_ACCEPT IS NULL) AND PP1_STATUS=0 AND FLAG_RETURN= 0 AND  PP1_YEAR = " & CInt(Lb_Year.Caption) & _
'                                            " AND PP1_NO_STATUS=1" & _
'                                            " GROUP BY A.PP1_NO,PP1_DATE,B.PRENAME,B.OWNER_NAME,B.OWNER_SURNAME "
'             Case "4"
'                    sql_txt = " SELECT A.PBA1_NO,B.PRENAME & B.OWNER_NAME & '  '  & B.OWNER_SURNAME, Count (A.LICENSE_BOOK), Format(Sum(A.PBA1_AMOUNT),'#,###,###0.00#'), Format(PBA1_DATE,'dd/mm/yyyy') " & _
'                                            " FROM PBA1 AS A, OWNERSHIP AS B, LICENSEDATA AS C " & _
'                                            " Where B.OWNERSHIP_ID = A.OWNERSHIP_ID And C.LICENSE_BOOK = A.LICENSE_BOOK AND C.LICENSE_NO = A.LICENSE_NO " & _
'                                            " AND (A.PBA1_NO_ACCEPT = '' OR A.PBA1_NO_ACCEPT IS NULL) AND PBA1_STATUS=0 AND FLAG_RETURN= 0 AND  PBA1_YEAR = " & CInt(Lb_Year.Caption) & _
'                                            " AND PBA1_NO_STATUS=1" & _
'                                            " GROUP BY A.PBA1_NO,PBA1_DATE,B.PRENAME,B.OWNER_NAME,B.OWNER_SURNAME "
'  End Select
'  Call SET_QUERY(sql_txt, Query)
'  If Query.RecordCount > 0 Then
'        Set Grid_Result_PBT9.DataSource = Query
'  Else
'       Call Clear_Grid
'       Call Set_Grid
'  End If
'  LB_Sum.Caption = Query.RecordCount
'End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
        Set Query2 = Nothing
        Set Frm_AcceptTax = Nothing
End Sub

Private Sub Grid_Result_PBT9_Click()
        Call Grid_Result_PBT9_RowColChange
End Sub

Private Sub Grid_Result_PBT9_EnterCell()
        strTemp = Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)
End Sub

Private Sub Grid_Result_PBT9_KeyPress(KeyAscii As Integer)
        If KeyAscii = 8 And Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) <> Empty Then
                Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) = Left$(Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3), Len(Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)) - 1)
                strTemp2 = Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)
        End If
        If InStr(1, "0123456789.", Chr(KeyAscii), vbTextCompare) <> 0 Or KeyAscii = 13 Then
                With Grid_Result_PBT9
                        If Not (FindText(.TextMatrix(.Row, 3), ".") > 0 And KeyAscii = 46) Then
'                                .TextMatrix(.Row, 2) = Empty
                                strTemp2 = strTemp2 & Chr$(KeyAscii)
                                .TextMatrix(.Row, 3) = strTemp2
                                If KeyAscii = 13 Then
                                        strTemp = strTemp2
                                        .TextMatrix(.Row, 3) = strTemp2
                                End If
                        End If
                End With
                If Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) = Empty Then Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) = 0
                Lb_money.Caption = "0.00"
                For i = 1 To Grid_Result_PBT9.Rows - 1
                        Lb_money.Caption = CDbl(Lb_money.Caption) + IIf(Grid_Result_PBT9.TextMatrix(i, 3) = ".", 0, Grid_Result_PBT9.TextMatrix(i, 3))
                Next i
                Lb_money.Caption = FormatNumber(CDbl(Lb_money.Caption), 2, vbTrue)
        End If
End Sub

Private Sub Grid_Result_PBT9_LeaveCell()
'        Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 2) = strTemp
        strTemp2 = Empty
        For i = 1 To Grid_Result_PBT9.Rows - 1
                If Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) = Empty Or Trim$(Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)) = "." Then
                        Grid_Result_PBT9.TextMatrix(i, 3) = "0.00"
                End If
                Grid_Result_PBT9.TextMatrix(i, 3) = Format$(Grid_Result_PBT9.TextMatrix(i, 3), "0.00")
        Next i
End Sub

Private Sub Grid_Result_PBT9_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        strTemp = Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)
End Sub

Private Sub Grid_Result_PBT9_RowColChange()
'        If Grid_Result_PBT9.TextMatrix(1, 0) = Empty Then Exit Sub
'        Lb_Name.Caption = "���ͼ���������� :"
'        Lb_money.Caption = Empty
'        Lb_Name.Caption = "���ͼ���������� :  " & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 1)
'        Lb_money.Caption = FormatNumber(CSng(Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 2)), 2)
        Lb_money.Caption = "0.00"
        For i = 1 To Grid_Result_PBT9.Rows - 1
                Lb_money.Caption = CCur(Lb_money.Caption) + IIf(Grid_Result_PBT9.TextMatrix(i, 3) = Empty, 0, Grid_Result_PBT9.TextMatrix(i, 3))
        Next i
        Lb_money.Caption = FormatNumber(CCur(Lb_money.Caption), 2, vbTrue)
End Sub

'Private Sub Label3_Click(index As Integer)
'        Select Case index
'                Case 5
'                            chkAppeal.Value = chkAppeal.Value Xor 1
'                Case 10
'                            chkAppeal_Ok.Value = chkAppeal_Ok.Value Xor 1
'                Case 11
'                            chkAppeal_No.Value = chkAppeal_No.Value Xor 1
'        End Select
'End Sub

Private Sub LB_Menu_Change()
LB_Menu.Tag = Assets_Type
Call Clear_Grid
'Call SEARCH_PBT9
Call Set_Grid
End Sub

Private Sub Lb_Year_Change()
        Frm_AcceptTax.MousePointer = 11
'        Call SEARCH_PBT9
        Grid_Result_PBT9.Clear
        Grid_Result_PBT9.Rows = 2
        Call Set_Grid
        Txt_PBT_NO.Text = Empty
        Lb_OwnerShip_ID.Caption = Empty
        Text1.Text = Empty
        Lb_money.Caption = "0.00"
        LB_Sum.Caption = Empty
        Frm_AcceptTax.MousePointer = 0
End Sub

Private Sub MaskEdBox1_GotFocus()
        MaskEdBox1.SelStart = 0
        MaskEdBox1.SelLength = Len(MaskEdBox1.Text)
End Sub

'Private Sub MaskEdBox2_GotFocus()
'        MaskEdBox2.SelStart = 0
'        MaskEdBox2.SelLength = 10
'End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then MaskEdBox1.SetFocus
End Sub

Private Sub Txt_PBT_NO_DblClick()
        Txt_PBT_NO.Text = Empty
End Sub

Private Sub Txt_PBT_NO_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call Btn_SearchOwnerShip_PBT5_Click
End Sub



