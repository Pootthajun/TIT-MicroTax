VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_PayTax 
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   11880
   ControlBox      =   0   'False
   Icon            =   "Frm_PayTax.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_PayTax.frx":151A
   ScaleHeight     =   9030
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin VB.OptionButton OptionChkType 
      BackColor       =   &H00C0C0C0&
      Caption         =   "������ �"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   255
      Index           =   1
      Left            =   1350
      TabIndex        =   13
      Top             =   2390
      Width           =   1095
   End
   Begin VB.OptionButton OptionChkType 
      BackColor       =   &H00C0C0C0&
      Caption         =   "�.�.1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   12
      Top             =   2390
      Value           =   -1  'True
      Width           =   915
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   345
      Left            =   0
      TabIndex        =   11
      Top             =   8670
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   609
      _Version        =   393216
      Appearance      =   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   360
      Left            =   7230
      TabIndex        =   10
      Top             =   1770
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   635
      _Version        =   393216
      Value           =   2545
      BuddyControl    =   "TXT_YEAR"
      BuddyDispid     =   196613
      OrigLeft        =   7320
      OrigTop         =   1800
      OrigRight       =   7575
      OrigBottom      =   2205
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   9060
      TabIndex        =   9
      Top             =   2340
      Width           =   2925
   End
   Begin VB.CommandButton Btn_SearchOwnerShip_PBT5 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   12060
      Picture         =   "Frm_PayTax.frx":1583A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   2340
      Width           =   585
   End
   Begin VB.CommandButton Btn_Del 
      BackColor       =   &H00E0E0E0&
      Caption         =   "���͡����¡��੾���������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   8670
      Picture         =   "Frm_PayTax.frx":15DC4
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   8130
      Width           =   4035
   End
   Begin VB.TextBox TXT_YEAR 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   6180
      MaxLength       =   4
      TabIndex        =   5
      Text            =   "2548"
      Top             =   1770
      Width           =   1050
   End
   Begin VB.CommandButton Btn_Post 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�Ѵ����¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   4440
      Picture         =   "Frm_PayTax.frx":16C06
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8130
      Width           =   4245
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   5385
      Left            =   150
      TabIndex        =   2
      Top             =   2700
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   9499
      _Version        =   393216
      BackColor       =   14737632
      Rows            =   17
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   9884603
      ForeColorSel    =   16777215
      BackColorBkg    =   12632256
      GridColor       =   8421504
      GridColorFixed  =   4210752
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.CommandButton Btn_Search 
      BackColor       =   &H00E0E0E0&
      Caption         =   "�Ѵ�����ŷ���¡�� >>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   180
      Picture         =   "Frm_PayTax.frx":17190
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   8130
      Width           =   4275
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   375
      Index           =   0
      Left            =   180
      Top             =   2310
      Width           =   2415
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���Ҫ��ͺؤ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   7710
      TabIndex        =   8
      Top             =   2400
      Width           =   1275
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   375
      Index           =   1
      Left            =   7560
      Top             =   2310
      Width           =   5115
   End
   Begin VB.Label Lb_1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻվط��ѡ�Ҫ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   300
      Left            =   5730
      TabIndex        =   1
      Top             =   1440
      Width           =   1950
   End
   Begin VB.Label LB_Menu 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���պ��ا��ͧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6165
      TabIndex        =   0
      Top             =   960
      Width           =   1245
   End
End
Attribute VB_Name = "Frm_PayTax"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset
Dim SET_GK As Boolean
'Dim MONEY_IN_GK As String

Private Function CalculateSignbord(DateCal As Date, Money As Currency) As Currency
Dim buffer_sum As Currency
buffer_sum = Money / 4
If Right$(DateCal, 4) = TXT_YEAR.Text Then
                If Int(Month(DateCal)) = 4 Or Int(Month(DateCal)) = 5 Or Int(Month(DateCal)) = 6 Then     '�Դ 3/4
                        CalculateSignbord = buffer_sum * 3
                End If
                If Int(Month(DateCal)) = 7 Or Int(Month(DateCal)) = 8 Or Int(Month(DateCal)) = 9 Then      '�Դ 2/4
                        CalculateSignbord = buffer_sum * 2
                End If
                If Int(Month(DateCal)) = 10 Or Int(Month(DateCal)) = 11 Or Int(Month(DateCal)) = 12 Then      '�Դ 1/4
                        CalculateSignbord = buffer_sum
                End If
                If Int(Month(DateCal)) = 1 Or Int(Month(DateCal)) = 2 Or Int(Month(DateCal)) = 3 Then      '�Դ �����
                        CalculateSignbord = Money
                End If
Else
                CalculateSignbord = Money
End If
End Function

Private Sub Clear_Grid()
    On Error Resume Next
    Dim i As Byte
          Grid_Result.Rows = 2
    For i = 0 To Grid_Result.Cols - 1
           Grid_Result.TextMatrix(1, i) = ""
    Next i
    Btn_Post.Caption = "�Ѵ����¡��"
End Sub

Private Sub Set_Grid()
With Grid_Result
        Select Case LB_Menu.Tag
                Case "1", "5"
                        .FormatString = "^���ʼ����������|^���ͼ����������|^�������|^�ӹǹ�ŧ���Թ|^�������"
                Case "2", "6"
                        .FormatString = "^���ʼ����������|^���ͼ����������|^�������|^�ӹǹ�ç���͹|^�������"
                Case "3", "7"
                        .FormatString = "^���ʼ����������|^���ͼ����������|^�������|^�ӹǹ����|^�������"
                Case "4"
                        .FormatString = "^���ʼ����������|^���ͼ����������|^�������|^�ӹǹ�͹حҵ|^��Ҹ�������"
         End Select
        .ColWidth(0) = 1400: .ColAlignment(0) = 1
        .ColWidth(1) = 2900: .ColAlignment(1) = 1
        .ColWidth(2) = 4750: .ColAlignment(2) = 1
        .ColWidth(3) = 1500: .ColAlignment(3) = 7
        .ColWidth(4) = 1500: .ColAlignment(4) = 7
        .ColWidth(5) = 0
End With
End Sub

Private Sub Btn_Del_Click()
Dim Rs_Chk_PBT5 As ADODB.Recordset
Dim Temp_money As Currency
        On Error GoTo ErrInsert
        Globle_Connective.BeginTrans
If LenB(Trim$(Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Col))) > 0 Then
'    If SET_GK = True Then  '���������
'        Dim sql_txt As String
'                             Select Case LB_Menu.Tag
''                                         Case "1"
''                                                         sql_txt = "SELECT A.LAND_ID,LAND_SUMTAX ,B.OWNERSHIP_ID AS OWNERSHIP_ID,LAND_DATE" & _
''                                                                " FROM LANDDATA A ,LANDDATA_NOTIC B" & _
''                                                                " Where B.LAND_ID = A.LAND_ID AND FLAG_PAYTAX  = 1 AND  B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'" & _
''                                                                " AND NOT EXISTS(SELECT LAND_ID FROM PBT5 WHERE PBT5_SET_GK=0 AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) & _
''                                                                " AND PBT5.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "' AND A.LAND_ID=PBT5.LAND_ID)"
''                                                        sql_txt = "SELECT A.LAND_ID,LAND_SUMTAX ,B.OWNERSHIP_ID AS OWNERSHIP_ID,LAND_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE" & _
''                                                                " FROM LANDDATA A ,LANDDATA_NOTIC B,OWNERSHIP C" & _
''                                                                " Where B.LAND_ID = A.LAND_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID AND FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC=1 AND  B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "'" & _
''                                                                " AND NOT EXISTS(SELECT LAND_ID FROM PBT5 WHERE PBT5_SET_GK=0 AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) & _
''                                                                " AND PBT5.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "' AND A.LAND_ID=PBT5.LAND_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID)"
''                                         Case "2"
''                                                         sql_txt = "SELECT A.BUILDING_ID , TOTAL_TAX_EXCLUDE , B.OWNERSHIP_ID as OWNERSHIP_ID FROM  BUILDINGDATA A , BUILDINGDATA_NOTIC B , LANDDATA C Where B.BUILDING_ID = A.BUILDING_ID AND C.LAND_ID = A.LAND_ID AND A.FLAG_PAYTAX =1 AND B.OWNERSHIP_ID = '" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'"
''                                         Case "3"
''                                                         sql_txt = "SELECT Format(SIGNBORD_START_DATE,'dd/mm/yyyy') AS START_DATE,A.SIGNBORD_ID,SIGNBORD_SUMTAX ,B.OWNERSHIP_ID AS OWNERSHIP_ID" & _
''                                                                " FROM SIGNBORDDATA A ,SIGNBORDDATA_NOTIC B" & _
''                                                                " Where B.SIGNBORD_ID = A.SIGNBORD_ID AND FLAG_PAYTAX  = 1 AND  B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "'" & _
''                                                                " AND NOT EXISTS(SELECT SIGNBORD_ID FROM PP1 WHERE PP1_SET_GK=0 AND PP1_YEAR=" & CInt(TXT_YEAR.Text) & _
''                                                                " AND PP1.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "' AND A.SIGNBORD_ID=PBT5.SIGNBORD_ID)"
'                                         Case "4"
'                                                         sql_txt = "SELECT LICENSE_BOOK ,LICENSE_NO ,LICENSE_SUMTAX , OWNERSHIP_ID FROM  LICENSEDATA  Where OWNERSHIP_ID = '" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'"
'                                         Case "5", "1"
'                                                        sql_txt = "SELECT A.LAND_ID,LAND_SUMTAX ,B.OWNERSHIP_ID AS OWNERSHIP_ID,LAND_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE" & _
'                                                                        " FROM LANDDATA A ,LANDDATA_NOTIC B,OWNERSHIP C" & _
'                                                                        " Where B.LAND_ID = A.LAND_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID AND FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC=1 AND  B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'" & _
'                                                                        " AND NOT EXISTS(SELECT LAND_ID FROM PBT5 WHERE PBT5_YEAR=" & CInt(TXT_YEAR.Text) & _
'                                                                        " AND PBT5.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "' AND A.LAND_ID=PBT5.LAND_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID)"
'                                        Case "6", "2"
'                                                        sql_txt = "SELECT A.BUILDING_ID,TOTAL_TAX_EXCLUDE,B.OWNERSHIP_ID as OWNERSHIP_ID,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE" & _
'                                                                        " FROM BUILDINGDATA A,BUILDINGDATA_NOTIC B,OWNERSHIP C" & _
'                                                                        " WHERE B.BUILDING_ID=A.BUILDING_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID AND FLAG_PAYTAX = 1 AND OWNERSHIP_NOTIC=1 AND B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "'" & _
'                                                                        " AND NOT EXISTS(SELECT BUILDING_ID FROM PRD2 WHERE PRD2_YEAR=" & CInt(TXT_YEAR.Text) & _
'                                                                        " AND PRD2.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "' AND A.BUILDING_ID=PRD2.BUILDING_ID)"
'                                        Case "7", "3"
'                                                        sql_txt = "SELECT Format(SIGNBORD_START_DATE,'dd/mm/yyyy') AS START_DATE,A.SIGNBORD_ID,SIGNBORD_SUMTAX ,B.OWNERSHIP_ID AS OWNERSHIP_ID,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE" & _
'                                                                        " FROM SIGNBORDDATA A ,SIGNBORDDATA_NOTIC B,OWNERSHIP C" & _
'                                                                        " Where B.SIGNBORD_ID = A.SIGNBORD_ID AND C.OWNERSHIP_ID=B.OWNERSHIP_ID AND FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC=1 AND B.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "'" & _
'                                                                        " AND NOT EXISTS(SELECT SIGNBORD_ID FROM PP1 WHERE PP1_YEAR=" & CInt(TXT_YEAR.Text) & _
'                                                                        " AND PP1.OWNERSHIP_ID ='" & Grid_Result.TextMatrix(i, 5) & "' AND A.SIGNBORD_ID=PP1.SIGNBORD_ID)"
'
'                            End Select
'                            Call SET_QUERY(sql_txt, Query)
'                If Query.RecordCount > 0 Then
'                   Query.MoveFirst
'                    Do While Not Query.EOF
'                             Temp_money = 0
'                             Select Case LB_Menu.Tag   '  PBT5_SET_GK=0 ��ҡѺ�������,2 ��ҡѺ�������
'                                         Case "1", "5"
'                                                sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                        " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, 0) & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_DATE").Value), "Null", "'" & Format$(Query.Fields("LAND_DATE").Value, "dd/mm/yyyy") & "'") & ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
''                                         sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_OWNER_DATE) " & _
''                                                                " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & ",0" & _
''                                                                ",'" & Format$(Query.Fields("LAND_DATE").Value, "dd/mm/yyyy") & "')"
'                                         Case "2", "6"
'                                                sql_txt = "INSERT INTO PRD2 ( PRD2_YEAR, BUILDING_ID,OWNERSHIP_ID,PRD2_RENTYEAR_TOTAL,PRD2_GK_MONEY,PRD2_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PRD2_AMOUNT_ACCEPT) " & _
'                                                                " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "6", 2, 0) & _
'                                                                ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & ")"
'                                         Case "3", "7"
'                                                If FormatNumber(CalculateSignbord(Query.Fields("START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2) < 200 Then
'                                                        Temp_money = 200
'                                                Else
'                                                        Temp_money = FormatNumber(CalculateSignbord(Query.Fields("START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
'                                                End If
'                                                sql_txt = "INSERT INTO PP1 ( PP1_YEAR,SIGNBORD_ID,OWNERSHIP_ID,PP1_AMOUNT,PP1_GK_MONEY,PP1_SET_GK,PP1_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PP1_AMOUNT_ACCEPT) " & _
'                                                                 " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, 0) & ",'" & Format$(Query.Fields("START_DATE").Value, "dd/mm/yyyy") & _
'                                                                 "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ")"
'                                         Case "4"
'                                                sql_txt = "INSERT INTO PBA1 ( PBA1_YEAR, LICENSE_BOOK,LICENSE_NO ,OWNERSHIP_ID,PBA1_AMOUNT,PBA1_GK_MONEY,PBA1_SET_GK) " & _
'                                                                " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LICENSE_BOOK").Value & "','" & Query.Fields("LICENSE_NO").Value & "','" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'," & IIf(IsNull(Query.Fields("LICENSE_SUMTAX").Value), 0, Query.Fields("LICENSE_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & ",0)"
'                             End Select
'                                                SET_Execute (sql_txt)
'                                                Query.MoveNext
'                    Loop
'                            Btn_Post.Caption = "�Ѵ����¡�� : �ӹǹ " & Grid_Result.Rows - 1 & " ���"
'                            If Grid_Result.Rows > 2 Then
'                                    Grid_Result.RemoveItem (Grid_Result.Row)
'                            Else
'                                    Call Clear_Grid
'                            End If
'                            Globle_Connective.CommitTrans
'                            MsgBox "�ӡ�èѴ�红�����੾���������ó� ", vbOKOnly, "Microtax Software"
'                    End If
'       Else
'            Globle_Connective.RollbackTrans
'            MsgBox "�������ö�ѹ�֡����������ºؤ�������ͧ�ҡ�ѧ���Ѵ�� �.�.��Шӻվط��ѡ�Ҫ " & TXT_YEAR.Text, vbExclamation, "Microtax Software"
'      End If
'Else
'        Globle_Connective.RollbackTrans
'End If

'        If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Col) = Empty Then
'                MsgBox "��辺���������ͷ���¡�����ջ�Шӻ�", vbCritical, "����͹ !"
'                Exit Sub
'        End If
'        Dim GK_SUMMARY As Currency
        
'GK_SUMMARY = 0
Me.MousePointer = 11
'Me.Enabled = False
        If SET_GK Then
              j = 0             '�������
        Else
              j = 1             '�.�.1
        End If
        Set Rs_Chk_PBT5 = New ADODB.Recordset
'For i = 1 To Grid_Result.Rows - 1
            Set Query = Globle_Connective.Execute("exec sp_paytax_post_search '" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "'," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'," & j, , adCmdUnknown)
                    If Query.RecordCount > 0 Then
                        Query.MoveFirst
                        Do While Not Query.EOF
                                Temp_money = 0
                                Select Case LB_Menu.Tag
                                         Case "1", "5"                                 '���պ��ا��ͧ���
                                                If j = 0 Then ' �������
                                                        If IsNull(Query.Fields("LAND_DATE").Value) Then
                                                                strDate = ""
                                                        Else
                                                                strDate = CvDate2(Query.Fields("LAND_DATE").Value)
                                                        End If
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'" & strDate & "',0", , adCmdUnknown
'                                                         sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                        " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_DATE").Value), "Null", "'" & Format$(Query.Fields("LAND_DATE").Value, "dd/mm/yyyy") & "'") & ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                Else
                                                        If (CInt(TXT_YEAR.Text) - 2549) Mod 4 = 0 Then  '����㹻շ������������Ẻ����
                                                                Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1", , adCmdUnknown
'                                                                sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                        Else
'                                                                sql_txt = "SELECT PBT5_NO,PBT5_DATE,PBT5_NO_ACCEPT FROM PBT5 WHERE OWNERSHIP_ID='" & Grid_Result.TextMatrix(i, 5) & "' AND LAND_ID='" & Query.Fields("LAND_ID").Value & "'" & _
'                                                                                " AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) - 1 & " AND PBT5_NO_STATUS=1"
'                                                                Call SET_QUERY(sql_txt, Rs_Chk_PBT5)
                                                               
                                                                Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_pbt5_find4year '" & Grid_Result.TextMatrix(Grid_Result.Row, 5) & "','" & Query.Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) - 1, , adCmdUnknown)
                                                                If Rs_Chk_PBT5.RecordCount > 0 Then         '��������Ẻ㹻շ������  ��� PBT5_NO ��ҡѺ PBT5_NO �ͧ�շ������
                                                                         If IsNull(Rs_Chk_PBT5.Fields(1).Value) Then
                                                                                strDate = ""
                                                                        Else
                                                                                strDate = CvDate2(Rs_Chk_PBT5.Fields(1).Value)
                                                                        End If
                                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5_2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & j & _
                                                                                    ",'" & Rs_Chk_PBT5.Fields(0).Value & "',1" & ",'" & strDate & "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                                    ",'" & Rs_Chk_PBT5.Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value)
'                                                                        sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_NO,PBT5_NO_STATUS,PBT5_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_NO_ACCEPT,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                    " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields(0).Value & "',1" & ",'" & CStr(Rs_Chk_PBT5.Fields(1).Value) & "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                                Else
'                                                                        Debug.Print "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1"
                                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1", , adCmdUnknown
'                                                                        sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                    " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                    ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                                End If
                                                        End If
                                                End If
                                         Case "2", "6"                              ' �����ç���͹��з��Թ
                                                If j = 0 Then
                                                        Globle_Connective.Execute "exec sp_insert_paytax_prd2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "6", 2, j) & _
                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value), , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PRD2 ( PRD2_YEAR, BUILDING_ID,OWNERSHIP_ID,PRD2_RENTYEAR_TOTAL,PRD2_GK_MONEY,PRD2_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PRD2_AMOUNT_ACCEPT) " & _
'                                                                            " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "6", 2, j) & _
'                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & ")"
                                                Else
                                                        Globle_Connective.Execute "exec sp_insert_paytax_prd2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & j & _
                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value), , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PRD2 ( PRD2_YEAR, BUILDING_ID,OWNERSHIP_ID,PRD2_RENTYEAR_TOTAL,PRD2_GK_MONEY,PRD2_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PRD2_AMOUNT_ACCEPT) " & _
'                                                                            " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & ")"
                                                End If
                                         Case "3", "7"                               ' ���ջ���
                                                If j = 0 Then  ' ���������
                                                        If FormatNumber(CalculateSignbord(Query.Fields("SIGNBORD_START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2) < 200 Then
                                                                Temp_money = 200
                                                        Else
                                                                Temp_money = FormatNumber(CalculateSignbord(Query.Fields("SIGNBORD_START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
                                                        End If
                                                        If IsNull(Query.Fields("SIGNBORD_START_DATE").Value) Then
                                                                strDate = ""
                                                        Else
                                                                strDate = CvDate2(Query.Fields("SIGNBORD_START_DATE").Value)
                                                        End If
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pp1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & _
                                                                         Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ",'" & strDate & "',0", , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PP1 ( PP1_YEAR,SIGNBORD_ID,OWNERSHIP_ID,PP1_AMOUNT,PP1_GK_MONEY,PP1_SET_GK,PP1_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PP1_AMOUNT_ACCEPT) " & _
'                                                                         " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & Format$(Query.Fields("START_DATE").Value, "dd/mm/yyyy") & _
'                                                                         "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ")"
                                                Else
'                                                        Temp_money = FormatNumber(CalculateSignbord(Query.Fields("START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pp1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & _
                                                                         Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ",'" & strDate & "',1", , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PP1 ( PP1_YEAR, SIGNBORD_ID,OWNERSHIP_ID,PP1_AMOUNT,PP1_GK_MONEY,PP1_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PP1_AMOUNT_ACCEPT) " & _
'                                                                         " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                         ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & ")"
                                                End If
                                         Case "4"
                                                Globle_Connective.Execute "exec sp_insert_paytax_pba1" & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LICENSE_BOOK").Value & "','" & Query.Fields("LICENSE_NO").Value & "','" & Query.Fields("CLASS_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LICENSE_SUMTAX").Value), 0, Query.Fields("LICENSE_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(Grid_Result.Row, 4)) & "," & j & _
                                                                 ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value, , adCmdUnknown
'                                                sql_txt = "INSERT INTO PBA1 ( PBA1_YEAR, LICENSE_BOOK,LICENSE_NO ,OWNERSHIP_ID,PBA1_AMOUNT,PBA1_GK_MONEY,PBA1_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE) " & _
'                                                                 " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LICENSE_BOOK").Value & "','" & Query.Fields("LICENSE_NO").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LICENSE_SUMTAX").Value), 0, Query.Fields("LICENSE_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                 ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & ")"
                             End Select
                                Query.MoveNext
                        Loop
                        DoEvents
'                            ProgressBar1.Value = i
                End If
        End If
'Next i
'        Set Query = Globle_Connective.Execute("exec sp_search_gk " & CInt(TXT_YEAR.Text), , adCmdUnknown)
'        If Query.RecordCount > 0 Then   ' update ੾������ͧ
'                Globle_Connective.Execute "exec sp_update_gk " & CCur(FormatNumber(GK_SUMMARY, 2)) & "," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown
'        Else   ' �.�.1 �����á
'                Globle_Connective.Execute "exec sp_insert_gk " & CCur(FormatNumber(GK_SUMMARY, 2)) & "," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown
'        End If
        Call Clear_Grid
        Call Set_Grid
        Me.MousePointer = 0
        Globle_Connective.CommitTrans
        MsgBox "��èѴ����¡��������ºؤ�� " & LB_Menu.Caption & " ��Шӻ� " & TXT_YEAR.Text & " �����������ó� ", vbExclamation, "�Ӫ��ᨧ"
'        ProgressBar1.Value = 0
'                MONEY_IN_GK = Empty
        Set Rs_Chk_PBT5 = Nothing
Exit Sub
ErrInsert:
        Globle_Connective.RollbackTrans
        Set Rs_Chk_PBT5 = Nothing
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
'        ProgressBar1.Value = 0
        Me.MousePointer = 0
End Sub

Private Sub Btn_Post_Click()
        Dim sql_txt As String, strDate As String
        Dim GK_SUMMARY As Currency
        Dim j As Byte
        Dim Temp_money As Currency
        Dim Rs_Chk_PBT5 As ADODB.Recordset
'        Dim Rs_PBT5_LANDDATA As ADODB.Recordset                                   edit 17/08/2552
'        Dim Rs_PBT5_LANDDATA_NOTIC As ADODB.Recordset
'        Dim Rs_PBT5_LANDDATA_APPLY As ADODB.Recordset
        Dim intYear As Integer, intValue As Byte
        
        If TProduct = "MANAGER" Or TProduct = "MODELLER" Then
                If LenB(CheckDongleKey) = 0 Then
                        Exit Sub
                End If
        End If
        
        If LenB(Trim$(Grid_Result.TextMatrix(1, 0))) = 0 Then
                MsgBox "��辺���������ͷ���¡�����ջ�Шӻ�", vbCritical, "����͹ !"
                Exit Sub
        End If
        On Error GoTo ErrHandler
Globle_Connective.BeginTrans
GK_SUMMARY = 0
Me.MousePointer = 11
'Me.Enabled = False
ProgressBar1.max = Grid_Result.Rows - 1
If SET_GK Then
      j = 0             '�������
Else
      j = 1             '�.�.1
End If
Set Rs_Chk_PBT5 = New ADODB.Recordset
'Set Rs_PBT5_LANDDATA = New ADODB.Recordset                         edit 17/08/2552
'Set Rs_PBT5_LANDDATA_NOTIC = New ADODB.Recordset
'Set Rs_PBT5_LANDDATA_APPLY = New ADODB.Recordset
For i = 1 To Grid_Result.Rows - 1
'            Debug.Print "exec sp_paytax_post_search '" & Grid_Result.TextMatrix(i, 5) & "'," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'," & j
            Set Query = Globle_Connective.Execute("exec sp_paytax_post_search '" & Grid_Result.TextMatrix(i, 5) & "'," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'," & j, , adCmdUnknown)
            If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                    Query.Fields("LAND_ID").Properties("Optimize") = True
            End If
            If Query.RecordCount > 0 Then
                  Query.MoveFirst
                  Do While Not Query.EOF
                             sql_txt = ""
                             Temp_money = 0
                             Select Case LB_Menu.Tag
                                         Case "1", "5"                                 '���պ��ا��ͧ���
                                                intValue = (CInt(TXT_YEAR.Text) - 2549) Mod 4
                                                intYear = CInt(TXT_YEAR.Text) - intValue
                                                Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_search_paytax_count_change " & TXT_YEAR.Text & ",'" & Query.Fields("LAND_ID").Value & "','1'", , adCmdUnknown)
                                                If Rs_Chk_PBT5.RecordCount > 0 Then
                                                        Label3(2).Tag = Rs_Chk_PBT5.Fields(0).Value + 1
                                                Else
                                                        Label3(2).Tag = "0"
                                                End If
                                                If j = 0 Then ' �������
'                                                        Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_search_paytax_count_change " & TXT_YEAR.Text & ",'" & Query.Fields("LAND_ID").Value & "','1'", , adCmdUnknown)

'                                                        Set Rs_PBT5_LANDDATA = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',1", , adCmdUnknown)                        ###edit 17/08/2552
'                                                        Set Rs_PBT5_LANDDATA_NOTIC = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',2", , adCmdUnknown)
'                                                        Set Rs_PBT5_LANDDATA_APPLY = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',3", , adCmdUnknown)

'                                                        If IsNull(Rs_PBT5_LANDDATA.Fields("LAND_DATE").Value) Then
'                                                                strDate = ""
'                                                        Else
'                                                                strDate = CvDate2(Rs_PBT5_LANDDATA.Fields("LAND_DATE").Value)
'                                                        End If
'                                                        Globle_Connective.Execute "exec sp_insert_pbt5_landdata '" & Rs_PBT5_LANDDATA.Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & Rs_PBT5_LANDDATA.Fields("ZONE_BLOCK").Value & "','" & Rs_PBT5_LANDDATA.Fields("ZONETAX_ID").Value & "','" & _
'                                                                        Rs_PBT5_LANDDATA.Fields("LAND_NUMBER").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_RAWANG").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_SURVEY").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_NOTIC").Value & "','" & _
'                                                                        Rs_PBT5_LANDDATA.Fields("TAMBON_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("VILLAGE_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("STREET_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("SOI_ID").Value & "','" & _
'                                                                        Rs_PBT5_LANDDATA.Fields("LAND_TYPE").Value & "'," & Rs_PBT5_LANDDATA.Fields("LAND_A_RAI").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_A_POT").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_A_VA").Value & "," & _
'                                                                        Rs_PBT5_LANDDATA.Fields("LAND_B_RAI").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_B_POT").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_B_VA").Value & "," & Rs_PBT5_LANDDATA.Fields("FLAG_STREET").Value & "," & _
'                                                                        Rs_PBT5_LANDDATA.Fields("FLAG_ASSESS").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_PRICE").Value & "," & IIf(IsNull(Rs_PBT5_LANDDATA.Fields("LAND_SUMTAX").Value), 0, Rs_PBT5_LANDDATA.Fields("LAND_SUMTAX").Value) & "," & Rs_PBT5_LANDDATA.Fields("FLAG_PAYTAX").Value & ",'" & _
'                                                                        strDate & "','" & Rs_PBT5_LANDDATA.Fields("LAND_REMARK_DETAILS").Value & "'", , adCmdUnknown
'                                                        With Rs_PBT5_LANDDATA_NOTIC
'                                                                If .RecordCount > 0 Then
'                                                                        Do While Not .EOF
'                                                                                Globle_Connective.Execute "exec sp_insert_pbt5_landdata_notic '" & .Fields("LAND_NOTIC_ID").Value & "','" & .Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & .Fields("OWNERSHIP_ID").Value & "','" & .Fields("OWNERSHIP_NOTIC").Value & "','" & .Fields("OWNERSHIP_MAIL").Value & "'", , adCmdUnknown
'                                                                                .MoveNext
'                                                                        Loop
'                                                                End If
'                                                        End With
'                                                        With Rs_PBT5_LANDDATA_APPLY
'                                                                If .RecordCount > 0 Then
'                                                                        Do While Not .EOF
'                                                                                Globle_Connective.Execute "exec sp_insert_pbt5_landdata_apply '" & .Fields("LAND_APPLY_ID").Value & "','" & .Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & .Fields("OWNERSHIP_ID").Value & "','" & .Fields("REMARK").Value & "'," & .Fields("APPLY_RAI").Value & "," & _
'                                                                                .Fields("APPLY_POT").Value & "," & .Fields("APPLY_VA").Value & "," & IIf(IsNull(.Fields("APPLY_SUMRATE").Value), 0, .Fields("APPLY_SUMRATE").Value) & ",'" & .Fields("LM_ID").Value & "'," & .Fields("LAND_FLAG_APPLY").Value & "," & .Fields("LAND_OPTION").Value & ",'" & .Fields("ZONETAX_ID").Value & "'", , adCmdUnknown
'                                                                                .MoveNext
'                                                                        Loop
'                                                                End If
'                                                        End With
'                                                        If IsNull(Query.Fields("LAND_DATE").Value) Then
'                                                                strDate = ""
'                                                        Else
'                                                                strDate = CvDate2(Query.Fields("LAND_DATE").Value)
'                                                        End If    edit 17/08/2552 ###

'                                                        Debug.Print "exec; sp_insert_paytax_pbt5; " & CInt(TXT_YEAR.Text) & ", '" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'" & strDate & "',0" & "," & Label3(2).Tag
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'" & strDate & "',0" & "," & Label3(2).Tag, , adCmdUnknown
'                                                         sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                        " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_DATE").Value), "Null", "'" & Format$(Query.Fields("LAND_DATE").Value, "dd/mm/yyyy") & "'") & ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                Else
                                                        If intValue = 0 Then  '����㹻շ������������Ẻ����
'                                                                Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_search_paytax_count_change " & TXT_YEAR.Text & ",'" & Query.Fields("LAND_ID").Value & "','1'", , adCmdUnknown)

'                                                                Set Rs_PBT5_LANDDATA = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',1", , adCmdUnknown)     ### edit 17/08/2552
'                                                                Set Rs_PBT5_LANDDATA_NOTIC = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',2", , adCmdUnknown)
'                                                                Set Rs_PBT5_LANDDATA_APPLY = Globle_Connective.Execute("exec sp_pbt5_landdata '" & Query.Fields("LAND_ID").Value & "',3", , adCmdUnknown)
'                                                                If IsNull(Rs_PBT5_LANDDATA.Fields("LAND_DATE").Value) Then
'                                                                        strDate = ""
'                                                                Else
'                                                                        strDate = CvDate2(Rs_PBT5_LANDDATA.Fields("LAND_DATE").Value)
'                                                                End If
'                                                                Globle_Connective.Execute "exec sp_insert_pbt5_landdata '" & Rs_PBT5_LANDDATA.Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & Rs_PBT5_LANDDATA.Fields("ZONE_BLOCK").Value & "','" & Rs_PBT5_LANDDATA.Fields("ZONETAX_ID").Value & "','" & _
'                                                                                Rs_PBT5_LANDDATA.Fields("LAND_NUMBER").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_RAWANG").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_SURVEY").Value & "','" & Rs_PBT5_LANDDATA.Fields("LAND_NOTIC").Value & "','" & _
'                                                                                Rs_PBT5_LANDDATA.Fields("TAMBON_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("VILLAGE_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("STREET_ID").Value & "','" & Rs_PBT5_LANDDATA.Fields("SOI_ID").Value & "','" & _
'                                                                                Rs_PBT5_LANDDATA.Fields("LAND_TYPE").Value & "'," & Rs_PBT5_LANDDATA.Fields("LAND_A_RAI").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_A_POT").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_A_VA").Value & "," & _
'                                                                                Rs_PBT5_LANDDATA.Fields("LAND_B_RAI").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_B_POT").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_B_VA").Value & "," & Rs_PBT5_LANDDATA.Fields("FLAG_STREET").Value & "," & _
'                                                                                Rs_PBT5_LANDDATA.Fields("FLAG_ASSESS").Value & "," & Rs_PBT5_LANDDATA.Fields("LAND_PRICE").Value & "," & IIf(IsNull(Rs_PBT5_LANDDATA.Fields("LAND_SUMTAX").Value), 0, Rs_PBT5_LANDDATA.Fields("LAND_SUMTAX").Value) & "," & Rs_PBT5_LANDDATA.Fields("FLAG_PAYTAX").Value & ",'" & _
'                                                                                strDate & "','" & Rs_PBT5_LANDDATA.Fields("LAND_REMARK_DETAILS").Value & "'", , adCmdUnknown
'                                                                With Rs_PBT5_LANDDATA_NOTIC
'                                                                        If .RecordCount > 0 Then
'                                                                                Do While Not .EOF
'                                                                                        Globle_Connective.Execute "exec sp_insert_pbt5_landdata_notic '" & .Fields("LAND_NOTIC_ID").Value & "','" & .Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & .Fields("OWNERSHIP_ID").Value & "','" & .Fields("OWNERSHIP_NOTIC").Value & "','" & .Fields("OWNERSHIP_MAIL").Value & "'", , adCmdUnknown
'                                                                                        .MoveNext
'                                                                                Loop
'                                                                        End If
'                                                                End With
'                                                                With Rs_PBT5_LANDDATA_APPLY
'                                                                        If .RecordCount > 0 Then
'                                                                                Do While Not .EOF
'                                                                                        Globle_Connective.Execute "exec sp_insert_pbt5_landdata_apply '" & .Fields("LAND_APPLY_ID").Value & "','" & .Fields("LAND_ID").Value & "'," & CInt(TXT_YEAR.Text) & ",'" & .Fields("OWNERSHIP_ID").Value & "','" & .Fields("REMARK").Value & "'," & .Fields("APPLY_RAI").Value & "," & _
'                                                                                        .Fields("APPLY_POT").Value & "," & .Fields("APPLY_VA").Value & "," & IIf(IsNull(.Fields("APPLY_SUMRATE").Value), 0, .Fields("APPLY_SUMRATE").Value) & ",'" & .Fields("LM_ID").Value & "'," & .Fields("LAND_FLAG_APPLY").Value & "," & .Fields("LAND_OPTION").Value & ",'" & .Fields("ZONETAX_ID").Value & "'", , adCmdUnknown
'                                                                                        .MoveNext
'                                                                                Loop
'                                                                        End If
'                                                                End With     edit 17/08/2552 ###
                                                                Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1" & "," & Label3(2).Tag, , adCmdUnknown
'                                                                sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                        Else
'                                                                sql_txt = "SELECT PBT5_NO,PBT5_DATE,PBT5_NO_ACCEPT FROM PBT5 WHERE OWNERSHIP_ID='" & Grid_Result.TextMatrix(i, 5) & "' AND LAND_ID='" & Query.Fields("LAND_ID").Value & "'" & _
'                                                                                " AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) - 1 & " AND PBT5_NO_STATUS=1"
'                                                                Call SET_QUERY(sql_txt, Rs_Chk_PBT5)
                                                                If LB_Menu.Tag = "1" Then
                                                                        Dim str_begin_year As String, str_end_year As String
                                                                        Select Case CInt((TXT_YEAR.Text) - 2549) Mod 4
                                                                                Case 0
                                                                                            str_begin_year = CInt(TXT_YEAR.Text)
                                                                                            str_end_year = CInt(TXT_YEAR.Text) + 3
                                                                                Case 1
                                                                                            str_begin_year = CInt(TXT_YEAR.Text) - 1
                                                                                            str_end_year = CInt(TXT_YEAR.Text) + 2
                                                                                Case 2
                                                                                            str_begin_year = CInt(TXT_YEAR.Text) - 2
                                                                                            str_end_year = CInt(TXT_YEAR.Text) + 1
                                                                                Case 3
                                                                                            str_begin_year = CInt(TXT_YEAR.Text) - 3
                                                                                            str_end_year = CInt(TXT_YEAR.Text)
                                                                        End Select
                                                                        Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_pbt5_find4year '" & Grid_Result.TextMatrix(i, 5) & "'," & str_begin_year & "," & str_end_year, , adCmdUnknown)
                                                                End If
                                                               '����� OWNERSHIP_ID ����ա�����Ẻ���������ѧ
                                                                If Rs_Chk_PBT5.RecordCount > 0 Then         '��������Ẻ㹻շ������  ��� PBT5_NO ��ҡѺ PBT5_NO �ͧ�շ������
                                                                         If IsNull(Rs_Chk_PBT5.Fields(1).Value) Then
                                                                                strDate = ""
                                                                        Else
                                                                                strDate = CvDate2(Rs_Chk_PBT5.Fields(1).Value)
                                                                        End If
'                                                                        Debug.Print "exec sp_insert_paytax_pbt5_2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields(0).Value & "',1" & ",'" & strDate & "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & Label3(2).Tag
                                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5_2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                                    ",'" & Rs_Chk_PBT5.Fields(0).Value & "',1" & ",'" & strDate & "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                                    ",'" & Rs_Chk_PBT5.Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & Label3(2).Tag, , adCmdUnknown
'                                                                        sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,PBT5_NO,PBT5_NO_STATUS,PBT5_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_NO_ACCEPT,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                    " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields(0).Value & "',1" & ",'" & CStr(Rs_Chk_PBT5.Fields(1).Value) & "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                                    ",'" & Rs_Chk_PBT5.Fields("PBT5_NO_ACCEPT").Value & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                                Else
'                                                                        Debug.Print "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
'                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
'                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1" & "," & Label3(2).Tag
                                                                        Globle_Connective.Execute "exec sp_insert_paytax_pbt5 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "5", 2, j) & _
                                                                        ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & _
                                                                        "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ",'',1" & "," & Label3(2).Tag, , adCmdUnknown
'                                                                        sql_txt = "INSERT INTO PBT5 ( PBT5_YEAR, LAND_ID ,OWNERSHIP_ID,PBT5_AMOUNT,PBT5_GK_MONEY,PBT5_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PBT5_AMOUNT_ACCEPT) " & _
'                                                                                    " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LAND_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                                    ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value) & ")"
                                                                End If
                                                        End If
                                                        GK_SUMMARY = GK_SUMMARY + IIf(IsNull(Query.Fields("LAND_SUMTAX").Value), 0, Query.Fields("LAND_SUMTAX").Value)
                                                End If
                                         Case "2", "6"                              ' �����ç���͹��з��Թ
                                                Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_search_paytax_count_change " & TXT_YEAR.Text & ",'" & Query.Fields("BUILDING_ID").Value & "','2'", , adCmdUnknown)
                                                If Rs_Chk_PBT5.RecordCount > 0 Then
                                                        Label3(2).Tag = Rs_Chk_PBT5.Fields(0).Value + 1
                                                Else
                                                        Label3(2).Tag = "0"
                                                End If
                                                If j = 0 Then
                                                        Globle_Connective.Execute "exec sp_insert_paytax_prd2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "6", 2, j) & _
                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & Label3(2).Tag, , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PRD2 ( PRD2_YEAR, BUILDING_ID,OWNERSHIP_ID,PRD2_RENTYEAR_TOTAL,PRD2_GK_MONEY,PRD2_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PRD2_AMOUNT_ACCEPT) " & _
'                                                                            " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "6", 2, j) & _
'                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & ")"
                                                Else
                                                        Globle_Connective.Execute "exec sp_insert_paytax_prd2 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & Label3(2).Tag, , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PRD2 ( PRD2_YEAR, BUILDING_ID,OWNERSHIP_ID,PRD2_RENTYEAR_TOTAL,PRD2_GK_MONEY,PRD2_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PRD2_AMOUNT_ACCEPT) " & _
'                                                                            " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("BUILDING_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                            ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value) & ")"
                                                            GK_SUMMARY = GK_SUMMARY + IIf(IsNull(Query.Fields("TOTAL_TAX_EXCLUDE").Value), 0, Query.Fields("TOTAL_TAX_EXCLUDE").Value)
                                                End If
                                         Case "3", "7"                               ' ���ջ���
                                                Set Rs_Chk_PBT5 = Globle_Connective.Execute("exec sp_search_paytax_count_change " & TXT_YEAR.Text & ",'" & Query.Fields("SIGNBORD_ID").Value & "','3'", , adCmdUnknown)
                                                If Rs_Chk_PBT5.RecordCount > 0 Then
                                                        Label3(2).Tag = Rs_Chk_PBT5.Fields(0).Value + 1
                                                Else
                                                        Label3(2).Tag = "0"
                                                End If
                                                If j = 0 Then  ' ���������
                                                        If FormatNumber(CalculateSignbord(Query.Fields("SIGNBORD_START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2) < 200 Then
                                                                Temp_money = 200
                                                        Else
                                                                Temp_money = FormatNumber(CalculateSignbord(Query.Fields("SIGNBORD_START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
                                                        End If
                                                        If IsNull(Query.Fields("SIGNBORD_START_DATE").Value) Then
                                                                strDate = ""
                                                        Else
                                                                strDate = CvDate2(Query.Fields("SIGNBORD_START_DATE").Value)
                                                        End If
'                                                        Debug.Print "exec sp_insert_paytax_pp1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & _
'                                                                         Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ",'" & strDate & "',0"
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pp1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & _
                                                                         Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ",'" & strDate & "',0" & "," & Label3(2).Tag, , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PP1 ( PP1_YEAR,SIGNBORD_ID,OWNERSHIP_ID,PP1_AMOUNT,PP1_GK_MONEY,PP1_SET_GK,PP1_OWNER_DATE,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PP1_AMOUNT_ACCEPT) " & _
'                                                                         " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & Temp_money & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & Format$(Query.Fields("START_DATE").Value, "dd/mm/yyyy") & _
'                                                                         "','" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & Temp_money & ")"
                                                Else
'                                                        Temp_money = FormatNumber(CalculateSignbord(Query.Fields("START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
                                                        Globle_Connective.Execute "exec sp_insert_paytax_pp1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & "," & IIf(LB_Menu.Tag = "7", 2, j) & ",'" & _
                                                                         Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & ",'" & strDate & "',1" & "," & Label3(2).Tag, , adCmdUnknown
'                                                        sql_txt = "INSERT INTO PP1 ( PP1_YEAR, SIGNBORD_ID,OWNERSHIP_ID,PP1_AMOUNT,PP1_GK_MONEY,PP1_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE,PP1_AMOUNT_ACCEPT) " & _
'                                                                         " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("SIGNBORD_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                         ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & "," & IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value) & ")"
                                                                         GK_SUMMARY = GK_SUMMARY + IIf(IsNull(Query.Fields("SIGNBORD_SUMTAX").Value), 0, Query.Fields("SIGNBORD_SUMTAX").Value)
                                                End If
                                         Case "4"
                                                Globle_Connective.Execute "exec sp_insert_paytax_pba1 " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LICENSE_BOOK").Value & "','" & Query.Fields("LICENSE_NO").Value & "','" & Query.Fields("CLASS_ID").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LICENSE_SUMTAX").Value), 0, Query.Fields("LICENSE_SUMTAX").Value) & "," & CCur(Grid_Result.TextMatrix(i, 4)) & _
                                                                 ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value, , adCmdUnknown
'                                                sql_txt = "INSERT INTO PBA1 ( PBA1_YEAR, LICENSE_BOOK,LICENSE_NO ,OWNERSHIP_ID,PBA1_AMOUNT,PBA1_GK_MONEY,PBA1_SET_GK,OWNER_NAME,OWNER_SURNAME,PRENAME,OWNER_TYPE) " & _
'                                                                 " VALUES ( " & CInt(TXT_YEAR.Text) & ",'" & Query.Fields("LICENSE_BOOK").Value & "','" & Query.Fields("LICENSE_NO").Value & "','" & Grid_Result.TextMatrix(i, 5) & "'," & IIf(IsNull(Query.Fields("LICENSE_SUMTAX").Value), 0, Query.Fields("LICENSE_SUMTAX").Value) & "," & CDbl(Grid_Result.TextMatrix(i, 4)) & "," & j & _
'                                                                 ",'" & Query.Fields("OWNER_NAME").Value & "','" & Query.Fields("OWNER_SURNAME").Value & "','" & Query.Fields("PRENAME").Value & "'," & Query.Fields("OWNER_TYPE").Value & ")"
'                                                                GK_SUMMARY = GK_SUMMARY + Query.Fields("LICENSE_SUMTAX").Value
                             End Select
'                            SET_Execute (sql_txt)
                            Query.MoveNext
                 Loop
                            DoEvents
                            ProgressBar1.Value = i
              End If
Next i
'        Call SET_QUERY("SELECT GK_YEAR,GK_TOTAL_PBT5,GK_TOTAL_PRD2,GK_TOTAL_PP1,GK_TOTAL_PBA1 FROM GK WHERE GK_YEAR=" & CInt(TXT_YEAR.Text), Query)
        Set Query = Globle_Connective.Execute("exec sp_search_gk " & CInt(TXT_YEAR.Text), , adCmdUnknown)
        If Query.RecordCount > 0 Then   ' update ੾������ͧ
                Globle_Connective.Execute "exec sp_update_gk " & CCur(FormatNumber(GK_SUMMARY, 2)) & "," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown
'                sql_txt = Empty
'                Select Case LB_Menu.Tag
'                            Case "1"
'                                        If Query.Fields(1).Value = 0 Then sql_txt = "UPDATE GK  SET  GK_TOTAL_PBT5 = " & CCur(FormatNumber(GK_SUMMARY, 2)) & " WHERE GK_YEAR = " & TXT_YEAR.Text
'                            Case "2"
'                                        If Query.Fields(2).Value = 0 Then sql_txt = "UPDATE GK  SET  GK_TOTAL_PRD2 = " & CCur(FormatNumber(GK_SUMMARY, 2)) & " WHERE GK_YEAR = " & TXT_YEAR.Text
'                            Case "3"
'                                        If Query.Fields(3).Value = 0 Then sql_txt = "UPDATE GK  SET  GK_TOTAL_PP1 = " & CCur(FormatNumber(GK_SUMMARY, 2)) & " WHERE GK_YEAR = " & TXT_YEAR.Text
'                            Case "4"
'                                        If Query.Fields(4).Value = 0 Then sql_txt = "UPDATE GK  SET  GK_TOTAL_PBA1 = " & CCur(FormatNumber(GK_SUMMARY, 2)) & " WHERE GK_YEAR = " & TXT_YEAR.Text
'                End Select
'                If sql_txt <> Empty Then SET_Execute (sql_txt)
        Else   ' �.�.1 �����á
                Globle_Connective.Execute "exec sp_insert_gk " & CCur(FormatNumber(GK_SUMMARY, 2)) & "," & CInt(TXT_YEAR.Text) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown
        End If
'        Me.Enabled = True
        Call Clear_Grid
        Call Set_Grid
        Me.MousePointer = 0
        Globle_Connective.CommitTrans
        MsgBox "��èѴ����¡������ " & LB_Menu.Caption & " ��Шӻ� " & TXT_YEAR.Text & " �����������ó� ", vbExclamation, "�Ӫ��ᨧ"
        ProgressBar1.Value = 0
        Set Rs_Chk_PBT5 = Nothing
'        Set Rs_PBT5_LANDDATA = Nothing   ### edit 17/08/2552
'        Set Rs_PBT5_LANDDATA_NOTIC = Nothing
'        Set Rs_PBT5_LANDDATA_APPLY = Nothing   edit 17/08/2552 ###
Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Set Rs_Chk_PBT5 = Nothing
'        Set Rs_PBT5_LANDDATA = Nothing  ### edit 17/08/2552
'        Set Rs_PBT5_LANDDATA_NOTIC = Nothing
'        Set Rs_PBT5_LANDDATA_APPLY = Nothing edit 17/08/2552 ###
        ProgressBar1.Value = 0
'        Me.Enabled = True
        Me.MousePointer = 0
End Sub

Private Sub Btn_Search_Click()
Dim cmd As ADODB.command
On Error GoTo ErrSearch
Dim i As Integer
'        Globle_Connective.BeginTrans
        Me.MousePointer = 11
        Set cmd = New ADODB.command
        Select Case LB_Menu.Tag
                     Case "1"  '����շ�Ѿ���Թ����㹢����������պ��ا��ͧ���
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '',0", , adCmdUnknown)
'                            Call SET_QUERY("SELECT MAX(PBT5_YEAR),MIN(PBT5_YEAR) FROM PBT5", Query)
                            If Query.RecordCount > 0 Then
                                    If CInt(TXT_YEAR.Text) < Query.Fields(1).Value Or CInt(TXT_YEAR.Text) > Query.Fields(0).Value + 1 Then
                                            MsgBox "�շ�����͡�������ö�Ӻѭ�ա.�.1�� ��س����͡�����١��ͧ����", vbCritical, "����͹"
                                            Me.MousePointer = 0
'                                            Globle_Connective.RollbackTrans
                                            Exit Sub
                                    End If
                            End If
'                            cmd.ActiveConnection = Globle_Connective
'                            cmd.CommandType = adCmdUnknown
'                            cmd.CommandText = "exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',1"
'                            cmd.Prepared = True
'                            Set Query = cmd.Execute
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',1", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PBT5_YEAR FROM PBT5 WHERE PBT5_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then
                                    SET_GK = True                        'True  ��ҡѺ��÷Ӻѭ��(�������)
'                                    cmd.CommandText = "sp_search_paytax"
'                                    cmd.CommandType = adCmdStoredProc
'                                    cmd.ActiveConnection = Globle_Connective
'                                    ' Refresh the parameters collection and populate it
'                                    cmd.Parameters.Refresh
'                                    cmd.Parameters(1).Value = "PBT5"
'                                    cmd.Parameters(2).Value = CInt(TXT_YEAR.Text)
'                                    cmd.Parameters(3).Value = 0
''                                    cmd.CommandText = "exec sp_search_paytax 'PBT5'," & CInt(TXT_YEAR.Text) & ",0"
'                            cmd.Prepared = True
'                            Set Query = cmd.Execute
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PBT5'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LAND_ID), FORMAT$(SUM(LAND_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID FROM" & _
'                                                    " (SELECT OWNERSHIP_REAL_ID, PRENAME,OWNER_NAME,OWNER_SURNAME, ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME,AMPHOE_NAME,PROVINCE_NAME,B.LAND_ID,LAND_SUMTAX,B.OWNERSHIP_ID" & _
'                                                    " FROM OWNERSHIP AS A, LANDDATA_NOTIC AS B, LANDDATA AS C" & _
'                                                    " Where B.LAND_ID = C.LAND_ID And B.OWNERSHIP_ID = A.OWNERSHIP_ID And FLAG_PAYTAX = 1 And OWNERSHIP_NOTIC = 1 AND B.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS" & _
'                                                    " (SELECT OWNERSHIP_ID FROM PBT5 WHERE PBT5.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) & ")) AS AAA" & _
'                                                    " WHERE LAND_ID NOT IN (SELECT LAND_ID FROM PBT5 WHERE PBT5_SET_GK=0 AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) & " AND AAA.OWNERSHIP_ID=PBT5.OWNERSHIP_ID)" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
                            Else
                                    SET_GK = False                     'False  ��ҡѺ��÷Ӻѭ�ա.�.1
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PBT5'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LAND_ID), FORMAT$(SUM(LAND_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID" & _
'                                                    " FROM OWNERSHIP AS A, LANDDATA_NOTIC AS B, LANDDATA AS C " & _
'                                                    " WHERE B.LAND_ID = C.LAND_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC = 1" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, ID_GARD, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                            AND NOT EXISTS (SELECT OWNERSHIP_ID FROM PBT5 WHERE A.OWNERSHIP_ID=PBT5.OWNERSHIP_ID AND PBT5_YEAR = " & CInt(TXT_YEAR.Text) & " AND PBT5_SET_GK  = 1 )
                            End If
                    Case "2"    '����շ�Ѿ���Թ����㹢������������ç���͹��з��Թ (������������)
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',2", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PRD2_YEAR FROM PRD2 WHERE PRD2_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then
                                    SET_GK = True
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PRD2'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    MsgBox "�Ѵ����¡�úѭ�����ջ��� ��Шӻվ.�." & TXT_YEAR.Text & " ���º��������", vbOKOnly, "�š�èѴ��"
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.BUILDING_ID), FORMAT$(SUM(TOTAL_TAX_EXCLUDE),'##,###,###0.00##'),B.OWNERSHIP_ID" & _
'                                                    " FROM OWNERSHIP AS A, BUILDINGDATA_NOTIC AS B, BUILDINGDATA AS C , LANDDATA AS D " & _
'                                                    " WHERE B.BUILDING_ID = C.BUILDING_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND D.LAND_ID = C.LAND_ID AND C.FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC = 1 AND 1=2" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
                            Else
                                    SET_GK = False
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PRD2'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.BUILDING_ID), FORMAT$(SUM(TOTAL_TAX_EXCLUDE),'##,###,###0.00##'),B.OWNERSHIP_ID" & _
'                                                    " FROM OWNERSHIP AS A, BUILDINGDATA_NOTIC AS B, BUILDINGDATA AS C , LANDDATA AS D " & _
'                                                    " WHERE B.BUILDING_ID = C.BUILDING_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND D.LAND_ID = C.LAND_ID AND C.FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC = 1" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                            AND NOT EXISTS (SELECT OWNERSHIP_ID FROM PRD2 WHERE A.OWNERSHIP_ID=PRD2.OWNERSHIP_ID AND PRD2_YEAR = " & CInt(TXT_YEAR.Text) & " AND PRD2_SET_GK  = 1 )
                            End If
                    Case "3"    '����շ�Ѿ���Թ����㹢����������ջ���
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',3", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PP1_YEAR FROM PP1 WHERE PP1_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then
                                    SET_GK = True
'                                    Debug.Print "exec sp_search_paytax 'PP1'," & CInt(TXT_YEAR.Text) & ",0"
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PP1'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.SIGNBORD_ID), FORMAT$(SUM(SIGNBORD_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID FROM" & _
'                                                    " (SELECT OWNERSHIP_REAL_ID, PRENAME,OWNER_NAME,OWNER_SURNAME, ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME,AMPHOE_NAME,PROVINCE_NAME,B.SIGNBORD_ID,SIGNBORD_SUMTAX,B.OWNERSHIP_ID" & _
'                                                    " FROM OWNERSHIP AS A, SIGNBORDDATA_NOTIC AS B, SIGNBORDDATA AS C" & _
'                                                    " Where B.SIGNBORD_ID = C.SIGNBORD_ID And FLAG_PAYTAX = 1 And OWNERSHIP_NOTIC = 1 AND B.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS" & _
'                                                    " (SELECT OWNERSHIP_ID FROM PP1 WHERE PP1.OWNERSHIP_ID=B.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_YEAR=" & CInt(TXT_YEAR.Text) & ")) AS AAA" & _
'                                                    " WHERE SIGNBORD_ID NOT IN (SELECT SIGNBORD_ID FROM PP1 WHERE PP1_SET_GK=0 AND PP1_YEAR=" & CInt(TXT_YEAR.Text) & " AND AAA.OWNERSHIP_ID=PP1.OWNERSHIP_ID)" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
                            Else
                                    SET_GK = False
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PP1'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.SIGNBORD_ID), FORMAT$(SUM(SIGNBORD_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID" & _
'                                            " FROM OWNERSHIP AS A, SIGNBORDDATA_NOTIC AS B, SIGNBORDDATA AS C , LANDDATA AS D " & _
'                                            " WHERE B.SIGNBORD_ID = C.SIGNBORD_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND D.LAND_ID = C.LAND_ID AND C.FLAG_PAYTAX = 1 AND OWNERSHIP_NOTIC = 1" & _
'                                            " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                                            AND NOT EXISTS (SELECT OWNERSHIP_ID FROM PP1 WHERE A.OWNERSHIP_ID= PP1.OWNERSHIP_ID AND PP1_YEAR = " & CInt(TXT_YEAR.Text) & " AND PP1_SET_GK  = 1 )
                            End If
                    Case "4"    '����շ�Ѿ���Թ����㹢��������͹حҵ
                            Set Query = Globle_Connective.Execute("exec sp_search_paytax 'PBA1'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                            sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LICENSE_BOOK),FORMAT$(SUM(LICENSE_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID"
'                            sql_txt = sql_txt & " FROM OWNERSHIP AS A, LICENSEDATA AS B"
'                            sql_txt = sql_txt & " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID  AND FLAG_STATUS = 1 AND NOT EXISTS(SELECT * FROM  (SELECT * FROM PBA1 WHERE PBA1_YEAR=" & CInt(TXT_YEAR.Text) & ") WHERE PBA1.LICENSE_BOOK=B.LICENSE_BOOK AND PBA1.LICENSE_NO=B.LICENSE_NO) "
'                            sql_txt = sql_txt & " GROUP BY OWNERSHIP_REAL_ID,PRENAME,OWNER_NAME,OWNER_SURNAME,OWNER_NUMBER,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME,AMPHOE_NAME,PROVINCE_NAME,B.OWNERSHIP_ID"
                    Case "5"  '���պ��ا��ͧ���(�������)
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '',0", , adCmdUnknown)
'                            Call SET_QUERY("SELECT MAX(PBT5_YEAR),MIN(PBT5_YEAR) FROM PBT5", Query)
                            If Query.RecordCount > 0 And IsNull(Query.Fields(0).Value) = False Then
                                    If CInt(TXT_YEAR.Text) < Query.Fields(1).Value Or CInt(TXT_YEAR.Text) > Query.Fields(0).Value + 2 Then
                                            MsgBox "�շ�����͡�������ö�Ӻѭ�ա.�.1�� ��س����͡�����١��ͧ����", vbCritical, "����͹"
                                            Me.MousePointer = 0
'                                            Globle_Connective.RollbackTrans
                                            Exit Sub
                                    End If
                            End If
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',1", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PBT5_YEAR FROM PBT5 WHERE PBT5_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then      '����ҡ���� 0 �ʴ�����ա�õ�駡.�.1����
                                    SET_GK = True
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PBT5_OTHER'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    Call SET_Execute("DELETE FROM PBT5_OTHER")
'                                    Call SET_Execute("INSERT INTO PBT5_OTHER SELECT DISTINCT LAND_ID FROM PBT5 WHERE PBT5_YEAR=" & CInt(TXT_YEAR.Text))
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LAND_ID), FORMAT$(SUM(LAND_SUMTAX),'##,###,###0.00##'),A.OWNERSHIP_ID FROM" & _
'                                                   " LANDDATA_NOTIC AS A, LANDDATA AS B,OWNERSHIP D" & _
'                                                   " WHERE A.LAND_ID=B.LAND_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.LAND_ID  FROM PBT5_OTHER B WHERE A.LAND_ID=B.LAND_ID) AND FLAG_PAYTAX=1" & _
'                                                " AND EXISTS(SELECT * FROM PBT5 WHERE  D.OWNERSHIP_ID=PBT5.OWNERSHIP_ID AND PBT5_YEAR=" & CInt(TXT_YEAR.Text) & ")" & _
'                                                   " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, A.OWNERSHIP_ID"
                            Else
                                    SET_GK = False
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PBT5_OTHER'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
                                    sql_txt = "SELECT OWNER_NAME FROM PBT5 WHERE 1=2"
                            End If
                    Case "6"  '�����ç���͹��з��Թ(�������)
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',2", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PRD2_YEAR FROM PRD2 WHERE PRD2_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then
                                    SET_GK = True
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PRD2_OTHER'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    Call SET_Execute("DELETE FROM PRD2_OTHER")
'                                    Call SET_Execute("INSERT INTO PRD2_OTHER SELECT DISTINCT BUILDING_ID FROM PRD2 WHERE PRD2_YEAR=" & CInt(TXT_YEAR.Text))
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.BUILDING_ID), FORMAT$(SUM(TOTAL_TAX_EXCLUDE),'##,###,###0.00##'),D.OWNERSHIP_ID" & _
'                                                    " FROM BUILDINGDATA_NOTIC AS A,BUILDINGDATA AS B,OWNERSHIP D WHERE A.BUILDING_ID=B.BUILDING_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.BUILDING_ID  FROM PRD2_OTHER B WHERE A.BUILDING_ID=B.BUILDING_ID) AND FLAG_PAYTAX=1" & _
'                                                    " AND EXISTS(SELECT * FROM PRD2 WHERE D.OWNERSHIP_ID=PRD2.OWNERSHIP_ID AND PRD2_YEAR=" & CInt(TXT_YEAR.Text) & " )" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID,PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, D.OWNERSHIP_ID"
                            Else
                                    SET_GK = False
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PRD2_OTHER'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNER_NAME FROM PRD2 WHERE 1=2"
                            End If
                    Case "7"  '���ջ���(�������)
                            Set Query = Globle_Connective.Execute("exec sp_exists_year '" & CInt(TXT_YEAR.Text) & "',3", , adCmdUnknown)
'                            sql_txt = "SELECT DISTINCT PP1_YEAR FROM PP1 WHERE PP1_YEAR = " & CInt(TXT_YEAR.Text)
'                            Call SET_QUERY(sql_txt, Query)
                            If Query.RecordCount > 0 Then
                                    SET_GK = True
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PP1_OTHER'," & CInt(TXT_YEAR.Text) & ",0", , adCmdUnknown)
'                                    Call SET_Execute("DELETE FROM PP1_OTHER")
'                                    Call SET_Execute("INSERT INTO PP1_OTHER SELECT DISTINCT SIGNBORD_ID FROM PP1 WHERE PP1_YEAR=" & CInt(TXT_YEAR.Text))
'                                    sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.SIGNBORD_ID), FORMAT$(SUM(SIGNBORD_SUMTAX),'##,###,###0.00##'),D.OWNERSHIP_ID FROM" & _
'                                                    " SIGNBORDDATA_NOTIC AS A, SIGNBORDDATA AS B,OWNERSHIP D WHERE A.SIGNBORD_ID=B.SIGNBORD_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.SIGNBORD_ID FROM PP1 B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) AND FLAG_PAYTAX=1" & _
'                                                    " AND EXISTS(SELECT * FROM PP1 WHERE D.OWNERSHIP_ID=PP1.OWNERSHIP_ID AND PP1_YEAR=" & CInt(TXT_YEAR.Text) & ")" & _
'                                                    " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, D.OWNERSHIP_ID"
                            Else
                                    SET_GK = False
                                    Set Query = Globle_Connective.Execute("exec sp_search_paytax_other 'PP1_OTHER'," & CInt(TXT_YEAR.Text) & ",1", , adCmdUnknown)
'                                    sql_txt = "SELECT OWNER_NAME FROM PP1 WHERE 1=2"
                            End If
        End Select
'        sql_txt = sql_txt & " Order by OWNER_NAME "
'        Call SET_QUERY(sql_txt, Query)
        
        If Query.RecordCount > 0 Then
                Set Grid_Result.DataSource = Query
                Call Set_Grid
                Btn_Post.Caption = "�Ѵ����¡�� : �ӹǹ " & Query.RecordCount & " ���"
                Me.MousePointer = 0
        Else
                Call Clear_Grid
                Call Set_Grid
                MsgBox "����բ���������Ѻ����赡��ҧ�����������", vbInformation, "�Ӫ��ᨧ ?"
                Me.MousePointer = 0
        End If
'        Globle_Connective.CommitTrans
        Exit Sub
ErrSearch:
'        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        MsgBox Err.Description
        Exit Sub
End Sub

Private Sub Btn_SearchOwnerShip_PBT5_Click()
        On Error GoTo ErrSearch
'        Globle_Connective.BeginTrans
        If LenB(Trim$(Text1.Text)) = 0 Then
'                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        Dim str1 As String, str2 As String
        Dim i As Integer, Temp_money As Currency, n As Integer

        Me.MousePointer = 11
'        Select Case LB_Menu.Tag
'                Case "1"
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LAND_ID), FORMAT$(SUM(LAND_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID"
'                        sql_txt = sql_txt & " FROM OWNERSHIP AS A, LANDDATA_NOTIC AS B, LANDDATA AS C "
'                        sql_txt = sql_txt & " WHERE B.LAND_ID = C.LAND_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC = 1 AND B.OWNERSHIP_ID NOT IN (SELECT OWNERSHIP_ID FROM PBT5 WHERE PBT5_YEAR = " & CInt(TXT_YEAR.Text) & " AND PBT5_SET_GK  = 1 ) "
'                        sql_txt = sql_txt & " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, ID_GARD, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                Case "2"
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.BUILDING_ID), FORMAT$(SUM(TOTAL_TAX_EXCLUDE),'##,###,###0.00##'),B.OWNERSHIP_ID"
'                        sql_txt = sql_txt & " FROM OWNERSHIP AS A, BUILDINGDATA_NOTIC AS B, BUILDINGDATA AS C ,LANDDATA D "
'                        sql_txt = sql_txt & " WHERE B.BUILDING_ID = C.BUILDING_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND D.LAND_ID = C.LAND_ID AND C.FLAG_PAYTAX  = 1 AND OWNERSHIP_NOTIC = 1 AND B.OWNERSHIP_ID NOT IN (SELECT OWNERSHIP_ID FROM PRD2 WHERE PRD2_YEAR = " & CInt(TXT_YEAR.Text) & " AND PRD2_SET_GK  = 1 ) "
'                        sql_txt = sql_txt & " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                Case "3"
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.SIGNBORD_ID), FORMAT$(SUM(SIGNBORD_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID"
'                        sql_txt = sql_txt & " FROM OWNERSHIP AS A, SIGNBORDDATA_NOTIC AS B, SIGNBORDDATA AS C ,LANDDATA D "
'                        sql_txt = sql_txt & " WHERE B.SIGNBORD_ID = C.SIGNBORD_ID AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND D.LAND_ID = C.LAND_ID AND C.FLAG_PAYTAX = 1 AND OWNERSHIP_NOTIC = 1 AND B.OWNERSHIP_ID NOT IN (SELECT OWNERSHIP_ID FROM PP1 WHERE PP1_YEAR = " & CInt(TXT_YEAR.Text) & " AND PP1_SET_GK  = 1 ) "
'                        sql_txt = sql_txt & " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                Case "4"
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LICENSE_BOOK),FORMAT$(SUM(LICENSE_SUMTAX),'##,###,###0.00##'),B.OWNERSHIP_ID"
'                        sql_txt = sql_txt & " FROM OWNERSHIP AS A, LICENSEDATA AS B"
'                        sql_txt = sql_txt & " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID  AND FLAG_STATUS = 1 AND B.OWNERSHIP_ID NOT IN (SELECT OWNERSHIP_ID FROM PBA1 WHERE PBA1_YEAR = " & CInt(TXT_YEAR.Text) & " AND PBA1_SET_GK  = 1 ) "
'                        sql_txt = sql_txt & " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, B.OWNERSHIP_ID"
'                Case "5"
'                        Call SET_Execute("DELETE FROM PBT5_OTHER")
'                        Call SET_Execute("INSERT INTO PBT5_OTHER SELECT DISTINCT LAND_ID FROM PBT5 WHERE PBT5_YEAR=" & CInt(TXT_YEAR.Text))
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.LAND_ID), FORMAT$(SUM(LAND_SUMTAX),'##,###,###0.00##'),A.OWNERSHIP_ID FROM" & _
'                                       " LANDDATA_NOTIC AS A, LANDDATA AS B,OWNERSHIP D" & _
'                                       " WHERE A.LAND_ID=B.LAND_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.LAND_ID  FROM PBT5_OTHER B WHERE A.LAND_ID=B.LAND_ID) AND FLAG_PAYTAX=1" & _
'                                       " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, A.OWNERSHIP_ID"
'                Case "6"
'                        Call SET_Execute("DELETE FROM PRD2_OTHER")
'                        Call SET_Execute("INSERT INTO PRD2_OTHER SELECT DISTINCT BUILDING_ID FROM PRD2 WHERE PRD2_YEAR=" & CInt(TXT_YEAR.Text))
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.BUILDING_ID), FORMAT$(SUM(TOTAL_TAX_EXCLUDE),'##,###,###0.00##'),D.OWNERSHIP_ID" & _
'                                        " FROM BUILDINGDATA_NOTIC AS A, BUILDINGDATA AS B,OWNERSHIP D WHERE A.BUILDING_ID=B.BUILDING_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.BUILDING_ID  FROM PRD2_OTHER B WHERE A.BUILDING_ID=B.BUILDING_ID) AND FLAG_PAYTAX=1" & _
'                                        " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER, ADD_HOME, ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, D.OWNERSHIP_ID"
'                Case "7"
'                        Call SET_Execute("DELETE FROM PP1_OTHER")
'                        Call SET_Execute("INSERT INTO PP1_OTHER SELECT DISTINCT SIGNBORD_ID FROM PP1 WHERE PP1_YEAR=" & CInt(TXT_YEAR.Text))
'                        sql_txt = "SELECT OWNERSHIP_REAL_ID, PRENAME & ' ' & OWNER_NAME & '   ' & OWNER_SURNAME, ADD_HOME & ' ����.' & ADD_MOO & ' �.-' & ADD_SOI & ' �.' & ADD_ROAD & ' �.' & TAMBON_NAME & ' �.' & AMPHOE_NAME & ' �.' & PROVINCE_NAME,  COUNT(B.SIGNBORD_ID), FORMAT$(SUM(SIGNBORD_SUMTAX),'##,###,###0.00##'),D.OWNERSHIP_ID FROM" & _
'                                        " SIGNBORDDATA_NOTIC AS A, SIGNBORDDATA AS B,OWNERSHIP D WHERE A.SIGNBORD_ID=B.SIGNBORD_ID AND D.OWNERSHIP_ID=A.OWNERSHIP_ID AND NOT EXISTS(SELECT B.SIGNBORD_ID FROM PP1 B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) AND FLAG_PAYTAX=1" & _
'                                        " GROUP BY OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TAMBON_NAME, AMPHOE_NAME, PROVINCE_NAME, D.OWNERSHIP_ID"
'        End Select
'                sql_txt = Empty
                n = InStr(1, Text1.Text, " ", vbTextCompare)
                If n <> 0 Then
                        str1 = Left$(Text1.Text, n - 1)   '����
                        str2 = Trim$(Mid$(Text1.Text, n, Len(Text1.Text))) '���ʡ��
                Else
                        str1 = Text1.Text
                End If
                str1 = " HAVING OWNER_NAME LIKE ''" & str1 & "%''"
                If LenB(Trim$(str2)) > 0 Then str1 = str1 & " AND OWNER_SURNAME LIKE ''" & str2 & "%''"
                str1 = str1 & " Order by OWNER_NAME "
                Set Query = Globle_Connective.Execute("exec sp_exists_year '" & TXT_YEAR.Text & "','1'", , adCmdUnknown)
                If Query.RecordCount > 0 Then
                        
                Else
                        Set Query = Globle_Connective.Execute("exec sp_search_paytax_ownership '" & TXT_YEAR.Text & "','" & str1 & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
                End If
'        Call SET_QUERY(sql_txt, Query)
        Me.MousePointer = 0
            If Query.RecordCount > 0 Then
                Set Grid_Result.DataSource = Query
                        Call Set_Grid
                        Btn_Post.Caption = "�Ѵ����¡�� : �ӹǹ " & CLng(Query.RecordCount) & " ���"
                        If LB_Menu.Tag = "3" Then '�óդӹǳ�Թ������¨ҡ�ѹ���Դ���  ************************************************
                                For i = 1 To Grid_Result.Rows - 1
'                                        sql_txt = "SELECT Format(SIGNBORD_START_DATE,'dd/mm/yyyy') AS START_DATE , A.SIGNBORD_ID , SIGNBORD_SUMTAX , B.OWNERSHIP_ID as OWNERSHIP_ID FROM  SIGNBORDDATA A , SIGNBORDDATA_NOTIC B,LANDDATA C  Where B.SIGNBORD_ID = A.SIGNBORD_ID AND C.LAND_ID = A.LAND_ID AND A.FLAG_PAYTAX  = 1 AND  B.OWNERSHIP_ID = '" & Grid_Result.TextMatrix(i, 5) & "'"
'                                        Call SET_QUERY(sql_txt, Query)
                                        Set Query = Globle_Connective.Execute("exec sp_paytax_signboard_tax '" & Grid_Result.TextMatrix(i, 5) & "'", , adCmdUnknown)
                                                Temp_money = 0
                                                Query.MoveFirst
                                        Do While Not Query.EOF
                                                Temp_money = Temp_money + FormatNumber(CalculateSignbord(Query.Fields("SIGNBORD_START_DATE").Value, Query.Fields("SIGNBORD_SUMTAX").Value), 2)
                                                Query.MoveNext
                                        Loop
                                        Grid_Result.TextMatrix(i, 4) = Temp_money
                                Next i
                        End If
'                        sql_txt = "Select * FROM GK WHERE GK_YEAR = " & CInt(TXT_YEAR.Text)
'                        Call SET_QUERY(sql_txt, Query)
                        Set Query = Globle_Connective.Execute("exec sp_search_gk " & CInt(TXT_YEAR.Text), , adCmdUnknown)
                        If Query.RecordCount > 0 Then
                                SET_GK = True    'True ��ҡѺ��÷Ӻѭ�ռ����������������
                                Select Case LB_Menu.Tag
                                        Case "1"
                                                    If Query.Fields("GK_TOTAL_PBT5").Value > 0 Then
                                                            MONEY_IN_GK = "UNUPDATE"
                                                    Else
                                                            MONEY_IN_GK = "UPDATE"
                                                    End If
                                        Case "2"
                                                    If Query.Fields("GK_TOTAL_PRD2").Value > 0 Then
                                                            MONEY_IN_GK = "UNUPDATE"
                                                    Else
                                                            MONEY_IN_GK = "UPDATE"
                                                    End If
                                        Case "3"
                                                    If Query.Fields("GK_TOTAL_PP1").Value > 0 Then
                                                            MONEY_IN_GK = "UNUPDATE"
                                                    Else
                                                            MONEY_IN_GK = "UPDATE"
                                                    End If
                                        Case "4"
                                                    If Query.Fields("GK_TOTAL_PBA1").Value > 0 Then
                                                            MONEY_IN_GK = "UNUPDATE"
                                                    Else
                                                            MONEY_IN_GK = "UPDATE"
                                                    End If
                                End Select
                        Else
                                SET_GK = False    'False ��ҡѺ��÷Ӻѭ�ռ��������դ����á
                        End If
            Else
                Call Clear_Grid
                Call Set_Grid
                MsgBox "����բ���������Ѻ�������ҡ���駻����Թ ", vbInformation, "�Ӫ��ᨧ ?"
            End If
'            Globle_Connective.CommitTrans
            Me.MousePointer = 0
            Exit Sub
ErrSearch:
'            Globle_Connective.RollbackTrans
            MsgBox Err.Description
            Me.MousePointer = 0
End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
        Call Set_Grid
        If Frm_PayTax.LB_Menu.Tag = "4" Then
                    Shape2(0).Visible = False
                    OptionChkType(0).Visible = False
                    OptionChkType(1).Visible = False
        Else
                    Shape2(0).Visible = True
                    OptionChkType(0).Visible = True
                    OptionChkType(1).Visible = True
        End If
End Sub

Private Sub Form_Load()
        Set Query = New ADODB.Recordset
        TXT_YEAR.Text = Right$(Date, 4)
        Call Clear_Grid
        Call Set_Grid
        SET_GK = True
'SendMessage ProgressBar1.hWnd, PBM_SETBARCOLOR, 0&, ByVal &HFF00&
'MONEY_IN_GK = ""
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
        Set Frm_PayTax = Nothing
End Sub

Private Sub LB_Menu_Change()
        Call Clear_Grid
        Call Set_Grid
End Sub

Private Sub OptionChkType_Click(Index As Integer)
        If Index = 0 Then
                Select Case LB_Menu.Tag
                            Case "5"
                                        Frm_PayTax.LB_Menu.Tag = "1"
                                        Frm_PayTax.LB_Menu.Caption = "���շ�ͧ���"
                            Case "6"
                                        Frm_PayTax.LB_Menu.Tag = "2"
                                        Frm_PayTax.LB_Menu.Caption = "�����ç���͹��з��Թ"
                            Case "7"
                                        Frm_PayTax.LB_Menu.Tag = "3"
                                        Frm_PayTax.LB_Menu.Caption = "���ջ���"
                End Select
        Else
                Select Case LB_Menu.Tag
                            Case "1"
                                        Frm_PayTax.LB_Menu.Tag = "5"
                                        Frm_PayTax.LB_Menu.Caption = "���շ�ͧ��� (�������)"
                            Case "2"
                                        Frm_PayTax.LB_Menu.Tag = "6"
                                        Frm_PayTax.LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
                            Case "3"
                                        Frm_PayTax.LB_Menu.Tag = "7"
                                        Frm_PayTax.LB_Menu.Caption = "���ջ��� (�������)"
                End Select
        End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call Btn_SearchOwnerShip_PBT5_Click
End Sub

Private Sub TXT_YEAR_Change()
        Call Clear_Grid
End Sub

Private Sub TXT_YEAR_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub
