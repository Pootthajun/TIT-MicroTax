VERSION 5.00
Begin VB.Form Frm_Login 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   3000
   ClientLeft      =   60
   ClientTop       =   165
   ClientWidth     =   6000
   ControlBox      =   0   'False
   Icon            =   "Frm_Login.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Login.frx":151A
   ScaleHeight     =   200
   ScaleMode       =   0  'User
   ScaleWidth      =   399.002
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Txt_Password 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      IMEMode         =   3  'DISABLE
      Left            =   2310
      MaxLength       =   14
      PasswordChar    =   "�"
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2400
      Width           =   2475
   End
   Begin VB.TextBox Txt_UserName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   2310
      MaxLength       =   14
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1980
      Width           =   2475
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   14
      Left            =   2280
      Top             =   1950
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   2280
      Top             =   2370
      Width           =   2535
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Height          =   285
      Left            =   5670
      TabIndex        =   4
      ToolTipText     =   "Exit Program"
      Top             =   60
      Width           =   285
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Checking Status ..."
      ForeColor       =   &H00404040&
      Height          =   195
      Left            =   4530
      TabIndex        =   3
      Top             =   2760
      Visible         =   0   'False
      Width           =   1350
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0000FFFF&
      BackStyle       =   0  'Transparent
      Height          =   195
      Left            =   2955
      TabIndex        =   2
      Top             =   2670
      Width           =   75
   End
End
Attribute VB_Name = "Frm_Login"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset

Private Sub Form_Activate()
        Txt_UserName.SetFocus
End Sub

'Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
 'CTRL Shift RETURN 2
 'ALT     Shift RETURN 4
 'Shift      Shift RETURN 0 OR 1
'If Shift = 2 And KeyCode = 65 Then
 '   MsgBox Shift & "---" & KeyCode
'End If
'End Sub

Private Sub Form_Load()
        Set Query = New ADODB.Recordset
        Call SET_QUERY("Select  *  From  USER_PASSWORD ", Query)
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
        Set Frm_Login = Nothing
End Sub

Private Sub Label3_Click()
        Call SET_QUERY("Select  *  From  USER_PASSWORD ", Query)
        If Query.RecordCount > 0 Then
                Call DETERMINE_FORM(Clone_Form.Name, True)
                Unload Clone_Form
                Unload Me
        Else
                Unload Me
        End If
End Sub

Private Sub Txt_Password_Change()
        Label2.Visible = False
End Sub

Private Sub Txt_Password_GotFocus()
        Txt_Password.SelStart = 0
        Txt_Password.SelLength = Len(Txt_Password.Text)
End Sub

Private Sub Txt_Password_KeyPress(KeyAscii As Integer)
        If KeyAscii = 9 Then
                With Txt_UserName
                        .SetFocus
                        .SelStart = 0
                        .SelLength = Len(.Text)
                End With
                Exit Sub
        End If
        If KeyAscii <> 13 Then
                KeyAscii = KeyCharecter(KeyAscii, Character)
                Exit Sub
        End If
        If Len(Txt_UserName.Text) >= 4 And Len(Txt_Password.Text) >= 4 Then
                        Label2.Visible = True
                        If LenB(Trim$(Frm_Login.Tag)) <> 0 Then
                                If UCase$(Frm_Login.Tag) <> UCase$(Trim$(Txt_UserName.Text)) Then
                                        Label2.Caption = "User name Incorrect !"
                                        Txt_UserName.SetFocus
                                        Exit Sub
                                End If
                        End If
                        
                        Dim Buffer_Password As String
                        Buffer_Password = Enzinc_Password(Txt_Password.Text, "ENCODE")
                        Call SET_QUERY("Select  *  From  USER_PASSWORD Where User_Login = '" & Trim$(Txt_UserName.Text) & "' And User_Password  =   '" & Trim$(Buffer_Password) & "'", Query)
                        If Query.RecordCount > 0 Then
                '                If Query.Fields("User_Status").Value = "GIS" Then
                '
                '                Else
                '
                '                End If
                                Clone_Form.StatusBar1.Panels(1).Text = "Status : " & Query.Fields("User_Status").Value 'edit pass
                                Clone_Form.StatusBar1.Panels(2).Text = "User Login : " & Query.Fields("User_Login").Value 'edit pass
                                strUser = Query.Fields("User_Login").Value
                                Call Set_Menu(strUser)
                                Unload Me
                        Else
                             'Label2.ForeColor = &HC0&
                             Label2.Caption = "Password Incorrect !"
                             Txt_Password.SetFocus
                        End If
                        
          End If
End Sub

Private Sub Txt_UserName_Change()
        Label2.Visible = False
End Sub

Private Sub Txt_UserName_GotFocus()
        Txt_UserName.SelStart = 0
        Txt_UserName.SelLength = Len(Txt_UserName.Text)
End Sub

Private Sub Txt_UserName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 9 Then
                With Txt_Password
                        .SetFocus
                        .SelStart = 0
                        .SelLength = Len(.Text)
                End With
                Exit Sub
        End If
        If KeyAscii <> 13 Then
            KeyAscii = KeyCharecter(KeyAscii, Character)
            Exit Sub
        Else
            Call Txt_Password_KeyPress(13)
        End If
End Sub

Private Sub Set_Menu(ByVal UserName As String)
        On Error GoTo ErrsetMenu
        Dim rs As ADODB.Recordset
        Set rs = New ADODB.Recordset
        With Clone_Form
                Call SET_QUERY("SELECT KEY_ID,MENU_FLAG,RELETIVE_KEY FROM MENU_SETTING WHERE USER_LOGIN='" & UserName & "'", rs)
                If rs.RecordCount > 0 Then
                        rs.MoveFirst
                        Do While Not rs.EOF
                                Select Case Trim$(rs.Fields(0).Value)
                                        Case "M0001"  '�����ž�鹰ҹ
                                                .keyIndex.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(1).Enabled = rs.Fields(1).Value
                                        Case "M0002" '����¹��Ѿ���Թ
                                                .keyAccess.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(2).Enabled = rs.Fields(1).Value
                                        Case "M0003" '����ѵԷ���¹��Ѿ���Թ
                                                .keyEditAccess.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(3).Enabled = rs.Fields(1).Value
                                        Case "M0004" '���Ẻ�����Թ
                                                .keyContact.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(4).Enabled = rs.Fields(1).Value
                                                .Lb_menuLeft(5).Enabled = rs.Fields(1).Value
                                        Case "M0005" '�ͺ���������
                                                .keyQuery.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(6).Enabled = rs.Fields(1).Value
                                                If rs.Fields(1).Value = False Then
                                                        Clone_Form.Picture10.Enabled = False
                                                        Clone_Form.Picture100.Enabled = False
                                                End If
                                        Case "M0006" '��§ҹ��ػ
                                                .keyReport.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(7).Enabled = rs.Fields(1).Value
                                        Case "M0007" '�к��Ǻ���
                                                .keyControl.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(9).Enabled = rs.Fields(1).Value
                                                If rs.Fields(1).Value = False Then
                                                        Clone_Form.Picture111.Enabled = False
                                                        Clone_Form.Picture12.Enabled = False
                                                End If
                                        Case "M0008" '�к���������
                                                .keyAnalyse.Visible = rs.Fields(1).Value
                                                .Lb_menuLeft(8).Enabled = rs.Fields(1).Value
                                        Case "M0002-01" '����ա����Է���
                                                If .keyAccess.Visible = True Then
                                                        .Lb_Reg(1).Enabled = rs.Fields(1).Value
                                                        .keyOwnerShip.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0002-01-01" '����,���,ź
                                                With Frm_Ownership
                                                        .Btn_Add.Visible = rs.Fields(1).Value
                                                        .Btn_Edit.Visible = rs.Fields(1).Value
                                                        .Btn_Cancel.Visible = rs.Fields(1).Value
                                                        .Btn_Del.Visible = rs.Fields(1).Value
                                                        .Btn_Post.Visible = rs.Fields(1).Value
                                                        .Btn_Search.Visible = rs.Fields(1).Value
                                                End With
                                        Case "M0002-02" '����¹���Թ
                                                If .keyAccess.Visible = True Then
                                                        .Lb_Reg(2).Enabled = rs.Fields(1).Value
                                                        .keyLandData.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0002-03" '����¹�ç���͹
                                                If .keyAccess.Visible = True Then
                                                        .Lb_Reg(3).Enabled = rs.Fields(1).Value
                                                        .keyBulidingData.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0002-04" '����¹����
                                                If .keyAccess.Visible = True Then
                                                        .Lb_Reg(4).Enabled = rs.Fields(1).Value
                                                        .keySignbordData.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0002-05" '����¹�͹حҵ
                                                If .keyAccess.Visible = True Then
                                                        .Lb_Reg(5).Enabled = rs.Fields(1).Value
                                                        .keyWorkLicense.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-01" '���ҷ���¹��Ѿ���Թ
                                                If .keyQuery.Visible = True Then
                                                        .Lb_Find(1).Enabled = rs.Fields(1).Value
                                                        .keyQueryOwner.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-02" '���ҷ���¹���Թ
                                                If .keyQuery.Visible = True Then
                                                        .Lb_Find(2).Enabled = rs.Fields(1).Value
                                                        .keyQueryLand.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-03" '���ҷ���¹�ç���͹
                                                If .keyQuery.Visible = True Then
                                                        .Lb_Find(3).Enabled = rs.Fields(1).Value
                                                        .keyQueryBuild.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-04" '���ҷ���¹����
                                                If .keyQuery.Visible = True Then
                                                        .Lb_Find(4).Enabled = rs.Fields(1).Value
                                                        .keyQuerySingBord.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-05" '���ҷ���¹�͹حҵ
                                                If .keyQuery.Visible = True Then
                                                        .Lb_Find(5).Enabled = rs.Fields(1).Value
                                                        .keyQuerySale.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-06" 'Ẻ���Ǩ��з���¹
                                                If .keyQuery.Visible = True Then
                                                        .keySurvey.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-07" 'Ẻ��§ҹ��û�Ѻ������
                                                If .keyQuery.Visible = True Then
                                                        .keyAdjust.Visible = rs.Fields(1).Value
                                                End If
                                        Case "M0005-08"  '����ѵԡ�ê�������
                                                If .keyQuery.Visible = True Then
                                                        .keyHistory.Visible = rs.Fields(1).Value
                                                Else
                                                        Clone_Form.Picture6.Enabled = False
                                                End If
                                        Case "M0005-09"  '����ѵԡ�ê�������Ẻ�Ǵ
                                                If .keyQuery.Visible = True Then
                                                        .keyHistoryInstallment.Visible = rs.Fields(1).Value
                                                End If
                                End Select
                                rs.MoveNext
                        Loop
                End If
        End With
        Exit Sub
ErrsetMenu:
        MsgBox Err.Description
End Sub
