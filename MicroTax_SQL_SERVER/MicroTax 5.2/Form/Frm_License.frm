VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_License 
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12885
   ControlBox      =   0   'False
   Icon            =   "Frm_License.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_License.frx":151A
   ScaleHeight     =   9030
   ScaleWidth      =   12885
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cb_License_Change_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9360
      Style           =   2  'Dropdown List
      TabIndex        =   84
      Top             =   30
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ComboBox cb_License_Change 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4260
      Style           =   2  'Dropdown List
      TabIndex        =   83
      Top             =   900
      Visible         =   0   'False
      Width           =   8595
   End
   Begin VB.PictureBox picParent 
      BackColor       =   &H006E6B68&
      Height          =   2145
      Left            =   10560
      ScaleHeight     =   2085
      ScaleWidth      =   2235
      TabIndex        =   81
      Top             =   1260
      Width           =   2295
      Begin VB.PictureBox picPicture 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   2085
         Left            =   0
         MouseIcon       =   "Frm_License.frx":104AA
         ScaleHeight     =   2085
         ScaleWidth      =   2235
         TabIndex        =   82
         Top             =   0
         Width           =   2235
      End
      Begin VB.Image ImgSize 
         Height          =   1305
         Left            =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.CommandButton Btn_SelectImg 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_License.frx":107B4
      Style           =   1  'Graphical
      TabIndex        =   80
      Top             =   2790
      Width           =   345
   End
   Begin VB.CommandButton Btn_FullScreen 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_License.frx":10D3E
      Style           =   1  'Graphical
      TabIndex        =   79
      Top             =   3090
      Width           =   345
   End
   Begin VB.ComboBox Cmb_LAND_ID 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3390
      TabIndex        =   74
      Top             =   7620
      Width           =   1605
   End
   Begin VB.CheckBox Chk_STATUS_LINK0 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   71
      Top             =   8580
      Width           =   195
   End
   Begin VB.CheckBox Chk_STATUS_LINK1 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   70
      Top             =   7770
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_STATUS_LINK2 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   69
      Top             =   8160
      Width           =   195
   End
   Begin VB.CheckBox Chk_FLAG_STATUS0 
      Enabled         =   0   'False
      Height          =   195
      Left            =   8310
      TabIndex        =   62
      Top             =   4440
      Width           =   195
   End
   Begin VB.CheckBox Chk_FLAG_STATUS1 
      Enabled         =   0   'False
      Height          =   195
      Left            =   8310
      TabIndex        =   61
      Top             =   4050
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.TextBox Txt_LICENSE_SUMTAX 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   5910
      Locked          =   -1  'True
      TabIndex        =   60
      Text            =   "0.00"
      Top             =   6150
      Width           =   1275
   End
   Begin VB.TextBox Txt_LICENSE_REMARK 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   58
      Text            =   "-"
      Top             =   5730
      Width           =   6615
   End
   Begin VB.TextBox Txt_StoryRemark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   150
      TabIndex        =   56
      Text            =   "-"
      Top             =   90
      Visible         =   0   'False
      Width           =   4185
   End
   Begin VB.ComboBox CMB_CLASS_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6030
      TabIndex        =   55
      Top             =   3960
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.ComboBox CMB_BUSINESS_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6000
      TabIndex        =   54
      Top             =   3420
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.ComboBox CMB_CLASS_DETAILS 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1020
      TabIndex        =   5
      Top             =   5250
      Width           =   6705
   End
   Begin VB.ComboBox CMB_BUSINESS_TYPE 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1050
      TabIndex        =   4
      Top             =   4800
      Width           =   6705
   End
   Begin VB.ComboBox Cmb_Building_ID 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3390
      TabIndex        =   10
      Top             =   8100
      Width           =   1605
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1050
      TabIndex        =   9
      Top             =   7200
      Width           =   1335
   End
   Begin VB.TextBox Txt_License_Fax 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   10260
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   8550
      Width           =   2325
   End
   Begin VB.TextBox Txt_License_Tel 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7080
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   8550
      Width           =   1965
   End
   Begin VB.TextBox Txt_LICENSE_AREA 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      TabIndex        =   8
      Text            =   "0"
      Top             =   6120
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_ENGINEER 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      MaxLength       =   10
      TabIndex        =   7
      Text            =   "0"
      Top             =   5700
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_EMPLOYEE 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   6
      Text            =   "0"
      Top             =   5280
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_NO 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3420
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   2
      Top             =   4020
      Width           =   1305
   End
   Begin VB.TextBox Txt_LICENSE_BOOK 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   1
      Top             =   4020
      Width           =   1305
   End
   Begin VB.TextBox Txt_LICENSE_NAME 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "-"
      Top             =   4440
      Width           =   6615
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":112C8
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_License.frx":11B2F
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_License.frx":14426
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_License.frx":14CBD
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":175A2
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_License.frx":17E05
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_License.frx":1A82A
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_License.frx":1B0DC
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":1DB17
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_License.frx":1E406
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":20F61
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_License.frx":2178F
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_License.frx":24019
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_License.frx":24882
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin MSComCtl2.DTPicker DTP_LICENSE_DATE 
      Height          =   330
      Left            =   11190
      TabIndex        =   13
      Top             =   3990
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_License.frx":2707A
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   67698689
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComCtl2.DTPicker DTP_LICENSE_ENDDATE 
      Height          =   330
      Left            =   11190
      TabIndex        =   66
      Top             =   4350
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_License.frx":27394
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   67698689
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComctlLib.ListView LstV_Img_Name 
      Height          =   2145
      Left            =   8550
      TabIndex        =   76
      Top             =   1260
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   3784
      View            =   3
      Arrange         =   2
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�ٻ����"
         Object.Width           =   2752
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Ѿ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   20
      Left            =   6210
      TabIndex        =   78
      Top             =   8580
      Width           =   720
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����кط����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   34
      Left            =   570
      TabIndex        =   77
      Top             =   8580
      Width           =   915
   End
   Begin VB.Label TMP_LAND_ID 
      Height          =   285
      Left            =   5040
      TabIndex        =   75
      Top             =   8130
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   26
      Left            =   10230
      Top             =   8100
      Width           =   2385
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   25
      Left            =   10230
      Top             =   7650
      Width           =   2385
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   24
      Left            =   7050
      Top             =   8100
      Width           =   2025
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   23
      Left            =   7050
      Top             =   7650
      Width           =   2025
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   35
      Left            =   2550
      TabIndex        =   73
      Top             =   7710
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�кط���駷��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   29
      Left            =   540
      TabIndex        =   72
      Top             =   7710
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�кط�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   28
      Left            =   540
      TabIndex        =   68
      Top             =   8160
      Width           =   1395
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ʶҹ��͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   27
      Left            =   8790
      TabIndex        =   67
      Top             =   3510
      Width           =   1785
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ¡��ԡ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   26
      Left            =   10380
      TabIndex        =   65
      Top             =   4410
      Width           =   705
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "¡��ԡ�͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   25
      Left            =   8670
      TabIndex        =   64
      Top             =   4410
      Width           =   1275
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   23
      Left            =   8670
      TabIndex        =   63
      Top             =   4020
      Width           =   915
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   975
      Index           =   22
      Left            =   7980
      Top             =   3870
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1035
      Index           =   21
      Left            =   7950
      Top             =   3840
      Width           =   4875
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   20
      Left            =   1050
      Top             =   5700
      Width           =   6675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   24
      Left            =   180
      TabIndex        =   59
      Top             =   5760
      Width           =   765
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   19
      Left            =   1410
      Top             =   2040
      Width           =   3285
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   705
      Index           =   18
      Left            =   1410
      Top             =   2550
      Width           =   6825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   15
      Left            =   1410
      Top             =   1530
      Width           =   6825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   14
      Left            =   5880
      Top             =   6120
      Width           =   1335
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*���˵ػ�Ѻ����¹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   107
      Left            =   2520
      TabIndex        =   57
      Top             =   960
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҹ�������                           �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   22
      Left            =   4680
      TabIndex        =   53
      Top             =   6150
      Width           =   3015
   End
   Begin VB.Label Lb_Owner_ID 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   150
      TabIndex        =   52
      Top             =   510
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   17
      Left            =   10230
      Top             =   8520
      Width           =   2385
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   16
      Left            =   7050
      Top             =   8520
      Width           =   2025
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   21
      Left            =   9480
      TabIndex        =   51
      Top             =   8580
      Width           =   645
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   19
      Left            =   2250
      TabIndex        =   50
      Top             =   8160
      Width           =   1065
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����⫹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   18
      Left            =   240
      TabIndex        =   49
      Top             =   7320
      Width           =   705
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   33
      Left            =   6585
      TabIndex        =   48
      Top             =   8160
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   32
      Left            =   9750
      TabIndex        =   47
      Top             =   8160
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� (�����)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   31
      Left            =   9180
      TabIndex        =   46
      Top             =   7710
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   30
      Left            =   6510
      TabIndex        =   45
      Top             =   7710
      Width           =   435
   End
   Begin VB.Label Lb_Tambon 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   7080
      TabIndex        =   44
      Top             =   7680
      Width           =   1965
   End
   Begin VB.Label Lb_Village 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   10260
      TabIndex        =   43
      Top             =   7680
      Width           =   2325
   End
   Begin VB.Label Lb_Soi 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   7080
      TabIndex        =   42
      Top             =   8130
      Width           =   1965
   End
   Begin VB.Label Lb_Street 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   10260
      TabIndex        =   41
      Top             =   8130
      Width           =   2355
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ŷ����ʶҹ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   17
      Left            =   480
      TabIndex        =   40
      Top             =   6690
      Width           =   1380
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1905
      Index           =   7
      Left            =   60
      Top             =   7050
      Width           =   12705
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2145
      Index           =   8
      Left            =   30
      Top             =   1260
      Width           =   8505
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   11
      Left            =   9900
      Top             =   6090
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   10
      Left            =   9900
      Top             =   5670
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   9
      Left            =   9900
      Top             =   5250
      Width           =   1695
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѡɳ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   345
      TabIndex        =   39
      Top             =   5370
      Width           =   585
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   285
      TabIndex        =   38
      Top             =   4920
      Width           =   645
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1665
      Index           =   6
      Left            =   7950
      Top             =   4950
      Width           =   4875
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1605
      Index           =   5
      Left            =   7980
      Top             =   4980
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   4
      Left            =   3390
      Top             =   3990
      Width           =   1365
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   1050
      Top             =   3990
      Width           =   1365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   14
      Left            =   2790
      TabIndex        =   37
      Top             =   4080
      Width           =   420
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   540
      TabIndex        =   36
      Top             =   4080
      Width           =   420
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�.�.�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   12
      Left            =   12000
      TabIndex        =   35
      Top             =   6150
      Width           =   465
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ç���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   11
      Left            =   12000
      TabIndex        =   34
      Top             =   5730
      Width           =   525
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   10
      Left            =   12000
      TabIndex        =   33
      Top             =   5310
      Width           =   240
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ�͡"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   10380
      TabIndex        =   32
      Top             =   4020
      Width           =   540
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��鹷���Сͺ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   8280
      TabIndex        =   31
      Top             =   6120
      Width           =   1350
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ѧ����ͧ�ѡ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   7
      Left            =   8280
      TabIndex        =   30
      Top             =   5700
      Width           =   1215
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ���ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   8280
      TabIndex        =   29
      Top             =   5280
      Width           =   1095
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   705
      TabIndex        =   28
      Top             =   4500
      Width           =   225
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ʶҹ��Сͺ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   270
      TabIndex        =   27
      Top             =   3510
      Width           =   1845
   End
   Begin VB.Label Lb_Owner_Add 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   645
      Left            =   1440
      TabIndex        =   26
      Top             =   2580
      Width           =   6780
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   2
      Left            =   915
      TabIndex        =   25
      Top             =   2580
      Width           =   345
   End
   Begin VB.Label Lb_Owner_Type 
      Alignment       =   2  'Center
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1440
      TabIndex        =   24
      Top             =   2070
      Width           =   3225
   End
   Begin VB.Label Lb_Owner_Name 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1440
      TabIndex        =   23
      Top             =   1560
      Width           =   6765
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   690
      TabIndex        =   22
      Top             =   1590
      Width           =   570
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   270
      TabIndex        =   21
      Top             =   2100
      Width           =   990
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   1050
      Top             =   4410
      Width           =   6675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�â��͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   330
      TabIndex        =   20
      Top             =   960
      Width           =   1620
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   4
      Left            =   30
      Top             =   930
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   0
      Left            =   30
      Top             =   3450
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2775
      Index           =   2
      Left            =   30
      Top             =   3840
      Width           =   7875
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   1
      Left            =   60
      Top             =   6660
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2715
      Index           =   1
      Left            =   60
      Top             =   3870
      Width           =   7815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2085
      Index           =   12
      Left            =   60
      Top             =   1290
      Width           =   8445
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1965
      Index           =   13
      Left            =   30
      Top             =   7020
      Width           =   12765
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   2
      Left            =   8520
      Top             =   3450
      Width           =   2325
   End
End
Attribute VB_Name = "Frm_License"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Status As String

Private Sub SET_REFRESH()
   GBQueryZoneTax.Requery
   GBQueryBusiness.Requery
   GBQueryClass.Requery
   
If GBQueryBusiness.RecordCount > 0 Then
   CMB_BUSINESS_TYPE.Clear
   CMB_BUSINESS_ID.Clear
    GBQueryBusiness.MoveFirst
    Do While Not GBQueryBusiness.EOF
           CMB_BUSINESS_TYPE.AddItem GBQueryBusiness.Fields("�������Ԩ���").Value
           CMB_BUSINESS_ID.AddItem GBQueryBusiness.Fields("���ʻ������Ԩ���").Value
           GBQueryBusiness.MoveNext
    Loop
End If
         Cmb_Zoneblock.Clear
         GBQueryZone.Filter = "ZONE_ID <> '' "
If GBQueryZone.RecordCount > 0 Then
            GBQueryZone.MoveFirst
    Do While Not GBQueryZone.EOF
                  If LenB(Trim$(GBQueryZone.Fields("ZONE_BLOCK").Value)) > 0 Then
                    Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
    Loop
End If
End Sub

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
    Btn_Search.Enabled = False:     Btn_Refresh.Enabled = False
    Btn_SelectImg.Enabled = True: Btn_FullScreen.Enabled = True
    CMB_BUSINESS_TYPE.Enabled = True
    CMB_CLASS_DETAILS.Enabled = True
    Cmb_Zoneblock.Enabled = True
    Cmb_Building_ID.Enabled = True
    Cmb_Land_ID.Enabled = True
    
    Txt_LICENSE_BOOK.Locked = False:                             Txt_LICENSE_BOOK.BackColor = &H80000005
    Txt_LICENSE_NO.Locked = False:                                   Txt_LICENSE_NO.BackColor = &H80000005
    Txt_LICENSE_NAME.Locked = False:                               Txt_LICENSE_NAME.BackColor = &H80000005
    Txt_LICENSE_EMPLOYEE.Locked = False:                    Txt_LICENSE_EMPLOYEE.BackColor = &H80000005
    Txt_LICENSE_ENGINEER.Locked = False:                     Txt_LICENSE_ENGINEER.BackColor = &H80000005
    Txt_LICENSE_AREA.Locked = False:                                Txt_LICENSE_AREA.BackColor = &H80000005
    Txt_License_Tel.Locked = False:                                        Txt_License_Tel.BackColor = &H80000005
    Txt_License_Fax.Locked = False:                                        Txt_License_Fax.BackColor = &H80000005
    Txt_LICENSE_REMARK.Locked = False:                             Txt_LICENSE_REMARK.BackColor = &H80000005
    Txt_LICENSE_SUMTAX.Locked = False:                              Txt_LICENSE_SUMTAX.BackColor = &H80000005
    Lb_Owner_Name.BackColor = &H80000005
    
    Chk_FLAG_STATUS0.Enabled = True
    Chk_FLAG_STATUS1.Enabled = True
    Chk_STATUS_LINK0.Enabled = True
    Chk_STATUS_LINK1.Enabled = True
    Chk_STATUS_LINK2.Enabled = True
    
    If STATE = "EDIT" Then
           Txt_LICENSE_BOOK.Locked = True:                             Txt_LICENSE_BOOK.BackColor = &HEBEBE7
           Txt_LICENSE_NO.Locked = True:                                   Txt_LICENSE_NO.BackColor = &HEBEBE7
            
            Txt_StoryRemark.Visible = True
            Label2(107).Visible = True
            Shape3(0).BackColor = &HC0C000
            Shape3(1).BackColor = &HC0C000
            Shape3(2).BackColor = &HC0C000
            Shape3(4).BackColor = &HC0C000
            cb_License_Change_ID.Clear
            cb_License_Change.Clear
            With GBQueryLicenseChange
                    .Requery
                    If .RecordCount > 0 Then
                        .MoveFirst
                        Do While Not .EOF
                              cb_License_Change_ID.AddItem .Fields("CHANGE_ID").Value
                              cb_License_Change.AddItem .Fields("CHANGE_DETAILS").Value
                              .MoveNext
                        Loop
                    End If
            End With
            cb_License_Change.Visible = True
            cb_License_Change.ListIndex = 0
            cb_License_Change_ID.ListIndex = 0
    End If
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                           Lb_Owner_ID.Caption = ""
                           CMB_BUSINESS_TYPE.Text = ""
                           CMB_CLASS_DETAILS.Text = ""
                           Cmb_Zoneblock.Text = ""
                           Cmb_Building_ID.Text = ""
                           DTP_LICENSE_DATE.Value = Now
                           Txt_LICENSE_BOOK.Text = ""
                            Txt_LICENSE_NO.Text = ""
                            Txt_LICENSE_NAME.Text = ""
                            Txt_LICENSE_EMPLOYEE.Text = ""
                            Txt_LICENSE_ENGINEER.Text = ""
                            Txt_LICENSE_AREA.Text = ""
                            Txt_License_Tel.Text = ""
                            Txt_License_Fax.Text = ""
                            Lb_Owner_Name.Caption = ""
                            Lb_Owner_Type.Caption = ""
                            Lb_Owner_Add.Caption = ""
                            Lb_Tambon.Caption = ""
                            Lb_Village.Caption = ""
                            Lb_Soi.Caption = ""
                            Lb_Street.Caption = ""
                            Txt_LICENSE_SUMTAX.Text = ""
                            Txt_LICENSE_REMARK.Text = ""
                            TMP_LAND_ID.Caption = ""
                            Cmb_Land_ID.Text = ""
                            LstV_Img_Name.ListItems.Clear
                            picPicture.Picture = LoadPicture("")
                            picPicture.Width = 2235
                            picPicture.Height = 2085
                            picPicture.Left = 0
                            cb_License_Change.Visible = False
    If STATE <> "ADD" Then
        Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
        Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
        Btn_Search.Enabled = True:      Btn_Refresh.Enabled = True
        Btn_SelectImg.Enabled = False: Btn_FullScreen.Enabled = False
    Txt_LICENSE_BOOK.Locked = True:                             Txt_LICENSE_BOOK.BackColor = &HEBEBE7
    Txt_LICENSE_NO.Locked = True:                                   Txt_LICENSE_NO.BackColor = &HEBEBE7
    Txt_LICENSE_NAME.Locked = True:                               Txt_LICENSE_NAME.BackColor = &HEBEBE7
    Txt_LICENSE_EMPLOYEE.Locked = True:                    Txt_LICENSE_EMPLOYEE.BackColor = &HEBEBE7
    Txt_LICENSE_ENGINEER.Locked = True:                     Txt_LICENSE_ENGINEER.BackColor = &HEBEBE7
    Txt_LICENSE_AREA.Locked = True:                                Txt_LICENSE_AREA.BackColor = &HEBEBE7
    Txt_License_Tel.Locked = True:                                        Txt_License_Tel.BackColor = &HEBEBE7
    Txt_License_Fax.Locked = True:                                        Txt_License_Fax.BackColor = &HEBEBE7
    Txt_LICENSE_REMARK.Locked = True:                                        Txt_LICENSE_REMARK.BackColor = &HEBEBE7
    Txt_LICENSE_SUMTAX.Locked = True:                                        Txt_LICENSE_SUMTAX.BackColor = &HEBEBE7
    Lb_Owner_Name.BackColor = &HEBEBE7
    
    CMB_BUSINESS_TYPE.Enabled = False
    CMB_CLASS_DETAILS.Enabled = False
    Cmb_Zoneblock.Enabled = False
    Cmb_Building_ID.Enabled = False
    Cmb_Land_ID.Enabled = False
    
    Chk_FLAG_STATUS0.Enabled = False
    Chk_FLAG_STATUS1.Enabled = False
    Chk_STATUS_LINK0.Enabled = False
    Chk_STATUS_LINK1.Enabled = False
    Chk_STATUS_LINK2.Enabled = False
    
    Txt_StoryRemark.Visible = False
    Label2(107).Visible = False
    Shape3(0).BackColor = &HD6D6D6
    Shape3(1).BackColor = &HD6D6D6
    Shape3(2).BackColor = &HD6D6D6
    Shape3(4).BackColor = &HD6D6D6
   End If
End If
End Sub

Private Function CheckBeforPost() As Boolean
         CheckBeforPost = True
    If LenB(Trim$(Lb_Owner_ID.Caption)) = 0 Then
        MsgBox "�ô�кت��ͼ����͹حҵ !", vbCritical, "Warning !"
        CheckBeforPost = False
        Exit Function
    End If
    
    If LenB(Trim$(Txt_LICENSE_BOOK.Text)) = 0 Or LenB(Trim$(Txt_LICENSE_NO.Text)) = 0 Then
                MsgBox "�ô�к� �Ţ��� ���� ����������ú! ", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
       If LenB(Trim$(Txt_LICENSE_NAME.Text)) = 0 Then
                MsgBox "�ô�к� ����ʶҹ��Сͺ���! ", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
    If LenB(Trim$(CMB_CLASS_ID.Text)) = 0 Then
                MsgBox "�ô�к� �ѡɳ�ʶҹ��Сͺ���", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
   
   If Chk_STATUS_LINK1.Value Then
             If LenB(Trim$(Cmb_Land_ID.Text)) = 0 Then
                MsgBox "�ô�к� ���ʷ��Թ", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
             End If
   ElseIf Chk_STATUS_LINK2.Value Then
             If LenB(Trim$(Cmb_Building_ID.Text)) = 0 Then
                MsgBox "�ô�к� �����ç���͹", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
             End If
   ElseIf Chk_STATUS_LINK0.Value Then
                CheckBeforPost = True
   Else
                MsgBox "�ô�к� ����駻�Сͺ��ä��", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
   End If
End Function

Private Sub WriteHistoryFile()
Dim ROUND_COUNT As String, Temp_Change As String
Dim Flag_Change As Byte
Dim Query As ADODB.Recordset
Set Query = New ADODB.Recordset

'sql_txt = "Select * From LICENSEDATA WHERE  LICENSE_BOOK = '" & Trim$(Txt_LICENSE_BOOK.Text) & "' AND LICENSE_NO = '" & Trim$(Txt_LICENSE_NO.Text) & "'"
'Call SET_QUERY(sql_txt, Query)
Set Query = Globle_Connective.Execute("exec sp_writehistory_license_query '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO.Text) & "','" & CMB_CLASS_ID.Text & "'", , adCmdUnknown)
With Query
'            sql_txt = Empty
        If Status = "EDIT" Then
                Temp_Change = cb_License_Change_ID.Text
                Flag_Change = 4
        ElseIf Status = "DEL" Then
                Temp_Change = "002"
                Flag_Change = 8
        ElseIf Status = "ADD" Then
                Temp_Change = "001"
                Flag_Change = 8
        End If
            ROUND_COUNT = RunAutoNumber("LICENSEDATASTORY", "ROUND_COUNT", Txt_LICENSE_BOOK.Text & "/" & Txt_LICENSE_NO.Text & "/" & CMB_CLASS_ID.Text & "-0001", False, " AND LICENSE_BOOK = '" & Trim$(Txt_LICENSE_BOOK) & "' AND LICENSE_NO = '" & Trim$(Txt_LICENSE_NO.Text) & "' AND CLASS_ID = '" & CMB_CLASS_ID.Text & "'")
            Globle_Connective.Execute "exec sp_insert_licensestory '" & ROUND_COUNT & "','" & .Fields("LICENSE_BOOK").Value & "','" & .Fields("LICENSE_NO").Value & "','" & IIf(IsNull(.Fields("LICENSE_DATE").Value), "", CvDate2(.Fields("LICENSE_DATE").Value)) & "','" & _
            .Fields("LICENSE_NAME").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_ID").Value & "'," & .Fields("STATUS_LINK").Value & ",'" & .Fields("CLASS_ID").Value & "','" & .Fields("LICENSE_TEL").Value & "','" & .Fields("LICENSE_FAX").Value & "','" & _
            .Fields("OWNERSHIP_ID").Value & "'," & .Fields("LICENSE_ENGINEER").Value & "," & .Fields("LICENSE_EMPLOYEE").Value & "," & .Fields("LICENSE_AREA").Value & ",'" & _
            Trim$(Txt_StoryRemark.Text) & "','" & CvDate2(Date) & "'," & .Fields("FLAG_STATUS").Value & ",'" & IIf(IsNull(.Fields("LICENSE_ENDDATE").Value), "", CvDate2(.Fields("LICENSE_ENDDATE").Value)) & "'," & .Fields("LICENSE_SUMTAX").Value & ",'" & .Fields("LICENSE_REMARK").Value & "','" & _
            strUser & "','" & CvDate2(Now, 1) & "'," & Flag_Change & ",'" & Temp_Change & "'", , adCmdUnknown
'                                       sql_txt = "INSERT INTO LICENSEDATASTORY ( ROUND_COUNT,LICENSE_BOOK,LICENSE_NO,LICENSE_DATE,LICENSE_NAME,LAND_ID,BUILDING_ID,STATUS_LINK,CLASS_ID,LICENSE_TEL," & _
'                                                            " LICENSE_FAX, OWNERSHIP_ID , LICENSE_ENGINEER, LICENSE_EMPLOYEE ,LICENSE_AREA ,LICENSE_STORY,LICENSE_UPDATE,FLAG_STATUS,LICENSE_ENDDATE,LICENSE_SUMTAX,LICENSE_REMARK) "
'                                        sql_txt = sql_txt & " VALUES ('" & ROUND_COUNT & "','" & .Fields("LICENSE_BOOK").Value & "','"
'                                        sql_txt = sql_txt & .Fields("LICENSE_NO").Value & "','" & .Fields("LICENSE_DATE").Value & "','" & .Fields("LICENSE_NAME").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_ID").Value & "',"
'                                        sql_txt = sql_txt & .Fields("STATUS_LINK").Value & ",'" & .Fields("CLASS_ID").Value & "','" & .Fields("LICENSE_TEL").Value & "','" & .Fields("LICENSE_FAX").Value & "','" & .Fields("OWNERSHIP_ID").Value & "',"
'                                        sql_txt = sql_txt & .Fields("LICENSE_ENGINEER").Value & "," & .Fields("LICENSE_EMPLOYEE").Value & "," & .Fields("LICENSE_AREA").Value & ",'"
'                                        sql_txt = sql_txt & Trim$(Txt_StoryRemark.Text) & "','" & Date & "'," & .Fields("FLAG_STATUS").Value & "," & IIf(IsNull(.Fields("LICENSE_ENDDATE").Value), "Null", "'" & .Fields("LICENSE_ENDDATE").Value & "'") & "," & .Fields("LICENSE_SUMTAX").Value & ",'" & .Fields("LICENSE_REMARK").Value & "')"
'                                        Call SET_Execute(sql_txt)
                                    
'     update ������������ PBA1

'            sql_txt = "UPDATE PBA1 SET PBA1_AMOUNT = " & CDbl(FormatNumber(Txt_LICENSE_SUMTAX.Text, 2)) & _
'                            " WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "' AND PBA1_YEAR = " & IIf(Year(Date) < 2500, Year(Date) + 543, Year(Date) - 543) & " AND PBA1_STATUS = 0 AND PBA1_NO_STATUS = 0 "
'            Call SET_Execute(sql_txt)
               
               'MAKE FOR UPDATE ������㹵��ҧ PBA1 ��èѴ���������������¹����͡����Է��� =================================
'                sql_txt = Empty
'                sql_txt = " Select  * From PBA1 WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "'"
'                Call SET_QUERY(sql_txt, Query)
'                If .RecordCount > 0 Then
'                    sql_txt = "UPDATE PBA1 SET OWNERSHIP_ID = '" & Lb_Owner_ID.Caption & "'" & " WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "'" & _
'                                     " AND OWNERSHIP_ID = '" & .Fields("OWNERSHIP_ID").Value & "'"
'                End If
End With
Set Query = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
'Dim sql_txt As String
Dim LAND_ID  As String, Building_id As String, FlagStatus As Byte
Dim i As Integer
         If Chk_STATUS_LINK1.Value Then
                    FlagStatus = 1
                    LAND_ID = Cmb_Land_ID.Text
                    Building_id = ""
         ElseIf Chk_STATUS_LINK2.Value Then
                    FlagStatus = 2
                    Building_id = Cmb_Building_ID.Text
                    LAND_ID = TMP_LAND_ID.Caption
         Else
                    FlagStatus = 0
                    Building_id = ""
                    LAND_ID = ""
         End If
        Call SET_Execute2("exec sp_del_img_licensedata '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO) & "'", 8, False)
        If LstV_Img_Name.ListItems.Count > 0 Then
                For i = 1 To LstV_Img_Name.ListItems.Count
                        Call SET_Execute2("exec sp_insert_licensedata_image '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO) & "','" & Trim$(Txt_LICENSE_BOOK.Text) & Trim$(Txt_LICENSE_NO) & "-" & Format$(i, "##0#") & "','" & LstV_Img_Name.ListItems(i).Text & "'", 8, False)
                        Call CopyFile(LstV_Img_Name.ListItems(i).SubItems(1), iniPath_Picture & "\License\" & LstV_Img_Name.ListItems(i).Text, 0)
                Next i
        End If
        If STATE = "DEL" Or STATE = "EDIT" Then Call WriteHistoryFile
        Globle_Connective.Execute "exec sp_manage_licensedata '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO) & "','" & IIf(IsNull(DTP_LICENSE_DATE) = False, CvDate1(DTP_LICENSE_DATE), "") & "','" & _
                        Trim$(Txt_LICENSE_NAME.Text) & "','" & LAND_ID & "','" & Building_id & "'," & FlagStatus & ",'" & CMB_CLASS_ID.Text & "','" & Trim$(Txt_License_Tel.Text) & "','" & Trim$(Txt_License_Fax.Text) & "','" & _
                        Lb_Owner_ID.Caption & "'," & CInt(Txt_LICENSE_ENGINEER.Text) & "," & CInt(Txt_LICENSE_EMPLOYEE.Text) & "," & CSng(Txt_LICENSE_AREA.Text) & ",'" & Trim$(Txt_LICENSE_REMARK.Text) & "'," & CCur(Txt_LICENSE_SUMTAX.Text) & "," & CByte(Chk_FLAG_STATUS1.Value) & ",'" & _
                        IIf(IsNull(DTP_LICENSE_ENDDATE) = False, CvDate1(DTP_LICENSE_ENDDATE), "") & "','" & strUser & "','" & CvDate2(Now, 1) & "','" & STATE & "'", , adCmdUnknown
        If STATE = "ADD" Then Call WriteHistoryFile
        Call SET_REFRESH
End Sub

'Private Sub DTC_Business_Type_KeyPress(KeyAscii As Integer)
'        KeyAscii = 0
'End Sub

Private Sub Btn_Add_Click()
        Call SET_TEXTBOX("ADD", "Perpose")
        Status = "ADD"
End Sub

Private Sub Btn_Cancel_Click()
        Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If LenB(Trim$(Lb_Owner_ID.Caption)) = 0 Then Exit Sub
If MsgBox("�׹�ѹ���ź�������͹حҵ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
Status = "DEL"
Globle_Connective.BeginTrans
            On Error GoTo ErrHandler
                    Call SET_DATABASE("DEL")
                    Call SET_TEXTBOX("DEL")
                    Globle_Connective.CommitTrans
                    Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Edit_Click()
If LenB(Trim$(Lb_Owner_ID.Caption)) = 0 Then Exit Sub
Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
End Sub

Private Sub Btn_FullScreen_Click()
        If LstV_Img_Name.ListItems.Count > 0 Then
            GB_ImagePath = LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1)
            Frm_FocusImage.Show vbModal
        End If
End Sub

Private Sub Btn_Post_Click()
If CheckBeforPost = False Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE(Status)
        Call SET_TEXTBOX("POST")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "Warning!"
End Sub

Private Sub Btn_Refresh_Click()
  Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
 With Frm_Search
      .ZOrder
      .Show
      .Frame1.Visible = False
        .Shp_Menu(1).BackColor = &HD6D6D6
        .Shp_Menu(2).BackColor = &HD6D6D6
        .Shp_Menu(3).BackColor = &HD6D6D6
        .Shp_Menu(4).BackColor = &HD6D6D6
        .Shp_Menu(5).BackColor = &HB8CFD6
        .SSTab1.Tab = 4
        .optCode.Visible = False
        .optName.Visible = False
        .cmd_SendToEdit.Caption = " << ��䢢����� - �͹حҵ"
End With
End Sub

Private Sub Btn_SelectImg_Click()
Dim Picture_Path  As String
    With Clone_Form.CommonDialog1
            .DialogTitle = "���͡�ٻ�Ҿ�͹حҵ��Сͺ�Ԩ���"
            .CancelError = True
              On Error GoTo ErrHandler
            .InitDir = "C:\"
            .Flags = cdlOFNHideReadOnly
            .Filter = "All Files (*.*)|*.*|Picture Files(*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif"     ' Set filters
            .FilterIndex = 2
            '.Action = 1
            .ShowOpen
             Picture_Path = .FileName
    End With
              If LenB(Trim$(Picture_Path)) > 0 Then
                 Call ShowPicture(Picture_Path)
           End If
                Set itmX = LstV_Img_Name.ListItems.Add()
                       itmX.SmallIcon = Clone_Form.ImageList1.ListImages(2).Index
                       Dim i As Byte, File_Name As String
                       For i = 0 To Len(Picture_Path)
                             If Mid$(Picture_Path, Len(Picture_Path) - i, 1) <> "\" Then
                                 File_Name = File_Name + Mid$(Picture_Path, Len(Picture_Path) - i, 1)
                              Else
                                  File_Name = StrReverse(File_Name)
                                  Exit For
                             End If
                       Next i
                       itmX.Text = File_Name
                       itmX.SubItems(1) = Picture_Path
                       Exit Sub
ErrHandler:
  picPicture.Cls
  MsgBox Err.Description
End Sub

Private Sub cb_License_Change_Click()
        cb_License_Change_ID.ListIndex = cb_License_Change.ListIndex
End Sub

Private Sub Chk_FLAG_STATUS0_Click()
If Chk_FLAG_STATUS0.Value Then
   Chk_FLAG_STATUS1.Value = Unchecked
   DTP_LICENSE_DATE.Enabled = False
    DTP_LICENSE_ENDDATE.Enabled = True
Else
  Chk_FLAG_STATUS1.Value = Checked
End If
End Sub

Private Sub Chk_FLAG_STATUS1_Click()
If Chk_FLAG_STATUS1.Value Then
   Chk_FLAG_STATUS0.Value = Unchecked
    DTP_LICENSE_ENDDATE.Enabled = False
    DTP_LICENSE_DATE.Enabled = True
Else
  Chk_FLAG_STATUS0.Value = Checked
End If
End Sub

Private Sub Chk_STATUS_LINK0_Click()
If Chk_STATUS_LINK0.Value Then
   Chk_STATUS_LINK1.Value = Unchecked
   Chk_STATUS_LINK2.Value = Unchecked
   Cmb_Building_ID.Enabled = False
   Cmb_Land_ID.Enabled = False
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
End If
End Sub

Private Sub Chk_STATUS_LINK1_Click()
If Chk_STATUS_LINK1.Value Then
   Chk_STATUS_LINK0.Value = Unchecked
   Chk_STATUS_LINK2.Value = Unchecked
   Cmb_Land_ID.Enabled = True
   Cmb_Building_ID.Enabled = False
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
Else
      Cmb_Land_ID.Text = ""
      Cmb_Land_ID.Enabled = False
End If
End Sub

Private Sub Chk_STATUS_LINK2_Click()
If Chk_STATUS_LINK2.Value Then
   Chk_STATUS_LINK0.Value = Unchecked
   Chk_STATUS_LINK1.Value = Unchecked
   Cmb_Building_ID.Enabled = True
   Cmb_Land_ID.Enabled = False
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
            TMP_LAND_ID.Caption = ""
Else
    Cmb_Building_ID.Text = ""
    Cmb_Building_ID.Enabled = False
End If
End Sub

Private Sub Cmb_Building_ID_Click()
         TMP_LAND_ID.Caption = ""
If GBQueryBuildingData.RecordCount > 0 Then
         GBQueryBuildingData.MoveFirst
         GBQueryBuildingData.Find " BUILDING_ID = '" & Cmb_Building_ID.Text & "'", , adSearchForward
         Lb_Tambon.Caption = GBQueryBuildingData.Fields("TAMBON_NAME").Value
         Lb_Village.Caption = GBQueryBuildingData.Fields("VILLAGE_NAME").Value
         Lb_Soi.Caption = GBQueryBuildingData.Fields("SOI_NAME").Value
         Lb_Street.Caption = GBQueryBuildingData.Fields("STREET_NAME").Value
         TMP_LAND_ID.Caption = GBQueryBuildingData.Fields("LAND_ID").Value
End If
End Sub

Private Sub Cmb_Building_ID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub CMB_BUSINESS_TYPE_Click()
   Txt_LICENSE_SUMTAX.Text = ""
   CMB_BUSINESS_ID.ListIndex = CMB_BUSINESS_TYPE.ListIndex
   GBQueryClass.Filter = "BUSINESS_ID = '" & Left$(CMB_BUSINESS_ID.Text, 3) & "'"
 If GBQueryClass.RecordCount > 0 Then
     CMB_CLASS_DETAILS.Clear
     CMB_CLASS_ID.Clear
     GBQueryClass.MoveFirst
     Do While Not GBQueryClass.EOF
          CMB_CLASS_DETAILS.AddItem GBQueryClass.Fields("�ѡɳСԨ���").Value
          CMB_CLASS_ID.AddItem GBQueryClass.Fields("�ӴѺ���").Value
          GBQueryClass.MoveNext
     Loop
     CMB_CLASS_DETAILS.ListIndex = 0
 End If
End Sub

Private Sub CMB_BUSINESS_TYPE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub CMB_CLASS_DETAILS_Click()
    Txt_LICENSE_SUMTAX.Text = ""
     CMB_CLASS_ID.ListIndex = CMB_CLASS_DETAILS.ListIndex
 If GBQueryClass.RecordCount > 0 Then
    GBQueryClass.MoveFirst
    GBQueryClass.Find "�ӴѺ��� = '" & CMB_CLASS_ID.Text & "'", , adSearchForward
    Txt_LICENSE_SUMTAX.Text = GBQueryClass.Fields(2).Value
 End If
End Sub

Private Sub CMB_CLASS_DETAILS_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub Cmb_Land_ID_Click()
If Len(Cmb_Land_ID.Text) >= 6 Then
         GBQueryLandData.Filter = " LAND_ID =  '" & Cmb_Land_ID.Text & "'"
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
            Lb_Tambon.Caption = GBQueryLandData.Fields("TAMBON_NAME").Value
            Lb_Village.Caption = GBQueryLandData.Fields("VILLAGE_NAME").Value
            Lb_Soi.Caption = GBQueryLandData.Fields("SOI_NAME").Value
            Lb_Street.Caption = GBQueryLandData.Fields("STREET_NAME").Value
End If
End If
End Sub

Private Sub Cmb_Zoneblock_Click()
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
            TMP_LAND_ID.Caption = ""
             Cmb_Building_ID.Clear
         GBQueryBuildingData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
If GBQueryBuildingData.RecordCount > 0 Then
    Do While Not GBQueryBuildingData.EOF
                  If LenB(Trim$(GBQueryBuildingData.Fields("ZONE_BLOCK").Value)) > 0 Then
                    Cmb_Building_ID.AddItem GBQueryBuildingData.Fields("BUILDING_ID").Value
                  End If
                    GBQueryBuildingData.MoveNext
    Loop
End If
         Cmb_Land_ID.Clear
         GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
    Do While Not GBQueryLandData.EOF
                  If LenB(Trim$(GBQueryLandData.Fields("ZONE_BLOCK").Value)) > 0 Then
                    Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                  End If
                    GBQueryLandData.MoveNext
    Loop
End If
End Sub

Private Sub Cmb_Zoneblock_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then SendKeys "{Tab}"
     KeyAscii = 0
End Sub

Private Sub Form_Activate()
  Clone_Form.Picture1.Visible = True
  Me.WindowState = vbMaximized
End Sub

Private Sub Form_Initialize()
        LstV_Img_Name.Icons = Clone_Form.ImageList1
        LstV_Img_Name.SmallIcons = Clone_Form.ImageList1
End Sub

Private Sub Form_Load()
        Clone_Form.mnuPopup6.Visible = False
        Call SET_REFRESH
End Sub

Private Sub LstV_Img_Name_ItemClick(ByVal Item As MSComctlLib.ListItem)
        If LstV_Img_Name.ListItems.Count > 0 Then Call ShowPicture(LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1))
End Sub

Private Sub LstV_Img_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        If Button = vbRightButton Then
            If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
                 Clone_Form.mnuDelImg4.Visible = True
                 PopupMenu Clone_Form.mnuPopup6, vbPopupMenuLeftAlign, LstV_Img_Name.Left + X, LstV_Img_Name.Top + Me.Top + y
                 picPicture.Picture = LoadPicture("")
                 picPicture.Width = 2235
                 picPicture.Height = 2085
            End If
        End If
End Sub

Private Sub Txt_LICENSE_NAME_Change()
If LenB(Trim$(Txt_LICENSE_NAME.Text)) = 0 Then Txt_LICENSE_NAME.Text = "-"
End Sub

Private Sub Txt_LICENSE_REMARK_Change()
If LenB(Trim$(Txt_LICENSE_REMARK.Text)) = 0 Then Txt_LICENSE_REMARK.Text = "-"
End Sub

Private Sub Txt_LICENSE_SUMTAX_Change()
If LenB(Trim$(Txt_LICENSE_SUMTAX.Text)) = 0 Then Txt_LICENSE_SUMTAX.Text = "0.00"
End Sub
Private Sub Lb_Owner_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
  If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            PopupMenu Clone_Form.mnuPopup4, vbPopupMenuLeftAlign, Lb_Owner_Name.Left + X, Lb_Owner_Name.Top + Me.Top + y
  End If
End If
End Sub

Private Sub Txt_LICENSE_AREA_Change()
If LenB(Trim$(Txt_LICENSE_AREA.Text)) = 0 Then Txt_LICENSE_AREA.Text = "0"
End Sub

Private Sub Txt_LICENSE_AREA_GotFocus()
Txt_LICENSE_AREA.SelStart = 0
Txt_LICENSE_AREA.SelLength = Len(Txt_LICENSE_AREA.Text)
End Sub

Private Sub Txt_LICENSE_AREA_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_LICENSE_BOOK_GotFocus()
Txt_LICENSE_BOOK.SelStart = 0
Txt_LICENSE_BOOK.SelLength = Len(Txt_LICENSE_BOOK.Text)
End Sub

Private Sub Txt_LICENSE_BOOK_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_Change()
If LenB(Trim$(Txt_LICENSE_EMPLOYEE.Text)) = 0 Then Txt_LICENSE_EMPLOYEE.Text = "0"
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_GotFocus()
Txt_LICENSE_EMPLOYEE.SelStart = 0
Txt_LICENSE_EMPLOYEE.SelLength = Len(Txt_LICENSE_EMPLOYEE.Text)
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_ENGINEER_Change()
If LenB(Trim$(Txt_LICENSE_ENGINEER.Text)) = 0 Then Txt_LICENSE_ENGINEER.Text = "0"
End Sub

Private Sub Txt_LICENSE_ENGINEER_GotFocus()
Txt_LICENSE_ENGINEER.SelStart = 0
Txt_LICENSE_ENGINEER.SelLength = Len(Txt_LICENSE_ENGINEER.Text)
End Sub

Private Sub Txt_LICENSE_ENGINEER_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_LICENSE_NAME_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_LICENSE_NO_GotFocus()
Txt_LICENSE_NO.SelStart = 0
Txt_LICENSE_NO.SelLength = Len(Txt_LICENSE_NO.Text)
End Sub

Private Sub Txt_LICENSE_NO_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_SUMTAX_KeyPress(KeyAscii As Integer)
    KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
End Sub

Private Sub Txt_License_Tel_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_StoryRemark_Change()
If LenB(Trim$(Txt_StoryRemark.Text)) = 0 Then Txt_StoryRemark.Text = "-"
End Sub

Private Sub ShowPicture(Path_Picture As String)
                On Error GoTo HldNoPic
                Dim ShrinkScale As Single
                
                ImgSize.Stretch = False
                ImgSize.Picture = LoadPicture(Path_Picture)
                originalWidth = ImgSize.Width
                originalHeight = ImgSize.Height
                sWidth = ImgSize.Width
                sHeight = ImgSize.Height
                picPicture.AutoSize = True
                picPicture.Picture = LoadPicture(Path_Picture)
                
                picPicture.Left = 0
                picPicture.Top = 0
                        
                ImgSize.Stretch = True
                ImgSize.Width = sWidth
                ImgSize.Height = sHeight
        
                sWidth = picParent.Width
                ShrinkScale = picParent.Width / ImgSize.Width
                sHeight = CSng(sHeight * ShrinkScale)
        
        If sHeight > picParent.Height Then
                ShrinkScale = picParent.Height / sHeight
                sWidth = CSng(sWidth * ShrinkScale)
                sHeight = CSng(sHeight * ShrinkScale) - 1
        End If
                ImgSize.Stretch = False
                  Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
                  Exit Sub
HldNoPic:
            picPicture.Picture = LoadPicture("")
            ImgSize.Picture = LoadPicture("")
End Sub

Private Sub SizeImage(picBox As PictureBox, sizePic As Picture, sizeWidth As Single, sizeHeight As Single)
On Error Resume Next
        picBox.Picture = LoadPicture("")
        picBox.Width = sizeWidth
        picBox.Height = sizeHeight
        picBox.AutoRedraw = True
        picBox.PaintPicture sizePic, 0, 0, sizeWidth, sizeHeight
        picBox.Picture = picBox.Image
        picBox.AutoRedraw = False
        picBox.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

