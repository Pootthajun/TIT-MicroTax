VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_PrintSummary 
   BackColor       =   &H00808080&
   ClientHeight    =   9195
   ClientLeft      =   60
   ClientTop       =   165
   ClientWidth     =   13035
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   222
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Frm_PrintSummary.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_PrintSummary.frx":151A
   ScaleHeight     =   9195
   ScaleWidth      =   13035
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkPerformAll 
      Height          =   195
      Left            =   5250
      TabIndex        =   39
      Top             =   8265
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.ComboBox cb_Part 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198D28
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198D35
      Style           =   2  'Dropdown List
      TabIndex        =   35
      Top             =   7800
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.CheckBox chkListName 
      Height          =   195
      Left            =   5250
      TabIndex        =   34
      Top             =   7290
      Visible         =   0   'False
      Width           =   195
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   420
      Left            =   2506
      TabIndex        =   33
      Top             =   1200
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   741
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "txtYear"
      BuddyDispid     =   196625
      OrigLeft        =   3690
      OrigTop         =   1140
      OrigRight       =   3945
      OrigBottom      =   1635
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.CheckBox chkAccept 
      Height          =   195
      Left            =   5250
      TabIndex        =   32
      Top             =   6750
      Width           =   195
   End
   Begin VB.ComboBox cbType 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198D6A
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198D77
      Style           =   2  'Dropdown List
      TabIndex        =   31
      Top             =   5610
      Width           =   3855
   End
   Begin VB.ComboBox cbPerform 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198DAE
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198DB8
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   6120
      Width           =   3855
   End
   Begin VB.CheckBox ChkAllyr 
      Caption         =   "Check1"
      Height          =   195
      Left            =   5250
      TabIndex        =   26
      Top             =   2670
      Width           =   195
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8850
      Locked          =   -1  'True
      TabIndex        =   25
      Text            =   "01/01/2548"
      Top             =   5100
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6780
      Locked          =   -1  'True
      TabIndex        =   24
      Text            =   "01/01/2547"
      Top             =   5100
      Width           =   1395
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   8820
      TabIndex        =   17
      Top             =   5070
      Width           =   1785
      _ExtentX        =   3149
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarTitleBackColor=   14737632
      Format          =   67764225
      CurrentDate     =   37987
   End
   Begin VB.CheckBox chkGK 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5250
      TabIndex        =   15
      Top             =   3150
      Width           =   195
   End
   Begin VB.CheckBox chkMonth 
      Caption         =   "Check1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5250
      TabIndex        =   13
      Top             =   3660
      Width           =   195
   End
   Begin VB.ComboBox cbMonth2 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198DD5
      Left            =   8820
      List            =   "Frm_PrintSummary.frx":198E00
      Locked          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   3540
      Width           =   1800
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      ItemData        =   "Frm_PrintSummary.frx":198E74
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198E76
      TabIndex        =   8
      Top             =   4560
      Width           =   1695
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   8820
      TabIndex        =   7
      Top             =   4560
      Width           =   1785
   End
   Begin VB.ComboBox Cmb_Type 
      BackColor       =   &H00E3DFE0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198E78
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198E9A
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   4050
      Width           =   3855
   End
   Begin VB.ComboBox cbMonth1 
      BackColor       =   &H00E3DFE0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_PrintSummary.frx":198F03
      Left            =   6750
      List            =   "Frm_PrintSummary.frx":198F2E
      Locked          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   3540
      Width           =   1680
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00D6D6D6&
      Caption         =   "�ʴ� Preview ������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   10140
      Picture         =   "Frm_PrintSummary.frx":198FA2
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8370
      Width           =   2655
   End
   Begin VB.TextBox txtYear 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   420
      Left            =   1440
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   2
      Text            =   "2548"
      Top             =   1200
      Width           =   1065
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   6750
      TabIndex        =   14
      Top             =   5070
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   16777215
      CalendarTitleBackColor=   14737632
      Format          =   67764225
      CurrentDate     =   37987
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   7305
      Left            =   0
      TabIndex        =   20
      Top             =   1710
      Width           =   3825
      _ExtentX        =   6747
      _ExtentY        =   12885
      _Version        =   393217
      Indentation     =   353
      LabelEdit       =   1
      Style           =   3
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   90
      Top             =   90
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintSummary.frx":19968C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintSummary.frx":199C26
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintSummary.frx":19A1C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintSummary.frx":19A75A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintSummary.frx":19ACF4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   10
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19B28E
      Top             =   8220
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����������������������շء������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   15
      Left            =   5700
      TabIndex        =   40
      Top             =   8235
      Visible         =   0   'False
      Width           =   3240
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ͺ�Ѻ����駻����Թ���� (�����ͧ)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   12
      Left            =   5700
      TabIndex        =   38
      Top             =   6720
      Width           =   3075
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ʴ���������´����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   13
      Left            =   5700
      TabIndex        =   37
      Top             =   7260
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ǵ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   14
      Left            =   6210
      TabIndex        =   36
      Top             =   7890
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   9
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19B818
      Top             =   7245
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   8
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19BDA2
      Top             =   6705
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   7
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19C32C
      Top             =   5670
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����������¹��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   11
      Left            =   4530
      TabIndex        =   30
      Top             =   5700
      Width           =   2130
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   6
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19C8B6
      Top             =   6180
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������������Ẻ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   10
      Left            =   5025
      TabIndex        =   28
      Top             =   6180
      Width           =   1635
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   0
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19CE40
      Top             =   2670
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�кطء��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   9
      Left            =   5670
      TabIndex        =   27
      Top             =   2640
      Width           =   690
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   5
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19D3CA
      Top             =   5160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   4
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19D954
      Top             =   4650
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   3
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19DEDE
      Top             =   4140
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   2
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19E468
      Top             =   3630
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Img_Chk 
      Height          =   240
      Index           =   1
      Left            =   10800
      Picture         =   "Frm_PrintSummary.frx":19E9F2
      Top             =   3150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ҧ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   8
      Left            =   5895
      TabIndex        =   23
      Top             =   5160
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�֧"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   7
      Left            =   8520
      TabIndex        =   22
      Top             =   5190
      Width           =   210
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�֧"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   6
      Left            =   8520
      TabIndex        =   21
      Top             =   3660
      Width           =   210
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H005C5C5F&
      BorderColor     =   &H005C5C5F&
      Height          =   7305
      Index           =   1
      Left            =   3840
      Top             =   1710
      Width           =   9000
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ʴ�����§ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   1
      Left            =   9690
      TabIndex        =   19
      Top             =   1350
      Width           =   1365
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   2
      Left            =   9330
      Picture         =   "Frm_PrintSummary.frx":19EF7C
      Top             =   1320
      Width           =   240
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ʴ��繡�ҿ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   0
      Left            =   11580
      TabIndex        =   18
      Top             =   1350
      Width           =   1185
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   1
      Left            =   11280
      Picture         =   "Frm_PrintSummary.frx":19F506
      Top             =   1350
      Width           =   240
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ࡳ���ҧ�Ѻ (�.�.2) ������ѧ������������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   5670
      TabIndex        =   16
      Top             =   3120
      Width           =   3360
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ѡ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   5775
      TabIndex        =   11
      Top             =   4650
      Width           =   885
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�֧"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   8520
      TabIndex        =   10
      Top             =   4650
      Width           =   210
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����������Է��� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   4
      Left            =   5100
      TabIndex        =   9
      Top             =   4170
      Width           =   1560
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ш���͹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   5
      Left            =   5610
      TabIndex        =   4
      Top             =   3660
      Width           =   1050
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Index           =   0
      Left            =   390
      TabIndex        =   1
      Top             =   1260
      Width           =   945
   End
   Begin VB.Label LB_Menu 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��������������§ҹ��ػ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   390
      Left            =   3900
      TabIndex        =   0
      Top             =   1770
      Width           =   8895
   End
End
Attribute VB_Name = "Frm_PrintSummary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim rs As ADODB.Recordset
Dim Rs2 As ADODB.Recordset
Dim Rs3 As ADODB.Recordset
Dim RsTotal As ADODB.Recordset
Dim strSQL As String
Dim tmpArr(44) As String
Dim StrContent As String, strGK_Type As String, SQL_REPORT As String

Private Sub Btn_Print_Click()
        Dim strBegin As String
        Dim strEnd As String
        Dim i As Integer

        On Error GoTo ErrPrint
        
        Me.MousePointer = 11
        strSQL = ""
        Call MakeThai
        Clone_Form.CTReport.Reset
        Clone_Form.CTReport.Connect = str_ConnectReport
        Clone_Form.CTReport.DiscardSavedData = True
        Select Case TreeView1.SelectedItem.Index  'PBT  &  ��ػ
                     Case 2
                            If Create_View3 = True Then  '��§ҹ��ػ������
                                    With Clone_Form.CTReport
                                            .ReportTitle = "��������´��ػ�����ŷ�Ѿ���Թ������"
                                            .ReportFileName = App.Path & "\Report\SummaryALL.rpt"
                                            If Cmb_Type.ListIndex = 0 Then
                                                    .Formulas(0) = "OWNER_TYPE = '" & "�����������Է��������" & "'"
                                            Else
                                                    .Formulas(0) = "OWNER_TYPE = '" & "�����������Է��� " & Cmb_Type.Text & "'"
                                            End If
                                    End With
                            End If
                   Case 3 '��§ҹ��ػ�����ŷ�Ѿ���Թ����¹�ŧ
                            If Create_View2 = True Then
                                    With Clone_Form.CTReport
                                            .ReportTitle = "��������´��ػ�����ŷ�Ѿ���Թ����¹�ŧ"
                                            .ReportFileName = App.Path & "\Report\summation.rpt"
                                            .Formulas(0) = "YEAR_FIX = '" & txtYear.Text & "'"
                                            If chkMonth.Value = Checked Then
                                                    If (cbMonth1.ListIndex <> 0 And cbMonth2.ListIndex <> 0) Then .Formulas(1) = "TMP_MONTH = '��͹ " & cbMonth1.Text & " �֧ " & cbMonth2.Text & "'"
                                                    If (cbMonth1.ListIndex <> 0 And cbMonth2.ListIndex = 0) Then .Formulas(1) = "TMP_MONTH = '��͹ " & cbMonth1.Text & "'"
                                                    If (cbMonth1.ListIndex = 0 And cbMonth2.ListIndex <> 0) Then .Formulas(1) = "TMP_MONTH = '��͹ " & cbMonth2.Text & "'"
                                            End If
                                    End With
                            End If
                 Case 6    '�.�. ��ºؤ��
                            If chkGK.Value = Checked Then
                                    If Create_View12 = True Then
                                            With Clone_Form.CTReport
                                                    .Formulas(0) = "YEAR_FIX = '" & txtYear.Text & "'"
                                                    .Formulas(1) = "CHAR1 = '" & Combo1(0).Text & "'"
                                                    .Formulas(2) = "CHAR2 = '" & Combo1(1).Text & "'"
                                                    .ReportTitle = "��������´����������� Ẻ �.�. 2"
                                                    .ReportFileName = App.Path & "\Report\GK2.rpt"
                                            End With
                                    End If
                            Else
                                    If Create_View = True Then
                                            With Clone_Form.CTReport
                                                    .Formulas(0) = "YEAR_FIX = '" & txtYear.Text & "'"
                                                    .Formulas(1) = "CHAR1 = '" & Combo1(0).Text & "'"
                                                    .Formulas(2) = "CHAR2 = '" & Combo1(1).Text & "'"
                                                    .ReportTitle = "��������´����������� Ẻ �.�. 1"
                                                    .ReportFileName = App.Path & "\Report\GK1.rpt"
                                            End With
                                    End If
                            End If
                    Case 7
                            strSQL = ""
                            With Clone_Form.CTReport
                                            Select Case cbType.ListIndex
                                                    Case 0 '���պ��ا��ͧ���
                                                            strGK_Type = "PBT5"
                                                            If cbPerform.ListIndex = 1 Then '1 ��������Ẻ
                                                                    If chkListName.Value = Checked Then
                                                                            .ReportTitle = "�ѭ����ª��ͤ�ҧ���Ẻ���պ��ا��ͧ���"
                                                                            .ReportFileName = App.Path & "\Report\PBT5_Name_Not_Perform.rpt"
                                                                    Else
                                                                            Create_View14
                                                                            .ReportTitle = "��§ҹ�����������Ẻ ����ѭ�� �.�. 2"
                                                                            .ReportFileName = App.Path & "\Report\PBT5_Not_Perform.rpt"
                                                                    End If
                                                            Else   ' ���Ẻ
                                                                    If chkAccept.Value = Checked Then
                                                                            .ReportTitle = "��§ҹ���ͺ�Ѻ����駻����Թ ����ѭ�� �.�. 2"
                                                                            strSQL = strSQL & " DATEDIFF('d',{PBT5.PBT5_DATE_RETURN},#" & Date & "#) > " & CInt(Frm_Index.Txt_Due_Accept1.Text)
                                                                            .Formulas(0) = "Date_Due = " & CInt(Frm_Index.Txt_Due_Accept1.Text)
                                                                            .ReportFileName = App.Path & "\Report\PBT5_Accept.rpt"
                                                                    Else
                                                                            If chkPerformAll.Value = Checked Then
                                                                                    .ReportFileName = App.Path & "\Report\PBT5_Perform_All.rpt"
                                                                                    strGK_Type = "VIEW_PBT5_PERFORM_ALL"
                                                                            Else
                                                                                    .ReportTitle = "��§ҹ��������Ẻ ����ѭ�� �.�. 2"
                                                                                    .ReportFileName = App.Path & "\Report\PBT5_Perform.rpt"
                                                                                    strGK_Type = "VIEW_PBT5_PERFORM"
                                                                            End If
                                                                            .Formulas(0) = "FYear = '" & txtYear.Text & "'"
                                                                    End If
                                                            End If
                                                            strBegin = "PBT5"
                                                    Case 1 '�����ç���͹��з��Թ
                                                            strGK_Type = "PRD2"
                                                            If cbPerform.ListIndex = 1 Then '1 ��������Ẻ
                                                                    If chkListName.Value = Checked Then
                                                                            .ReportTitle = "�ѭ����ª��ͤ�ҧ���Ẻ�����ç���͹��з��Թ"
                                                                            .ReportFileName = App.Path & "\Report\PRD2_Name_Not_Perform.rpt"
                                                                    Else
                                                                            Create_View14
                                                                            .ReportTitle = "��§ҹ�����������Ẻ ����ѭ�� �.�. 2"
                                                                            .ReportFileName = App.Path & "\Report\PRD2_Not_Perform.rpt"
                                                                    End If
                                                            Else
                                                                    If chkAccept.Value = Checked Then
                                                                            .ReportTitle = "��§ҹ���ͺ�Ѻ����駻����Թ ����ѭ�� �.�. 2"
                                                                            strSQL = strSQL & " DATEDIFF('d',{PRD2.PRD2_DATE_RETURN},#" & Date & "#) > " & CInt(Frm_Index.Txt_Due_Accept0.Text)
                                                                            .Formulas(0) = "Date_Due = " & CInt(Frm_Index.Txt_Due_Accept0.Text)
                                                                            .ReportFileName = App.Path & "\Report\PRD2_Accept.rpt"
                                                                    Else
                                                                            If chkPerformAll.Value = Checked Then
                                                                                    .ReportFileName = App.Path & "\Report\PRD2_Perform_All.rpt"
                                                                                    strGK_Type = "VIEW_PRD2_PERFORM_ALL"
                                                                            Else
                                                                                    .ReportTitle = "��§ҹ��������Ẻ ����ѭ�� �.�. 2"
                                                                                    .ReportFileName = App.Path & "\Report\PRD2_Perform.rpt"
                                                                                    strGK_Type = "VIEW_PRD2_PERFORM"
                                                                            End If
                                                                            .Formulas(0) = "FYear = '" & txtYear.Text & "'"
                                                                    End If
                                                            End If
                                                            strBegin = "PRD2"
                                                    Case 2 '���ջ���
                                                            strGK_Type = "PP1"
                                                            If cbPerform.ListIndex = 1 Then '1 ��������Ẻ
                                                                    If chkListName.Value = Checked Then
                                                                            .ReportTitle = "�ѭ����ª��ͤ�ҧ���Ẻ���ջ���"
                                                                            .ReportFileName = App.Path & "\Report\PP1_Name_Not_Perform.rpt"
                                                                    Else
                                                                            Create_View14
                                                                            .ReportTitle = "��§ҹ�����������Ẻ ����ѭ�� �.�. 2"
                                                                            .ReportFileName = App.Path & "\Report\PP1_Not_Perform.rpt"
                                                                    End If
                                                            Else
                                                                    If chkAccept.Value = Checked Then
                                                                            .ReportTitle = "��§ҹ���ͺ�Ѻ����駻����Թ���� ����ѭ�� �.�. 2"
                                                                            strSQL = strSQL & " DATEDIFF('d',{PP1.PP1_DATE_RETURN},#" & Date & "#) > " & CInt(Frm_Index.Txt_Due_Accept2.Text)
                                                                            .Formulas(0) = "Date_Due = " & CInt(Frm_Index.Txt_Due_Accept2.Text)
                                                                            .ReportFileName = App.Path & "\Report\PP1_Accept.rpt"
                                                                    Else
                                                                            If chkPerformAll.Value = Checked Then
                                                                                    .ReportFileName = App.Path & "\Report\PP1_Perform_All.rpt"
                                                                                    strGK_Type = "VIEW_PP1_PERFORM_ALL"
                                                                            Else
                                                                                    .ReportTitle = "��§ҹ��������Ẻ ����ѭ�� �.�. 2"
                                                                                    .ReportFileName = App.Path & "\Report\PP1_Perform.rpt"
                                                                                    strGK_Type = "VIEW_PP1_PERFORM"
                                                                            End If
                                                                            .Formulas(0) = "FYear = '" & txtYear.Text & "'"
                                                                    End If
                                                            End If
                                                            strBegin = "PP1"
                                            End Select
                                            If cbPerform.ListIndex <> 1 Or (cbPerform.ListIndex = 1 And chkListName.Value = Checked) Then
                                                    If Cmb_Type.ListIndex > 0 Then strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_TYPE} = " & Cmb_Type.ListIndex & ")"
                                                    If (LenB(Trim$(Combo1(0).Text)) > 0) Or (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                        If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                                    If Combo1(0).ListIndex < Combo1(1).ListIndex Then
                                                                            If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                                    strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(0) & "')"
                                                                            Else
                                                                                    strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & strGK_Type & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                                            End If
                                                                    Else
                                                                            If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                                    strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(1) & "')"
                                                                            Else
                                                                                    strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & strGK_Type & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                                            End If
                                                                    End If
                                                        End If
                                                End If
                                                If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) = 0) Then
                                                        If Combo1(0) = "�" Then
                                                                strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(0) & "')"
                                                        Else
                                                                strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & strGK_Type & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                        End If
                                                End If
                                                If (LenB(Trim$(Combo1(0).Text)) = 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                        If Combo1(1) = "�" Then
                                                                strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(1) & "')"
                                                        Else
                                                                strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & strGK_Type & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                        End If
                                                End If
                                                If Left$(Trim$(strSQL), 3) = "AND" Then strSQL = Right$(strSQL, Len(Trim$(strSQL)) - 3)
                                                                strSQL = strSQL & " AND ({" & strGK_Type & "." & strBegin & "_YEAR}=" & txtYear.Text & ")"
                                                If cbPerform.ListIndex = 0 Then
                                                                strSQL = strSQL & " AND ({" & strGK_Type & "." & strBegin & "_DATE} >= " & "Date(" & DTPicker1.Year & "," & DTPicker1.Month & "," & DTPicker1.Day & "))"
                                                                strSQL = strSQL & " AND ({" & strGK_Type & "." & strBegin & "_DATE} <= " & "Date(" & DTPicker2.Year & "," & DTPicker2.Month & "," & DTPicker2.Day & "))"
                                                End If
                                                If Left$(Trim$(strSQL), 3) = "AND" Then strSQL = Right$(Trim$(strSQL), Len(Trim$(strSQL)) - 3)
                                                .SelectionFormula = strSQL
                                        End If
                                    End With
                    Case 8 '��ª��ͺѭ���������
                            If ChkAllyr.Value = Checked Then
                                    If Create_View15 = True Then
                                            With Clone_Form.CTReport
                                                    .Formulas(0) = "YEAR_FIX = '" & txtYear.Text & "'"
                                                    Select Case cbType.ListIndex
                                                            Case 0
                                                                    .Formulas(1) = "payment = '�����ҧ��͹ ���Ҥ� �֧ ��͹ ���Ҥ� (�Ǵ��� 1)'"
                                                                    .Formulas(2) = "payment2 ='1'"
                                                            Case 1
                                                                    .Formulas(1) = "payment = '�����ҧ��͹ ����Ҿѹ�� �֧ ��͹ ����Ҥ� (�Ǵ��� 2)'"
                                                                    .Formulas(2) = "payment2 ='2'"
                                                            Case 2
                                                                    .Formulas(1) = "payment = '�����ҧ��͹ �Զع�¹ �֧ ��͹ �ѹ��¹ (�Ǵ��� 3)'"
                                                                    .Formulas(2) = "payment2 ='3'"
                                                    End Select
                                                    .ReportTitle = "��ª��ͺѭ���������"
                                                    .ReportFileName = App.Path & "\Report\GK_OTHER.rpt"
                                            End With
                                    End If
                            Else
                                    If Create_View17 = True Then
                                            With Clone_Form.CTReport
                                                    .Formulas(0) = "YEAR_FIX = '" & txtYear.Text & "'"
                                                    .ReportTitle = "��ª��ͺѭ���������"
                                                    .ReportFileName = App.Path & "\Report\GK_NEW.rpt"
                                            End With
                                    End If
                            End If
                    Case 9 '�ѭ���١˹��
                            If Create_View13 = True Then
                                    With Clone_Form.CTReport
                                                Select Case cbType.ListIndex
                                                        Case 0 '���պ��ا��ͧ���
                                                                .ReportTitle = "�ѭ���١˹�����պ��ا��ͧ���"
                                                                .ReportFileName = App.Path & "\Report\GK2_PBT5.rpt"
                                                        Case 1 '�����ç���͹��з��Թ
                                                                .ReportTitle = "�ѭ���١˹�������ç���͹��з��Թ"
                                                                .ReportFileName = App.Path & "\Report\GK2_PRD2.rpt"
                                                        Case 2 '���ջ���
                                                                .ReportTitle = "�ѭ���١˹�����ջ���"
                                                                .ReportFileName = App.Path & "\Report\GK2_PP1.rpt"
                                                End Select
                                    End With
                            End If
                    Case 10    '��§ҹ�鹡�˹���û����Թ����
                        With Clone_Form.CTReport
                                        strSQL = ""
                                        SQL_REPORT = ""
                                        StrContent = ""
                                        Select Case cbType.ListIndex
                                                Case 0 '���պ��ا��ͧ���
                                                        .ReportTitle = "��§ҹ�鹡�˹���û����Թ���պ��ا��ͧ���"
                                                        .ReportFileName = App.Path & "\Report\PBT5_Accept_Over.rpt"
                                                        .Formulas(0) = "Tax_Outtime =" & Frm_Index.Txt_Tax_OutTime1.Text
                                                        SQL_REPORT = SQL_REPORT & "{PBT5.PBT5_YEAR} = " & txtYear.Text
                                                        StrContent = "PBT5"
                                                Case 1 '�����ç���͹��з��Թ
                                                        .ReportTitle = "��§ҹ�鹡�˹���û����Թ�����ç���͹��з��Թ"
                                                        .ReportFileName = App.Path & "\Report\PRD2_Accept_Over.rpt"
                                                        .Formulas(0) = "Due1 =" & Frm_Index.Txt_Due_1Month.Text
                                                        .Formulas(1) = "Due2 =" & Frm_Index.Txt_Due_2Month.Text
                                                        .Formulas(2) = "Due3 =" & Frm_Index.Txt_Due_3Month.Text
                                                        .Formulas(3) = "Due4 =" & Frm_Index.Txt_Due_4Month.Text
                                                        SQL_REPORT = SQL_REPORT & "{PRD2.PRD2_YEAR} = " & txtYear.Text
                                                        StrContent = "PRD2"
                                                Case 2 '���ջ���
                                                        .ReportTitle = "��§ҹ�鹡�˹���û����Թ���ջ���"
                                                        .ReportFileName = App.Path & "\Report\PP1_Accept_Over.rpt"
                                                        .Formulas(0) = "Tax_Outtime =" & Frm_Index.Txt_Tax_OutTime2.Text
                                                        SQL_REPORT = SQL_REPORT & "{PP1.PP1_YEAR} = " & txtYear.Text
                                                        StrContent = "PP1"
                                        End Select
                                        If (LenB(Trim$(Combo1(0).Text)) > 0) Or (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                            If Combo1(0).ListIndex < Combo1(1).ListIndex Then
                                                                    If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                            SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'"
                                                                    Else
                                                                            SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                                    End If
                                                            Else
                                                                    If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                            SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'"
                                                                    Else
                                                                            SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                                    End If
                                                            End If
                                                End If
                                         End If
                                        If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) = 0) Then
                                                If Combo1(0) = "�" Then
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'"
                                                Else
                                                        SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                End If
                                        End If
                                        If (LenB(Trim$(Combo1(0).Text)) = 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                If Combo1(1) = "�" Then
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'"
                                                Else
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                End If
                                        End If
                                        If Cmb_Type.ListIndex > 0 Then
                                              SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_TYPE} = " & Cmb_Type.ListIndex
                                        End If
                                        .SelectionFormula = SQL_REPORT
                                End With
                    Case 11   '��§ҹ��û�Ѻ������
                                With Clone_Form.CTReport
                                        strSQL = ""
                                        SQL_REPORT = ""
                                        StrContent = ""
                                        Select Case cbType.ListIndex
                                                Case 0 '���պ��ا��ͧ���
                                                        Select Case cbPerform.ListIndex
                                                                Case 0
                                                                        .ReportFileName = App.Path & "\Report\PBT5_CHANGE_OWNER.rpt"
                                                                Case 1
                                                                        .ReportFileName = App.Path & "\Report\PBT5_ERASE.rpt"
                                                                Case 2
                                                                        .ReportFileName = App.Path & "\Report\PBT5_NOT_PAYTAX.rpt"
                                                                Case 3
                                                                        .ReportFileName = App.Path & "\Report\PBT5_CHANGE_NAME.rpt"
                                                        End Select
                                                        SQL_REPORT = "{PBT5.PBT5_YEAR} = " & txtYear.Text
                                                        If cbPerform.ListIndex = 0 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATA.LAND_DATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({LANDDATA.LAND_DATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATA.LAND_DATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({LANDDATA.LAND_DATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATA.LAND_DATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({LANDDATA.LAND_DATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        ElseIf cbPerform.ListIndex = 1 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATASTORY.LAND_UPDATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({LANDDATASTORY.LAND_UPDATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATASTORY.LAND_UPDATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({LANDDATASTORY.LAND_UPDATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({LANDDATASTORY.LAND_UPDATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({LANDDATASTORY.LAND_UPDATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        End If
                                                        StrContent = "PBT5"
                                                Case 1 '�����ç���͹��з��Թ
                                                        Select Case cbPerform.ListIndex
                                                                Case 0
                                                                        .ReportFileName = App.Path & "\Report\PRD2_CHANGE_OWNER.rpt"
                                                                Case 1
                                                                        .ReportFileName = App.Path & "\Report\PRD2_ERASE.rpt"
                                                                Case 2
                                                                        .ReportFileName = App.Path & "\Report\PRD2_NOT_PAYTAX.rpt"
                                                                Case 3
                                                                        .ReportFileName = App.Path & "\Report\PRD2_CHANGE_NAME.rpt"
                                                        End Select
                                                        SQL_REPORT = "{PRD2.PRD2_YEAR} = " & txtYear.Text
                                                        If cbPerform.ListIndex = 0 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATA.BUILDING_DATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({BUILDINGDATA.BUILDING_DATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATA.BUILDING_DATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({BUILDINGDATA.BUILDING_DATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATA.BUILDING_DATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({BUILDINGDATA.BUILDING_DATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        ElseIf cbPerform.ListIndex = 1 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({BUILDINGDATASTORY.BUILDING_UPDATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        End If
                                                        StrContent = "PRD2"
                                                Case 2 '���ջ���
                                                        Select Case cbPerform.ListIndex
                                                                Case 0
                                                                         .ReportFileName = App.Path & "\Report\PP1_CHANGE_OWNER.rpt"
                                                                Case 1
                                                                        .ReportFileName = App.Path & "\Report\PP1_ERASE.rpt"
                                                                Case 2
                                                                        .ReportFileName = App.Path & "\Report\PP1_NOT_PAYTAX.rpt"
                                                                Case 3
                                                                        .ReportFileName = App.Path & "\Report\PP1_CHANGE_NAME.rpt"
                                                        End Select
                                                        SQL_REPORT = "{PP1.PP1_YEAR} = " & txtYear.Text
                                                        If cbPerform.ListIndex = 0 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({SIGNBORDDATA.SIGNBORD_START_DATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        ElseIf cbPerform.ListIndex = 1 Then
                                                                Select Case cb_Part.ListIndex
                                                                        Case 0
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATASTORY.STORY_UPDATE}  >= #" & "10-01-" & CInt(txtYear.Text) - 544 & "#) AND ({SIGNBORDDATASTORY.STORY_UPDATE}  <= #" & "01-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 1
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATASTORY.STORY_UPDATE}  >= #" & "02-01-" & CInt(txtYear.Text) - 543 & "#) AND ({SIGNBORDDATASTORY.STORY_UPDATE}  <= #" & "05-31-" & CInt(txtYear.Text) - 543 & "#)"
                                                                        Case 2
                                                                                SQL_REPORT = SQL_REPORT & " AND ({SIGNBORDDATASTORY.STORY_UPDATE}  >= #" & "06-01-" & CInt(txtYear.Text) - 543 & "#) AND ({SIGNBORDDATASTORY.STORY_UPDATE}  <= #" & "09-30-" & CInt(txtYear.Text) - 543 & "#)"
                                                                End Select
                                                        End If
                                                        StrContent = "PP1"
                                        End Select
                                        If cbPerform.ListIndex = 0 Or cbPerform.ListIndex = 1 Then
                                                Select Case cb_Part.ListIndex
                                                        Case 0
                                                                .Formulas(0) = "part = '�����ҧ��͹ ���Ҥ� �֧ ��͹ ���Ҥ� (�Ǵ��� 1)'"
                                                        Case 1
                                                                .Formulas(0) = "part = '�����ҧ��͹ ����Ҿѹ�� �֧ ��͹ ����Ҥ� (�Ǵ��� 2)'"
                                                        Case 2
                                                                .Formulas(0) = "part = '�����ҧ��͹ �Զع�¹ �֧ ��͹ �ѹ��¹ (�Ǵ��� 3)'"
                                                End Select
                                        End If
                                        If Cmb_Type.ListIndex > 0 Then strSQL = strSQL & " AND ({" & strGK_Type & ".OWNER_TYPE} = " & Cmb_Type.ListIndex & ")"
                                        If (LenB(Trim$(Combo1(0).Text)) > 0) Or (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                            If Combo1(0).ListIndex < Combo1(1).ListIndex Then
                                                                    If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                            SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'"
                                                                    Else
                                                                            SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                                    End If
                                                            Else
                                                                    If Combo1(0) = "�" Or Combo1(1) = "�" Then
                                                                            SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'"
                                                                    Else
                                                                            SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                                    End If
                                                            End If
                                                End If
                                         End If
                                        If (LenB(Trim$(Combo1(0).Text)) > 0) And (LenB(Trim$(Combo1(1).Text)) = 0) Then
                                                If Combo1(0) = "�" Then
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'"
                                                Else
                                                        SQL_REPORT = SQL_REPORT & " AND ({" & StrContent & ".OWNER_NAME} > '" & Combo1(0) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(0).List(Combo1(0).ListIndex + 1) & "')"
                                                End If
                                        End If
                                        If (LenB(Trim$(Combo1(0).Text)) = 0) And (LenB(Trim$(Combo1(1).Text)) > 0) Then
                                                If Combo1(1) = "�" Then
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'"
                                                Else
                                                        SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_NAME} > '" & Combo1(1) & "'" & " AND {" & StrContent & ".OWNER_NAME} < '" & Combo1(1).List(Combo1(1).ListIndex + 1) & "')"
                                                End If
                                        End If
                                        If Cmb_Type.ListIndex > 0 Then
                                              SQL_REPORT = SQL_REPORT & " AND {" & StrContent & ".OWNER_TYPE} = " & Cmb_Type.ListIndex
                                        End If
                                        .SelectionFormula = SQL_REPORT
                                End With
                    Case 12   '�.� �Ѵ������������ѡ��
                        If Create_View4 = True Then
                                strSQL = ""
                                If Cmb_Type.ListIndex > 0 Then strSQL = "({Temp_GK.OWNER_TYPE} = " & Cmb_Type.ListIndex & ")"
                                If LenB(Trim$(Combo1(0).Text)) > 0 Or LenB(Trim$(Combo1(1).Text)) > 0 Then
                                        If LenB(Trim$(strSQL)) > 0 Then strSQL = strSQL & " AND "
                                        If LenB(Trim$(Combo1(1).Text)) = 0 Then
                                                strSQL = strSQL & "({Temp_GK.CHAR_ID}  = '" & Combo1(0).Text & "')"
                                        ElseIf LenB(Trim$(Combo1(0).Text)) = 0 Then
                                                strSQL = strSQL & " ({Temp_GK.CHAR_ID}  = '" & Combo1(1).Text & "')"
                                        ElseIf Asc(Combo1(0).Text) < Asc(Combo1(1).Text) Then
                                                strSQL = strSQL & " ({Temp_GK.CHAR_ID}  >= '" & Combo1(0).Text & "' AND {Temp_GK.CHAR_ID} <=  '" & Combo1(1).Text & "')"
                                        Else
                                                strSQL = strSQL & " ({Temp_GK.CHAR_ID}  >= '" & Combo1(1).Text & "' AND {Temp_GK.CHAR_ID} <=  '" & Combo1(0).Text & "')"
                                        End If
                                End If
                                With Clone_Form.CTReport
                                        .SelectionFormula = strSQL
                                        .ReportTitle = "��������´ࡳ���ҧ����  �.�.1 (��ºؤ��)"
                                        .ReportFileName = App.Path & "\Report\GK1_Summary.rpt"
                                        If chkGK.Value = Checked Then
                                                .Formulas(0) = "TITLE_GK = '���ػ�ѭ���������ࡳ���ҧ�Ѻ � �ѹ�Ѩ�غѹ " & "'"
                                        Else
                                                .Formulas(0) = "TITLE_GK= '���ػ�ѭ���������ࡳ���ҧ�Ѻ ��Шӻ� " & txtYear.Text & "'"
                                        End If
                                        If LenB(Trim$(Combo1(1).Text)) = 0 Then
                                                .Formulas(1) = "CHAR1 = '" & Combo1(0).Text & "'"
                                        ElseIf (LenB(Trim$(Combo1(1).Text)) > 0) And (LenB(Trim$(Combo1(0).Text)) = 0) Then
                                                .Formulas(1) = "CHAR1 = '" & Combo1(1).Text & "'"
                                        Else
                                                .Formulas(1) = "CHAR1 = '" & Combo1(0).Text & "'"
                                                .Formulas(2) = "CHAR2 = '" & Combo1(1).Text & "'"
                                        End If
                                End With
                        End If
                Case 15
                        With Clone_Form.CTReport
                                    If ChkAllyr.Value = Checked Then
                                            If Create_View5 = True Then
                                                    .ReportTitle = "��������´��ػ���շ��Ѵ�����Ш��ѹ"
                                                    .ReportFileName = App.Path & "\Report\Optain_Summary.rpt"
                                                    .Formulas(2) = "OwnerType = '" & Cmb_Type.Text & "'"
                                                    If LenB(Trim$(Combo1(0).Text)) = 0 And LenB(Trim$(Combo1(1).Text)) = 0 Then
                                                            .Formulas(3) = "GroupChar = ''"
                                                    Else
                                                            .Formulas(3) = "GroupChar = '�ѡ��  " & Combo1(0).Text & "  -  " & Combo1(1).Text & "'"
                                                    End If
                                            End If
                                    Else
                                            If Create_View16 = True Then
                                            Select Case cbType.ListIndex
                                                    Case 0
'                                                            strSQL = "{PBT5.PBT5_YEAR}=" & CInt(txtYear.Text)
'                                                            If Cmb_Type.ListIndex > 0 Then
'                                                                    strSQL = strSQL & " AND {PBT5.OWNER_TYPE}=" & Cmb_Type.ListIndex
'                                                            End If
'                                                            strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "{PBT5.OWNER_NAME}")
                                                            .ReportFileName = App.Path & "\Report\PBT5_Day.rpt"
                                                            .SelectionFormula = "{PBT5.PBT5_SET_GK}  = " & cbPerform.ListIndex
'                                                            If Not IsNull(DTPicker2.Value) Then strSQL = strSQL & " AND (Date({PBT5.PBT5_PAY_DATE}) >= #" & DTPicker1.Month & "/" & DTPicker1.Day & "/" & DTPicker1.Year & "# AND Date({PBT5.PBT5_PAY_DATE}) <= #" & DTPicker2.Month & "/" & DTPicker2.Day & "/" & DTPicker2.Year & "#)"
                                                    Case 1
'                                                            strSQL = "{PRD2.PRD2_YEAR}=" & CInt(txtYear.Text)
'                                                            If Cmb_Type.ListIndex > 0 Then
'                                                                    strSQL = strSQL & " AND {PRD2.OWNER_TYPE}=" & Cmb_Type.ListIndex
'                                                            End If
'                                                            strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "{PRD2.OWNER_NAME}")
                                                            .ReportFileName = App.Path & "\Report\PRD2_Day.rpt"
                                                            .SelectionFormula = "{PRD2.PRD2_SET_GK}  = " & cbPerform.ListIndex
                                                            
'                                                            If Not IsNull(DTPicker2.Value) Then strSQL = strSQL & " AND (Date({PRD2.PRD2_PAY_DATE}) >= #" & DTPicker1.Month & "/" & DTPicker1.Day & "/" & DTPicker1.Year & "# AND Date({PRD2.PRD2_PAY_DATE}) <= #" & DTPicker2.Month & "/" & DTPicker2.Day & "/" & DTPicker2.Year & "#)"
                                                    Case 2
'                                                            strSQL = "{PP1.PP1_YEAR}=" & CInt(txtYear.Text)
'                                                            If Cmb_Type.ListIndex > 0 Then
'                                                                    strSQL = strSQL & " AND {PP1.OWNER_TYPE}=" & Cmb_Type.ListIndex
'                                                            End If
'                                                            strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "{PP1.OWNER_NAME}")
                                                            .ReportFileName = App.Path & "\Report\PP1_Day.rpt"
                                                            .SelectionFormula = "{PP1.PP1_SET_GK}  = " & cbPerform.ListIndex
'                                                            If Not IsNull(DTPicker2.Value) Then strSQL = strSQL & " AND (Date({PP1.PP1_PAY_DATE}) >= #" & DTPicker1.Month & "/" & DTPicker1.Day & "/" & DTPicker1.Year & "# AND Date({PP1.PP1_PAY_DATE}) <= #" & DTPicker2.Month & "/" & DTPicker2.Day & "/" & DTPicker2.Year & "#)"
                                            End Select
                                            End If
                                            .Formulas(3) = "Tax_State='(" & cbPerform.Text & ")'"
'                                            .SelectionFormula = strSQL
                                    End If
                                    If DTPicker2.Visible = False Then
                                            .Formulas(0) = "PERIOD1 = '�ѹ��� " & DTPicker1.Value & "'"      ''�ѹ��� " & Format$(DTPicker1.Value, "dd/mmm") & "/" & Right$(DTPicker1.Value, 4) & "'"
                                    Else
                                            .Formulas(0) = "PERIOD1 = '�ѹ��� " & DTPicker1.Value & "  �֧  '" ''�ѹ��� " & Format$(DTPicker1.Value, "dd/mmm") & "/" & Right$(DTPicker1.Value, 4) & "  �֧  '"
                                    End If
                                    If DTPicker2.Visible = True Then
                                            .Formulas(1) = "PERIOD2 = '" & DTPicker2.Value & "'" 'Format$(DTPicker2.Value, "dd/mmm") & "/" & Right$(DTPicker2.Value, 4) & "'"
                                    Else
                                            .Formulas(1) = "PERIOD2 = ''"
                                    End If
                                    If DTPicker2.Visible = False Then
                                            .Formulas(0) = "PERIOD1 = '�ѹ��� " & DTPicker1.Value & "'"
                                    Else
                                            .Formulas(0) = "PERIOD1 = '�ѹ��� " & DTPicker1.Value & "  �֧  '"
                                    End If
                                    If DTPicker2.Visible = True Then
                                            .Formulas(1) = "PERIOD2 = '" & DTPicker2.Value & "'"
                                    Else
                                            .Formulas(1) = "PERIOD2 = ''"
                                    End If
                            End With
                Case 16, 17
                        If Create_View6 = True Then
                        With Clone_Form.CTReport
                                    If TreeView1.SelectedItem.Index = 16 Then
                                            .ReportTitle = "��§ҹ��ػ���շ��Ѵ����㹻է�����ҳ"
                                            .Formulas(0) = "HEAD_TITLE = '" & "��ػ�ʹ�Ѻ������������ʹ��ҧ�������ջ�Шӻ�  " & txtYear.Text & "'"
                                            .ReportFileName = App.Path & "\Report\GK_Real_Obtain.rpt"
                                    ElseIf TreeView1.SelectedItem.Index = 17 Then
                                            .ReportTitle = "��§ҹ��ػ�����������Ѵ����㹻է�����ҳ"
                                            .Formulas(0) = "HEAD_TITLE = '" & "��ػ�ʹ�Ѻ�����������������ʹ��ҧ���л�Шӻ�  " & txtYear.Text & "'"
                                            .ReportFileName = App.Path & "\Report\Other_Real_Obtain.rpt"
                                    End If
                        End With
                        End If
                Case 19
                        If Create_View8 = True Then
                        With Clone_Form.CTReport
                                    .ReportTitle = "��§ҹ��èѴ�ѹ�Ѻ����ҧ��������"
                                    .Formulas(1) = "AT_YEAR = '��Шӻ� " & txtYear.Text & "'"
                                    .ReportFileName = App.Path & "\Report\GK_Rank.rpt"
                        End With
                        End If
                Case 20
                        If Create_View9 = True Then
                        With Clone_Form.CTReport
                                    .ReportTitle = "��§ҹ��èѴ�ѹ�Ѻ����ҧ�������պ��ا��ͧ���"
                                    .ReportFileName = App.Path & "\Report\PBT5_Rank.rpt"
                        End With
                        End If
                Case 21
                        If Create_View10 = True Then
                        With Clone_Form.CTReport
                                    .ReportTitle = "��§ҹ��èѴ�ѹ�Ѻ����ҧ���������ç���͹��з��Թ"
                                    .ReportFileName = App.Path & "\Report\PRD2_Rank.rpt"
                        End With
                        End If
                Case 22
                        If Create_View11 = True Then
                        With Clone_Form.CTReport
                                    .ReportTitle = "��§ҹ��èѴ�ѹ�Ѻ����ҧ�������ջ���"
                                    .ReportFileName = App.Path & "\Report\PP1_Rank.rpt"
                        End With
                        End If
                Case 14 '21
                        If Create_View7 = True Then
                        With Clone_Form.CTReport
                                    .ReportTitle = "��§ҹ��ػ�š�û���ҳ���ջ�Шӻ�"
                                    .Formulas(0) = "SECTION = '" & iniSection & "'"
                                    .Formulas(1) = "SECTION2 = ' " & txtYear & "'"
                                    .ReportFileName = App.Path & "\Report\Summation_Count_Money.rpt"
                        End With
                        End If
                Case 24
                        Clone_Form.CTReport.ReportFileName = App.Path & "\Report\Rpt_GhpPayYear.rpt"
                Case 25
                        Clone_Form.CTReport.ReportFileName = App.Path & "\Report\Rpt_GhpPayCompareCredit.rpt"
                Case 27
'                        On Error Resume Next
'                        Call SET_Execute("DROP VIEW VIEW_GhpPBT5INYEAR")
                        StrContent = ""
                        StrContent = "ALTER VIEW  VIEW_GhpPBT5INYEAR AS "
                        StrContent = StrContent & "SELECT CONVERT(varchar(5), GK_YEAR) AS ALLYEAR,(SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 From dbo.PBT5 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '1') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY1," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_23 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '2') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY2," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_22 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '3') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY3," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_21 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '4') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY4," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_20 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '5') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY5," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_19 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '6') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY6," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_18 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '7') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY7," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_17 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '8') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY8," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_16 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '9') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY9," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_15 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '10') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY10," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_14 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '11') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY11," & _
                    " (SELECT SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_13 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '12') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_PAY12," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_12 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '1') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE1," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_11 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '2') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE2," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_10 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '3') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE3," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_9 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '4') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE4," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_8 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '5') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE5," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_7 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '6') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE6," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_6 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '7') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE7," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_5 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '8') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE8," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_4 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '9') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE9," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_3 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '10') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE10," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_2 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '11') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE11," & _
                    " (SELECT SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1 FROM dbo.PBT5 AS PBT5_1 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) AND (MONTH(PBT5_PAY_DATE) = '12') GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_INCLUDE12" & _
                    " From dbo.GK Where (GK_YEAR Is Not Null) GROUP BY GK_YEAR Having (GK_YEAR = " & txtYear.Text & ")"
                        
                                    Call SET_Execute(StrContent)
                                                    With Clone_Form.CTReport
                                                                .Formulas(0) = "AtYear = '" & txtYear.Text & "'"
                                                                .Formulas(1) = "BeforeYear = '" & CInt(txtYear.Text) - 1 & "'"
                                                                .ReportFileName = App.Path & "\Report\Rpt_GhpPBT5INYEAR.rpt"
                                                    End With
                                                    
                                  Case 28
'                                                On Error Resume Next
'                                                Call SET_Execute("DROP VIEW VIEW_GhpPRD2INYEAR")
                                                StrContent = ""
                                                StrContent = "ALTER VIEW  VIEW_GhpPRD2INYEAR AS "
                        StrContent = StrContent & "SELECT CONVERT(varchar(5), GK_YEAR) AS ALLYEAR,(SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 From dbo.PRD2 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '1') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY1," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_23 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '2') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY2," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_22 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '3') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY3," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_21 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '4') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY4," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_20 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '5') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY5," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_19 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '6') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY6," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_18 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '7') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY7," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_17 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '8') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY8," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_16 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '9') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY9," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_15 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '10') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY10," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_14 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '11') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY11," & _
                    " (SELECT SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_13 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '12') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_PAY12," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_12 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '1') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE1," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_11 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '2') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE2," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_10 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '3') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE3," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_9 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '4') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE4," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_8 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '5') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE5," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_7 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '6') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE6," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_6 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '7') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE7," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_5 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '8') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE8," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_4 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '9') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE9," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_3 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '10') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE10," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_2 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '11') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE11," & _
                    " (SELECT SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_1 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) AND (MONTH(PRD2_PAY_DATE) = '12') GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_INCLUDE12" & _
                    " From dbo.GK Where (GK_YEAR Is Not Null) GROUP BY GK_YEAR Having (GK_YEAR = " & txtYear.Text & ")"
                                                                                     
                                    Call SET_Execute(StrContent)
                                    With Clone_Form.CTReport
                                                .Formulas(0) = "AtYear = '" & txtYear.Text & "'"
                                                .Formulas(1) = "BeforeYear = '" & CInt(txtYear.Text) - 1 & "'"
                                                .ReportFileName = App.Path & "\Report\Rpt_GhpPRD2INYEAR.rpt"
                                    End With
                    Case 29
'                                                On Error Resume Next
'                                                Call SET_Execute("DROP VIEW VIEW_GhpPP1INYEAR")
                                                StrContent = ""
                                                StrContent = "ALTER VIEW  VIEW_GhpPP1INYEAR AS "
                        StrContent = StrContent & "SELECT CONVERT(varchar(5), GK_YEAR) AS ALLYEAR,(SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 From dbo.PP1 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '1') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY1," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_23 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '2') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY2," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_22 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '3') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY3," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_21 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '4') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY4," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_20 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '5') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY5," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_19 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '6') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY6," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_18 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '7') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY7," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_17 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '8') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY8," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_16 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '9') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY9," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_15 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '10') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY10," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_14 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '11') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY11," & _
                    " (SELECT SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_13 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '12') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_PAY12," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_12 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '1') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE1," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_11 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '2') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE2," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_10 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '3') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE3," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_9 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '4') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE4," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_8 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '5') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE5," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_7 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '6') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE6," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_6 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '7') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE7," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_5 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '8') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE8," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_4 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '9') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE9," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_3 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '10') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE10," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_2 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '11') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE11," & _
                    " (SELECT SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1 FROM dbo.PP1 AS PP1_1 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1) AND (MONTH(PP1_PAY_DATE) = '12') GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_INCLUDE12" & _
                    " From dbo.GK Where (GK_YEAR Is Not Null) GROUP BY GK_YEAR Having (GK_YEAR = " & txtYear.Text & ")"
                                                                           
                                    Call SET_Execute(StrContent)
                                    With Clone_Form.CTReport
                                                .Formulas(0) = "AtYear = '" & txtYear.Text & "'"
                                                .Formulas(1) = "BeforeYear = '" & CInt(txtYear.Text) - 1 & "'"
                                                .ReportFileName = App.Path & "\Report\Rpt_GhpPP1INYEAR.rpt"
                                    End With
                    Case 31
'                                                On Error Resume Next
'                                                Call SET_Execute("DROP VIEW VIEW_GhpPayAllMonth")
'                                                StrContent = vbNullString
                                                StrContent = "ALTER VIEW  VIEW_GhpPayAllMonth AS " & _
                                                "SELECT  GK_YEAR AS ALLYEAR,(SELECT SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS Expr1" & _
                                                " From dbo.PBT5 Where (Month(PBT5_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PBT5_STATUS = 1) AND (PBT5_SET_GK=1)" & _
                                                " GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PAY_PBT5," & _
                                                " (SELECT SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS Expr1" & _
                                                " From dbo.PRD2 Where (Month(PRD2_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PRD2_STATUS = 1) AND (PRD2_SET_GK=1)" & _
                                                " GROUP BY PRD2_YEAR HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PAY_PRD2," & _
                                                " (SELECT SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) AS Expr1" & _
                                                " From dbo.PP1 Where (Month(PP1_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PP1_STATUS = 1) AND (PP1_SET_GK=1)" & _
                                                " GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PAY_PP1," & _
                                                " (SELECT SUM(PBA1_AMOUNT) AS Expr1 From dbo.PBA1 Where (Month(PBA1_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PBA1_STATUS = 1)" & _
                                                " GROUP BY PBA1_YEAR HAVING (PBA1_YEAR = dbo.GK.GK_YEAR)) AS PAY_PBA1," & _
                                                " (SELECT SUM(PBT5_TAXINCLUDE) + SUM(PBT5_DISCOUNT) AS Expr1" & _
                                                " FROM dbo.PBT5 AS PBT5_1 Where (Month(PBT5_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PBT5_STATUS = 1) AND (PBT5_SET_GK=1)" & _
                                                " GROUP BY PBT5_YEAR HAVING (PBT5_YEAR = dbo.GK.GK_YEAR)) AS PBT5_ADD," & _
                                                " (SELECT SUM(PRD2_TAXINCLUDE) + SUM(PRD2_DISCOUNT) AS Expr1 FROM dbo.PRD2 AS PRD2_1" & _
                                                " Where (Month(PRD2_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) GROUP BY PRD2_YEAR" & _
                                                " HAVING (PRD2_YEAR = dbo.GK.GK_YEAR)) AS PRD2_ADD," & _
                                                " (SELECT SUM(PP1_TAXINCLUDE) + SUM(PP1_DISCOUNT) AS Expr1" & _
                                                " FROM dbo.PP1 AS PP1_1 Where (Month(PP1_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PP1_STATUS = 1) AND (PP1_SET_GK=1)" & _
                                                " GROUP BY PP1_YEAR HAVING (PP1_YEAR = dbo.GK.GK_YEAR)) AS PP1_ADD," & _
                                                " (SELECT SUM(PBA1_TAXINCLUDE) + SUM(PBA1_SATANGINCLUDE) AS Expr1" & _
                                                " FROM  dbo.PBA1 AS PBA1_1 Where (Month(PBA1_PAY_DATE) = " & cbMonth1.ListIndex & ") And (PBA1_STATUS = 1) GROUP BY PBA1_YEAR" & _
                                                " HAVING (PBA1_YEAR = dbo.GK.GK_YEAR)) AS PBA1_ADD From dbo.GK" & _
                                                " Where (GK_YEAR Is Not Null) GROUP BY GK_YEAR Having (GK_YEAR = " & txtYear.Text & ")"
                                    Call SET_Execute(StrContent)
                                    With Clone_Form.CTReport
                                                .Formulas(0) = "MonthANDYear = '" & cbMonth1.Text & "  �� " & txtYear.Text & "'"
                                                .ReportFileName = App.Path & "\Report\Rpt_GhpPayMonth.rpt"
                                    End With
        End Select
        With Clone_Form.CTReport
                    .WindowMaxButton = True
                    .WindowMinButton = True
                    .Destination = crptToWindow
                    .WindowState = crptMaximized
                    .WindowShowPrintSetupBtn = True
                    .WindowShowExportBtn = True
                    .Action = 1
        End With
        Me.MousePointer = 0
Exit Sub
ErrPrint:
        Me.MousePointer = 0
        MsgBox Err.Description
End Sub

Private Function Create_View8() As Boolean
        Dim strOwnerType As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        If Cmb_Type.ListIndex > 0 Then
                strOwnerType = " AND (A.OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        
'        strSQL = "DROP VIEW VIEW_SORT_BY_MONEY"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_SORT_BY_MONEY AS " & _
'        " SELECT A.OWNERSHIP_ID,A.PRENAME & A.OWNER_NAME & '  ' & A.OWNER_SURNAME AS FULLNAME, IIF(ISNULL(PBT5_GK_Money),0,PBT5_GK_Money) AS PBT5_MONEY_GK, IIF(ISNULL(PP1_GK_Money),0,PP1_GK_Money) AS PP1_MONEY_GK, IIF(ISNULL(PRD2_GK_Money),0,PRD2_GK_Money) AS PRD2_MONEY_GK,(PBT5_MONEY_GK+PRD2_MONEY_GK+PP1_MONEY_GK) AS SUM_TOTAL,SWITCH(NOT ISNULL(PBT5_YEAR),PBT5_YEAR,NOT ISNULL(PRD2_YEAR),PRD2_YEAR,NOT ISNULL(PP1_YEAR),PP1_YEAR) AS AT_YEAR " & _
'        "FROM ((OWNERSHIP AS A LEFT JOIN (SELECT PBT5.OWNERSHIP_ID,PBT5_GK_Money,PBT5_YEAR FROM (SELECT PBT5.OWNERSHIP_ID,PBT5_GK_Money,PBT5_YEAR " & _
'        "FROM OWNERSHIP INNER JOIN PBT5 ON OWNERSHIP.OWNERSHIP_ID=PBT5.OWNERSHIP_ID WHERE PBT5_SET_GK=1 AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_STATUS=0) WHERE PBT5_YEAR=" & txtYear.Text & " GROUP BY PBT5.OWNERSHIP_ID,PBT5_GK_Money,PBT5_YEAR) AS AAA1 ON A.OWNERSHIP_ID = AAA1.OWNERSHIP_ID) LEFT JOIN " & _
'        "(SELECT PRD2.OWNERSHIP_ID, PRD2.PRD2_GK_Money,PRD2_YEAR FROM (SELECT PRD2.OWNERSHIP_ID,PRD2_GK_Money,PRD2_YEAR " & _
'        " FROM OWNERSHIP INNER JOIN PRD2 ON OWNERSHIP.OWNERSHIP_ID=PRD2.OWNERSHIP_ID WHERE PRD2_SET_GK=1 AND PRD2_YEAR=" & txtYear.Text & " AND PRD2_STATUS=0) WHERE PRD2_YEAR=" & txtYear.Text & " GROUP BY PRD2.OWNERSHIP_ID,PRD2.PRD2_GK_Money,PRD2_YEAR) AS BBB1 ON A.OWNERSHIP_ID = BBB1.OWNERSHIP_ID) LEFT JOIN " & _
'        "(SELECT PP1.OWNERSHIP_ID,PP1_GK_Money,PP1_YEAR FROM (SELECT PP1.OWNERSHIP_ID,PP1_GK_Money,PP1_YEAR " & _
        "FROM OWNERSHIP INNER JOIN PP1 ON OWNERSHIP.OWNERSHIP_ID=PP1.OWNERSHIP_ID WHERE PP1_SET_GK=1 AND PP1_YEAR=" & txtYear.Text & " AND PP1_STATUS=0) WHERE PP1_YEAR=" & txtYear.Text & " GROUP BY PP1.OWNERSHIP_ID,PP1_GK_Money,PP1_YEAR) AS CCC1 ON A.OWNERSHIP_ID=CCC1.OWNERSHIP_ID WHERE NOT( IsNull(PBT5_YEAR) AND IsNull(PRD2_YEAR) AND IsNull(PP1_YEAR)) " & strOwnerType
        
        strSQL = "ALTER VIEW VIEW_SORT_BY_MONEY AS " & _
        "SELECT A.OWNERSHIP_ID, A.PRENAME + A.OWNER_NAME + '  ' + A.OWNER_SURNAME AS FULLNAME, CASE WHEN PBT5_GK_Money IS NULL" & _
        " THEN 0 ELSE PBT5_GK_Money END AS PBT5_Money_GK, CASE WHEN PRD2_GK_Money IS NULL THEN 0 ELSE PRD2_GK_Money END AS PRD2_Money_GK, CASE WHEN PP1_GK_Money IS NULL" & _
        " THEN 0 ELSE PP1_GK_Money END AS PP1_Money_GK, CASE WHEN PBT5_GK_Money IS NULL THEN 0 ELSE PBT5_GK_Money END + CASE WHEN PRD2_GK_Money IS NULL" & _
        " THEN 0 ELSE PRD2_GK_Money END + CASE WHEN PP1_GK_Money IS NULL THEN 0 ELSE PP1_GK_Money END AS SUM_TOTAL,CASE WHEN PBT5_YEAR IS NULL THEN CASE WHEN PRD2_YEAR IS NULL THEN CASE WHEN PP1_YEAR IS NULL" & _
        " THEN 0 ELSE PP1_YEAR END ELSE PRD2_YEAR END ELSE PBT5_YEAR END AS AT_YEAR" & _
        " FROM dbo.OWNERSHIP AS A LEFT OUTER JOIN (SELECT dbo.PBT5.OWNERSHIP_ID, dbo.PBT5.PBT5_GK_Money, dbo.PBT5.PBT5_YEAR" & _
        " FROM dbo.OWNERSHIP INNER JOIN dbo.PBT5 ON dbo.OWNERSHIP.OWNERSHIP_ID = dbo.PBT5.OWNERSHIP_ID" & _
        " Where (dbo.PBT5.PBT5_SET_GK = 1) And (dbo.PBT5.PBT5_YEAR = " & txtYear.Text & ") And (dbo.PBT5.PBT5_STATUS = 0)" & _
        " GROUP BY dbo.PBT5.OWNERSHIP_ID, dbo.PBT5.PBT5_GK_Money, dbo.PBT5.PBT5_YEAR) AS AAA1 ON A.OWNERSHIP_ID = AAA1.OWNERSHIP_ID LEFT OUTER JOIN" & _
        " (SELECT dbo.PRD2.OWNERSHIP_ID, dbo.PRD2.PRD2_GK_Money, dbo.PRD2.PRD2_YEAR" & _
        " FROM dbo.OWNERSHIP AS OWNERSHIP_2 INNER JOIN dbo.PRD2 ON OWNERSHIP_2.OWNERSHIP_ID = dbo.PRD2.OWNERSHIP_ID" & _
        " Where (dbo.PRD2.PRD2_SET_GK = 1) And (dbo.PRD2.PRD2_YEAR = " & txtYear.Text & ") And (dbo.PRD2.PRD2_STATUS = 0)" & _
        " GROUP BY dbo.PRD2.OWNERSHIP_ID, dbo.PRD2.PRD2_GK_Money, dbo.PRD2.PRD2_YEAR) AS BBB1 ON A.OWNERSHIP_ID = BBB1.OWNERSHIP_ID LEFT OUTER JOIN" & _
        " (SELECT dbo.PP1.OWNERSHIP_ID, dbo.PP1.PP1_GK_Money, dbo.PP1.PP1_YEAR" & _
        " FROM dbo.OWNERSHIP AS OWNERSHIP_1 INNER JOIN dbo.PP1 ON OWNERSHIP_1.OWNERSHIP_ID = dbo.PP1.OWNERSHIP_ID" & _
        " Where (dbo.PP1.PP1_SET_GK = 1) And (dbo.PP1.PP1_YEAR = " & txtYear.Text & ") And (dbo.PP1.PP1_STATUS = 0)" & _
        " GROUP BY dbo.PP1.OWNERSHIP_ID, dbo.PP1.PP1_GK_Money, dbo.PP1.PP1_YEAR) AS CCC1 ON A.OWNERSHIP_ID = CCC1.OWNERSHIP_ID" & _
        " WHERE ((AAA1.PBT5_YEAR IS NOT NULL) OR (BBB1.PRD2_YEAR IS NOT NULL) OR (CCC1.PP1_YEAR IS NOT NULL))" & strOwnerType
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View8 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View8 = False
End Function

Private Function Create_View9() As Boolean
        Dim strOwnerType As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        
        If Cmb_Type.ListIndex > 0 Then
                strOwnerType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        
'        strSQL = "DROP VIEW VIEW_RANK_PBT5"
'        Call SET_Execute(strSQL)
        strSQL = "ALTER VIEW VIEW_RANK_PBT5 AS " & _
        " SELECT  TOP (10) OWNERSHIP_ID, PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, CASE WHEN PBT5_GK_Money IS NULL" & _
        " THEN 0 ELSE PBT5_GK_Money END AS PBT5_MONEY_GK, PBT5_YEAR" & _
        " From dbo.PBT5" & _
        " Where (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 1) And (PBT5_STATUS = 0)" & strOwnerType
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        
        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PBT5_GK_Money, PBT5_YEAR" & _
                            " ORDER BY PBT5_GK_Money DESC "
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View9 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View9 = False
End Function

Private Function Create_View10() As Boolean
        Dim strOwnerType As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        
        If Cmb_Type.ListIndex > 0 Then
                strOwnerType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        
'        strSQL = "DROP VIEW VIEW_RANK_PRD2"
'        Call SET_Execute(strSQL)
        strSQL = "ALTER VIEW VIEW_RANK_PRD2 AS " & _
        " SELECT TOP (10) OWNERSHIP_ID, PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, CASE WHEN PRD2_GK_Money IS NULL THEN 0 ELSE PRD2_GK_Money END AS PRD2_MONEY_GK, PRD2_YEAR" & _
        " FROM dbo.PRD2" & _
        " WHERE (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 1) And (PRD2_STATUS = 0)" & strOwnerType
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        
        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PRD2_GK_Money, PRD2_YEAR" & _
            " ORDER BY PRD2_GK_Money DESC"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View10 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View10 = False
End Function

Private Function Create_View11() As Boolean
        Dim strOwnerType As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        
        If Cmb_Type.ListIndex > 0 Then
                strOwnerType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        
'        strSQL = "DROP VIEW VIEW_RANK_PP1"
'        Call SET_Execute(strSQL)
        strSQL = "ALTER VIEW VIEW_RANK_PP1 AS " & _
        " SELECT TOP (10) OWNERSHIP_ID, PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, CASE WHEN PP1_GK_Money IS NULL THEN 0 ELSE PP1_GK_Money END AS PP1_MONEY_GK, PP1_YEAR" & _
        " FROM dbo.PP1" & _
        " WHERE (PP1_YEAR = " & txtYear.Text & " ) And (PP1_SET_GK = 1) And (PP1_STATUS = 0)" & strOwnerType
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PP1_GK_Money, PP1_YEAR" & _
            " ORDER BY PP1_GK_Money DESC"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View11 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View11 = False
End Function

Private Function Create_View7() As Boolean
        Dim strSQL7 As String
        Globle_Connective.BeginTrans
On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
'        strSQL = "DROP VIEW VIEW_COUNT_SUMMARY_ALL"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_COUNT_SUMMARY_ALL AS " & _
'        " SELECT IIF(ISNULL(TOTAL_BUILDING),0,TOTAL_BUILDING) AS BUILDING_TOTAL,IIF(ISNULL(BUILDING_IN_TAX),0,BUILDING_IN_TAX) AS BUILDING_TAX_IN,IIF(ISNULL(BUILDING_OUT_TAX),0,BUILDING_OUT_TAX) AS BUILDING_TAX_OUT,IIF(ISNULL(TOTAL_LANDDATA),0,TOTAL_LANDDATA) AS LANDDATA_TOTAL " & _
'        ", IIF(ISNULL(LANDDATA_IN_TAX),0,LANDDATA_IN_TAX) AS LANDDATA_TAX_IN,IIF(ISNULL(LANDDATA_OUT_TAX),0,LANDDATA_OUT_TAX) AS LANDDATA_TAX_OUT,IIF(ISNULL(TOTAL_SIGNBOARD),0,TOTAL_SIGNBOARD) AS SIGNBOARD_TOTAL,IIF(ISNULL(SIGNBOARD_IN_TAX),0,SIGNBOARD_IN_TAX) AS SIGNBOARD_TAX_IN, IIF(ISNULL(SIGNBOARD_OUT_TAX),0,SIGNBOARD_OUT_TAX) AS SIGNBOARD_TAX_OUT,IIF(ISNULL(TOTAL_LICENCE),0,TOTAL_LICENCE) AS LICENCE_TOTAL, IIF(ISNULL(TOTAL_PRD2),0,TOTAL_PRD2) AS PRD2_TOTAL, IIF(ISNULL(TOTAL_PP1),0,TOTAL_PP1) AS PP1_TOTAL, IIF(ISNULL(TOTAL_PBT5),0,TOTAL_PBT5) AS PBT5_TOTAL, IIF(ISNULL(TOTAL_PBA1),0,TOTAL_PBA1) AS PBA1_TOTAL " & _
'        " FROM (SELECT COUNT(BUILDING_ID) AS TOTAL_BUILDING FROM BUILDINGDATA ) AS A, (SELECT COUNT(BUILDING_ID) AS BUILDING_IN_TAX FROM BUILDINGDATA WHERE FLAG_PAYTAX=1) AS B, (SELECT COUNT(BUILDING_ID) AS BUILDING_OUT_TAX FROM BUILDINGDATA WHERE FLAG_PAYTAX=0) AS C, (SELECT COUNT(SIGNBORD_ID) AS TOTAL_SIGNBOARD FROM SIGNBORDDATA) AS D, (SELECT COUNT(SIGNBORD_ID) AS SIGNBOARD_IN_TAX FROM SIGNBORDDATA WHERE FLAG_PAYTAX=1) AS E, (SELECT COUNT(SIGNBORD_ID) AS SIGNBOARD_OUT_TAX FROM SIGNBORDDATA WHERE FLAG_PAYTAX=0) AS F, (SELECT COUNT(LAND_ID) AS TOTAL_LANDDATA FROM LANDDATA) AS G, (SELECT COUNT(LAND_ID) AS LANDDATA_IN_TAX " & _
'        " FROM LANDDATA WHERE FLAG_PAYTAX=1) AS H, (SELECT COUNT(LAND_ID) AS LANDDATA_OUT_TAX FROM LANDDATA WHERE FLAG_PAYTAX=0) AS I, (SELECT COUNT(LICENSE_BOOK+LICENSE_NO) AS TOTAL_LICENCE FROM LICENSEDATA) AS J " & _
'        " , (SELECT SUM(PBT5_AMOUNT+ PBT5_TAXINCLUDE+PBT5_SATANGINCLUDE) AS TOTAL_PBT5 " & _
'        " FROM PBT5 WHERE PBT5_YEAR=" & txtYear & ") AS K, (SELECT SUM(PRD2_RENTYEAR_TOTAL+PRD2_TAXINCLUDE+PRD2_SATANGINCLUDE) AS TOTAL_PRD2 " & _
'        " FROM PRD2 WHERE PRD2_YEAR=" & txtYear & ") AS L, (SELECT SUM(PP1_AMOUNT+PP1_TAXINCLUDE+PP1_SATANGINCLUDE) AS TOTAL_PP1" & _
'        " FROM PP1 WHERE PP1_YEAR=" & txtYear & ") AS M, (SELECT SUM(PBA1_AMOUNT+PBA1_TAXINCLUDE+PBA1_SATANGINCLUDE) AS TOTAL_PBA1 " & _
'        " FROM PBA1 WHERE PBA1_YEAR=" & txtYear & ") AS N"
        strSQL = "ALTER VIEW VIEW_COUNT_SUMMARY_ALL AS SELECT CASE WHEN A.TOTAL_BUILDING IS NULL THEN 0 ELSE A.TOTAL_BUILDING END AS BUILDING_TOTAL, CASE WHEN B.BUILDING_IN_TAX IS NULL " & _
            " THEN 0 ELSE B.BUILDING_IN_TAX END AS BUILDING_TAX_IN, CASE WHEN C.BUILDING_OUT_TAX IS NULL THEN 0 ELSE C.BUILDING_OUT_TAX END AS BUILDING_TAX_OUT, CASE WHEN TOTAL_LANDDATA IS NULL" & _
            " THEN 0 ELSE TOTAL_LANDDATA END AS LANDDATA_TOTAL, CASE WHEN LANDDATA_IN_TAX IS NULL THEN 0 ELSE LANDDATA_IN_TAX END AS LANDDATA_TAX_IN, CASE WHEN LANDDATA_OUT_TAX IS NULL" & _
            " THEN 0 ELSE LANDDATA_OUT_TAX END AS LANDDATA_TAX_OUT, CASE WHEN TOTAL_SIGNBOARD IS NULL THEN 0 ELSE TOTAL_SIGNBOARD END AS SIGNBOARD_TOTAL, CASE WHEN SIGNBOARD_IN_TAX IS NULL" & _
            " THEN 0 ELSE SIGNBOARD_IN_TAX END AS SIGNBOARD_TAX_IN, CASE WHEN SIGNBOARD_OUT_TAX IS NULL THEN 0 ELSE SIGNBOARD_OUT_TAX END AS SIGNBOARD_TAX_OUT, CASE WHEN TOTAL_LICENCE IS NULL" & _
            " THEN 0 ELSE TOTAL_LICENCE END AS LICENCE_TOTAL, CASE WHEN TOTAL_PRD2 IS NULL THEN 0 ELSE TOTAL_PRD2 END AS PRD2_TOTAL,CASE WHEN TOTAL_PP1 IS NULL THEN 0 ELSE TOTAL_PP1 END AS PP1_TOTAL, CASE WHEN TOTAL_PBT5 IS NULL" & _
            " THEN 0 ELSE TOTAL_PBT5 END AS PBT5_TOTAL, CASE WHEN TOTAL_PBA1 IS NULL THEN 0 ELSE TOTAL_PBA1 END AS PBA1_TOTAL" & _
            " FROM (SELECT COUNT(BUILDING_ID) AS TOTAL_BUILDING FROM dbo.BUILDINGDATA) AS A CROSS JOIN (SELECT COUNT(BUILDING_ID) AS BUILDING_IN_TAX FROM dbo.BUILDINGDATA AS BUILDINGDATA_1" & _
            " WHERE (FLAG_PAYTAX = 1)) AS B CROSS JOIN (SELECT COUNT(BUILDING_ID) AS BUILDING_OUT_TAX FROM dbo.BUILDINGDATA AS BUILDINGDATA_2 WHERE (FLAG_PAYTAX = 0)) AS C CROSS JOIN (SELECT COUNT(SIGNBORD_ID) AS TOTAL_SIGNBOARD" & _
            " FROM dbo.SIGNBORDDATA) AS D CROSS JOIN (SELECT COUNT(SIGNBORD_ID) AS SIGNBOARD_IN_TAX FROM dbo.SIGNBORDDATA AS SIGNBORDDATA_2 WHERE (FLAG_PAYTAX = 1)) AS E CROSS JOIN (SELECT COUNT(SIGNBORD_ID) AS SIGNBOARD_OUT_TAX" & _
            " FROM dbo.SIGNBORDDATA AS SIGNBORDDATA_1 WHERE (FLAG_PAYTAX = 0)) AS F CROSS JOIN (SELECT COUNT(LAND_ID) AS TOTAL_LANDDATA FROM dbo.LANDDATA) AS G CROSS JOIN (SELECT COUNT(LAND_ID) AS LANDDATA_IN_TAX" & _
            " FROM dbo.LANDDATA AS LANDDATA_2 WHERE (FLAG_PAYTAX = 1)) AS H CROSS JOIN (SELECT COUNT(LAND_ID) AS LANDDATA_OUT_TAX FROM dbo.LANDDATA AS LANDDATA_1 WHERE (FLAG_PAYTAX = 0)) AS I CROSS JOIN" & _
            " (SELECT COUNT(LICENSE_BOOK + LICENSE_NO) AS TOTAL_LICENCE FROM dbo.LICENSEDATA) AS J CROSS JOIN (SELECT SUM(PBT5_AMOUNT + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS TOTAL_PBT5 FROM dbo.PBT5" & _
            " WHERE (PBT5_YEAR = " & txtYear & ")) AS K CROSS JOIN (SELECT SUM(PRD2_RENTYEAR_TOTAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS TOTAL_PRD2 FROM dbo.PRD2" & _
            " WHERE (PRD2_YEAR = " & txtYear & ")) AS L CROSS JOIN (SELECT SUM(PP1_AMOUNT + PP1_TAXINCLUDE + PP1_DISCOUNT) AS TOTAL_PP1 FROM dbo.PP1" & _
            " WHERE (PP1_YEAR = " & txtYear & ")) AS M CROSS JOIN (SELECT SUM(PBA1_AMOUNT + PBA1_TAXINCLUDE + PBA1_DISCOUNT) AS TOTAL_PBA1 FROM dbo.PBA1" & _
            " WHERE (PBA1_YEAR = " & txtYear & ")) AS N"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View7 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View7 = False
End Function

Private Sub MakeThai()
        Dim n As Byte
        Dim i As Byte
        
        n = 161
        For i = 0 To 43
                If n = 163 Or n = 165 Then
                    tmpArr(i) = Chr$(n + 1)
                    n = n + 1
                Else
                    tmpArr(i) = Chr$(n)
                End If
                n = n + 1
        Next i
End Sub

Private Sub cbMonth1_Click()
        On Error Resume Next
        If TreeView1.SelectedItem.Index = 21 Then
            cbMonth2.ListIndex = cbMonth1.ListIndex
        End If
End Sub

Private Sub cbMonth2_Click()
        On Error Resume Next
        If TreeView1.SelectedItem.Index = 26 Then
            cbMonth1.ListIndex = cbMonth2.ListIndex
        End If
End Sub

Private Sub cbPerform_Click()
        Select Case cbPerform.Text
                Case "�����Ẻ", "��������Ẻ"
                            If cbPerform.ListIndex = 0 And LB_Menu.Tag = "7" Then
                                    chkAccept.Enabled = True: chkAccept.Value = Unchecked
                                    chkListName.Visible = False: chkListName.Value = Unchecked
                                    Img_Chk(9).Visible = False: Label1(13).Visible = False
                                    chkPerformAll.Visible = True: chkPerformAll.Value = Unchecked: chkPerformAll.Top = chkListName.Top: chkPerformAll.Enabled = True
                                    Img_Chk(10).Visible = True: Label1(15).Visible = True: Img_Chk(10).Top = Img_Chk(9).Top: Label1(15).Top = Label1(13).Top
                            ElseIf cbPerform.ListIndex = 1 And LB_Menu.Tag = "7" Then
                                    chkAccept.Enabled = False: chkAccept.Value = Unchecked
                                    chkListName.Enabled = True: chkListName.Value = Unchecked: chkListName.Visible = True
                                    Img_Chk(9).Visible = True: Label1(13).Visible = True
                                    chkPerformAll.Visible = False: Img_Chk(10).Visible = False: Label1(15).Visible = False
                            End If
                            If cbPerform.Text = "��������Ẻ" Then
                                    Img_Chk(5).Visible = False
                                    DTPicker1.Enabled = False
                                    DTPicker2.Enabled = False
                                    Text1.Enabled = False
                                    Text2.Enabled = False
                            Else
                                    Img_Chk(5).Visible = True
                                    DTPicker1.Enabled = True
                                    DTPicker2.Enabled = True
                                    Text1.Enabled = True
                                    Text2.Enabled = True
                            End If
                Case "����������Թ����", "�������¹�ŧ����"
                            cb_Part.Enabled = False
                Case "����͹��Ѿ���Թ", "��è�˹��·�Ѿ���Թ"
                            cb_Part.Enabled = True
        End Select
End Sub

Private Sub cbType_Click()
On Error Resume Next
        If TreeView1.SelectedItem.Index = 10 Or TreeView1.SelectedItem.Index = 7 Then
                If cbPerform.ListIndex = 1 Then
                        chkAccept.Enabled = False
                Else
                        chkAccept.Enabled = True
                End If
        Else
                chkAccept.Enabled = False
        End If
        chkAccept.Value = Unchecked
End Sub

Private Sub ChkAllyr_Click()
         If Label1(9).Caption = "��ػ�ʹ���" Then
                If ChkAllyr.Value = Checked Then
                        cbType.Enabled = False: cbType.BackColor = &HE0E0E0
                Else
                        cbType.Enabled = True: cbType.BackColor = &HFFFFFF
                End If
        End If
        If LB_Menu.Tag = "8" Then
                If ChkAllyr.Value = Checked Then
                        Label1(9).Caption = "Ẻ�Ǵ"
                        cbType.Enabled = True
                        Img_Chk(7).Visible = True
'                        DTPicker1.Enabled = False
'                        DTPicker2.Enabled = False
'                        Img_Chk(5).Visible = False
'                        Text1.Enabled = False
'                        Text2.Enabled = False
                        Text1.BackColor = &HE0E0E0: Text2.BackColor = &HE0E0E0
                Else
                        Label1(9).Caption = "੾�з���ջ����Թ����"
'                        DTPicker1.Enabled = True
'                        DTPicker2.Enabled = True
'                        Img_Chk(5).Visible = True
'                        Text1.Enabled = True
'                        Text2.Enabled = True
                        cbType.Enabled = False
                        Img_Chk(7).Visible = False
                        Text1.BackColor = &HFFFFFF: Text2.BackColor = &HFFFFFF
                End If
        End If
End Sub

Private Sub chkMonth_Click()
            If chkMonth.Value = Unchecked Then
                    cbMonth1.Locked = True
                    cbMonth2.Locked = True
                    cbMonth1.ListIndex = 0
                    cbMonth2.ListIndex = 0
            Else
                    cbMonth1.Locked = False
                    cbMonth2.Locked = False
            End If
End Sub

Private Sub chkPerformAll_Click()
        If chkPerformAll.Value = Checked Then
                chkAccept.Enabled = False
                Img_Chk(8).Visible = False
'                DTPicker1.Enabled = True
'                DTPicker2.Enabled = True
'                Text1.Enabled = True: Text2.Enabled = True
'                Text1.BackColor = &HFFFFFF: Text2.BackColor = &HFFFFFF
'                Img_Chk(5).Visible = True
        Else
                chkAccept.Enabled = True
                Img_Chk(8).Visible = True
'                DTPicker1.Enabled = False
'                DTPicker2.Enabled = False
'                Text1.Enabled = False: Text2.Enabled = False
'                Text1.BackColor = &HE0E0E0: Text2.BackColor = &HE0E0E0
'                Img_Chk(5).Visible = False
        End If
        chkAccept.Value = Unchecked
End Sub

Private Sub Combo1_KeyPress(Index As Integer, KeyAscii As Integer)
        Dim i As Byte
        If KeyAscii < 26 Then Exit Sub
        If KeyAscii >= 161 And KeyAscii <= 206 Then
                If Len(Trim$(Combo1(Index).Text)) = 1 Then Combo1(Index).Text = ""
                For i = 0 To Combo1.Count - 1
                        If Chr(KeyAscii) = Combo1(Index).List(i) Then Combo1(Index).ListIndex = i
                Next i
        Else
                KeyAscii = 0
        End If
End Sub

Private Sub DTPicker1_Change()
        Text1.Text = Format$(DTPicker1.Value, "dd/mm/yyyy")
End Sub

Private Sub DTPicker2_Change()
        Text2.Text = Format$(DTPicker2.Value, "dd/mm/yyyy")
End Sub


Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
        Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()
        TreeView1.ImageList = ImageList1
        Dim i As Byte
        Set rs = New ADODB.Recordset
        Set Rs2 = New ADODB.Recordset
        Set Rs3 = New ADODB.Recordset
        Set RsTotal = New ADODB.Recordset
        Combo1(0).AddItem ""
        Combo1(1).AddItem ""
        DTPicker1.Value = Date
        DTPicker2.Value = Date
        Text1.Text = DTPicker1.Value: Text2.Text = DTPicker2.Value
        ChkAllyr.Enabled = False
        chkGK.Enabled = False
        chkMonth.Enabled = False
        Combo1(0).Enabled = False
        Combo1(1).Enabled = False
        Cmb_Type.Enabled = False
        cbMonth1.Enabled = False
        cbMonth2.Enabled = False
        txtYear.Text = Right$(Date, 4)
        For i = 1 To 46
             If 160 + i <> 163 And 160 + i <> 165 Then
                    Combo1(0).AddItem Chr$(160 + i)
                    Combo1(1).AddItem Chr$(160 + i)
             End If
        Next i
        cbMonth1.ListIndex = 0
        cbMonth2.ListIndex = 0
        Cmb_Type.ListIndex = 0
        cbPerform.ListIndex = 0
        cbType.ListIndex = 0
           Dim nodX As Node ' Create a tree.
        
           Set nodX = TreeView1.Nodes.Add(, , "Root", "��������������§ҹ��ػ", 1) '1
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt1", "����¹��Ѿ���Թ������", 3, 2) '2
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt2", "����¹��Ѿ���Թ����¹�ŧ", 3, 2) '3
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep1", "..........................................................") '4
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt3", "�.�.1 ��� �.�.2 (��ºؤ��)", 3, 2) '5
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt32", "�.�.1 �����ࡳ���ҧ�Ѻ (��ºؤ��)", 3, 2) '6
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt33", "�.�.2 �����ࡳ���ҧ�Ѻ (��ºؤ��)", 3, 2) '7
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt36", "��ª��ͺѭ���������", 3, 2)
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt34", "�ѭ���١˹�� (��ºؤ��)", 3, 2) '8
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt35", "��§ҹ�鹡�˹�����駻����Թ", 3, 2) '9
           Set nodX = TreeView1.Nodes.Add("Rpt3", tvwChild, "Rpt37", "��§ҹ��û�Ѻ�����š.�.1", 3, 2) '9
           
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt4", "�.�.1 �����ࡳ���ҧ�Ѻ (������ѡ��)", 3, 2) '9,10
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep2", "..........................................................") '10,11
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt5", "�š�û���ҳ����", 3, 2) '11,12
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt6", "���������Ш��ѹ", 3, 2) '12,13
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt7", "��������㹻է�����ҳ", 3, 2) '13,14
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt8", "���������㹻է�����ҳ", 3, 2) '14,15
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Rpt9", "�Ѵ�ѹ�Ѻ����ҧ��������", 3, 2) '15,16
           Set nodX = TreeView1.Nodes.Add("Rpt9", tvwChild, "Rpt92", "�Ѵ�ѹ�Ѻ����ҧ�����������������", 3, 2) '16,17
           Set nodX = TreeView1.Nodes.Add("Rpt9", tvwChild, "Rpt93", "�Ѵ�ѹ�Ѻ����ҧ�������պ��ا��ͧ���", 3, 2) '17,18
           Set nodX = TreeView1.Nodes.Add("Rpt9", tvwChild, "Rpt94", "�Ѵ�ѹ�Ѻ����ҧ���������ç���͹��з��Թ", 3, 2) '18,19
           Set nodX = TreeView1.Nodes.Add("Rpt9", tvwChild, "Rpt95", "�Ѵ�ѹ�Ѻ����ҧ�������ջ���", 3, 2) '19,20
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep3", "..........................................................") '20,21
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph1", "��ҿ��ػ������èѴ������", 4, 5) '21,22
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph2", "��ҿ��ػ�������������º���դ�ҧ", 4, 5) '22,23
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep4", "..........................................................") '23,24
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph3", "��ҿ�ʹ�Ѻ���պ��ا��ͧ����Шӻ�", 4, 5) '24,25
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph4", "��ҿ�ʹ�Ѻ�����ç���͹��Шӻ�", 4, 5) '25,26
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph5", "��ҿ�ʹ�Ѻ���ջ��»�Шӻ�", 4, 5) '26,27
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep5", "..........................................................") '27,28
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "RptGph6", "��ҿ��ػ�ʹ��������Ѻ��Ш���͹", 4, 5) '28,29
           Set nodX = TreeView1.Nodes.Add(1, tvwChild, "Sep6", "..........................................................") '29,30
           nodX.EnsureVisible
           Set nodX = Nothing
           TreeView1.Nodes(1).Selected = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
        On Error GoTo ErrUnload
        Set rs = Nothing
        Set Rs2 = Nothing
        Set Rs3 = Nothing
        Set RsTotal = Nothing
        Erase tmpArr
        Call SET_Execute("UPDATE Temp_GK SET PBT5_COUNT=0,PBT5_SUMTAX=0,PP1_COUNT=0,PP1_SUMTAX=0,PRD2_COUNT=0,PRD2_SUMTAX=0,TOTAL_COUNT=0")
        Set Frm_PrintSummary = Nothing
Exit Sub
ErrUnload:
        MsgBox Err.Description
End Sub

Private Function Create_View() As Boolean '�.�.1
        Dim strType As String
        Dim strPBT5 As String
        Dim strPRD2 As String
        Dim strPP1 As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        
        strSQL = ""
        Me.MousePointer = 11
        strType = ""
        If Cmb_Type.ListIndex > 0 Then
                strType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        If chkGK.Value = Checked Then   '�ѧ������Ẻ
                strPBT5 = " AND  (PBT5_DATE IS NULL)"
                strPRD2 = " AND  (PRD2_DATE IS NULL)"
                strPP1 = " AND  (PP1_DATE IS NULL)"
        Else
                strPBT5 = " AND (PBT5_IN_GK <> 2)"
                strPRD2 = " AND (PRD2_IN_GK <> 2)"
                strPP1 = " AND (PP1_IN_GK <> 2)"
        End If
        
        strSQL = "ALTER VIEW [dbo].[VIEW_GK1] AS" & _
            " SELECT  A_1.OWNERSHIP_ID, A_1.OWNER_NAME + '  ' + A_1.OWNER_SURNAME AS FULLNAME, CASE WHEN PBT5_GK IS NULL" & _
            " THEN 0 ELSE PBT5_GK END AS PBT5_GK_MONEY, CASE WHEN PP1_GK IS NULL THEN 0 ELSE PP1_GK END AS PP1_GK_MONEY,CASE WHEN PRD2_GK IS NULL THEN 0 ELSE PRD2_GK END AS PRD2_GK_MONEY, CASE WHEN ADD2_PBT5 IS NULL" & _
            " THEN 0 ELSE ADD2_PBT5 END AS ADD_PBT5, CASE WHEN ADD2_PP1 IS NULL THEN 0 ELSE ADD2_PP1 END AS ADD_PP1,CASE WHEN ADD2_PRD2 IS NULL THEN 0 ELSE ADD2_PRD2 END AS ADD_PRD2, AA_1.TIMES_PAY_PBT5, AA_2.TIMES_PAY_PRD2," & _
            " AA_3.TIMES_PAY_PP1, AA_1.PBT5_YEAR, AA_2.PRD2_YEAR, AA_3.PP1_YEAR, CASE WHEN SUM_REALKEEP_PBT5 IS NULL THEN 0 ELSE SUM_REALKEEP_PBT5 END AS REAL_KEEP1, CASE WHEN SUM_REALKEEP_PRD2 IS NULL" & _
            " THEN 0 ELSE SUM_REALKEEP_PRD2 END AS REAL_KEEP2, CASE WHEN SUM_REALKEEP_PP1 IS NULL THEN 0 ELSE SUM_REALKEEP_PP1 END AS REAL_KEEP3, AA_1.SUM_PBT5_AMOUNT, AA_2.SUM_PRD2_AMOUNT, AA_3.SUM_PP1_AMOUNT,A_1.ID_GARD" & _
            " FROM (SELECT AAA.OWNERSHIP_ID, AAA.OWNER_NAME, AAA.OWNER_SURNAME, A.ID_GARD FROM (SELECT     OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME From dbo.PBT5 Where (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 1)" & _
            " Union SELECT OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME From dbo.PRD2 Where (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 1) Union SELECT OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME From dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") AND (PP1_SET_GK = 1)) AS AAA LEFT OUTER JOIN" & _
            " dbo.OWNERSHIP AS A ON AAA.OWNERSHIP_ID = A.OWNERSHIP_ID) AS A_1 LEFT OUTER JOIN (SELECT OWNERSHIP_ID, PBT5_GK_Money AS PBT5_GK, TIMES_PAY_PBT5,SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) - PBT5_GK_Money AS ADD2_PBT5, PBT5_YEAR," & _
            " SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS SUM_REALKEEP_PBT5, SUM(PBT5_AMOUNT) AS SUM_PBT5_AMOUNT FROM (SELECT PBT5_AMOUNT, PBT5_DATE, OWNERSHIP_ID, PBT5_GK_Money, PBT5_AMOUNT_REAL, PBT5_DISCOUNT," & _
            " PBT5_TAXINCLUDE, CASE MONTH(PBT5_PAY_DATE) WHEN 10 THEN '1' WHEN 11 THEN '1' WHEN 12 THEN '2' WHEN 1 THEN '2' WHEN 2 THEN '3' WHEN 3 THEN '3' WHEN 4 THEN '4' WHEN 5 THEN '4' WHEN 6 THEN '5' WHEN 7 THEN '5' WHEN 8 THEN '6' WHEN 9 THEN '6' END AS TIMES_PAY_PBT5," & _
            " PBT5_YEAR FROM dbo.PBT5 AS PBT5_1 WHERE (PBT5_SET_GK = 1) " & strPBT5 & strType & " AND (PBT5_YEAR = " & txtYear.Text & ")) AS BB Where (PBT5_YEAR = " & txtYear.Text & ") GROUP BY OWNERSHIP_ID, PBT5_GK_Money, TIMES_PAY_PBT5, PBT5_YEAR) AS AA_1 ON" & _
            " A_1.OWNERSHIP_ID = AA_1.OWNERSHIP_ID LEFT OUTER JOIN (SELECT OWNERSHIP_ID, PRD2_GK_Money AS PRD2_GK, TIMES_PAY_PRD2,SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) - PRD2_GK_Money AS ADD2_PRD2, PRD2_YEAR," & _
            " SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS SUM_REALKEEP_PRD2, SUM(PRD2_RENTYEAR_TOTAL) AS SUM_PRD2_AMOUNT FROM (SELECT     PRD2_RENTYEAR_TOTAL, PRD2_DATE, OWNERSHIP_ID, PRD2_GK_Money, PRD2_AMOUNT_REAL, PRD2_DISCOUNT," & _
            " PRD2_TAXINCLUDE, CASE MONTH(PRD2_PAY_DATE) WHEN 10 THEN '1' WHEN 11 THEN '1' WHEN 12 THEN '2' WHEN 1 THEN '2' WHEN 2 THEN '3' WHEN 3 THEN '3' WHEN 4 THEN '4' WHEN 5 THEN '4' WHEN 6 THEN '5' WHEN 7 THEN '5' WHEN 8 THEN '6' WHEN 9 THEN '6' END AS TIMES_PAY_PRD2," & _
            " PRD2_YEAR FROM dbo.PRD2 AS PRD2_1 WHERE (PRD2_SET_GK = 1) " & strPRD2 & strType & " AND (PRD2_YEAR = " & txtYear.Text & ")) AS CC Where (PRD2_YEAR = " & txtYear.Text & ") GROUP BY OWNERSHIP_ID, PRD2_GK_Money, TIMES_PAY_PRD2, PRD2_YEAR) AS AA_2 ON" & _
            " A_1.OWNERSHIP_ID = AA_2.OWNERSHIP_ID LEFT OUTER JOIN (SELECT OWNERSHIP_ID, PP1_GK_Money AS PP1_GK, TIMES_PAY_PP1, SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) - PP1_GK_Money AS ADD2_PP1, PP1_YEAR, SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT)" & _
            " AS SUM_REALKEEP_PP1, SUM(PP1_AMOUNT) AS SUM_PP1_AMOUNT FROM (SELECT PP1_AMOUNT, PP1_DATE, OWNERSHIP_ID, PP1_GK_Money, PP1_DISCOUNT, PP1_AMOUNT_REAL, PP1_TAXINCLUDE,Case Month(PP1_PAY_DATE) WHEN 10 THEN '1' WHEN 11 THEN '1' WHEN 12 THEN '2' WHEN 1 THEN '2' WHEN 2 THEN '3' WHEN 3 THEN '3' WHEN 4 THEN" & _
            " '4' WHEN 5 THEN '4' WHEN 6 THEN '5' WHEN 7 THEN '5' WHEN 8 THEN '6' WHEN 9 THEN '6' END AS TIMES_PAY_PP1,PP1_YEAR FROM dbo.PP1 AS PP1_1 WHERE (PP1_SET_GK = 1) " & strPP1 & strType & " AND (PP1_YEAR = " & txtYear.Text & ")) AS DD Where (PP1_YEAR = " & txtYear.Text & ")" & _
            " GROUP BY OWNERSHIP_ID, PP1_GK_Money, TIMES_PAY_PP1, PP1_YEAR) AS AA_3 ON A_1.OWNERSHIP_ID = AA_3.OWNERSHIP_ID WHERE ((AA_1.PBT5_YEAR IS NOT NULL) OR (AA_2.PRD2_YEAR IS NOT NULL) OR (AA_3.PP1_YEAR IS NOT NULL))"
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                MsgBox Err.Description
                Me.MousePointer = 0
                Create_View = False
        Exit Function
End Function

Private Function Create_View15() As Boolean '��ª��ͺѭ���������
        Dim strType As String
        Dim strCon1 As String
        Dim strCon2 As String
        Dim strCon3 As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        
'        strSQL = ""
        Me.MousePointer = 11
        strType = ""
        If Cmb_Type.ListIndex > 0 Then
                strType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        Select Case cbType.ListIndex
                Case 0
                        strCon1 = " AND (PBT5_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-01-31')"
                        strCon2 = " AND (PRD2_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-01-31')"
                        strCon3 = " AND (PP1_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-01-31')"
                Case 1
                        strCon1 = " AND (PBT5_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-02-01' AND '" & CInt(txtYear.Text) - 543 & "-05-31')"
                        strCon2 = " AND (PRD2_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-02-01' AND '" & CInt(txtYear.Text) - 543 & "-05-31')"
                        strCon3 = " AND (PP1_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-02-01' AND '" & CInt(txtYear.Text) - 543 & "-05-31')"
                Case 2
                        strCon1 = " AND (PBT5_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-06-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')"
                        strCon2 = " AND (PRD2_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-06-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')"
                        strCon3 = " AND (PP1_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 543 & "-06-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')"
        End Select
        
        strSQL = "ALTER VIEW [dbo].[VIEW_GK_OTHER] AS" & _
            " SELECT  DISTINCT A_1.OWNERSHIP_ID, A_1.OWNER_NAME + '   ' + A_1.OWNER_SURNAME AS FULLNAME, A_1.ID_GARD, CASE WHEN PBT5_SET_GK IS NULL " & _
            " THEN 10 ELSE PBT5_SET_GK END AS PBT5_GK, CASE WHEN sum_pbt5 IS NULL THEN 0 ELSE sum_pbt5 END AS sum_pbt5,CASE WHEN PRD2_SET_GK IS NULL THEN 10 ELSE PRD2_SET_GK END AS PRD2_GK, CASE WHEN sum_PRD2 IS NULL" & _
            " THEN 0 ELSE sum_PRD2 END AS sum_PRD2, CASE WHEN PP1_SET_GK IS NULL THEN 10 ELSE PP1_SET_GK END AS PP1_GK,CASE WHEN sum_PP1 IS NULL THEN 0 ELSE sum_PP1 END AS sum_PP1, A_1.OWNER_NAME,CCC.PBT5_PAY_DATE,EEE.PRD2_PAY_DATE,GGG.PP1_PAY_DATE" & _
            " FROM (SELECT AAA.OWNERSHIP_ID, AAA.OWNER_NAME, AAA.OWNER_SURNAME, A.ID_GARD" & _
            " FROM (SELECT OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME FROM dbo.PBT5 WHERE (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK <> 1) AND (PBT5_STATUS=1)" & strCon1 & strType & _
            " Union SELECT OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME FROM dbo.PRD2 WHERE (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK <> 1) AND (PRD2_STATUS=1)" & strCon2 & strType & _
            " Union SELECT OWNERSHIP_ID, OWNER_NAME, OWNER_SURNAME FROM dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") AND (PP1_SET_GK <> 1) AND (PP1_STATUS=1)" & strCon3 & strType & ") AS AAA LEFT OUTER JOIN" & _
            " dbo.OWNERSHIP AS A ON AAA.OWNERSHIP_ID = A.OWNERSHIP_ID) AS A_1 LEFT OUTER JOIN" & _
            " (SELECT PBT5_2.OWNERSHIP_ID, BBB.sum_pbt5, PBT5_2.PBT5_SET_GK, PBT5_2.OWNER_NAME, PBT5_2.OWNER_SURNAME,PBT5_2.PBT5_PAY_DATE" & _
            " FROM dbo.PBT5 AS PBT5_2 INNER JOIN (SELECT OWNERSHIP_ID, SUM(PBT5_AMOUNT_REAL + PBT5_DISCOUNT + PBT5_TAXINCLUDE) AS sum_pbt5" & _
            " FROM dbo.PBT5 AS PBT5_1 WHERE (PBT5_SET_GK <> 1) And (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_STATUS = 1)" & strCon1 & " GROUP BY OWNERSHIP_ID) AS BBB ON BBB.OWNERSHIP_ID = PBT5_2.OWNERSHIP_ID" & _
            " WHERE (PBT5_2.PBT5_SET_GK <> 1) And (PBT5_2.PBT5_YEAR = " & txtYear.Text & ") And (PBT5_2.PBT5_STATUS = 1) " & strCon1 & strType & _
            " ) AS CCC ON A_1.OWNERSHIP_ID = CCC.OWNERSHIP_ID LEFT OUTER JOIN" & _
            " (SELECT PRD2_2.OWNERSHIP_ID, DDD.sum_PRD2, PRD2_2.PRD2_SET_GK, PRD2_2.OWNER_NAME, PRD2_2.OWNER_SURNAME,PRD2_2.PRD2_PAY_DATE FROM dbo.PRD2 AS PRD2_2 INNER JOIN" & _
            " (SELECT  OWNERSHIP_ID, SUM(PRD2_AMOUNT_REAL + PRD2_DISCOUNT + PRD2_TAXINCLUDE) AS sum_PRD2 FROM dbo.PRD2 AS PRD2_1" & _
            " WHERE (PRD2_SET_GK <> 1) And (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_STATUS = 1)" & strCon2 & " GROUP BY OWNERSHIP_ID) AS DDD ON DDD.OWNERSHIP_ID = PRD2_2.OWNERSHIP_ID" & _
            " WHERE (PRD2_2.PRD2_SET_GK <> 1) And (PRD2_2.PRD2_YEAR = " & txtYear.Text & ") And (PRD2_2.PRD2_STATUS = 1) " & strCon2 & strType & _
            " ) AS EEE ON A_1.OWNERSHIP_ID = EEE.OWNERSHIP_ID LEFT OUTER JOIN" & _
            " (SELECT PP1_2.OWNERSHIP_ID, FFF.sum_PP1, PP1_2.PP1_SET_GK, PP1_2.OWNER_NAME, PP1_2.OWNER_SURNAME,PP1_2.PP1_PAY_DATE" & _
            " FROM dbo.PP1 AS PP1_2 INNER JOIN (SELECT OWNERSHIP_ID, SUM(PP1_AMOUNT_REAL + PP1_DISCOUNT + PP1_TAXINCLUDE) AS sum_PP1" & _
            " FROM dbo.PP1 AS PP1_1 WHERE (PP1_SET_GK <> 1) And (PP1_YEAR = " & txtYear.Text & ") And (PP1_STATUS = 1)" & strCon3 & " GROUP BY OWNERSHIP_ID) AS FFF ON FFF.OWNERSHIP_ID = PP1_2.OWNERSHIP_ID" & _
            " WHERE (PP1_2.PP1_SET_GK <> 1) And (PP1_2.PP1_YEAR = " & txtYear.Text & ") And (PP1_2.PP1_STATUS = 1) " & strCon3 & strType & _
            " ) AS GGG ON A_1.OWNERSHIP_ID = GGG.OWNERSHIP_ID WHERE 1=1 "
            strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "A_1.OWNER_NAME")
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View15 = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View15 = False
                MsgBox Err.Description
        Exit Function
End Function

Private Function Create_View17() As Boolean '��ª��ͺѭ��������� ੾�з���ͧ�����Թ
        Dim strType As String
        Dim strCon1 As String
        Dim strCon2 As String
        Dim strCon3 As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        
'        strSQL = ""
        Me.MousePointer = 11
        strType = ""
        If Cmb_Type.ListIndex > 0 Then
                strType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        
        strSQL = "ALTER VIEW [dbo].[VIEW_GK_NEW] AS" & _
            " SELECT AAA.OWNERSHIP_ID, AAA.PRENAME + AAA.OWNER_NAME + '   ' + AAA.OWNER_SURNAME AS FULLNAME, CASE WHEN SUMTAX_BUILDING IS NULL" & _
            " THEN 0 ELSE SUMTAX_BUILDING END AS TAX_BUILDING, CASE WHEN SUMTAX_LAND IS NULL THEN 0 ELSE SUMTAX_LAND END AS TAX_LAND," & _
            " CASE WHEN SUMTAX_SIGNBOARD IS NULL THEN 0 ELSE SUMTAX_SIGNBOARD END AS TAX_SIGNBOARD, AAA.OWNER_TYPE, AAA.OWNER_NAME,AAA.OWNER_SURNAME" & _
            " FROM (SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE FROM dbo.PRD2 WHERE (PRD2_YEAR = " & txtYear.Text & ") AND (PRD2_SET_GK = 0) AND (PRD2_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE Union" & _
            " SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE FROM dbo.PBT5 WHERE (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 0) And (PBT5_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE Union" & _
            " SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE FROM dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") And (PP1_SET_GK = 0) And (PP1_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE) AS AAA LEFT OUTER JOIN" & _
            " (SELECT OWNERSHIP_ID, SUM(PRD2_RENTYEAR_TOTAL) AS SUMTAX_BUILDING FROM dbo.PRD2 AS PRD2_1 WHERE (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 0) And (PRD2_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID) AS BBB ON BBB.OWNERSHIP_ID = AAA.OWNERSHIP_ID LEFT OUTER JOIN" & _
            " (SELECT OWNERSHIP_ID, SUM(PBT5_AMOUNT) AS SUMTAX_LAND FROM dbo.PBT5 AS PBT5_1 WHERE (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 0) And (PBT5_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID) AS CCC ON CCC.OWNERSHIP_ID = AAA.OWNERSHIP_ID LEFT OUTER JOIN" & _
            " (SELECT OWNERSHIP_ID, SUM(PP1_AMOUNT) AS SUMTAX_SIGNBOARD FROM dbo.PP1 AS PP1_1 Where (PP1_YEAR = " & txtYear.Text & ") And (PP1_SET_GK = 0) And (PP1_STATUS = 0)" & _
            " GROUP BY OWNERSHIP_ID) AS DDD ON DDD.OWNERSHIP_ID = AAA.OWNERSHIP_ID WHERE (1 = 1)"
            strSQL = strSQL & strType
            strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
'            strSQL = strSQL & " ORDER BY OWNER_NAME,OWNER_SURNAME"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        
        Me.MousePointer = 0
        Create_View17 = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View17 = False
                MsgBox Err.Description
        Exit Function
End Function

Private Function Create_View16() As Boolean '��ª��ͺѭ���������
        Dim strType As String
        Dim strCon1 As String
        Dim strCon2 As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        
        strSQL = ""
        Me.MousePointer = 11
        If Cmb_Type.ListIndex > 0 Then
                strType = " AND (OWNER_TYPE=" & Cmb_Type.ListIndex & ")"
        End If
        strCon2 = QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        Select Case cbType.ListIndex
                Case 0
                        strCon1 = " AND (PBT5_PAY_DATE >= '" & CvDate1(DTPicker1) & "') AND (PBT5_PAY_DATE <= '" & CvDate1(DTPicker2) & "')"
                        strSQL = "ALTER VIEW [dbo].[VIEW_PBT5_BY_DAY] AS" & _
                                    " SELECT PBT5_YEAR, SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS TAX_PBT5, PBT5_BOOK_NUMBER," & _
                                    "PBT5_BOOK_NO , PBT5_PAY_DATE, OWNERSHIP_ID" & _
                                    " FROM dbo.PBT5 AS PBT5" & _
                                    " WHERE (PBT5_STATUS = 1) AND PBT5_YEAR=" & CInt(txtYear.Text) & " AND PBT5_SET_GK=" & cbPerform.ListIndex & strCon1 & strType & strCon2 & _
                                    " GROUP BY PBT5_YEAR, PBT5_BOOK_NUMBER, PBT5_BOOK_NO, PBT5_PAY_DATE, OWNERSHIP_ID"
                Case 1
                        strCon1 = " AND (PRD2_PAY_DATE >= '" & CvDate1(DTPicker1) & "') AND (PRD2_PAY_DATE <= '" & CvDate1(DTPicker2) & "')"
                        strSQL = "ALTER VIEW [dbo].[VIEW_PRD2_BY_DAY] AS" & _
                                    " SELECT PRD2_YEAR, SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS TAX_PRD2, PRD2_BOOK_NUMBER," & _
                                    "PRD2_BOOK_NO , PRD2_PAY_DATE, OWNERSHIP_ID" & _
                                    " FROM dbo.PRD2 AS PRD2" & _
                                    " WHERE (PRD2_STATUS = 1) AND PRD2_YEAR=" & CInt(txtYear.Text) & " AND PRD2_SET_GK=" & cbPerform.ListIndex & strCon1 & strType & strCon2 & _
                                    " GROUP BY PRD2_YEAR, PRD2_BOOK_NUMBER, PRD2_BOOK_NO, PRD2_PAY_DATE, OWNERSHIP_ID"
                Case 2
                        strCon1 = " AND (PP1_PAY_DATE >= '" & CvDate1(DTPicker1) & "') AND (PP1_PAY_DATE <= '" & CvDate1(DTPicker2) & "')"
                        strSQL = "ALTER VIEW [dbo].[VIEW_PP1_BY_DAY] AS" & _
                                    " SELECT PP1_YEAR, SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) AS TAX_PP1, PP1_BOOK_NUMBER," & _
                                    "PP1_BOOK_NO , PP1_PAY_DATE, OWNERSHIP_ID" & _
                                    " FROM dbo.PP1 AS PP1" & _
                                    " WHERE (PP1_STATUS = 1) AND PP1_YEAR=" & CInt(txtYear.Text) & " AND PP1_SET_GK=" & cbPerform.ListIndex & strCon1 & strType & strCon2 & _
                                    " GROUP BY PP1_YEAR, PP1_BOOK_NUMBER, PP1_BOOK_NO, PP1_PAY_DATE, OWNERSHIP_ID"
        End Select
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View16 = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View16 = False
                MsgBox Err.Description
        Exit Function
End Function

Private Function Create_View12() As Boolean '�.�.2
        On Error GoTo ERR_1
        
        Globle_Connective.BeginTrans
        Frm_PrintSummary.MousePointer = 11
        
'        strSQL = "DROP VIEW OWNERSHIP_GK2": Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW OWNERSHIP_GK2 AS" & _
'                        " SELECT * FROM (SELECT OWNERSHIP_ID,PRENAME,OWNER_NAME,OWNER_SURNAME,OWNER_TYPE FROM PBT5" & _
'                        " WHERE PBT5_YEAR = " & txtYear.Text & " AND PBT5_SET_GK = 1" & _
'                        " UNION SELECT OWNERSHIP_ID,PRENAME,OWNER_NAME,OWNER_SURNAME,OWNER_TYPE FROM PRD2 WHERE PRD2_YEAR = " & txtYear.Text & _
'                        " AND PRD2_SET_GK = 1 UNION SELECT OWNERSHIP_ID,PRENAME,OWNER_NAME,OWNER_SURNAME,OWNER_TYPE FROM PP1 " & _
'                        " WHERE PP1_YEAR= " & txtYear.Text & " AND PP1_SET_GK=1)"
'        Call SET_Execute(strSQL)
'        '******* PBT5
'        strSQL = "DROP VIEW VIEW_PBT5_CHANGE": Call SET_Execute(strSQL)
'        strSQL = "DROP VIEW VIEW_PBT5_GK2_DELASSET": Call SET_Execute(strSQL)
'        strSQL = "DELETE * FROM PBT5_OWNER_CHANGE": Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_PBT5_CHANGE AS" & _
'                        " SELECT B.OWNERSHIP_ID FROM [SELECT COUNT(OWNERSHIP_ID) AS A_COUNT,OWNERSHIP_ID FROM PBT5 WHERE PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS AAA, " & _
'                        "[SELECT COUNT(OWNERSHIP_ID) AS B_COUNT,OWNERSHIP_ID FROM PBT5 WHERE FLAG_CHANGE=1 AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS B" & _
'                        " Where AAA.OWNERSHIP_ID = B.OWNERSHIP_ID And A_COUNT = B_COUNT"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_PBT5_GK2_DELASSET AS SELECT OWNERSHIP_ID, PBT5_GK_MONEY, PBT5_YEAR From PBT5 WHERE EXISTS (SELECT * FROM LANDDATA WHERE LANDDATA.LAND_ID = PBT5.LAND_ID) AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1" & _
'                        " GROUP BY OWNERSHIP_ID, PBT5_GK_MONEY, PBT5_YEAR"
'        Call SET_Execute(strSQL)
'        strSQL = "INSERT INTO PBT5_OWNER_CHANGE ( OWNERSHIP_ID ) SELECT OWNERSHIP_ID FROM VIEW_PBT5_CHANGE"
'        Call SET_Execute(strSQL)
'        '******* PP1
'        strSQL = "DROP VIEW VIEW_PP1_CHANGE": Call SET_Execute(strSQL)
'        strSQL = "DROP VIEW VIEW_PP1_GK2_DELASSET": Call SET_Execute(strSQL)
'        strSQL = "DELETE * FROM PP1_OWNER_CHANGE": Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_PP1_CHANGE AS" & _
'                        " SELECT B.OWNERSHIP_ID FROM [SELECT COUNT(OWNERSHIP_ID) AS A_COUNT,OWNERSHIP_ID FROM PP1 WHERE PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS AAA, " & _
'                        "[SELECT COUNT(OWNERSHIP_ID) AS B_COUNT,OWNERSHIP_ID FROM PP1 WHERE FLAG_CHANGE=1 AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS B" & _
'                        " Where AAA.OWNERSHIP_ID = B.OWNERSHIP_ID And A_COUNT = B_COUNT"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_PP1_GK2_DELASSET AS SELECT OWNERSHIP_ID, PP1_GK_MONEY, PP1_YEAR From PP1" & _
'                        " WHERE EXISTS (SELECT * FROM SIGNBORDDATA WHERE SIGNBORDDATA.SIGNBORD_ID = PP1.SIGNBORD_ID) AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1" & _
'                        " GROUP BY OWNERSHIP_ID, PP1_GK_MONEY, PP1_YEAR"
'        Call SET_Execute(strSQL)
'        strSQL = "INSERT INTO PP1_OWNER_CHANGE ( OWNERSHIP_ID ) SELECT OWNERSHIP_ID FROM VIEW_PP1_CHANGE"
'        Call SET_Execute(strSQL)
'        On Error GoTo ERR_CREATE_VIEW
'        strSQL = "DROP VIEW VIEW_GK2"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_GK2 AS " & _
'                "SELECT A.OWNERSHIP_ID, OWNER_NAME & '  ' & OWNER_SURNAME AS FULLNAME, PBT5_GK_Money, PBT5_YEAR, PRD2_GK_Money, PRD2_YEAR, PP1_GK_Money, PP1_YEAR, OWNER_NAME" & _
'                " FROM ((OWNERSHIP_GK2 AS A" & _
'                " LEFT JOIN [SELECT A.OWNERSHIP_ID,PBT5_GK_Money,PBT5_YEAR FROM VIEW_PBT5_GK2_DELASSET AS A WHERE NOT EXISTS (SELECT * FROM PBT5_OWNER_CHANGE  B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID )" & _
'                " AND Not Exists (SELECT PBT5.OWNERSHIP_ID From PBT5 Where PBT5_SET_GK = 1 And PBT5_YEAR =" & txtYear.Text & " And PBT5_NO_STATUS = 1 And PBT5.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID)" & _
'                " AND PBT5_YEAR =" & txtYear.Text & " GROUP BY A.OWNERSHIP_ID,PBT5_GK_Money, PBT5_YEAR]. AS AAA ON A.OWNERSHIP_ID=AAA.OWNERSHIP_ID)" & _
'                " LEFT JOIN [SELECT A.OWNERSHIP_ID,PRD2_GK_Money,PRD2_YEAR FROM PRD2 A Where PRD2_SET_GK = 1 And PRD2_DATE Is Null And PRD2_YEAR =" & txtYear.Text & _
'                " AND Not Exists (SELECT PRD2.OWNERSHIP_ID From PRD2 Where PRD2_SET_GK = 1 And PRD2_YEAR =" & txtYear.Text & " And PRD2_NO_STATUS = 1 And PRD2.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID)" & _
'                " GROUP BY A.OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR]. AS BBB ON A.OWNERSHIP_ID=BBB.OWNERSHIP_ID)" & _
'                " LEFT JOIN [SELECT A.OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR FROM VIEW_PP1_GK2_DELASSET AS A WHERE NOT EXISTS (SELECT * FROM PP1_OWNER_CHANGE B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID )" & _
'                " AND Not Exists (SELECT PP1.OWNERSHIP_ID From PP1 Where PP1_SET_GK = 1 And PP1_YEAR =" & txtYear.Text & " And PP1_NO_STATUS = 1 And PP1.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID)  And PP1_YEAR =" & txtYear.Text & _
'                " GROUP BY A.OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR]. AS CCC ON A.OWNERSHIP_ID=CCC.OWNERSHIP_ID" & _
'                " Where Not (IsNull(PBT5_YEAR) And IsNull(PRD2_YEAR) And IsNull(PP1_YEAR))"
        strSQL = "ALTER VIEW [dbo].[VIEW_GK2] AS" & _
                " SELECT BBB.OWNERSHIP_ID, BBB.OWNER_NAME + '  ' + BBB.OWNER_SURNAME AS FULLNAME, CCC.PBT5_GK_Money, CCC.PBT5_YEAR,DDD.PRD2_GK_Money,DDD.PRD2_YEAR,EEE.PP1_GK_Money,PP1_YEAR" & _
                " FROM (SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE" & _
                " FROM (SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE" & _
                " FROM dbo.PBT5 WHERE (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 1) UNION SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE" & _
                " FROM dbo.PRD2 WHERE (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 1) UNION SELECT OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE" & _
                " FROM dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") AND (PP1_SET_GK = 1)) AS AAA) AS BBB LEFT OUTER JOIN (SELECT OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR" & _
                " FROM dbo.PBT5 AS A_1 WHERE (NOT EXISTS (SELECT OWNERSHIP_ID FROM dbo.PBT5 AS PBT5_1" & _
                " WHERE (PBT5_SET_GK = 1) And (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_STATUS = 1) And (OWNERSHIP_ID = A_1.OWNERSHIP_ID) GROUP BY OWNERSHIP_ID)) AND (PBT5_YEAR = " & txtYear.Text & ")" & _
                " AND (PBT5_SET_GK = 1) GROUP BY OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR) AS CCC ON CCC.OWNERSHIP_ID = BBB.OWNERSHIP_ID LEFT OUTER JOIN (SELECT     OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR" & _
                " FROM dbo.PRD2 AS A WHERE (PRD2_SET_GK = 1) AND (PRD2_DATE IS NULL) AND (PRD2_YEAR = " & txtYear.Text & ") AND (NOT EXISTS (SELECT OWNERSHIP_ID FROM dbo.PRD2 AS PRD2_1" & _
                " WHERE (PRD2_SET_GK = 1) And (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_STATUS = 1) And (OWNERSHIP_ID = A.OWNERSHIP_ID) GROUP BY OWNERSHIP_ID))" & _
                " GROUP BY OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR) AS DDD ON DDD.OWNERSHIP_ID = BBB.OWNERSHIP_ID LEFT OUTER JOIN (SELECT     OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR" & _
                " FROM dbo.PP1 AS A_2 WHERE (NOT EXISTS " & _
                " (SELECT OWNERSHIP_ID FROM dbo.PP1 AS PP1_1 WHERE (PP1_SET_GK = 1) And (PP1_YEAR = " & txtYear.Text & ") And (PP1_STATUS = 1) And (OWNERSHIP_ID = A_2.OWNERSHIP_ID)" & _
                " GROUP BY OWNERSHIP_ID)) AND (PP1_YEAR = " & txtYear.Text & ") AND (PP1_SET_GK = 1) GROUP BY OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR) AS EEE ON EEE.OWNERSHIP_ID = BBB.OWNERSHIP_ID WHERE (CCC.PBT5_YEAR IS NOT NULL OR" & _
                " DDD.PRD2_YEAR IS NOT NULL OR EEE.PP1_YEAR IS NOT NULL)"
        If Cmb_Type.ListIndex > 0 Then
                strSQL = strSQL & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
        End If
        
        strSQL = strSQL & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View12 = True
        Exit Function
ERR_1:
                Globle_Connective.RollbackTrans
                Frm_PrintSummary.MousePointer = 0
                Create_View12 = False
                MsgBox Err.Description
End Function

Private Function Create_View13() As Boolean '�ѭ���١˹��
        On Error GoTo ERR_1
        Dim strSQL2 As String
        
        Globle_Connective.BeginTrans
        Frm_PrintSummary.MousePointer = 11
        If Cmb_Type.ListIndex > 0 Then
                strSQL2 = strSQL2 & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
        End If
        strSQL2 = strSQL2 & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        Select Case cbType.ListIndex
                Case 0
                        '******* PBT5
'                        strSQL = "DROP VIEW VIEW_PBT5_CHANGE": Call SET_Execute(strSQL)
'                        strSQL = "DROP  VIEW VIEW_PBT5_GK2_DELASSET2": Call SET_Execute(strSQL)
'                        strSQL = "DROP VIEW VIEW_PBT5_GK2": Call SET_Execute(strSQL)
'                        strSQL = "DELETE * FROM PBT5_OWNER_CHANGE": Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PBT5_CHANGE AS" & _
'                                        " SELECT B.OWNERSHIP_ID FROM [SELECT COUNT(OWNERSHIP_ID) AS A_COUNT,OWNERSHIP_ID FROM PBT5 WHERE PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS AAA, " & _
'                                        "[SELECT COUNT(OWNERSHIP_ID) AS B_COUNT,OWNERSHIP_ID FROM PBT5 WHERE FLAG_CHANGE=1 AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS B" & _
'                                        " Where AAA.OWNERSHIP_ID = B.OWNERSHIP_ID And A_COUNT = B_COUNT"
'                        Call SET_Execute(strSQL)
'                        strSQL = "INSERT INTO PBT5_OWNER_CHANGE ( OWNERSHIP_ID ) SELECT OWNERSHIP_ID FROM VIEW_PBT5_CHANGE"
'                        Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PBT5_GK2_DELASSET2 AS SELECT PRENAME & OWNER_NAME & '  ' & OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, PBT5_GK_MONEY, PBT5_YEAR,OWNER_TYPE,OWNER_NAME" & _
'                                        " From PBT5 WHERE EXISTS (SELECT * FROM LANDDATA WHERE LANDDATA.LAND_ID = PBT5.LAND_ID) AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1" & _
'                                        " GROUP BY OWNERSHIP_ID, PBT5_GK_MONEY, PBT5_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME,OWNER_TYPE"
'                        Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PBT5_GK2 AS" & _
'                                    " SELECT A.OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, FULLNAME,OWNER_NAME FROM VIEW_PBT5_GK2_DELASSET2 AS A" & _
'                                    " WHERE NOT EXISTS (SELECT * FROM PBT5_OWNER_CHANGE B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID ) and Not Exists (SELECT PBT5.OWNERSHIP_ID From PBT5 Where PBT5_SET_GK = 1 And PBT5_YEAR =" & txtYear.Text & " And PBT5_NO_STATUS = 1 And PBT5.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID) And PBT5_YEAR =" & txtYear.Text
                        strSQL = "ALTER VIEW VIEW_PBT5_GK2 AS SELECT OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, FULLNAME, OWNER_NAME" & _
                            " FROM (SELECT PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, OWNER_TYPE,OWNER_NAME FROM dbo.PBT5 WHERE Exists" & _
                            " (SELECT LAND_ID FROM dbo.LANDDATA WHERE (LAND_ID = dbo.PBT5.LAND_ID)  AND FLAG_PAYTAX=1) AND (PBT5_YEAR = " & txtYear.Text & ") AND (PBT5_SET_GK = 1) AND (FLAG_NO_USED = 'FALSE')" & _
                            " GROUP BY OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE) AS A" & _
                            " WHERE (PBT5_YEAR = " & txtYear.Text & ") " & strSQL2 & " AND (NOT EXISTS" & _
                            " (SELECT OWNERSHIP_ID FROM (SELECT B_1.OWNERSHIP_ID FROM (SELECT COUNT(OWNERSHIP_ID) AS A_COUNT, OWNERSHIP_ID FROM dbo.PBT5 AS PBT5_3" & _
                            " WHERE (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 1) GROUP BY OWNERSHIP_ID) AS AAA INNER JOIN" & _
                            " (SELECT COUNT(OWNERSHIP_ID) AS B_COUNT, OWNERSHIP_ID FROM dbo.PBT5 AS PBT5_2 Where (FLAG_CHANGE = 1) And (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_SET_GK = 1)" & _
                            " GROUP BY OWNERSHIP_ID) AS B_1 ON AAA.OWNERSHIP_ID = B_1.OWNERSHIP_ID AND AAA.A_COUNT = B_1.B_COUNT) AS B WHERE      (A.OWNERSHIP_ID = OWNERSHIP_ID))) AND (NOT EXISTS" & _
                            " (SELECT OWNERSHIP_ID FROM dbo.PBT5 AS PBT5_1 Where (PBT5_SET_GK = 1) And (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_STATUS = 1) And (OWNERSHIP_ID = A.OWNERSHIP_ID)" & _
                            " GROUP BY OWNERSHIP_ID)) GROUP BY OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, FULLNAME, OWNER_NAME"
                Case 1
                        '*******PRD2
'                        strSQL = "DROP VIEW VIEW_PRD2_GK2": Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PRD2_GK2 AS" & _
'                                    " SELECT A.OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR, PRENAME & OWNER_NAME & '  ' & OWNER_SURNAME AS FULLNAME,OWNER_NAME,OWNER_TYPE" & _
'                                    " FROM PRD2 AS A WHERE PRD2_SET_GK = 1 And PRD2_DATE Is Null And PRD2_YEAR =" & txtYear.Text & " AND Not Exists (SELECT PRD2.OWNERSHIP_ID From PRD2 Where PRD2_SET_GK = 1 And PRD2_YEAR =" & txtYear.Text & " And PRD2_NO_STATUS = 1 And PRD2.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID)"
                        strSQL = "ALTER VIEW VIEW_PRD2_GK2 AS SELECT OWNERSHIP_ID,PRD2_GK_Money,PRD2_YEAR,FULLNAME,OWNER_NAME" & _
                            " FROM (SELECT PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR, OWNER_TYPE,OWNER_NAME FROM dbo.PRD2 WHERE Exists" & _
                            " (SELECT BUILDING_ID FROM dbo.BUILDINGDATA WHERE (BUILDING_ID = dbo.PRD2.BUILDING_ID)) AND (PRD2_YEAR = " & txtYear.Text & ") AND (PRD2_SET_GK = 1) AND (FLAG_NO_USED = 'FALSE') GROUP BY OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE) AS A" & _
                            " WHERE (PRD2_YEAR = " & txtYear.Text & ")" & strSQL2 & " AND (NOT EXISTS (SELECT OWNERSHIP_ID FROM (SELECT B_1.OWNERSHIP_ID FROM (SELECT COUNT(OWNERSHIP_ID) AS A_COUNT, OWNERSHIP_ID FROM dbo.PRD2 AS PRD2_3" & _
                            " WHERE (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 1) GROUP BY OWNERSHIP_ID) AS AAA INNER JOIN (SELECT COUNT(OWNERSHIP_ID) AS B_COUNT, OWNERSHIP_ID FROM dbo.PRD2 AS PRD2_2 WHERE (FLAG_CHANGE = 1) And (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_SET_GK = 1)" & _
                            " GROUP BY OWNERSHIP_ID) AS B_1 ON AAA.OWNERSHIP_ID = B_1.OWNERSHIP_ID AND AAA.A_COUNT = B_1.B_COUNT) AS B WHERE      (A.OWNERSHIP_ID = OWNERSHIP_ID))) AND (NOT EXISTS (SELECT OWNERSHIP_ID FROM dbo.PRD2 AS PRD2_1" & _
                            " WHERE (PRD2_SET_GK = 1) And (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_STATUS = 1) And (OWNERSHIP_ID = A.OWNERSHIP_ID) GROUP BY OWNERSHIP_ID)) GROUP BY OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR, FULLNAME, OWNER_NAME"
                Case 2
                        '******* PP1
'                        strSQL = "DROP VIEW VIEW_PP1_CHANGE": Call SET_Execute(strSQL)
'                        strSQL = "DROP VIEW VIEW_PP1_GK2_DELASSET2": Call SET_Execute(strSQL)
'                        strSQL = "DROP VIEW VIEW_PP1_GK2": Call SET_Execute(strSQL)
'                        strSQL = "DELETE * FROM PP1_OWNER_CHANGE": Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PP1_CHANGE AS" & _
'                                        " SELECT B.OWNERSHIP_ID FROM [SELECT COUNT(OWNERSHIP_ID) AS A_COUNT,OWNERSHIP_ID FROM PP1 WHERE PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS AAA, " & _
'                                        "[SELECT COUNT(OWNERSHIP_ID) AS B_COUNT,OWNERSHIP_ID FROM PP1 WHERE FLAG_CHANGE=1 AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1 GROUP BY OWNERSHIP_ID]. AS B" & _
'                                        " Where AAA.OWNERSHIP_ID = B.OWNERSHIP_ID And A_COUNT = B_COUNT"
'                        Call SET_Execute(strSQL)
'                        strSQL = "INSERT INTO PP1_OWNER_CHANGE ( OWNERSHIP_ID ) SELECT OWNERSHIP_ID FROM VIEW_PP1_CHANGE"
'                        Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PP1_GK2_DELASSET2 AS SELECT PRENAME & OWNER_NAME & '  ' & OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, PP1_GK_MONEY, PP1_YEAR,OWNER_TYPE,OWNER_NAME" & _
'                                        " From PP1 WHERE EXISTS (SELECT * FROM SIGNBORDDATA WHERE SIGNBORDDATA.SIGNBORD_ID = PP1.SIGNBORD_ID) AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK=1" & _
'                                        " GROUP BY OWNERSHIP_ID, PP1_GK_MONEY, PP1_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME,OWNER_TYPE"
'                        Call SET_Execute(strSQL)
'                        strSQL = "CREATE VIEW VIEW_PP1_GK2 AS" & _
'                                        " SELECT A.OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR, FULLNAME,OWNER_NAME FROM VIEW_PP1_GK2_DELASSET2 AS A" & _
'                                        " WHERE NOT EXISTS (SELECT * FROM PP1_OWNER_CHANGE B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID ) and Not Exists (SELECT PP1.OWNERSHIP_ID From PP1 Where PP1_SET_GK = 1 And PP1_YEAR =" & txtYear.Text & " And PP1_NO_STATUS = 1 And PP1.OWNERSHIP_ID = A.OWNERSHIP_ID GROUP BY OWNERSHIP_ID) And PP1_YEAR =" & txtYear.Text
                        strSQL = "ALTER VIEW VIEW_PP1_GK2 AS SELECT OWNERSHIP_ID, PP1_GK_Money,PP1_YEAR, FULLNAME,OWNER_NAME FROM (SELECT PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR, OWNER_TYPE,OWNER_NAME" & _
                            " FROM dbo.PP1 WHERE Exists (SELECT SIGNBORD_ID,SIGNBORD_NAME,BUILDING_ID,BUILDING_NO,ZONE_BLOCK,LAND_ID,SIGNBORD_START_DATE,SIGNBORD_END_DATE, SIGNBORD_ROUND, SIGNBORD_SUMTAX, FLAG_PAYTAX, SIGNBOARD_CHANGE_ID, USER_LOGIN,DATE_ACCESS" & _
                            " FROM dbo.SIGNBORDDATA WHERE(SIGNBORD_ID = dbo.PP1.SIGNBORD_ID) AND FLAG_PAYTAX=1) AND (PP1_YEAR = " & txtYear.Text & ") AND (PP1_SET_GK = 1) AND (FLAG_NO_USED = 'FALSE') GROUP BY OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE) AS A" & _
                            " WHERE (PP1_YEAR = " & txtYear.Text & ")" & strSQL2 & " AND (NOT EXISTS (SELECT OWNERSHIP_ID FROM (SELECT B_1.OWNERSHIP_ID FROM (SELECT COUNT(OWNERSHIP_ID) AS A_COUNT, OWNERSHIP_ID FROM dbo.PP1 AS PP1_3 WHERE (PP1_YEAR = " & txtYear.Text & ") And (PP1_SET_GK = 1)" & _
                            " GROUP BY OWNERSHIP_ID) AS AAA INNER JOIN (SELECT COUNT(OWNERSHIP_ID) AS B_COUNT, OWNERSHIP_ID FROM dbo.PP1 AS PP1_2 WHERE (FLAG_CHANGE = 1) And (PP1_YEAR = 2549) And (PP1_SET_GK = 1) GROUP BY OWNERSHIP_ID) AS B_1 ON AAA.OWNERSHIP_ID = B_1.OWNERSHIP_ID AND AAA.A_COUNT = B_1.B_COUNT) AS B" & _
                            " WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID))) AND (NOT EXISTS (SELECT OWNERSHIP_ID FROM dbo.PP1 AS PP1_1 WHERE (PP1_SET_GK = 1) And (PP1_YEAR = " & txtYear.Text & ") And (PP1_STATUS = 1) And (OWNERSHIP_ID = A.OWNERSHIP_ID) GROUP BY OWNERSHIP_ID)) GROUP BY OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR, FULLNAME, OWNER_NAME"
        End Select
        
'        Select Case cbType.ListIndex
'                Case 0
'                        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR, FULLNAME,OWNER_NAME"
'                Case 1
'                        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR, PRENAME, OWNER_NAME, OWNER_SURNAME,OWNER_TYPE"
'                Case 2
'                        strSQL = strSQL & " GROUP BY OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR, FULLNAME,OWNER_NAME"
'        End Select
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View13 = True
        Exit Function
ERR_1:
                Globle_Connective.RollbackTrans
                Frm_PrintSummary.MousePointer = 0
'                MsgBox Err.Description
                Create_View13 = False
End Function

Private Sub LB_Menu_Change()
Select Case LB_Menu.Tag
             Case "2"
                         Call CheckIndexTree(2, "����¹��Ѿ���Թ������")
                                 TreeView1.Nodes.Item(2).Selected = True
             Case "3"
                         Call CheckIndexTree(3, "����¹��Ѿ���Թ����¹�ŧ")
                                 TreeView1.Nodes.Item(3).Selected = True
             Case "6"
                         Call CheckIndexTree(6, "�.�.1 �����ࡳ���ҧ�Ѻ (��ºؤ��)")
                                 TreeView1.Nodes.Item(6).Selected = True
             Case "7"
                        Call CheckIndexTree(7, "�.�.2 �����ࡳ���ҧ�Ѻ (��ºؤ��)")
                        TreeView1.Nodes.Item(7).Selected = True
            Case "8"
                        Call CheckIndexTree(8, "��ª��ͺѭ���������")
                        TreeView1.Nodes.Item(8).Selected = True
             Case "9", "10"
                         Call CheckIndexTree(4, IIf(LB_Menu.Tag = "9", "�ѭ���١˹�� (��ºؤ��)", "��§ҹ�鹡�˹�����駻����Թ"))
                                 If LB_Menu.Tag = "9" Then
                                        TreeView1.Nodes.Item(9).Selected = True
                                 Else
                                        TreeView1.Nodes.Item(10).Selected = True
                                 End If
            Case "11"
                        Call CheckIndexTree(5, "��§ҹ��û�Ѻ�����š.�.1")  '�Դ����͹
                                 TreeView1.Nodes.Item(11).Selected = True
            Case "12"
                        Call CheckIndexTree(6, "�.�.1 �����ࡳ���ҧ�Ѻ (������ѡ��)")
                                 TreeView1.Nodes.Item(12).Selected = True
             Case "15"
                         Call CheckIndexTree(9, "���������Ш��ѹ")
                                 TreeView1.Nodes.Item(15).Selected = True
             Case "16"
                         Call CheckIndexTree(10, "��������㹻է�����ҳ")
                                 TreeView1.Nodes.Item(16).Selected = True
             Case "17"
                        Call CheckIndexTree(11, "���������㹻է�����ҳ")
                                 TreeView1.Nodes.Item(17).Selected = True
            Case "18"
                        Call CheckIndexTree(12, "�Ѵ�ѹ�Ѻ����ҧ��������")
                        TreeView1.Nodes.Item(18).Selected = True
            Case "19"
                        Call CheckIndexTree(13, "�Ѵ�ѹ�Ѻ����ҧ�����������������")
                                 TreeView1.Nodes.Item(19).Selected = True
            Case "20"
                        Call CheckIndexTree(14, "�Ѵ�ѹ�Ѻ����ҧ�������պ��ا��ͧ���")
                                 TreeView1.Nodes.Item(20).Selected = True
            Case "21"
                        Call CheckIndexTree(15, "�Ѵ�ѹ�Ѻ����ҧ���������ç���͹��з��Թ")
                                 TreeView1.Nodes.Item(21).Selected = True
            Case "22"
                        Call CheckIndexTree(16, "�Ѵ�ѹ�Ѻ����ҧ�������ջ���")
                                 TreeView1.Nodes.Item(22).Selected = True
             Case "14"
                        Call CheckIndexTree(33, "�š�û���ҳ����")
                                 TreeView1.Nodes.Item(14).Selected = True
             Case "31"
                        Call CheckIndexTree(27, "��ҿ��ػ�ʹ��������Ѻ��Ш���͹")
             Case Else
                        Call CheckIndexTree(0, "��������������§ҹ��ػ")
End Select
End Sub

Private Sub CheckIndexTree(Tindex As Byte, TStr As String)
Dim i As Byte
With cbType
        .Clear
        .AddItem "���պ��ا��ͧ���"
        .AddItem "�����ç���͹��з��Թ"
        .AddItem "���ջ���"
        .ListIndex = 0
End With
With cbPerform
        .Clear
        .AddItem "�����Ẻ"
        .AddItem "��������Ẻ"
        .ListIndex = 0
End With
For i = 0 To Img_Chk.Count - 1
      Img_Chk(i).Visible = False
Next i
ChkAllyr.Enabled = False: ChkAllyr.Value = False
chkGK.Enabled = False: chkGK.BackColor = &HE0E0E0: chkGK.Value = False
chkMonth.Enabled = False: chkMonth.BackColor = &HE0E0E0: chkMonth.Value = False
chkAccept.Enabled = False: chkAccept.Value = Unchecked: chkAccept.Visible = True: Label1(12).Visible = True
chkPerformAll.Enabled = False: chkPerformAll.Value = Unchecked: chkPerformAll.Visible = False: Label1(15).Visible = False: Img_Chk(10).Visible = False
cbMonth1.Enabled = False: cbMonth1.BackColor = &HE0E0E0: cbMonth1.ListIndex = -1
cbMonth2.Enabled = False: cbMonth2.BackColor = &HE0E0E0: cbMonth2.ListIndex = -1
Cmb_Type.Enabled = False: Cmb_Type.BackColor = &HE0E0E0: Cmb_Type.ListIndex = -1
Combo1(0).Enabled = False: Combo1(0).BackColor = &HE0E0E0: Combo1(0).ListIndex = -1
Combo1(1).Enabled = False: Combo1(1).BackColor = &HE0E0E0: Combo1(1).ListIndex = -1
DTPicker1.Enabled = False
DTPicker2.Enabled = False
Text1.Enabled = False: Text2.Enabled = False
cb_Part.BackColor = &HFFFFFF: cb_Part.Visible = False
chkListName.Visible = False
Label1(13).Visible = False
Label1(14).Visible = False
cbPerform.Enabled = False: cbPerform.BackColor = &HE0E0E0
cbType.Enabled = False: cbType.BackColor = &HE0E0E0
Text1.BackColor = &HE0E0E0: Text2.BackColor = &HE0E0E0
Label1(11).Caption = "����������¹��Ѿ���Թ :"
Label1(10).Caption = "������������Ẻ :"
Label1(9).Caption = "�кطء��"
'Label1(13).Caption = "�ʴ���������´����"
Label1(10).Caption = "������������Ẻ :"
Select Case Tindex
             Case 2, 3, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16, 17, 9, 4, 8, 33
                        chkAccept.Visible = False: Label1(12).Visible = False: Img_Chk(8).Visible = False
                        LB_Menu.Caption = TStr
                        If Tindex = 2 Then
                             Cmb_Type.Enabled = True
                             Cmb_Type.BackColor = &HFFFFFF
                             Img_Chk(3).Visible = True
                        End If
                       
                        If Tindex = 3 Then
                             chkMonth.Enabled = True
                             cbMonth1.Enabled = True: cbMonth1.BackColor = &HFFFFFF
                             cbMonth2.Enabled = True: cbMonth2.BackColor = &HFFFFFF
                             Img_Chk(2).Visible = True
                        End If
                        
'                        If Tindex = 10 Then
'                                Cmb_Type.Enabled = True
'                                Cmb_Type.BackColor = &HFFFFFF
'                                Img_Chk(3).Visible = True
'                        End If
             
                       If Tindex = 6 Or Tindex = 7 Or Tindex = 4 Or Tindex = 8 Or Tindex = 5 Then
                            Cmb_Type.Enabled = True
                            Cmb_Type.BackColor = &HFFFFFF
                            Img_Chk(3).Visible = True
                            Img_Chk(4).Visible = True
                            Combo1(0).Enabled = True: Combo1(0).BackColor = &HFFFFFF
                            Combo1(1).Enabled = True: Combo1(1).BackColor = &HFFFFFF
                            Img_Chk(1).Visible = True
                            chkGK.Enabled = True
                            If Tindex = 7 Then '�.�.2 �����ࡳ���ҧ�Ѻ
                                    cbPerform.Enabled = True
                                    cbPerform.BackColor = &HFFFFFF
                                    cbType.Enabled = True
                                    cbType.BackColor = &HFFFFFF
                                    Img_Chk(6).Visible = True
                                    Img_Chk(7).Visible = True
                                    Img_Chk(8).Visible = True
                                    Img_Chk(1).Visible = False
                                    chkGK.Enabled = False
                                    chkAccept.Visible = True: Label1(12).Visible = True: Img_Chk(8).Visible = True
                                    chkAccept.Enabled = True
                                    DTPicker1.Enabled = True
                                    DTPicker2.Enabled = True
                                    Text1.Enabled = True: Text2.Enabled = True
                                    Text1.BackColor = &HFFFFFF: Text2.BackColor = &HFFFFFF
                                    Img_Chk(5).Visible = True
'                                    chkListName.Visible = True
'                                    Label1(13).Visible = True: Label1(13).Caption = "�����������������": Img_Chk(9).Visible = True
                            End If
                            If Tindex = 4 Then
                                    cbType.Enabled = True: cbType.BackColor = &HFFFFFF
                                    chkGK.Enabled = False: chkGK.BackColor = &HE0E0E0: chkGK.Value = False
                                    Img_Chk(1).Visible = False
                                    Img_Chk(7).Visible = True
                            End If
                            If Tindex = 8 Then
                                    chkGK.Enabled = False: chkGK.BackColor = &HFFFFFF: Cmb_Type.BackColor = &HFFFFFF: chkGK.Value = Unchecked
                                    ChkAllyr.Enabled = True: ChkAllyr.Value = Checked
                                    Img_Chk(1).Visible = False
                                    Label1(11).Caption = "�Ǵ :"
                                    Img_Chk(7).Visible = True
                                    Label1(9).Caption = "Ẻ�Ǵ"
                                    Img_Chk(0).Visible = True
                                    With cbType
                                            .Clear
                                            .AddItem "1 �.� - 31 �.�"
                                            .AddItem "1 �.� - 31 �.�"
                                            .AddItem "1 ��.� - 30 �.�"
                                            .ListIndex = 0
                                            .BackColor = &HFFFFFF
                                            .Enabled = True
                                    End With
                            End If
                       End If
                       
                       If Tindex = 9 Then
                            DTPicker1.Enabled = True
                            DTPicker2.Enabled = True
                            Text1.Enabled = True: Text2.Enabled = True
                            Text1.BackColor = &HFFFFFF: Text2.BackColor = &HFFFFFF
                            Img_Chk(5).Visible = True
                            Label1(9).Caption = "��ػ�ʹ���"
                            ChkAllyr.Enabled = True: Img_Chk(0).Visible = True
                            cbType.Enabled = True: cbType.BackColor = &HFFFFFF
                            With cbPerform
                                    .Clear
                                    .AddItem "�������"
                                    .AddItem "�.�.1"
                                    .AddItem "�������"
                                    .ListIndex = 0
                            End With
                            Label1(10).Caption = "��������ê�������:": cbPerform.Enabled = True: Img_Chk(6).Visible = True: cbPerform.BackColor = &HFFFFFF
                       End If
                       If Tindex = 33 Then
                            chkAccept.Visible = False: Label1(12).Visible = False: Img_Chk(8).Visible = False
                       End If
                       If Tindex = 11 Or Tindex = 10 Then
                            ChkAllyr.Enabled = True
                            Img_Chk(0).Visible = True
                       End If
                       If Tindex = 15 Or Tindex = 21 Or Tindex = 18 Or Tindex = 16 Or Tindex = 17 Or Tindex = 14 Or Tindex = 13 Or Tindex = 9 Then
                            Cmb_Type.Enabled = True: Cmb_Type.BackColor = &HFFFFFF
                            Combo1(0).ListIndex = 0
                            Combo1(1).ListIndex = 0
                            Combo1(0).Enabled = True: Combo1(0).BackColor = &HFFFFFF
                            Combo1(1).Enabled = True: Combo1(1).BackColor = &HFFFFFF
                            Img_Chk(3).Visible = True
                            Img_Chk(4).Visible = True
                       End If
                       If Tindex = 5 Then
                                chkGK.Enabled = False: Img_Chk(1).Visible = False
                                cbType.Enabled = True: cbType.BackColor = &HFFFFFF: Img_Chk(7).Visible = True
                                chkListName.Visible = False: Label1(13).Visible = False: Img_Chk(9).Visible = False
                                Img_Chk(6).Visible = True
                                Label1(10).Caption = "��������û�Ѻ������ :"
                                With cbPerform
                                        .Clear
                                        .AddItem "����͹��Ѿ���Թ"
                                        .AddItem "��è�˹��·�Ѿ���Թ"
                                        .AddItem "����������Թ����"
                                        .AddItem "�������¹�ŧ����"
                                        .BackColor = &HFFFFFF
                                        .Enabled = True
                                        .ListIndex = 0
                                End With
                                cbPerform.Enabled = True
                                cb_Part.Visible = True: cb_Part.Top = 6660
                                cb_Part.ListIndex = 0
                                Label1(14).Visible = True: Label1(14).Top = 6750
                       End If
            Case 27
                           cbMonth1.Enabled = True: cbMonth1.BackColor = &HFFFFFF: cbMonth1.Locked = False
                           cbMonth2.Enabled = False: cbMonth2.BackColor = &HE0E0E0
                           chkMonth.Enabled = False: chkMonth.Value = Unchecked
                           Img_Chk(2).Visible = True
            Case 12
                            For i = 0 To Img_Chk.Count - 1
                                    Img_Chk(i).Visible = False
                            Next i
                            ChkAllyr.Enabled = False: ChkAllyr.Value = False
                            chkGK.Enabled = False: chkGK.BackColor = &HE0E0E0: chkGK.Value = False
                            chkMonth.Enabled = False: chkMonth.BackColor = &HE0E0E0: chkMonth.Value = False
                            cbMonth1.Enabled = False: cbMonth1.BackColor = &HE0E0E0: cbMonth1.ListIndex = -1
                            cbMonth2.Enabled = False: cbMonth2.BackColor = &HE0E0E0: cbMonth2.ListIndex = -1
                            Cmb_Type.Enabled = False: Cmb_Type.BackColor = &HE0E0E0: Cmb_Type.ListIndex = -1
                            Combo1(0).Enabled = False: Combo1(0).BackColor = &HE0E0E0: Combo1(0).ListIndex = -1
                            Combo1(1).Enabled = False: Combo1(1).BackColor = &HE0E0E0: Combo1(1).ListIndex = -1
                            DTPicker1.Enabled = False
                            DTPicker2.Enabled = False
                            Text1.Enabled = False: Text2.Enabled = False
                            Text1.BackColor = &HE0E0E0: Text2.BackColor = &HE0E0E0
End Select
End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)
       LB_Menu.Tag = Node.Index
       LB_Menu.Caption = Node.Text
End Sub

Private Sub txtYear_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Function Create_View3() As Boolean
        Dim strOwnerType As String, strOwnerType2 As String
        
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        Globle_Connective.BeginTrans
        
        If Cmb_Type.ListIndex > 0 Then
                strOwnerType = "AND OWNER_TYPE=" & Cmb_Type.ListIndex
                strOwnerType2 = "WHERE BB." & Right$(strOwnerType, Len(strOwnerType) - 4)
        End If
        strSQL = "ALTER VIEW [dbo].[VIEW_SUMMATION_ALL] AS" & _
            " SELECT COUNT_ALL,(SELECT COUNT(OWNERSHIP_ID) AS Expr1 FROM (SELECT DISTINCT A.OWNERSHIP_ID" & _
            " FROM dbo.LANDDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_1) AS COUNT_OWNER_LAND," & _
            " (SELECT COUNT(LAND_ID) AS Expr1 FROM (SELECT DISTINCT A.LAND_ID FROM dbo.LANDDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_2) AS COUNT_LAND," & _
            " (SELECT COUNT(OWNERSHIP_ID) AS Expr1 FROM (SELECT DISTINCT A.OWNERSHIP_ID FROM dbo.BUILDINGDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_3) AS COUNT_OWNER_BUILDING," & _
            " (SELECT COUNT(BUILDING_ID) AS Expr1 FROM (SELECT DISTINCT A.BUILDING_ID FROM dbo.BUILDINGDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_4) AS COUNT_BUILDING," & _
            " (SELECT COUNT(OWNERSHIP_ID) AS Expr1 FROM (SELECT DISTINCT A.OWNERSHIP_ID FROM dbo.SIGNBORDDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_5) AS COUNT_OWNER_SIGNBORDDATA," & _
            " (SELECT COUNT(SIGNBORD_ID) AS Expr1 FROM (SELECT DISTINCT A.SIGNBORD_ID FROM dbo.SIGNBORDDATA_NOTIC AS A INNER JOIN dbo.OWNERSHIP AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID " & strOwnerType & ") AS AA_6) AS COUNT_SIGNBORDDATA" & _
            " FROM (SELECT COUNT(*) AS COUNT_ALL FROM (SELECT OWNERSHIP_ID, OWNER_TYPE FROM (SELECT OWNERSHIP_ID, OWNER_TYPE FROM dbo.OWNERSHIP AS A Where Exists (SELECT OWNERSHIP_ID FROM dbo.LANDDATA_NOTIC AS B" & _
            " WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID)) OR Exists (SELECT OWNERSHIP_ID FROM dbo.BUILDINGDATA_NOTIC AS B WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID )) OR" & _
            " Exists (SELECT OWNERSHIP_ID FROM dbo.SIGNBORDDATA_NOTIC AS B WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID))) AS AA) AS BB " & strOwnerType2 & ") AS TST"
'        strSQL = "CREATE VIEW VIEW_SUMMATION_ALL AS " & _
'                    "SELECT COUNT_ALL, * " & _
'                    " FROM (SELECT COUNT(*) AS COUNT_ALL" & _
'                    " FROM (SELECT * FROM (SELECT OWNERSHIP_ID,OWNER_TYPE  FROM OWNERSHIP A WHERE  Exists (SELECT OWNERSHIP_ID  FROM LANDDATA_NOTIC B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID) OR" & _
'                    " Exists (SELECT OWNERSHIP_ID  FROM BUILDINGDATA_NOTIC B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID) OR  Exists (SELECT OWNERSHIP_ID  FROM SIGNBORDDATA_NOTIC B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID)) AS AA) AS BB " & _
'                    strOwnerType2 & ")," & _
'                    " (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_LAND " & _
'                    " FROM (SELECT DISTINCT A.OWNERSHIP_ID FROM LANDDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & _
'                    strOwnerType & " )) AS AA," & _
'                    " (SELECT COUNT(LAND_ID) AS COUNT_LAND FROM (SELECT DISTINCT LAND_ID " & _
'                    " FROM LANDDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & strOwnerType & " )) AS BB, " & _
'                    " (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_BUILDING" & _
'                    " FROM (SELECT DISTINCT A.OWNERSHIP_ID FROM BUILDINGDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & _
'                    strOwnerType & " )) AS CC ," & _
'                    " (SELECT COUNT(BUILDING_ID) AS COUNT_BUILDING FROM (SELECT DISTINCT BUILDING_ID " & _
'                    " FROM BUILDINGDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & strOwnerType & " )) AS DD," & _
'                    " (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_SIGNBORDDATA " & _
'                    " FROM (SELECT DISTINCT A.OWNERSHIP_ID FROM SIGNBORDDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & _
'                    strOwnerType & " )) AS EE," & _
'                    " (SELECT COUNT(SIGNBORD_ID) AS COUNT_SIGNBORDDATA FROM (SELECT DISTINCT SIGNBORD_ID " & _
'                    " FROM SIGNBORDDATA_NOTIC A,OWNERSHIP B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID " & strOwnerType & " )) AS FF"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View3 = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View3 = False
                MsgBox Err.Description
        Exit Function
End Function

Private Function Create_View2() As Boolean
        Dim strSQL2 As String
        Dim strSQL3 As String
        Dim strSQL4 As String
        
        On Error GoTo ERR_CREATE_VIEW
        
        Me.MousePointer = 11
        Globle_Connective.BeginTrans
        If chkMonth.Value = Checked Then
                If (cbMonth1.ListIndex <> 0 And cbMonth2.ListIndex <> 0) Then
                        strSQL2 = " AND MONTH(LAND_UPDATE) BETWEEN " & cbMonth1.ListIndex + 1 & " AND " & cbMonth2.ListIndex + 1
                        strSQL3 = " AND MONTH(BUILDING_UPDATE) BETWEEN " & cbMonth1.ListIndex + 1 & " AND " & cbMonth2.ListIndex + 1
                        strSQL4 = " AND MONTH(STORY_UPDATE) BETWEEN " & cbMonth1.ListIndex + 1 & " AND " & cbMonth2.ListIndex + 1
                End If
                If (cbMonth1.ListIndex <> 0 And cbMonth2.ListIndex = 0) Then
                        strSQL2 = " AND MONTH(LAND_UPDATE) =" & cbMonth1.ListIndex + 1
                        strSQL3 = " AND MONTH(BUILDING_UPDATE) = " & cbMonth1.ListIndex + 1
                        strSQL4 = " AND MONTH(STORY_UPDATE) = " & cbMonth1.ListIndex + 1
                End If
                If (cbMonth1.ListIndex = 0 And cbMonth2.ListIndex <> 0) Then
                        strSQL2 = " AND MONTH(LAND_UPDATE) = " & cbMonth2.ListIndex + 1
                        strSQL3 = " AND MONTH(BUILDING_UPDATE) = " & cbMonth2.ListIndex + 1
                        strSQL4 = " AND MONTH(STORY_UPDATE) = " & cbMonth2.ListIndex + 1
                End If
        End If
'        strSQL = "DROP VIEW VIEW_SUMMATION"
'        Call SET_Execute(strSQL)
'        strSQL = "CREATE VIEW VIEW_SUMMATION AS " & _
'                        "SELECT *,(COUNT_OWNER_LAND+COUNT_OWNER_BUILDING+COUNT_OWNER_SIGNBOARD) AS ALL_COUNT " & _
'                        " FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_LAND" & _
'                        " FROM (SELECT DISTINCT OWNERSHIP_ID FROM LANDDATA_NOTIC_STORY INNER JOIN LANDDATASTORY ON " & _
'                        " LANDDATA_NOTIC_STORY.ROUND_COUNT = LANDDATASTORY.ROUND_COUNT WHERE YEAR(LAND_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL2 & " )) AS AA, " & _
'                        " (SELECT COUNT(LAND_ID) AS COUNT_LAND FROM (SELECT DISTINCT LAND_ID FROM LANDDATA_NOTIC_STORY " & _
'                        " INNER JOIN LANDDATASTORY ON LANDDATA_NOTIC_STORY.ROUND_COUNT = LANDDATASTORY.ROUND_COUNT " & _
'                        " WHERE YEAR(LAND_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL2 & " )) AS BB," & _
'                        " (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_BUILDING FROM (SELECT DISTINCT OWNERSHIP_ID " & _
'                        " FROM BUILDINGDATASTORY INNER JOIN BUILDINGDATA_NOTIC_STORY ON BUILDINGDATASTORY.ROUND_COUNT = BUILDINGDATA_NOTIC_STORY.ROUND_COUNT " & _
'                        " WHERE YEAR(BUILDING_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL3 & " )) AS CC," & _
'                        " (SELECT COUNT(BUILDING_ID) AS COUNT_BUIUDING FROM (SELECT DISTINCT BUILDING_ID " & _
'                        " FROM BUILDINGDATASTORY INNER JOIN BUILDINGDATA_NOTIC_STORY ON BUILDINGDATASTORY.ROUND_COUNT = BUILDINGDATA_NOTIC_STORY.ROUND_COUNT " & _
'                        " WHERE YEAR(BUILDING_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL3 & " )) AS DD," & _
'                        " (SELECT  COUNT(OWNERSHIP_ID) AS COUNT_OWNER_SIGNBOARD FROM (SELECT DISTINCT OWNERSHIP_ID " & _
'                        " FROM SIGNBORDDATASTORY INNER JOIN SIGNBORDDATA_NOTIC_STORY ON SIGNBORDDATASTORY.ROUND_COUNT = SIGNBORDDATA_NOTIC_STORY.ROUND_COUNT " & _
'                        " WHERE YEAR(STORY_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL4 & " )) AS EE," & _
'                        " (SELECT COUNT(SIGNBORD_ID) AS COUNT_SIGNBOARD FROM (SELECT DISTINCT SIGNBORD_ID " & _
'                        " FROM SIGNBORDDATASTORY INNER JOIN SIGNBORDDATA_NOTIC_STORY ON SIGNBORDDATASTORY.ROUND_COUNT = SIGNBORDDATA_NOTIC_STORY.ROUND_COUNT " & _
'                        " WHERE YEAR(STORY_UPDATE)=" & CInt(txtYear.Text) - 543 & strSQL4 & " )) AS FF"
        strSQL = "ALTER VIEW [dbo].[VIEW_SUMMATION] AS" & _
                " SELECT  AA.COUNT_OWNER_LAND + CC.COUNT_OWNER_BUILDING + EE.COUNT_OWNER_SIGNBOARD AS ALL_COUNT, AA.COUNT_OWNER_LAND," & _
                " BB.COUNT_LAND , CC.COUNT_OWNER_BUILDING, dd.COUNT_BUIUDING, EE.COUNT_OWNER_SIGNBOARD, FF.COUNT_SIGNBOARD" & _
                " FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_LAND FROM (SELECT DISTINCT dbo.LANDDATA_NOTIC_STORY.OWNERSHIP_ID" & _
                " FROM dbo.LANDDATA_NOTIC_STORY INNER JOIN dbo.LANDDATASTORY ON dbo.LANDDATA_NOTIC_STORY.ROUND_COUNT = dbo.LANDDATASTORY.ROUND_COUNT" & _
                " WHERE (YEAR(dbo.LANDDATASTORY.LAND_UPDATE) = " & CInt(txtYear.Text) - 543 & ") " & strSQL2 & ") AS AA_1) AS AA CROSS JOIN" & _
                " (SELECT COUNT(LAND_ID) AS COUNT_LAND FROM (SELECT DISTINCT LANDDATASTORY_1.LAND_ID FROM dbo.LANDDATA_NOTIC_STORY AS LANDDATA_NOTIC_STORY_1 INNER JOIN" & _
                " dbo.LANDDATASTORY AS LANDDATASTORY_1 ON LANDDATA_NOTIC_STORY_1.ROUND_COUNT = LANDDATASTORY_1.ROUND_COUNT WHERE (YEAR(LANDDATASTORY_1.LAND_UPDATE) = " & CInt(txtYear.Text) - 543 & ") " & strSQL2 & _
                " ) AS AA_2) AS BB CROSS JOIN (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_BUILDING" & _
                " FROM (SELECT DISTINCT dbo.BUILDINGDATA_NOTIC_STORY.OWNERSHIP_ID FROM dbo.BUILDINGDATASTORY INNER JOIN" & _
                " dbo.BUILDINGDATA_NOTIC_STORY ON dbo.BUILDINGDATASTORY.ROUND_COUNT = dbo.BUILDINGDATA_NOTIC_STORY.ROUND_COUNT" & _
                " WHERE (YEAR(dbo.BUILDINGDATASTORY.BUILDING_UPDATE) = " & CInt(txtYear.Text) - 543 & ") " & strSQL3 & _
                " ) AS AA_3) AS CC CROSS JOIN (SELECT COUNT(BUILDING_ID) AS COUNT_BUIUDING FROM (SELECT DISTINCT BUILDINGDATASTORY_1.BUILDING_ID" & _
                " FROM dbo.BUILDINGDATASTORY AS BUILDINGDATASTORY_1 INNER JOIN dbo.BUILDINGDATA_NOTIC_STORY AS BUILDINGDATA_NOTIC_STORY_1 ON" & _
                " BUILDINGDATASTORY_1.ROUND_COUNT = BUILDINGDATA_NOTIC_STORY_1.ROUND_COUNT WHERE (YEAR(BUILDINGDATASTORY_1.BUILDING_UPDATE) =" & CInt(txtYear.Text) - 543 & ") " & strSQL3 & _
                " ) AS AA_4) AS DD CROSS JOIN (SELECT COUNT(OWNERSHIP_ID) AS COUNT_OWNER_SIGNBOARD FROM (SELECT DISTINCT dbo.SIGNBORDDATA_NOTIC_STORY.OWNERSHIP_ID" & _
                " FROM dbo.SIGNBORDDATASTORY INNER JOIN dbo.SIGNBORDDATA_NOTIC_STORY ON dbo.SIGNBORDDATASTORY.ROUND_COUNT = dbo.SIGNBORDDATA_NOTIC_STORY.ROUND_COUNT" & _
                " WHERE (YEAR(dbo.SIGNBORDDATASTORY.STORY_UPDATE) = " & CInt(txtYear.Text) - 543 & ") " & strSQL4 & ") AS AA_5) AS EE CROSS JOIN" & _
                " (SELECT COUNT(SIGNBORD_ID) AS COUNT_SIGNBOARD FROM (SELECT DISTINCT SIGNBORDDATASTORY_1.SIGNBORD_ID FROM dbo.SIGNBORDDATASTORY AS SIGNBORDDATASTORY_1 INNER JOIN" & _
                " dbo.SIGNBORDDATA_NOTIC_STORY AS SIGNBORDDATA_NOTIC_STORY_1 ON SIGNBORDDATASTORY_1.ROUND_COUNT = SIGNBORDDATA_NOTIC_STORY_1.ROUND_COUNT" & _
                " WHERE (YEAR(SIGNBORDDATASTORY_1.STORY_UPDATE) = " & CInt(txtYear.Text) - 543 & ") " & strSQL4 & ") AS AA_6) AS FF"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View2 = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View2 = False
End Function

Private Function Create_View4() As Boolean
        Dim strBegin As String
        Dim strEnd As String
        Dim strTemp As String
        Dim i As Integer, j As Integer
        
        On Error GoTo ERR_CREATE_VIEW
        
        Globle_Connective.BeginTrans
        strSQL = ""
        If chkGK.Value = Unchecked Then
                '�ӹǹ��·�� PBT5,PP1,PRD2
                strBegin = "SELECT COUNT(OWNERSHIP_ID) "
'                For i = 1 To 43
'                    strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID FROM OWNERSHIP WHERE OWNER_TYPE = 1" & _
'                    " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%') AS A WHERE EXISTS (SELECT * FROM PBT5 B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK=1" & ") " & _
'                    " OR EXISTS (SELECT * FROM PP1 C WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PRD2 D WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_YEAR=" & txtYear.Text & "))"
'                Next i
                j = 0
                For i = 1 To 43
                    If i <> 43 Then
                            strTemp = " AND OWNER_NAME < " & Chr(34) & tmpArr(i + 1) & Chr(34)
                    Else
                            strTemp = ""
                    End If
                    strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PBT5 WHERE PBT5_YEAR = " & txtYear.Text & " AND PBT5_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PRD2 WHERE PRD2_YEAR = " & txtYear.Text & " AND PRD2_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PP1 WHERE PP1_YEAR= " & txtYear.Text & " AND PP1_SET_GK=1) AS ALL_OWNER" & _
                                    " WHERE OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND OWNER_TYPE=1)"
                Next i
                For i = 2 To 9
'                        strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID FROM OWNERSHIP WHERE  OWNER_TYPE=" & i & ") AS A " & _
'                        " WHERE EXISTS (SELECT * FROM PBT5 B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PP1 C WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_YEAR=" & txtYear.Text & ") " & _
'                        " OR EXISTS (SELECT * FROM PRD2 D WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_YEAR=" & txtYear.Text & ")) as TYPE" & i
                        strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PBT5 WHERE PBT5_YEAR = " & txtYear.Text & " AND PBT5_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PRD2 WHERE PRD2_YEAR = " & txtYear.Text & " AND PRD2_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PP1 WHERE PP1_YEAR= " & txtYear.Text & " AND PP1_SET_GK=1) AS ALL_OWNER" & _
                                    " WHERE OWNER_TYPE=" & i & ") AS TYPE" & i
                Next i
                strEnd = " FROM (SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PBT5 WHERE PBT5_YEAR = " & txtYear.Text & " AND PBT5_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PRD2 WHERE PRD2_YEAR = " & txtYear.Text & " AND PRD2_SET_GK = 1" & _
                                    " UNION SELECT OWNERSHIP_ID,OWNER_NAME,OWNER_TYPE FROM PP1 WHERE PP1_YEAR= " & txtYear.Text & " AND PP1_SET_GK=1) AS ALL_OWNER" & _
                                    " WHERE OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND OWNER_TYPE=1"
                strSQL = strBegin & strSQL & strEnd
                Set RsTotal = Globle_Connective.Execute("exec sp_gk_by_char '" & strSQL & "'", , adCmdUnknown)
'                Call SET_QUERY(strSQL, RsTotal)

                strSQL = ""
'                strTemp = vbNullString
                'PBT5
                
'                                ",(SELECT SUM(AAA.PBT5_GK_MONEY) FROM OWNERSHIP AS A,(SELECT OWNERSHIP_ID,PBT5_GK_MONEY FROM PBT5 WHERE PBT5_YEAR =" & txtYear.Text & " " & _
'                                " AND PBT5_SET_GK = 1 GROUP BY OWNERSHIP_ID,PBT5_GK_MONEY) AS AAA" & _
'                                " WHERE A.OWNER_TYPE=1 AND A.OWNERSHIP_ID=AAA.OWNERSHIP_ID AND A.OWNERSHIP_REAL_ID Like '�%')  AS ThiSum1"
                strBegin = "SELECT COUNT(Temp) AS Tha1 " & _
                                ",(SELECT SUM(PBT5_GK_MONEY) FROM (SELECT PBT5_GK_MONEY,OWNERSHIP_ID FROM PBT5 " & _
                                " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK = 1" & _
                                " GROUP BY PBT5_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum1"
                For i = 1 To 43
'                strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID)  FROM OWNERSHIP A,PBT5 B " & _
'                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBT5_STATUS = 0 AND PBT5_YEAR =" & txtYear & _
'                                " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%' AND A.OWNER_TYPE=1 GROUP BY  A.OWNERSHIP_ID)) as Tha" & i + 1 & _
'                                ",(SELECT SUM(AAA.PBT5_GK_MONEY) FROM OWNERSHIP AS A,(SELECT OWNERSHIP_ID,PBT5_GK_MONEY FROM PBT5 WHERE PBT5_YEAR =" & txtYear.Text & " " & _
'                                "  AND PBT5_SET_GK = 1 GROUP BY OWNERSHIP_ID,PBT5_GK_MONEY) AS AAA" & _
'                                " WHERE A.OWNER_TYPE=1 AND A.OWNERSHIP_ID=AAA.OWNERSHIP_ID AND A.OWNERSHIP_REAL_ID Like '" & tmpArr(i) & "%')  AS ThiSum" & i + 1
                
                        If i <> 43 Then
                                strTemp = " AND OWNER_NAME < " & Chr(34) & tmpArr(i + 1) & Chr(34)
                        Else
                                strTemp = ""
                        End If
                        strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID  FROM PBT5 Where PBT5_SET_GK=1" & _
                                " AND PBT5_YEAR = " & txtYear.Text & " AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A) AS Tha" & i + 1 & _
                                ",(SELECT SUM(PBT5_GK_MONEY) FROM (SELECT PBT5_GK_MONEY,OWNERSHIP_ID FROM PBT5 " & _
                                " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_SET_GK = 1" & _
                                " GROUP BY PBT5_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum" & i + 1
                Next i

                For i = 2 To 9
'                        strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID) FROM OWNERSHIP A,PBT5 B " & _
'                        " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_YEAR =" & txtYear & _
'                        " AND A.OWNER_TYPE=" & i & " GROUP BY A.OWNERSHIP_ID)) AS COUNT_TYPE" & i & _
'                        ",(SELECT SUM(AAA.PBT5_GK_MONEY) FROM OWNERSHIP AS A,(SELECT OWNERSHIP_ID,PBT5_GK_MONEY FROM PBT5 WHERE PBT5_YEAR =" & txtYear.Text & " " & _
'                        "  AND PBT5_SET_GK = 1 GROUP BY OWNERSHIP_ID,PBT5_GK_MONEY) AS AAA" & _
'                        " WHERE A.OWNER_TYPE=" & i & " AND A.OWNERSHIP_ID=AAA.OWNERSHIP_ID AND A.OWNER_TYPE =" & i & ")  AS SUM_TYPE" & i
                        strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID FROM PBT5 WHERE PBT5_SET_GK=1 AND PBT5_YEAR =" & txtYear & _
                        " AND OWNER_TYPE=" & i & " GROUP BY OWNERSHIP_ID) AS A) AS COUNT_TYPE" & i & _
                        ",(SELECT SUM(PBT5_GK_MONEY) FROM (SELECT OWNERSHIP_ID,PBT5_GK_MONEY FROM PBT5 " & _
                        " WHERE PBT5_YEAR =" & txtYear.Text & " AND PBT5_SET_GK=1 AND OWNER_TYPE=" & i & " GROUP BY PBT5_GK_MONEY,OWNERSHIP_ID) AS A) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(OWNERSHIP_ID) AS Temp FROM PBT5 Where PBT5_SET_GK = 1 " & _
                                " AND PBT5_YEAR = " & txtYear.Text & "AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A"
                strSQL = strBegin & strSQL & strEnd
                Set rs = Globle_Connective.Execute("exec sp_gk_by_char '" & strSQL & "'", , adCmdUnknown)
'                Call SET_QUERY(strSQL, Rs)

                strSQL = ""
                'PP1
                strBegin = "SELECT COUNT(Temp) AS Tha1 " & _
                                ",(SELECT SUM(PP1_GK_MONEY) FROM (SELECT PP1_GK_MONEY,OWNERSHIP_ID FROM PP1 " & _
                                " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK = 1" & _
                                " GROUP BY PP1_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum1"
                For i = 1 To 43
                            If i <> 43 Then
                                    strTemp = " AND OWNER_NAME < " & Chr(34) & tmpArr(i + 1) & Chr(34)
                            Else
                                    strTemp = ""
                            End If
                            strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID FROM PP1 Where PP1_SET_GK=1" & _
                                    " AND PP1_YEAR = " & txtYear.Text & " AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A) AS Tha" & i + 1 & _
                                    ",(SELECT SUM(PP1_GK_MONEY) FROM (SELECT PP1_GK_MONEY,OWNERSHIP_ID FROM PP1 " & _
                                    " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND PP1_YEAR=" & txtYear.Text & " AND PP1_SET_GK = 1" & _
                                    " GROUP BY PP1_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum" & i + 1
                Next i

                For i = 2 To 9
                            strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID FROM PP1 WHERE PP1_SET_GK=1 AND PP1_YEAR =" & txtYear & _
                                    " AND OWNER_TYPE=" & i & " GROUP BY OWNERSHIP_ID) AS A) AS COUNT_TYPE" & i & _
                                    ",(SELECT SUM(PP1_GK_MONEY) FROM (SELECT OWNERSHIP_ID,PP1_GK_MONEY FROM PP1 " & _
                                    " WHERE PP1_YEAR =" & txtYear.Text & " AND PP1_SET_GK=1 AND OWNER_TYPE=" & i & " GROUP BY PP1_GK_MONEY,OWNERSHIP_ID) AS A) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(OWNERSHIP_ID) AS Temp FROM PP1 Where PP1_SET_GK = 1 " & _
                                " AND PP1_YEAR = " & txtYear.Text & "AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A"
                strSQL = strBegin & strSQL & strEnd
                Set Rs2 = Globle_Connective.Execute("exec sp_gk_by_char '" & strSQL & "'", , adCmdUnknown)
'                Call SET_QUERY(strSQL, Rs2)

                strSQL = ""
                'PRD2
                strBegin = "SELECT COUNT(Temp) AS Tha1 " & _
                                ",(SELECT SUM(PRD2_GK_MONEY) FROM (SELECT PRD2_GK_MONEY,OWNERSHIP_ID FROM PRD2" & _
                                " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND PRD2_YEAR=" & txtYear.Text & " AND PRD2_SET_GK = 1" & _
                                " GROUP BY PRD2_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum1"
                For i = 1 To 43
                            If i <> 43 Then
                                    strTemp = " AND OWNER_NAME < " & Chr(34) & tmpArr(i + 1) & Chr(34)
                            Else
                                    strTemp = ""
                            End If
                            strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID FROM PRD2 Where PRD2_SET_GK=1" & _
                                    " AND PRD2_YEAR = " & txtYear.Text & " AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A) AS Tha" & i + 1 & _
                                    ",(SELECT SUM(PRD2_GK_MONEY) FROM (SELECT PRD2_GK_MONEY,OWNERSHIP_ID FROM PRD2 " & _
                                    " WHERE OWNER_TYPE=1  AND OWNER_NAME > " & Chr(34) & tmpArr(i) & Chr(34) & strTemp & " AND PRD2_YEAR=" & txtYear.Text & " AND PRD2_SET_GK = 1" & _
                                    " GROUP BY PRD2_GK_MONEY,OWNERSHIP_ID) AS A) AS ThiSum" & i + 1
                Next i

                For i = 2 To 9
                            strSQL = strSQL & ",(SELECT COUNT(COUNT_ID) FROM (SELECT COUNT(OWNERSHIP_ID) AS COUNT_ID FROM PRD2 WHERE PRD2_SET_GK=1 AND PRD2_YEAR =" & txtYear & _
                                    " AND OWNER_TYPE=" & i & " GROUP BY OWNERSHIP_ID) AS A) AS COUNT_TYPE" & i & _
                                    ",(SELECT SUM(PRD2_GK_MONEY) FROM (SELECT OWNERSHIP_ID,PRD2_GK_MONEY FROM PRD2 " & _
                                    " WHERE PRD2_YEAR =" & txtYear.Text & " AND PRD2_SET_GK=1 AND OWNER_TYPE=" & i & " GROUP BY PRD2_GK_MONEY,OWNERSHIP_ID) AS A) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(OWNERSHIP_ID) AS Temp FROM PRD2 Where PRD2_SET_GK = 1 " & _
                                " AND PRD2_YEAR = " & txtYear.Text & "AND OWNER_NAME > " & Chr(34) & "�" & Chr(34) & " AND OWNER_NAME < " & Chr(34) & "�" & Chr(34) & " AND OWNER_TYPE=1 GROUP BY OWNERSHIP_ID) AS A"
                strSQL = strBegin & strSQL & strEnd
                Set Rs3 = Globle_Connective.Execute("exec sp_gk_by_char '" & strSQL & "'", , adCmdUnknown)
'                Call SET_QUERY(strSQL, Rs3)

                For i = 0 To 43
'                        strSQL = "UPDATE Temp_GK SET TOTAL_COUNT=" & RsTotal.Fields(i).Value & " WHERE CHAR_ID='" & tmpArr(i) & "'"
                        Globle_Connective.Execute "exec sp_update_temp_gk_total_count " & RsTotal.Fields(i).Value & ",'" & tmpArr(i) & "'", , adCmdUnknown
'                        Call SET_Execute(strSQL)
                Next i

                For i = 2 To 9
                        Globle_Connective.Execute "exec sp_update_temp_gk_total_count " & RsTotal.Fields("TYPE" & i).Value & ",'" & i & "'", , adCmdUnknown
'                        strSQL = "UPDATE Temp_GK SET TOTAL_COUNT=" & RsTotal.Fields("TYPE" & i).Value & " WHERE CHAR_ID='" & i & "'"
'                        Call SET_Execute(strSQL)
                Next i

                For i = 0 To 43
'                Debug.Print "exec sp_update_temp_gk " & Rs.Fields("Tha" & i + 1).Value & "," & IIf(IsNull(Rs.Fields("ThiSum" & i + 1).Value), 0, Rs.Fields("ThiSum" & i + 1).Value) & "," & _
                                Rs2.Fields("Tha" & i + 1).Value & "," & IIf(IsNull(Rs2.Fields("ThiSum" & i + 1).Value), 0, Rs2.Fields("ThiSum" & i + 1).Value) & "," & Rs3.Fields("Tha" & i + 1).Value & "," & _
                                IIf(IsNull(Rs3.Fields("ThiSum" & i + 1).Value), 0, Rs3.Fields("ThiSum" & i + 1).Value) & ",'" & tmpArr(i) & "'"
                        Globle_Connective.Execute "exec sp_update_temp_gk " & rs.Fields("Tha" & i + 1).Value & "," & IIf(IsNull(rs.Fields("ThiSum" & i + 1).Value), 0, rs.Fields("ThiSum" & i + 1).Value) & "," & _
                                Rs2.Fields("Tha" & i + 1).Value & "," & IIf(IsNull(Rs2.Fields("ThiSum" & i + 1).Value), 0, Rs2.Fields("ThiSum" & i + 1).Value) & "," & Rs3.Fields("Tha" & i + 1).Value & "," & _
                                IIf(IsNull(Rs3.Fields("ThiSum" & i + 1).Value), 0, Rs3.Fields("ThiSum" & i + 1).Value) & ",'" & tmpArr(i) & "'", , adCmdUnknown
'                        strSQL = "UPDATE Temp_GK SET PBT5_COUNT=" & Rs.Fields("Tha" & i + 1).Value & _
'                                        ",PBT5_SUMTAX=" & IIf(IsNull(Rs.Fields("ThiSum" & i + 1).Value), 0, Rs.Fields("ThiSum" & i + 1).Value) & _
'                                        ",PP1_COUNT=" & Rs2.Fields("Tha" & i + 1).Value & _
'                                        ",PP1_SUMTAX=" & IIf(IsNull(Rs2.Fields("ThiSum" & i + 1).Value), 0, Rs2.Fields("ThiSum" & i + 1).Value) & _
'                                        ",PRD2_COUNT=" & Rs3.Fields("Tha" & i + 1).Value & _
'                                        ",PRD2_SUMTAX=" & IIf(IsNull(Rs3.Fields("ThiSum" & i + 1).Value), 0, Rs3.Fields("ThiSum" & i + 1).Value) & _
'                                        " WHERE CHAR_ID='" & tmpArr(i) & "'"
'                        Call SET_Execute(strSQL)
               Next i
               For i = 2 To 9
                        Globle_Connective.Execute "exec sp_update_temp_gk " & rs.Fields("COUNT_TYPE" & i).Value & "," & IIf(IsNull(rs.Fields("SUM_TYPE" & i).Value), 0, rs.Fields("SUM_TYPE" & i).Value) & "," & _
                                Rs2.Fields("COUNT_TYPE" & i).Value & "," & IIf(IsNull(Rs2.Fields("SUM_TYPE" & i).Value), 0, Rs2.Fields("SUM_TYPE" & i).Value) & "," & Rs3.Fields("COUNT_TYPE" & i).Value & "," & _
                                IIf(IsNull(Rs3.Fields("SUM_TYPE" & i).Value), 0, Rs3.Fields("SUM_TYPE" & i).Value) & ",'" & i & "'", , adCmdUnknown
'                        strSQL = "UPDATE Temp_GK SET PBT5_COUNT=" & Rs.Fields("COUNT_TYPE" & i).Value & _
'                                        ",PBT5_SUMTAX=" & IIf(IsNull(Rs.Fields("SUM_TYPE" & i).Value), 0#, Rs.Fields("SUM_TYPE" & i).Value) & _
'                                        ",PP1_COUNT=" & Rs2.Fields("COUNT_TYPE" & i).Value & _
'                                        ",PP1_SUMTAX=" & IIf(IsNull(Rs2.Fields("SUM_TYPE" & i).Value), 0#, Rs2.Fields("SUM_TYPE" & i).Value) & _
'                                        ",PRD2_COUNT=" & Rs3.Fields("COUNT_TYPE" & i).Value & _
'                                        ",PRD2_SUMTAX=" & IIf(IsNull(Rs3.Fields("SUM_TYPE" & i).Value), 0#, Rs3.Fields("SUM_TYPE" & i).Value) & _
'                                        " WHERE CHAR_ID='" & i & "'"
'                        Call SET_Execute(strSQL)
                Next i
        Else 'ࡳ���ҧ�Ѻ��Шӻ� � ���һѨ�غѹ
                strBegin = "SELECT COUNT(OWNERSHIP_ID) "
                For i = 1 To 43
                    strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID FROM OWNERSHIP WHERE OWNER_TYPE = 1" & _
                    " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%') AS A WHERE EXISTS (SELECT * FROM PBT5 B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_YEAR=" & txtYear.Text & " AND PBT5_STATUS=0" & ") " & _
                    " OR EXISTS (SELECT * FROM PP1 C WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS=0 AND PP1_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PRD2 D WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND PRD2_YEAR=" & txtYear.Text & "))"
                Next i

                For i = 2 To 9
                        strSQL = strSQL & ",(SELECT COUNT(OWNERSHIP_ID) FROM (SELECT OWNERSHIP_ID FROM OWNERSHIP WHERE  OWNER_TYPE=" & i & ") AS A " & _
                        " WHERE EXISTS (SELECT * FROM PBT5 B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND PBT5_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PP1 C WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS=0 AND PP1_YEAR=" & txtYear.Text & ") " & _
                        " OR EXISTS (SELECT * FROM PRD2 D WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND PRD2_YEAR=" & txtYear.Text & ")) as TYPE" & i
                Next i
                strEnd = " FROM (SELECT OWNERSHIP_ID FROM OWNERSHIP WHERE OWNER_TYPE =1 AND OWNERSHIP_REAL_ID LIKE '�%') AS A WHERE EXISTS (SELECT * FROM PBT5 B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND PBT5_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PP1 C WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS=0 AND PP1_YEAR=" & txtYear.Text & ") OR EXISTS (SELECT * FROM PRD2 D WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND PRD2_YEAR=" & txtYear.Text & ")"
                strSQL = strBegin & strSQL & strEnd
                Call SET_QUERY(strSQL, RsTotal)

                strSQL = ""

                'PBT5
                strBegin = "SELECT COUNT(Temp) AS Tha1 " & _
                                ",(SELECT SUM(TEST) FROM (SELECT PBT5_AMOUNT AS TEST FROM  PBT5 A,OWNERSHIP B WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                "GROUP BY LAND_ID, PBT5_YEAR,PBT5_AMOUNT,PBT5_SET_GK, PBT5_STATUS,OWNER_TYPE,OWNERSHIP_REAL_ID HAVING PBT5_YEAR=" & txtYear.Text & " " & _
                                " AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND OWNER_TYPE=1 AND OWNERSHIP_REAL_ID LIKE '�%')) as ThiSum1"
                For i = 1 To 43
                strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID)  FROM OWNERSHIP A,PBT5 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS = 0 AND PBT5_YEAR =" & txtYear & _
                                " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%' AND OWNER_TYPE=1 GROUP BY  A.OWNERSHIP_ID)) as Tha" & i + 1 & _
                                ",(SELECT SUM(TEST) FROM (SELECT PBT5_AMOUNT AS TEST FROM  PBT5 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                " GROUP BY LAND_ID, PBT5_YEAR, PBT5_AMOUNT,PBT5_SET_GK,PBT5_STATUS,OWNER_TYPE,OWNERSHIP_REAL_ID HAVING PBT5_YEAR=" & txtYear & _
                                " AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND OWNER_TYPE=1 AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%')) as ThiSum" & i + 1
                Next i

                For i = 2 To 9
                        strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID) FROM OWNERSHIP A,PBT5 B " & _
                        " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND PBT5_YEAR =" & txtYear & _
                        " AND OWNER_TYPE=" & i & " GROUP BY A.OWNERSHIP_ID)) AS COUNT_TYPE" & i & _
                        ",(SELECT SUM(TEST) FROM (SELECT PBT5_AMOUNT AS TEST FROM  PBT5 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                        " GROUP BY LAND_ID, PBT5_YEAR, PBT5_AMOUNT,PBT5_SET_GK, PBT5_STATUS,OWNER_TYPE HAVING PBT5_YEAR=" & txtYear & _
                        " AND PBT5_SET_GK=1 AND PBT5_STATUS=0 AND  OWNER_TYPE =" & i & ")) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(B.OWNERSHIP_ID) as Temp FROM OWNERSHIP A , PBT5 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBT5_SET_GK=1 AND PBT5_STATUS = 0 AND PBT5_YEAR =" & txtYear & " AND OWNERSHIP_REAL_ID LIKE '�%'" & " AND OWNER_TYPE=1 GROUP BY A.OWNERSHIP_ID)"
                strSQL = strBegin & strSQL & strEnd
                Call SET_QUERY(strSQL, rs)

                strSQL = ""
                'PP1
                strBegin = "SELECT COUNT(Temp) AS Tha1" & _
                                ",(SELECT SUM(TEST)   FROM (SELECT PP1_AMOUNT AS TEST FROM  PP1 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                "GROUP BY SIGNBORD_ID, PP1_YEAR, PP1_AMOUNT,OWNER_TYPE,PP1_SET_GK,PP1_STATUS,OWNERSHIP_REAL_ID HAVING PP1_YEAR=" & txtYear.Text & " " & _
                                " AND PP1_SET_GK=1AND PP1_STATUS=0 AND OWNER_TYPE=1 AND OWNERSHIP_REAL_ID LIKE '�%')) AS ThiSum1"
                For i = 1 To 43
                strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID)  FROM OWNERSHIP A,PP1 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS = 0 AND PP1_YEAR =" & txtYear & _
                                " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%' AND OWNER_TYPE=1 GROUP BY A.OWNERSHIP_ID)) as Tha" & i + 1 & _
                                ",(SELECT SUM(TEST) FROM (SELECT PP1_AMOUNT AS TEST FROM  PP1 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                " GROUP BY SIGNBORD_ID, PP1_YEAR, PP1_AMOUNT,OWNER_TYPE,PP1_SET_GK, PP1_STATUS,OWNERSHIP_REAL_ID HAVING PP1_YEAR=" & txtYear & _
                                " AND PP1_SET_GK=1 AND PP1_STATUS=0 AND OWNER_TYPE=1 AND  OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%')) as ThiSum" & i + 1
                Next i

                For i = 2 To 9
                            strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID) FROM OWNERSHIP A,PP1 B " & _
                            " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS = 0 AND PP1_YEAR =" & txtYear & _
                            " AND OWNER_TYPE=" & i & " GROUP BY A.OWNERSHIP_ID)) AS COUNT_TYPE" & i & _
                            ",(SELECT SUM(TEST) FROM (SELECT PP1_AMOUNT AS TEST FROM  PP1 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                            " GROUP BY SIGNBORD_ID, PP1_YEAR, PP1_AMOUNT,PP1_SET_GK, PP1_STATUS,OWNER_TYPE HAVING PP1_YEAR=" & txtYear & _
                            " AND PP1_SET_GK=1 AND PP1_STATUS=0 AND  OWNER_TYPE =" & i & ")) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(A.OWNERSHIP_ID) AS Temp FROM OWNERSHIP A , PP1 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PP1_SET_GK=1 AND PP1_STATUS = 0 AND PP1_YEAR =" & txtYear & " AND OWNERSHIP_REAL_ID LIKE '�%'" & " AND OWNER_TYPE=1 GROUP BY A.OWNERSHIP_ID)"
                strSQL = strBegin & strSQL & strEnd
                Call SET_QUERY(strSQL, Rs2)

                strSQL = ""
                'PRD2
                strBegin = "SELECT COUNT(Temp) AS Tha1" & _
                                ",(SELECT SUM(TEST)   FROM (SELECT PRD2_RENTYEAR_TOTAL AS TEST FROM  PRD2 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                "GROUP BY BUILDING_ID, PRD2_YEAR, PRD2_RENTYEAR_TOTAL,OWNER_TYPE,PRD2_SET_GK, PRD2_STATUS,OWNERSHIP_REAL_ID HAVING PRD2_YEAR=" & txtYear.Text & " " & _
                                " AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND OWNER_TYPE=1 AND OWNERSHIP_REAL_ID LIKE '�%')) as ThiSum1"

                For i = 1 To 43
                strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID)  FROM OWNERSHIP A,PRD2 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PRD2_STATUS = 0 AND PRD2_SET_GK=1 AND PRD2_YEAR =" & txtYear & _
                                " AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%' AND OWNER_TYPE=1 GROUP BY A.OWNERSHIP_ID)) AS Tha" & i + 1 & _
                                ",(SELECT SUM(TEST) FROM (SELECT PRD2_RENTYEAR_TOTAL AS TEST FROM  PRD2 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                                " GROUP BY BUILDING_ID, PRD2_YEAR, PRD2_RENTYEAR_TOTAL,OWNER_TYPE,PRD2_SET_GK, PRD2_STATUS,OWNERSHIP_REAL_ID HAVING PRD2_YEAR=" & txtYear & _
                                " AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND OWNER_TYPE=1 AND OWNERSHIP_REAL_ID LIKE '" & tmpArr(i) & "%')) AS ThiSum" & i + 1
                Next i

                For i = 2 To 9
                            strSQL = strSQL & ",(SELECT COUNT(*) FROM (SELECT COUNT(A.OWNERSHIP_ID) FROM OWNERSHIP A,PRD2 B " & _
                            " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PRD2_STATUS = 0 AND PRD2_SET_GK=1 AND PRD2_YEAR =" & txtYear & _
                            " AND OWNER_TYPE=" & i & " GROUP BY A.OWNERSHIP_ID)) AS COUNT_TYPE" & i & _
                            ",(SELECT SUM(TEST) FROM (SELECT PRD2_RENTYEAR_TOTAL AS TEST FROM  PRD2 A,OWNERSHIP B   WHERE B.OWNERSHIP_ID=A.OWNERSHIP_ID " & _
                            " GROUP BY BUILDING_ID, PRD2_YEAR, PRD2_RENTYEAR_TOTAL,PRD2_SET_GK, PRD2_STATUS,OWNER_TYPE HAVING PRD2_YEAR=" & txtYear & _
                            " AND PRD2_SET_GK=1 AND PRD2_STATUS=0 AND  OWNER_TYPE =" & i & ")) AS SUM_TYPE" & i
                Next i

                strEnd = " FROM (SELECT COUNT(A.OWNERSHIP_ID) AS Temp FROM OWNERSHIP A , PRD2 B " & _
                                " WHERE B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PRD2_STATUS = 0 AND PRD2_SET_GK=1 AND PRD2_YEAR =" & txtYear & " AND OWNERSHIP_REAL_ID LIKE '�%'" & " AND OWNER_TYPE=1 GROUP BY A.OWNERSHIP_ID)"
                strSQL = strBegin & strSQL & strEnd
                Call SET_QUERY(strSQL, Rs3)

                For i = 0 To 43
                        strSQL = "UPDATE Temp_GK SET TOTAL_COUNT=" & RsTotal.Fields(i).Value & " WHERE CHAR_ID='" & tmpArr(i) & "'"
                        Call SET_Execute(strSQL)
                Next i

                For i = 2 To 9
                        strSQL = "UPDATE Temp_GK SET TOTAL_COUNT=" & RsTotal.Fields("TYPE" & i).Value & " WHERE CHAR_ID='" & i & "'"
                        Call SET_Execute(strSQL)
                Next i

                For i = 0 To 43
                        strSQL = "UPDATE Temp_GK SET PBT5_COUNT=" & rs.Fields("Tha" & i + 1).Value & _
                                        ",PBT5_SUMTAX=" & IIf(IsNull(rs.Fields("ThiSum" & i + 1).Value), 0#, rs.Fields("ThiSum" & i + 1).Value) & _
                                        ",PP1_COUNT=" & Rs2.Fields("Tha" & i + 1).Value & _
                                        ",PP1_SUMTAX=" & IIf(IsNull(Rs2.Fields("ThiSum" & i + 1).Value), 0#, Rs2.Fields("ThiSum" & i + 1).Value) & _
                                        ",PRD2_COUNT=" & Rs3.Fields("Tha" & i + 1).Value & _
                                        ",PRD2_SUMTAX=" & IIf(IsNull(Rs3.Fields("ThiSum" & i + 1).Value), 0#, Rs3.Fields("ThiSum" & i + 1).Value) & _
                                        " WHERE CHAR_ID='" & tmpArr(i) & "'"
                        Call SET_Execute(strSQL)
               Next i
               For i = 2 To 9
                        strSQL = "UPDATE Temp_GK SET PBT5_COUNT=" & rs.Fields("COUNT_TYPE" & i).Value & _
                                        ",PBT5_SUMTAX=" & IIf(IsNull(rs.Fields("SUM_TYPE" & i).Value), 0#, rs.Fields("SUM_TYPE" & i).Value) & _
                                        ",PP1_COUNT=" & Rs2.Fields("COUNT_TYPE" & i).Value & _
                                        ",PP1_SUMTAX=" & IIf(IsNull(Rs2.Fields("SUM_TYPE" & i).Value), 0#, Rs2.Fields("SUM_TYPE" & i).Value) & _
                                        ",PRD2_COUNT=" & Rs3.Fields("COUNT_TYPE" & i).Value & _
                                        ",PRD2_SUMTAX=" & IIf(IsNull(Rs3.Fields("SUM_TYPE" & i).Value), 0#, Rs3.Fields("SUM_TYPE" & i).Value) & _
                                        " WHERE CHAR_ID='" & i & "'"
                        Call SET_Execute(strSQL)
                Next i
        End If
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View4 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        MsgBox Err.Description
        Create_View4 = False
End Function

Private Function Create_View5() As Boolean
        Dim strSQL2 As String
        Dim strSQL3 As String
        Dim strSQL4 As String
        
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        Globle_Connective.BeginTrans
'        If DTPicker2.Visible = True Then
                strSQL2 = " AND (CONVERT(int, PBT5_PAY_DATE, 3) >= " & CLng(DTPicker1.Value) & " AND CONVERT(int, PBT5_PAY_DATE, 3)<= " & CLng(DTPicker2.Value) & ")"
                strSQL3 = " AND (CONVERT(int, PRD2_PAY_DATE, 3) >= " & CLng(DTPicker1.Value) & " AND CONVERT(int, PRD2_PAY_DATE, 3) <= " & CLng(DTPicker2.Value) & ")"
                strSQL4 = " AND (CONVERT(int, PP1_PAY_DATE, 3) >= " & CLng(DTPicker1.Value) & " AND CONVERT(int, PP1_PAY_DATE, 3)<= " & CLng(DTPicker2.Value) & ")"
        If Cmb_Type.ListIndex > 0 Then
                strSQL2 = strSQL2 & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
                strSQL3 = strSQL3 & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
                strSQL4 = strSQL4 & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
        End If
        strSQL2 = strSQL2 & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        strSQL3 = strSQL3 & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        strSQL4 = strSQL4 & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        
        strSQL = "ALTER VIEW VIEW_OBTAIN_TOTAL AS " & _
            "SELECT CASE WHEN SUM_PBT5 IS NULL THEN 0 ELSE SUM_PBT5 END AS S_PBT5, CASE WHEN SUM_PRD2 IS NULL" & _
            " THEN 0 ELSE SUM_PRD2 END AS S_PRD2, CASE WHEN SUM_PP1 IS NULL THEN 0 ELSE SUM_PP1 END AS S_PP1" & _
            " FROM (SELECT SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS SUM_PBT5" & _
            " From dbo.PBT5 WHERE (PBT5_STATUS = 1) " & strSQL2 & " ) AS A CROSS JOIN" & _
            " (SELECT SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS SUM_PRD2" & _
            " From dbo.PRD2 WHERE (PRD2_STATUS = 1) " & strSQL3 & " ) AS B CROSS JOIN" & _
            " (SELECT SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) AS SUM_PP1" & _
            " From dbo.PP1 WHERE (PP1_STATUS = 1) " & strSQL4 & " ) AS C"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View5 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View5 = False
        MsgBox Err.Description
End Function

Private Function Create_View6() As Boolean
Dim strSQL6 As String
Dim FlagType As String
On Error GoTo ERR_CREATE_VIEW
        Globle_Connective.BeginTrans
        Me.MousePointer = 11
        If ChkAllyr.Value = Unchecked Then
                strSQL6 = " WHERE (AA.PBT5_YEAR = " & txtYear & ")"
        End If
        strSQL = ""
'        strSQL = "DROP VIEW VIEW_REAL_KEEP"
'        Call SET_Execute(strSQL)
        If TreeView1.SelectedItem.Index = 16 Then
                strSQL = "ALTER VIEW VIEW_REAL_KEEP AS " & _
                "SELECT AA.PBT5_YEAR, CASE WHEN GK_PBT5 IS NULL THEN 0 ELSE GK_PBT5 END AS GKPBT5, CASE WHEN OBTAIN_PBT5 IS NULL THEN 0 ELSE OBTAIN_PBT5 END AS OBTAINPBT5, CASE WHEN REAL_KEEP_PBT5 IS NULL THEN 0 ELSE REAL_KEEP_PBT5 END AS REAL_KEEP_PBT5, CASE WHEN GK_PRD2 IS NULL THEN 0 ELSE GK_PRD2 END AS GKPRD2," & _
                "CASE WHEN OBTAIN_PRD2 IS NULL THEN 0 ELSE OBTAIN_PRD2 END AS OBTAINPRD2, CASE WHEN REAL_KEEP_PRD2 IS NULL THEN 0 ELSE REAL_KEEP_PRD2 END AS REAL_KEEP_PRD2, CASE WHEN GK_PP1 IS NULL THEN 0 ELSE GK_PP1 END AS GKPP1,CASE WHEN OBTAIN_PP1 IS NULL THEN 0 ELSE OBTAIN_PP1 END AS OBTAINPP1, CASE WHEN REAL_KEEP_PP1 IS NULL" & _
                " THEN 0 ELSE REAL_KEEP_PP1 END AS REAL_KEEP_PP1 FROM (SELECT PBT5_YEAR FROM dbo.PBT5 WHERE (PBT5_SET_GK = 1) GROUP BY PBT5_YEAR Union SELECT PRD2_YEAR FROM dbo.PRD2 WHERE (PRD2_SET_GK = 1) GROUP BY PRD2_YEAR Union SELECT PP1_YEAR FROM dbo.PP1 WHERE (PP1_SET_GK = 1) GROUP BY PP1_YEAR ) AS AA LEFT OUTER JOIN" & _
                " (SELECT (CASE WHEN GK_PBT5 IS NULL THEN 0 ELSE GK_PBT5 END) - (CASE WHEN OBTAIN_PBT5 IS NULL THEN 0 ELSE OBTAIN_PBT5 END) AS REAL_KEEP_PBT5, A_2.GK_PBT5, B_1.OBTAIN_PBT5, A_2.PBT5_YEAR" & _
                " FROM (SELECT SUM(PBT5_GK_Money) AS GK_PBT5, PBT5_YEAR FROM (SELECT PBT5_GK_Money, OWNERSHIP_ID, PBT5_YEAR" & _
                " FROM  dbo.PBT5 AS PBT5_1 WHERE PBT5_SET_GK=1 GROUP BY OWNERSHIP_ID, PBT5_GK_Money, PBT5_YEAR) AS A_1 GROUP BY PBT5_YEAR) AS A_2 LEFT OUTER JOIN" & _
                " (SELECT SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS OBTAIN_PBT5, PBT5_YEAR" & _
                " FROM dbo.PBT5 AS PBT5_2 WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK=1) GROUP BY PBT5_YEAR) AS B_1 ON B_1.PBT5_YEAR = A_2.PBT5_YEAR) AS B ON AA.PBT5_YEAR = B.PBT5_YEAR LEFT OUTER JOIN" & _
                " (SELECT (CASE WHEN GK_PRD2 IS NULL THEN 0 ELSE GK_PRD2 END) - (CASE WHEN OBTAIN_PRD2 IS NULL THEN 0 ELSE OBTAIN_PRD2 END) AS REAL_KEEP_PRD2, A_2_1.GK_PRD2, B_1_1.OBTAIN_PRD2, A_2_1.PRD2_YEAR" & _
                " FROM (SELECT SUM(PRD2_GK_Money) AS GK_PRD2, PRD2_YEAR FROM (SELECT PRD2_GK_Money, OWNERSHIP_ID, PRD2_YEAR" & _
                " FROM  dbo.PRD2 AS PRD2_1 WHERE PRD2_SET_GK=1 GROUP BY OWNERSHIP_ID, PRD2_GK_Money, PRD2_YEAR) AS A_1_1 GROUP BY PRD2_YEAR) AS A_2_1 LEFT OUTER JOIN" & _
                " (SELECT SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS OBTAIN_PRD2, PRD2_YEAR" & _
                " FROM dbo.PRD2 AS PRD2_2 WHERE (PRD2_STATUS = 1) AND (PRD2_SET_GK=1) GROUP BY PRD2_YEAR) AS B_1_1 ON B_1_1.PRD2_YEAR = A_2_1.PRD2_YEAR) AS D ON AA.PBT5_YEAR = D.PRD2_YEAR LEFT OUTER JOIN" & _
                " (SELECT (CASE WHEN GK_PP1 IS NULL THEN 0 ELSE GK_PP1 END) - (CASE WHEN OBTAIN_PP1 IS NULL THEN 0 ELSE OBTAIN_PP1 END) AS REAL_KEEP_PP1, A_2_1_1.GK_PP1, B_1_1_1.OBTAIN_PP1, A_2_1_1.PP1_YEAR" & _
                " FROM (SELECT SUM(PP1_GK_Money) AS GK_PP1, PP1_YEAR FROM (SELECT PP1_GK_Money, OWNERSHIP_ID, PP1_YEAR FROM dbo.PP1 AS PP1_1 WHERE PP1_SET_GK=" & _
                " 1 GROUP BY OWNERSHIP_ID, PP1_GK_Money, PP1_YEAR) AS A_1_1_1 GROUP BY PP1_YEAR) AS A_2_1_1 LEFT OUTER JOIN" & _
                " (SELECT SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) AS OBTAIN_PP1, PP1_YEAR FROM dbo.PP1 AS PP1_2 WHERE (PP1_STATUS = 1) AND (PP1_SET_GK=1)" & _
                " GROUP BY PP1_YEAR) AS B_1_1_1 ON B_1_1_1.PP1_YEAR = A_2_1_1.PP1_YEAR) AS E ON AA.PBT5_YEAR = E.PP1_YEAR" & strSQL6
        ElseIf TreeView1.SelectedItem.Index = 17 Then
                strSQL = "ALTER VIEW VIEW_REAL_KEEP_OTHER AS "
                strSQL = strSQL & "SELECT AA.PBT5_YEAR, CASE WHEN GK_PBT5 IS NULL THEN 0 ELSE GK_PBT5 END AS GKPBT5, CASE WHEN OBTAIN_PBT5 IS NULL THEN 0 ELSE OBTAIN_PBT5 END AS OBTAINPBT5, CASE WHEN REAL_KEEP_PBT5 IS NULL THEN 0 ELSE REAL_KEEP_PBT5 END AS REAL_KEEP_PBT5, CASE WHEN PBT5_EXTEND IS NULL THEN 0 ELSE PBT5_EXTEND END AS PBT5_EXTEND, CASE WHEN GK_PRD2 IS NULL THEN 0 ELSE GK_PRD2 END AS GKPRD2,CASE WHEN OBTAIN_PRD2 IS NULL THEN 0 ELSE OBTAIN_PRD2 END AS OBTAINPRD2, CASE WHEN REAL_KEEP_PRD2 IS NULL THEN 0 ELSE REAL_KEEP_PRD2 END AS REAL_KEEP_PRD2, CASE WHEN PRD2_EXTEND IS NULL THEN 0 ELSE PRD2_EXTEND END AS PRD2_EXTEND, CASE WHEN GK_PP1 IS NULL THEN 0 ELSE GK_PP1 END AS GKPP1,CASE WHEN OBTAIN_PP1 IS NULL THEN 0 ELSE OBTAIN_PP1 END AS OBTAINPP1,"
                strSQL = strSQL & " CASE WHEN REAL_KEEP_PP1 IS NULL THEN 0 ELSE REAL_KEEP_PP1 END AS REAL_KEEP_PP1, CASE WHEN PP1_EXTEND IS NULL THEN 0 ELSE PP1_EXTEND END AS PP1_EXTEND,I.SUM_PBT5_DEPTOR, J.SUM_PRD2_DEPTOR, K.SUM_PP1_DEPTOR"
                strSQL = strSQL & " FROM (SELECT PBT5_YEAR From dbo.PBT5 GROUP BY PBT5_YEAR Union SELECT PRD2_YEAR From dbo.PRD2 GROUP BY PRD2_YEAR Union SELECT PP1_YEAR From dbo.PP1 GROUP BY PP1_YEAR UNION SELECT ASSET_YEAR FROM dbo.ASSETDATA_EXTEND GROUP BY ASSET_YEAR) AS AA LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT (CASE WHEN GK_PBT5 IS NULL THEN 0 ELSE GK_PBT5 END) - (CASE WHEN OBTAIN_PBT5 IS NULL THEN 0 ELSE OBTAIN_PBT5 END) AS REAL_KEEP_PBT5, A_2.GK_PBT5, B_1.OBTAIN_PBT5, A_2.PBT5_YEAR"
                strSQL = strSQL & " FROM (SELECT SUM(PBT5_AMOUNT) AS GK_PBT5, PBT5_YEAR FROM (SELECT PBT5_AMOUNT, PBT5_YEAR FROM dbo.PBT5 AS PBT5_1"
                strSQL = strSQL & " WHERE (PBT5_SET_GK <> 1) AND (FLAG_NO_USED = 'FALSE')) AS A_1 GROUP BY PBT5_YEAR) AS A_2 LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT SUM(PBT5_AMOUNT_REAL + PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS OBTAIN_PBT5, PBT5_YEAR FROM dbo.PBT5 AS PBT5_2"
                strSQL = strSQL & " WHERE (PBT5_STATUS = 1) AND (PBT5_SET_GK <> 1) AND (FLAG_NO_USED = 'FALSE') OR (PBT5_STATUS = 1) AND (PBT5_SET_GK = 1) AND (PBT5_IN_GK = 2) GROUP BY PBT5_YEAR) AS B_1 ON B_1.PBT5_YEAR = A_2.PBT5_YEAR) AS B ON AA.PBT5_YEAR = B.PBT5_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT SUM(TAX_MONEY) AS PBT5_EXTEND, ASSET_YEAR From dbo.ASSETDATA_EXTEND Where (ASSET_TYPE_ID = 1)"
                strSQL = strSQL & " GROUP BY ASSET_YEAR) AS F ON AA.PBT5_YEAR = F.ASSET_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT ISNULL(SUM(PBT5_AMOUNT_REAL + PBT5_DISCOUNT + PBT5_TAXINCLUDE), 0) AS SUM_PBT5_DEPTOR, '" & txtYear.Text & "' AS PBT5_YEAR"
                strSQL = strSQL & " FROM dbo.PBT5 AS PBT5_3"
                strSQL = strSQL & " WHERE (PBT5_YEAR < " & txtYear.Text & ") AND (PBT5_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')) AS I ON"
                strSQL = strSQL & " AA.PBT5_YEAR = i.PBT5_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT (CASE WHEN GK_PRD2 IS NULL THEN 0 ELSE GK_PRD2 END) - (CASE WHEN OBTAIN_PRD2 IS NULL THEN 0 ELSE OBTAIN_PRD2 END) AS REAL_KEEP_PRD2, A_2_1.GK_PRD2, B_1_1.OBTAIN_PRD2, A_2_1.PRD2_YEAR"
                strSQL = strSQL & " FROM (SELECT SUM(PRD2_RENTYEAR_TOTAL) AS GK_PRD2, PRD2_YEAR FROM (SELECT PRD2_RENTYEAR_TOTAL, PRD2_YEAR FROM dbo.PRD2 AS PRD2_1 WHERE (PRD2_SET_GK <> 1) AND (FLAG_NO_USED = 'FALSE')) AS A_1_1"
                strSQL = strSQL & " GROUP BY PRD2_YEAR) AS A_2_1 LEFT OUTER JOIN (SELECT SUM(PRD2_AMOUNT_REAL + PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS OBTAIN_PRD2, PRD2_YEAR FROM dbo.PRD2 AS PRD2_2 Where (PRD2_STATUS = 1) And (PRD2_SET_GK <> 1) OR (PRD2_STATUS = 1) AND (PRD2_SET_GK = 1) AND (PRD2_IN_GK = 2)"
                strSQL = strSQL & " GROUP BY PRD2_YEAR) AS B_1_1 ON B_1_1.PRD2_YEAR = A_2_1.PRD2_YEAR) AS D ON AA.PBT5_YEAR = D.PRD2_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & "(SELECT ISNULL(SUM(PRD2_AMOUNT_REAL + PRD2_DISCOUNT + PRD2_TAXINCLUDE), 0) AS SUM_PRD2_DEPTOR, '" & txtYear.Text & "' AS PBT5_YEAR"
                strSQL = strSQL & " FROM dbo.PRD2 AS PRD2_3"
                strSQL = strSQL & " WHERE (PRD2_YEAR < " & txtYear.Text & ") AND (PRD2_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')) AS J ON"
                strSQL = strSQL & " AA.PBT5_YEAR = J.PBT5_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT SUM(TAX_MONEY) AS PRD2_EXTEND, ASSET_YEAR FROM dbo.ASSETDATA_EXTEND AS ASSETDATA_EXTEND_1 Where (ASSET_TYPE_ID = 2)"
                strSQL = strSQL & " GROUP BY ASSET_YEAR) AS G ON AA.PBT5_YEAR = G.ASSET_YEAR LEFT OUTER JOIN (SELECT (CASE WHEN GK_PP1 IS NULL THEN 0 ELSE GK_PP1 END) - (CASE WHEN OBTAIN_PP1 IS NULL THEN 0 ELSE OBTAIN_PP1 END) AS REAL_KEEP_PP1, A_2_1_1.GK_PP1, B_1_1_1.OBTAIN_PP1, A_2_1_1.PP1_YEAR"
                strSQL = strSQL & " FROM (SELECT SUM(PP1_AMOUNT) AS GK_PP1, PP1_YEAR FROM (SELECT PP1_AMOUNT, PP1_YEAR FROM dbo.PP1 AS PP1_1 WHERE (PP1_SET_GK <> 1) AND (FLAG_NO_USED = 'FALSE')) AS A_1_1_1"
                strSQL = strSQL & " GROUP BY PP1_YEAR) AS A_2_1_1 LEFT OUTER JOIN (SELECT SUM(PP1_AMOUNT_REAL + PP1_TAXINCLUDE + PP1_DISCOUNT) AS OBTAIN_PP1, PP1_YEAR FROM dbo.PP1 AS PP1_2"
                strSQL = strSQL & " Where (PP1_STATUS = 1) And (PP1_SET_GK <> 1) AND (FLAG_NO_USED = 'FALSE') OR (PP1_STATUS = 1) AND (PP1_SET_GK = 1) AND (PP1_IN_GK = 2)GROUP BY PP1_YEAR) AS B_1_1_1 ON B_1_1_1.PP1_YEAR = A_2_1_1.PP1_YEAR) AS E ON AA.PBT5_YEAR = E.PP1_YEAR LEFT OUTER JOIN"
                strSQL = strSQL & " (SELECT SUM(TAX_MONEY) AS PP1_EXTEND, ASSET_YEAR FROM dbo.ASSETDATA_EXTEND AS ASSETDATA_EXTEND_1 Where (ASSET_TYPE_ID = 3)"
                strSQL = strSQL & " GROUP BY ASSET_YEAR) AS H ON AA.PBT5_YEAR = H.ASSET_YEAR"
                strSQL = strSQL & " LEFT OUTER JOIN (SELECT ISNULL(SUM(PP1_AMOUNT_REAL + PP1_DISCOUNT + PP1_TAXINCLUDE), 0) AS SUM_PP1_DEPTOR, '" & txtYear.Text & "' AS PBT5_YEAR"
                strSQL = strSQL & " FROM dbo.PP1 AS PP1_3"
                strSQL = strSQL & " WHERE (PP1_YEAR < " & txtYear.Text & ") AND (PP1_PAY_DATE BETWEEN '" & CInt(txtYear.Text) - 544 & "-10-01' AND '" & CInt(txtYear.Text) - 543 & "-09-30')) AS K ON AA.PBT5_YEAR = K.PBT5_YEAR" & strSQL6
        End If
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View6 = True
        Exit Function
ERR_CREATE_VIEW:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        MsgBox Err.Description
        Create_View6 = False
End Function

Private Function Create_View14() As Boolean
        On Error GoTo ERR_1
        Dim strTemp As String
        Globle_Connective.BeginTrans
        Frm_PrintSummary.MousePointer = 11
        
        If Cmb_Type.ListIndex > 0 Then
                strTemp = strTemp & " AND OWNER_TYPE=" & Cmb_Type.ListIndex
        End If
        strTemp = strTemp & QueryUtility_Char(Combo1(0), Combo1(1), "OWNER_NAME")
        Select Case cbType.ListIndex
                Case 0
                        strSQL = "ALTER VIEW VIEW_PBT5_NOT_PERFORM AS" & _
                                " SELECT  TOP (100) PERCENT OWNERSHIP_ID, SUM(PBT5_AMOUNT) AS sum_pbt5_tax, " & _
                                " PRENAME + OWNER_NAME + '   ' + OWNER_SURNAME AS fullname,PBT5_YEAR , Owner_Type, OWNER_NAME" & _
                                " From dbo.PBT5 WHERE (PBT5_YEAR = " & txtYear.Text & ") AND EXISTS (SELECT  LAND_ID From dbo.LANDDATA" & _
                                " WHERE (LAND_ID = dbo.PBT5.LAND_ID) AND (FLAG_PAYTAX = 1))" & strTemp & " AND (PBT5_NO_STATUS = 0) AND (PBT5_STATUS = 0) AND" & _
                                " (PBT5_SET_GK = 1) AND EXISTS" & _
                                " (SELECT  LAND_NOTIC_ID, LAND_ID, OWNERSHIP_ID, OWNERSHIP_NOTIC, OWNERSHIP_MAIL" & _
                                " From dbo.LANDDATA_NOTIC" & _
                                " WHERE (OWNERSHIP_ID = dbo.PBT5.OWNERSHIP_ID) AND (LAND_ID = dbo.PBT5.LAND_ID) AND (OWNERSHIP_NOTIC = 1))" & _
                                " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PBT5_YEAR, OWNER_TYPE" & _
                                " ORDER BY OWNER_NAME"
                Case 1
                        strSQL = "ALTER VIEW VIEW_PRD2_NOT_PERFORM AS" & _
                            " SELECT TOP (100) PERCENT OWNERSHIP_ID, SUM(PRD2_RENTYEAR_TOTAL) AS sum_prd2_tax," & _
                            " PRENAME + OWNER_NAME + '   ' + OWNER_SURNAME AS fullname, PRD2_YEAR, OWNER_TYPE, OWNER_NAME" & _
                            " From dbo.PRD2 WHERE (PRD2_YEAR =" & txtYear.Text & ") " & strTemp & " AND (PRD2_NO_STATUS = 0) AND (PRD2_STATUS = 0) AND" & _
                            " (PRD2_SET_GK = 1) GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PRD2_YEAR, OWNER_TYPE" & _
                            " ORDER BY OWNER_NAME"
                Case 2
                        strSQL = "ALTER VIEW VIEW_PP1_NOT_PERFORM AS" & _
                                " SELECT  TOP (100) PERCENT OWNERSHIP_ID, SUM(PP1_AMOUNT) AS sum_pp1_tax, PRENAME + OWNER_NAME + '   ' + OWNER_SURNAME AS fullname," & _
                                " PP1_YEAR , Owner_Type, OWNER_NAME" & _
                                " From dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") " & strTemp & " AND (PP1_NO_STATUS = 0) AND (PP1_STATUS = 0) AND (PP1_SET_GK = 1)" & _
                                " GROUP BY OWNERSHIP_ID, PRENAME, OWNER_NAME, OWNER_SURNAME, PP1_YEAR, OWNER_TYPE" & _
                                " ORDER BY OWNER_NAME"
        End Select
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View14 = True
        Exit Function
ERR_1:
        If Err.Number = -2147217865 Then
                Globle_Connective.RollbackTrans
                Frm_PrintSummary.MousePointer = 0
                Create_View14 = False
        Else
                Globle_Connective.RollbackTrans
                Frm_PrintSummary.MousePointer = 0
                MsgBox Err.Description
                Create_View14 = False
        End If
End Function


