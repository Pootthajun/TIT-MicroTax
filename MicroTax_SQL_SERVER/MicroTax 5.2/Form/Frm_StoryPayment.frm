VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_StoryPayment 
   BackColor       =   &H00C0C0C0&
   ClientHeight    =   9195
   ClientLeft      =   165
   ClientTop       =   165
   ClientWidth     =   12915
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "Frm_StoryPayment.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_StoryPayment.frx":151A
   ScaleHeight     =   9195
   ScaleMode       =   0  'User
   ScaleWidth      =   13594.74
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cbArticle 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":183C0
      Left            =   5040
      List            =   "Frm_StoryPayment.frx":183D0
      Style           =   2  'Dropdown List
      TabIndex        =   61
      Top             =   30
      Width           =   2355
   End
   Begin VB.ComboBox cbZone 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":1842D
      Left            =   3480
      List            =   "Frm_StoryPayment.frx":1842F
      TabIndex        =   60
      Text            =   "cbZone"
      Top             =   510
      Width           =   915
   End
   Begin VB.ComboBox cbStart_Char 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":18431
      Left            =   7530
      List            =   "Frm_StoryPayment.frx":184B9
      Style           =   2  'Dropdown List
      TabIndex        =   56
      Top             =   510
      Width           =   645
   End
   Begin VB.ComboBox cbEnd_Char 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":18540
      Left            =   8610
      List            =   "Frm_StoryPayment.frx":185C8
      Style           =   2  'Dropdown List
      TabIndex        =   55
      Top             =   510
      Width           =   645
   End
   Begin VB.CheckBox chkFlag2 
      BackColor       =   &H00000000&
      Height          =   195
      Left            =   5910
      TabIndex        =   53
      Top             =   630
      Width           =   195
   End
   Begin VB.CheckBox chkFlag1 
      BackColor       =   &H00000000&
      Height          =   195
      Left            =   4620
      TabIndex        =   52
      Top             =   630
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox chkAllYear 
      BackColor       =   &H00000000&
      Height          =   195
      Left            =   2880
      TabIndex        =   51
      Top             =   150
      Width           =   195
   End
   Begin VB.ComboBox cmb_SignBoard_Owner 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":1864F
      Left            =   870
      List            =   "Frm_StoryPayment.frx":18671
      Style           =   2  'Dropdown List
      TabIndex        =   50
      Top             =   510
      Width           =   1935
   End
   Begin VB.ComboBox Cmb_StoryAll 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayment.frx":186D8
      Left            =   7410
      List            =   "Frm_StoryPayment.frx":186EE
      Style           =   2  'Dropdown List
      TabIndex        =   49
      Top             =   30
      Width           =   4335
   End
   Begin VB.TextBox txtName 
      Height          =   315
      Left            =   9870
      TabIndex        =   27
      Top             =   540
      Width           =   1185
   End
   Begin VB.TextBox txtSurName 
      Height          =   315
      Left            =   11700
      TabIndex        =   26
      Top             =   540
      Width           =   1125
   End
   Begin VB.TextBox txtYear 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   24
      Top             =   30
      Width           =   795
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_PRD2 
      Height          =   1125
      Left            =   3300
      TabIndex        =   3
      Top             =   5610
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   1984
      _Version        =   393216
      Cols            =   19
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   19
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_PP1 
      Height          =   1245
      Left            =   3300
      TabIndex        =   2
      Top             =   3630
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   2196
      _Version        =   393216
      Cols            =   13
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   13
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_PBT5 
      Height          =   1245
      Left            =   3300
      TabIndex        =   1
      Top             =   1650
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   2196
      _Version        =   393216
      BackColor       =   16777215
      Cols            =   13
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   13
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_OwnerShip 
      Height          =   5055
      Left            =   0
      TabIndex        =   0
      Top             =   930
      Width           =   3045
      _ExtentX        =   5371
      _ExtentY        =   8916
      _Version        =   393216
      BackColor       =   16777215
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   0
      ForeColorSel    =   255
      BackColorBkg    =   12632256
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0).GridLinesBand=   0
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   405
      Left            =   4770
      TabIndex        =   25
      Top             =   0
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   714
      _Version        =   393216
      Value           =   2000
      BuddyControl    =   "txtYear"
      BuddyDispid     =   196620
      OrigLeft        =   1800
      OrigTop         =   30
      OrigRight       =   2055
      OrigBottom      =   345
      Max             =   3500
      Min             =   2000
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_PBA1 
      Height          =   1245
      Left            =   3300
      TabIndex        =   40
      Top             =   7470
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   2196
      _Version        =   393216
      Cols            =   14
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   14
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   11
      Left            =   10095
      TabIndex        =   73
      Top             =   6810
      Width           =   2475
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   10
      Left            =   10095
      TabIndex        =   72
      Top             =   4950
      Width           =   2475
   End
   Begin VB.Label lbRealPP1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   12030
      TabIndex        =   74
      Top             =   4950
      Width           =   75
   End
   Begin VB.Label lbRealPRD2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   12030
      TabIndex        =   75
      Top             =   6810
      Width           =   75
   End
   Begin VB.Label lbRealPBT5 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   12030
      TabIndex        =   70
      Top             =   2940
      Width           =   75
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   11
      Left            =   10020
      Shape           =   4  'Rounded Rectangle
      Top             =   6720
      Width           =   2595
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   10
      Left            =   10020
      Shape           =   4  'Rounded Rectangle
      Top             =   4860
      Width           =   2595
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   9
      Left            =   10095
      TabIndex        =   71
      Top             =   2940
      Width           =   2445
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00000000&
      FillColor       =   &H0041745F&
      Height          =   375
      Left            =   11760
      Top             =   0
      Width           =   525
   End
   Begin VB.Label lbRecordCount_PBT5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1500
      TabIndex        =   65
      Top             =   6030
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡�÷��Թ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   13
      Left            =   0
      TabIndex        =   69
      Top             =   6030
      Width           =   1485
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   2
      Left            =   3330
      Picture         =   "Frm_StoryPayment.frx":1876F
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   3270
      Width           =   480
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   4
      Left            =   3330
      Picture         =   "Frm_StoryPayment.frx":1AAF3
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   7110
      Width           =   480
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   3
      Left            =   3330
      Picture         =   "Frm_StoryPayment.frx":1CE77
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   5250
      Width           =   480
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "⫹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   7
      Left            =   2955
      TabIndex        =   59
      Top             =   600
      Width           =   450
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   6
      Left            =   60
      TabIndex        =   58
      Top             =   600
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�֧"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   5
      Left            =   8310
      TabIndex        =   57
      Top             =   630
      Width           =   210
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ҧ��������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   4
      Left            =   6180
      TabIndex        =   54
      Top             =   630
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥ�                           �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   8
      Left            =   10200
      TabIndex        =   48
      Top             =   8760
      Width           =   2385
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ                         �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   6
      Left            =   6615
      TabIndex        =   47
      Top             =   6780
      Width           =   2505
   End
   Begin VB.Label lbPrice_PBA1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   11970
      TabIndex        =   46
      Top             =   8760
      Width           =   75
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   0
      Left            =   10830
      TabIndex        =   45
      Top             =   7230
      Width           =   1155
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   9
      Left            =   3300
      TabIndex        =   44
      Top             =   8730
      Width           =   1635
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   4
      Left            =   4950
      TabIndex        =   43
      Top             =   8730
      Width           =   1410
   End
   Begin VB.Label lbTotalMoney 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   300
      Left            =   12105
      TabIndex        =   17
      Top             =   960
      Width           =   90
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   195
      Index           =   3
      Left            =   4920
      TabIndex        =   31
      Top             =   630
      Width           =   690
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   0
      Left            =   9420
      TabIndex        =   30
      Top             =   630
      Width           =   345
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ʡ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   1
      Left            =   11190
      TabIndex        =   29
      Top             =   630
      Width           =   435
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   2
      Left            =   3150
      TabIndex        =   28
      Top             =   120
      Width           =   765
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   3
      Left            =   4950
      TabIndex        =   23
      Top             =   6750
      Width           =   1410
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   3
      Left            =   3300
      TabIndex        =   22
      Top             =   6750
      Width           =   1635
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   2
      Left            =   4950
      TabIndex        =   21
      Top             =   4890
      Width           =   1410
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   2
      Left            =   3300
      TabIndex        =   20
      Top             =   4890
      Width           =   1635
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   1
      Left            =   4950
      TabIndex        =   19
      Top             =   2910
      Width           =   1410
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   1
      Left            =   3300
      TabIndex        =   18
      Top             =   2910
      Width           =   1635
   End
   Begin VB.Label lbPRice_PBT5 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   8460
      TabIndex        =   16
      Top             =   2940
      Width           =   75
   End
   Begin VB.Label lbPrice_PRD2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   8520
      TabIndex        =   14
      Top             =   6780
      Width           =   75
   End
   Begin VB.Label lbPrice_PP1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   240
      Left            =   8490
      TabIndex        =   13
      Top             =   4920
      Width           =   75
   End
   Begin VB.Label lbYear 
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� 2547"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   3150
      TabIndex        =   12
      Top             =   960
      Width           =   1605
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥ����                          �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   300
      Index           =   7
      Left            =   9465
      TabIndex        =   9
      Top             =   960
      Width           =   3300
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   6
      Left            =   6600
      Shape           =   4  'Rounded Rectangle
      Top             =   6690
      Width           =   2580
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ                         �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   5
      Left            =   6645
      TabIndex        =   8
      Top             =   4920
      Width           =   2505
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   5
      Left            =   6570
      Shape           =   4  'Rounded Rectangle
      Top             =   4830
      Width           =   2625
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   3
      Left            =   10860
      TabIndex        =   7
      Top             =   5370
      Width           =   1035
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   3
      Left            =   10020
      Shape           =   4  'Rounded Rectangle
      Top             =   5310
      Width           =   2595
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ջ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   2
      Left            =   11025
      TabIndex        =   6
      Top             =   3390
      Width           =   675
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   2
      Left            =   10035
      Shape           =   4  'Rounded Rectangle
      Top             =   3330
      Width           =   2580
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���պ��ا��ͧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   1
      Left            =   10740
      TabIndex        =   5
      Top             =   1410
      Width           =   1245
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   1
      Left            =   10005
      Shape           =   4  'Rounded Rectangle
      Top             =   1350
      Width           =   2595
   End
   Begin VB.Label lbFullName 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   4800
      TabIndex        =   4
      Top             =   960
      Width           =   90
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   345
      Index           =   0
      Left            =   3060
      Top             =   930
      Width           =   9795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   8
      Left            =   10020
      Shape           =   4  'Rounded Rectangle
      Top             =   7170
      Width           =   2595
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   7
      Left            =   10080
      Shape           =   4  'Rounded Rectangle
      Top             =   8670
      Width           =   2535
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ                         �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   4
      Left            =   6600
      TabIndex        =   15
      Top             =   2940
      Width           =   2505
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   4
      Left            =   6570
      Shape           =   4  'Rounded Rectangle
      Top             =   2850
      Width           =   2595
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   420
      Left            =   11790
      Picture         =   "Frm_StoryPayment.frx":1F1FB
      Stretch         =   -1  'True
      ToolTipText     =   "����"
      Top             =   -30
      Width           =   480
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   0
      Left            =   12330
      Picture         =   "Frm_StoryPayment.frx":21574
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   0
      Width           =   480
   End
   Begin VB.Label lbRecordCount_PRD2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1500
      TabIndex        =   63
      Top             =   6330
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡���ç���͹ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   12
      Left            =   0
      TabIndex        =   68
      Top             =   6330
      Width           =   1485
   End
   Begin VB.Label lbRecordCount_PP1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1500
      TabIndex        =   64
      Top             =   6630
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡�û��� : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   11
      Left            =   0
      TabIndex        =   67
      Top             =   6630
      Width           =   1485
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡���͹حҵ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   10
      Left            =   0
      TabIndex        =   66
      Top             =   6930
      Width           =   1485
   End
   Begin VB.Label lbRecordCount_PBA1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1500
      TabIndex        =   62
      Top             =   6930
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��� : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   0
      TabIndex        =   11
      Top             =   7230
      Width           =   1485
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   1500
      TabIndex        =   10
      Top             =   7230
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "���շ��Թ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   0
      TabIndex        =   34
      Top             =   7530
      Width           =   1485
   End
   Begin VB.Label lbSumPBT5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   1500
      TabIndex        =   38
      Top             =   7530
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�����ç���͹ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   6
      Left            =   0
      TabIndex        =   35
      Top             =   7830
      Width           =   1485
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "���շ����� : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   7
      Left            =   0
      TabIndex        =   36
      Top             =   8130
      Width           =   1485
   End
   Begin VB.Label lbSumPRD2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   1500
      TabIndex        =   37
      Top             =   7830
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�����͹حҵ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   8
      Left            =   0
      TabIndex        =   41
      Top             =   8430
      Width           =   1485
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��ػ�ʹ�Թ : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   0
      TabIndex        =   32
      Top             =   8730
      Width           =   1485
   End
   Begin VB.Label lbSumPP1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   1500
      TabIndex        =   39
      Top             =   8130
      Width           =   1560
   End
   Begin VB.Label lbSumPBA1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   1500
      TabIndex        =   42
      Top             =   8430
      Width           =   1560
   End
   Begin VB.Label lbTotalMoneyAll 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   1500
      TabIndex        =   33
      Top             =   8730
      Width           =   1560
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   390
      Index           =   1
      Left            =   3330
      Picture         =   "Frm_StoryPayment.frx":238F8
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   1290
      Width           =   480
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   375
      Index           =   9
      Left            =   10050
      Shape           =   4  'Rounded Rectangle
      Top             =   2880
      Width           =   2565
   End
End
Attribute VB_Name = "Frm_StoryPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rs As ADODB.Recordset
Dim NumYear As Integer
Dim strSQL As String ', strSQL2 As String, strSQL3 As String, strSQL4 As String
'Dim strSQL_Sum1 As String, strSQL_Sum2 As String, strSQL_Sum3 As String, strSQL_Sum4 As String
Dim Sum1 As Currency, Sum2 As Currency, Sum3 As Currency, Sum4 As Currency
Dim numCount As Long
Dim Flag_Assess As Byte, Flag_Old_New As Byte

Private Function Create_View() As Boolean
        Dim FlagStatus As Byte, i As Byte
        Dim strCondition As String
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        
        strSQL = ""
        Me.MousePointer = 11
        If chkFlag1.Value = Checked Then
                FlagStatus = 1
        Else
                FlagStatus = 0
        End If
        Select Case cbArticle.ListIndex
                Case 1
                        strCondition = " = 1 "
                        i = 1
                Case 2
                        strCondition = " = 0 "
                        i = 0
                Case 3
                        strCondition = " = 2 "
                        i = 2
        End Select
        
        strSQL = "ALTER VIEW [dbo].[VIEW_TAX_ALL] AS" & _
            " SELECT     TOP (100) PERCENT OWNERSHIP_ID, FULLNAME, OWNER_NAME, OWNER_SURNAME, OWNER_TYPE, CASE WHEN EXISTS (SELECT     OWNERSHIP_ID FROM (SELECT DISTINCT OWNERSHIP_ID FROM dbo.PBT5 AS PBT5_1" & _
            " WHERE (PBT5_STATUS = " & FlagStatus & ") AND (PBT5_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PBT5_YEAR = " & txtYear.Text & ")) AS B WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS LAND_FLAG," & _
            " CASE WHEN EXISTS (SELECT     OWNERSHIP_ID FROM          (SELECT DISTINCT OWNERSHIP_ID FROM          dbo.PRD2 AS PRD2_1" & _
            " WHERE      (PRD2_STATUS = " & FlagStatus & ") AND (PRD2_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PRD2_YEAR = " & txtYear.Text & ")) AS B WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS BUILDING_FLAG," & _
            " CASE WHEN EXISTS (SELECT     OWNERSHIP_ID FROM          (SELECT DISTINCT OWNERSHIP_ID FROM          dbo.PP1 AS PP1_1 WHERE      (PP1_STATUS = " & FlagStatus & ") AND (PP1_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PP1_YEAR = " & txtYear.Text & ")) AS B" & _
            " WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS SIGNBOARD_FLAG " & _
            " FROM         (SELECT DISTINCT dbo.PBT5.OWNERSHIP_ID, dbo.PBT5.PRENAME + dbo.PBT5.OWNER_NAME + '   ' + dbo.PBT5.OWNER_SURNAME AS FULLNAME," & _
            " dbo.PBT5.OWNER_NAME , dbo.PBT5.OWNER_SURNAME, dbo.PBT5.Owner_Type FROM          dbo.PBT5 LEFT OUTER JOIN dbo.LANDDATA ON dbo.PBT5.LAND_ID = dbo.LANDDATA.LAND_ID" & _
            " WHERE      (dbo.PBT5.PBT5_STATUS = " & FlagStatus & ") AND (dbo.PBT5.PBT5_SET_GK " & strCondition & " ) AND (dbo.PBT5.FLAG_NO_USED = 'FALSE') AND (dbo.PBT5.PBT5_YEAR = " & txtYear.Text & ")" & _
            " AND (NOT EXISTS (SELECT     LAND_ID FROM          dbo.PBT5 AS B WHERE      (LAND_ID = dbo.PBT5.LAND_ID) AND (PBT5_STATUS = 1) AND (PBT5_SET_GK <>" & i & ") AND (PBT5_YEAR = " & txtYear.Text & "))) Union" & _
            " SELECT DISTINCT dbo.PRD2.OWNERSHIP_ID, dbo.PRD2.PRENAME + dbo.PRD2.OWNER_NAME + '   ' + dbo.PRD2.OWNER_SURNAME AS FULLNAME,dbo.PRD2.OWNER_NAME , dbo.PRD2.OWNER_SURNAME, dbo.PRD2.Owner_Type" & _
            " FROM         dbo.PRD2 LEFT OUTER JOIN dbo.BUILDINGDATA ON dbo.PRD2.BUILDING_ID = dbo.BUILDINGDATA.BUILDING_ID" & _
            " WHERE     (dbo.PRD2.PRD2_STATUS = " & FlagStatus & ") AND (dbo.PRD2.PRD2_SET_GK " & strCondition & " ) AND (dbo.PRD2.FLAG_NO_USED = 'FALSE') AND (dbo.PRD2.PRD2_YEAR = " & txtYear.Text & ")" & _
            " AND (NOT EXISTS (SELECT     BUILDING_ID FROM          dbo.PRD2 AS B WHERE      (BUILDING_ID = dbo.PRD2.BUILDING_ID) AND (PRD2_STATUS = 1) AND (PRD2_SET_GK <>" & i & ") AND (PRD2_YEAR = " & txtYear.Text & "))) Union" & _
            " SELECT DISTINCT dbo.PP1.OWNERSHIP_ID, dbo.PP1.PRENAME + dbo.PP1.OWNER_NAME + '   ' + dbo.PP1.OWNER_SURNAME AS FULLNAME,dbo.PP1.OWNER_NAME , dbo.PP1.OWNER_SURNAME, dbo.PP1.Owner_Type" & _
            " FROM dbo.PP1 LEFT OUTER JOIN dbo.SIGNBORDDATA ON dbo.PP1.SIGNBORD_ID = dbo.SIGNBORDDATA.SIGNBORD_ID" & _
            " WHERE     (dbo.PP1.PP1_STATUS = " & FlagStatus & ") AND (dbo.PP1.PP1_SET_GK " & strCondition & " ) AND (dbo.PP1.FLAG_NO_USED = 'FALSE') AND (dbo.PP1.PP1_YEAR = " & txtYear.Text & ")" & _
            " AND (NOT EXISTS (SELECT     SIGNBORD_ID FROM          dbo.PP1 AS B WHERE      (SIGNBORD_ID = dbo.PP1.SIGNBORD_ID) AND (PP1_STATUS = 1) AND (PP1_SET_GK <>" & i & ") AND (PP1_YEAR = " & txtYear.Text & "))) ) AS A" & _
            " Where Exists (SELECT     OWNERSHIP_ID FROM          (SELECT DISTINCT OWNERSHIP_ID FROM          dbo.PBT5 AS PBT5_1 " & _
            " WHERE      (PBT5_STATUS = " & FlagStatus & ") AND (PBT5_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PBT5_YEAR = " & txtYear.Text & ")) AS B" & _
            " WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) OR Exists (SELECT     OWNERSHIP_ID FROM          (SELECT DISTINCT OWNERSHIP_ID FROM          dbo.PRD2 AS PRD2_1" & _
            " WHERE      (PRD2_STATUS = " & FlagStatus & ") AND (PRD2_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PRD2_YEAR = " & txtYear.Text & ")) AS C WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) OR" & _
            " Exists (SELECT     OWNERSHIP_ID FROM          (SELECT DISTINCT OWNERSHIP_ID FROM          dbo.PP1 AS PP1_1" & _
            " WHERE      (PP1_STATUS = " & FlagStatus & ") AND (PP1_SET_GK " & strCondition & " ) AND (FLAG_NO_USED = 'FALSE') AND (PP1_YEAR = " & txtYear.Text & ")) AS B_1 WHERE      (OWNERSHIP_ID = A.OWNERSHIP_ID)) ORDER BY OWNER_NAME"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View = True
        Exit Function
ERR_CREATE_VIEW:
                Globle_Connective.RollbackTrans
                MsgBox Err.Description
                Me.MousePointer = 0
                Create_View = False
        Exit Function
End Function

Private Sub ShowReport(File_Name As String, Formula As String, Optional BEGIN_DATE As String, Optional ENDDATE As String)
Dim i As Byte
Dim strCondition As String, strTemp As String
On Error GoTo ErrHlde
Call FindPrinterDeFault
With Clone_Form.CTReport
        .Reset
        .Connect = str_ConnectReport
        .DiscardSavedData = True
        .ReportFileName = App.Path & "\Report\" & File_Name
        If cbArticle.ListIndex > 0 And Cmb_StoryAll.ListIndex = 1 Then
                If chkFlag1.Value = Checked Then
                        i = 1
                        strTemp = "��¡�����շ�������������������"
                Else
                        i = 0
                        strTemp = "��¡�����շ���������ҧ��������"
                End If
                Select Case cbArticle.ListIndex
                        Case 1
                                strCondition = " = 1"
                                .Formulas(0) = "iniSection = '" & strTemp & " (�.�.1)" & "'"
                        Case 2
                                strCondition = " = 0"
                                .Formulas(0) = "iniSection = '" & strTemp & " (�������)" & "'"
                        Case 3
                                strCondition = " = 2"
                                .Formulas(0) = "iniSection = '" & strTemp & " (�������)" & "'"
                End Select
                .SubreportToChange = "PBT5"
                .SelectionFormula = "{PBT5.PBT5_YEAR} = " & txtYear.Text & " AND {PBT5.PBT5_STATUS}=" & i & " AND {PBT5.FLAG_NO_USED}=FALSE" & " AND {PBT5.PBT5_SET_GK}" & strCondition
                .Formulas(0) = "Flag_Status=" & i
                .SubreportToChange = "PRD2"
                .SelectionFormula = "{PRD2.PRD2_YEAR} = " & txtYear.Text & " AND {PRD2.PRD2_STATUS}=" & i & " AND {PRD2.FLAG_NO_USED}=FALSE" & " AND {PRD2.PRD2_SET_GK}" & strCondition
                .Formulas(0) = "Flag_Status=" & i
                .SubreportToChange = "PP1"
                .SelectionFormula = "{PP1.PP1_YEAR} = " & txtYear.Text & " AND {PP1.PP1_STATUS}=" & i & " AND {PP1.FLAG_NO_USED}=FALSE" & " AND {PP1.PP1_SET_GK}" & strCondition
                .Formulas(0) = "Flag_Status=" & i
                .SubreportToChange = ""
        End If
        .SelectionFormula = Formula
        .PrinterDriver = TruePrinter.DriverName
        .PrinterPort = TruePrinter.Port
        .PrinterName = TruePrinter.DeviceName
        .WindowShowPrintSetupBtn = True
        .Destination = crptToWindow
        .WindowState = crptMaximized
        .WindowShowExportBtn = True
        .Action = 1
End With
                
Exit Sub
ErrHlde:
        MsgBox Err.Description & " " & Err.Number
End Sub

Private Sub Condition_Assess() '��Ǵ��û����Թ���� ....
        Select Case Cmb_StoryAll.ListIndex
                Case 1
'                        strSQL = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND C.FLAG_PAYTAX= " & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL2 = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL3 = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND C.FLAG_PAYTAX= " & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL4 = "SELECT DISTINCT OWNERSHIP.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE 1=1 "
'                        strSQL_Sum1 = "SELECT SUM(LAND_SUMTAX),COUNT(B.LAND_ID)" & _
'                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum2 = "SELECT SUM(TOTAL_TAX_EXCLUDE),COUNT(B.BUILDING_ID)" & _
'                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum3 = "SELECT SUM(SIGNBORD_SUMTAX),COUNT(B.SIGNBORD_ID)" & _
'                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum4 = "SELECT SUM(LICENSE_SUMTAX),COUNT(LICENSEDATA.LICENSE_BOOK & LICENSEDATA.LICENSE_NO)" & _
'                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE 1=1 "
'                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL2 = strSQL2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL3 = strSQL3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL4 = strSQL4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                End If
'                        End If
'                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                strSQL = strSQL & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL2 = strSQL2 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL3 = strSQL3 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL4 = strSQL4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum2 = strSQL_Sum2 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum3 = strSQL_Sum3 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum4 = strSQL_Sum4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                        End If
'                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL2 = strSQL2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL3 = strSQL3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL4 = strSQL4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                        End If
'                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL2 = strSQL2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL3 = strSQL3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL4 = strSQL4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                Else
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                End If
'                                    End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                If cbStart_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                If cbEnd_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                        strSQL2 = strSQL2 & " ORDER BY OWNER_NAME"
'                        strSQL3 = strSQL3 & " ORDER BY OWNER_NAME"
'                        strSQL4 = strSQL4 & " ORDER BY OWNER_NAME"
'                        strSQL = strSQL & " UNION " & strSQL2 & " UNION " & strSQL3 & " UNION " & strSQL4
'                        Call SET_QUERY(strSQL, Rs)
'                        Debug.Print "exec sp_search_storypayment_all_asset '" & Flag_Assess & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & _
'                        Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'"
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_all_asset '" & Flag_Assess & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & _
                        Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Set Grid_OwnerShip.DataSource = rs
                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                Else
                                        Grid_OwnerShip.Rows = 2
                                        Grid_OwnerShip.Clear
                                End If
                        End If
'                        Call SET_QUERY(strSQL_Sum1, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','2','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        numCount = 0
                        Sum1 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                        lbSumPBT5.Caption = Format$(Sum1, "###,##0.00")
                        lbRecordCount_PBT5.Caption = IIf(IsNull(rs.Fields(0).Value), 0, FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue))
'                        Call SET_QUERY(strSQL_Sum2, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','3','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        Sum2 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                        lbSumPRD2.Caption = Format$(Sum2, "###,##0.00")
                        lbRecordCount_PRD2.Caption = IIf(IsNull(rs.Fields(0).Value), 0, FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue))
'                        Call SET_QUERY(strSQL_Sum3, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','4','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        Sum3 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                        lbSumPP1.Caption = Format$(Sum3, "###,##0.00")
                        lbRecordCount_PP1.Caption = IIf(IsNull(rs.Fields(0).Value), 0, FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue))
'                        Call SET_QUERY(strSQL_Sum4, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','5','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        Sum4 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                        lbSumPBA1.Caption = Format$(Sum4, "###,##0.00")
                        lbTotalMoneyAll.Caption = FormatNumber(Sum1 + Sum2 + Sum3 + Sum4, 2, vbTrue)
                        lbRecordCount_PBA1.Caption = IIf(IsNull(rs.Fields(0).Value), 0, FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue))
                        Call SetGrid_Ownership
                Case 2 '���Թ
'                        strSQL = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND C.FLAG_PAYTAX= " & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum1 = "SELECT SUM(LAND_SUMTAX),COUNT(B.LAND_ID)" & _
'                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                End If
'                        End If
'                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                strSQL = strSQL & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                        End If
'                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                        End If
'                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                Else
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                End If
'                                    End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                If cbStart_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                If cbEnd_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Set Grid_OwnerShip.DataSource = rs
                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                Else
                                        Grid_OwnerShip.Rows = 2
                                        Grid_OwnerShip.Clear
                                End If
                        End If
'                        Call SET_QUERY(strSQL_Sum1, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        lbRecordCount_PBT5.Caption = FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue)
                        lbSumPBT5.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        lbTotalMoneyAll.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        Call SetGrid_Ownership
                Case 3  '�ç���͹
'                        strSQL = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum1 = "SELECT SUM(TOTAL_TAX_EXCLUDE),COUNT(B.BUILDING_ID)" & _
'                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                End If
'                        End If
'                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                strSQL = strSQL & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                        End If
'                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                        End If
'                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                Else
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                End If
'                                    End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                If cbStart_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                If cbEnd_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Set Grid_OwnerShip.DataSource = rs
                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                Else
                                        Grid_OwnerShip.Rows = 2
                                        Grid_OwnerShip.Clear
                                End If
                        End If
'                        Call SET_QUERY(strSQL_Sum1, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        lbRecordCount_PRD2.Caption = FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue)
                        lbSumPRD2.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        lbTotalMoneyAll.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        Call SetGrid_Ownership
                Case 4 '����
'                        strSQL = "SELECT DISTINCT A.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND C.FLAG_PAYTAX= " & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        strSQL_Sum1 = "SELECT SUM(SIGNBORD_SUMTAX),COUNT(B.SIGNBORD_ID)" & _
'                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND C.FLAG_PAYTAX=" & Flag_Assess & " AND OWNERSHIP_NOTIC = 1"
'                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                End If
'                        End If
'                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                strSQL = strSQL & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND C.ZONE_BLOCK='" & cbZone.Text & "'"
'                        End If
'                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                        End If
'                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                Else
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                End If
'                                    End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                If cbStart_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                If cbEnd_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Set Grid_OwnerShip.DataSource = rs
                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                Else
                                        Grid_OwnerShip.Rows = 2
                                        Grid_OwnerShip.Clear
                                End If
                        End If
                        
'                        Call SET_QUERY(strSQL_Sum1, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        lbRecordCount_PP1.Caption = FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue)
                        lbSumPP1.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        lbTotalMoneyAll.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        Call SetGrid_Ownership
                Case 5 '�͹حҵ
'                        strSQL = "SELECT DISTINCT OWNERSHIP.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME" & _
'                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE 1=1"
'                        strSQL_Sum1 = "SELECT SUM(LICENSE_SUMTAX),COUNT(LICENSEDATA.LICENSE_BOOK & LICENSEDATA.LICENSE_NO)" & _
'                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE 1=1 "
'                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                End If
'                        End If
'                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                strSQL = strSQL & " AND (LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND (LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                        End If
'                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                        End If
'                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                Else
'                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        Else
'                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        End If
'                                                End If
'                                    End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                If cbStart_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                If cbEnd_Char = "�" Then
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                Else
'                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                End If
'                        End If
'                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Set Grid_OwnerShip.DataSource = rs
                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                Else
                                        Grid_OwnerShip.Rows = 2
                                        Grid_OwnerShip.Clear
                                End If
                        End If
'                        Call SET_QUERY(strSQL_Sum1, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum_asset '" & Flag_Assess & "','" & Cmb_StoryAll.ListIndex & "','" & Trim$(txtName.Text) & "','" & _
                                     Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & Trim$(cbZone.Text) & "','" & Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                        lbRecordCount_PBA1.Caption = FormatNumber(rs.Fields(1).Value, 0, vbFalse, vbUseDefault, vbTrue)
                        lbSumPBA1.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        lbTotalMoneyAll.Caption = IIf(IsNull(rs.Fields(0).Value), "0.00", Format$(rs.Fields(0).Value, "#,##0.00"))
                        Call SetGrid_Ownership
                End Select
End Sub

Private Sub cbArticle_Click()
        If cbArticle.ListIndex = 1 Or cbArticle.ListIndex = 2 Or cbArticle.ListIndex = 3 Then
                Label1(3).Caption = "��������"
                Label1(4).Caption = "��ҧ��������"
                UpDown1.Enabled = True
                chkAllYear.Enabled = True
        End If
        If cbArticle.ListIndex = 0 Then
                Label1(3).Caption = "�����Թ����"
                Label1(4).Caption = "�������Թ����"
                UpDown1.Enabled = False
                chkAllYear.Enabled = False
        End If
        If cbArticle.ListIndex = 0 Then
                cbZone.Enabled = True
        Else
                cbZone.Text = ""
                cbZone.Enabled = False
        End If
End Sub

Private Sub chkAllYear_Click()
        If chkAllYear.Value = Checked Then
                UpDown1.Enabled = False
                Label1(2).Caption = "�ء�� :"
                lbYear.Visible = False
        Else
                UpDown1.Enabled = True
                Label1(2).Caption = "��Шӻ� :"
                lbYear.Visible = True
        End If
End Sub

Private Sub chkFlag1_Click()
        If chkFlag1.Value = Checked Then
                Flag_Assess = 1
                chkFlag2.Value = Unchecked
                Label1(3).ForeColor = &HFF00&
                Label1(4).ForeColor = &HFFFFFF   '&HFF&
        Else
                chkFlag2.Value = Checked
                Flag_Assess = 0
        End If
End Sub

Private Sub chkFlag2_Click()
        If chkFlag2.Value = Checked Then
                chkFlag1.Value = Unchecked
                Label1(3).ForeColor = &HFFFFFF
                Label1(4).ForeColor = &HFF&
        Else
                chkFlag1.Value = Checked
        End If
End Sub

Private Sub Cmb_StoryAll_Click()
        If Cmb_StoryAll.ListIndex = 1 Then
                cbZone.Text = ""
                cbZone.Enabled = False
        ElseIf cbArticle.ListIndex = 0 Then
                cbZone.Enabled = True
        End If
End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
        Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()
        Set rs = New ADODB.Recordset
        txtYear.Text = Right$(Date, 4)
        Call SetGrid_Ownership
        Call SetGrid_PBT5(True)
        Call SetGrid_PP1(True)
        Call SetGrid_PRD2(True)
        Call SetGrid_PBA1(True)
           GBQueryZone.Filter = "ZONE_ID<> '' "
        If GBQueryZone.RecordCount > 0 Then
           cbZone.Clear
           GBQueryZone.MoveFirst
           Do While Not GBQueryZone.EOF
                  If GBQueryZone.Fields("ZONE_BLOCK").Value <> "Null" Then
                  cbZone.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
            Loop
        End If
        cbArticle.ListIndex = 0
        Flag_Assess = 1
End Sub

Private Sub SetGrid_Ownership()
        With Grid_OwnerShip
                .ColWidth(0) = 0
                .ColWidth(2) = 0
                .TextArray(1) = "��¡�ê���": .ColWidth(1) = 3050
                .ColAlignmentFixed(1) = 4
        End With
End Sub

Private Sub SetGrid_PBT5(Flag As Boolean)
        If Flag = True Then
                With Grid_PBT5
                        .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "���ʷ��Թ": .ColWidth(1) = 1100: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4
                        .TextArray(5) = "ʶҹм�����": .ColWidth(5) = 0: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�������": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�Ţ���": .ColWidth(7) = 1000: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "�ѹ����������": .ColWidth(8) = 1300: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "���": .ColWidth(9) = 800: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "�ҹ": .ColWidth(10) = 800: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "��": .ColWidth(11) = 800: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "�������": .ColWidth(12) = 1300: .ColAlignmentFixed(12) = 4
                End With
        Else
                With Grid_PBT5  '��� Flase ��û����Թ����
                        .TextArray(0) = "���ʷ��Թ": .ColWidth(0) = 1100: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "�����Ţ⫹": .ColWidth(1) = 1100: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�������": .ColWidth(2) = 1100: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "�������͡����Է���": .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�Ţ���Թ": .ColWidth(4) = 1000: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "�Ţ������ҧ": .ColWidth(5) = 1200: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "˹�����Ǩ": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4: .ColAlignment(6) = 4
                        .TextArray(7) = "�����Ţ�͡����Է���": .ColWidth(7) = 1550: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "���": .ColWidth(8) = 0: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "�ҹ": .ColWidth(9) = 0: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "��": .ColWidth(10) = 0: .ColAlignmentFixed(10) = 4
                        .ColWidth(11) = 0
                        .ColWidth(12) = 0
                End With
        End If
End Sub

Private Sub SetGrid_PP1(Flag As Boolean)
        If Flag = True Then
                With Grid_PP1
                        .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "���ʻ���": .ColWidth(1) = 1100: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "ʶҹм�����": .ColWidth(5) = 0: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�������": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�Ţ���": .ColWidth(7) = 1000: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "�ѹ����������": .ColWidth(8) = 1300: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "���ʷ��Թ": .ColWidth(9) = 1200: .ColAlignment(9) = 4
'                        .TextArray(10) = "���ջ������": .ColWidth(10) = 1500: .ColAlignmentFixed(10) = 4
                        .TextArray(10) = "��鹷�����": .ColWidth(10) = 1500: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "�������������1��ҹ": .ColWidth(11) = 1700: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "�������": .ColWidth(12) = 1300: .ColAlignmentFixed(12) = 4: .ColAlignment(12) = 4
                End With
        Else
                With Grid_PP1
                        .TextArray(0) = "���ʻ���": .ColWidth(0) = 1100: .ColAlignmentFixed(0) = 4: .ColAlignment(4) = 4
                        .TextArray(1) = "�����Ţ⫹": .ColWidth(1) = 1100: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�������": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "���ʷ��Թ": .ColWidth(3) = 1100: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "���ͻ���": .ColWidth(4) = 5000: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "�ӹǹ��ҹ": .ColWidth(5) = 1000: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�Ţ����駻����Թ": .ColWidth(6) = 0: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�ѹ����駻����Թ": .ColWidth(7) = 0: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "���ʷ��Թ": .ColWidth(8) = 0: .ColAlignment(8) = 4
                        .TextArray(9) = "���ջ������": .ColWidth(9) = 0: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "��鹷�����": .ColWidth(10) = 0: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "�������������1��ҹ": .ColWidth(11) = 0: .ColAlignmentFixed(11) = 4
                        .ColWidth(12) = 0
'                        .ColWidth(13) = 0
                End With
        End If
End Sub

Private Sub SetGrid_PRD2(Flag As Boolean)
        If Flag = True Then
                With Grid_PRD2
                        .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "�����ç���͹": .ColWidth(1) = 1200: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 6
                        .TextArray(5) = "ʶҹм�����": .ColWidth(5) = 0: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�������": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�Ţ���": .ColWidth(7) = 1000: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "�ѹ����������": .ColWidth(8) = 1300: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "���ʷ��Թ": .ColWidth(9) = 1100: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "�Ţ����ҹ": .ColWidth(10) = 800: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "�ѡɳ��Ҥ��": .ColWidth(11) = 1200: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "���ŷ����": .ColWidth(12) = 1200: .ColAlignmentFixed(12) = 4
                        .TextArray(13) = "��������»�": .ColWidth(13) = 1200: .ColAlignmentFixed(13) = 4
                        .TextArray(14) = "�ӹǹ��ͧ": .ColWidth(14) = 1200: .ColAlignmentFixed(14) = 4
                        .TextArray(15) = "�ӹǹ���": .ColWidth(15) = 1200: .ColAlignmentFixed(15) = 4
                        .TextArray(16) = "���ҧ ( ����)": .ColWidth(16) = 1200: .ColAlignmentFixed(16) = 4
                        .TextArray(17) = "��� ( ���� )": .ColWidth(17) = 1200: .ColAlignmentFixed(17) = 4
                        .TextArray(18) = "�������": .ColWidth(18) = 1300: .ColAlignmentFixed(18) = 4: .ColAlignment(18) = 6
                End With
        Else
                With Grid_PRD2
                        .TextArray(0) = "�����ç���͹": .ColWidth(0) = 1200: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "�����Ţ⫹": .ColWidth(1) = 1000: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�������": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "���ʷ��Թ": .ColWidth(3) = 1100: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�Ţ����ҹ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "�ѡɳ��ç���͹": .ColWidth(5) = 1300: .ColAlignmentFixed(5) = 4
                        .TextArray(6) = "�Ţ����駻����Թ": .ColWidth(6) = 0: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�ѹ����駻����Թ": .ColWidth(7) = 0: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "���ʷ��Թ": .ColWidth(8) = 0: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "�Ţ����ҹ": .ColWidth(9) = 0: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "��Ҿ": .ColWidth(10) = 0: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "���ŷ����": .ColWidth(11) = 0: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "��������»�": .ColWidth(12) = 0: .ColAlignmentFixed(12) = 4
                        .TextArray(13) = "�ӹǹ��ͧ": .ColWidth(13) = 0: .ColAlignmentFixed(13) = 4
                        .TextArray(14) = "�ӹǹ���": .ColWidth(14) = 0: .ColAlignmentFixed(14) = 4
                        .TextArray(15) = "���ҧ ( ����)": .ColWidth(15) = 0: .ColAlignmentFixed(15) = 4
                        .TextArray(16) = "��� ( ���� )": .ColWidth(16) = 0: .ColAlignmentFixed(16) = 4
                        .ColWidth(17) = 0
                        .ColWidth(18) = 0
                End With
        End If
End Sub

Private Sub SetGrid_PBA1(Flag As Boolean)
        If Flag = True Then
                With Grid_PBA1
                        .TextArray(0) = "��": .ColWidth(0) = 1000: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "�������": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�Ţ���": .ColWidth(2) = 800: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "�Ţ������Ẻ": .ColWidth(3) = 1000: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�ѹ������Ẻ": .ColWidth(4) = 1500: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "�ӹǹ�Թ": .ColWidth(5) = 1300: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�Ţ����駻����Թ": .ColWidth(6) = 1500: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�ѹ����駻����Թ": .ColWidth(7) = 1500: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "�����ç���͹": .ColWidth(8) = 1100: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "���ʷ��Թ": .ColWidth(9) = 1100: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "ʶҹ��͹حҵ": .ColWidth(10) = 1300: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "ʶҹз����": .ColWidth(11) = 1400: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "������": .ColWidth(12) = 1600: .ColAlignmentFixed(12) = 4
                        .TextArray(13) = "�ѡɳ�": .ColWidth(13) = 2400: .ColAlignmentFixed(13) = 4
                End With
        Else
                With Grid_PBA1
                        .TextArray(0) = "�������": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                        .TextArray(1) = "�Ţ���": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "��Ҹ�������": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4
                        .TextArray(3) = "����ʶҹ��Сͺ���": .ColWidth(3) = 4500: .ColAlignmentFixed(3) = 4
                        .TextArray(4) = "�ѹ����͹حҵ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                        .TextArray(5) = "ʶҹз����": .ColWidth(5) = 1300: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 4
                        .TextArray(6) = "�Ţ����駻����Թ": .ColWidth(6) = 0: .ColAlignmentFixed(6) = 4
                        .TextArray(7) = "�ѹ����駻����Թ": .ColWidth(7) = 0: .ColAlignmentFixed(7) = 4
                        .TextArray(8) = "�����ç���͹": .ColWidth(8) = 0: .ColAlignmentFixed(8) = 4
                        .TextArray(9) = "���ʷ��Թ": .ColWidth(9) = 0: .ColAlignmentFixed(9) = 4
                        .TextArray(10) = "ʶҹ��͹حҵ": .ColWidth(10) = 0: .ColAlignmentFixed(10) = 4
                        .TextArray(11) = "ʶҹз����": .ColWidth(11) = 0: .ColAlignmentFixed(11) = 4
                        .TextArray(12) = "������": .ColWidth(12) = 0: .ColAlignmentFixed(12) = 4
                        .TextArray(13) = "�ѡɳ�": .ColWidth(13) = 0: .ColAlignmentFixed(13) = 4
                End With
        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
      Set rs = Nothing
End Sub

Private Sub GRID_OWNERSHIP_Click()
        Dim strSQL As String
        Dim Amount As Currency, Amount2 As Currency
        
        On Error GoTo ErrGetQuery
        If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 1))) = 0 Then Exit Sub
        '----------------------- PBT5
        Amount = 0
        Amount2 = 0
        lbPrice_PBA1.Caption = "0.00"
        lbPRice_PBT5.Caption = "0.00"
        lbPrice_PP1.Caption = "0.00"
        lbPrice_PRD2.Caption = "0.00"
        lbRealPBT5.Caption = "0.00"
        lbRealPP1.Caption = "0.00"
        lbRealPRD2.Caption = "0.00"
        lbFullName.Caption = Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 1)
        If cbArticle.ListIndex = 0 Then '����ͧ����¹��Ѿ���Թ
                Select Case Cmb_StoryAll.ListIndex
                        Case 1 '������
                                If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                                        strSQL = "SELECT DISTINCT B.LAND_ID,ZONE_BLOCK,LAND_TYPE,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC,LAND_SUMTAX " & _
'                                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','2','" & _
                                                 Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PBT5.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PBT5
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("LAND_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("LAND_SUMTAX").Value
                                                                        Select Case rs.Fields("LAND_TYPE").Value
                                                                                Case 1
                                                                                        .TextMatrix(.Row, 3) = "⩹�"
                                                                                Case 2
                                                                                        .TextMatrix(.Row, 3) = "��. 3 �"
                                                                                Case 3
                                                                                        .TextMatrix(.Row, 3) = "��.3"
                                                                                Case 4
                                                                                        .TextMatrix(.Row, 3) = "ʤ 1"
                                                                                Case 5
                                                                                        .TextMatrix(.Row, 3) = "��Ҩͧ"
                                                                                Case 6
                                                                                        .TextMatrix(.Row, 3) = "ʻ�."
                                                                                Case 7
                                                                                        .TextMatrix(.Row, 3) = "��."
                                                                                Case 8
                                                                                        .TextMatrix(.Row, 3) = "���."
                                                                                Case 9
                                                                                        .TextMatrix(.Row, 3) = "��� �"
                                                                        End Select
                                                                        .TextMatrix(.Row, 4) = rs.Fields("LAND_NUMBER").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("LAND_RAWANG").Value
                                                                        .TextMatrix(.Row, 6) = rs.Fields("LAND_SURVEY").Value
                                                                        .TextMatrix(.Row, 7) = rs.Fields("LAND_NOTIC").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PBT5.Rows = Grid_PBT5.Rows - 1
                                                        Else
                                                                Grid_PBT5.Rows = 2
                                                                Grid_PBT5.Clear
                                                                Call SetGrid_PBT5(False)
                                                        End If
                                                        lbRecordCount(1).Caption = rs.RecordCount
                                                End If
                                                lbPRice_PBT5.Caption = Format$(Amount, "###,##0.00")
                                                Call SetGrid_PBT5(False)
                                                Amount = 0
                                '�ç���͹
'                                        strSQL = "SELECT DISTINCT B.BUILDING_ID,ZONE_BLOCK,LAND_ID,BUILDING_HOME_NO,TOTAL_TAX_EXCLUDE,BUILDING_TYPE " & _
'                                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','3','" & _
                                                 Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PRD2.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PRD2
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("BUILDING_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("TOTAL_TAX_EXCLUDE").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("TOTAL_TAX_EXCLUDE").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("BUILDING_HOME_NO").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("BUILDING_TYPE").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PRD2.Rows = Grid_PRD2.Rows - 1
                                                        Else
                                                                Grid_PRD2.Rows = 2
                                                                Grid_PRD2.Clear
                                                                Call SetGrid_PRD2(False)
                                                        End If
                                                        lbRecordCount(3).Caption = rs.RecordCount
                                                End If
                                                lbPrice_PRD2.Caption = Format$(Amount, "###,##0.00")
                                                Call SetGrid_PRD2(False)
                                                Amount = 0
                                        '����
'                                        strSQL = "SELECT DISTINCT B.SIGNBORD_ID,ZONE_BLOCK,LAND_ID,SIGNBORD_NAME,SIGNBORD_SUMTAX,SIGNBORD_ROUND " & _
'                                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','4','" & _
                                                Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PP1.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PP1
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("SIGNBORD_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("SIGNBORD_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("SIGNBORD_SUMTAX").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("SIGNBORD_NAME").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("SIGNBORD_ROUND").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PP1.Rows = Grid_PP1.Rows - 1
                                                Else
                                                        Grid_PP1.Rows = 2
                                                        Grid_PP1.Clear
                                                        Call SetGrid_PP1(False)
                                                End If
                                                lbRecordCount(2).Caption = rs.RecordCount
                                        End If
                                        lbPrice_PP1.Caption = Format$(Amount, "###,##0.00")
                                        Call SetGrid_PP1(False)
                                        Amount = 0
                                        '�͹حҵ
'                                        strSQL = "SELECT LICENSE_BOOK,LICENSE_NO,LICENSE_SUMTAX,LICENSE_DATE,LICENSE_NAME,STATUS_LINK " & _
'                                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE OWNERSHIP.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'"
'                                        Call SET_QUERY(strSQL, Rs)
'                                        Debug.Print "exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','5','" & _
'                                                Flag_Assess & "'"
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','5','" & _
                                                Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PBA1.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PBA1
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("LICENSE_BOOK").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("LICENSE_NO").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("LICENSE_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("LICENSE_SUMTAX").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LICENSE_NAME").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("LICENSE_DATE").Value
                                                                        Select Case rs.Fields("STATUS_LINK").Value
                                                                                Case 0
                                                                                        .TextMatrix(.Row, 5) = "����շ����"
                                                                                Case 1
                                                                                        .TextMatrix(.Row, 5) = "��駺��ŧ���Թ"
                                                                                Case 2
                                                                                        .TextMatrix(.Row, 5) = "��駺��ç���͹"
                                                                        End Select
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PBA1.Rows = Grid_PBA1.Rows - 1
                                                Else
                                                        Grid_PBA1.Rows = 2
                                                        Grid_PBA1.Clear
                                                        Call SetGrid_PBA1(False)
                                                End If
                                                lbRecordCount(4).Caption = rs.RecordCount
                                        End If
                                        lbPrice_PBA1.Caption = Format$(Amount, "###,##0.00")
                                        Call SetGrid_PBA1(False)
                                End If
                        Case 2 '���Թ
                                If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                                        strSQL = "SELECT B.LAND_ID,ZONE_BLOCK,LAND_TYPE,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC,LAND_SUMTAX " & _
'                                                    " FROM OWNERSHIP A,LANDDATA_NOTIC B,LANDDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.LAND_ID=C.LAND_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & _
                                                Cmb_StoryAll.ListIndex & "','" & Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PBT5.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PBT5
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("LAND_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("LAND_SUMTAX").Value
                                                                        Select Case rs.Fields("LAND_TYPE").Value
                                                                                Case 1
                                                                                        .TextMatrix(.Row, 3) = "⩹�"
                                                                                Case 2
                                                                                        .TextMatrix(.Row, 3) = "��. 3 �"
                                                                                Case 3
                                                                                        .TextMatrix(.Row, 3) = "��.3"
                                                                                Case 4
                                                                                        .TextMatrix(.Row, 3) = "ʤ 1"
                                                                                Case 5
                                                                                        .TextMatrix(.Row, 3) = "��Ҩͧ"
                                                                                Case 6
                                                                                        .TextMatrix(.Row, 3) = "ʻ�."
                                                                                Case 7
                                                                                        .TextMatrix(.Row, 3) = "��."
                                                                                Case 8
                                                                                        .TextMatrix(.Row, 3) = "���."
                                                                                Case 9
                                                                                        .TextMatrix(.Row, 3) = "��� �"
                                                                        End Select
                                                                        .TextMatrix(.Row, 4) = rs.Fields("LAND_NUMBER").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("LAND_RAWANG").Value
                                                                        .TextMatrix(.Row, 6) = rs.Fields("LAND_SURVEY").Value
                                                                        .TextMatrix(.Row, 7) = rs.Fields("LAND_NOTIC").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PBT5.Rows = Grid_PBT5.Rows - 1
                                                Else
                                                        Grid_PBT5.Rows = 2
                                                        Grid_PBT5.Clear
                                                        Call SetGrid_PBT5(False)
                                                End If
                                                lbRecordCount(1).Caption = rs.RecordCount
                                        End If
                                        lbPRice_PBT5.Caption = Format$(Amount, "###,##0.00")
                                End If
                                Call SetGrid_PBT5(False)
                        Case 3      '�ç���͹
                                If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                                        strSQL = "SELECT B.BUILDING_ID,ZONE_BLOCK,LAND_ID,BUILDING_HOME_NO,TOTAL_TAX_EXCLUDE,BUILDING_TYPE " & _
'                                                    " FROM OWNERSHIP A,BUILDINGDATA_NOTIC B,BUILDINGDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.BUILDING_ID=C.BUILDING_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & _
                                                Cmb_StoryAll.ListIndex & "','" & Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PRD2.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PRD2
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("BUILDING_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("TOTAL_TAX_EXCLUDE").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("TOTAL_TAX_EXCLUDE").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("BUILDING_HOME_NO").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("BUILDING_TYPE").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PRD2.Rows = Grid_PRD2.Rows - 1
                                                Else
                                                        Grid_PRD2.Rows = 2
                                                        Grid_PRD2.Clear
                                                        Call SetGrid_PRD2(False)
                                                End If
                                                lbRecordCount(3).Caption = rs.RecordCount
                                        End If
                                        lbPrice_PRD2.Caption = Format$(Amount, "###,##0.00")
                                End If
                                Call SetGrid_PRD2(False)
                        Case 4 '����
                                If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                                        strSQL = "SELECT B.SIGNBORD_ID,ZONE_BLOCK,LAND_ID,SIGNBORD_NAME,SIGNBORD_SUMTAX,SIGNBORD_ROUND " & _
'                                                    " FROM OWNERSHIP A,SIGNBORDDATA_NOTIC B,SIGNBORDDATA C " & _
'                                                    " WHERE (B.OWNERSHIP_ID=A.OWNERSHIP_ID) AND (B.SIGNBORD_ID=C.SIGNBORD_ID) AND A.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND FLAG_PAYTAX=" & Flag_Assess
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & _
                                                Cmb_StoryAll.ListIndex & "','" & Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PP1.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PP1
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("SIGNBORD_ID").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("ZONE_BLOCK").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("SIGNBORD_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("SIGNBORD_SUMTAX").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LAND_ID").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("SIGNBORD_NAME").Value
                                                                        .TextMatrix(.Row, 5) = rs.Fields("SIGNBORD_ROUND").Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PP1.Rows = Grid_PP1.Rows - 1
                                                Else
                                                        Grid_PP1.Rows = 2
                                                        Grid_PP1.Clear
                                                        Call SetGrid_PP1(False)
                                                End If
                                                lbRecordCount(2).Caption = rs.RecordCount
                                        End If
                                        lbPrice_PP1.Caption = Format$(Amount, "###,##0.00")
                                End If
                                Call SetGrid_PP1(False)
                        Case 5 ' �͹حҵ
                                If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                                        strSQL = "SELECT LICENSE_BOOK,LICENSE_NO,LICENSE_SUMTAX,LICENSE_DATE,LICENSE_NAME,STATUS_LINK " & _
'                                                    " FROM ((OWNERSHIP INNER JOIN LICENSEDATA ON OWNERSHIP.OWNERSHIP_ID = LICENSEDATA.OWNERSHIP_ID) " & _
'                                                    " LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                                    " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID WHERE OWNERSHIP.OWNERSHIP_ID='" & _
'                                                    Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'"
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership2 '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & _
                                                Cmb_StoryAll.ListIndex & "','" & Flag_Assess & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Grid_PBA1.Rows = 2
                                                        rs.MoveFirst
                                                        Do While Not rs.EOF
                                                                With Grid_PBA1
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields("LICENSE_BOOK").Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields("LICENSE_NO").Value
                                                                        .TextMatrix(.Row, 2) = Format$(rs.Fields("LICENSE_SUMTAX").Value, "#,##0.00")
                                                                        Amount = Amount + rs.Fields("LICENSE_SUMTAX").Value
                                                                        .TextMatrix(.Row, 3) = rs.Fields("LICENSE_NAME").Value
                                                                        .TextMatrix(.Row, 4) = rs.Fields("LICENSE_DATE").Value
                                                                        Select Case rs.Fields("STATUS_LINK").Value
                                                                                Case 0
                                                                                        .TextMatrix(.Row, 5) = "����շ����"
                                                                                Case 1
                                                                                        .TextMatrix(.Row, 5) = "��駺��ŧ���Թ"
                                                                                Case 2
                                                                                        .TextMatrix(.Row, 5) = "��駺��ç���͹"
                                                                        End Select
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_PBA1.Rows = Grid_PBA1.Rows - 1
                                                Else
                                                        Grid_PBA1.Rows = 2
                                                        Grid_PBA1.Clear
                                                        Call SetGrid_PBA1(False)
                                                End If
                                                lbRecordCount(4).Caption = rs.RecordCount
                                        End If
                                        lbPrice_PBA1.Caption = Format$(Amount, "###,##0.00")
                                End If
                                Call SetGrid_PBA1(False)
                End Select
        Else '����ͧ��ê�������
                Select Case cbArticle.ListIndex
                        Case 1
                                Flag_Old_New = 1
                        Case 2
                                Flag_Old_New = 0
                        Case 3
                                Flag_Old_New = 2
                End Select
                Select Case Cmb_StoryAll.ListIndex
                            Case 1
                        If LenB(Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0))) > 0 Then
'                        strSQL = "SELECT DISTINCT PBT5.LAND_ID,PBT5_NO,PBT5_DATE,PBT5_AMOUNT_ACCEPT,PBT5_PAY,PBT5_BOOK_NUMBER,PBT5_BOOK_NO," & _
'                                                " PBT5_PAY_DATE,LAND_A_RAI,LAND_A_POT,LAND_A_VA,PBT5_YEAR,SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS SUM_TAX" & _
'                                                " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID WHERE PBT5.OWNERSHIP_ID='" & _
'                                                Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PBT5_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PBT5_YEAR", " AND PBT5_YEAR=" & NumYear) & _
'                                                " GROUP BY PBT5.LAND_ID,PBT5_NO,PBT5_DATE,PBT5_AMOUNT_ACCEPT,PBT5_PAY,PBT5_BOOK_NUMBER,PBT5_BOOK_NO,PBT5_PAY_DATE,LAND_A_RAI,LAND_A_POT,LAND_A_VA,PBT5_YEAR"
'                                Call SET_QUERY(strSQL, Rs)
                                Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','2','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                                If rs.STATE = adStateOpen Then
                                        If rs.RecordCount > 0 Then
                                                Grid_PBT5.Rows = 2
                                                rs.MoveFirst
                                                Do While Not rs.EOF
                                                        With Grid_PBT5
                                                                .Row = .Rows - 1
                                                                .TextMatrix(.Row, 0) = rs.Fields("PBT5_YEAR").Value
                                                                .TextMatrix(.Row, 1) = rs.Fields("LAND_ID").Value
                                                                .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PBT5_NO").Value), "", rs.Fields("PBT5_NO").Value)
                                                                .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PBT5_DATE").Value), "", rs.Fields("PBT5_DATE").Value)
                                                                .TextMatrix(.Row, 4) = Format$(rs.Fields("PBT5_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                                Amount = Amount + rs.Fields("PBT5_AMOUNT_ACCEPT").Value
                                                                If rs.Fields("PBT5_PAY").Value = 0 Then
                                                                        .TextMatrix(.Row, 5) = "������繼�����"
                                                                Else
                                                                        .TextMatrix(.Row, 5) = "�繼�����"
                                                                End If
                                                                .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PBT5_BOOK_NUMBER").Value), "", rs.Fields("PBT5_BOOK_NUMBER").Value)
                                                                .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PBT5_BOOK_NO").Value), "", rs.Fields("PBT5_BOOK_NO").Value)
                                                                .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PBT5_PAY_DATE").Value), "", rs.Fields("PBT5_PAY_DATE").Value)
                                                                .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_A_RAI").Value), "", rs.Fields("LAND_A_RAI").Value)
                                                                .TextMatrix(.Row, 10) = IIf(IsNull(rs.Fields("LAND_A_POT").Value), "", rs.Fields("LAND_A_POT").Value)
                                                                .TextMatrix(.Row, 11) = Format$(rs.Fields("LAND_A_VA").Value, "###,##0.00")
                                                                .TextMatrix(.Row, 12) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                                Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                                .Rows = .Rows + 1
                                                        End With
                                                        rs.MoveNext
                                                Loop
                                                Grid_PBT5.Rows = Grid_PBT5.Rows - 1
                                                Call SetGrid_PBT5(True)
                                        Else
                                                Grid_PBT5.Rows = 2
                                                Grid_PBT5.Clear
                                                Call SetGrid_PBT5(True)
                                        End If
                                        lbRecordCount(1).Caption = rs.RecordCount
                                End If
                                lbPRice_PBT5.Caption = Format$(Amount, "###,##0.00")
                                lbRealPBT5.Caption = Format$(Amount2, "###,##0.00")
                '-------------------------- PP1
                        Amount = 0
                        Amount2 = 0
'                        strSQL = "SELECT DISTINCT PP1.SIGNBORD_ID,PP1_NO,PP1_DATE,PP1_AMOUNT,PP1_YEAR,PP1_PAY,PP1_BOOK_NUMBER,PP1_BOOK_NO," & _
'                                                    " PP1_PAY_DATE,BUILDING_ID,LAND_ID,SIGNBORD_SUMTAX,SIGNBORD_TOTAL,SIGNBORD_AMOUNT,SUM(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS SUM_TAX " & _
'                                                    " FROM (PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID = SIGNBORDDATA.SIGNBORD_ID) LEFT JOIN SIGNBORDDATA_DETAILS ON SIGNBORDDATA.SIGNBORD_ID = SIGNBORDDATA_DETAILS.SIGNBORD_ID" & _
'                                                    " WHERE PP1.OWNERSHIP_ID='" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PP1_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PP1_YEAR", " AND PP1_YEAR=" & NumYear) & _
'                                                    " GROUP BY PP1.SIGNBORD_ID,PP1_NO,PP1_DATE,PP1_AMOUNT,PP1_YEAR,PP1_PAY,PP1_BOOK_NUMBER,PP1_BOOK_NO,PP1_PAY_DATE,BUILDING_ID,LAND_ID,SIGNBORD_SUMTAX,SIGNBORD_TOTAL,SIGNBORD_AMOUNT"
'                        Call SET_QUERY(strSQL, Rs)
'                        Debug.Print "exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','4','" & _
'                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'"
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','4','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Grid_PP1.Rows = 2
                                        rs.MoveFirst
                                        Do While Not rs.EOF
                                                With Grid_PP1
                                                        .Row = .Rows - 1
                                                                    .TextMatrix(.Row, 0) = rs.Fields("PP1_YEAR").Value
                                                                    .TextMatrix(.Row, 1) = rs.Fields("SIGNBORD_ID").Value
                                                                    .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PP1_NO").Value), "", rs.Fields("PP1_NO").Value)
                                                                    .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PP1_DATE").Value), "", rs.Fields("PP1_DATE").Value)
                                                                    .TextMatrix(.Row, 4) = Format$(rs.Fields("PP1_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                                    Amount = Amount + rs.Fields("PP1_AMOUNT_ACCEPT").Value
                                                                    If rs.Fields("PP1_PAY").Value = 0 Then
                                                                            .TextMatrix(.Row, 5) = "������繼�����"
                                                                    Else
                                                                            .TextMatrix(.Row, 5) = "�繼�����"
                                                                    End If
                                                                    .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PP1_BOOK_NUMBER").Value), "", rs.Fields("PP1_BOOK_NUMBER").Value)
                                                                    .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PP1_BOOK_NO").Value), "", rs.Fields("PP1_BOOK_NO").Value)
                                                                    .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PP1_PAY_DATE").Value), "", rs.Fields("PP1_PAY_DATE").Value)
                                                                    .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_ID").Value), "��辺", rs.Fields("LAND_ID").Value)
'                                                                    .TextMatrix(.Row, 10) = Format$(Rs.Fields("SIGNBORD_SUMTAX").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 10) = Format$(rs.Fields("SIGNBORD_TOTAL").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 11) = Format$(rs.Fields("SIGNBORD_AMOUNT").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 12) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                                    Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                                    .Rows = .Rows + 1
                                                End With
                                                rs.MoveNext
                                        Loop
                                        Grid_PP1.Rows = Grid_PP1.Rows - 1
                                        Call SetGrid_PP1(True)
                                Else
                                        Grid_PP1.Rows = 2
                                        Grid_PP1.Clear
                                        Call SetGrid_PP1(True)
                                End If
                        End If
                        lbRecordCount(2).Caption = rs.RecordCount
                        lbRealPP1.Caption = Format$(Amount2, "###,##0.00")
                End If
                lbPrice_PP1.Caption = Format$(Amount, "###,##0.00")
                '---------------------------- PRD2
                Amount = 0
                Amount2 = 0
'               strSQL = "SELECT DISTINCT PRD2.BUILDING_ID,PRD2_NO,PRD2_DATE,RENTYEAR_TOTAL_ALL,PRD2_YEAR,PRD2_PAY," & _
'                                " PRD2_BOOK_NUMBER,PRD2_BOOK_NO,PRD2_PAY_DATE,LAND_ID,BUILDING_HOME_NO,BUILDING_STATE_PRICE, " & _
'                                " PRD2_RENTYEAR_TOTAL,BUILDING_STATUS,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT," & _
'                                " ZONETAX_ID,BUILDING_TYPE,SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS SUM_TAX" & _
'                                " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID" & _
'                                " WHERE PRD2.OWNERSHIP_ID='" & _
'                                Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PRD2_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PRD2_YEAR", " AND PRD2_YEAR=" & NumYear) & _
'                                " GROUP BY PRD2.BUILDING_ID,PRD2_NO,PRD2_DATE,RENTYEAR_TOTAL_ALL,PRD2_YEAR,PRD2_PAY,PRD2_BOOK_NUMBER,PRD2_BOOK_NO,PRD2_PAY_DATE,LAND_ID,BUILDING_HOME_NO,BUILDING_STATE_PRICE,PRD2_RENTYEAR_TOTAL,BUILDING_STATUS,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT,ZONETAX_ID,BUILDING_TYPE"
'                                Call SET_QUERY(strSQL, Rs)
                            Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','3','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                            If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Grid_PRD2.Rows = 2
                                        rs.MoveFirst
                                        Do While Not rs.EOF
                                                With Grid_PRD2
                                                        .Row = .Rows - 1
                                                        .TextMatrix(.Row, 0) = rs.Fields("PRD2_YEAR").Value
                                                        .TextMatrix(.Row, 1) = rs.Fields("BUILDING_ID").Value
                                                        .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PRD2_NO").Value), "", rs.Fields("PRD2_NO").Value)
                                                        .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PRD2_DATE").Value), "", rs.Fields("PRD2_DATE").Value)
                                                        .TextMatrix(.Row, 4) = Format$(rs.Fields("PRD2_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                        Amount = Amount + rs.Fields("PRD2_AMOUNT_ACCEPT").Value
                                                        If rs.Fields("PRD2_PAY").Value = 0 Then
                                                                .TextMatrix(.Row, 5) = "������繼�����"
                                                        Else
                                                                .TextMatrix(.Row, 5) = "�繼�����"
                                                        End If
                                                        .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PRD2_BOOK_NUMBER").Value), "", rs.Fields("PRD2_BOOK_NUMBER").Value)
                                                        .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PRD2_BOOK_NO").Value), "", rs.Fields("PRD2_BOOK_NO").Value)
                                                        .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PRD2_PAY_DATE").Value), "", rs.Fields("PRD2_PAY_DATE").Value)
                                                        .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_ID").Value), "", rs.Fields("LAND_ID").Value)
                                                        .TextMatrix(.Row, 10) = IIf(IsNull(rs.Fields("BUILDING_HOME_NO").Value), "", rs.Fields("BUILDING_HOME_NO").Value)
                                                        .TextMatrix(.Row, 11) = IIf(IsNull(rs.Fields("BUILDING_TYPE").Value), "", rs.Fields("BUILDING_TYPE").Value)
                                                        .TextMatrix(.Row, 12) = IIf(IsNull(rs.Fields("ZONETAX_ID").Value), "", rs.Fields("ZONETAX_ID").Value)
                                                        .TextMatrix(.Row, 13) = Format$(rs.Fields("RENTYEAR_TOTAL_ALL").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 14) = IIf(IsNull(rs.Fields("BUILDING_ROOM").Value), "", rs.Fields("BUILDING_ROOM").Value)
                                                        .TextMatrix(.Row, 15) = IIf(IsNull(rs.Fields("BUILDING_FLOOR").Value), "", rs.Fields("BUILDING_FLOOR").Value)
                                                        .TextMatrix(.Row, 16) = Format$(rs.Fields("BUILDING_WIDTH").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 17) = Format$(rs.Fields("BUILDING_HEIGHT").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 18) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                        Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                        .Rows = .Rows + 1
                                                End With
                                                rs.MoveNext
                                        Loop
                                        Grid_PRD2.Rows = Grid_PRD2.Rows - 1
                                        Call SetGrid_PRD2(True)
                                Else
                                        Grid_PRD2.Rows = 2
                                        Grid_PRD2.Clear
                                        Call SetGrid_PRD2(True)
                                End If
                                lbRecordCount(3).Caption = rs.RecordCount
                        End If
                        lbPrice_PRD2.Caption = Format$(Amount, "###,##0.00")
                        lbRealPRD2.Caption = Format$(Amount2, "###,##0.00")
                        '----------------------------- PBA1
                        Amount = 0
                        Amount2 = 0
'                        strSQL = "SELECT PBA1.LICENSE_BOOK,PBA1.LICENSE_NO,BUILDING_ID,LAND_ID,PBA1_DATE,PBA1_AMOUNT,PBA1_YEAR,PBA1_NO_ACCEPT," & _
'                                " PBA1_DATE_ACCEPT,LICENSE_SUMTAX,STATUS_LINK,FLAG_STATUS,CLASS_DETAILS,BUSINESS_TYPE,PBA1_NO " & _
'                                " FROM PBA1,LICENSEDATA,CLASS_DETAILS,BUSINESS_TYPE " & _
'                                " WHERE (PBA1.LICENSE_BOOK=LICENSEDATA.LICENSE_BOOK) AND (PBA1.LICENSE_NO=LICENSEDATA.LICENSE_NO) AND " & _
'                                " (LICENSEDATA.CLASS_ID=CLASS_DETAILS.CLASS_ID) AND (BUSINESS_TYPE.BUSINESS_ID=CLASS_DETAILS.BUSINESS_ID) AND " & _
'                                " PBA1.OWNERSHIP_ID='" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PBA1_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PBA1_YEAR", " AND PBA1_YEAR=" & NumYear)
'
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','5','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Grid_PBA1.Rows = 2
                                        rs.MoveFirst
                                        Do While Not rs.EOF
                                                With Grid_PBA1
                                                        .Row = .Rows - 1
                                                        .TextMatrix(.Row, 0) = rs.Fields("PBA1_YEAR").Value
                                                        .TextMatrix(.Row, 1) = rs.Fields("LICENSE_BOOK").Value
                                                        .TextMatrix(.Row, 2) = rs.Fields("LICENSE_NO").Value
                                                        .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PBA1_NO").Value), "", rs.Fields("PBA1_NO").Value)
                                                        .TextMatrix(.Row, 4) = IIf(IsNull(rs.Fields("PBA1_DATE").Value), "", rs.Fields("PBA1_DATE").Value)
                                                        .TextMatrix(.Row, 5) = Format$(rs.Fields("PBA1_AMOUNT").Value, "###,##0.00")
                                                        Amount = Amount + rs.Fields("PBA1_AMOUNT").Value
                                                        .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PBA1_NO_ACCEPT").Value), "", rs.Fields("PBA1_NO_ACCEPT").Value)
                                                        .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PBA1_DATE_ACCEPT").Value), "", rs.Fields("PBA1_DATE_ACCEPT").Value)
                                                        .TextMatrix(.Row, 8) = rs.Fields("BUILDING_ID").Value
                                                        .TextMatrix(.Row, 9) = rs.Fields("LAND_ID").Value
                                                        .TextMatrix(.Row, 10) = IIf(rs.Fields("FLAG_STATUS").Value = 0, "������͹حҵ", "���͹حҵ")
                                                        Select Case rs.Fields("STATUS_LINK").Value
                                                                Case 0
                                                                        .TextMatrix(.Row, 11) = "����շ����"
                                                                Case 1
                                                                        .TextMatrix(.Row, 11) = "��駺��ŧ���Թ"
                                                                Case 2
                                                                        .TextMatrix(.Row, 11) = "��駺��ç���͹"
                                                        End Select
                                                        .TextMatrix(.Row, 12) = rs.Fields("CLASS_DETAILS").Value
                                                        .TextMatrix(.Row, 13) = rs.Fields("BUSINESS_TYPE").Value
                                                        .Rows = .Rows + 1
                                                End With
                                                rs.MoveNext
                                        Loop
                                        Grid_PBA1.Rows = Grid_PBA1.Rows - 1
                                        Call SetGrid_PBA1(True)
                                Else
                                        Grid_PBA1.Rows = 2
                                        Grid_PBA1.Clear
                                        Call SetGrid_PBA1(True)
                                End If
                                lbRecordCount(4).Caption = rs.RecordCount
                        End If
                        lbPrice_PBA1.Caption = Format$(Amount, "###,##0.00")
                        Case 2 '���Թ
'                                        strSQL = "SELECT DISTINCT PBT5.LAND_ID,PBT5_NO,PBT5_DATE,PBT5_AMOUNT_ACCEPT,PBT5_PAY,PBT5_BOOK_NUMBER,PBT5_BOOK_NO," & _
'                                                " PBT5_PAY_DATE,LAND_A_RAI,LAND_A_POT,LAND_A_VA,PBT5_YEAR,SUM(PBT5_AMOUNT_REAL+PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS SUM_TAX" & _
'                                                " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID WHERE PBT5.OWNERSHIP_ID='" & _
'                                                Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PBT5_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, "", " AND PBT5_YEAR=" & NumYear) & _
'                                                " GROUP BY PBT5.LAND_ID,PBT5_NO,PBT5_DATE,PBT5_AMOUNT_ACCEPT,PBT5_PAY,PBT5_BOOK_NUMBER,PBT5_BOOK_NO,PBT5_PAY_DATE,LAND_A_RAI,LAND_A_POT,LAND_A_VA,PBT5_YEAR"
'                                Call SET_QUERY(strSQL, Rs)
'                                Debug.Print "exec sp_search_storypayment_ownership '" & NumYear & "','" & GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'"
                                Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                                If rs.STATE = adStateOpen Then
                                        If rs.RecordCount > 0 Then
                                                Grid_PBT5.Rows = 2
                                                rs.MoveFirst
                                                Do While Not rs.EOF
                                                        With Grid_PBT5
                                                                .Row = .Rows - 1
                                                                .TextMatrix(.Row, 0) = rs.Fields("PBT5_YEAR").Value
                                                                .TextMatrix(.Row, 1) = rs.Fields("LAND_ID").Value
                                                                .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PBT5_NO").Value), "", rs.Fields("PBT5_NO").Value)
                                                                .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PBT5_DATE").Value), "", rs.Fields("PBT5_DATE").Value)
                                                                .TextMatrix(.Row, 4) = Format$(rs.Fields("PBT5_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                                Amount = Amount + rs.Fields("PBT5_AMOUNT_ACCEPT").Value
                                                                If rs.Fields("PBT5_PAY").Value = 0 Then
                                                                        .TextMatrix(.Row, 5) = "������繼�����"
                                                                Else
                                                                        .TextMatrix(.Row, 5) = "�繼�����"
                                                                End If
                                                                .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PBT5_BOOK_NUMBER").Value), "", rs.Fields("PBT5_BOOK_NUMBER").Value)
                                                                .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PBT5_BOOK_NO").Value), "", rs.Fields("PBT5_BOOK_NO").Value)
                                                                .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PBT5_PAY_DATE").Value), "", rs.Fields("PBT5_PAY_DATE").Value)
                                                                .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_A_RAI").Value), "", rs.Fields("LAND_A_RAI").Value)
                                                                .TextMatrix(.Row, 10) = IIf(IsNull(rs.Fields("LAND_A_POT").Value), "", rs.Fields("LAND_A_POT").Value)
                                                                .TextMatrix(.Row, 11) = Format$(rs.Fields("LAND_A_VA").Value, "###,##0.00")
                                                                .TextMatrix(.Row, 12) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                                Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                                .Rows = .Rows + 1
                                                        End With
                                                        rs.MoveNext
                                                Loop
                                                Grid_PBT5.Rows = Grid_PBT5.Rows - 1
                                                Call SetGrid_PBT5(True)
                                        Else
                                                Grid_PBT5.Rows = 2
                                                Grid_PBT5.Clear
                                                Call SetGrid_PBT5(True)
                                        End If
                                        lbRecordCount(1).Caption = rs.RecordCount
                                End If
                                lbPRice_PBT5.Caption = Format$(Amount, "###,##0.00")
                                lbRealPBT5.Caption = Format$(Amount2, "###,##0.00")
                        Case 3 '�ç���͹
'                strSQL = "SELECT DISTINCT PRD2.BUILDING_ID,PRD2_NO,PRD2_DATE,RENTYEAR_TOTAL_ALL,PRD2_YEAR,PRD2_PAY," & _
'                                " PRD2_BOOK_NUMBER,PRD2_BOOK_NO,PRD2_PAY_DATE,LAND_ID,BUILDING_HOME_NO,BUILDING_STATE_PRICE, " & _
'                                " PRD2_AMOUNT_ACCEPT,BUILDING_STATUS,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT," & _
'                                " ZONETAX_ID,BUILDING_TYPE,SUM(PRD2_AMOUNT_REAL+PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS SUM_TAX" & _
'                                " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID" & _
'                                " WHERE PRD2.OWNERSHIP_ID='" & _
'                                Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PRD2_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PRD2_YEAR", " AND PRD2_YEAR=" & NumYear) & _
'                                " GROUP BY PRD2.BUILDING_ID,PRD2_NO,PRD2_DATE,RENTYEAR_TOTAL_ALL,PRD2_YEAR,PRD2_PAY,PRD2_BOOK_NUMBER,PRD2_BOOK_NO,PRD2_PAY_DATE,LAND_ID,BUILDING_HOME_NO,BUILDING_STATE_PRICE,PRD2_AMOUNT_ACCEPT,BUILDING_STATUS,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_WIDTH,BUILDING_HEIGHT,ZONETAX_ID,BUILDING_TYPE"
'                Call SET_QUERY(strSQL, Rs)
'                        Debug.Print "exec sp_search_storypayment_ownership '" & NumYear & "','" & GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
'                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'"
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Grid_PRD2.Rows = 2
                                        rs.MoveFirst
                                        Do While Not rs.EOF
                                                With Grid_PRD2
                                                        .Row = .Rows - 1
                                                        .TextMatrix(.Row, 0) = rs.Fields("PRD2_YEAR").Value
                                                        .TextMatrix(.Row, 1) = rs.Fields("BUILDING_ID").Value
                                                        .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PRD2_NO").Value), "", rs.Fields("PRD2_NO").Value)
                                                        .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PRD2_DATE").Value), "", rs.Fields("PRD2_DATE").Value)
                                                        .TextMatrix(.Row, 4) = Format$(rs.Fields("PRD2_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                        Amount = Amount + rs.Fields("PRD2_AMOUNT_ACCEPT").Value
                                                        If rs.Fields("PRD2_PAY").Value = 0 Then
                                                                .TextMatrix(.Row, 5) = "������繼�����"
                                                        Else
                                                                .TextMatrix(.Row, 5) = "�繼�����"
                                                        End If
                                                        .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PRD2_BOOK_NUMBER").Value), "", rs.Fields("PRD2_BOOK_NUMBER").Value)
                                                        .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PRD2_BOOK_NO").Value), "", rs.Fields("PRD2_BOOK_NO").Value)
                                                        .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PRD2_PAY_DATE").Value), "", rs.Fields("PRD2_PAY_DATE").Value)
                                                        .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_ID").Value), "", rs.Fields("LAND_ID").Value)
                                                        .TextMatrix(.Row, 10) = IIf(IsNull(rs.Fields("BUILDING_HOME_NO").Value), "", rs.Fields("BUILDING_HOME_NO").Value)
                                                        .TextMatrix(.Row, 11) = IIf(IsNull(rs.Fields("BUILDING_TYPE").Value), "", rs.Fields("BUILDING_TYPE").Value)
                                                        .TextMatrix(.Row, 12) = IIf(IsNull(rs.Fields("ZONETAX_ID").Value), "", rs.Fields("ZONETAX_ID").Value)
                                                        .TextMatrix(.Row, 13) = Format$(rs.Fields("RENTYEAR_TOTAL_ALL").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 14) = IIf(IsNull(rs.Fields("BUILDING_ROOM").Value), "", rs.Fields("BUILDING_ROOM").Value)
                                                        .TextMatrix(.Row, 15) = IIf(IsNull(rs.Fields("BUILDING_FLOOR").Value), "", rs.Fields("BUILDING_FLOOR").Value)
                                                        .TextMatrix(.Row, 16) = Format$(rs.Fields("BUILDING_WIDTH").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 17) = Format$(rs.Fields("BUILDING_HEIGHT").Value, "###,##0.00")
                                                        .TextMatrix(.Row, 18) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                        Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                        .Rows = .Rows + 1
                                                End With
                                                rs.MoveNext
                                        Loop
                                        Grid_PRD2.Rows = Grid_PRD2.Rows - 1
                                        Call SetGrid_PRD2(True)
                                Else
                                        Grid_PRD2.Rows = 2
                                        Grid_PRD2.Clear
                                        Call SetGrid_PRD2(True)
                                End If
                                lbRecordCount(3).Caption = rs.RecordCount
                        End If
                        lbPrice_PRD2.Caption = Format$(Amount, "###,##0.00")
                        lbRealPRD2.Caption = Format$(Amount2, "###,##0.00")
                        Case 4 '����
                                '                                                    " FROM PP1,SIGNBORDDATA,SIGNBORDDATA_DETAILS "
'                                    strSQL = "SELECT DISTINCT PP1.SIGNBORD_ID,PP1_NO,PP1_DATE,PP1_AMOUNT_ACCEPT,PP1_YEAR,PP1_PAY,PP1_BOOK_NUMBER,PP1_BOOK_NO," & _
'                                                    " PP1_PAY_DATE,BUILDING_ID,LAND_ID,SIGNBORD_SUMTAX,SIGNBORD_TOTAL,SIGNBORD_AMOUNT,(PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT) AS SUM_TAX " & _
'                                                    " FROM (PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID = SIGNBORDDATA.SIGNBORD_ID) LEFT JOIN SIGNBORDDATA_DETAILS ON SIGNBORDDATA.SIGNBORD_ID = SIGNBORDDATA_DETAILS.SIGNBORD_ID" & _
'                                                    " WHERE PP1.OWNERSHIP_ID='" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PP1_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PP1_YEAR", " AND PP1_YEAR=" & NumYear) & _
'                                                    " GROUP BY PP1.SIGNBORD_ID,PP1_NO,PP1_DATE,PP1_AMOUNT_ACCEPT,PP1_YEAR,PP1_PAY,PP1_BOOK_NUMBER,PP1_BOOK_NO,PP1_PAY_DATE,BUILDING_ID,LAND_ID,SIGNBORD_SUMTAX,SIGNBORD_TOTAL,SIGNBORD_AMOUNT,PP1_AMOUNT_REAL+PP1_TAXINCLUDE+PP1_DISCOUNT"
'                                    Call SET_QUERY(strSQL, Rs)
                                    Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                                    If rs.STATE = adStateOpen Then
                                            If rs.RecordCount > 0 Then
                                                    Grid_PP1.Rows = 2
                                                    rs.MoveFirst
                                                    Do While Not rs.EOF
                                                            With Grid_PP1
                                                                    .Row = .Rows - 1
                                                                    .TextMatrix(.Row, 0) = rs.Fields("PP1_YEAR").Value
                                                                    .TextMatrix(.Row, 1) = rs.Fields("SIGNBORD_ID").Value
                                                                    .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields("PP1_NO").Value), "", rs.Fields("PP1_NO").Value)
                                                                    .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PP1_DATE").Value), "", rs.Fields("PP1_DATE").Value)
                                                                    .TextMatrix(.Row, 4) = Format$(rs.Fields("PP1_AMOUNT_ACCEPT").Value, "###,##0.00")
                                                                    Amount = Amount + rs.Fields("PP1_AMOUNT_ACCEPT").Value
                                                                    If rs.Fields("PP1_PAY").Value = 0 Then
                                                                            .TextMatrix(.Row, 5) = "������繼�����"
                                                                    Else
                                                                            .TextMatrix(.Row, 5) = "�繼�����"
                                                                    End If
                                                                    .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PP1_BOOK_NUMBER").Value), "", rs.Fields("PP1_BOOK_NUMBER").Value)
                                                                    .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PP1_BOOK_NO").Value), "", rs.Fields("PP1_BOOK_NO").Value)
                                                                    .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("PP1_PAY_DATE").Value), "", rs.Fields("PP1_PAY_DATE").Value)
                                                                    .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_ID").Value), "��辺", rs.Fields("LAND_ID").Value)
'                                                                    .TextMatrix(.Row, 10) = Format$(Rs.Fields("SIGNBORD_SUMTAX").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 10) = Format$(rs.Fields("SIGNBORD_TOTAL").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 11) = Format$(rs.Fields("SIGNBORD_AMOUNT").Value, "###,##0.00")
                                                                    .TextMatrix(.Row, 12) = Format$(rs.Fields("SUM_TAX").Value, "###,##0.00")
                                                                    Amount2 = Amount2 + rs.Fields("SUM_TAX").Value
                                                                    .Rows = .Rows + 1
                                                            End With
                                                            rs.MoveNext
                                                    Loop
                                                    Grid_PP1.Rows = Grid_PP1.Rows - 1
                                                    Call SetGrid_PP1(True)
                                            Else
                                                    Grid_PP1.Rows = 2
                                                    Grid_PP1.Clear
                                                    Call SetGrid_PP1(True)
                                            End If
                                    End If
                                    lbRecordCount(2).Caption = rs.RecordCount
                            lbPrice_PP1.Caption = Format$(Amount, "###,##0.00")
                            lbRealPP1.Caption = Format$(Amount2, "###,##0.00")
                        Case 5 '�͹حҵ
'                        strSQL = "SELECT PBA1.LICENSE_BOOK,PBA1.LICENSE_NO,BUILDING_ID,LAND_ID,PBA1_DATE,PBA1_AMOUNT,PBA1_YEAR,PBA1_NO_ACCEPT," & _
'                                " PBA1_DATE_ACCEPT,LICENSE_SUMTAX,STATUS_LINK,FLAG_STATUS,CLASS_DETAILS,BUSINESS_TYPE,PBA1_NO " & _
'                                " FROM PBA1,LICENSEDATA,CLASS_DETAILS,BUSINESS_TYPE " & _
'                                " WHERE (PBA1.LICENSE_BOOK=LICENSEDATA.LICENSE_BOOK) AND (PBA1.LICENSE_NO=LICENSEDATA.LICENSE_NO) AND " & _
'                                " (LICENSEDATA.CLASS_ID=CLASS_DETAILS.CLASS_ID) AND (BUSINESS_TYPE.BUSINESS_ID=CLASS_DETAILS.BUSINESS_ID) AND " & _
'                                " PBA1.OWNERSHIP_ID='" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "'" & " AND PBA1_STATUS=" & Flag_Assess & IIf(chkAllYear.Value = Checked, " ORDER BY PBA1_YEAR", " AND PBA1_YEAR=" & NumYear)
'
'                        Call SET_QUERY(strSQL, Rs)
                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_ownership '" & NumYear & "','" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0) & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                            Flag_Assess & "','" & chkAllYear.Value & "','" & Flag_Old_New & "'", , adCmdUnknown)
                        If rs.STATE = adStateOpen Then
                                If rs.RecordCount > 0 Then
                                        Grid_PBA1.Rows = 2
                                        rs.MoveFirst
                                        Do While Not rs.EOF
                                                With Grid_PBA1
                                                        .Row = .Rows - 1
                                                        .TextMatrix(.Row, 0) = rs.Fields("PBA1_YEAR").Value
                                                        .TextMatrix(.Row, 1) = rs.Fields("LICENSE_BOOK").Value
                                                        .TextMatrix(.Row, 2) = rs.Fields("LICENSE_NO").Value
                                                        .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields("PBA1_NO").Value), "", rs.Fields("PBA1_NO").Value)
                                                        .TextMatrix(.Row, 4) = IIf(IsNull(rs.Fields("PBA1_DATE").Value), "", rs.Fields("PBA1_DATE").Value)
                                                        .TextMatrix(.Row, 5) = Format$(rs.Fields("PBA1_AMOUNT").Value, "###,##0.00")
                                                        Amount = Amount + rs.Fields("PBA1_AMOUNT").Value
                                                        .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields("PBA1_NO_ACCEPT").Value), "", rs.Fields("PBA1_NO_ACCEPT").Value)
                                                        .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields("PBA1_DATE_ACCEPT").Value), "", rs.Fields("PBA1_DATE_ACCEPT").Value)
                                                        .TextMatrix(.Row, 8) = IIf(IsNull(rs.Fields("BUILDING_ID").Value), "", rs.Fields("BUILDING_ID").Value)
                                                        .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields("LAND_ID").Value), "", rs.Fields("LAND_ID").Value)
                                                        .TextMatrix(.Row, 10) = IIf(rs.Fields("FLAG_STATUS").Value = 0, "������͹حҵ", "���͹حҵ")
                                                        Select Case rs.Fields("STATUS_LINK").Value
                                                                Case 0
                                                                        .TextMatrix(.Row, 11) = "����շ����"
                                                                Case 1
                                                                        .TextMatrix(.Row, 11) = "��駺��ŧ���Թ"
                                                                Case 2
                                                                        .TextMatrix(.Row, 11) = "��駺��ç���͹"
                                                        End Select
                                                        .TextMatrix(.Row, 12) = rs.Fields("CLASS_DETAILS").Value
                                                        .TextMatrix(.Row, 13) = rs.Fields("BUSINESS_TYPE").Value
                                                        .Rows = .Rows + 1
                                                End With
                                                rs.MoveNext
                                        Loop
                                        Grid_PBA1.Rows = Grid_PBA1.Rows - 1
                                        Call SetGrid_PBA1(True)
                                Else
                                        Grid_PBA1.Rows = 2
                                        Grid_PBA1.Clear
                                        Call SetGrid_PBA1(True)
                                End If
                                lbRecordCount(4).Caption = rs.RecordCount
                        End If
                        lbPrice_PBA1.Caption = Format$(Amount, "###,##0.00")
                End Select
                End If  'If cbArticle.ListIndex = 0 Then
        Exit Sub
ErrGetQuery:
        MsgBox Err.Description
End Sub

Private Sub Grid_OwnerShip_GotFocus()
        WheelUnHook
        WheelHook Frm_StoryPayment, Grid_OwnerShip
End Sub

Private Sub Grid_OwnerShip_LostFocus()
        WheelUnHook
End Sub

Private Sub Image_Print_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
    Image_Print(Index).Top = Image_Print(Index).Top + 39
End Sub

Private Sub Image_Print_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  If Index = 1 Or Index = 2 Or Index = 3 Or Index = 4 Then
     Shape2.Left = 3470 '3300
 Else
    Shape2.Top = 0
    Shape2.Left = 12960 '12300
 End If
  If Index = 1 Then Shape2.Top = 1280
  If Index = 2 Then Shape2.Top = 3240
  If Index = 3 Then Shape2.Top = 5220
  If Index = 4 Then Shape2.Top = 7080
End Sub

Private Sub Image_Print_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  If Index = 0 Then Image_Print(Index).Top = -30
  If Index = 1 Then Image_Print(Index).Top = 1290
  If Index = 2 Then Image_Print(Index).Top = 3270
  If Index = 3 Then Image_Print(Index).Top = 5250
  If Index = 4 Then Image_Print(Index).Top = 7110
     If CheckStandardDemo = True Then
           MsgBox "��ҹ���ѧ��������� STANDRD DEMO" & vbCrLf & "��سҵԴ��� �.�͹��෤ �ӡѴ" & "Tel.02-9331116", vbExclamation, "����͹"
           Exit Sub
    End If
  Dim SQL_REPORT As String
  Dim strType As String
  Select Case Index  '���� print �����ѹ
        Case 0
                Select Case cbArticle.ListIndex
                        Case 0 ' ����¹��Ѿ���Թ
                                If Cmb_StoryAll.ListIndex = 1 Then
                                        If chkFlag1.Value = Checked Then
                                                strType = "VIEW_ASSET_TAX_1"
                                        Else
                                                strType = "VIEW_ASSET_TAX_0"
                                        End If
                                Else
                                        strType = "OWNERSHIP"
                                End If
                        Case 1, 2, 3 ' �.�.1
                                Select Case Cmb_StoryAll.ListIndex
                                        Case 1
                                                strType = "VIEW_TAX_ALL"
                                        Case 2
                                                strType = "PBT5"
                                        Case 3
                                                strType = "PRD2"
                                        Case 4
                                                strType = "PP1"
                                        Case 5
                                                strType = "PBA1"
                                End Select
                End Select
        Case 1
                If cbArticle.ListIndex = 0 Then
                        strType = "OWNERSHIP"
                Else
                        strType = "PBT5"
                End If
        Case 3
                If cbArticle.ListIndex = 0 Then
                        strType = "OWNERSHIP"
                Else
                        strType = "PRD2"
                End If
        Case 2
                If cbArticle.ListIndex = 0 Then
                        strType = "OWNERSHIP"
                Else
                        strType = "PP1"
                End If
        Case 4
                If cbArticle.ListIndex = 0 Then
                        strType = "OWNERSHIP"
                Else
                        strType = "PBA1"
                End If
  End Select

  SQL_REPORT = ""

  If cmb_SignBoard_Owner.ListIndex > 0 Then  '����������ա����Է���
        SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_TYPE} = " & cmb_SignBoard_Owner.ListIndex
  End If
  If LenB(Trim$(cbZone.Text)) > 0 Then
        SQL_REPORT = SQL_REPORT & " AND {LANDDATA.ZONE_BLOCK} = '" & Trim$(cbZone.Text) & "'"
  End If
'  If cbStart_Char.Text <> Empty Then
'      SQL_REPORT = SQL_REPORT & " AND ({OWNERSHIP.OWNER_NAME} >= '" & cbStart_Char.Text & "' AND {OWNERSHIP.OWNER_NAME} <= '" & cbEnd_Char.Text & "')"
'  End If
  If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
                                If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
                                            If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
                                                            SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} > '" & cbStart_Char & "'"
                                                    Else
                                                            SQL_REPORT = SQL_REPORT & " AND ({" & strType & ".OWNER_NAME} > '" & cbStart_Char & "'" & " AND {" & strType & ".OWNER_NAME} < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "')"
                                                    End If
                                            Else
                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
                                                            SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} > '" & cbEnd_Char & "'"
                                                    Else
                                                            SQL_REPORT = SQL_REPORT & " AND ({" & strType & ".OWNER_NAME} > '" & cbEnd_Char & "'" & " AND {" & strType & ".OWNER_NAME} < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "')"
                                                    End If
                                            End If
                                End If
                         End If
                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
                                If cbStart_Char = "�" Then
                                        SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} > '" & cbStart_Char & "'"
                                Else
                                        SQL_REPORT = SQL_REPORT & " AND ({" & strType & ".OWNER_NAME} > '" & cbStart_Char & "'" & " AND {" & strType & ".OWNER_NAME} < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "')"
                                End If
                        End If
                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
                                If cbEnd_Char = "�" Then
                                        SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} > '" & cbEnd_Char & "'"
                                Else
                                        SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} > '" & cbEnd_Char & "'" & " AND {" & strType & ".OWNER_NAME} < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "')"
                                End If
                        End If
  If Index = 0 Then
            If LenB(Trim$(txtName.Text)) > 0 Then
                SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_NAME} LIKE '" & txtName.Text & "*'"
            End If
            If LenB(Trim$(txtSurName.Text)) > 0 Then
                SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNER_SURNAME} LIKE '" & txtSurName.Text & "*'"
            End If
  Else
                SQL_REPORT = SQL_REPORT & " AND {" & strType & ".OWNERSHIP_ID} = '" & Trim$(Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 0)) & "'"
  End If
  Select Case Cmb_StoryAll.ListIndex
        Case 2
                    Select Case cbArticle.ListIndex
                          Case 1  '�.�.1
                                  SQL_REPORT = SQL_REPORT & " AND {PBT5.PBT5_SET_GK} = 1"
                          Case 2 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PBT5.PBT5_SET_GK} = 0"
                          Case 3 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PBT5.PBT5_SET_GK} = 2"
                    End Select
        Case 3
                    Select Case cbArticle.ListIndex
                          Case 1  '�.�.1
                                  SQL_REPORT = SQL_REPORT & " AND {PRD2.PRD2_SET_GK} = 1"
                          Case 2 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PRD2.PRD2_SET_GK} = 0"
                          Case 3 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PRD2.PRD2_SET_GK} = 2"
                    End Select
        Case 4
                    Select Case cbArticle.ListIndex
                          Case 1  '�.�.1
                                  SQL_REPORT = SQL_REPORT & " AND {PP1.PP1_SET_GK} = 1"
                          Case 2 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PP1.PP1_SET_GK} = 0"
                          Case 3 '�������
                                  SQL_REPORT = SQL_REPORT & " AND {PP1.PP1_SET_GK} = 2"
                    End Select
  End Select
  If cbArticle.ListIndex > 0 Then
       If chkAllYear.Value = Unchecked Then
           Select Case Cmb_StoryAll.ListIndex
                        Case 2
                                SQL_REPORT = SQL_REPORT & " AND {PBT5.PBT5_YEAR} = " & txtYear.Text
                        Case 3
                                SQL_REPORT = SQL_REPORT & " AND {PRD2.PRD2_YEAR} = " & txtYear.Text
                        Case 4
                                SQL_REPORT = SQL_REPORT & " AND {PP1.PP1_YEAR} = " & txtYear.Text
                        Case 5
                                SQL_REPORT = SQL_REPORT & " AND {PBA1.PBA1_YEAR} = " & txtYear.Text
           End Select
       End If
  End If
  
            If LenB(Trim$(SQL_REPORT)) > 0 Then SQL_REPORT = Right$(SQL_REPORT, Len(SQL_REPORT) - 4)

If cbArticle.ListIndex = 0 Then '��Ǵ�͡��§ҹ��û����Թ����  =================================
      If chkFlag1.Value Then  '�١�����Թ����
                Select Case Cmb_StoryAll.ListIndex
                            Case 1
                                    Select Case Index
                                            Case 0
                                                    Call ShowReport("All_AssetTax_PayTax.rpt", SQL_REPORT)
                                            Case 1
                                                    Call ShowReport("Rpt_Flag_Paytax1_Land.rpt", SQL_REPORT)
                                            Case 2
                                                    Call ShowReport("Rpt_Flag_Paytax1_Signbord.rpt", SQL_REPORT)
                                            Case 3
                                                    Call ShowReport("Rpt_Flag_Paytax1_Building.rpt", SQL_REPORT)
                                            Case 4
                                                    Call ShowReport("Rpt_Flag_Paytax1_Licence.rpt", SQL_REPORT)
                                    End Select
                            Case 2
                                    Call ShowReport("Rpt_Flag_Paytax1_Land.rpt", SQL_REPORT)
                            Case 4
                                    Call ShowReport("Rpt_Flag_Paytax1_Signbord.rpt", SQL_REPORT)
                            Case 3
                                    Call ShowReport("Rpt_Flag_Paytax1_Building.rpt", SQL_REPORT)
                            Case 5
                                    Call ShowReport("Rpt_Flag_Paytax1_Licence.rpt", SQL_REPORT)
                End Select
      Else                                     '  ���١�����Թ����
                Select Case Cmb_StoryAll.ListIndex
                            Case 1
                                    Select Case Index
                                            Case 0
                                                    Call ShowReport("All_AssetTax_Not_PayTax.rpt", SQL_REPORT)
                                            Case 1
                                                    Call ShowReport("Rpt_Flag_Paytax0_Land.rpt", SQL_REPORT)
                                            Case 2
                                                    Call ShowReport("Rpt_Flag_Paytax0_Signbord.rpt", SQL_REPORT)
                                            Case 3
                                                    Call ShowReport("Rpt_Flag_Paytax0_Building.rpt", SQL_REPORT)
                                            Case 4
                                                    Call ShowReport("Rpt_Flag_Paytax1_Licence.rpt", SQL_REPORT)
                                    End Select
                            Case 2
                                    Call ShowReport("Rpt_Flag_Paytax0_Land.rpt", SQL_REPORT)
                            Case 4
                                    Call ShowReport("Rpt_Flag_Paytax0_Signbord.rpt", SQL_REPORT)
                            Case 3
                                    Call ShowReport("Rpt_Flag_Paytax0_Building.rpt", SQL_REPORT)
                            Case 4
                                    Call ShowReport("Rpt_Flag_Paytax1_Licence.rpt", SQL_REPORT)
                End Select
      End If

ElseIf cbArticle.ListIndex > 0 Then '��Ǵ�͡��§ҹ��ê�������    =================================
      If chkFlag1.Value Then
                Select Case Cmb_StoryAll.ListIndex
                        Case 1
                                If Create_View = True Then
                                        Call ShowReport("All_GK_Paytax.rpt", SQL_REPORT)
                                End If
                        Case 2
                                Call ShowReport("Rpt_PBT5_STATUS1.rpt", SQL_REPORT)
                        Case 4
                                Call ShowReport("Rpt_PP1_STATUS1.rpt", SQL_REPORT)
                        Case 3
                                Call ShowReport("Rpt_PRD2_STATUS1.rpt", SQL_REPORT)
                        Case 5
                                Call ShowReport("Rpt_PBA1_STATUS1.rpt", SQL_REPORT)
                End Select
      Else
                Select Case Cmb_StoryAll.ListIndex
                        Case 1
                                If Create_View = True Then
                                        Call ShowReport("All_GK_Paytax.rpt", SQL_REPORT)
                                End If
                        Case 2
                                Call ShowReport("Rpt_PBT5_STATUS0.rpt", SQL_REPORT)
                        Case 4
                                Call ShowReport("Rpt_PP1_STATUS0.rpt", SQL_REPORT)
                        Case 3
                                Call ShowReport("Rpt_PRD2_STATUS0.rpt", SQL_REPORT)
                        Case 5
                                Call ShowReport("Rpt_PBA1_STATUS0.rpt", SQL_REPORT)
                  End Select
      End If
End If
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        Image1.Top = Image1.Top + 24
End Sub

Private Sub Image1_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        Shape2.Left = 12370
        Shape2.Top = 0
End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        On Error GoTo ErrSearch
        
        Image1.Top = -30
        Me.MousePointer = 11
        Sum1 = 0
        Sum2 = 0
        Sum3 = 0
        Sum4 = 0
        numCount = 0
        Grid_PBT5.Rows = 2
        Grid_PBT5.Clear
        Grid_PP1.Rows = 2
        Grid_PP1.Clear
        Grid_PRD2.Rows = 2
        Grid_PRD2.Clear
        Grid_PBA1.Rows = 2
        Grid_PBA1.Clear
        lbRecordCount(1).Caption = "0"
        lbRecordCount(2).Caption = "0"
        lbRecordCount(3).Caption = "0"
        lbRecordCount(4).Caption = "0"
        lbRecordCount(0).Caption = "0"
        lbRecordCount_PBA1.Caption = "0"
        lbRecordCount_PBT5.Caption = "0"
        lbRecordCount_PP1.Caption = "0"
        lbRecordCount_PRD2.Caption = "0"
        lbSumPBT5.Caption = "0.00"
        lbSumPP1.Caption = "0.00"
        lbSumPRD2.Caption = "0.00"
        lbSumPBA1.Caption = "0.00"
        lbTotalMoneyAll.Caption = "0.00"
        
        If cbArticle.ListIndex = 0 Then  '------------------------- ����¹��Ѿ���Թ
                Call SetGrid_PBT5(False)
                Call SetGrid_PRD2(False)
                Call SetGrid_PP1(False)
                Call SetGrid_PBA1(False)
                Call Condition_Assess                     '-------------- ��Ǵ��û����Թ���� ....
        ElseIf cbArticle.ListIndex = 1 Or cbArticle.ListIndex = 2 Or cbArticle.ListIndex = 3 Then        '-------------- ��Ǵ��ê������� ....
                Call SetGrid_PBT5(True)
                Call SetGrid_PRD2(True)
                Call SetGrid_PP1(True)
                Call SetGrid_PBA1(True)
                Select Case cbArticle.ListIndex
                        Case 1
                                Flag_Old_New = 1
                        Case 2
                                Flag_Old_New = 0
                        Case 3
                                Flag_Old_New = 2
                End Select
'                If chkAllYear.Value = Checked Then  ' ��Ǵ��ê��� �ء�� ....
                        Select Case Cmb_StoryAll.ListIndex
                                Case 1
'                                        strSQL = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID WHERE PBT5_STATUS = " & Flag_Assess & " AND PBT5_SET_GK=" & Flag_Old_New
'                                        strSQL2 = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID=SIGNBORDDATA.SIGNBORD_ID WHERE PP1_STATUS = " & Flag_Assess & " AND PP1_SET_GK=" & Flag_Old_New
'                                        strSQL3 = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID WHERE PRD2_STATUS = " & Flag_Assess & " AND PRD2_SET_GK=" & Flag_Old_New
'                                        strSQL4 = "SELECT DISTINCT PBA1.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM  ((LICENSEDATA LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                                " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID) INNER JOIN PBA1 ON LICENSEDATA.OWNERSHIP_ID = PBA1.OWNERSHIP_ID " & _
'                                                " WHERE PBA1_STATUS = " & Flag_Assess & " AND PBA1_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum1 = "SELECT SUM(" & strLand2 & ") AS SUM_PBT5 FROM (SELECT " & strLand & _
'                                                " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID WHERE PBT5_STATUS = " & Flag_Assess & " AND PBT5_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum2 = "SELECT SUM(" & strSignboard2 & ") AS SUM_PP1 FROM (SELECT " & strSignboard & _
'                                                " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID=SIGNBORDDATA.SIGNBORD_ID WHERE PP1_STATUS = " & Flag_Assess & " AND PP1_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum3 = "SELECT SUM(" & strBuilding2 & ") AS SUM_PRD2 FROM (SELECT " & strBuilding & _
'                                                " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID WHERE PRD2_STATUS = " & Flag_Assess & " AND PRD2_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum4 = "SELECT SUM(PBA1_AMOUNT ) AS SUM_PBA1 FROM (SELECT PBA1_AMOUNT " & _
'                                                        " FROM (((LICENSEDATA LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID)" & _
'                                                        " INNER JOIN PBA1 ON LICENSEDATA.OWNERSHIP_ID = PBA1.OWNERSHIP_ID) WHERE PBA1_STATUS = " & Flag_Assess & " AND PBA1_SET_GK=" & Flag_Old_New
'                                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL2 = strSQL2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL3 = strSQL3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL4 = strSQL4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                        End If
'                                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL2 = strSQL2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL3 = strSQL3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL4 = strSQL4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                        End If
'
'                                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL2 = strSQL2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL3 = strSQL3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL4 = strSQL4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                End If
'                                        End If
'
'                                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                                strSQL = strSQL & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL2 = strSQL2 & " AND SIGNBORDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL3 = strSQL3 & " AND BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL4 = strSQL4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND SIGNBORDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                        End If
'
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                    If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                                If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                        Else
'                                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL4 = strSQL4 & " AND OWNER_NAME >"
'                                                                                '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                        End If
'                                                                Else
'                                                                        If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                        Else
'                                                                                strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                        End If
'                                                                End If
'                                                    End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                                If cbStart_Char = "�" Then
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                Else
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbEnd_Char = "�" Then
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                Else
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        strSQL_Sum1 = strSQL_Sum1 & ")"
'                                        strSQL_Sum2 = strSQL_Sum2 & ")"
'
'
'                                        strSQL_Sum3 = strSQL_Sum3 & ")"
'                                        strSQL_Sum4 = strSQL_Sum4 & ")"
'                                        Call SET_QUERY(strSQL_Sum1, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','2','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum1 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)

'                                        Call SET_QUERY(strSQL_Sum2, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','3','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum2 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)

'                                        Call SET_QUERY(strSQL_Sum3, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','4','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum3 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                                        
'                                        Call SET_QUERY(strSQL_Sum4, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','5','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum4 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                                        
                                        lbSumPBT5.Caption = Format$(Sum1, "###,##0.00")
                                        lbSumPP1.Caption = Format$(Sum2, "###,##0.00")
                                        lbSumPRD2.Caption = Format$(Sum3, "###,##0.00")
                                        lbSumPBA1.Caption = Format$(Sum4, "###,##0.00")
                                        lbTotalMoneyAll.Caption = Format$(CCur(lbSumPBT5.Caption) + CCur(lbSumPP1.Caption) + CCur(lbSumPRD2.Caption) + CCur(lbSumPBA1.Caption), "###,##0.00")

'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','2','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PBT5.Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PBT5.Caption = "0"
                                        End If
'                                        Call SET_QUERY(strSQL2, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','3','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PP1.Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PP1.Caption = "0"
                                        End If
'                                        Call SET_QUERY(strSQL3, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','4','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PRD2.Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PRD2.Caption = "0"
                                        End If
'                                        Call SET_QUERY(strSQL4, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','5','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PBA1.Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PBA1.Caption = "0"
                                        End If
'                                        strSQL = strSQL & " ORDER BY OWNER_NAME"
'                                        strSQL2 = strSQL2 & " ORDER BY OWNER_NAME"
'                                        strSQL3 = strSQL3 & " ORDER BY OWNER_NAME"
'                                        strSQL4 = strSQL4 & " ORDER BY OWNER_NAME"
'                                        strSQL = strSQL & " UNION " & strSQL2 & " UNION " & strSQL3 & " UNION " & strSQL4
'                                        Call SET_QUERY(strSQL, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_all '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        Set Grid_OwnerShip.DataSource = rs
                                                        NumYear = CInt(txtYear.Text)
                                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                                Else
                                                        Grid_OwnerShip.Rows = 2
                                                        Grid_OwnerShip.Clear
                                                End If
                                        End If
                                        Call SetGrid_Ownership
                                Case 2 '���Թ
'                                        strSQL = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID WHERE PBT5_STATUS = " & Flag_Assess & " AND PBT5_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum1 = "SELECT SUM(" & strLand2 & ") AS SUM_PBT5 FROM (SELECT " & strLand & _
'                                                        " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID  WHERE PBT5_STATUS = " & Flag_Assess & " AND PBT5_SET_GK=" & Flag_Old_New
'                                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                                strSQL = strSQL & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                        End If
'                                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                                strSQL = strSQL & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                        End If
'
'                                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                                        strSQL = strSQL & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                End If
'                                        End If
'                                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                                strSQL = strSQL & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum1 = strSQL_Sum1 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                        End If
'
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                            If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                            strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                    Else
'                                                                            strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            Else
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                            strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                    Else
'                                                                            strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            End If
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                                If cbStart_Char = "�" Then
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                Else
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbEnd_Char = "�" Then
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                Else
'                                                        strSQL = strSQL & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum1 = strSQL_Sum1 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'
'                                        strSQL_Sum1 = strSQL_Sum1 & ")"
'
'                                        Call SET_QUERY(strSQL_Sum1, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum1 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)

                                        lbSumPBT5.Caption = FormatNumber(Sum1, 2, vbTrue)
                                        lbTotalMoneyAll.Caption = lbSumPBT5.Caption
'                                        strSQL = strSQL & " ORDER BY OWNER_NAME"
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
'                                        Call SET_QUERY(strSQL, Rs)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                                Set Grid_OwnerShip.DataSource = rs
                                                NumYear = CInt(txtYear.Text)
                                        Else
                                                Grid_OwnerShip.Rows = 2
                                                Grid_OwnerShip.Clear
                                                lbRecordCount_PBT5.Caption = "0"
                                                lbRecordCount(0).Caption = "0"
                                        End If
'                                        Debug.Print "exec sp_search_storypayment_count_gk '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
'                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
'                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'"
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_count_gk '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PBT5.Caption = FormatNumber(rs.Fields(0).Value, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PBT5.Caption = "0"
                                        End If
                                        Call SetGrid_Ownership
                                Case 3 '�ç���͹
'                                        strSQL3 = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                        " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID WHERE PRD2_STATUS = " & Flag_Assess & " AND PRD2_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum3 = "SELECT SUM(" & strBuilding2 & ") AS SUM_PRD2 FROM (SELECT " & strBuilding & _
'                                                        " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID=BUILDINGDATA.BUILDING_ID WHERE PRD2_STATUS = " & Flag_Assess & " AND PRD2_SET_GK=" & Flag_Old_New
'                                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                                strSQL3 = strSQL3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                        End If
'                                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                                strSQL3 = strSQL3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                        End If
'
'                                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                                        strSQL3 = strSQL3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                End If
'                                        End If
'                                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                                strSQL3 = strSQL3 & " AND BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum3 = strSQL_Sum3 & " AND BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                        End If
'
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                            If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                            strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                    Else
'                                                                            strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            Else
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                            strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                    Else
'                                                                            strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            End If
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                                If cbStart_Char = "�" Then
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                Else
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbEnd_Char = "�" Then
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                Else
'                                                        strSQL3 = strSQL3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum3 = strSQL_Sum3 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'
'                                        strSQL_Sum3 = strSQL_Sum3 & ")"
'                                        Call SET_QUERY(strSQL_Sum3, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum3 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                                        lbSumPRD2.Caption = FormatNumber(Sum3, 2, vbTrue)
                                        lbTotalMoneyAll.Caption = lbSumPRD2.Caption
                                        
'                                        strSQL3 = strSQL3 & " ORDER BY OWNER_NAME"
'                                        Call SET_QUERY(strSQL3, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                                        Set Grid_OwnerShip.DataSource = rs
                                                        NumYear = CInt(txtYear.Text)
                                                Else
                                                        lbRecordCount_PRD2.Caption = "0"
                                                        lbRecordCount(0).Caption = "0"
                                                        Grid_OwnerShip.Rows = 2
                                                        Grid_OwnerShip.Clear
                                                End If
                                        End If
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_count_gk '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PRD2.Caption = FormatNumber(rs.Fields(0).Value, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PRD2.Caption = "0"
                                        End If
                                        Call SetGrid_Ownership
                                Case 4 '����
'                                        strSQL2 = "SELECT DISTINCT OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                        " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID=SIGNBORDDATA.SIGNBORD_ID WHERE PP1_STATUS = " & Flag_Assess & " AND PP1_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum2 = "SELECT SUM(" & strSignboard2 & ") AS SUM_PP1 FROM (SELECT " & strSignboard & _
'                                                        " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID=SIGNBORDDATA.SIGNBORD_ID WHERE PP1_STATUS = " & Flag_Assess & " AND PP1_SET_GK=" & Flag_Old_New
'                                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                                strSQL2 = strSQL2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                        End If
'                                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                                strSQL2 = strSQL2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                        End If
'
'                                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                                        strSQL2 = strSQL2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                End If
'                                        End If
'                                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                                strSQL2 = strSQL2 & " AND SIGNBORDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum2 = strSQL_Sum2 & " AND SIGNBORDDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                        End If
'
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                            If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                            strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                    Else
'                                                                            strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            Else
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                            strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                    Else
'                                                                            strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            End If
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                                If cbStart_Char = "�" Then
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                Else
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbEnd_Char = "�" Then
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                Else
'                                                        strSQL2 = strSQL2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum2 = strSQL_Sum2 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'
'                                        strSQL_Sum2 = strSQL_Sum2 & ")"
'                                        Call SET_QUERY(strSQL_Sum2, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum2 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)

                                        lbSumPP1.Caption = Format$(Sum2, "###,##0.00")
                                        lbTotalMoneyAll.Caption = lbSumPP1.Caption
                                        
'                                        strSQL2 = strSQL2 & " ORDER BY OWNER_NAME"
'                                        Call SET_QUERY(strSQL2, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                                        Set Grid_OwnerShip.DataSource = rs
                                                        NumYear = CInt(txtYear.Text)
                                                Else
                                                        lbRecordCount_PP1.Caption = "0"
                                                        lbRecordCount(0).Caption = "0"
                                                        Grid_OwnerShip.Rows = 2
                                                        Grid_OwnerShip.Clear
                                                End If
                                        End If
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_count_gk '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PP1.Caption = FormatNumber(rs.Fields(0).Value, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PP1.Caption = "0"
                                        End If
                                        Call SetGrid_Ownership
                                Case 5 '�͹حҵ
'                                         strSQL4 = "SELECT DISTINCT PBA1.OWNERSHIP_ID,PRENAME & '' & OWNER_NAME & '   ' & OWNER_SURNAME,OWNER_NAME " & _
'                                                " FROM  ((LICENSEDATA LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON " & _
'                                                " LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID) INNER JOIN PBA1 ON LICENSEDATA.OWNERSHIP_ID = PBA1.OWNERSHIP_ID " & _
'                                                " WHERE PBA1_STATUS = " & Flag_Assess & " AND PBA1_SET_GK=" & Flag_Old_New
'                                        strSQL_Sum4 = "SELECT SUM(PBA1_AMOUNT ) AS SUM_PBA1 FROM (SELECT PBA1_AMOUNT " & _
'                                                        " FROM (((LICENSEDATA LEFT JOIN LANDDATA ON LICENSEDATA.LAND_ID = LANDDATA.LAND_ID) LEFT JOIN BUILDINGDATA ON LICENSEDATA.BUILDING_ID = BUILDINGDATA.BUILDING_ID)" & _
'                                                        " INNER JOIN PBA1 ON LICENSEDATA.OWNERSHIP_ID = PBA1.OWNERSHIP_ID) WHERE PBA1_STATUS = " & Flag_Assess & " AND PBA1_SET_GK=" & Flag_Old_New
'                                        If LenB(Trim$(txtName.Text)) > 0 Then
'                                                strSQL4 = strSQL4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME LIKE '" & Trim$(txtName.Text) & "%'"
'                                        End If
'                                        If LenB(Trim$(txtSurName.Text)) > 0 Then
'                                                strSQL4 = strSQL4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_SURNAME LIKE '" & Trim$(txtSurName.Text) & "%'"
'                                        End If
'
'                                        If LenB(Trim$(cmb_SignBoard_Owner.Text)) <> 0 Then
'                                                If cmb_SignBoard_Owner.ListIndex <> 0 Then
'                                                        strSQL4 = strSQL4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_TYPE = " & cmb_SignBoard_Owner.ListIndex
'                                                End If
'                                        End If
'                                        If LenB(Trim$(cbZone.Text)) <> 0 Then
'                                                strSQL4 = strSQL4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                                strSQL_Sum4 = strSQL_Sum4 & " AND LANDDATA.ZONE_BLOCK='" & cbZone.Text & "'" & " OR BUILDINGDATA.ZONE_BLOCK='" & cbZone.Text & "'"
'                                        End If
'
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) Or (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                            If cbStart_Char.ListIndex < cbEnd_Char.ListIndex Then
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                            strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                                    Else
'                                                                            strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            Else
'                                                                    If cbStart_Char = "�" Or cbEnd_Char = "�" Then
'                                                                            strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                            strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                                    Else
'                                                                            strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                            strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                                    End If
'                                                            End If
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) <> 0) And (LenB(Trim$(cbEnd_Char.Text)) = 0) Then
'                                                If cbStart_Char = "�" Then
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'"
'                                                Else
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbStart_Char & "'" & " AND OWNER_NAME < '" & cbStart_Char.List(cbStart_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'                                        If (LenB(Trim$(cbStart_Char.Text)) = 0) And (LenB(Trim$(cbEnd_Char.Text)) <> 0) Then
'                                                If cbEnd_Char = "�" Then
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'"
'                                                Else
'                                                        strSQL4 = strSQL4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                        strSQL_Sum4 = strSQL_Sum4 & " AND OWNER_NAME > '" & cbEnd_Char & "'" & " AND OWNER_NAME < '" & cbEnd_Char.List(cbEnd_Char.ListIndex + 1) & "'"
'                                                End If
'                                        End If
'
'                                        strSQL_Sum4 = strSQL_Sum4 & ")"
'                                        Call SET_QUERY(strSQL_Sum4, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_sum '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "','" & chkFlag1.Value & "'", , adCmdUnknown)
                                        Sum4 = IIf(IsNull(rs.Fields(0).Value), 0, rs.Fields(0).Value)
                                        
                                        lbSumPBA1.Caption = Format$(Sum4, "###,##0.00")
                                        lbTotalMoneyAll.Caption = lbSumPBA1.Caption
                                        
'                                        strSQL4 = strSQL4 & " ORDER BY OWNER_NAME"
'                                        Call SET_QUERY(strSQL4, Rs)
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.STATE = adStateOpen Then
                                                If rs.RecordCount > 0 Then
                                                        lbRecordCount(0).Caption = FormatNumber(rs.RecordCount, 0, vbFalse, vbUseDefault, vbTrue)
                                                        Set Grid_OwnerShip.DataSource = rs
                                                        NumYear = CInt(txtYear.Text)
                                                Else
                                                        lbRecordCount(0).Caption = "0"
                                                        lbRecordCount_PBA1.Caption = "0"
                                                        Grid_OwnerShip.Rows = 2
                                                        Grid_OwnerShip.Clear
                                                End If
                                        End If
                                        Set rs = Globle_Connective.Execute("exec sp_search_storypayment_count_gk '" & txtYear.Text & "','" & Flag_Assess & "','" & Flag_Old_New & "','" & Cmb_StoryAll.ListIndex & "','" & _
                                                        chkAllYear.Value & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & Trim$(cmb_SignBoard_Owner.ListIndex) & "','" & cbZone.Text & "','" & _
                                                        Trim$(cbStart_Char.Text) & "','" & Trim$(cbEnd_Char.Text) & "'", , adCmdUnknown)
                                        If rs.RecordCount > 0 Then
                                                lbRecordCount_PBA1.Caption = FormatNumber(rs.Fields(0).Value, 0, vbFalse, vbUseDefault, vbTrue)
                                        Else
                                                lbRecordCount_PBA1.Caption = "0"
                                        End If
                                        Call SetGrid_Ownership
                        End Select
        End If
        Me.MousePointer = 0
        Exit Sub
ErrSearch:
        Me.MousePointer = 0
        MsgBox Err.Description
End Sub

Private Sub Label1_Click(Index As Integer)
  If Index = 3 Then chkFlag1.Value = Checked
  If Index = 4 Then chkFlag2.Value = Checked
End Sub

Private Sub lbPrice_PBA1_Change()
        lbTotalMoney.Caption = Format$(CCur(lbPRice_PBT5.Caption) + CCur(lbPrice_PP1.Caption) + CCur(lbPrice_PRD2.Caption) + CCur(lbPrice_PBA1.Caption), "###,##0.00")
End Sub

Private Sub lbPRice_PBT5_Change()
        lbTotalMoney.Caption = Format$(CCur(lbPRice_PBT5.Caption) + CCur(lbPrice_PP1.Caption) + CCur(lbPrice_PRD2.Caption) + CCur(lbPrice_PBA1.Caption), "###,##0.00")
End Sub

Private Sub lbPrice_PP1_Change()
        lbTotalMoney.Caption = Format$(CCur(lbPRice_PBT5.Caption) + CCur(lbPrice_PP1.Caption) + CCur(lbPrice_PRD2.Caption) + CCur(lbPrice_PBA1.Caption), "###,##0.00")
End Sub

Private Sub lbPrice_PRD2_Change()
        lbTotalMoney.Caption = Format$(CCur(lbPRice_PBT5.Caption) + CCur(lbPrice_PP1.Caption) + CCur(lbPrice_PRD2.Caption) + CCur(lbPrice_PBA1.Caption), "###,##0.00")
End Sub

Private Sub txtName_Change()
        Call KILL_DOT(txtName)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 39 Or KeyAscii = 91 Or KeyAscii = 93 Then KeyAscii = 0
        If KeyAscii = 13 Then Call Image1_MouseUp(1, 0, 300, 225)
End Sub

Private Sub txtSurName_Change()
        Call KILL_DOT(txtSurName)
End Sub

Private Sub txtSurName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 39 Or KeyAscii = 91 Or KeyAscii = 93 Then KeyAscii = 0
        If KeyAscii = 13 Then Call Image1_MouseUp(1, 0, 300, 225)
End Sub
Private Sub txtYear_Change()
       lbYear.Caption = "��Шӻ�  " & txtYear.Text
End Sub

Private Sub txtYear_KeyPress(KeyAscii As Integer)
        If KeyAscii = 39 Or KeyAscii = 91 Or KeyAscii = 93 Then KeyAscii = 0
End Sub

