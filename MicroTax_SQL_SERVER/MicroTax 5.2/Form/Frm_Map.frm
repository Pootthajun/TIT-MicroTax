VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{EBAB1620-5BB5-11CF-80D0-004F4B002122}#5.0#0"; "Sis.ocx"
Begin VB.Form Frm_Map 
   BackColor       =   &H00D6D6D6&
   ClientHeight    =   9045
   ClientLeft      =   -945
   ClientTop       =   165
   ClientWidth     =   15285
   ControlBox      =   0   'False
   FillColor       =   &H000000FF&
   Icon            =   "Frm_Map.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9045
   ScaleWidth      =   15285
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   285
      Left            =   14400
      TabIndex        =   69
      Top             =   60
      Visible         =   0   'False
      Width           =   435
   End
   Begin SisLib.Sis Sis1 
      Height          =   8685
      Left            =   2940
      TabIndex        =   66
      Top             =   360
      Width           =   11940
      _Version        =   327680
      _ExtentX        =   21061
      _ExtentY        =   15319
      _StockProps     =   64
      BorderBevel     =   -1  'True
      Display         =   0
   End
   Begin VB.Frame Fme_CreateRubberSheet 
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   11550
      TabIndex        =   34
      Top             =   8535
      Visible         =   0   'False
      Width           =   3300
      Begin VB.TextBox Txt_RubberY 
         Height          =   285
         Left            =   1920
         TabIndex        =   36
         Text            =   "0"
         Top             =   210
         Width           =   1335
      End
      Begin VB.TextBox Txt_RubberX 
         Height          =   285
         Left            =   345
         TabIndex        =   35
         Text            =   "0"
         Top             =   210
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M i c r o T a x  C r e a t e  R u b b e r s h e e t"
         BeginProperty Font 
            Name            =   "BrowalliaUPC"
            Size            =   11.25
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   60
         TabIndex        =   39
         Top             =   -45
         Width           =   2970
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00404040&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00404040&
         Height          =   210
         Left            =   0
         Top             =   0
         Width           =   3255
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y :"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   12
         Left            =   1680
         TabIndex        =   38
         Top             =   255
         Width           =   195
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X :"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   10
         Left            =   105
         TabIndex        =   37
         Top             =   255
         Width           =   195
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   14280
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   64
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":151A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":1AB4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":204E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":25E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":2B82
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":311C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":36B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":3C50
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":41EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":4784
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":4D1E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":52B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":5852
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":5DEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":6386
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":6920
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":6EBA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":7454
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":79EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":7F88
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":8522
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":8ABC
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":9056
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":95F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":9B8A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":A124
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":A6BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":AC58
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":B1F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":B78C
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":BD26
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":C2C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":C85A
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":CDF4
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":D38E
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":D928
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":DEC2
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":E45C
            Key             =   ""
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":E9F6
            Key             =   ""
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":EF90
            Key             =   ""
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":F52A
            Key             =   ""
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":FAC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":1005E
            Key             =   ""
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":105F8
            Key             =   ""
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":10B92
            Key             =   ""
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":1112C
            Key             =   ""
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":116C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":11C60
            Key             =   ""
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":121FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":12794
            Key             =   ""
         EndProperty
         BeginProperty ListImage51 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":12D2E
            Key             =   ""
         EndProperty
         BeginProperty ListImage52 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":132C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage53 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":13862
            Key             =   ""
         EndProperty
         BeginProperty ListImage54 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":13DFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage55 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":14396
            Key             =   ""
         EndProperty
         BeginProperty ListImage56 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":14930
            Key             =   ""
         EndProperty
         BeginProperty ListImage57 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":14ECA
            Key             =   ""
         EndProperty
         BeginProperty ListImage58 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":15464
            Key             =   ""
         EndProperty
         BeginProperty ListImage59 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":159FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage60 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":15F98
            Key             =   ""
         EndProperty
         BeginProperty ListImage61 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":16532
            Key             =   ""
         EndProperty
         BeginProperty ListImage62 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":16ACC
            Key             =   ""
         EndProperty
         BeginProperty ListImage63 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":17066
            Key             =   ""
         EndProperty
         BeginProperty ListImage64 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Map.frx":17600
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Tb_Mesure 
      Height          =   360
      Left            =   6840
      TabIndex        =   2
      Top             =   0
      Width           =   2325
      _ExtentX        =   4101
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasureAngle"
            Object.ToolTipText     =   "�Ѵ���"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasArea"
            Object.ToolTipText     =   "�Ѵ��鹷��"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasureDist"
            Object.ToolTipText     =   "�Ѵ���зҧ"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasureLen"
            Object.ToolTipText     =   "�Ѵ�������"
            ImageIndex      =   16
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasurePos"
            Object.ToolTipText     =   "���˹觨ش"
            ImageIndex      =   17
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComMeasureRoute"
            Object.ToolTipText     =   "�������зҧ"
            ImageIndex      =   18
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar Tb_Order 
      Height          =   360
      Left            =   12360
      TabIndex        =   31
      Top             =   0
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComBluePrint"
            Object.ToolTipText     =   "����������Ҥ��"
            ImageIndex      =   52
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPrj"
            Object.ToolTipText     =   "�к��ԡѴ����ҵ���Ѵ"
            ImageIndex      =   58
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SetColors"
            Object.ToolTipText     =   "��˹���"
            ImageIndex      =   61
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComSelect"
            Object.ToolTipText     =   "���Ң�����"
            ImageIndex      =   60
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComReplicate"
            Object.ToolTipText     =   "�Ѵ�͡�����Ţ������"
            ImageIndex      =   64
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar Tb_Select 
      Height          =   360
      Left            =   9150
      TabIndex        =   4
      Top             =   0
      Width           =   3225
      _ExtentX        =   5689
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComSelectSlide"
            Object.ToolTipText     =   "������͡����"
            ImageIndex      =   24
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComSelectAll"
            Object.ToolTipText     =   "���͡������"
            ImageIndex      =   25
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComDeselectAll"
            Object.ToolTipText     =   "¡��ԡ������͡"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComFence"
            Object.ToolTipText     =   "�ҧ����������"
            ImageIndex      =   27
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComFenceCircle"
            Object.ToolTipText     =   "�ҧ�����ǧ���"
            ImageIndex      =   28
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComListArea"
            Object.ToolTipText     =   "�ʴ�����ŧ���Թ"
            ImageIndex      =   53
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPrint"
            Object.ToolTipText     =   "�����Ἱ���"
            ImageIndex      =   30
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComExport"
            Object.ToolTipText     =   "���͡"
            ImageIndex      =   63
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar Tb_View 
      Height          =   360
      Left            =   2940
      TabIndex        =   1
      Top             =   0
      Width           =   3915
      _ExtentX        =   6906
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComZoomModeIn"
            Object.ToolTipText     =   "���¢�Ҵ"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComZoomModeOut"
            Object.ToolTipText     =   "Ŵ��Ҵ"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComRoamerMode"
            Object.ToolTipText     =   "��蹢���"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPanDrag"
            Object.ToolTipText     =   "����͹Ἱ���"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPanContinuous"
            Object.ToolTipText     =   "����͹Ἱ��������ͧ"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPan"
            Object.ToolTipText     =   "�֧Ἱ���"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComZoomAll"
            Object.ToolTipText     =   "���·�駢ͺࢵ"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComZoomExtent"
            Object.ToolTipText     =   "���·�����"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComZoomSelect"
            Object.ToolTipText     =   "���¾�蹷�����͡"
            ImageIndex      =   11
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComZoomToScale"
            Object.ToolTipText     =   "���µ���ҵ����ǹ"
            ImageIndex      =   12
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6705
      Left            =   0
      TabIndex        =   6
      Top             =   2310
      Width           =   3075
      _ExtentX        =   5424
      _ExtentY        =   11827
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      BackColor       =   14737632
      MouseIcon       =   "Frm_Map.frx":176E2
      TabCaption(0)   =   "  "
      TabPicture(0)   =   "Frm_Map.frx":176FE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvwOverlay"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "  "
      TabPicture(1)   =   "Frm_Map.frx":17C98
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Txt_Buffer"
      Tab(1).Control(1)=   "Grid_PROPERTY"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   " "
      TabPicture(2)   =   "Frm_Map.frx":18232
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Sis3"
      Tab(2).Control(1)=   "tvwTheme"
      Tab(2).Control(2)=   "Image1"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   " "
      TabPicture(3)   =   "Frm_Map.frx":187CC
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Cmd_Clear"
      Tab(3).Control(1)=   "cmdOK"
      Tab(3).Control(2)=   "fraSchema(3)"
      Tab(3).Control(3)=   "cboOverlay"
      Tab(3).Control(4)=   "fraSchema(0)"
      Tab(3).Control(5)=   "fraSchema(2)"
      Tab(3).Control(6)=   "fraSchema(1)"
      Tab(3).Control(7)=   "Label1(9)"
      Tab(3).Control(8)=   "Shape1(1)"
      Tab(3).ControlCount=   9
      TabCaption(4)   =   " "
      TabPicture(4)   =   "Frm_Map.frx":18D66
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "UpDown2"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Command1"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Chk_Preform3"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Chk_Payment3"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "Chk_Payment2"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "Chk_Preform2"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "Chk_Payment1"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "Chk_Preform1"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).Control(8)=   "Lb_Year"
      Tab(4).Control(8).Enabled=   0   'False
      Tab(4).Control(9)=   "Label2(0)"
      Tab(4).Control(9).Enabled=   0   'False
      Tab(4).Control(10)=   "Label1(15)"
      Tab(4).Control(10).Enabled=   0   'False
      Tab(4).Control(11)=   "Label1(14)"
      Tab(4).Control(11).Enabled=   0   'False
      Tab(4).Control(12)=   "Label1(13)"
      Tab(4).Control(12).Enabled=   0   'False
      Tab(4).Control(13)=   "Label1(11)"
      Tab(4).Control(13).Enabled=   0   'False
      Tab(4).Control(14)=   "Shape1(2)"
      Tab(4).Control(14).Enabled=   0   'False
      Tab(4).ControlCount=   15
      Begin VB.CommandButton Cmd_Clear 
         Caption         =   "��������"
         Height          =   330
         Left            =   -73020
         TabIndex        =   33
         Top             =   5895
         Width           =   870
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "����"
         Height          =   330
         Left            =   -73905
         TabIndex        =   32
         Top             =   5895
         Width           =   870
      End
      Begin VB.Frame fraSchema 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4875
         Index           =   3
         Left            =   -74910
         TabIndex        =   70
         Top             =   960
         Width           =   2760
         Begin VB.ComboBox cboOverlayFind_Pos 
            Height          =   315
            ItemData        =   "Frm_Map.frx":19300
            Left            =   150
            List            =   "Frm_Map.frx":19302
            Style           =   2  'Dropdown List
            TabIndex        =   75
            Top             =   4350
            Visible         =   0   'False
            Width           =   2490
         End
         Begin VB.ComboBox cboOverlayFind 
            Height          =   315
            ItemData        =   "Frm_Map.frx":19304
            Left            =   150
            List            =   "Frm_Map.frx":19306
            Style           =   2  'Dropdown List
            TabIndex        =   74
            Top             =   330
            Width           =   2490
         End
         Begin VB.ComboBox cboSchema_id 
            Height          =   315
            ItemData        =   "Frm_Map.frx":19308
            Left            =   120
            List            =   "Frm_Map.frx":1930A
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   3780
            Visible         =   0   'False
            Width           =   2490
         End
         Begin VB.ComboBox cboSchema 
            Height          =   315
            ItemData        =   "Frm_Map.frx":1930C
            Left            =   150
            List            =   "Frm_Map.frx":1930E
            Style           =   2  'Dropdown List
            TabIndex        =   72
            Top             =   1020
            Width           =   2490
         End
         Begin VB.TextBox txtSchema 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   150
            TabIndex        =   71
            Top             =   1800
            Width           =   2490
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "��Ң�����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   22
            Left            =   180
            TabIndex        =   78
            Top             =   1500
            Width           =   615
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "��������´������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   21
            Left            =   180
            TabIndex        =   77
            Top             =   750
            Width           =   1290
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "��鹢�����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   20
            Left            =   180
            TabIndex        =   76
            Top             =   60
            Width           =   675
         End
      End
      Begin SisLib.Sis Sis3 
         Height          =   3105
         Left            =   -74970
         TabIndex        =   68
         Top             =   3270
         Width           =   2865
         _Version        =   327680
         _ExtentX        =   5054
         _ExtentY        =   5477
         _StockProps     =   64
         BorderBevel     =   -1  'True
         Display         =   0
      End
      Begin MSComCtl2.UpDown UpDown2 
         Height          =   300
         Left            =   -73304
         TabIndex        =   53
         Top             =   630
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   529
         _Version        =   393216
         Value           =   2500
         BuddyControl    =   "Lb_Year"
         BuddyDispid     =   196638
         OrigLeft        =   2580
         OrigTop         =   720
         OrigRight       =   2835
         OrigBottom      =   1215
         Max             =   3500
         Min             =   2500
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComctlLib.TreeView tvwTheme 
         Height          =   3195
         Left            =   -74970
         TabIndex        =   56
         Top             =   30
         Width           =   2880
         _ExtentX        =   5080
         _ExtentY        =   5636
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   619
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
      Begin VB.TextBox Txt_Buffer 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   225
         Left            =   -73890
         TabIndex        =   58
         Top             =   810
         Visible         =   0   'False
         Width           =   1255
      End
      Begin VB.PictureBox Image1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   120
         Left            =   -74970
         MousePointer    =   7  'Size N S
         ScaleHeight     =   120
         ScaleWidth      =   2835
         TabIndex        =   55
         Top             =   3210
         Width           =   2835
      End
      Begin MSComctlLib.TreeView tvwOverlay 
         Height          =   6285
         Left            =   30
         TabIndex        =   7
         Top             =   30
         Width           =   2880
         _ExtentX        =   5080
         _ExtentY        =   11086
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   619
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         Height          =   315
         Left            =   -72450
         Picture         =   "Frm_Map.frx":19310
         Style           =   1  'Graphical
         TabIndex        =   52
         ToolTipText     =   "�ʴ�����Ҵ����"
         Top             =   90
         Width           =   315
      End
      Begin VB.CheckBox Chk_Preform3 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż�������Ẻ"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   51
         Top             =   5220
         Width           =   1995
      End
      Begin VB.CheckBox Chk_Payment3 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż���Ҫ�������"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   50
         Top             =   5700
         Width           =   2025
      End
      Begin VB.CheckBox Chk_Payment2 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż���Ҫ�������"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   49
         Top             =   3990
         Width           =   2025
      End
      Begin VB.CheckBox Chk_Preform2 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż�������Ẻ"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   48
         Top             =   3510
         Width           =   1995
      End
      Begin VB.CheckBox Chk_Payment1 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż���Ҫ�������"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   47
         Top             =   2220
         Width           =   2025
      End
      Begin VB.CheckBox Chk_Preform1 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "�ʴ������ż�������Ẻ"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74520
         TabIndex        =   46
         Top             =   1740
         Width           =   1995
      End
      Begin VB.ComboBox cboOverlay 
         Height          =   315
         ItemData        =   "Frm_Map.frx":1989A
         Left            =   -74775
         List            =   "Frm_Map.frx":198AA
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   540
         Width           =   2490
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_PROPERTY 
         Height          =   6315
         Left            =   -74970
         TabIndex        =   54
         Top             =   30
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   11139
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         GridColor       =   8421504
         GridColorFixed  =   0
         GridLinesFixed  =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.Frame fraSchema 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4875
         Index           =   0
         Left            =   -74910
         TabIndex        =   10
         Top             =   945
         Width           =   2760
         Begin VB.TextBox txtLand 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   0
            Left            =   1230
            TabIndex        =   15
            Top             =   630
            Width           =   1320
         End
         Begin VB.TextBox txtLand 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   1
            Left            =   1230
            TabIndex        =   14
            Top             =   990
            Width           =   1320
         End
         Begin VB.TextBox txtLand 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   2
            Left            =   1230
            TabIndex        =   13
            Top             =   1350
            Width           =   1320
         End
         Begin VB.TextBox txtLand 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   3
            Left            =   1230
            TabIndex        =   12
            Top             =   1710
            Width           =   1320
         End
         Begin VB.TextBox txtLand 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   4
            Left            =   1230
            TabIndex        =   11
            Top             =   2070
            Width           =   1320
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�͡����Է���"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   1
            Left            =   315
            TabIndex        =   0
            Top             =   765
            Width           =   810
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ���Թ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   2
            Left            =   540
            TabIndex        =   19
            Top             =   1815
            Width           =   585
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ���ҧ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   3
            Left            =   465
            TabIndex        =   18
            Top             =   1095
            Width           =   660
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "˹�����Ǩ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   4
            Left            =   390
            TabIndex        =   17
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "���ʷ��Թ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   5
            Left            =   510
            TabIndex        =   16
            Top             =   2175
            Width           =   615
         End
      End
      Begin VB.Frame fraSchema 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   4875
         Index           =   2
         Left            =   -74910
         TabIndex        =   27
         Top             =   945
         Visible         =   0   'False
         Width           =   2760
         Begin VB.ListBox Lst_Details 
            BackColor       =   &H00FFFFFF&
            Height          =   2985
            Index           =   1
            Left            =   30
            TabIndex        =   61
            Top             =   1860
            Width           =   2715
         End
         Begin VB.TextBox txtSignboard 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   1
            Left            =   1095
            TabIndex        =   59
            Top             =   390
            Width           =   1410
         End
         Begin VB.TextBox txtSignboard 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   0
            Left            =   1095
            TabIndex        =   28
            Top             =   765
            Width           =   1410
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00808080&
            Caption         =   "��¡�����ʻ���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Index           =   17
            Left            =   30
            TabIndex        =   62
            Top             =   1530
            Width           =   2700
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "���ʷ��Թ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   16
            Left            =   405
            TabIndex        =   60
            Top             =   480
            Width           =   615
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "���ʻ���"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   450
            TabIndex        =   29
            Top             =   855
            Width           =   570
         End
      End
      Begin VB.Frame fraSchema 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   4830
         Index           =   1
         Left            =   -74910
         TabIndex        =   20
         Top             =   945
         Visible         =   0   'False
         Width           =   2760
         Begin VB.TextBox txtBuilding 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   3
            Left            =   1245
            TabIndex        =   65
            Top             =   120
            Width           =   1275
         End
         Begin VB.ListBox Lst_Details 
            BackColor       =   &H00FFFFFF&
            Height          =   2790
            Index           =   2
            Left            =   30
            TabIndex        =   63
            Top             =   1980
            Width           =   2715
         End
         Begin VB.TextBox txtBuilding 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   0
            Left            =   1245
            TabIndex        =   23
            Top             =   480
            Width           =   1275
         End
         Begin VB.TextBox txtBuilding 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   1
            Left            =   1245
            TabIndex        =   22
            Top             =   840
            Width           =   1275
         End
         Begin VB.TextBox txtBuilding 
            Appearance      =   0  'Flat
            Height          =   330
            Index           =   2
            Left            =   1245
            TabIndex        =   21
            Top             =   1200
            Width           =   1275
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "���ʷ��Թ"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   19
            Left            =   540
            TabIndex        =   57
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00808080&
            Caption         =   "��¡�������ç���͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Index           =   18
            Left            =   30
            TabIndex        =   64
            Top             =   1650
            Width           =   2700
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "��ҹ�Ţ���"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   6
            Left            =   495
            TabIndex        =   26
            Top             =   585
            Width           =   660
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "�����ç���͹"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   7
            Left            =   255
            TabIndex        =   25
            Top             =   960
            Width           =   900
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "������"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   8
            Left            =   795
            TabIndex        =   24
            Top             =   1320
            Width           =   315
         End
      End
      Begin VB.Label Lb_Year 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "2547"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00A6EEFD&
         Height          =   300
         Left            =   -73860
         TabIndex        =   45
         Top             =   630
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Шӻ� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   0
         Left            =   -74550
         TabIndex        =   44
         Top             =   690
         Width           =   645
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "�ʴ���ػ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00A6EEFD&
         Height          =   330
         Index           =   15
         Left            =   -74910
         TabIndex        =   43
         Top             =   4620
         Width           =   2760
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "��ػ�ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00A6EEFD&
         Height          =   330
         Index           =   14
         Left            =   -74910
         TabIndex        =   42
         Top             =   2850
         Width           =   2760
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "�ʴ���ػ�ŧ���Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00A6EEFD&
         Height          =   330
         Index           =   13
         Left            =   -74910
         TabIndex        =   41
         Top             =   1140
         Width           =   2760
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00404040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "�ʴ���ػ����������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00A6EEFD&
         Height          =   390
         Index           =   11
         Left            =   -74940
         TabIndex        =   40
         Top             =   60
         Width           =   2850
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00808080&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         BorderStyle     =   2  'Dash
         BorderWidth     =   2
         DrawMode        =   4  'Mask Not Pen
         FillColor       =   &H000000FF&
         Height          =   5865
         Index           =   2
         Left            =   -74940
         Top             =   450
         Width           =   2835
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         BackStyle       =   0  'Transparent
         Caption         =   "���͡��������ä���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   300
         Index           =   9
         Left            =   -74790
         TabIndex        =   30
         Top             =   225
         Width           =   2520
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00808080&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         Height          =   6285
         Index           =   1
         Left            =   -74955
         Top             =   45
         Width           =   2835
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   1350
      Left            =   14880
      TabIndex        =   8
      Top             =   7575
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   2381
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPasteFrom"
            Object.ToolTipText     =   "PasteFrom"
            ImageIndex      =   51
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComCreateArea"
            Object.ToolTipText     =   "��˹���鹷���ŧ���Թ"
            ImageIndex      =   50
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComDisplace"
            Object.ToolTipText     =   "Displacement"
            ImageIndex      =   54
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComRubberSheet"
            Object.ToolTipText     =   "Rubber Sheet"
            ImageIndex      =   55
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar Tb_Draw 
      Height          =   3660
      Left            =   14880
      TabIndex        =   5
      Top             =   4140
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   6456
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComPoint"
            Object.ToolTipText     =   "�Ҵ�ش"
            ImageIndex      =   19
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComLineEx"
            Object.ToolTipText     =   "�Ҵ���"
            ImageIndex      =   20
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComFreeHand"
            Object.ToolTipText     =   "�Ҵ��������"
            ImageIndex      =   45
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComAreaEx"
            Object.ToolTipText     =   "���ҧ��鹷��Դ"
            ImageIndex      =   21
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComRect"
            Object.ToolTipText     =   "���ҧ��鹷������������"
            ImageIndex      =   22
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComEllipse"
            Object.ToolTipText     =   "���ҧ��鹷��ǧ���"
            ImageIndex      =   23
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComBoxText"
            Object.ToolTipText     =   "���ҧ����ѡ��"
            ImageIndex      =   47
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComText"
            Object.ToolTipText     =   "���ҧ����ѡ�èҡ�ش"
            ImageIndex      =   48
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComBoxLabel"
            Object.ToolTipText     =   "���ҧ����ѡ�èҡ���"
            ImageIndex      =   49
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar Tb_File 
      Height          =   4650
      Left            =   14880
      TabIndex        =   3
      Top             =   0
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   8202
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   14
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComFileOpen"
            Object.ToolTipText     =   "�Դ���Ἱ���"
            ImageIndex      =   31
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComFileSave"
            Object.ToolTipText     =   "�ѹ�֡���Ἱ���"
            ImageIndex      =   32
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComCut"
            Object.ToolTipText     =   "�Ѵ"
            ImageIndex      =   33
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComCopy"
            Object.ToolTipText     =   "�Ѵ�͡"
            ImageIndex      =   34
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComPaste"
            Object.ToolTipText     =   "�ҧ"
            ImageIndex      =   35
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComDelete"
            Object.ToolTipText     =   "ź"
            ImageIndex      =   36
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComUndo"
            Object.ToolTipText     =   "��¡�Ѻ"
            ImageIndex      =   37
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "AComRedo"
            Object.ToolTipText     =   "�˹��"
            ImageIndex      =   38
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComLayers"
            Object.ToolTipText     =   "�Ǻ�����鹢�����"
            ImageIndex      =   39
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComAddOverlay"
            Object.ToolTipText     =   "������鹢�����"
            ImageIndex      =   40
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AComAddTheme"
            Object.ToolTipText     =   "�����������س�ѡɳ�"
            ImageIndex      =   41
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin SisLib.Sis Sis2 
      Height          =   2295
      Left            =   0
      TabIndex        =   67
      Top             =   0
      Width           =   2925
      _Version        =   327680
      _ExtentX        =   5159
      _ExtentY        =   4048
      _StockProps     =   64
      BorderBevel     =   -1  'True
      Display         =   0
   End
End
Attribute VB_Name = "Frm_Map"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Tb_ViewOldPush As Byte
Dim x1_Index  As Double, y1_Index As Double, z1_Index As Double, x2_Index As Double, y2_Index As Double, z2_Index As Double
Dim RubberX1 As Double, RubberY1 As Double
Dim Drag As Boolean
Dim xPush As Single, yPush As Single
Dim MouseDown As Byte
Dim Txt_Buffer2 As String

Private Sub UpdateUndo_Redo()
              Tb_File.Buttons.Item(9).Enabled = Sis1.CanDoCommand("AComUndo")
              Tb_File.Buttons.Item(10).Enabled = Sis1.CanDoCommand("AComRedo")
End Sub

Private Sub Get_PropertyObject(Logic As Boolean)
 Dim i As Integer, j As Integer
 Dim strFormula As String
'DIM N As Integer, m As Integer, sum As Integer
'Dim Package_Attrib  As String
     Txt_Buffer.Tag = ""
     Sis1.OpenSel 0
With Grid_PROPERTY
   If Logic Then
            .Rows = 2
'            .TextMatrix(1, 1) = Sis1.GetInt(SIS_OT_CURITEM, 0, "_id&")
'            .TextMatrix(2, 1) = Sis1.GetStr(SIS_OT_CURITEM, 0, "_classLocal$")
'            If Sis1.GetStr(SIS_OT_CURITEM, 0, "_layer$") = Empty Then .TextMatrix(3, 1) = "(anonymous)"
'                .TextMatrix(4, 1) = Sis1.GetInt(SIS_OT_CURITEM, 0, "_level&")
'                .TextMatrix(5, 1) = Sis1.GetStr(SIS_OT_CURITEM, 0, "_DESC$")
'                .TextMatrix(6, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_area#")
'                .TextMatrix(7, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_length#")
'                .TextMatrix(1, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_oLat#")
'                .TextMatrix(2, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_oLon#")
'                .TextMatrix(3, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_ox#")
'                .TextMatrix(4, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_oy#")
'                .TextMatrix(12, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_oz#")
'                .TextMatrix(13, 1) = Sis1.GetInt(SIS_OT_CURITEM, 0, "_point_height&")
'                .TextMatrix(14, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_scale#")
'            If Sis1.GetStr(SIS_OT_CURITEM, 0, "_shape$") = Empty Then .TextMatrix(15, 1) = " (By Overlay)"
'                .TextMatrix(16, 1) = Sis1.GetStr(SIS_OT_CURITEM, 0, "_dataset$")
'                    Debug.Print Sis1.GetStr(SIS_OT_CURITEM, 0, "_attributes$")
'                    Debug.Print Sis1.GetListItemStr("Test", 0, "_properties_edit$")
'                    For i = 0 To Sis1.GetInt(SIS_OT_WINDOW, 0, "_nOverlay&") - 1
                            i = Sis1.FindDatasetOverlay(Sis1.GetDataset, -1, True)
'                            If Sis1.GetStr(SIS_OT_DATASET, Sis1.GetInt(SIS_OT_OVERLAY, i, "_nDataset&"), "_name$") = Sis1.GetStr(SIS_OT_CURITEM, 0, "_dataset$") Then
'                                    If Sis1.GetNumSel = 1 Then
                                                With Grid_PROPERTY
                                                        Sis1.GetOverlaySchema i, "Schema1"
                                                        Sis1.LoadSchema "Schema1"
                                                        For j = 0 To Sis1.GetInt(SIS_OT_SCHEMA, 0, "_nColumns&") - 1
                                                                If Sis1.GetInt(SIS_OT_SCHEMACOLUMN, j, "_bHidden&") = 0 Then
                                                                        .Rows = .Rows + 1
                                                                        .Row = .Rows - 2
                                                                        .TextMatrix(.Row, 0) = Sis1.GetStr(SIS_OT_SCHEMACOLUMN, j, "_formula$")          '�� Col = 2 �� Col = 0
                                                                        .TextMatrix(.Row, 1) = Sis1.GetStr(SIS_OT_SCHEMACOLUMN, j, "_description$")   '�� Col = 0 �� Col = 1
        '                                                                Debug.Print Sis1.GetStr(SIS_OT_SCHEMACOLUMN, j, "_bModified&")
        '                                                                If InStr(1, Grid_PROPERTY.Tag, .TextMatrix(.Row, 2), vbTextCompare) <> 0 Then
'                                                                                .Col = 0
'                                                                                .CellBackColor = &HFFFFFF
'                                                                                .Col = 1
'                                                                                .CellBackColor = &HFFFFFF
        '                                                                Else
        '                                                                        .Col = 0
        '                                                                        .CellBackColor = &HC0C0C0
        '                                                                        .Col = 1
        '                                                                        .CellBackColor = &HC0C0C0
        '                                                                End If
                                                                        
                                                                        strFormula = Sis1.GetStr(SIS_OT_SCHEMACOLUMN, j, "_formula$")
                                                                        '�� Col = 1 �� Col = 2
                                                                        If InStr(Right$(strFormula, 1), "&") <> 0 Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateInt(SIS_OT_CURITEM, 0, strFormula)
                                                                        ElseIf InStr(Right$(strFormula, 1), "$") <> 0 Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateStr(SIS_OT_CURITEM, 0, strFormula)
                                                                        ElseIf InStr(Right$(strFormula, 1), "#") <> 0 Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, strFormula)
                                                                        ElseIf Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, strFormula) <> 0 Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, strFormula)
                                                                        ElseIf Sis1.EvaluateInt(SIS_OT_CURITEM, 0, strFormula) <> 0 Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateInt(SIS_OT_CURITEM, 0, strFormula)
                                                                        ElseIf Sis1.EvaluateStr(SIS_OT_CURITEM, 0, strFormula) <> "" Then
                                                                                .TextMatrix(.Row, 2) = Sis1.EvaluateStr(SIS_OT_CURITEM, 0, strFormula)
                                                                        Else
                                                                                .TextMatrix(.Row, 2) = ""
                                                                        End If
                                                                End If
                                                        Next j
                                                End With
'                                    End If
'                                    Exit For
'                            End If
'                    Next i
                .Rows = .Rows - 1
                
                    'Sis1.CreateListFromSelection "SelectedItems"
                    'Sis1.OpenList "SelectedItems", 0
                    '    Package_Attrib = Sis1.GetStr(SIS_OT_CURITEM, 0, "_attributes$")
                     ' i = 1
                      ' sum = 0
        'While InStr(i, Package_Attrib, " ", vbTextCompare) <> 0
        '        N = InStr(i, Package_Attrib, " ", vbTextCompare)
        '        m = Abs(sum - N)
        '         .Rows = .Rows + 1
        '         .Row = .Rows - 1
        '         .Col = 0
        '         .CellBackColor = &HFFFFFF
        '         .Col = 1
        '         .CellBackColor = &HFFFFFF
        '        .TextMatrix(.Rows - 1, 0) = Trim$(Mid$(Package_Attrib, i, m))
        '        Select Case Right$(Trim$(Mid$(Package_Attrib, i, m)), 1)
        '                     Case "$"
        '                            .TextMatrix(.Rows - 1, 1) = Sis1.GetStr(SIS_OT_CURITEM, 0, Trim$(.TextMatrix(.Rows - 1, 0)))
        '                     Case "#"
        '                             .TextMatrix(.Rows - 1, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, Trim$(.TextMatrix(.Rows - 1, 0)))
        '                     Case "&"
        '                           .TextMatrix(.Rows - 1, 1) = Sis1.GetInt(SIS_OT_CURITEM, 0, Trim$(.TextMatrix(.Rows - 1, 0)))
        '          End Select
        '                .TextMatrix(.Rows - 1, 2) = .TextMatrix(.Rows - 1, 0)
        '                .TextMatrix(.Rows - 1, 0) = Left$(Trim$(.TextMatrix(.Rows - 1, 0)), Len(Trim$(.TextMatrix(.Rows - 1, 0))) - 1)
        '        i = N + 1
        '        sum = N
        ' Wend
        '        .TopRow = .Rows - 1
    Else
                    For i = 1 To .Rows - 1
                           .TextMatrix(i, 2) = Empty    '�� Col = 1 �� Col = 2
                    Next i
    End If
     .ColAlignment(2) = 1
   End With
End Sub

Private Sub Create_TableProperty()

   With Grid_PROPERTY
             .ColWidth(1) = 1200: .ColWidth(2) = 1580: .ColWidth(0) = 1
            .TextArray(1) = "��������´": .TextArray(2) = "��Ң�����": .ColAlignmentFixed(2) = 4
'            .TextMatrix(1, 0) = "�ӴѺ�ѵ��"
'            .TextMatrix(2, 0) = "�������ѵ��"
'            .TextMatrix(3, 0) = "�������鹢�����"
'            .TextMatrix(4, 0) = "�дѺ"
'            .TextMatrix(5, 0) = "��������´"
'            .TextMatrix(6, 0) = "��Ҵ��鹷��"
'            .TextMatrix(7, 0) = "�������"
'            .TextMatrix(1, 0) = "�еԨٴ"
'            .TextMatrix(2, 0) = "�ͧ�Ԩٴ"
'            .TextMatrix(3, 0) = "�ԡѴ�ǹ͹"
'            .TextMatrix(4, 0) = "�ԡѴ�ǵ��"
'            .TextMatrix(12, 0) = "�ԡѴ�Ǵ��"
'            .TextMatrix(13, 0) = "�ش�����٧"
'            .TextMatrix(14, 0) = "�ҵ����ǹ"
'            .TextMatrix(15, 0) = "�ѡɳ�"
'            .TextMatrix(16, 0) = "��������"
            .ColAlignment(2) = flexAlignLeftBottom
'            .ColAlignment(1) = flexAlignLeftBottom
   End With
End Sub

Private Sub Get_Schema()
        Dim j As Integer
        cboSchema_id.Clear
        cboSchema.Clear
        With Sis1
                .GetOverlaySchema cboOverlayFind_Pos.Text, "Schema1"
                .LoadSchema "Schema1"
                For j = 0 To .GetInt(SIS_OT_SCHEMA, 0, "_nColumns&") - 1
                        cboSchema_id.AddItem .GetStr(SIS_OT_SCHEMACOLUMN, j, "_formula$")
                        cboSchema.AddItem .GetStr(SIS_OT_SCHEMACOLUMN, j, "_description$")
                Next j
                cboSchema.ListIndex = 0
        End With
End Sub

Private Sub Push_ToolBar(INDEXS As Byte, TypeToolBar As Byte)
On Error Resume Next
       Tb_View.Buttons(Tb_ViewOldPush).Enabled = True
       Tb_Mesure.Buttons(Tb_ViewOldPush).Enabled = True
       Tb_Select.Buttons(Tb_ViewOldPush).Enabled = True
       Tb_File.Buttons(Tb_ViewOldPush).Enabled = True
       
       Tb_Draw.Buttons(Tb_ViewOldPush).Enabled = True

Select Case TypeToolBar
            Case 1
                    Tb_View.Buttons(INDEXS).Enabled = False
            Case 2
                    Tb_Mesure.Buttons(INDEXS).Enabled = False
            Case 3
                    Tb_Select.Buttons(INDEXS).Enabled = False
            Case 4
                    Tb_File.Buttons(INDEXS).Enabled = False
            Case 5
                    Tb_Draw.Buttons(INDEXS).Enabled = False
 End Select
       Tb_ViewOldPush = INDEXS
End Sub

Private Sub UnPush_ToolBar(INDEXS As Byte, TypeToolBar As Byte)
On Error Resume Next
Select Case TypeToolBar
            Case 1
                    Tb_View.Buttons(Tb_ViewOldPush).Enabled = True
            Case 2
                    Tb_Mesure.Buttons(Tb_ViewOldPush).Enabled = True
            Case 3
                    Tb_Select.Buttons(Tb_ViewOldPush).Enabled = True
            Case 4
                    Tb_File.Buttons(Tb_ViewOldPush).Enabled = True
            Case 5
                    Tb_Draw.Buttons(Tb_ViewOldPush).Enabled = True
End Select
       Tb_ViewOldPush = 0
End Sub

Public Sub CLEAR_CHECK()
        With Clone_Form
                .mnuEditable.Checked = False
                .mnuHittable.Checked = False
                .mnuVisible.Checked = False
                .mnuInvisible.Checked = False
        End With
End Sub

Private Sub Openswd()
   Dim strLocalNm As String
    strLocalNm = Frm_Logo.ChkLocalNm
    
If TProduct <> "DEMO" And TProduct <> "MODELLER" Then
        If Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date) < CDate("01/01/2007") Then  ' For user have Hard key lock
                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
                Exit Sub
        End If
ElseIf TProduct = "DEMO" Then
        If Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date) < CDate("01/11/2004") Then      ' Or  CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("11/01/2004")
                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
                End
        End If
ElseIf TProduct = "MODELLER" And strLocalNm <> "���ͺ" Then
        If Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date) < CDate("01/01/2007") Then  ' For user have Hard key lock
                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
                Exit Sub
        End If
ElseIf TProduct = "MODELLER" And strLocalNm = "���ͺ" Then
        If Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date) < CDate("01/11/2004") Then      ' Or  CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("11/01/2004")
                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
                End
        End If
End If

'If TProduct <> "DEMO" Then
'        If CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("01/01/2007") Then  ' For user have Hard key lock
'                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
'                Exit Sub
'        End If
'ElseIf TProduct = "DEMO" Then
'        If CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("01/11/2004") Then      ' Or  CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("11/01/2004")
'                MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
'                End
'        End If
'End If

'If CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("01/11/2004") Then     ' Or  CDate(Format$(Day(Date), "#0") & "/" & Format(Month(Date), "0#") & "/" & Year(Date)) < CDate("11/01/2004")
'        MsgBox "SIS Spatial Information System Date Found !", vbExclamation, "Spatial Information System"
'        Exit Sub
'End If

If swdFile = "" Then Exit Sub
With Sis1
                 Select Case TProduct
                        Case "DEMO"
                                      .Level = SIS_LEVEL_MODELLER
                        Case "VIEWER"
                                      .Level = SIS_LEVEL_VIEWER
                        Case "MANAGER"
                                      .Level = SIS_LEVEL_MANAGER
                        Case "MODELLER"
                                      .Level = SIS_LEVEL_MODELLER
                 End Select
                .LoadSwd (swdFile)
                .CreateClassTreeFilter "LinesAndPolys", "-Item +Line +MultiLine +Area +MultiArea"
'               .AddCommand "Length", "", "Item", -1, 1, "LinesAndPolys", ""
                .AddCommand "Open Link", "", "Item", -1, 1, "LinesAndPolys", ""
'               .AddCommand "Area", "", "Item", -1, 1, "LinesAndPolys", ""
                .NolInsert App.Path & "\NOL.nol", 1, True
                .AllowCommands SIS_COM_ADD, "AComRubberExplode AComMakeRubber AComRubberSetCurrent"
                .SetInt SIS_OT_OPTION, 0, "_bSelectOverlapAutomatic&", False
                .SetInt SIS_OT_OPTION, 0, "_bShowMapTips&", True
                .SetInt SIS_OT_OPTION, 0, "_bFlickerDisplay&", True
                .SetInt SIS_OT_OPTION, 0, "_HotSnap&", 3
                .TrackMouse = True
                .ZoomExtent
End With
With Sis2
                .Level = Sis1.Level
                 .Swd = Sis1.Swd
                .SetInt SIS_OT_WINDOW, 0, "_bScrollBars&", False
                .SetInt SIS_OT_WINDOW, 0, "_bKeyboardMove&", False
                .SetInt SIS_OT_WINDOW, 0, "_bKeyboardSnap&", False
                .SetInt SIS_OT_WINDOW, 0, "_bMouseMove&", False
                ' .SetInt SIS_OT_OPTION, 0, "_bFlickerDisplay&", False
                 '.AllowCommands SIS_COM_NONE, ""
                .TrackMouse = False
                .ZoomExtent
End With
With Sis3
        .TrackMouse = True
        .SetInt SIS_OT_WINDOW, 0, "_bScrollBars&", False
        .SetInt SIS_OT_WINDOW, 0, "_bKeyboardMove&", False
        .SetInt SIS_OT_WINDOW, 0, "_bKeyboardSnap&", False
        .SetInt SIS_OT_WINDOW, 0, "_bMouseMove&", False
End With
        Sis1.Tag = "TRUE"
        Call Get_Overay
        Sis1.Activate
        Call Create_ViewDB_Cadcorp
End Sub

Private Sub CHECK_STATUSLAYER(Optional Overlay As Byte)
                    Call CLEAR_CHECK
       Select Case Frm_Map.Sis1.GetInt(SIS_OT_OVERLAY, CLng(Overlay), "_status&")
                     Case 3
                           Clone_Form.mnuEditable.Checked = True
                     Case 2
                           Clone_Form.mnuHittable.Checked = True
                     Case 1
                           Clone_Form.mnuVisible.Checked = True
                     Case 0
                           Clone_Form.mnuInvisible.Checked = True
        End Select
End Sub

Public Sub Get_Overay()
    Dim totalOverlay As Integer, noverlay As Integer, numTheme As Integer, nTheme As Integer
    Dim strNameOverlay As String
'    Dim newNode As Node
'    Dim BufferTxt As String
    Dim i As Integer
    Dim arrayLayer As Variant
    
    tvwOverlay.Nodes.Clear
    tvwOverlay.ImageList = ImageList1
    'Set imgX = ImageList1.ListImages.Add(, , ImageList1)
    tvwTheme.Nodes.Clear
    tvwTheme.ImageList = ImageList1
   
    Dim nodX As Node   ' Create a tree.
    Dim nodXThm As Node   ' Create a tree.
   
    With Sis1
            totalOverlay = .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")    '�ӹǹ��� Overlay
            If totalOverlay = 0 Then Exit Sub
            For noverlay = 0 To totalOverlay - 1   '������§ index ��� Overlay
                    strNameOverlay = .GetStr(SIS_OT_OVERLAY, noverlay, "_name$")
                    Sis2.Tag = .GetStr(SIS_OT_OVERLAY, noverlay, "_notes$")
                    Sis3.Tag = .GetInt(SIS_OT_OVERLAY, noverlay, "_status&")
                    Set nodX = tvwOverlay.Nodes.Add(, , "O" & noverlay, strNameOverlay, 2)
                    Set nodXThm = tvwTheme.Nodes.Add(, , "O" & noverlay, strNameOverlay, 2)
                    If .Tag = "TRUE" And UCase(Sis2.Tag) = "MICROTAX" And Sis3.Tag <> SIS_INVISIBLE Then
                            .SetInt SIS_OT_OVERLAY, noverlay, "_status&", SIS_VISIBLE
                    End If
                    Select Case Sis3.Tag
                            Case SIS_EDITABLE
                                    nodX.Checked = True
                                    nodX.Image = 1
                                    nodXThm.Checked = True
                                    nodXThm.Image = 1
                            Case SIS_HITTABLE, SIS_VISIBLE
                                    nodX.Checked = True
                                    nodX.Image = 1
                                    nodXThm.Checked = True
                                    nodXThm.Image = 1
                            Case SIS_INVISIBLE
                                    nodX.Checked = False
                                    nodXThm.Image = 2
                                    nodXThm.Checked = False
                                    nodX.Image = 2
                    End Select
'                    If .Tag = "TRUE" Then
'                            .CreatePropertyFilter "Overlay1", "_classLocal$ = 'Bitmap'"
'                            If .ScanOverlay("selectitem", noverlay, "Overlay1", "") > 0 Then
'                                    .SetInt SIS_OT_OVERLAY, noverlay, "_status&", SIS_VISIBLE
'                            End If
'                            .Tag = "FALSE"
'                    End If
            Next
             .Tag = "FALSE"
             
'    For noverlay = 0 To totalOverlay - 1 '������§ index �ͧ Treeview ��� layer
'            BufferTxt = Sis1.GetStr(SIS_OT_DATASET, Sis1.GetInt(SIS_OT_OVERLAY, noverlay, "_nDataset&"), "_layers$")
'            n = 1
'            If BufferTxt <> Empty Then
'                    Do While InStr(n, BufferTxt, Chr$(9), vbBinaryCompare) <> 0
'                            Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , Mid$(BufferTxt, n, InStr(n, BufferTxt, Chr$(9), vbBinaryCompare) - 1), 56)
'                            n = InStr(n, BufferTxt, Chr$(9), vbBinaryCompare)
'                            BufferTxt = Mid$(BufferTxt, n + 1)
'                            n = 1
'                            nodX.Checked = True
'                    Loop
'                    Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , Mid$(BufferTxt, n), 56)
'                    nodX.Checked = True
'            End If
'    Next

    For noverlay = 0 To totalOverlay - 1 '������§ index �ͧ Treeview ��� layer
'            BufferTxt = Sis1.GetStr(SIS_OT_DATASET, Sis1.GetInt(SIS_OT_OVERLAY, noverlay, "_nDataset&"), "_layers$")
            arrayLayer = Split(Sis1.GetStr(SIS_OT_DATASET, Sis1.GetInt(SIS_OT_OVERLAY, noverlay, "_nDataset&"), "_layers$"), Chr$(9), , vbBinaryCompare)
            If UBound(arrayLayer) > -1 Then
                    For i = 0 To UBound(arrayLayer)
'                            Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , arrayLayer(i), 56)
'                            nodX.Checked = True
                            Sis1.CreatePropertyFilter "Overlay1", "_layer$ = '" & arrayLayer(i) & "'"
                            If Sis1.ScanOverlay("Test1", noverlay, "Overlay1", "") > 0 Then
                                    Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , arrayLayer(i), 56)
                                    nodX.Checked = True
                            Else
                                    Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , arrayLayer(i), 57)
                                    nodX.Checked = False
                            End If
                            Sis1.EmptyList "Test1"
                    Next i
            End If
            Erase arrayLayer
            numTheme = .GetInt(SIS_OT_OVERLAY, noverlay, "_nTheme&")
            For nTheme = 0 To numTheme - 1
                    .GetOverlayTheme noverlay, "EMPTY", nTheme
                    .LoadTheme "EMPTY"
                    Set nodXThm = tvwTheme.Nodes.Add("O" & noverlay, tvwChild, "O" & noverlay & "-" & nTheme, .GetStr(SIS_OT_THEME, 0, "_title$"))
                            nodXThm.Checked = True
                            nodXThm.ForeColor = &H4080&
                            nodXThm.Image = 46
                    If .GetInt(SIS_OT_THEME, 0, "_bDisabled&") = True Then
                            nodXThm.Checked = False
'                            MsgBox .IsOverlayThemeEnabled(noverlay, nTheme)
                    End If
            Next
    Next
'            If BufferTxt <> Empty Then
'                    Do While InStr(n, BufferTxt, Chr$(9), vbBinaryCompare) <> 0
'                            Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , Mid$(BufferTxt, n, InStr(n, BufferTxt, Chr$(9), vbBinaryCompare) - 1), 56)
'                            n = InStr(n, BufferTxt, Chr$(9), vbBinaryCompare)
'                            BufferTxt = Mid$(BufferTxt, n + 1)
'                            n = 1
'                            nodX.Checked = True
'                    Loop
'                    Set nodX = tvwOverlay.Nodes.Add("O" & noverlay, tvwChild, , Mid$(BufferTxt, n), 56)
'                    nodX.Checked = True
'            End If
'    For noverlay = 0 To totalOverlay - 1  '���§ Theme 㹪�� Overlay ��ҧ�
'            numTheme = .GetInt(SIS_OT_OVERLAY, noverlay, "_nTheme&")
'            For nTheme = 0 To numTheme - 1
'                    .GetOverlayTheme noverlay, "EMPTY", nTheme
'                    .LoadTheme "EMPTY"
'                    Set nodXThm = tvwTheme.Nodes.Add("O" & noverlay, tvwChild, "O" & noverlay & "-" & nTheme, .GetStr(SIS_OT_THEME, 0, "_title$"))
'                            nodXThm.Checked = True
'                            nodXThm.ForeColor = &H4080&
'                            nodXThm.Image = 46
'                    If .GetInt(SIS_OT_THEME, 0, "_bDisabled&") = True Then
'                            nodXThm.Checked = False
'                    End If
'            Next
'   Next
End With
End Sub

Private Sub cboOverlay_Click()
        Select Case cboOverlay.ListIndex
                Case 0
                    fraSchema(0).Visible = True
                    fraSchema(1).Visible = False
                    fraSchema(2).Visible = False
                    fraSchema(3).Visible = False
                    'optLand(0).Value = True
                Case 1
                    fraSchema(0).Visible = False
                    fraSchema(1).Visible = True
                    fraSchema(2).Visible = False
                    fraSchema(3).Visible = False
                    'optBuilding(0).Value = True
                Case 2
                    fraSchema(0).Visible = False
                    fraSchema(1).Visible = False
                    fraSchema(2).Visible = True
                    fraSchema(3).Visible = False
                    'optSignboard(0).Value = True
                Case 3
                    fraSchema(0).Visible = False
                    fraSchema(1).Visible = False
                    fraSchema(2).Visible = False
                    fraSchema(3).Visible = True
                    Call Get_OverlayFind
        End Select
End Sub

Private Sub Get_OverlayFind()
        Dim totalOverlay As Integer, i As Integer
        cboOverlayFind.Clear
        cboOverlayFind_Pos.Clear
        With Sis1
            totalOverlay = .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
            For i = 0 To totalOverlay - 1   '������§ index ��� Overlay
'                    cboOverlayFind.AddItem .GetStr(SIS_OT_OVERLAY, i, "_name$")
                    If .GetInt(SIS_OT_OVERLAY, i, "_status&") <> SIS_INVISIBLE Then
                            cboOverlayFind.AddItem .GetStr(SIS_OT_OVERLAY, i, "_name$")
                            cboOverlayFind_Pos.AddItem i
                    End If
            Next i
            cboOverlayFind.ListIndex = 0
        End With
End Sub

Private Sub cboOverlayFind_Click()
        cboOverlayFind_Pos.ListIndex = cboOverlayFind.ListIndex
        Call Get_Schema
End Sub

Private Sub cboSchema_Click()
        cboSchema_id.ListIndex = cboSchema.ListIndex
End Sub

Private Sub Chk_Payment1_Click()
        With Sis1
            .GetOverlayTheme iniLand, "EMPTY", ini_landtaxpay
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Payment1.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniLand, ini_landtaxpay
            .InsertOverlayTheme iniLand, "themeName", ini_landtaxpay
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Chk_Payment2_Click()
        With Sis1
            .GetOverlayTheme iniBuliding, "EMPTY", ini_buildingtaxpay
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Payment2.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniBuliding, ini_buildingtaxpay
            .InsertOverlayTheme iniBuliding, "themeName", ini_buildingtaxpay
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Chk_Payment3_Click()
        With Sis1
            .GetOverlayTheme iniSignbord, "EMPTY", ini_signbordtaxpay
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Payment3.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniSignbord, ini_signbordtaxpay
            .InsertOverlayTheme iniSignbord, "themeName", ini_signbordtaxpay
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Chk_Preform1_Click()
        With Sis1
            .GetOverlayTheme iniLand, "EMPTY", ini_landtaxaccept
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Preform1.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniLand, ini_landtaxaccept
            .InsertOverlayTheme iniLand, "themeName", ini_landtaxaccept
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Chk_Preform2_Click()
        With Sis1
            .GetOverlayTheme iniBuliding, "EMPTY", ini_buildingtaxaccept
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Preform2.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniBuliding, ini_buildingtaxaccept
            .InsertOverlayTheme iniBuliding, "themeName", ini_buildingtaxaccept
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Chk_Preform3_Click()
        With Sis1
            .GetOverlayTheme iniSignbord, "EMPTY", ini_signbordtaxaccept
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not CBool(Chk_Preform3.Value)
            .StoreTheme "themeName"
            .RemoveOverlayTheme iniSignbord, ini_signbordtaxaccept
            .InsertOverlayTheme iniSignbord, "themeName", ini_signbordtaxaccept
            .Redraw SIS_CURRENTWINDOW
        End With
End Sub

Private Sub Cmd_Clear_Click()
        txtLand(0).Text = Empty
        txtLand(1).Text = Empty
        txtLand(2).Text = Empty
        txtLand(3).Text = Empty
        txtLand(4).Text = Empty
        txtSignboard(0).Text = Empty
        txtBuilding(0).Text = Empty
        txtBuilding(1).Text = Empty
        txtBuilding(2).Text = Empty
        txtSignboard(1).Text = Empty
        Lst_Details(1).Clear
        txtSchema.Text = Empty
End Sub

Private Sub cmdOK_Click()
On Error GoTo errHandle
Dim strFilterCode As String
Dim indexOverlay As Integer
Dim i As Integer

strFilterCode = Empty
Select Case cboOverlay.ListIndex
            Case 0   '���Թ
                        If (LenB(Trim$(txtLand(0).Text)) = 0) And (LenB(Trim$(txtLand(1).Text)) = 0) And (LenB(Trim$(txtLand(2).Text)) = 0) And (LenB(Trim$(txtLand(3).Text)) = 0) And (LenB(Trim$(txtLand(4).Text)) = 0) Then
                            MsgBox "�ô�������ŷ���ͧ��ä���", vbInformation + vbOKOnly, "Warning !"
                            txtLand(0).SetFocus
                            Exit Sub
                        End If
                        Me.MousePointer = 11
                        If LenB(Trim$(txtLand(0).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(LAND_NOTIC$=""" & Trim$(txtLand(0).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtLand(1).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(LAND_RAWANG$=""" & Trim$(txtLand(1).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtLand(2).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(LAND_SURVEY$=""" & Trim$(txtLand(2).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtLand(3).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(LAND_NUMBER$=""" & Trim$(txtLand(3).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtLand(4).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(LAND_ID$=""" & Trim$(txtLand(4).Text) & """" & ") and "
                        End If
                        indexOverlay = iniLand

            Case 1  '�ç���͹
                        If LenB(Trim$(txtBuilding(3).Text)) > 0 Then
                                    Lst_Details(2).Clear
                                    Call SET_QUERY("Select BUILDING_ID From BUILDINGDATA Where LAND_ID = '" & Trim$(txtBuilding(3).Text) & "' Order by BUILDING_ID", SearchRS)
                                    If SearchRS.RecordCount > 0 Then
                                            Do While Not SearchRS.EOF
                                                  Lst_Details(2).AddItem SearchRS.Fields("BUILDING_ID").Value
                                                  SearchRS.MoveNext
                                            Loop
                                            Exit Sub
                                    Else
                                            Exit Sub
                                    End If
                        End If
                        If LenB(Trim$(txtBuilding(0).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(BUILDING_NO$=""" & Trim$(txtBuilding(0).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtBuilding(1).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(BUILDING_ID$=""" & Trim$(txtBuilding(1).Text) & """" & ") and "
                        End If
                        If LenB(Trim$(txtBuilding(2).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(VILLAGE_NO$=""" & Trim$(txtBuilding(2).Text) & """" & ") and "
                        End If
                        
                        If LenB(Trim$(txtBuilding(0).Text)) + LenB(Trim$(txtBuilding(1).Text)) + LenB(Trim$(txtBuilding(2).Text)) = 0 Then Exit Sub
                        Me.MousePointer = 11
                        indexOverlay = iniBuliding
                
                Case 2
                        If LenB(Trim$(txtSignboard(0).Text)) > 0 Then
                            strFilterCode = strFilterCode & "(SIGNBORD_ID$=""" & Trim$(txtSignboard(0).Text) & """" & ") and "
                            indexOverlay = iniSignbord
                        Else
                                If txtSignboard(1).Text <> Empty Then
                                        Lst_Details(1).Clear
                                        Call SET_QUERY("Select  SIGNBORD_ID From SIGNBORDDATA Where Land_id = '" & Trim$(txtSignboard(1).Text) & "' Order by SIGNBORD_ID", SearchRS)
                                        If SearchRS.RecordCount > 0 Then
                                                Do While Not SearchRS.EOF
                                                      Lst_Details(1).AddItem SearchRS.Fields("SIGNBORD_ID").Value
                                                      SearchRS.MoveNext
                                                Loop
                                                Exit Sub
                                        Else
                                                Exit Sub
                                        End If
                                End If
                       End If
                Case 3
                        If LenB(Trim$(txtSchema.Text)) > 0 Then
                                If InStr(Right$(cboSchema_id.Text, 1), "$") Then
                                    strFilterCode = cboSchema_id.Text & " = """ & Trim$(txtSchema.Text) & """"
                                Else
                                    strFilterCode = cboSchema_id.Text & " = " & Trim$(txtSchema.Text)
                                End If
                                indexOverlay = cboOverlayFind_Pos.Text
                        End If
End Select
                Me.MousePointer = 11
                If cboOverlay.ListIndex <> 3 Then
                        strFilterCode = Left$(strFilterCode, Len(strFilterCode) - 5)
                End If
                With Sis1
                            If .GetInt(SIS_OT_OVERLAY, indexOverlay, "_status&") = SIS_INVISIBLE Then
                                    .SetInt SIS_OT_OVERLAY, indexOverlay, "_status&", SIS_HITTABLE
                                    tvwOverlay.Nodes.Item(indexOverlay + 1).Selected = True
                                    tvwTheme.Nodes.Item(indexOverlay + 1).Selected = True
                                    tvwOverlay.Nodes.Item(indexOverlay + 1).Checked = True
                                    tvwTheme.Nodes.Item(indexOverlay + 1).Checked = True
                                    tvwOverlay.Nodes.Item(indexOverlay + 1).Image = 1
                                    tvwTheme.Nodes.Item(indexOverlay + 1).Image = 1
                            End If
                            .DeselectAll
                            .EmptyList "selectitem"
                            .CreatePropertyFilter "filter", strFilterCode
                            .ScanOverlay "selectitem", indexOverlay, "filter", ""
                            If .GetListSize("selectitem") > 0 Then
                                    .SelectList "selectitem"
                                    For i = 0 To .GetListSize("selectitem") - 1
                                            .OpenList "selectitem", i
                                            .DoCommand "AComZoomSelect"
                                    Next i
                            Else
                                    MsgBox "��辺������", vbOKOnly + vbInformation, "�š�ä���"
                            End If
                End With
                Me.MousePointer = 0
                Exit Sub
errHandle:
    Me.MousePointer = 0
    MsgBox Err.Description, vbOKOnly + vbCritical, "Microtax Software"
End Sub

Private Sub Command3_Click()
        With Sis1
                .OpenSel 0
                '.CreatePropertyFilter "Areas", "_id& <> 0 "
                .CreateClassTreeFilter "Areas", "+Item"
                .SetOverlayFilter 5, "Areas"
                .ZoomExtent
        End With
End Sub

Private Sub Command1_Click()
        With Sis1
            .Activate
            .DoCommand "AComRegenView"
            .RefreshDbTable "VIEW_PBT5"
            .RefreshDbTable "VIEW_PRD2"
            .RefreshDbTable "VIEW_PP1"
            .RefreshDataset 1
            .DoCommand "AComRedraw"
            .Redraw 0
    End With
End Sub

Function GetNumberOf(Items As String, Sep As String)
    Dim StopLoop As Boolean, cPos As Integer, FCodesNum As Integer
    cPos = 0
    FCodesNum = 0
    StopLoop = False
    
    Do
        If InStr(cPos + 1, Items, Sep) > 0 Then
            cPos = InStr(cPos + 1, Items, Sep)
        Else
            StopLoop = True
        End If
            FCodesNum = FCodesNum + 1
    Loop Until StopLoop
    GetNumberOf = FCodesNum
End Function

Private Sub Form_Activate()
            Clone_Form.Picture1.Visible = False
           Me.WindowState = vbMaximized
End Sub

Private Sub Command2_Click()
        With Sis1
                .RefreshDbTable "BUILDINGDATA"
                .RefreshDbTable "LANDDATA"
                .RefreshDbTable "SIGNBORDDATA"
                .RefreshDataset 1
                .Redraw 0
        End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Screen.ActiveControl Is TextBox And Screen.ActiveControl.Name = "Txt_Buffer" Then Exit Sub
    Select Case KeyCode
            Case vbKeyF2
                        Sis1.DoCommand "AComLayers"
                        Call Get_Overay
            Case vbKeyF5
                        Sis1.Redraw SIS_CURRENTWINDOW
                        Call Get_Overay
            Case vbKeyF7
                        Sis1.SelectAll
                        Clone_Form.StatusBar1.Panels(4).Text = Sis1.GetNumSel  'edit pass
                        Clone_Form.StatusBar1.Panels(5).Text = "0" 'edit pass
            Case vbKeyF8
                        Sis1.DeselectAll
                        Clone_Form.StatusBar1.Panels(4).Text = "0" 'edit pass
                        Clone_Form.StatusBar1.Panels(5).Text = "0" 'edit pass
                        Call Sis1_CommandAction("AComDeselectAll", "Succeeded")
            Case vbKeyDelete
                        Sis1.DoCommand "AComDelete"
            Case vbKeyEscape
                        Fme_CreateRubberSheet.Visible = False
    End Select
    If Shift = 1 And KeyCode = vbKeyZ Then
         Sis1.DoCommand "AComUndo"
    End If
End Sub

Private Sub Form_Load()
'        Dim i As Integer
        swdFile = iniPath_Map
        cboOverlay.ListIndex = 0
        Call Create_TableProperty
        Call Openswd
        Select Case TProduct
                       Case "VIEWER"
                                        Tb_Select.Buttons(6).Enabled = False
                                        Tb_Select.Buttons(7).Enabled = False
                                        Tb_Mesure.Visible = False
                                        Tb_Order.Visible = False
                                        Tb_File.Visible = False
                                        Tb_Draw.Visible = False
                                        Toolbar1.Visible = False
                                        Sis1.Width = 12400
                                        tvwOverlay.Height = 6345
                                        Tb_Select.Left = 6840
                                        Tb_Select.Buttons(11).Enabled = False
                        Case "MANAGER"
                                        Tb_Mesure.Buttons(7).Enabled = False
                                        Toolbar1.Buttons(3).Enabled = False
                                        Toolbar1.Buttons(4).Enabled = False
                                        
        End Select
'        gsLockedColumns = "|0|"
'        gHW = Grid_PROPERTY.hWnd
'        Call Hook
'        g_lngDefaultHandler = GetWindowLong(Grid_PROPERTY.hWnd, GWL_WNDPROC)
'        Call SetWindowLong(Grid_PROPERTY.hWnd, GWL_WNDPROC, AddressOf GridMessage)
End Sub

Private Sub Form_Unload(Cancel As Integer)
'        Call SetWindowLong(Grid_PROPERTY.hWnd, GWL_WNDPROC, g_lngDefaultHandler)
        Set Frm_Map = Nothing
End Sub

Private Sub Grid_PROPERTY_Click()
        Call Grid_PROPERTY_EnterCell
End Sub

Private Sub Grid_PROPERTY_Collapse(Cancel As Boolean)
        Txt_Buffer.Visible = False
End Sub

Private Sub Grid_PROPERTY_EnterCell()
On Error Resume Next
        Sis1.OpenSel 0
        If Sis1.GetNumSel <= 0 Then Exit Sub  'Or Sis1.GetNumSel >= 2
        With Grid_PROPERTY
            If (.Col = 2) Then     'And .Row > 16
                    If Sis1.GetNumSel = 1 Then
                            Select Case Right$(Trim$(Txt_Buffer2), 1)
                                      Case "$"
                                              Sis1.SetStr SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                                      Case "&"
                                              Sis1.SetInt SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                                      Case "#"
                                              Sis1.SetFlt SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                            End Select
                    Else
                            Dim i As Long
                            For i = 0 To Sis1.GetNumSel - 1
                                   Sis1.OpenSel i
                                   Select Case Right$(Trim$(Txt_Buffer2), 1)
                                        Case "$"
                                            Sis1.SetStr SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                                        Case "&"
                                            Sis1.SetInt SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                                        Case "#"
                                            Sis1.SetFlt SIS_OT_CURITEM, 0, Txt_Buffer2, Txt_Buffer.Text
                                  End Select
                            Next i
                    End If
                    Sis1.UpdateItem
'                    Debug.Print Sis1.GetErrorString(Sis1.GetInt(SIS_OT_SYSTEM, 0, "_ExecError&"))
                    If Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, Txt_Buffer2) <> 0 Then
                            .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, Txt_Buffer2)
                    ElseIf Sis1.EvaluateInt(SIS_OT_CURITEM, 0, Txt_Buffer2) <> 0 Then
                            .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateInt(SIS_OT_CURITEM, 0, Txt_Buffer2)
                    ElseIf Sis1.EvaluateStr(SIS_OT_CURITEM, 0, Txt_Buffer2) <> "" Then
                            .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateStr(SIS_OT_CURITEM, 0, Txt_Buffer2)
                    End If
'                    Select Case Right$(Trim$(Txt_Buffer2), 1)
'                          Case "$"
'                              .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateStr(SIS_OT_CURITEM, 0, Txt_Buffer2)
'                          Case "&"
'                              .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateInt(SIS_OT_CURITEM, 0, Txt_Buffer2)
'                          Case "#"
'                              .TextMatrix(Grid_PROPERTY.Tag, .Col) = Sis1.EvaluateFlt(SIS_OT_CURITEM, 0, Txt_Buffer2)
'                    End Select
'                    .TextMatrix(Grid_PROPERTY.Tag, .Col) = Txt_Buffer.Text
                    Txt_Buffer.Visible = False
                    Grid_PROPERTY.SetFocus
                    If Txt_Buffer.Tag <> .Row Then
                            Txt_Buffer.Text = .TextMatrix(.Row, .Col)
                            Txt_Buffer2 = .TextMatrix(.Row, 0)  '�� Col = 2 �� Col = 0
                            Grid_PROPERTY.Tag = .Row
                    End If
                    Txt_Buffer.Tag = .Row
                    Txt_Buffer.Left = .CellLeft + 60   '.CellLeft + .Left -8
                    Txt_Buffer.Top = .CellTop + .Top - 8
                    Txt_Buffer.Width = .CellWidth - 40
                    Txt_Buffer.Height = .CellHeight
                    Txt_Buffer.SelStart = 0
                    Txt_Buffer.SelLength = Len(Txt_Buffer.Text)
                    Txt_Buffer.Visible = True
                    Txt_Buffer.SetFocus
            Else
                    Txt_Buffer.Visible = False
            End If
        End With
End Sub

Private Sub Grid_PROPERTY_Expand(Cancel As Boolean)
        Txt_Buffer.Visible = False
End Sub

Private Sub Grid_PROPERTY_Scroll()
        Txt_Buffer.Visible = False
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        Drag = True
End Sub

Private Sub Image1_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
If Drag Then
End If
End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
   If y < 200 Then
       y = 1100
  End If
  If y >= 6000 Then
      y = 6000
  End If
    Image1.Top = y
    tvwTheme.Height = Image1.Top
    Sis3.Move 30, Image1.Top + 100 ' Image.Height
    Sis3.Height = 6330 - (tvwTheme.Height + Image1.Height) - 30
Drag = False
End Sub

Private Sub Lb_Year_Change()
             
             Set SearchRS = Globle_Connective.Execute("exec sp_find_pbt5 " & Lb_Year.Caption, , adCmdUnknown)
             With Sis1
                    If SearchRS.RecordCount > 0 Then
                            .DeleteRecordset "RsStateTaxLand"
                            .DefineRecordset "RsStateTaxLand", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PBT5,VIEW_PBT5,VIEW_PBT5,VIEW_PBT5", "LAND_ID,PBT5_YEAR,PBT5_STATUS,PBT5_NO_STATUS", "LAND_ID$,PBT5_YEAR&,PBT5_STATUS&,PBT5_NO_STATUS&", " LAND_ID IS NOT NULL AND PBT5_YEAR = " & CInt(Lb_Year.Caption)
                            .CreateDbTable "VIEW_PBT5", "RsStateTaxLand", "LAND_ID$,PBT5_YEAR&,PBT5_STATUS&,PBT5_NO_STATUS&", True
                            .DoCommand "AComRegenView"
                            .RefreshDbTable "VIEW_PBT5"
                            .RefreshDataset 1
                            'Sis1.DoCommand "AComRedraw"
                            .Redraw 0
                    Else
                            .DeleteRecordset "RsStateTaxLand"
                            .DefineRecordset "", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PBT5,VIEW_PBT5,VIEW_PBT5,VIEW_PBT5", "LAND_ID,PBT5_YEAR,PBT5_STATUS,PBT5_NO_STATUS", "LAND_ID$,PBT5_YEAR&,PBT5_STATUS&,PBT5_NO_STATUS&", " LAND_ID IS NOT NULL AND PBT5_YEAR = " & CInt(Lb_Year.Caption)
                            .CreateDbTable "VIEW_PBT5", "", "LAND_ID$,PBT5_YEAR&,PBT5_STATUS&,PBT5_NO_STATUS&", True
                            .DoCommand "AComRegenView"
                            .RefreshDbTable ""
                            .RefreshDataset 1
                            .Redraw 0
                    End If
                     
'                    Call SET_QUERY("Select * From VIEW_PRD2 WHERE PRD2_YEAR = " & Lb_Year.Caption, SearchRS)
                    Set SearchRS = Globle_Connective.Execute("exec sp_find_prd2 " & Lb_Year.Caption, , adCmdUnknown)
                    If SearchRS.RecordCount > 0 Then
                        .DeleteRecordset "RsStateTaxBuilding"
                        .DefineRecordset "RsStateTaxBuilding", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PRD2,VIEW_PRD2,VIEW_PRD2,VIEW_PRD2", "BUILDING_ID,PRD2_YEAR,PRD2_STATUS,PRD2_NO_STATUS", "BUILDING_ID$,PRD2_YEAR&,PRD2_STATUS&,PRD2_NO_STATUS&", " BUILDING_ID IS NOT NULL AND PRD2_YEAR = " & CInt(Lb_Year.Caption)
                        .CreateDbTable "VIEW_PRD2", "RsStateTaxBuilding", "BUILDING_ID$,PRD2_YEAR&,PRD2_STATUS&,PRD2_NO_STATUS&", True
                        .DoCommand "AComRegenView"
                        .RefreshDbTable "VIEW_PRD2"
                        .RefreshDataset 1
                        .Redraw 0
                    Else
                        .DeleteRecordset "RsStateTaxBuilding"
                        .DefineRecordset "", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PRD2,VIEW_PRD2,VIEW_PRD2,VIEW_PRD2", "BUILDING_ID,PRD2_YEAR,PRD2_STATUS,PRD2_NO_STATUS", "BUILDING_ID$,PRD2_YEAR&,PRD2_STATUS&,PRD2_NO_STATUS&", " BUILDING_ID IS NOT NULL AND PRD2_YEAR = " & CInt(Lb_Year.Caption)
                        .CreateDbTable "VIEW_PRD2", "", "BUILDING_ID$,PRD2_YEAR&,PRD2_STATUS&,PRD2_NO_STATUS&", True
                        .DoCommand "AComRegenView"
                        .RefreshDbTable "VIEW_PRD2"
                        .RefreshDataset 1
                        .Redraw 0
                    End If
                     
'                    Call SET_QUERY("Select * From VIEW_PP1 WHERE PP1_YEAR = " & Lb_Year.Caption, SearchRS)
                    Set SearchRS = Globle_Connective.Execute("exec sp_find_pp1 " & Lb_Year.Caption, , adCmdUnknown)
                    If SearchRS.RecordCount > 0 Then
                            .DeleteRecordset "RsStateTaxSignbord"
                            .DefineRecordset "RsStateTaxSignbord", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PP1,VIEW_PP1,VIEW_PP1,VIEW_PP1", "SIGNBORD_ID,PP1_YEAR,PP1_STATUS,PP1_NO_STATUS", "SIGNBORD_ID$,PP1_YEAR&,PP1_STATUS&,PP1_NO_STATUS&", " SIGNBORD_ID IS NOT NULL AND PP1_YEAR = " & CInt(Lb_Year.Caption)
                            .CreateDbTable "VIEW_PP1", "RsStateTaxSignbord", "SIGNBORD_ID$,PP1_YEAR&,PP1_STATUS&,PP1_NO_STATUS&", True
                            .DoCommand "AComRegenView"
                            .RefreshDbTable "VIEW_PP1"
                            .RefreshDataset 1
                            .Redraw 0
                    Else
                            .DeleteRecordset "RsStateTaxSignbord"
                            .DefineRecordset "", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "VIEW_PP1,VIEW_PP1,VIEW_PP1,VIEW_PP1", "SIGNBORD_ID,PP1_YEAR,PP1_STATUS,PP1_NO_STATUS", "SIGNBORD_ID$,PP1_YEAR&,PP1_STATUS&,PP1_NO_STATUS&", " SIGNBORD_ID IS NOT NULL AND PP1_YEAR = " & CInt(Lb_Year.Caption)
                            .CreateDbTable "VIEW_PP1", "", "SIGNBORD_ID$,PP1_YEAR&,PP1_STATUS&,PP1_NO_STATUS&", True
                            .DoCommand "AComRegenView"
                            .RefreshDbTable "VIEW_PP1"
                            .RefreshDataset 1
                            .Redraw 0
                    End If
             End With
End Sub

Private Sub Sis1_AppCommand(ByVal comname As String)
        Dim r As Long
        Dim theLink As String
        With Sis1
                Select Case comname
                        Case "Open Link"
                                .OpenSel 0
                                theLink = .GetStr(SIS_OT_CURITEM, 0, "_URI$")
                                If Trim$(theLink) <> "" Then
                                        r = StartDoc(theLink)
                                        If r <= 32 Then
                                                Select Case r
                                                        Case SE_ERR_FNF
                                                                    MsgBox "File not found"
                                                        Case SE_ERR_PNF
                                                                    MsgBox "Path not found"
                                                        Case SE_ERR_ACCESSDENIED
                                                                    MsgBox "Access denied"
                                                        Case SE_ERR_OOM
                                                                    MsgBox "Out of Memory"
                                                        Case SE_ERR_DLLNOTFOUND
                                                                    MsgBox "DLL not found"
                                                        Case SE_ERR_SHARE
                                                                    MsgBox "A sharing violation occurred"
                                                        Case SE_ERR_ASSOCINCOMPLETE
                                                                    MsgBox "Incomplete or invalid file association"
                                                        Case SE_ERR_DDETIMEOUT
                                                                    MsgBox "DDE Time out"
                                                        Case SE_ERR_DDEFAIL
                                                                    MsgBox "DDE transaction failed"
                                                        Case SE_ERR_DDEBUSY
                                                                    MsgBox "DDE busy"
                                                        Case SE_ERR_NOASSOC
                                                                    MsgBox "No association for file extension"
                                                        Case ERROR_BAD_FORMAT
                                                                    MsgBox "Invalid EXE file or error in EXE image"
                                                        Case Else
                                                                    MsgBox "Unknown error"
                                                End Select
                                        End If
                                End If
                End Select
        End With
        Call Sis1_CommandAction("AComDeselectAll", "Succeeded")
End Sub

Private Sub Sis1_CommandAction(ByVal comname As String, ByVal Action As String)
If comname = "AComZoomModeIn" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComZoomModeOut" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComPanDrag" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComPanContinuous" And Action = "End" Then
                             Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComPan" And Action = "End" Then
                             Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComPan" And Action = "End" Then
                             Call UnPush_ToolBar(Tb_ViewOldPush, 1)
ElseIf comname = "AComDeselectAll" And Action = "Succeeded" Then
                                Tb_View.Buttons(12).Enabled = False
                                Tb_Select.Buttons(4).Enabled = False
                                Tb_File.Buttons(4).Enabled = False
                                Tb_File.Buttons(5).Enabled = False
                                Tb_File.Buttons(6).Enabled = False
                                Tb_File.Buttons(7).Enabled = False
                                Tb_File.Buttons(9).Enabled = False
                                Tb_File.Buttons(10).Enabled = False
                                Tb_Order.Buttons(6).Enabled = False
ElseIf comname = "AComSelectSlide" And Action = "Snap" Then
                                Tb_View.Buttons(12).Enabled = True
                                Tb_Select.Buttons(4).Enabled = True
                                Tb_File.Buttons(4).Enabled = True
                                Tb_File.Buttons(5).Enabled = True
                                Tb_File.Buttons(6).Enabled = True
                                Tb_File.Buttons(7).Enabled = True
                                Tb_File.Buttons(9).Enabled = True
                                Tb_File.Buttons(10).Enabled = True
                                Tb_Draw.Buttons(9).Enabled = True
                                Tb_Draw.Buttons(10).Enabled = True
                                Tb_Draw.Buttons(11).Enabled = True
                                Tb_Order.Buttons(6).Enabled = True
                                
ElseIf comname = "AComMeasureAngle" And Action = "End" Then
                                Tb_Mesure.Buttons(2).Enabled = True
ElseIf comname = "AComMeasureDist" And Action = "End" Then
                                Tb_Mesure.Buttons(4).Enabled = True
ElseIf comname = "AComMeasurePos" And Action = "End" Then
                                Tb_Mesure.Buttons(6).Enabled = True
ElseIf comname = "AComMeasureRoute" And Action = "End" Then
                                Tb_Mesure.Buttons(7).Enabled = True

ElseIf comname = "AComFence" And Action = "End" Then
                                Call UnPush_ToolBar(Tb_ViewOldPush, 3)
ElseIf comname = "AComFenceCircle" And Action = "End" Then
                                Call UnPush_ToolBar(Tb_ViewOldPush, 3)


 ElseIf comname = "AComPoint" And Action = "End" Then
                             Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                             isEdit = False
 ElseIf comname = "AComLineEx" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                            isEdit = False
ElseIf comname = "AComFreeHand" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                            isEdit = False
ElseIf comname = "AComAreaEx" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                            isEdit = False
ElseIf comname = "AComRect" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                            isEdit = False
ElseIf comname = "AComEllipse" And Action = "End" Then
                            Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                            isEdit = False
ElseIf comname = "AComBoxText" And Action = "End" Then
                              Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                              isEdit = False
ElseIf comname = "AComBoxText" And Action = "End" Then
                              Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                              isEdit = False
ElseIf comname = "AComBoxText" And Action = "End" Then
                              Call UnPush_ToolBar(Tb_ViewOldPush, 5)
                              isEdit = False
ElseIf comname = "AComDisplace" And Action = "End" Then
                              Fme_CreateRubberSheet.Visible = False
                              isEdit = False
ElseIf comname = "AComLayers" And Action = "Succeeded" Then
            Call Get_Overay
End If
End Sub

Private Sub Sis1_DblClick()
Dim X As Integer, indexOverlay As Integer
Dim OverlayName  As String

If isEdit = True Then

Else
               Sis1.OpenSel 0
            If Sis1.GetNumSel = 1 Then
                    With Sis1
                            .OpenSel 0
                        For X = 0 To .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&") - 1
                            If .GetStr(SIS_OT_DATASET, .GetInt(SIS_OT_OVERLAY, X, "_nDataset&"), "_name$") = .GetStr(SIS_OT_CURITEM, 0, "_dataset$") Then
                                OverlayName = .GetStr(SIS_OT_OVERLAY, X, "_name$")
                                indexOverlay = X
                                Exit For
                            End If
                        Next X
                         
                         Select Case X
                                      Case iniLand
                                        Call SET_QUERY("SELECT LAND_ID FROM LANDDATA  WHERE LAND_ID = '" & Trim$(.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")) & "'", SearchRS)
                                                 If SearchRS.RecordCount > 0 Then
                                                        Call Frm_Search.SetLand2(Trim$(.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")))
                                                        Call Frm_Search.SetLand(Trim$(.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")))
                                                                Frm_Search.ZOrder
                                                                Frm_Search.Show
                                                                Frm_Search.SetFocus
                                                                Frm_Search.SSTab1.Tab = 6
                                                                Frm_Search.SSTab2.Tab = 0
                                                                Frm_Search.Frame1.Visible = True
                                                                Frm_Search.Shp_Menu2(2).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(3).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(4).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(1).BackColor = &H96D3BB
                                                  Else
                                                                MsgBox "��辺�����ź��ŧ���Թ " & Trim$(.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")), vbExclamation, "�Ӫ��ᨧ !"
                                                  End If
                                      Case iniBuliding
                                        Call SET_QUERY("SELECT BUILDING_ID FROM BUILDINGDATA_NOTIC WHERE BUILDING_ID = '" & Trim$(.GetStr(SIS_OT_CURITEM, 0, "BUILDING_ID$")) & "'", SearchRS)
                                                 If SearchRS.RecordCount > 0 Then
                                                          Call Frm_Search.SetBuilding2(Trim$(.GetStr(SIS_OT_CURITEM, 0, "BUILDING_ID$")))
                                                          Call Frm_Search.SetBuilding(Trim$(.GetStr(SIS_OT_CURITEM, 0, "BUILDING_ID$")))
                                                                Frm_Search.ZOrder
                                                                Frm_Search.Show
                                                                Frm_Search.SetFocus
                                                                Frm_Search.SSTab1.Tab = 6
                                                                Frm_Search.SSTab2.Tab = 1
                                                                Frm_Search.Frame1.Visible = True
                                                                Frm_Search.Shp_Menu2(1).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(3).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(4).BackColor = &HD6D6D6
                                                                Frm_Search.Shp_Menu2(2).BackColor = &H96D3BB
                                                  Else
                                                                 MsgBox "��辺�������ç���͹ " & Trim$(.GetStr(SIS_OT_CURITEM, 0, "BUILDING_ID$")), vbExclamation, "�Ӫ��ᨧ !"
                                                  End If
                                      Case iniSignbord
                                        Call SET_QUERY("SELECT SIGNBORD_ID FROM SIGNBORDDATA_NOTIC WHERE SIGNBORD_ID = '" & Trim$(.GetStr(SIS_OT_CURITEM, 0, "SIGNBORD_ID$")) & "'", SearchRS)
                                                 If SearchRS.RecordCount > 0 Then
                                                            Call Frm_Search.SetSignbord2(Trim$(.GetStr(SIS_OT_CURITEM, 0, "SIGNBORD_ID$")))
                                                            Call Frm_Search.SetSignBoard(Trim$(.GetStr(SIS_OT_CURITEM, 0, "SIGNBORD_ID$")))
                                                            Frm_Search.ZOrder
                                                            Frm_Search.Show
                                                            Frm_Search.SetFocus
                                                            Frm_Search.SSTab1.Tab = 6
                                                            Frm_Search.SSTab2.Tab = 2
                                                            Frm_Search.Frame1.Visible = True
                                                            Frm_Search.Shp_Menu2(1).BackColor = &HD6D6D6
                                                            Frm_Search.Shp_Menu2(2).BackColor = &HD6D6D6
                                                            Frm_Search.Shp_Menu2(4).BackColor = &HD6D6D6
                                                            Frm_Search.Shp_Menu2(3).BackColor = &H96D3BB
                                                  Else
                                                            MsgBox "��辺�����Ż��� " & Trim$(.GetStr(SIS_OT_CURITEM, 0, "SIGNBORD_ID$")), vbExclamation, "�Ӫ��ᨧ !"
                                                  End If
                         End Select
                   End With
            End If
End If
End Sub

Private Sub Sis1_DragDrop(Source As Control, X As Single, y As Single)
        swdFile = Source
        Call Openswd
End Sub

Private Sub Sis1_GotFocus()
        Txt_Buffer.Visible = False
End Sub

Private Sub Sis1_MouseTrack(ByVal X As Double, ByVal y As Double, ByVal z As Double)
        If TProduct = "VIEWER" Then
            Clone_Form.StatusBar1.Panels(8).Text = FormatNumber(X, 4) & " , " & FormatNumber(y, 4) & " , " & Format$(z, "###,###,###,###0.0###")
        Else
            Clone_Form.StatusBar1.Panels(8).Text = Sis1.StrFromMetre(X, SIS_LENGTHDIM, True) & "," & Sis1.StrFromMetre(y, SIS_LENGTHDIM, True) & "," & Sis1.StrFromMetre(z, SIS_LENGTHDIM, True)
        End If
         Txt_RubberX.Text = FormatNumber(X, 3)
         Txt_RubberY.Text = FormatNumber(y, 3)
End Sub

Private Sub Sis1_ScaleChange(ByVal displayScale As Double)
        Dim Extent_Txt As String
        Dim x1 As Double, y1 As Double, z1 As Double, x2 As Double, y2 As Double, z2 As Double
        
        Clone_Form.StatusBar1.Panels(7).Text = " 1 : " & Format(displayScale, "##")  'edit pass
        
        Extent_Txt = Sis1.GetDisplayExtent
        Sis1.SplitExtent x1, y1, z1, x2, y2, z2, Extent_Txt
        
        x1_Index = x1
        y1_Index = y1
        z1_Index = z1
        x2_Index = x2
        y2_Index = y2
        z2_Index = z2
End Sub

Private Sub Sis1_SelectionChange(ByVal nEditable As Long, ByVal nHittable As Long, ByVal commonClass As String)
        If Sis1.GetNumSel = 1 And SSTab1.Tab = 1 Then
                Call Get_PropertyObject(True)
        Else
                Call Get_PropertyObject(False)
        End If
        Clone_Form.StatusBar1.Panels(4).Text = nHittable 'edit pass
        Clone_Form.StatusBar1.Panels(5).Text = nEditable 'edit pass
        Clone_Form.StatusBar1.Panels(6).Text = commonClass 'edit pass
    Call Sis1_CommandAction("AComSelectSlide", "Snap")
'If isEdit = False Then
'    Sis1.SetInt SIS_OT_OPTION, 0, "_bFlickerDisplay&", True
'Else
'    Sis1.SetInt SIS_OT_OPTION, 0, "_bFlickerDisplay&", False
'End If
End Sub

Private Sub Sis1_Snap(ByVal X As Double, ByVal y As Double, ByVal z As Double)
    RubberX1 = X
    RubberY1 = y
    'Sis1.SetViewExtent x - ((x2_Index - x1_Index) / 2), y - ((y2_Index - y1_Index) / 2), z - ((z2_Index - z1_Index) / 2), x + ((x2_Index - x1_Index) / 2), y + ((y2_Index - y1_Index) / 2), z + ((z2_Index - z1_Index) / 2)
    'Sis1.Activate
End Sub

Private Sub Sis2_Snap(ByVal X As Double, ByVal y As Double, ByVal z As Double)
    Sis1.SetViewExtent X - ((x2_Index - x1_Index) / 2), y - ((y2_Index - y1_Index) / 2), z - ((z2_Index - z1_Index) / 2), X + ((x2_Index - x1_Index) / 2), y + ((y2_Index - y1_Index) / 2), z + ((z2_Index - z1_Index) / 2)
    Sis1.Activate
End Sub

Private Sub Tb_Draw_ButtonClick(ByVal Button As MSComctlLib.Button)
 With Sis1
                    isEdit = True
                Select Case Button.Key
                            Case "AComPoint"
                                        .DoCommand "AComPoint"
                                        Call Push_ToolBar(1, 5)
                            Case "AComLineEx"
                                        .DoCommand "AComLineEx"
                                        Call Push_ToolBar(2, 5)
                            Case "AComFreeHand"
                                        .DoCommand "AComFreeHand"
                                        Call Push_ToolBar(3, 5)
                            Case "AComAreaEx"
                                        .DoCommand "AComAreaEx"
                                        Call Push_ToolBar(5, 5)
                            Case "AComRect"
                                        .DoCommand "AComRect"
                                        Call Push_ToolBar(6, 5)
                            Case "AComEllipse"
                                        .DoCommand "AComEllipse"
                                        Call Push_ToolBar(7, 5)
                            Case "AComBoxText"
                                        .DoCommand "AComBoxText"
                                        Call Push_ToolBar(9, 5)
                            Case "AComText"
                                        .DoCommand "AComText"
                                        Call Push_ToolBar(10, 5)
                            Case "AComBoxLabel"
                                        .DoCommand "AComBoxLabel"
                                        Call Push_ToolBar(11, 5)
                End Select
End With
End Sub

Private Sub Tb_File_ButtonClick(ByVal Button As MSComctlLib.Button)
 With Sis1
                .CreateListFromSelection "TextSteamItem"
                isEdit = False
                Select Case Button.Key
                             Case "AComCut"
                                            .Copy "TextSteamItem", True
                             Case "AComCopy"
                                            .Copy "TextSteamItem", False
                              Case "AComPaste"
                                            .Paste
                              Case "AComDelete"
                                            .Delete "TextSteamItem"
                                            .DeselectAll
                              Case "AComUndo"
                                            .DoCommand "AComUndo"
                              Case "AComRedo"
                                            .DoCommand "AComRedo"
                              Case "AComLayers"
                                            .DoCommand "AComLayers"
                                            Call Get_Overay
                              Case "AComAddOverlay"
                                            .DoCommand "AComAddOverlay"
                                            Call Get_Overay
                              Case "AComAddTheme"
                                            .DoCommand "AComAddTheme"
                                            Call Get_Overay
                                Case "AComFileSave"
                                            If MsgBox("Save changes to Map ?  " & GetPath(swdFile, "GETFILE"), vbExclamation + vbYesNo, "SAVE SWD") = vbNo Then Exit Sub
                                            .SaveSwd swdFile
                                Case "AComFileOpen"
                                             With Clone_Form.CommonDialog1
                                                    .DialogTitle = "Open file swd"
                                                    .CancelError = True
                                                    On Error GoTo ErrHandler
                                                    .InitDir = "C:\"
                                                    .Flags = cdlOFNHideReadOnly        ' Set flags
                                                    .Filter = "All Files (*.*)|*.*|Swd Files(*.swd)|*.swd"     ' Set filters
                                                    .FilterIndex = 2
                                                    .ShowOpen
                                                    swdFile = .FileName
                                                    Call Openswd
                                                    Sis1.ZoomExtent
                                            End With
               End Select
End With
Exit Sub
ErrHandler:
End Sub

Private Sub Tb_Mesure_ButtonClick(ByVal Button As MSComctlLib.Button)
        With Sis1
               isEdit = False
               Select Case Button.Key
                        Case "AComMeasureAngle"
                                    .DoCommand "AComMeasureAngle"
                                    Call Push_ToolBar(2, 2)
                        Case "AComMeasArea"
                                    .DoCommand "AComMeasArea"
                        Case "AComMeasureDist"
                                    .DoCommand "AComMeasureDist"
                                    Call Push_ToolBar(4, 2)
                        Case "AComMeasureLen"
                                    .DoCommand "AComMeasureLen"
                        Case "AComMeasurePos"
                                    .DoCommand "AComMeasurePos"
                                    Call Push_ToolBar(6, 2)
                        Case "AComMeasureRoute"
                                    .DoCommand "AComMeasureRoute"
                                    Call Push_ToolBar(7, 2)
                                    Tb_Mesure.Buttons(7).Enabled = True
                End Select
        End With
End Sub

Private Sub Tb_Order_ButtonClick(ByVal Button As MSComctlLib.Button)
                Select Case Button.Key
                            Case "AComBluePrint"
                                          Frm_Blueprint.Show vbModal
                            Case "AComPrj"
                                            Sis1.DoCommand "AComPrj"
                            Case "SetColors"
                                            On Error Resume Next
                                            Clone_Form.CommonDialog1.Flags = cdlCCRGBInit
                                            Clone_Form.CommonDialog1.ShowColor
                                            Sis1.DeselectAll
                                            Sis1.SetInt SIS_OT_SYSTEM, 0, "_ColSelection&", Clone_Form.CommonDialog1.Color
                            Case "AComSelect"
                                            Sis1.DoCommand "AComSelect"
                            Case "AComReplicate"
                                        Sis1.DoCommand "AComReplicate"
                End Select
End Sub

Private Sub Tb_Select_ButtonClick(ByVal Button As MSComctlLib.Button)
 With Sis1
                                isEdit = False
                Select Case Button.Key
                            Case "AComSelectSlide"
                                        .DoCommand "AComSelectSlide"
                                        Tb_View.Buttons(2).Enabled = True
                                        Tb_View.Buttons(3).Enabled = True
                                        Tb_View.Buttons(4).Enabled = True
                                        Tb_View.Buttons(6).Enabled = True
                                        Tb_View.Buttons(7).Enabled = True
                                        Tb_View.Buttons(8).Enabled = True
                                        Fme_CreateRubberSheet.Visible = False
                            Case "AComSelectAll"
                                        .DoCommand "AComSelectAll"
                                        Clone_Form.StatusBar1.Panels(4).Text = Sis1.GetNumSel  'edit pass
                                        Clone_Form.StatusBar1.Panels(5).Text = "0" 'edit pass
                                        Call Sis1_CommandAction("AComSelectSlide", "Snap")
                            Case "AComDeselectAll"
                                        .DoCommand "AComDeselectAll"
                                        Clone_Form.StatusBar1.Panels(4).Text = "0" 'edit pass
                                        Clone_Form.StatusBar1.Panels(5).Text = "0" 'edit pass
                                        Call Sis1_CommandAction("AComDeselectAll", "Succeeded")
                             Case "AComFence"
                                        .DoCommand "AComFence"
                                        Call Push_ToolBar(6, 3)
                             Case "AComFenceCircle"
                                        .DoCommand "AComFenceCircle"
                                        Call Push_ToolBar(6, 4)
                            Case "AComListArea"
                                        Frm_Area.Show vbModal
                            Case "AComExport"
                                        .DoCommand "AComExport"
                            Case "AComPrint"
                                        Frm_PrintMap.Show
                                        Frm_PrintMap.TxtScale.Text = Trim(Replace(Clone_Form.StatusBar1.Panels(7).Text, "1 :", ""))
'                                        Frm_PrintMap.Sis1.Swd = Sis1.Swd
'                                        Frm_PrintMap.Sis_Template.NolInsert App.Path & "\NOL.nol", 1, True
                                        Frm_PrintMap.Sis1.NolInsert App.Path & "\NOL.nol", 1, True
'                                        Dim theExtent As String
'                                        Dim x1#, y1#, z1#, x2#, y2#, z2#
'                                        theExtent = Sis1.GetViewExtent
'                                        Frm_PrintMap.Sis1.SplitExtent x1#, y1#, z1#, x2#, y2#, z2#, theExtent
'                                        Frm_PrintMap.Sis1.SetViewExtent x1#, y1#, z1#, x2#, y2#, z2#
'                                        Frm_PrintMap.Sis_Template.SplitExtent x1#, y1#, z1#, x2#, y2#, z2#, theExtent
'                                        Frm_PrintMap.Sis_Template.SetViewExtent x1#, y1#, z1#, x2#, y2#, z2#
'                                        Frm_PrintMap.Sis1.Redraw 0
'                                        .SetStr SIS_OT_PRINTER, 0, "_device$", Printer.DeviceName
'                                        .SetStr SIS_OT_PRINTER, 0, "_driver$", Printer.DriverName
'                                        .SetStr SIS_OT_PRINTER, 0, "_output$", Printer.Port
'                                        If Frm_SetPrint.opOrientation(0).Value = True Then
'                                                .SetInt SIS_OT_PRINTER, 0, "_orientation&", vbPRORPortrait
'                                        Else
'                                                .SetInt SIS_OT_PRINTER, 0, "_orientation&", vbPRORLandscape
'                                        End If
'                                        Select Case Frm_SetPrint.cbPaperSize.ListIndex
'                                                Case 0
'                                                        .SetInt SIS_OT_PRINTER, 0, "_paperSize&", vbPRPSA3
'                                                Case 1
'                                                        .SetInt SIS_OT_PRINTER, 0, "_paperSize&", vbPRPSA4
'                                        End Select
'                                        .DoCommand "AComPrintTemplateWizard"
'                            Case "SetPrint"
'                                         Frm_SetPrint.Show vbModal
                End Select
End With
End Sub

Private Sub Tb_View_ButtonClick(ByVal Button As MSComctlLib.Button)
 With Sis1
                                   isEdit = False
                Select Case Button.Key
                            Case "AComZoomModeIn"
                                          .DoCommand "AComZoomModeIn"
                                          Call Push_ToolBar(2, 1)
                             Case "AComZoomModeOut"
                                        .DoCommand "AComZoomModeOut"
                                        Call Push_ToolBar(3, 1)
                             Case "AComRoamerMode"
                                        .DoCommand "AComRoamerMode"
                                        Call Push_ToolBar(4, 1)
                              Case "AComPanDrag"
                                         .DoCommand "AComPanDrag"
                                         Call Push_ToolBar(6, 1)
                              Case "AComPanContinuous"
                                        .DoCommand "AComPanContinuous"
                                        Call Push_ToolBar(7, 1)
                               Case "AComPan"
                                            .DoCommand "AComPan"
                                            Call Push_ToolBar(8, 1)
                                Case "AComZoomAll"
                                           .DoCommand "AComZoomAll"
                                Case "AComZoomExtent"
                                           .DoCommand "AComZoomExtent"
                                Case "AComZoomSelect"
                                           .DoCommand "AComZoomSelect"
                                Case "AComZoomToScale"
                                           .DoCommand "AComZoomToScale"
                    End Select
 End With
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
With Sis1
                            isEdit = True
                Select Case Button.Key
                            Case "AComPasteFrom"
                                     .DoCommand "AComPasteFrom"
                            Case "AComCreateArea"
                                      Frm_CreateAreaOfScale.Show vbModal
                             Case "AComDisplace"
                                            If .Level <> SIS_LEVEL_MODELLER Then
                                                 MsgBox "Active on MicroTax Enterprise Product !", vbExclamation, "MicroTax Software"
                                                 Exit Sub
                                            End If
                                            Fme_CreateRubberSheet.Visible = True
                                            .DoCommand "AComDisplace"
                             Case "AComRubberSheet"
                                              If .Level <> SIS_LEVEL_MODELLER Then
                                                 MsgBox "Active on MicroTax Enterprise Product !", vbExclamation, "MicroTax Software"
                                                 Exit Sub
                                            End If
                                                 .DoCommand "AComRubberSheet"
                 End Select
End With
End Sub

Private Sub tvwOverlay_AfterLabelEdit(Cancel As Integer, NewString As String)
On Error Resume Next
If TProduct <> "VIEWER" Then
        If LenB(NewString) = 0 Then
                Cancel = True
        End If
        If tvwOverlay.SelectedItem.Parent Is Nothing Then 'parent
                Sis1.SetStr SIS_OT_OVERLAY, tvwOverlay.SelectedItem.Index - 1, "_name$", Trim$(NewString)
                tvwTheme.Nodes.Item(tvwOverlay.SelectedItem.Index).Text = NewString
        End If
End If
End Sub

Private Sub tvwOverlay_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        MouseDown = Button
        yPush = y
        xPush = X
End Sub

Private Sub tvwOverlay_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        xPush = X
        yPush = y
End Sub

Private Sub tvwOverlay_NodeCheck(ByVal Node As MSComctlLib.Node)
On Error Resume Next
'        Dim i As Byte, n As Long, m As Integer
        Dim n As Long
        Dim FormulaString As String
'    Node.Selected = True
'    tvwOverlay.SelectedItem = Node.Index
    
If Node.Parent Is Nothing Then               'New Line ==========================================
         IndexNode = Node.Index '
'With tvwOverlay
'   If Node.Parent Is Nothing Then 'parent
        If Node.Checked = False Then
                    Node.Image = 2
'                     Clone_Form.mnuInvisible_Click
                    Call CLEAR_CHECK
                    FrmMain.mnuInvisible.Checked = True
                    tvwOverlay.Nodes(IndexNode).Checked = False
                    tvwOverlay.Nodes(IndexNode).Image = 2
                    tvwTheme.Nodes.Item(IndexNode).Checked = False
                    tvwTheme.Nodes.Item(IndexNode).Image = 2
                    Sis1.DeselectAll
                    Sis1.SetInt SIS_OT_OVERLAY, IndexNode - 1, "_status&", SIS_INVISIBLE
        Else
                    Node.Image = 1
'                    Clone_Form.mnuHittable_Click
                    Call CLEAR_CHECK
                    FrmMain.mnuHittable.Checked = True
                    tvwOverlay.Nodes(IndexNode).Checked = True
                    tvwOverlay.Nodes(IndexNode).Image = 1
                    tvwTheme.Nodes.Item(IndexNode).Checked = True
                    tvwTheme.Nodes.Item(IndexNode).Image = 1
                    Sis1.SetInt SIS_OT_OVERLAY, IndexNode - 1, "_status&", SIS_HITTABLE
        End If
'  End If
'  End With
Else ' New Line ==========================================
'                        If Node.Checked = False Then
'                                      Node.Image = 57
'                                      Sis1.DeselectAll '****New Lines
'                        Else
'                                      Node.Image = 56
'                        End If
'
'                        If tvwOverlay.Nodes(Node.Index).Children > 0 Then Exit Sub
'                        If Node.Checked Then  '****New Lines
'                                   FormulaString = "(_layer$ = " & """" & Node.Text & """" & ")"  '****New Lines
'                                   Sis1.CreatePropertyFilter "Filter", FormulaString
'                                   n = Sis1.ScanOverlay("ItemsFound", Node.Parent.Index - 1, "Filter", "")   '****New Lines
'                                   Sis1.SelectList "ItemsFound"  '****New Lines
'                                   Sis1.OpenSel 0  '****New Lines
'                                  'Sis1.SetOverlayFilter (tvwOverlay.Nodes(Node.Index).Parent.Index) - 1, "Filter"
'                                   Sis1.DoCommand "AComZoomSelect"  '****New Lines
'                                   Sis1.EmptyList "ItemsFound"
'                        End If
                        
                        Sis1.GetOverlayFilter Node.Parent.Index - 1, "Overlay"
'                        MsgBox Sis1.ScanOverlay("test", Node.Parent.Index - 1, "Overlay", "")
'                        Sis1.CreateCombinedFilter ("FilterShow")
                        If Node.Checked = False Then
                                    Node.Image = 57
                                    Sis1.CreatePropertyFilter "FilterShow", "_layer$ = '" & Node.Text & "'"
                                    Sis1.CombineFilter "FilterOut", "Overlay", "FilterShow", SIS_BOOLEAN_DIFF
                                    Sis1.SetOverlayFilter Node.Parent.Index - 1, "FilterOut"
'                                    Sis1.SetOverlayFilter Node.Parent.Index - 1, ""
                        Else
                                    Node.Image = 56
                                    Sis1.CreatePropertyFilter "FilterShow", "_layer$ = '" & Node.Text & "'"
                                    Sis1.CombineFilter "FilterOut", "Overlay", "FilterShow", SIS_BOOLEAN_OR
                                    Sis1.SetOverlayFilter Node.Parent.Index - 1, "FilterOut"
'                                    Sis1.SetOverlayFilter Node.Parent.Index - 1, ""
                        End If
                        
'                        Dim Logic As Boolean
'                        Logic = False
'
'                        FormulaString = Empty
'                        FormulaString = "(_layer$ = " & """"
'                         m = tvwOverlay.Nodes(tvwOverlay.Nodes(Node.Index).Parent.Index).Children
'                         N = tvwOverlay.Nodes(tvwOverlay.Nodes(Node.Index).Parent.Index).Child.Index

'                        For i = 0 To m - 1
'                                If tvwOverlay.Nodes(N).Checked Then
'                                        FormulaString = FormulaString & tvwOverlay.Nodes(N).Text & """" & " OR _layer$ = " & """"
'                                        Logic = True
'                                End If
'                                        N = N + 1
'                        Next i
'                               If Logic Then
'                                  FormulaString = FormulaString & """ )"
'                              Else
'                                  FormulaString = "(_layer$ = " & " """ & "" & """ )"
'                              End If
                
End If
End Sub

Private Sub tvwOverlay_NodeClick(ByVal Node As MSComctlLib.Node)
On Error Resume Next
    
    Node.Selected = True
If tvwOverlay.SelectedItem.Parent Is Nothing Then ' New Line ========================================================================
    IndexNode = Node.Index
    Clone_Form.StatusBar1.Panels(3).Text = tvwOverlay.Nodes(Node.Index).Text  'edit pass
    Call CHECK_STATUSLAYER(Node.Index - 1)
    tvwTheme.Nodes.Item(Node.Index).Selected = True
End If
 If TProduct = "VIEWER" Then Exit Sub
 If MouseDown = 2 Then
        Dim SourceNode As Node
            If tvwOverlay.SelectedItem.Parent Is Nothing Then
                Set SourceNode = tvwOverlay.HitTest(xPush, yPush)
                If SourceNode Is Nothing Then
                    'PopupMenu mnuPopup, vbPopupMenuRightAlign, tvwOverlay.Left + X, tvwOverlay.Top + Frm_Map.Top + Y
                Else
                               Call CHECK_STATUSLAYER(Node.Index - 1)
                        Set tvwOverlay.SelectedItem = tvwOverlay.HitTest(xPush, yPush)
                               PopupMenu Clone_Form.mnuPopup5, vbPopupMenuRightAlign, tvwOverlay.Left + xPush, SSTab1.Top + tvwOverlay.Top + Me.Top + yPush + 550
                End If
          End If
                          MouseDown = 1
End If
End Sub

Private Sub tvwTheme_AfterLabelEdit(Cancel As Integer, NewString As String)
If TProduct = "VIEWER" Then Exit Sub
Dim nTheme As Integer

If LenB(NewString) = 0 Then
   Cancel = True
Else
                 With Sis1
                    With tvwTheme.SelectedItem
                          nTheme = Right(.Key, Len(.Key) - InStrRev(.Key, "-"))
                    End With
                    .GetOverlayTheme tvwTheme.SelectedItem.Parent.Index - 1, "EMPTY", nTheme
                    .LoadTheme "EMPTY"
                    .SetStr SIS_OT_THEME, 0, "_title$", Trim$(NewString)
                    .StoreTheme "themeName"
                    .RemoveOverlayTheme tvwTheme.SelectedItem.Parent.Index - 1, nTheme
                    .InsertOverlayTheme tvwTheme.SelectedItem.Parent.Index - 1, "themeName", nTheme
                End With
    End If
End Sub

Private Sub tvwTheme_BeforeLabelEdit(Cancel As Integer)
        If TProduct <> "VIEWER" Then
                If tvwTheme.SelectedItem.Parent Is Nothing Then Cancel = True
        End If
End Sub

Private Sub tvwTheme_Click()
On Error Resume Next
Dim checkTheme  As Boolean
If tvwTheme.Nodes.Count = 0 Then Exit Sub
        checkTheme = CBool(tvwTheme.SelectedItem.Index)
     If checkTheme = False Then Exit Sub
   '  if tvwTheme.Parent

Dim Blob As String, theExtent$
Dim x1#, y1#, z1#, x2#, y2#, z2#
Dim nTheme As Integer
    

If tvwTheme.SelectedItem.Parent Is Nothing Then

Else
    nTheme = CInt(Right$(tvwTheme.SelectedItem.Key, 1))
    Sis3.RemoveOverlay 0
    Blob = Sis1.GetOverlayThemeLegend(tvwTheme.SelectedItem.Parent.Index - 1, nTheme, "Cylindrical.Transverse Mercator", SIS_BLOB_SIS, 32)
  
  With Sis3
          .DeselectAll
         .CreateItem Blob, "Cylindrical.Transverse Mercator", SIS_BLOB_SIS
         .SetFlt SIS_OT_WINDOW, 0, "_displayScale#", -0.85
         .AddToList "Legend"
         .OpenList "Legend", 0
          theExtent$ = .GetViewExtent
         .SplitExtent x1#, y1#, z1#, x2#, y2#, z2#, theExtent$
         .MoveList "Legend", x1# * 0.87, y2# * 1.9, z2#, 0, 1
         .UpdateItem
   End With
 End If
End Sub

Private Sub tvwTheme_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
MouseDown = Button
yPush = y
xPush = X
End Sub

Private Sub tvwTheme_NodeCheck(ByVal Node As MSComctlLib.Node)
     tvwTheme.Nodes(Node.Index).Selected = True
If Node.Parent Is Nothing Then 'parent
        IndexNode = Node.Index
'        MsgBox Node.Index
      If Node.Checked = False Then
                     Node.Image = 2
                     tvwOverlay.Nodes.Item(Node.Index).Image = 2
                     tvwOverlay.Nodes.Item(Node.Index).Checked = False
                    Call Clone_Form.mnuInvisible_Click
        Else
                    Node.Image = 1
                    tvwOverlay.Nodes.Item(Node.Index).Image = 1
                    tvwOverlay.Nodes.Item(Node.Index).Checked = True
                    Call Clone_Form.mnuHittable_Click
        End If
                
Else
        If Node.Checked Then
               Node.Image = 46
        Else
               Node.Image = 62
        End If

        Dim i As Byte, nTheme As Integer
                 nTheme = Right(Node.Key, Len(Node.Key) - InStrRev(Node.Key, "-"))
        With Sis1
            .GetOverlayTheme Node.Parent.Index - 1, "EMPTY", nTheme
            .LoadTheme "EMPTY"
            .SetInt SIS_OT_THEME, 0, "_bDisabled&", Not Node.Checked
            .StoreTheme "themeName"
            .RemoveOverlayTheme Node.Parent.Index - 1, nTheme
            .InsertOverlayTheme Node.Parent.Index - 1, "themeName", nTheme
'            .Redraw SIS_CURRENTWINDOW
        End With
End If
End Sub

Private Sub tvwTheme_NodeClick(ByVal Node As MSComctlLib.Node)
    tvwTheme.Nodes(Node.Index).Selected = True
 If MouseDown = 2 Then
     If TProduct = "VIEWER" Then Exit Sub
            If Node.Parent Is Nothing Then
               Clone_Form.mnu_RemoveTheme.Visible = False
            Else
               Clone_Form.mnu_RemoveTheme.Visible = True
            End If
             Dim xNode As Nodes
                    Set tvwTheme.SelectedItem = tvwTheme.HitTest(xPush, yPush)
                            PopupMenu Clone_Form.mnuTheme, vbPopupMenuRightAlign, tvwTheme.Left + xPush, SSTab1.Top + tvwTheme.Top + Me.Top + yPush + 550
                            MouseDown = 1
 End If
End Sub

Private Sub Txt_Buffer_KeyDown(KeyCode As Integer, Shift As Integer)
'If KeyCode = 38 Or KeyCode = 40 Then
'        Txt_Buffer.Visible = False
'        Grid_PROPERTY.SetFocus
'End If
End Sub

Private Sub Txt_Buffer_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
      Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, Grid_PROPERTY.Col) = Txt_Buffer.Text
      Sis1.OpenSel 0
      If Sis1.GetNumSel = 1 Then
            Select Case Right$(Trim$(Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0)), 1)  '�� Col = 2 �� Col = 0
                        Case "$"
                                Sis1.SetStr SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                        Case "&"
                                Sis1.SetInt SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                        Case "#"
                                Sis1.SetFlt SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                End Select
      Else
             Dim i As Long
                      For i = 0 To Sis1.GetNumSel - 1
                             Sis1.OpenSel i
                             Select Case Right$(Trim$(Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0)), 1)  '�� Col = 2 �� Col = 0
                                 Case "$"
                                    Sis1.SetStr SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                                Case "&"
                                     Sis1.SetInt SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                               Case "#"
                                    Sis1.SetFlt SIS_OT_CURITEM, 0, Grid_PROPERTY.TextMatrix(Grid_PROPERTY.Row, 0), Txt_Buffer.Text  '�� Col = 2 �� Col = 0
                            End Select
                      Next i
      End If
                Sis1.UpdateItem
'                Sis1.DoCommand "AComRegenView"
'                Sis1.Redraw 1
'                Sis1.Redraw SIS_QUICK_REDRAW
                Txt_Buffer.Visible = False
                Grid_PROPERTY.SetFocus
End If
End Sub

Private Sub Txt_RubberX_Change()
        If Txt_RubberX.Text = Empty Then Txt_RubberX.Text = "0"
End Sub

Private Sub Txt_RubberX_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
       Call Txt_RubberY_KeyPress(KeyAscii)
End Sub

Private Sub Txt_RubberY_Change()
If Txt_RubberY.Text = Empty Then Txt_RubberY.Text = "0"
End Sub

Private Sub Txt_RubberY_KeyPress(KeyAscii As Integer)
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
  If KeyAscii = 13 Then
          Sis1.CreateDisplacement RubberX1, RubberY1, 0, CDbl(Txt_RubberX.Text), CDbl(Txt_RubberY.Text), 0
  End If
End Sub

Private Sub txtBuilding_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
  Call cmdOK_Click
End If
End Sub

Private Sub txtLand_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
  Call cmdOK_Click
End If
End Sub

Private Sub txtSchema_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Call cmdOK_Click
        End If
End Sub

Private Sub txtSignboard_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
  Call cmdOK_Click
End If
End Sub

Private Sub Create_ViewDB_Cadcorp()
        With Sis1
                .DeleteRecordset "RsStateBuildingdata"
                .DefineRecordset "RsStateBuildingdata", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA", "BUILDING_ID,BUILDING_HOME_NO,BUILDING_HOME_MOO,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_HEIGHT,BUILDING_WIDTH,FLAG_PAYTAX,BUILDING_TYPE,LAND_ID", "BUILDING_ID$,BUILDING_HOME_NO$,BUILDING_HOME_MOO$,BUILDING_ROOM&,BUILDING_FLOOR#,BUILDING_HEIGHT#,BUILDING_WIDTH#,FLAG_PAYTAX&,BUILDING_TYPE$,LAND_ID$", ""
'                .DefineRecordset "RsStateBuildingdata", "ODBC;DSN=MicroTax;", "BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA,BUILDINGDATA", "BUILDING_ID,BUILDING_HOME_NO,BUILDING_HOME_MOO,BUILDING_ROOM,BUILDING_FLOOR,BUILDING_HEIGHT,BUILDING_WIDTH,FLAG_PAYTAX,BUILDING_TYPE,LAND_ID", "BUILDING_ID$,BUILDING_HOME_NO$,BUILDING_HOME_MOO$,BUILDING_ROOM&,BUILDING_FLOOR#,BUILDING_HEIGHT#,BUILDING_WIDTH#,FLAG_PAYTAX&,BUILDING_TYPE$,LAND_ID$", ""
                .CreateDbTable "BUILDINGDATA", "RsStateBuildingdata", "BUILDING_ID$,BUILDING_HOME_NO$,BUILDING_HOME_MOO$,BUILDING_ROOM&,BUILDING_FLOOR#,BUILDING_HEIGHT#,BUILDING_WIDTH#,FLAG_PAYTAX&,BUILDING_TYPE$,LAND_ID$", True
                .DoCommand "AComRegenView"
                .RefreshDbTable "BUILDINGDATA"
                
                .DeleteRecordset "RsStateLanddata"
                .DefineRecordset "RsStateLanddata", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA_APPLY,LAND_EMPLOYMENT,LANDDATA", "LAND_ID,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC,FLAG_PAYTAX,LM_ID,LM_Details,LAND_TYPE", "LAND_ID$,LAND_NUMBER$,LAND_RAWANG$,LAND_SURVEY$,LAND_NOTIC$,FLAG_PAYTAX&,LM_ID$,LM_Details$,LAND_TYPE&", " LANDDATA.LAND_ID = LANDDATA_APPLY.LAND_ID and LANDDATA_APPLY.LM_ID = LAND_EMPLOYMENT.LM_ID AND right(LAND_APPLY_ID,2)='01'"
'                .DefineRecordset "RsStateLanddata", "ODBC;DSN=MicroTax;", "LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA,LANDDATA_APPLY,LAND_EMPLOYMENT,LANDDATA", "LAND_ID,LAND_NUMBER,LAND_RAWANG,LAND_SURVEY,LAND_NOTIC,FLAG_PAYTAX,LM_ID,LM_Details,LAND_TYPE", "LAND_ID$,LAND_NUMBER$,LAND_RAWANG$,LAND_SURVEY$,LAND_NOTIC$,FLAG_PAYTAX&,LM_ID$,LM_Details$,LAND_TYPE&", " LANDDATA.LAND_ID = LANDDATA_APPLY.LAND_ID AND LANDDATA_APPLY.LM_ID = LAND_EMPLOYMENT.LM_ID AND right(LAND_APPLY_ID,2)='01'"
                .CreateDbTable "LANDDATA", "RsStateLanddata", "LAND_ID$,LAND_NUMBER$,LAND_RAWANG$,LAND_SURVEY$,LAND_NOTIC$,FLAG_PAYTAX&,LM_ID$,LM_Details$,LAND_TYPE&", True
                .DoCommand "AComRegenView"
                .RefreshDbTable "LANDDATA"
                
                .DeleteRecordset "RsStateSignboarddata"
                .DefineRecordset "RsStateSignboarddata", "ODBC;DSN=" & "MicroTaxSQL" & ";Uid=" & ini_Username & ";Pwd=" & ini_Password & ";", "SIGNBORDDATA,SIGNBORDDATA,SIGNBORDDATA_DETAILS,SIGNBORDDATA_DETAILS,SIGNBORDDATA_DETAILS,SIGNBORDDATA,SIGNBORDDATA,SIGNBORDDATA", "SIGNBORD_ID,FLAG_PAYTAX,SIGNBORD_LABEL,SIGNBORD_WIDTH,SIGNBORD_HEIGHT,SIGNBORD_ROUND,SIGNBORD_NAME,LAND_ID", "SIGNBORD_ID$,FLAG_PAYTAX&,SIGNBORD_LABEL$,SIGNBORD_WIDTH#,SIGNBORD_HEIGHT#,SIGNBORD_ROUND&,SIGNBORD_NAME$,LAND_ID$", " SIGNBORDDATA.SIGNBORD_ID = SIGNBORDDATA_DETAILS.SIGNBORD_ID AND right(signbord_sub_id,2)='01'"
'                .DefineRecordset "RsStateSignboarddata", "ODBC;DSN=MicroTax;", "SIGNBORDDATA,SIGNBORDDATA,SIGNBORDDATA_DETAILS,SIGNBORDDATA_DETAILS,SIGNBORDDATA_DETAILS,SIGNBORDDATA,SIGNBORDDATA,SIGNBORDDATA", "SIGNBORD_ID,FLAG_PAYTAX,SIGNBORD_LABEL,SIGNBORD_WIDTH,SIGNBORD_HEIGHT,SIGNBORD_ROUND,SIGNBORD_NAME,LAND_ID", "SIGNBORD_ID$,FLAG_PAYTAX&,SIGNBORD_LABEL$,SIGNBORD_WIDTH#,SIGNBORD_HEIGHT#,SIGNBORD_ROUND&,SIGNBORD_NAME$,LAND_ID$", " SIGNBORDDATA.SIGNBORD_ID = SIGNBORDDATA_DETAILS.SIGNBORD_ID AND right(signbord_sub_id,2)='01'"
                .CreateDbTable "SIGNBORDDATA", "RsStateSignboarddata", "SIGNBORD_ID$,FLAG_PAYTAX&,SIGNBORD_LABEL$,SIGNBORD_WIDTH#,SIGNBORD_HEIGHT#,SIGNBORD_ROUND&,SIGNBORD_NAME$,LAND_ID$", True
                .DoCommand "AComRegenView"
                .RefreshDbTable "SIGNBORDDATA"
                
                .RefreshDataset 1
                'Sis1.DoCommand "AComRedraw"
                .Redraw 0
        End With
End Sub
