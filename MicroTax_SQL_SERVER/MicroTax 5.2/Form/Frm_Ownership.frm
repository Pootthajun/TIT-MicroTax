VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_Ownership 
   ClientHeight    =   9270
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12885
   ControlBox      =   0   'False
   Icon            =   "Frm_Ownership.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Ownership.frx":151A
   ScaleHeight     =   9270
   ScaleWidth      =   12885
   WindowState     =   2  'Maximized
   Begin VB.CommandButton BtnClear 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   12150
      Picture         =   "Frm_Ownership.frx":1A371
      Style           =   1  'Graphical
      TabIndex        =   71
      ToolTipText     =   "�������Ң����"
      Top             =   5880
      Width           =   375
   End
   Begin VB.ComboBox ComboTypeSch 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Ownership.frx":1A8FB
      Left            =   2460
      List            =   "Frm_Ownership.frx":1A90B
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   5850
      Width           =   1875
   End
   Begin VB.TextBox txtNm 
      Height          =   315
      Left            =   5025
      TabIndex        =   22
      Top             =   5880
      Width           =   2625
   End
   Begin VB.TextBox txtSurNm 
      Height          =   315
      Left            =   8715
      TabIndex        =   23
      Top             =   5880
      Width           =   2625
   End
   Begin VB.CommandButton BtnSearch 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   11640
      Picture         =   "Frm_Ownership.frx":1A942
      Style           =   1  'Graphical
      TabIndex        =   72
      ToolTipText     =   "����"
      Top             =   5880
      Width           =   405
   End
   Begin VB.TextBox Txt_ZipCode 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   11190
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   18
      Top             =   4170
      Width           =   1305
   End
   Begin VB.ComboBox Cmb_Amphoe_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   960
      TabIndex        =   70
      TabStop         =   0   'False
      Text            =   "Cmb_Amphoe_ID"
      Top             =   180
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   9
      Left            =   840
      TabIndex        =   68
      Top             =   5430
      Width           =   195
   End
   Begin VB.ComboBox Cmb_PostCode 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   10770
      Style           =   2  'Dropdown List
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   1470
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.ComboBox Cmb_Tambon_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1620
      TabIndex        =   66
      TabStop         =   0   'False
      Text            =   "Cmb_Tambon_ID"
      Top             =   60
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.ComboBox Cmb_Province_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   360
      Style           =   2  'Dropdown List
      TabIndex        =   65
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.TextBox Txt_OwnerShip_ID 
      Appearance      =   0  'Flat
      BackColor       =   &H0099AA88&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   6780
      Locked          =   -1  'True
      TabIndex        =   63
      TabStop         =   0   'False
      Top             =   1530
      Width           =   1725
   End
   Begin VB.TextBox Txt_ID_Gard 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   5
      Left            =   12480
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   10
      Top             =   2700
      Width           =   315
   End
   Begin VB.TextBox Txt_ID_Gard 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   4
      Left            =   11940
      Locked          =   -1  'True
      MaxLength       =   2
      TabIndex        =   9
      Top             =   2700
      Width           =   465
   End
   Begin VB.TextBox Txt_ID_Gard 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   3
      Left            =   10740
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   8
      Top             =   2700
      Width           =   1125
   End
   Begin VB.TextBox Txt_ID_Gard 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   2
      Left            =   9750
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   7
      Top             =   2700
      Width           =   915
   End
   Begin VB.TextBox Txt_ID_Gard 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   1
      Left            =   9330
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   6
      Top             =   2700
      Width           =   345
   End
   Begin VB.TextBox Txt_Owner_Number 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4050
      Locked          =   -1  'True
      MaxLength       =   15
      TabIndex        =   5
      Top             =   2700
      Width           =   4365
   End
   Begin VB.TextBox Txt_Surname 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   9300
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   2160
      Width           =   3495
   End
   Begin VB.ComboBox Cmb_Tambon 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   10080
      TabIndex        =   13
      Top             =   5040
      Width           =   2415
   End
   Begin VB.ComboBox Cmb_Amphoe 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6750
      TabIndex        =   12
      Top             =   5040
      Width           =   2415
   End
   Begin VB.ComboBox Cmb_Province 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3390
      TabIndex        =   11
      Top             =   5040
      Width           =   2415
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   8
      Left            =   840
      TabIndex        =   60
      Top             =   4950
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   7
      Left            =   840
      TabIndex        =   59
      Top             =   4470
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   6
      Left            =   840
      TabIndex        =   58
      Top             =   3930
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   5
      Left            =   840
      TabIndex        =   57
      Top             =   3420
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   4
      Left            =   840
      TabIndex        =   56
      Top             =   2910
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   3
      Left            =   840
      TabIndex        =   55
      Top             =   2430
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Height          =   195
      Index           =   2
      Left            =   840
      TabIndex        =   54
      Top             =   1950
      Width           =   195
   End
   Begin VB.OptionButton Op_Type 
      BackColor       =   &H00F1F1F1&
      Caption         =   "x"
      Height          =   195
      Index           =   1
      Left            =   840
      TabIndex        =   0
      Top             =   1500
      Width           =   195
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Ownership.frx":1AECC
      Left            =   4020
      List            =   "Frm_Ownership.frx":1AEE2
      TabIndex        =   2
      Top             =   1530
      Width           =   1395
   End
   Begin VB.TextBox Txt_Name 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4050
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   2160
      Width           =   4365
   End
   Begin VB.TextBox Txt_Add_Soi 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3450
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   4170
      Width           =   2925
   End
   Begin VB.TextBox Txt_Add_Home 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3450
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   3690
      Width           =   2925
   End
   Begin VB.TextBox Txt_Add_Moo 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8190
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   3690
      Width           =   4305
   End
   Begin VB.TextBox Txt_Add_Road 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7440
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   4170
      Width           =   2655
   End
   Begin VB.TextBox Txt_Add_Telephone 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3450
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   4620
      Width           =   2925
   End
   Begin VB.TextBox Txt_Email 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7440
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   4620
      Width           =   5055
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_OWNERSHIP 
      Height          =   2475
      Left            =   0
      TabIndex        =   43
      Top             =   6270
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   4366
      _Version        =   393216
      BackColor       =   16777215
      Rows            =   3
      Cols            =   16
      FixedCols       =   0
      BackColorFixed  =   14079702
      BackColorSel    =   14073244
      ForeColorSel    =   0
      BackColorBkg    =   15461351
      GridColor       =   15461351
      GridColorFixed  =   15856113
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   16
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Ownership.frx":1AF13
      Enabled         =   0   'False
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_Ownership.frx":1B77A
      Style           =   1  'Graphical
      TabIndex        =   28
      TabStop         =   0   'False
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Ownership.frx":1E071
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_Ownership.frx":1E908
      Style           =   1  'Graphical
      TabIndex        =   27
      TabStop         =   0   'False
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Ownership.frx":211ED
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_Ownership.frx":21A50
      Style           =   1  'Graphical
      TabIndex        =   26
      TabStop         =   0   'False
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_Ownership.frx":24475
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_Ownership.frx":24D27
      Style           =   1  'Graphical
      TabIndex        =   25
      TabStop         =   0   'False
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Ownership.frx":27762
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_Ownership.frx":28051
      Style           =   1  'Graphical
      TabIndex        =   24
      TabStop         =   0   'False
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Ownership.frx":2ABAC
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_Ownership.frx":2B3DA
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Ownership.frx":2DC64
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_Ownership.frx":2E4CD
      Style           =   1  'Graphical
      TabIndex        =   31
      TabStop         =   0   'False
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��¡�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   1
      Left            =   0
      TabIndex        =   77
      Top             =   8760
      Width           =   1635
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1650
      TabIndex        =   76
      Top             =   8760
      Width           =   1410
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�鹨ҡ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   18
      Left            =   1680
      TabIndex        =   75
      Top             =   5940
      Width           =   660
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   3
      Left            =   4590
      TabIndex        =   74
      Top             =   5940
      Width           =   345
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���ʡ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   17
      Left            =   7860
      TabIndex        =   73
      Top             =   5940
      Width           =   780
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��� �"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   9
      Left            =   1170
      TabIndex        =   69
      Top             =   5400
      Width           =   1125
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������Ңͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   2
      Left            =   5610
      TabIndex        =   64
      Top             =   1620
      Width           =   945
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00756E60&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   315
      Index           =   0
      Left            =   6840
      Top             =   1590
      Width           =   1725
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   15
      Left            =   12450
      Top             =   2670
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   14
      Left            =   11910
      Top             =   2670
      Width           =   525
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   5
      Left            =   10710
      Top             =   2670
      Width           =   1185
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   4
      Left            =   9720
      Top             =   2670
      Width           =   975
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   2
      Left            =   4020
      Top             =   2670
      Width           =   4425
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   1
      Left            =   9270
      Top             =   2130
      Width           =   3555
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʡ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   0
      Left            =   8550
      TabIndex        =   61
      Top             =   2220
      Width           =   660
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҥ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   6
      Left            =   1170
      TabIndex        =   53
      Top             =   3900
      Width           =   1275
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ŹԸ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   7
      Left            =   1170
      TabIndex        =   52
      Top             =   4410
      Width           =   1200
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ѵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Index           =   8
      Left            =   1170
      TabIndex        =   51
      Top             =   4920
      Width           =   1155
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ѱ����ˡԨ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   5
      Left            =   1170
      TabIndex        =   50
      Top             =   3390
      Width           =   1590
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ѱ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   4
      Left            =   1170
      TabIndex        =   49
      Top             =   2880
      Width           =   1170
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ҧ�����ǹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   1170
      TabIndex        =   48
      Top             =   2400
      Width           =   1590
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����ѷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Index           =   2
      Left            =   1170
      TabIndex        =   47
      Top             =   1920
      Width           =   1140
   End
   Begin VB.Label LB_Type 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ؤ�Ÿ�����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   1170
      TabIndex        =   46
      Top             =   1470
      Width           =   1560
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѧ��Ѵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   2580
      TabIndex        =   45
      Top             =   5160
      Width           =   570
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   13
      Left            =   9300
      Top             =   2670
      Width           =   405
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   12
      Left            =   7410
      Top             =   4140
      Width           =   2715
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   11
      Left            =   4020
      Top             =   2130
      Width           =   4425
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   10
      Left            =   8160
      Top             =   3660
      Width           =   4365
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   9
      Left            =   3420
      Top             =   3660
      Width           =   2985
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   8
      Left            =   3420
      Top             =   4140
      Width           =   2985
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   7
      Left            =   11160
      Top             =   4140
      Width           =   1365
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   6
      Left            =   3420
      Top             =   4590
      Width           =   2985
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   2580
      TabIndex        =   42
      Top             =   4200
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ҹ�Ţ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   2580
      TabIndex        =   41
      Top             =   3720
      Width           =   750
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   14
      Left            =   6840
      TabIndex        =   40
      Top             =   4200
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѵû��."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   8520
      TabIndex        =   39
      Top             =   2760
      Width           =   765
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   12
      Left            =   6840
      TabIndex        =   38
      Top             =   4650
      Width           =   390
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   11
      Left            =   6150
      TabIndex        =   37
      Top             =   5160
      Width           =   480
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   10
      Left            =   9510
      TabIndex        =   36
      Top             =   5160
      Width           =   435
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ɳ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   10290
      TabIndex        =   35
      Top             =   4200
      Width           =   690
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ������¹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   2580
      TabIndex        =   34
      Top             =   2760
      Width           =   1065
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����/����ʶҹ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   7
      Left            =   6840
      TabIndex        =   33
      Top             =   3720
      Width           =   1155
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   2580
      TabIndex        =   32
      Top             =   2250
      Width           =   225
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   2580
      TabIndex        =   30
      Top             =   1650
      Width           =   540
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Ѿ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   2550
      TabIndex        =   29
      Top             =   4650
      Width           =   720
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   7410
      Top             =   4590
      Width           =   5115
   End
   Begin VB.Label Lb_OwnerType_Name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2460
      Index           =   1
      Left            =   2460
      TabIndex        =   62
      Top             =   3240
      Width           =   10410
   End
   Begin VB.Label Lb_OwnerType_Name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��Ңͧ�����Է���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2250
      Index           =   2
      Left            =   2460
      TabIndex        =   44
      Top             =   930
      Width           =   10410
   End
End
Attribute VB_Name = "Frm_Ownership"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim LngResult As Long
Dim Status  As String
Dim INDEXS As Byte
Dim OwnershipId As String
Dim Query As ADODB.Recordset

Public Function CHECK_NAME() As Boolean
        Dim buffer_ownercode  As String
    If Btn_Add.Enabled = False And LenB(Trim$(Txt_Name.Text)) > 0 Then
                    With GBQueryOwnerShip
                            .Requery
                            If Op_Type(1).Value = True Then ' �ؤ�Ÿ�����
                                If LenB(Trim$(Txt_Surname.Text)) > 0 Then
                                .Filter = " OWNER_TYPE = " & INDEXS & " AND ���� = '" & Trim$(Txt_Name.Text) & "' AND  ���ʡ�� = '" & Trim$(Txt_Surname.Text) & "' AND �ӹ��= '" & Trim$(Combo1.Text) & "'"
                                        If .RecordCount > 0 Then
                                            If Status = "EDIT" And Trim$(Txt_Name.Text) & Trim$(Txt_Surname.Text) = Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)) & Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 2)) Then
                                                CHECK_NAME = False
                                                Exit Function
                                            End If
                                            MsgBox "�����Ū��� - " & LB_Type(INDEXS).Caption & " ����������ö������������ !", vbCritical, "Warning!"
                                            CHECK_NAME = True
                                        Else
                                             CHECK_NAME = False
                                             buffer_ownercode = GENERATE_OWNERCODE(CByte(INDEXS), Txt_Name.Text, Txt_Surname.Text) ' GenCode if name and surname no in base
                                             Txt_OwnerShip_ID.Text = RunAutoNumber("OWNERSHIP", "OWNERSHIP_REAL_ID", buffer_ownercode, True, " And OWNER_TYPE = 1 AND OWNERSHIP_REAL_ID >= '" & buffer_ownercode & "' AND OWNERSHIP_REAL_ID <  '" & buffer_ownercode & "/" & "99'")
                                        End If
                                Else
                                          CHECK_NAME = False
                                End If
                            Else
                                                .Filter = " OWNER_TYPE = " & INDEXS & " AND ���� = '" & Trim$(Txt_Name.Text) & "'"
                                            If .RecordCount > 0 Then
                                                If Status = "EDIT" And Trim$(Txt_Name.Text) = Trim$(GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 1)) Then
                                                    CHECK_NAME = False
                                                    Exit Function
                                                End If
                                                    MsgBox "�����Ū��� - " & LB_Type(INDEXS).Caption & " ����������ö������������ !", vbCritical, "Warning!"
                                                    CHECK_NAME = True
                                            Else
                                                      CHECK_NAME = False
                                                      Txt_OwnerShip_ID.Text = GENERATE_OWNERTYPE(INDEXS, False) & Txt_Name.Text
                                        End If
                            End If
                    End With
    Else
        CHECK_NAME = False
    End If
End Function

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
    GRID_OWNERSHIP.Enabled = False
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
'    Btn_Search.Enabled = False
    Btn_Refresh.Enabled = False
    
    Txt_Owner_Number.Locked = False:          Txt_Owner_Number.BackColor = &H80000005
    Txt_Name.Locked = False:                            Txt_Name.BackColor = &H80000005
    Txt_Add_Home.Locked = False:                  Txt_Add_Home.BackColor = &H80000005
    Txt_Add_Moo.Locked = False:                     Txt_Add_Moo.BackColor = &H80000005
    Txt_Add_Soi.Locked = False:                       Txt_Add_Soi.BackColor = &H80000005
    Txt_Add_Road.Locked = False:                   Txt_Add_Road.BackColor = &H80000005
    Txt_Email.Locked = False:                            Txt_Email.BackColor = &H80000005
    Txt_Add_Telephone.Locked = False:         Txt_Add_Telephone.BackColor = &H80000005
    Txt_ZipCode.Locked = False:                      Txt_ZipCode.BackColor = &H80000005
    For i = 1 To 5
        If Op_Type(1).Value = True Then
                 Txt_ID_Gard(i).Locked = False
                 Txt_ID_Gard(i).BackColor = &H80000005
                 Txt_Surname.Locked = False:                      Txt_Surname.BackColor = &H80000005
                 Txt_Owner_Number.BackColor = &HEBEBE7
        End If
             If STATE = "ADD" Then    'Manage Button
                 Txt_ID_Gard(i).Text = ""
             End If
    Next i
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                        Txt_OwnerShip_ID.Text = ""
                        Txt_Owner_Number.Text = ""
                        Txt_Name.Text = ""
                        Txt_Surname.Text = ""
                        Txt_Add_Home.Text = ""
                        Txt_Add_Moo.Text = ""
                        Txt_Add_Soi.Text = ""
                        Txt_Add_Road.Text = ""
                        Txt_Email.Text = ""
                        Txt_Add_Telephone.Text = ""
                        Txt_ZipCode.Text = ""
    If STATE <> "ADD" Then
        GRID_OWNERSHIP.Enabled = True
        Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
        Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
'        Btn_Search.Enabled = True
        Btn_Refresh.Enabled = True
        
        Txt_Owner_Number.Locked = True:            Txt_Owner_Number.BackColor = &HEBEBE7
        Txt_Name.Locked = True:            Txt_Name.BackColor = &HEBEBE7
        Txt_Surname.Locked = True:      Txt_Surname.BackColor = &HEBEBE7
        Txt_Add_Home.Locked = True:   Txt_Add_Home.BackColor = &HEBEBE7
        Txt_Add_Moo.Locked = True:       Txt_Add_Moo.BackColor = &HEBEBE7
        Txt_Add_Soi.Locked = True:         Txt_Add_Soi.BackColor = &HEBEBE7
        Txt_Add_Road.Locked = True:     Txt_Add_Road.BackColor = &HEBEBE7
        Txt_Email.Locked = True:              Txt_Email.BackColor = &HEBEBE7
        Txt_Add_Telephone.Locked = True:     Txt_Add_Telephone.BackColor = &HEBEBE7
        Txt_ZipCode.Locked = True:     Txt_ZipCode.BackColor = &HEBEBE7
          For i = 1 To 5
                 Txt_ID_Gard(i).Locked = True
                 Txt_ID_Gard(i).BackColor = &HEBEBE7
                 Txt_ID_Gard(i).Text = ""
        Next i
   End If
End If

If UCase(Perpose) = "PERPOSE" Then
            Txt_Name.SetFocus
'            If Op_Type(1).Value = True Then
'                        If STATE = UCase("ADD") Then
'                        ElseIf STATE = UCase("EDIT") Then
'                        End If
'            End If
'                            If Op_Type(2).Value = True Then
'                            End If
End If
End Sub

Private Sub SET_REFRESH()
        GBQueryOwnerShip.Requery
        GRID_OWNERSHIP.Refresh
        GBQueryOwnerShip.Filter = " OWNER_TYPE =  " & INDEXS
        If GBQueryOwnerShip.RecordCount > 0 Then
                      GBQueryOwnerShip.MoveFirst
               Set GRID_OWNERSHIP.DataSource = GBQueryOwnerShip
        Else
             Set GRID_OWNERSHIP.DataSource = Nothing
                GRID_OWNERSHIP.Rows = 2
                GRID_OWNERSHIP.Clear
        End If
        Call Set_Grid
End Sub

Private Sub Set_Grid()
'            DoEvents
            With GRID_OWNERSHIP
                    If Op_Type(1).Value = True Then
                            .ColWidth(0) = 1000 ':   .TextArray(0) = "�ӹ��"
                            .ColWidth(2) = 3000 ':   .TextArray(2) = "���ʡ��"
                            .ColWidth(4) = 1500: .ColAlignmentFixed(4) = 4 ':   .TextArray(4) = "�ѵû�ЪҪ�"
                            .ColWidth(3) = 0 ':    .TextArray(3) = ""
                    Else
                            .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4  ':   .TextArray(3) = "�Ţ������¹"
                            .ColWidth(0) = 0 ':    .TextArray(0) = ""
                            .ColWidth(2) = 0 ':     .TextArray(2) = ""
                            .ColWidth(4) = 0 ':    .TextArray(4) = ""
                    End If
            
                    .ColWidth(1) = 3500:   .TextArray(1) = "���� - " & LB_Type(INDEXS).Caption
                    .ColWidth(5) = 1200: .ColAlignmentFixed(5) = 4  ':   .TextArray(5) = "��ҹ�Ţ���"
                    .ColWidth(6) = 1400: .ColAlignmentFixed(6) = 4  ':   .TextArray(6) = "����/����ʶҹ���"
                    .ColWidth(7) = 1400: .ColAlignmentFixed(7) = 4  ':   .TextArray(7) = "���"
                    .ColWidth(8) = 1400: .ColAlignmentFixed(8) = 4  ':  .TextArray(8) = "���"
                    .ColWidth(9) = 1400: .ColAlignmentFixed(9) = 4  ':  .TextArray(9) = "�Ӻ�"
                    .ColWidth(10) = 1400: .ColAlignmentFixed(10) = 4  ':   .TextArray(10) = "�����"
                    .ColWidth(11) = 1400: .ColAlignmentFixed(11) = 4  ':   .TextArray(11) = "�ѧ��Ѵ"
                    .ColWidth(12) = 1700: .ColAlignmentFixed(12) = 4  ':   .TextArray(12) = "���Ѿ��"
                    .ColWidth(13) = 1500: .ColAlignmentFixed(13) = 4  ':   .TextArray(13) = "������"
                    .ColWidth(14) = 0
                    .ColWidth(15) = 0
                    .ColWidth(16) = 0
                    .ColWidth(17) = 0 'TAMBON_ID
                    .ColWidth(18) = 1500: .ColAlignmentFixed(18) = 4  ':   .TextArray(18) = "��ɳ���"
            End With
End Sub

Private Sub SET_DATABASE(STATE As String)
Dim sql_txt As String
        sql_txt = "exec sp_manage_ownership '" & IIf(STATE = "POST", RunAutoNumber("OWNERSHIP", "OWNERSHIP_ID", GENERATE_OWNERTYPE(INDEXS, True) & "00000001", False, " And OWNER_TYPE = " & INDEXS), GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 16)) & "','" & _
                        Trim$(Txt_OwnerShip_ID.Text) & "','" & Trim$(Combo1.Text) & "','" & Trim$(Txt_Name.Text) & "','" & IIf(Trim$(Txt_Surname.Text) = "", "-", Trim$(Txt_Surname.Text)) & "','" & IIf(Trim$(Txt_Add_Home.Text) = "", "-", Trim$(Txt_Add_Home.Text)) & "','" & IIf(Trim$(Txt_Add_Moo.Text) = "", "-", Trim$(Txt_Add_Moo.Text)) & "','" & IIf(Trim$(Txt_Add_Soi.Text) = "", "-", Trim$(Txt_Add_Soi.Text)) & _
                        "','" & IIf(Trim$(Txt_Add_Road.Text) = "", "-", Trim$(Txt_Add_Road.Text)) & "','" & IIf(Trim$(Txt_Add_Telephone.Text) = "", "-", Trim$(Txt_Add_Telephone.Text)) & "','" & IIf(Trim$(Txt_Email.Text) = "", "-", Trim$(Txt_Email.Text)) & "','" & IIf(Trim$(Cmb_Tambon_ID.Text) = "", "-", Trim$(Cmb_Tambon_ID.Text)) & "','" & Trim$(Txt_Owner_Number.Text) & "','" & _
                        Txt_ID_Gard(1).Text & Txt_ID_Gard(2).Text & Txt_ID_Gard(3).Text & Txt_ID_Gard(4).Text & Txt_ID_Gard(5).Text & "'," & INDEXS & ",'" & IIf(Trim$(Cmb_Tambon.Text) = "", "-", Trim$(Cmb_Tambon.Text)) & "','" & IIf(Trim$(Cmb_Amphoe.Text) = "", "-", Trim$(Cmb_Amphoe.Text)) & "','" & IIf(Trim$(Cmb_Province.Text) = "", "-", Trim$(Cmb_Province.Text)) & "','" & IIf(Trim$(Txt_ZipCode.Text) = "", "-", Trim$(Txt_ZipCode.Text)) & "','" & STATE & "'"
'        Globle_Connective.Execute sql_txt, , adCmdUnknown
        Call SET_Execute2(sql_txt, 8, False)
'                                   If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO OWNERSHIP (OWNERSHIP_ID, OWNERSHIP_REAL_ID, PRENAME, OWNER_NAME, OWNER_SURNAME,ADD_HOME,ADD_MOO,ADD_SOI,ADD_ROAD,TELEPHONE,EMAIL,TAMBON_ID ,OWNER_NUMBER ,ID_GARD,OWNER_TYPE ,TAMBON_NAME,AMPHOE_NAME,PROVINCE_NAME,ZIPCODE) " & _
'                                                         " VALUES ('" & RunAutoNumber("OWNERSHIP", "OWNERSHIP_ID", GENERATE_OWNERTYPE(INDEXS, True) & "00000001", False, " And OWNER_TYPE = " & INDEXS) & _
'                                                         "','" & Trim$(Txt_Ownership_ID.Text) & "','" & Trim$(Combo1.Text) & "','" & Trim$(Txt_Name.Text) & "','" & IIf(Trim$(Txt_Surname.Text) = Empty, "-", Trim$(Txt_Surname.Text)) & "','" & IIf(Trim$(Txt_Add_Home.Text) = Empty, "-", Trim$(Txt_Add_Home.Text)) & "','" & IIf(Trim$(Txt_Add_Moo.Text) = Empty, "-", Trim$(Txt_Add_Moo.Text)) & "','" & IIf(Trim$(Txt_Add_Soi.Text) = Empty, "-", Trim$(Txt_Add_Soi.Text)) & _
'                                                         "','" & IIf(Trim$(Txt_Add_Road.Text) = Empty, "-", Trim$(Txt_Add_Road.Text)) & "','" & IIf(Trim$(Txt_Add_Telephone.Text) = Empty, "-", Trim$(Txt_Add_Telephone.Text)) & "','" & IIf(Trim$(Txt_Email.Text) = Empty, "-", Trim$(Txt_Email.Text)) & "','" & IIf(Trim$(Cmb_Tambon_ID.Text) = Empty, "-", Trim$(Cmb_Tambon_ID.Text)) & "','" & Trim$(Txt_Owner_Number.Text) & "','" & _
'                                                         Txt_ID_Gard(1).Text & Txt_ID_Gard(2).Text & Txt_ID_Gard(3).Text & Txt_ID_Gard(4).Text & Txt_ID_Gard(5).Text & "'," & INDEXS & ",'" & IIf(Trim$(Cmb_Tambon.Text) = Empty, "-", Trim$(Cmb_Tambon.Text)) & "','" & IIf(Trim$(Cmb_Amphoe.Text) = Empty, "-", Trim$(Cmb_Amphoe.Text)) & "','" & IIf(Trim$(Cmb_Province.Text) = Empty, "-", Trim$(Cmb_Province.Text)) & "','" & IIf(Trim$(Txt_ZipCode.Text) = Empty, "-", Trim$(Txt_ZipCode.Text)) & "')"
'                                  ElseIf STATE = "EDIT" Then
'                                        sql_txt = " UPDATE OWNERSHIP SET OWNERSHIP_REAL_ID = '" & Trim$(Txt_Ownership_ID.Text) & "', PRENAME = '" & Trim$(Combo1.Text) & "', OWNER_NAME = '" & Trim$(Txt_Name.Text) & _
'                                                            "', OWNER_SURNAME = '" & Trim$(Txt_Surname.Text) & "', ADD_HOME = '" & Trim$(Txt_Add_Home.Text) & "', ADD_MOO = '" & Trim$(Txt_Add_Moo.Text) & "', ADD_SOI  = '" & Trim$(Txt_Add_Soi.Text) & _
'                                                            "', ADD_ROAD =  '" & Trim$(Txt_Add_Road.Text) & "', TELEPHONE = '" & Trim$(Txt_Add_Telephone.Text) & "', EMAIL = '" & Trim$(Txt_Email.Text) & "', TAMBON_ID = '" & Cmb_Tambon_ID.Text & _
'                                                            "' , OWNER_NUMBER = '" & Txt_Owner_Number.Text & "', ID_GARD  = '" & Txt_ID_Gard(1).Text & Txt_ID_Gard(2).Text & Txt_ID_Gard(3).Text & Txt_ID_Gard(4).Text & Txt_ID_Gard(5).Text & _
'                                                            "', OWNER_TYPE  = " & INDEXS & ",TAMBON_NAME = '" & Cmb_Tambon.Text & "', AMPHOE_NAME = '" & Cmb_Amphoe.Text & "', PROVINCE_NAME = '" & Cmb_Province.Text & "',ZIPCODE = '" & Txt_ZipCode.Text & "'" & _
'                                                            " WHERE OWNERSHIP_ID = '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 16) & "'"
'                                  ElseIf STATE = "DEL" Then
'                                        sql_txt = "DELETE  FROM OWNERSHIP " & _
'                                                         " WHERE OWNERSHIP_ID = '" & Grid_OwnerShip.TextMatrix(Grid_OwnerShip.Row, 16) & "'"
'                                  End If
        OwnershipId = GRID_OWNERSHIP.TextMatrix(GRID_OWNERSHIP.Row, 16)
'                                  Call SET_Execute(sql_txt)
End Sub

Private Sub Btn_Add_Click()
        If TProduct = "DEMO" Then
             If SumCheckOwner >= 500 Then
                  MsgBox "Microtax Demo Version Limit  to 500 record maximum", vbExclamation, "Microtax"
                  Exit Sub
             End If
        End If
        Call SET_TEXTBOX("ADD", "Perpose")
        Status = "POST"
        If Op_Type(1).Value Then
                 Combo1.SetFocus
        Else
                  Txt_Name.SetFocus
        End If
End Sub

Private Sub Btn_Cancel_Click()
        Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If MsgBox("�׹�ѹ���ź������ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE("DEL")
        Call SET_TEXTBOX("DEL")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        If Err.Number = -2147467259 Then
                MsgBox "�������öź�������� ���ͧ�ҡ�ѧ�ժ��͹��㹷���¹���Թ ���� �ç���͹ ���� ���� ���� �͹حҵ", vbCritical, "��ͼԴ��Ҵ"
        Else
                MsgBox Err.Number & "-" & Err.Description, vbExclamation, "��ͼԴ��Ҵ"
        End If
End Sub

Private Sub Btn_Edit_Click()
        Call GRID_OWNERSHIP_RowColChange
        Call SET_TEXTBOX("EDIT", "PERPOSE")
        Status = "EDIT"
End Sub

Private Sub Btn_Post_Click()
            Dim i As Integer
            
             If IDNOCHECK(Txt_ID_Gard(1).Text & Txt_ID_Gard(2).Text & Txt_ID_Gard(3).Text & Txt_ID_Gard(4).Text & Txt_ID_Gard(5).Text) = False And LenB(Trim$((Txt_ID_Gard(1).Text & Txt_ID_Gard(2).Text & Txt_ID_Gard(3).Text & Txt_ID_Gard(4).Text & Txt_ID_Gard(5).Text))) > 0 Then
                    MsgBox "���ʻ�ЪҪ����١��ͧ ��س�����������١��ͧ���� !!!", vbInformation, "����͹"
                    For i = 1 To 5
                            Txt_ID_Gard(i).Text = ""
                    Next i
                    Txt_ID_Gard(1).SetFocus
                    Exit Sub
             End If
            Globle_Connective.BeginTrans
            On Error GoTo ErrHandler
           
            Call SET_DATABASE(Status)
            Globle_Connective.CommitTrans
            On Error GoTo ErrHandler2
            Call SET_TEXTBOX("POST")
            Call SET_REFRESH
            If GBQueryOwnerShip.RecordCount > 0 Then
                    GBQueryOwnerShip.MoveFirst
                    GBQueryOwnerShip.Find "OWNERSHIP_ID = '" & OwnershipId & "'", , adSearchForward
                    If GBQueryOwnerShip.AbsolutePosition > 0 Then
                            GRID_OWNERSHIP.TopRow = GBQueryOwnerShip.AbsolutePosition
                            GRID_OWNERSHIP.Row = GBQueryOwnerShip.AbsolutePosition
                    End If
                            GRID_OWNERSHIP.Col = 0
                            GRID_OWNERSHIP.ColSel = GRID_OWNERSHIP.Cols - 1
                            GRID_OWNERSHIP.SetFocus
            End If

Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Exit Sub
ErrHandler2:
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Refresh_Click()
        Call SET_REFRESH
        GBQueryProvince.Requery
        Cmb_Province.Clear
        Cmb_Province_ID.Clear
        If GBQueryProvince.RecordCount > 0 Then
                GBQueryProvince.MoveFirst
                Do While Not GBQueryProvince.EOF
                        Cmb_Province.AddItem GBQueryProvince.Fields("Province_Name").Value
                        Cmb_Province_ID.AddItem GBQueryProvince.Fields("Province_ID").Value
                        GBQueryProvince.MoveNext
                Loop
                Cmb_Province.ListIndex = 0
        End If
End Sub

'Private Sub Btn_Search_Click()
'        Status_InToFrm = "OW_SHP"
'        Frm_SearchOwnerShip.Show vbModal
'        Call Set_Grid
'End Sub

Private Sub BtnClear_Click()
    On Error GoTo ErrClear
        txtNm.Text = ""
        txtSurNm.Text = ""
        With GRID_OWNERSHIP
                    Set .DataSource = Nothing
                    .Rows = 2
                    .Clear
                    If Op_Type(1).Value = True Then
                            .ColWidth(0) = 700:    .TextArray(0) = "�ӹ��"
                            .ColWidth(2) = 3000:    .TextArray(2) = "���ʡ��"
                            .ColWidth(4) = 1500: .ColAlignmentFixed(4) = 4:    .TextArray(4) = "�ѵû�ЪҪ�"
                            .ColWidth(3) = 0:     .TextArray(3) = ""
                    Else
                            .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4:     .TextArray(3) = "�Ţ������¹"
                            .ColWidth(0) = 0 ':    .TextArray(0) = ""
                            .ColWidth(2) = 0 ':     .TextArray(2) = ""
                            .ColWidth(4) = 0 ':    .TextArray(4) = ""
                    End If
            
                    .ColWidth(1) = 3500:   .TextArray(1) = "���� - " & LB_Type(INDEXS).Caption
                    .ColWidth(5) = 1200: .ColAlignmentFixed(5) = 4:     .TextArray(5) = "��ҹ�Ţ���"
                    .ColWidth(6) = 1400: .ColAlignmentFixed(6) = 4:     .TextArray(6) = "����/����ʶҹ���"
                    .ColWidth(7) = 1400: .ColAlignmentFixed(7) = 4:     .TextArray(7) = "���"
                    .ColWidth(8) = 1400: .ColAlignmentFixed(8) = 4:    .TextArray(8) = "���"
                    .ColWidth(9) = 1400: .ColAlignmentFixed(9) = 4:    .TextArray(9) = "�Ӻ�"
                    .ColWidth(10) = 1400: .ColAlignmentFixed(10) = 4:     .TextArray(10) = "�����"
                    .ColWidth(11) = 1400: .ColAlignmentFixed(11) = 4:     .TextArray(11) = "�ѧ��Ѵ"
                    .ColWidth(12) = 1700: .ColAlignmentFixed(12) = 4:     .TextArray(12) = "���Ѿ��"
                    .ColWidth(13) = 1500: .ColAlignmentFixed(13) = 4:     .TextArray(13) = "������"
                    .ColWidth(14) = 0
                    .ColWidth(15) = 0
                    .ColWidth(16) = 0
                    .ColWidth(17) = 0 'TAMBON_ID
                    .ColWidth(18) = 1500: .ColAlignmentFixed(18) = 4:     .TextArray(18) = "��ɳ���"
            End With
            Exit Sub
ErrClear:
End Sub

Private Sub BtnSearch_Click()
        Dim index_type As Byte
        Select Case ComboTypeSch.Text
                    Case "���� - ʡ��"
                            index_type = 0
                    Case "����"
                            index_type = 1
                    Case "�ѵû�ЪҪ�"
                            index_type = 2
                    Case "����¹��ä��"
                            index_type = 3
                    Case "���ʪ���"
                            index_type = 4
        End Select
        On Error GoTo ErrSearch
        Set Query = Globle_Connective.Execute("exec sp_search_ownership2 " & index_type & ",'" & txtNm.Text & "','" & txtSurNm.Text & "','" & INDEXS & "'", , adCmdUnknown)
        If Query.RecordCount > 0 Then
            Set GRID_OWNERSHIP.DataSource = Query
            lbRecordCount.Caption = Format$(Query.RecordCount, "###,###")
        Else
            Set GRID_OWNERSHIP.DataSource = Nothing
            lbRecordCount.Caption = "0"
            GRID_OWNERSHIP.Rows = 2
            GRID_OWNERSHIP.Clear
        End If
        Exit Sub
ErrSearch:
        MsgBox Err.Description
End Sub

Private Sub Cmb_Amphoe_Click()
    Cmb_Amphoe_ID.ListIndex = Cmb_Amphoe.ListIndex
    If LenB(Trim$(Cmb_Amphoe.Text)) > 0 Then
            Cmb_Tambon.Clear
            Cmb_Tambon_ID.Clear
            Cmb_PostCode.Clear
            GBQueryTambon.Filter = " AMPHOE_ID = '" & Cmb_Amphoe_ID.Text & "'"
            If GBQueryTambon.RecordCount > 0 Then
                    GBQueryTambon.MoveFirst
                    Do While Not GBQueryTambon.EOF
                            Cmb_Tambon.AddItem GBQueryTambon.Fields("�Ӻ�").Value
                            Cmb_Tambon_ID.AddItem GBQueryTambon.Fields("���ʷ����").Value
                            Cmb_PostCode.AddItem IIf(IsNull(GBQueryTambon.Fields("ZIPCODE").Value), "", GBQueryTambon.Fields("ZIPCODE").Value)
                            GBQueryTambon.MoveNext
                    Loop
                    Cmb_Tambon.ListIndex = 0
            End If
    End If
End Sub

Private Sub Cmb_Amphoe_GotFocus()
    LngResult = SendMessage(Cmb_Amphoe.hWnd, CB_SHOWDROPDOWN, 1, ByVal CLng(0))
End Sub

Private Sub Cmb_Amphoe_KeyPress(KeyAscii As Integer)
          KeyAscii = AutoFind(Cmb_Amphoe, KeyAscii, False)
          If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Cmb_Province_Click()
    Cmb_Province_ID.ListIndex = Cmb_Province.ListIndex
    If LenB(Trim$(Cmb_Province.Text)) > 0 Then
            Cmb_Amphoe.Clear
            Cmb_Amphoe_ID.Clear
            GBQueryAmphoe.Filter = " PROVINCE_ID = '" & Cmb_Province_ID.Text & "'"
            If GBQueryAmphoe.RecordCount > 0 Then
                    GBQueryAmphoe.MoveFirst
                    Do While Not GBQueryAmphoe.EOF
                                Cmb_Amphoe.AddItem GBQueryAmphoe.Fields("Amphoe_Name").Value
                                Cmb_Amphoe_ID.AddItem GBQueryAmphoe.Fields("Amphoe_ID").Value
                                GBQueryAmphoe.MoveNext
                    Loop
                    Cmb_Amphoe.ListIndex = 0
            End If
    End If
End Sub

Private Sub Cmb_Province_GotFocus()
        LngResult = SendMessage(Cmb_Province.hWnd, CB_SHOWDROPDOWN, 1, ByVal CLng(0))
End Sub

Private Sub Cmb_Province_KeyPress(KeyAscii As Integer)
        KeyAscii = AutoFind(Cmb_Province, KeyAscii, False)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Cmb_Tambon_Click()
        Cmb_Tambon_ID.ListIndex = Cmb_Tambon.ListIndex
        Cmb_PostCode.ListIndex = Cmb_Tambon_ID.ListIndex
        Txt_ZipCode.Text = Cmb_PostCode.Text
End Sub

Private Sub Cmb_Tambon_GotFocus()
        LngResult = SendMessage(Cmb_Tambon.hWnd, CB_SHOWDROPDOWN, 1, ByVal CLng(0))
End Sub

Private Sub Cmb_Tambon_KeyPress(KeyAscii As Integer)
        KeyAscii = AutoFind(Cmb_Tambon, KeyAscii, False)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Combo1_GotFocus()
      LngResult = SendMessage(Combo1.hWnd, CB_SHOWDROPDOWN, 1, ByVal CLng(0))
End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)
 If KeyAscii = 13 Then SendKeys "{Tab}"
        If Btn_Add.Enabled <> False Or Btn_Edit.Enabled <> False Then
                KeyAscii = 0
                Exit Sub
        End If
 If INDEXS = 1 Then
        If Combo1.ListIndex <> 5 And Combo1.ListIndex <> -1 Then
                KeyAscii = 0
        End If
End If

If INDEXS = 2 Then
        If Combo1.ListIndex <> 2 And Combo1.ListIndex <> -1 Then
                KeyAscii = 0
        End If
End If

If INDEXS = 3 Then
        If Combo1.ListIndex <> 1 And Combo1.ListIndex <> -1 Then
                KeyAscii = 0
        End If
End If
End Sub

Private Sub ComboTypeSch_Click()
        On Error Resume Next
'        txtIDCard.Text = ""
        txtNm.Text = ""
        txtSurNm.Text = ""
        Select Case ComboTypeSch.Text
                    Case "���� - ʡ��"
                                txtNm.Left = txtNm.Tag
                                txtNm.Width = txtSurNm.Tag
                                txtNm.MaxLength = 250
                                txtSurNm.Visible = True
                                Label2(3).Caption = "���� :"
                                Label2(17).Visible = True
                                Label2(17).Caption = "���ʡ�� :"
                    Case "����"
                                Label2(3).Caption = "���� :"
                                txtNm.Left = txtNm.Tag
                                txtNm.Width = txtSurNm.Tag + 3690
                                txtSurNm.Visible = False
                                Label2(17).Visible = False
                    Case "�ѵû�ЪҪ�"
                                Label2(3).Caption = "�ѵû�ЪҪ� :"
                                txtNm.Left = txtNm.Tag + 900
                                txtNm.Width = txtSurNm.Tag + 2790
                                txtNm.MaxLength = 13
                                txtSurNm.Visible = False
                                Label2(17).Visible = False
                    Case "����¹��ä��"
                                Label2(3).Caption = "����¹��ä�� :"
                                txtNm.Left = txtNm.Tag + 900
                                txtNm.Width = txtSurNm.Tag + 2790
                                txtNm.MaxLength = 15
                                txtSurNm.Visible = False
                                Label2(17).Visible = False
                    Case "���ʪ���"
                                Label2(3).Caption = "���ʪ��� :"
                                txtNm.MaxLength = 250
                                txtNm.Left = txtNm.Tag + 330
                                txtNm.Width = txtSurNm.Tag + 3360
                                txtSurNm.Visible = False
                                Label2(17).Visible = False
        End Select
End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
        Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()
        Op_Type(1).Value = True
        Call Btn_Refresh_Click
        txtNm.Tag = txtNm.Left
        txtSurNm.Tag = txtNm.Width
        Set Query = New ADODB.Recordset
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        WheelUnHook
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Frm_Ownership = Nothing
        Set Query = Nothing
End Sub

Private Sub GRID_OWNERSHIP_Click()
        Call GRID_OWNERSHIP_RowColChange
End Sub

Private Sub Grid_OwnerShip_LostFocus()
        WheelUnHook
End Sub

Private Sub GRID_OWNERSHIP_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        WheelUnHook
        WheelHook Frm_Ownership, Frm_Ownership.GRID_OWNERSHIP
End Sub

Private Sub GRID_OWNERSHIP_RowColChange()
On Error Resume Next
If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then
With GRID_OWNERSHIP
    DoEvents
     Txt_Name.Text = .TextMatrix(.Row, 1)
     Txt_Surname.Text = .TextMatrix(.Row, 2)
     Txt_Owner_Number.Text = .TextMatrix(.Row, 3)
     Txt_ID_Gard(1).Text = Mid$(.TextMatrix(.Row, 4), 1, 1)
     Txt_ID_Gard(2).Text = Mid$(.TextMatrix(.Row, 4), 2, 4)
     Txt_ID_Gard(3).Text = Mid$(.TextMatrix(.Row, 4), 6, 5)
     Txt_ID_Gard(4).Text = Mid$(.TextMatrix(.Row, 4), 11, 2)
     Txt_ID_Gard(5).Text = Mid$(.TextMatrix(.Row, 4), 13, 1)
     Txt_Add_Home.Text = .TextMatrix(.Row, 5)
     Txt_Add_Moo.Text = .TextMatrix(.Row, 6)
     Txt_Add_Soi.Text = .TextMatrix(.Row, 7)
     Txt_Add_Road.Text = .TextMatrix(.Row, 8)
     Cmb_Tambon.Text = .TextMatrix(.Row, 9)
     Cmb_Amphoe.Text = .TextMatrix(.Row, 10)
     Cmb_Province.Text = .TextMatrix(.Row, 11)
     Txt_Add_Telephone.Text = .TextMatrix(.Row, 12)
     Txt_Email.Text = .TextMatrix(.Row, 13)
     Combo1.ListIndex = 5
     Combo1.Text = .TextMatrix(.Row, 0)
     Txt_OwnerShip_ID.Text = .TextMatrix(.Row, 15)
     
     Cmb_Tambon_ID.Text = .TextMatrix(.Row, 17)
     Cmb_Amphoe_ID.Text = .TextMatrix(.Row, 18)
     Cmb_Province_ID.Text = .TextMatrix(.Row, 19)
     Txt_ZipCode.Text = .TextMatrix(.Row, 18)
     GRID_OWNERSHIP.SetFocus
End With
End If
End Sub

Private Sub LB_Type_Click(Index As Integer)
        Op_Type(Index).Value = True
End Sub

Private Sub Op_Type_Click(Index As Integer)
On Error Resume Next
        Me.MousePointer = 11
        Call SET_TEXTBOX("CANCEL")
        INDEXS = Index
        Label2(6).Caption = "����" & LB_Type(Index).Caption
        Dim i As Byte
        For i = 1 To 9
                LB_Type(i).Font.Bold = False
                LB_Type(i).ForeColor = &H0&
                Txt_ID_Gard(i).Enabled = False
        Next i
        LB_Type(Index).Font.Bold = True
        LB_Type(Index).ForeColor = &H68629
        Combo1.Clear
        Combo1.Text = ""
        If Index = 1 Then
                Combo1.AddItem "���", 0
                Combo1.AddItem "�ҧ", 1
                Combo1.AddItem "�ҧ���", 2
                Combo1.AddItem "�硪��", 3
                Combo1.AddItem "��˭ԧ", 4
                Combo1.AddItem "��� �", 5
        ElseIf Index = 2 Then
                Combo1.AddItem "���.", 0
                Combo1.AddItem "���.", 1
                Combo1.AddItem "��� �", 2
        ElseIf Index = 3 Then
                Combo1.AddItem "˨�.", 0
                Combo1.AddItem "��� �", 1
        End If
        Txt_Owner_Number.Enabled = True
        Lb_OwnerType_Name(2).Caption = "��Ңͧ�����Է���" & " : " & LB_Type(Index).Caption
        Txt_Surname.Enabled = False

    GBQueryOwnerShip.Filter = " OWNER_TYPE = " & Index
If GBQueryOwnerShip.RecordCount > 0 Then
       Set GRID_OWNERSHIP.DataSource = GBQueryOwnerShip
Else
     Set GRID_OWNERSHIP.DataSource = Nothing
            GRID_OWNERSHIP.Rows = 2
            GRID_OWNERSHIP.Clear
End If
        ComboTypeSch.Clear
        Select Case Index
                     Case 1
                                Combo1.ListIndex = 0
                                Txt_Surname.Enabled = True
                                Txt_Owner_Number.Enabled = False
                                For i = 1 To 5
                                        Txt_ID_Gard(i).Enabled = True
                                Next i
                                ComboTypeSch.AddItem "���� - ʡ��"
                                ComboTypeSch.AddItem "�ѵû�ЪҪ�"
                                ComboTypeSch.AddItem "���ʪ���"
                      Case Else
                                Combo1.ListIndex = 0
                                ComboTypeSch.AddItem "����"
                                ComboTypeSch.AddItem "����¹��ä��"
                                ComboTypeSch.AddItem "���ʪ���"
        End Select
        ComboTypeSch.ListIndex = 0
        Call Set_Grid
        With GRID_OWNERSHIP
                    If Op_Type(1).Value = True Then
:                               .TextArray(0) = "�ӹ��"
                                .TextArray(2) = "���ʡ��"
                                .TextArray(4) = "�ѵû�ЪҪ�"
                    Else
                            .TextArray(3) = "�Ţ������¹"
                    End If
                    .TextArray(5) = "��ҹ�Ţ���"
                    .TextArray(6) = "����/����ʶҹ���"
                    .TextArray(7) = "���"
                    .TextArray(8) = "���"
                    .TextArray(9) = "�Ӻ�"
                    .TextArray(10) = "�����"
                    .TextArray(11) = "�ѧ��Ѵ"
                    .TextArray(12) = "���Ѿ��"
                    .TextArray(13) = "������"
                    .TextArray(18) = "��ɳ���"
            End With
         Me.MousePointer = 0
End Sub

'Private Sub Text1_Validate(Cancel As Boolean)
'        MsgBox Cancel
'End Sub

Private Sub Txt_Add_Home_GotFocus()
      Txt_Add_Home.SelStart = 0
      Txt_Add_Home.SelLength = Len(Txt_Add_Home.Text)
End Sub

Private Sub Txt_Add_Home_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Add_Moo_GotFocus()
      Txt_Add_Moo.SelStart = 0
      Txt_Add_Moo.SelLength = Len(Txt_Add_Moo.Text)
End Sub

Private Sub Txt_Add_Moo_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Add_Road_GotFocus()
      Txt_Add_Road.SelStart = 0
      Txt_Add_Road.SelLength = Len(Txt_Add_Road.Text)
End Sub

Private Sub Txt_Add_Road_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Add_Soi_GotFocus()
        Txt_Add_Soi.SelStart = 0
        Txt_Add_Soi.SelLength = Len(Txt_Add_Soi.Text)
End Sub

Private Sub Txt_Add_Soi_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Add_Telephone_GotFocus()
        Txt_Add_Telephone.SelStart = 0
        Txt_Add_Telephone.SelLength = Len(Txt_Add_Telephone.Text)
End Sub

Private Sub Txt_Add_Telephone_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Email_GotFocus()
        Txt_Email.SelStart = 0
        Txt_Email.SelLength = Len(Txt_Email.Text)
End Sub

Private Sub Txt_ID_Gard_Change(Index As Integer)
        Select Case Index
                     Case 1
                                If Len(Txt_ID_Gard(1).Text) = 1 Then Txt_ID_Gard(Index + 1).SetFocus
                     Case 2
                                If Len(Txt_ID_Gard(2).Text) = 4 Then Txt_ID_Gard(Index + 1).SetFocus
                     Case 3
                                If Len(Txt_ID_Gard(3).Text) = 5 Then Txt_ID_Gard(Index + 1).SetFocus
                     Case 4
                                If Len(Txt_ID_Gard(4).Text) = 2 Then Txt_ID_Gard(Index + 1).SetFocus
        End Select
End Sub

Private Sub Txt_ID_Gard_GotFocus(Index As Integer)
      Txt_ID_Gard(Index).SelStart = 0
      Txt_ID_Gard(Index).SelLength = Len(Txt_ID_Gard(Index).Text)
End Sub

Private Sub Txt_ID_Gard_KeyPress(Index As Integer, KeyAscii As Integer)
        On Error Resume Next
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
        If Index = 5 Then
            Txt_Add_Home.SetFocus
            Exit Sub
        End If
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Name_GotFocus()
      Txt_Name.SelStart = 0
      Txt_Name.SelLength = Len(Txt_Name.Text)
End Sub

Private Sub Txt_Name_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Name_Validate(Cancel As Boolean)
        Cancel = CHECK_NAME
End Sub

Private Sub Txt_Owner_Number_GotFocus()
      Txt_Owner_Number.SelStart = 0
      Txt_Owner_Number.SelLength = Len(Txt_Owner_Number.Text)
End Sub

Private Sub Txt_Owner_Number_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_OwnerShip_ID_Change()
        Dim i As Byte, j As Long
    
        For i = 1 To Len(Txt_OwnerShip_ID.Text)
                j = j + TextWidth(Mid$(Txt_OwnerShip_ID.Text, i, 1))
        Next i
        If j > 795 Then
                Txt_OwnerShip_ID.Width = 1725 + (j - 700)
                Shape1(0).Width = 1727 + (j - 698)
        Else
                Txt_OwnerShip_ID.Width = 1725
                Shape1(0).Width = 1727
        End If
End Sub

Private Sub Txt_Surname_GotFocus()
        Txt_Surname.SelStart = 0
        Txt_Surname.SelLength = Len(Txt_Surname.Text)
End Sub

Private Sub Txt_Surname_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Surname_Validate(Cancel As Boolean)
        Cancel = CHECK_NAME
End Sub
Private Sub Txt_ZipCode_GotFocus()
        Txt_ZipCode.SelStart = 0
        Txt_ZipCode.SelLength = Len(Txt_ZipCode.Text)
End Sub

Private Sub Txt_ZipCode_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub txtNm_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Call BtnSearch_Click
        End If
        If ComboTypeSch.Text = "�ѵû�ЪҪ�" Or ComboTypeSch.Text = "����¹��ä��" Then
                KeyAscii = KeyCharecter(KeyAscii, "0123456789")
        End If
End Sub

Private Sub txtSurNm_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Call BtnSearch_Click
        End If
End Sub
