VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_StoryPayInstallment 
   Caption         =   "Lb_ResultSchNm"
   ClientHeight    =   9015
   ClientLeft      =   60
   ClientTop       =   555
   ClientWidth     =   12885
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_StoryPayInstallment.frx":0000
   ScaleHeight     =   9015
   ScaleMode       =   0  'User
   ScaleWidth      =   12885
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtYear 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   5040
      Locked          =   -1  'True
      TabIndex        =   36
      Top             =   30
      Width           =   660
   End
   Begin VB.ComboBox ComboInstallmentID 
      Height          =   315
      Left            =   5400
      TabIndex        =   28
      Top             =   1500
      Width           =   2175
   End
   Begin VB.Frame FrameSchNm 
      BackColor       =   &H80000012&
      BorderStyle     =   0  'None
      Height          =   390
      Left            =   6720
      TabIndex        =   23
      Top             =   480
      Width           =   6180
      Begin VB.TextBox txtNm 
         Height          =   315
         Left            =   650
         TabIndex        =   25
         Top             =   35
         Width           =   2055
      End
      Begin VB.TextBox txtSurNm 
         Height          =   315
         Left            =   3840
         TabIndex        =   24
         Top             =   35
         Width           =   2055
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000F&
         Height          =   240
         Index           =   3
         Left            =   165
         TabIndex        =   27
         Top             =   85
         Width           =   345
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���ʡ�� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000F&
         Height          =   240
         Index           =   5
         Left            =   2880
         TabIndex        =   26
         Top             =   85
         Width           =   780
      End
   End
   Begin VB.TextBox txtIDCard 
      Height          =   315
      Left            =   6960
      TabIndex        =   22
      Top             =   510
      Width           =   5500
   End
   Begin VB.ComboBox ComboTypeSch 
      Height          =   315
      ItemData        =   "Frm_StoryPayInstallment.frx":18CD7C
      Left            =   4440
      List            =   "Frm_StoryPayInstallment.frx":18CD8C
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   510
      Width           =   2295
   End
   Begin VB.ComboBox cmb_SignBoard_Owner 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   315
      ItemData        =   "Frm_StoryPayInstallment.frx":18CDC3
      Left            =   960
      List            =   "Frm_StoryPayInstallment.frx":18CDE5
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   510
      Width           =   2415
   End
   Begin VB.ComboBox Cmb_StoryAll 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayInstallment.frx":18CE4C
      Left            =   8640
      List            =   "Frm_StoryPayInstallment.frx":18CE5C
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   60
      Width           =   3615
   End
   Begin VB.ComboBox cbArticle 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_StoryPayInstallment.frx":18CEC4
      Left            =   6150
      List            =   "Frm_StoryPayInstallment.frx":18CED1
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   60
      Width           =   2475
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_SumNum 
      Height          =   1200
      Left            =   0
      TabIndex        =   8
      Top             =   5955
      Width           =   3525
      _ExtentX        =   6218
      _ExtentY        =   2117
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   3
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   3
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_SumPrice 
      Height          =   1200
      Left            =   0
      TabIndex        =   11
      Top             =   7480
      Width           =   3525
      _ExtentX        =   6218
      _ExtentY        =   2117
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   3
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      HighLight       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   3
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   2325
      Left            =   3555
      TabIndex        =   34
      Top             =   1920
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   4101
      _Version        =   393216
      BackColor       =   16777215
      Cols            =   12
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   12
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Installment 
      Height          =   1425
      Left            =   3585
      TabIndex        =   35
      Top             =   6645
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   2514
      _Version        =   393216
      BackColor       =   16777215
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   14933984
      BackColorBkg    =   14933984
      GridColor       =   11911361
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   360
      Left            =   5670
      TabIndex        =   37
      Top             =   30
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   635
      _Version        =   393216
      Value           =   2000
      BuddyControl    =   "txtYear"
      BuddyDispid     =   196609
      OrigLeft        =   1800
      OrigTop         =   30
      OrigRight       =   2055
      OrigBottom      =   345
      Max             =   3500
      Min             =   2000
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_OwnerShip 
      Height          =   4335
      Left            =   0
      TabIndex        =   38
      Top             =   960
      Width           =   3525
      _ExtentX        =   6218
      _ExtentY        =   7646
      _Version        =   393216
      BackColor       =   -2147483624
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   0
      ForeColorSel    =   255
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      RowSizingMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Label Lb_Discount 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   315
      Left            =   9675
      TabIndex        =   42
      Top             =   4995
      Width           =   2355
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Թ����Ŵ :                                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   1
      Left            =   8640
      TabIndex        =   41
      Top             =   4950
      Width           =   3930
   End
   Begin VB.Label Lb_AllPrice 
      Alignment       =   2  'Center
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   225
      Left            =   1215
      TabIndex        =   40
      Top             =   8720
      Width           =   1785
   End
   Begin VB.Label Lb_ResultSchNm 
      Alignment       =   2  'Center
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   220
      Left            =   1440
      TabIndex        =   39
      Top             =   5320
      Width           =   1425
   End
   Begin VB.Label Lb_Tax 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   315
      Left            =   5520
      TabIndex        =   33
      Top             =   5620
      Width           =   2355
   End
   Begin VB.Label Lb_AllPricePay 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   315
      Left            =   9675
      TabIndex        =   32
      Top             =   5620
      Width           =   2355
   End
   Begin VB.Label LbArrear 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080C0FF&
      Height          =   315
      Left            =   5520
      TabIndex        =   31
      Top             =   4950
      Width           =   2355
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��һ�Ѻ��� :                                             �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   48
      Left            =   4350
      TabIndex        =   30
      Top             =   5610
      Width           =   4050
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Left            =   12270
      Picture         =   "Frm_StoryPayInstallment.frx":18CF16
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   8190
      Width           =   480
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ�������繧Ǵ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   32
      Left            =   3720
      TabIndex        =   29
      Tag             =   "�Ţ���Ǩ��� :"
      Top             =   1530
      Width           =   1590
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   450
      Index           =   1
      Left            =   3555
      Shape           =   4  'Rounded Rectangle
      Top             =   1425
      Width           =   4275
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   "��¡�çǴ����������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   5
      Left            =   3540
      TabIndex        =   21
      Top             =   6330
      Width           =   9375
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� :                                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   9
      Left            =   9030
      TabIndex        =   20
      Top             =   5610
      Width           =   3555
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ҥһ����Թ :                                            �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   4
      Left            =   4230
      TabIndex        =   19
      Top             =   4950
      Width           =   4155
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   1575
      Index           =   4
      Left            =   3555
      Shape           =   4  'Rounded Rectangle
      Top             =   4665
      Width           =   9255
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "�ӹǹ��Ѿ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   1
      Left            =   3555
      TabIndex        =   18
      Top             =   4280
      Width           =   1635
   End
   Begin VB.Label lbRecordCount 
      Alignment       =   2  'Center
      BackColor       =   &H00E3DFE0&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   5220
      TabIndex        =   17
      Top             =   4275
      Width           =   1410
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��ػ�ʹ�Թ :                                  �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   20
      TabIndex        =   16
      Top             =   8720
      Width           =   3465
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00EBEBE7&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   8680
      Width           =   3525
   End
   Begin VB.Label LbTaxNm 
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   4800
      TabIndex        =   15
      Top             =   1080
      Width           =   4095
   End
   Begin VB.Label Lb_Name 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   3720
      TabIndex        =   14
      Top             =   1060
      Width           =   1080
   End
   Begin VB.Label LB_Menu 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���պ��ا��ͧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   10320
      TabIndex        =   13
      Top             =   1060
      Width           =   2325
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "����������  :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   4
      Left            =   9120
      TabIndex        =   12
      Top             =   1060
      Width           =   1155
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   465
      Left            =   3525
      Shape           =   4  'Rounded Rectangle
      Top             =   960
      Width           =   9375
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   ":: �Ҥ���������繧Ǵ ::"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   675
      TabIndex        =   10
      Top             =   7200
      Width           =   2145
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   7140
      Width           =   3525
   End
   Begin VB.Label LabelTile1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   ":: �ӹǹ���շ�����Ẻ�Ǵ ::"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Left            =   375
      TabIndex        =   9
      Top             =   5670
      Width           =   2460
   End
   Begin VB.Shape ShapeTitle1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   5610
      Width           =   3525
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�÷��鹾� :                       ��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   27
      Left            =   100
      TabIndex        =   7
      Top             =   5340
      Width           =   3330
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00EBEBE7&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   5300
      Width           =   3525
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   4
      Left            =   3600
      TabIndex        =   6
      Top             =   555
      Width           =   720
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   3
      Left            =   120
      TabIndex        =   4
      Top             =   555
      Width           =   765
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   2
      Left            =   4200
      TabIndex        =   2
      Top             =   120
      Width           =   765
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   420
      Left            =   12360
      Picture         =   "Frm_StoryPayInstallment.frx":18F29A
      Stretch         =   -1  'True
      ToolTipText     =   "����"
      Top             =   0
      Width           =   480
   End
End
Attribute VB_Name = "Frm_StoryPayInstallment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Rs As New ADODB.Recordset
Dim strSQL As String
Dim strOwnerShipID As String


Private Sub ComboInstallmentID_Click()
        Call displayDetailPayment
        Call countNumAsset(LB_Menu.Tag)
End Sub

Private Sub displayDetailPayment()
        Dim i As Integer
'                                 Debug.Print "exec sp_search_DataInstallment " & txtYear.Text & ",'" & strOwnerShipID & "','" & ComboInstallmentID.Text & "', " & LB_Menu.Tag
                                Set Rs = Globle_Connective.Execute("exec sp_search_DataInstallment " & txtYear.Text & ",'" & strOwnerShipID & "','" & ComboInstallmentID.Text & "', " & LB_Menu.Tag, adCmdUnknown)
    
                                 If Rs.RecordCount > 0 Then
                                            Set Grid_Result.DataSource = Rs
                                            With Grid_Result
                                                For i = 1 To Grid_Result.Rows - 1
                                                               .TextMatrix(i, 3) = IIf(IsNull(Rs.Fields(3).Value), "", Rs.Fields(3).Value)
                                                               .TextMatrix(i, 7) = IIf(IsNull(Rs.Fields(7).Value), "", Rs.Fields(7).Value)
                                                Next i
                                            End With
                                End If
                                
                                Debug.Print "exec sp_get_sumPriceInstallment " & txtYear.Text & ",'" & strOwnerShipID & "','" & ComboInstallmentID.Text & "', " & LB_Menu.Tag
                                Set Rs = Globle_Connective.Execute("exec sp_get_sumPriceInstallment " & txtYear.Text & ",'" & strOwnerShipID & "','" & ComboInstallmentID.Text & "', " & LB_Menu.Tag, adCmdUnknown)
    
                                 If Rs.RecordCount > 0 Then
                                        LbArrear.Caption = Format$(Rs.Fields(0), "#,##0.00")
                                        Lb_Discount.Caption = Format$(Rs.Fields(1), "#,##0.00")
                                        Lb_Tax.Caption = Format$(Rs.Fields(2), "#,##0.00")
                                        Lb_AllPricePay = Format$(Rs("sumAll"), "#,##0.00")
                                 End If
                                 
'                     Debug.Print "exec sp_search_Installment  '" & ComboInstallmentID.Text & "'," & txtYear.Text & ",'" & LB_Menu.Tag & "'"
                    Set Rs = Globle_Connective.Execute("exec sp_search_Installment  '" & ComboInstallmentID.Text & "'," & txtYear.Text & ",'" & LB_Menu.Tag & "'", adCmdUnknown)
                
                        Set Grid_Installment.DataSource = Rs
                        With Grid_Installment
                        i = 1
                                        Do While Not Rs.EOF
                                                .TextMatrix(i, 1) = IIf(IsNull(Rs.Fields(1).Value), "", Rs.Fields(1).Value)
                                                .TextMatrix(i, 2) = Format$(Rs.Fields(2), "#,##0.00")
                                                .TextMatrix(i, 3) = Format$(Rs.Fields(3), "#,##0.00")
                                                .TextMatrix(i, 4) = Format$(Rs.Fields(4), "#,##0.00")
                                                If Rs.Fields(5) = "1" Then
                                                            .TextMatrix(i, 5) = "���е����˹�"
                                                ElseIf Rs.Fields(5) = "2" Then
                                                            .TextMatrix(i, 5) = "�����Թ��˹�"
                                                ElseIf Rs.Fields(5) = "0" Then
                                                            .TextMatrix(i, 5) = "�ѧ��������"
                                                End If
                                                i = i + 1
                                                Rs.MoveNext
                                        Loop
                       
                            End With
        
        
            Call SetGrid

End Sub

Private Sub Form_Load()
        Call SetGridSumNum
        Call SetGridSumPrice
        Call SetGrid
        Call SetGrid_Ownership
        txtYear.Text = Right$(Date, 4)
        
        cbArticle.ListIndex = 0
        ComboTypeSch.ListIndex = 0
        Cmb_StoryAll.ListIndex = 0
        strOwnerShipID = ""
End Sub

Private Sub SetGrid_Ownership()
        With Grid_OwnerShip
                .TextArray(0) = "��¡�ê���": .ColWidth(0) = 3450
                .ColAlignmentFixed(0) = 4
               ' .TextArray(0) = "���� - ���ʡ��": .ColWidth(0) = 3400: .ColAlignment(0) = 0: .ColAlignmentFixed(0) = 0
                 .TextArray(1) = "OwnerShip_ID": .ColWidth(1) = 1500: .ColAlignment(1) = 0: .ColAlignmentFixed(1) = 0
        End With
        
End Sub

Private Sub SetGridSumNum()
    With Grid_SumNum
                        .Rows = .Rows + 2
                        .TextArray(0) = "����������": .ColWidth(0) = 1700: .ColAlignment(0) = 0: .ColAlignmentFixed(0) = 0
                        .TextArray(1) = "�ӹǹ": .ColWidth(1) = 1760: .ColAlignment(1) = 4: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "TypeTax": .ColWidth(3) = 2000: .ColAlignment(2) = 0: .ColAlignmentFixed(2) = 0
                        .TextMatrix(1, 0) = "��¡�÷��Թ"
                        .TextMatrix(2, 0) = "��¡���ç���͹"
                        .TextMatrix(3, 0) = "��¡�û���"
                        .TextMatrix(1, 2) = "PBT5"
                        .TextMatrix(2, 2) = "PRD2"
                        .TextMatrix(3, 2) = "PP1"
        End With
End Sub

Private Sub ComboTypeSch_Click()
        txtIDCard.Text = ""
        txtNm.Text = ""
        txtSurNm.Text = ""
        Select Case ComboTypeSch.ListIndex
                    Case 0
                                FrameSchNm.Visible = True
                    Case 1
                                FrameSchNm.Visible = False
                    Case 2
                                FrameSchNm.Visible = False
                    Case 3
                                FrameSchNm.Visible = False
        End Select
End Sub

Private Sub SetGridSumPrice()
    With Grid_SumPrice
                        .Rows = .Rows + 2
                        .TextArray(0) = "����������": .ColWidth(0) = 1700: .ColAlignment(0) = 0: .ColAlignmentFixed(0) = 0
                        .TextArray(1) = "�Ҥ�": .ColWidth(1) = 1760: .ColAlignment(1) = 4: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "TypeTax": .ColWidth(3) = 2000: .ColAlignment(2) = 0: .ColAlignmentFixed(2) = 4
                        .TextMatrix(1, 0) = "���պ��ا��ͧ���"
                        .TextMatrix(2, 0) = "�����ç���͹��з��Թ"
                        .TextMatrix(3, 0) = "���ջ���"
                        .TextMatrix(1, 2) = "PBT5"
                        .TextMatrix(2, 2) = "PRD2"
                        .TextMatrix(3, 2) = "PP1"
        End With
End Sub

Private Sub SetGrid()
        
        Select Case LB_Menu.Tag
                Case 1
                            With Grid_Result
                                    .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                                    .TextArray(1) = "���ʷ��Թ": .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4
                                    .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                                    .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                                    .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4
                                    .TextArray(5) = "�������": .ColWidth(5) = 1000: .ColAlignmentFixed(5) = 4
                                    .TextArray(6) = "�Ţ���": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                                    .TextArray(7) = "�ѹ����������": .ColWidth(7) = 1300: .ColAlignmentFixed(7) = 4
                                    .TextArray(8) = "���": .ColWidth(8) = 800: .ColAlignmentFixed(8) = 4
                                    .TextArray(9) = "�ҹ": .ColWidth(9) = 800: .ColAlignmentFixed(9) = 4
                                    .TextArray(10) = "��": .ColWidth(10) = 800: .ColAlignmentFixed(10) = 4
                                    .TextArray(11) = "�������": .ColWidth(11) = 1300: .ColAlignmentFixed(11) = 4
                            End With
                Case 2
                            With Grid_Result
                                    .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                                    .TextArray(1) = "�����ç���͹": .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4
                                    .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                                    .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                                    .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4
                                    .TextArray(5) = "�������": .ColWidth(5) = 1000: .ColAlignmentFixed(5) = 4
                                    .TextArray(6) = "�Ţ���": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                                    .TextArray(7) = "�ѹ����������": .ColWidth(7) = 1300: .ColAlignmentFixed(7) = 4
                                    .TextArray(8) = "���ʷ��Թ": .ColWidth(8) = 800: .ColAlignmentFixed(8) = 4
                                    .TextArray(9) = "�Ţ����ҹ": .ColWidth(9) = 800: .ColAlignmentFixed(9) = 4
                                    .TextArray(10) = "�ѡɳ��Ҥ��": .ColWidth(10) = 800: .ColAlignmentFixed(10) = 4
                                    .TextArray(11) = "���ŷ����": .ColWidth(11) = 1500: .ColAlignmentFixed(1) = 4
                                    .TextArray(12) = "��������»�": .ColWidth(12) = 1500: .ColAlignmentFixed(1) = 4
                                    .TextArray(13) = "�ӹǹ��ͧ": .ColWidth(13) = 1000: .ColAlignmentFixed(1) = 4
                                    .TextArray(14) = "�ӹǹ���": .ColWidth(14) = 1000: .ColAlignmentFixed(1) = 4
                                    .TextArray(15) = "���ҧ": .ColWidth(15) = 1000: .ColAlignmentFixed(1) = 4
                                    .TextArray(16) = "���": .ColWidth(16) = 1000: .ColAlignmentFixed(16) = 4
                                    .TextArray(17) = "�������": .ColWidth(17) = 1300: .ColAlignmentFixed(17) = 4
                            End With
                Case 3
                                With Grid_Result
                                    .TextArray(0) = "��": .ColWidth(0) = 800: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                                    .TextArray(1) = "���ʻ���": .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4
                                    .TextArray(2) = "�Ţ���": .ColWidth(2) = 1000: .ColAlignmentFixed(2) = 4
                                    .TextArray(3) = "�ѹ������Ẻ": .ColWidth(3) = 1300: .ColAlignmentFixed(3) = 4
                                    .TextArray(4) = "�Թ�����Թ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4
                                    .TextArray(5) = "�������": .ColWidth(5) = 1000: .ColAlignmentFixed(5) = 4
                                    .TextArray(6) = "�Ţ���": .ColWidth(6) = 1000: .ColAlignmentFixed(6) = 4
                                    .TextArray(7) = "�ѹ����������": .ColWidth(7) = 1300: .ColAlignmentFixed(7) = 4
                                    .TextArray(8) = "���ʷ��Թ": .ColWidth(8) = 800: .ColAlignmentFixed(8) = 4
                                    .TextArray(9) = "��鹷�����": .ColWidth(9) = 800: .ColAlignmentFixed(9) = 4
                                    .TextArray(10) = "������������ʹ�ҹ": .ColWidth(10) = 800: .ColAlignmentFixed(10) = 4
                                    .TextArray(11) = "�������": .ColWidth(11) = 1300: .ColAlignmentFixed(11) = 4
                            End With
        End Select
                
                 With Grid_Installment
                    .TextArray(0) = "�Ǵ���": .ColWidth(0) = 750: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 5
                    .TextArray(1) = "�ѹ������": .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 7
                    .TextArray(2) = "��һ�Ѻ�Թ�Ǵ": .ColWidth(2) = 1500: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                    .TextArray(3) = "�Թ����/Ŵ": .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4: .ColAlignment(3) = 7
                    .TextArray(4) = "�ӹǹ�Թ����": .ColWidth(4) = 1500: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 7
                    .TextArray(5) = "ʶҹС�ê���": .ColWidth(5) = 1500: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 7
            End With
End Sub

Private Sub SchListNm()
    Dim strYear As String
    Dim strCondition As String
    Dim strNm, strSurNm, strIDCard As String
    Dim i As Integer
    Dim TypeSch As Integer
    Dim personType As String
    
    strYear = txtYear.Text
    strNm = Trim(txtNm.Text)
    strSurNm = Trim(txtSurNm.Text)
    strIDCard = Trim(txtIDCard.Text)
    TypeSch = ComboTypeSch.ListIndex
    personType = ""
    If Trim(cmb_SignBoard_Owner.Text) <> "" Then
        personType = cmb_SignBoard_Owner.ListIndex
    End If
    
        Select Case cbArticle.ListIndex
                Case 0
                        strCondition = " 1 "
                Case 1
                        strCondition = " 0 "
                Case 2
                        strCondition = " 2 "
        End Select
    
      Debug.Print "exec sp_search_storyInstallment '" & strYear & "','" & strNm & "','" & strSurNm & "','" & strIDCard & "'," & TypeSch & ",'" & personType & "','" & strCondition & "'," & Cmb_StoryAll.ListIndex
      Set Rs = Globle_Connective.Execute("exec sp_search_storyInstallment " & strYear & ",'" & strNm & "','" & strSurNm & "','" & strIDCard & "'," & TypeSch & ",'" & personType & "','" & strCondition & "'," & Cmb_StoryAll.ListIndex, adCmdUnknown)

            If Rs.RecordCount > 0 Then
                    With Grid_OwnerShip
                       i = 1
                       .Rows = Rs.RecordCount + 1
                        Do While Not Rs.EOF
                                .TextMatrix(i, 0) = Rs("fullName")
                                .TextMatrix(i, 1) = Rs("OWNERSHIP_ID")
                        i = i + 1
                        Rs.MoveNext
                        Loop
                    Lb_ResultSchNm.Caption = Format(Rs.RecordCount, "#,##0")
                    End With
                    
                    Call chkNumInstallment

            Else
                Call clearSchGrid
                Call clrOldData
                SetGrid_Ownership
                SetGridSumNum
                SetGridSumPrice
                MsgBox "��辺�����ŷ�����", vbInformation, ":: Display Result Search ::"
            End If
End Sub

Private Sub clearSchGrid()
    On Error Resume Next
    Dim i As Byte
                    Grid_OwnerShip.Rows = 2
                    For i = 0 To Grid_OwnerShip.Cols - 1
                             Grid_OwnerShip.Col = i
                             Grid_OwnerShip.TextMatrix(1, i) = Empty
                    Next i
                    Lb_ResultSchNm.Caption = Empty
                    Lb_AllPrice.Caption = Empty
                    LbTaxNm.Caption = Empty
                    
                     Grid_SumNum.Rows = 2
                    For i = 0 To Grid_SumNum.Cols - 1
                            Grid_SumNum.TextMatrix(1, i) = Empty
                    Next i
                    
                    Grid_SumPrice.Rows = 2
                    For i = 0 To Grid_SumPrice.Cols - 1
                            Grid_SumPrice.TextMatrix(1, i) = Empty
                    Next i
End Sub

Private Sub GRID_OWNERSHIP_Click()
        Dim i As Integer
        
        strOwnerShipID = ""
        If Grid_OwnerShip.RowSel > 0 Then
            strOwnerShipID = Grid_OwnerShip.TextMatrix(Grid_OwnerShip.RowSel, 1)
        End If
        Call chkNumInstallment
        
        For i = 0 To 2
                    Grid_SumNum.Col = i
                    Grid_SumNum.CellBackColor = &H80000018
        Next i
        
        Call clrOldData

End Sub

Private Sub Grid_SumNum_Click()
    Dim strSelTaxType As String
        LB_Menu.Caption = Grid_SumNum.TextMatrix(Grid_SumNum.RowSel, 0)
        
        strSelTaxType = CStr(Grid_SumNum.TextMatrix(Grid_SumNum.RowSel, 2))
        
        Select Case strSelTaxType
                Case "PBT5"
                            getInstallmentID (1)
                            LB_Menu.Tag = 1
                            If ComboInstallmentID.Text <> "" Then
                                    Call displayDetailPayment
                                    Call countNumAsset(1)
                            Else
                                    Call clrOldData
                            End If
                Case "PRD2"
                            getInstallmentID (2)
                            LB_Menu.Tag = 2
                            If ComboInstallmentID.Text <> "" Then
                                    Call displayDetailPayment
                                    Call countNumAsset(2)
                            Else
                                    Call clrOldData
                            End If
                Case "PP1"
                            getInstallmentID (3)
                            LB_Menu.Tag = 3
                            If ComboInstallmentID.Text <> "" Then
                                    Call displayDetailPayment
                                    Call countNumAsset(3)
                            Else
                                    Call clrOldData
                            End If
        End Select
        
End Sub

Private Sub countNumAsset(FlagType As Integer)

        Set Rs = Globle_Connective.Execute("exec sp_count_numInstallmentAsset '" & strOwnerShipID & "'," & txtYear.Text & ", '" & ComboInstallmentID.Text & "'," & FlagType, adCmdUnknown)
        If Rs.RecordCount > 0 Then
                lbRecordCount.Caption = IIf(Rs("Count_ID") <> Null Or CInt(Rs("Count_ID")) > 0, Rs("Count_ID"), "0")
        End If

End Sub

Private Sub Image_Print_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
 Image_Print.Top = Image_Print.Top + 39
End Sub

Private Sub Image_Print_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
 Image_Print.Top = Image_Print.Top - 39
 
   Dim SQL_REPORT As String
   Dim strFileType As String
   Dim strType As String
   
    If ComboInstallmentID.Text <> "" Then
                Select Case LB_Menu.Tag
                        Case 1      ' PBT5
                                strFileType = "View_MainDataInstallment"
                                strType = "PBT5"
                        Case 2      'PRD2
                                strFileType = "View_PRD2_MainInstallment"
                                strType = "PRD2"
                        Case 3      'PP1
                                strFileType = "View_PP1_MainInstallment"
                                strType = "PP1"
                End Select
               
            SQL_REPORT = SQL_REPORT & " {" & strFileType & "." & strType & "_INSTALLMENT_ID} = '" & ComboInstallmentID.Text & "'"
            'SQL_REPORT = SQL_REPORT & " {View_PRD2_MainInstallment.PRD2_INSTALLMENT_ID} = '" & ComboInstallmentID.Text & "'"
               
               Select Case LB_Menu.Tag
                            Case 1
                                        Call ShowReport("Rpt_PBT5_INSTALLMENT.rpt", SQL_REPORT)
                            Case 2
                                        Call ShowReport("Rpt_PRD2_INSTALLMENT.rpt", SQL_REPORT)
                            Case 3
                                        Call ShowReport("Rpt_PP1_INSTALLMENT.rpt", SQL_REPORT)
                End Select
    Else
            MsgBox "��辺��������§ҹ����ͧ���", vbInformation, ":: ʶҹС�÷ӧҹ ::"
    End If
    
End Sub

Private Sub ShowReport(File_Name As String, Formula As String, Optional BEGIN_DATE As String, Optional ENDDATE As String)
On Error GoTo ErrHlde
Call FindPrinterDeFault
With Clone_Form.CTReport
        .Reset
        .Connect = "DSN = MicrotaxSQL;UID = " & ini_Username & ";PWD = " & ini_Password & ";DSQ = Administration"
        .DiscardSavedData = True
        .ReportTitle = "�������§ҹ"
        .ReportFileName = App.Path & "\Report\" & File_Name
               
        .SelectionFormula = Formula
        .PrinterDriver = TruePrinter.DriverName
        .PrinterPort = TruePrinter.Port
        .PrinterName = TruePrinter.DeviceName
        .WindowShowPrintSetupBtn = True
        .Destination = crptToWindow
        .WindowState = crptMaximized
        .WindowShowExportBtn = True
        .Action = 1
End With
                
Exit Sub
ErrHlde:
        MsgBox Err.Description & " " & Err.Number
End Sub

Private Sub Image1_Click()
        Call SchListNm
End Sub

Private Sub chkNumInstallment()
        Dim strYear As String
        
         LbTaxNm.Caption = Grid_OwnerShip.TextMatrix(Grid_OwnerShip.RowSel, 0)
         strYear = txtYear.Text
         If Cmb_StoryAll.ListIndex = 0 And Grid_OwnerShip.RowSel = 0 Then
                strOwnerShipID = ""
         End If
                    With Grid_SumNum
                                        '************************ PBT5 **********************************
                                        Set Rs = Globle_Connective.Execute("exec sp_count_numPayInstallment '" & strOwnerShipID & "'," & strYear & ", 1", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 Then
                                                .TextMatrix(1, 1) = Format(Rs.RecordCount, "#,##0")
                                        Else
                                                .TextMatrix(1, 1) = 0
                                        End If
                                        '************************ PRD2 **********************************
                                        
                                        Set Rs = Globle_Connective.Execute("exec sp_count_numPayInstallment '" & strOwnerShipID & "'," & strYear & ",2", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 Then
                                                .TextMatrix(2, 1) = Format(Rs.RecordCount, "#,##0")
                                        Else
                                                .TextMatrix(2, 1) = 0
                                        End If
                                        '************************ PP1 **********************************
                                        Set Rs = Globle_Connective.Execute("exec sp_count_numPayInstallment '" & strOwnerShipID & "'," & strYear & ", 3", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 Then
                                                .TextMatrix(3, 1) = Format(Rs.RecordCount, "#,##0")
                                        Else
                                                .TextMatrix(3, 1) = 0
                                        End If
                      End With
                      
                      
                      '>> Tatal All Price
                      With Grid_SumPrice
                                    '************************ PBT5 **********************************
'                                        Debug.Print "exec sp_count_PricePayInstallment '" & strOwnerShipID & "'," & strYear & ", " & Cmb_StoryAll.ListIndex
                                        Set Rs = Globle_Connective.Execute("exec sp_count_PricePayInstallment '" & strOwnerShipID & "'," & strYear & ", 1", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 And CCur(IIf(IsNull(Rs("AllPrice")), 0, Rs("AllPrice"))) > 0 Then
                                                .TextMatrix(1, 1) = Format(Rs("AllPrice"), "#,##0.00")
                                        Else
                                                .TextMatrix(1, 1) = "0.00"
                                        End If
                                        '************************ PRD2 **********************************
                                        Set Rs = Globle_Connective.Execute("exec sp_count_PricePayInstallment '" & strOwnerShipID & "'," & strYear & ",2", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 And CCur(IIf(IsNull(Rs("AllPrice")), 0, Rs("AllPrice"))) > 0 Then
                                                .TextMatrix(2, 1) = Format(Rs("AllPrice"), "#,##0.00")
                                        Else
                                                .TextMatrix(2, 1) = "0.00"
                                        End If
                                        '************************ PP1 **********************************
                                        Set Rs = Globle_Connective.Execute("exec sp_count_PricePayInstallment '" & strOwnerShipID & "'," & strYear & ", 3", adCmdUnknown)
                                        
                                        If Rs.RecordCount > 0 And CCur(IIf(IsNull(Rs("AllPrice")), 0, Rs("AllPrice"))) > 0 Then
                                                .TextMatrix(3, 1) = Format(Rs("AllPrice"), "#,##0.00")
                                        Else
                                                .TextMatrix(3, 1) = "0.00"
                                        End If
                                        
                                        Lb_AllPrice.Caption = Format$(CCur(.TextMatrix(1, 1)) + CCur(.TextMatrix(2, 1)) + CCur(.TextMatrix(3, 1)), "#,##0.00")
                      End With
End Sub

Private Sub getInstallmentID(FlagType As Integer)
    Dim i As Integer
                Set Rs = Globle_Connective.Execute("exec sp_get_HistoryInstallmentID " & txtYear.Text & ",'" & strOwnerShipID & "', " & FlagType, adCmdUnknown)
                ComboInstallmentID.Clear
                If Rs.RecordCount > 0 Then
                        i = 0
                        Do While Not Rs.EOF
                                    If i = 0 Then
                                            ComboInstallmentID.Text = Rs.Fields(0)
                                            i = 1
                                    End If
                                    ComboInstallmentID.AddItem Rs.Fields(0)
                                    
                                       Rs.MoveNext
                          Loop
                        
                End If
End Sub

Private Sub clrOldData()
Dim i As Byte

                ComboInstallmentID.Clear
                Grid_Result.Rows = 2
                For i = 0 To Grid_Result.Cols - 1
                         Grid_Result.Col = i
                         Grid_Result.CellBackColor = &H80000018     '&HE0E0E0
                         Grid_Result.TextMatrix(1, i) = Empty
                Next i
                
                Grid_Installment.Rows = 2
                For i = 0 To Grid_Installment.Cols - 1
                         Grid_Installment.Col = i
                         Grid_Installment.CellBackColor = &H80000018     '&HE0E0E0
                         Grid_Installment.TextMatrix(1, i) = Empty
                Next i
                 lbRecordCount.Caption = "0"
                 LbArrear.Caption = "0.00"
                 Lb_Discount.Caption = "0.00"
                 Lb_Tax.Caption = "0.00"
                 Lb_AllPricePay.Caption = "0.00"
                 LB_Menu.Caption = "��¡�÷��Թ"
                 LB_Menu.Tag = 1
                 
                 Call SetGrid
End Sub
