VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_Scantomize 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "�к���������"
   ClientHeight    =   5250
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8040
   Icon            =   "Frm_Scantomize.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Scantomize.frx":151A
   ScaleHeight     =   350
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   536
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox Cmb_Land_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3150
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   990
      Width           =   1980
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3150
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   600
      Width           =   1980
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Analyse 
      Height          =   3435
      Left            =   0
      TabIndex        =   4
      Top             =   1800
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   6059
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   12632256
      ForeColorSel    =   255
      BackColorBkg    =   14079702
      GridColor       =   12632256
      FocusRect       =   0
      GridLinesFixed  =   1
      SelectionMode   =   1
      AllowUserResizing=   1
      _NumberOfBands  =   1
      _Band(0).Cols   =   5
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.CommandButton Btn_Process 
      BackColor       =   &H00D6D6D6&
      Caption         =   "�������������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6510
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1380
      Width           =   1500
   End
   Begin VB.TextBox Txt_Percent 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3150
      TabIndex        =   1
      Text            =   "0"
      Top             =   1380
      Width           =   1980
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Index           =   3
      Left            =   2235
      TabIndex        =   6
      Top             =   1110
      Width           =   735
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҥ����Դ��Ҵ (%)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Index           =   0
      Left            =   1245
      TabIndex        =   3
      Top             =   1500
      Width           =   1740
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���͡��鹷��⫹/���ͤ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Index           =   2
      Left            =   1350
      TabIndex        =   0
      Top             =   720
      Width           =   1650
   End
End
Attribute VB_Name = "Frm_Scantomize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Set_Grid()
 With Grid_Analyse
                 .Rows = 2
                 .Clear
                 .FormatString = "^���ʷ��Թ|^��鹷����Ἱ��� (��.�.)|^��鹷��㹰ҹ������ (��.�.)|^��ǹ��ҧ��鹷�� (��.�.)|^����¤�ҼԴ��Ҵ (%)"
                .ColWidth(0) = 1000: .ColAlignment(0) = 1
                .ColWidth(1) = 1750: .ColAlignment(1) = 7
                .ColWidth(2) = 1850: .ColAlignment(2) = 7
'                .TextArray(2) = "���": .ColWidth(2) = 500
'                .TextArray(3) = "�ҹ": .ColWidth(3) = 500
'                .TextArray(4) = "��": .ColWidth(4) = 520
                .ColWidth(3) = 1530: .ColAlignment(3) = 7
                .ColWidth(4) = 1600: .ColAlignment(4) = 7
End With
End Sub

Private Sub Btn_Process_Click()
 Dim Chk_Value As Long
 Dim i As Integer, TRM As Single
 Dim Query As ADODB.Recordset
 Set Query = New ADODB.Recordset

Call Set_Grid
Frm_Scantomize.MousePointer = 11
 With Frm_Map
                   .Sis1.DoCommand "AComDeselectAll"
                   .Sis2.DoCommand "AComDeselectAll"
                    If LenB(Trim$(Cmb_Land_ID.Text)) > 0 Then
                        .Sis1.CreatePropertyFilter "filter", "LAND_ID$ = """ & Cmb_Land_ID.Text & """"       '���ҧ Property ��ä��� �繤���� CC ������㹡���Ң�����
                   Else
                        .Sis1.CreatePropertyFilter "filter", "LAND_ID$  LIKE """ & IIf(LenB(Trim$(Cmb_Zoneblock.Text)) > 0, Cmb_Zoneblock.Text, ".*") & """"
                   End If
'                         .Sis1.ScanOverlay "TextSteam", iniLand, "filter", ""      ' Scan �� LAND_ID �ҡ����ŧ���Թ (iniLand)
                        Chk_Value = .Sis1.ScanOverlay("TextSteam", iniLand, "filter", "")     ' �Ѻ��ҡ�� Scan ����ը������ҡѺ 0 Chk_Value ��ҡѺ�ӹǹ��������
                        'Sis1.OpenList "TextSteam", 0
                        .Sis1.SelectList "TextSteam"
                        .Sis1.OpenSel 0
            If Chk_Value > 0 Then
                For i = 1 To Chk_Value
                      .Sis1.OpenSel i - 1
                      If Trim$(.Sis1.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")) <> "0" Then
                            With Grid_Analyse
                                    'GaGrid.TextMatrix(GaGrid.Rows - 1, 1) = GaGrid.Rows
                                    .TextMatrix(.Rows - 1, 0) = Frm_Map.Sis1.GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")
                                    'GaGrid.TextMatrix(i + 1, 1) = Sis1.GetFlt(SIS_OT_CURITEM, 0, "_area#")
                                    .TextMatrix(.Rows - 1, 1) = FormatNumber(Frm_Map.Sis1.GetFlt(SIS_OT_CURITEM, 0, "_area#"), 2)  '��鹷���Ἱ���
'                                    Call SET_QUERY("Select LAND_A_RAI , LAND_A_POT , LAND_A_VA From LANDDATA Where ZONE_BLOCK = '" & Cmb_Zoneblock.Text & "' AND LAND_ID =  '" & .TextMatrix(.Rows - 1, 0) & "'", Query)
'                                    Call SET_QUERY("Select LAND_A_RAI , LAND_A_POT , LAND_A_VA From LANDDATA Where LAND_ID =  '" & .TextMatrix(.Rows - 1, 0) & "'", Query)
                                    Set Query = Globle_Connective.Execute("exec sp_scantomize '" & .TextMatrix(.Rows - 1, 0) & "'", , adCmdUnknown)
                                    If Query.RecordCount > 0 Then
'                                        .TextMatrix(.Rows - 1, 2) = Query.Fields("LAND_A_RAI").Value
'                                        .TextMatrix(.Rows - 1, 3) = Query.Fields("LAND_A_POT").Value
'                                        .TextMatrix(.Rows - 1, 4) = Query.Fields("LAND_A_VA").Value
                                        TRM = (CSng(Query.Fields("LAND_A_RAI").Value) * 1600) + (CSng(Query.Fields("LAND_A_POT").Value) * 400) + (CSng(Query.Fields("LAND_A_VA").Value) * 4)
                                        .TextMatrix(.Rows - 1, 2) = FormatNumber(TRM, 2)  '��鹷��㹰ҹ������
                                        .TextMatrix(.Rows - 1, 3) = Abs(Round(CSng(.TextMatrix(.Rows - 1, 1)) - CSng(TRM), 2))   '��ǹ��ҧ�����ҧ��鹷��
                                        .TextMatrix(.Rows - 1, 4) = Format$(Round(Abs((.TextMatrix(.Rows - 1, 3) / .TextMatrix(.Rows - 1, 1)) * 100), 2), "#0.##") & " %"
                                         If CSng(Txt_Percent.Text) > CSng(Trim$(Replace(.TextMatrix(.Rows - 1, 4), "%", ""))) Then
                                                  .Rows = .Rows - 1
                                         End If
                                            '(Abs(GaGrid.TextMatrix(GaGrid.Rows - 1, 6)) / Abs(CSng(GaGrid.TextMatrix(GaGrid.Rows - 1, 6))))) * 100
                                    End If
                                    .Rows = .Rows + 1
                         End With
                        End If
                Next i
                Grid_Analyse.FixedRows = 1
                Frm_Map.Sis1.EmptyList "TextSteam"
                Grid_Analyse.Rows = Grid_Analyse.Rows - 1
                Frm_Map.Sis1.DoCommand "AComZoomSelect"
'                Else
'                    MsgBox " ��辺�������ŧ���Թ " & Grid_Analyse.TextMatrix(Grid_Analyse.Row, 0) & " �Ἱ��� ! ", , "����͹ !"
                End If
                Frm_Scantomize.MousePointer = 0
                Set Query = Nothing
End With
End Sub

Private Sub Cmb_Zoneblock_Click()
    GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
    Cmb_Land_ID.Clear
    Cmb_Land_ID.AddItem ""
If GBQueryLandData.RecordCount > 0 Then
    GBQueryLandData.MoveFirst
    Do While Not GBQueryLandData.EOF
                If LenB(Trim$(GBQueryLandData.Fields("ZONE_BLOCK").Value)) > 0 Then
                  Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                End If
                GBQueryLandData.MoveNext
    Loop
End If
End Sub

Private Sub Form_Load()
        Me.Show
        StayTop = SetWindowPos(Me.hWnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
        
          Call Set_Grid
          GBQueryZoneTax.Requery
           GBQueryLandUse.Requery
           
                 GBQueryZone.Filter = "ZONE_ID <> '' "
        If GBQueryZone.RecordCount > 0 Then
                    GBQueryZone.MoveFirst
            Do While Not GBQueryZone.EOF
                          If LenB(Trim$(GBQueryZone.Fields("ZONE_BLOCK").Value)) > 0 Then
                              Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                          End If
                            GBQueryZone.MoveNext
            Loop
        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
        Set Frm_Scantomize = Nothing
End Sub

Private Sub Grid_Analyse_DblClick()
        Dim filterLandCode  As String
        With Frm_Map
                    .Sis1.DeselectAll
                    If .Sis1.GetInt(SIS_OT_OVERLAY, iniLand, "_status&") <> SIS_HITTABLE Then
                            .Sis1.SetInt SIS_OT_OVERLAY, iniLand, "_status&", SIS_HITTABLE
                    End If
                            filterLandCode = "(LAND_ID$ = " & """" & Grid_Analyse.TextMatrix(Grid_Analyse.Row, 0) & """)"
                            .Sis1.EmptyList "selectitem"
                            .Sis1.CreatePropertyFilter "filter", filterLandCode
                            .Sis1.ScanOverlay "selectitem", iniLand, "filter", ""
                    If .Sis1.GetListSize("selectitem") > 0 Then
                            .Sis1.SelectList "selectitem"
                            .Sis1.OpenSel 0
                            .Sis1.DoCommand "AComZoomSelect"
                            .Sis1.EmptyList "TextSteam"
                    End If
        End With
End Sub

Private Sub Txt_Percent_Change()
        If LenB(Trim$(Txt_Percent.Text)) = 0 Then Txt_Percent.Text = "0"
        Call KILL_DOT(Txt_Percent)
End Sub

Private Sub Txt_Percent_GotFocus()
        Txt_Percent.SelStart = 0
        Txt_Percent.SelLength = Len(Txt_Percent.Text)
End Sub

Private Sub Txt_Percent_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub
