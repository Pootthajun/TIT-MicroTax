Attribute VB_Name = "DMod_Search"
Option Explicit

Public Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Public Declare Function GetMenuItemID Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Public Declare Function SetMenuItemBitmaps Lib "user32" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long, ByVal hBitmapUnchecked As Long, ByVal hBitmapChecked As Long) As Long
Public Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Public Declare Function GetMenuItemInfo Lib "user32" Alias "GetMenuItemInfoA" (ByVal hMenu As Long, ByVal un As Long, ByVal b As Boolean, lpMenuItemInfo As MENUITEMINFO) As Boolean

Public Const MF_BITMAP = &H4&

Public Type MENUITEMINFO
    cbSize As Long
    fMask As Long
    fType As Long
    fState As Long
    wID As Long
    hSubMenu As Long
    hbmpChecked As Long
    hbmpUnchecked As Long
    dwItemData As Long
    dwTypeData As String
    cch As Long
End Type


Public Const MIIM_ID = &H2
Public Const MIIM_TYPE = &H10
Public Const MFT_STRING = &H0&

Public fType As Byte
Public fPrint As Byte
Public OrderListView As Byte
Public IndexNode As Long ' For Frm_Map
Public pw As New EDOU.EDOUF

Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public TruePrinter   As Printer

Public Declare Function CallWindowProc Lib "user32.dll" Alias "CallWindowProcA" ( _
ByVal lpPrevWndFunc As Long, _
ByVal hWnd As Long, _
ByVal Msg As Long, _
ByVal wParam As Long, _
ByVal lParam As Long) As Long

Public Declare Function SetWindowLong Lib "user32.dll" Alias "SetWindowLongA" ( _
ByVal hWnd As Long, _
ByVal nIndex As Long, _
ByVal dwNewLong As Long) As Long

Public Const MK_CONTROL = &H8
Public Const MK_LBUTTON = &H1
Public Const MK_RBUTTON = &H2
Public Const MK_MBUTTON = &H10
Public Const MK_SHIFT = &H4
Public Const CB_LIMITTEXT = &H141
Public Const GWL_WNDPROC = -4
Private Const WM_MOUSEWHEEL = &H20A
Private m_bLMousePressed As Boolean 'true if the left button is pressed
Private m_bLMouseClicked As Boolean 'true just after the click (i.e. just after the left button is released)
'Windows messages constants
Private Const WM_LBUTTONUP = &H202
Private Const WM_LBUTTONDOWN = &H201
Private Const WM_ERASEBKGND = &H14
Public gsLockedColumns As String
Dim LocalHwnd As Long
Dim LocalPrevWndProc As Long
Dim MyForm As Form
Dim MyGrid As MSHFlexGrid

Public Sub FindPrinterDeFault()
Dim tmplen&, PrinterDeviceName$
If LenB(Trim$(PrinterDeviceName$)) = 0 Then
        PrinterDeviceName$ = Printer.DeviceName ' Look Default PrinterName in you printer
End If
         tmplen& = Len(PrinterDeviceName$)
                For Each TruePrinter In Printers
                    If Right(TruePrinter.DeviceName, tmplen&) = PrinterDeviceName$ Then
                        Set Printer = TruePrinter   ' �� Defualt Printer for Set Defualt
                        Exit For
                    End If
                Next
End Sub

Public Function FindText(tval As String, tcompare As String) As Integer
        Dim n As Integer, i As Integer
        If InStr(1, tval, tcompare, vbTextCompare) <> 0 Then
                i = 0
                n = InStr(1, tval, tcompare, vbTextCompare)
                Do While InStr(n, tval, tcompare, vbTextCompare) <> 0
                        tval = Mid$(tval, InStr(n, tval, tcompare, vbTextCompare) + 1)
                        n = 1
                        i = i + 1
                Loop
                FindText = i
        End If
End Function

Public Function StartDoc(DocName As String) As Long
          Dim Scr_hDC As Long
          Scr_hDC = GetDesktopWindow()
          StartDoc = ShellExecute(Scr_hDC, "Open", DocName, "", "C:\", SW_SHOWNORMAL)
End Function

Public Sub GoMap(Keyid As String, iType As Integer, iOverlay As Integer)
        Dim strCode As String
        strCode = Trim$(Keyid)
        If LenB(strCode) > 0 Then
                Select Case iType
                        Case 0
                                strCode = "(LAND_ID$ = " & """" & strCode & """)"
                        Case 1
                                strCode = "(BUILDING_ID$ = " & """" & strCode & """)"
                        Case 2
                                strCode = "(SIGNBORD_ID$ = " & """" & strCode & """)"
                End Select
                With Frm_Map.Sis1
                    .DeselectAll
                    .CreatePropertyFilter "filter", strCode
                    .ScanOverlay "selectitem", iOverlay, "filter", ""
                    .SelectList "selectitem"
                    .EmptyList "selectitem"
                    .DoCommand "AComZoomSelect"
                End With
        End If
End Sub

Public Sub checkOverlayStatus(iOverlay As Integer)
        Dim i As Integer
        With Frm_Map.Sis1
            i = .GetInt(SIS_OT_OVERLAY, iOverlay, "_status&")
            If i = 0 Then
                .SetInt SIS_OT_OVERLAY, iOverlay, "_status&", SIS_EDITABLE
            End If
        End With
End Sub

Public Function IDNOCHECK(ByVal N_id As String) As Boolean
    Dim D1 As Integer, D2 As Integer, D3 As Integer, D4 As Integer
    Dim D5 As Integer, D6 As Integer, D7 As Integer, D8 As Integer
    Dim D9 As Integer, D10 As Integer, D11 As Integer, D12 As Integer, D13 As Integer
    Dim vSumID As Integer, vReminder As Integer
    IDNOCHECK = False  '//-- ����� false ��͹
    If Len(N_id) <> 13 Then '//-- ��Ҥ����������� 13 �ʴ�������١��ͧ
       Exit Function
    End If
    If Not IsNumeric(N_id) Then '//-- �������繵���Ţ �����١��ͧ
       Exit Function
    End If
    If N_id = "1212121212121" Then  '//-- ������ҧ�ͧ�óվ����
        Exit Function
    End If
    D1 = Val(Mid(N_id, 1, 1)) * 13
    D2 = Val(Mid(N_id, 2, 1)) * 12
    D3 = Val(Mid(N_id, 3, 1)) * 11
    D4 = Val(Mid(N_id, 4, 1)) * 10
    D5 = Val(Mid(N_id, 5, 1)) * 9
    D6 = Val(Mid(N_id, 6, 1)) * 8
    D7 = Val(Mid(N_id, 7, 1)) * 7
    D8 = Val(Mid(N_id, 8, 1)) * 6
    D9 = Val(Mid(N_id, 9, 1)) * 5
    D10 = Val(Mid(N_id, 10, 1)) * 4
    D11 = Val(Mid(N_id, 11, 1)) * 3
    D12 = Val(Mid(N_id, 12, 1)) * 2
    D13 = Mid(N_id, 13, 1)
    
    vSumID = D1 + D2 + D3 + D4 + D5 + D6 + D7 + D8 + D9 + D10 + D11 + D12
    vReminder = vSumID Mod 11
    If (11 - vReminder) Mod 10 = D13 Then
            IDNOCHECK = True
    End If
End Function

Public Function CvDate1(Pdate As DTPicker) As String
        CvDate1 = Year(Pdate.Value) & "-" & Format$(Month(Pdate.Value), "00") & "-" & Format$(Day(Pdate.Value), "00")
End Function

Public Function CvDate2(Pdate As Variant, Optional pFlag As Byte) As String
        CvDate2 = Year(Pdate) & "-" & Format$(Month(Pdate), "00") & "-" & Format$(Day(Pdate), "00")
        If pFlag = 1 Then CvDate2 = Year(Pdate) & "-" & Format$(Month(Pdate), "00") & "-" & Format$(Day(Pdate), "00") & " " & Format$(Time, "hh:mm:ss")
End Function

Public Function CvDate3(Pdate As String) As String
        If IsDate(Pdate) = True Then
                CvDate3 = Year(Pdate) & "-" & Format$(Month(Pdate), "00") & "-" & Format$(Day(Pdate), "00")
        Else
                CvDate3 = ""
        End If
End Function

Public Function ConvertDate(ByVal DateV As String, ByVal TypeV As Byte) As String
        Dim strDay As String
        Dim strMonth As String
        Dim strYear As String
        Dim n As Byte
        Dim m As Byte
        If LenB(Trim$(DateV)) = 0 Then Exit Function
        DateV = Format(DateV, "dd/mm/yyyy")
        n = InStr(1, DateV, "/", vbTextCompare)
        
        If InStr(1, DateV, "/", vbTextCompare) = 3 Then
                strDay = Left$(DateV, 2)
        Else
                strDay = "0" + Left$(DateV, 1)
        End If
        m = InStr(n + 1, DateV, "/", vbTextCompare)
        If (m - n) = 3 Then
                strMonth = Mid$(DateV, n + 1, 2)
        Else
                strMonth = "0" + Mid$(DateV, n + 1, 1)
        End If
        
        strYear = Right$(DateV, 4)
        If TypeV = 1 Then
                strYear = CInt(strYear) + 543
                If CInt(strDay) >= 12 Then
                    ConvertDate = strDay + "/" + strMonth + "/" + strYear
                Else
                    ConvertDate = strMonth + "/" + strDay + "/" + strYear
                End If
                Exit Function
        End If
        strYear = CInt(strYear) - 543
        ConvertDate = strMonth + "/" + strDay + "/" + strYear
End Function

Public Function ConvertDate2(ByVal DateV As String) As String
        Dim strDay As String
        Dim strMonth As String
        Dim strYear As String
        Dim n As Byte
        Dim m As Byte
        If LenB(Trim$(DateV)) = 0 Then Exit Function
        DateV = Format(DateV, "dd/mm/yyyy")
        n = InStr(1, DateV, "/", vbTextCompare)
        
        If InStr(1, DateV, "/", vbTextCompare) = 3 Then
                strDay = Left$(DateV, 2)
        Else
                strDay = "0" + Left$(DateV, 1)
        End If
        m = InStr(n + 1, DateV, "/", vbTextCompare)
        If (m - n) = 3 Then
                strMonth = Mid$(DateV, n + 1, 2)
        Else
                strMonth = "0" + Mid$(DateV, n + 1, 1)
        End If
        
        strYear = CInt(Right$(DateV, 4)) + 543
        ConvertDate2 = strDay + "/" + strMonth + "/" + strYear
End Function

Public Function ConvertDate3(ByVal DateV As String) As String ' For English Year
        Dim str_Day As String, str_Month As String, str_Year As String
        str_Year = Left(DateV, 4) + 543
        str_Month = Mid$(DateV, 6, 2)
        str_Day = Mid$(DateV, 9, 2)
        ConvertDate3 = str_Day & "/" & str_Month & "/" & str_Year
End Function

Public Function GetRegis() As String
        Dim hregkey As Long
        Dim retval As Long
        Dim datatype As Long  '�Ѻ data type �ҡ�����ҹ���
        Dim tmpVal As String
        Dim keyValSize As Long
         
        tmpVal = Space(255)
        keyValSize = 255
        retval = RegOpenKeyEx(HKEY_CLASSES_ROOT, subkey, 0, KEY_READ, hregkey)
        retval = RegQueryValueEx(hregkey, "Effect ID", 0, datatype, ByVal tmpVal, keyValSize)
        tmpVal = GetKeyValue(tmpVal, keyValSize)
        tmpVal = pw.HexToDecStr(tmpVal) 'DecToHexStr
        tmpVal = GetValue_Type(datatype, tmpVal)
        retval = RegCloseKey(hregkey)
        GetRegis = tmpVal
End Function

Public Function GetKeyValue(ByVal KeyValue As String, ByVal KeyValueSize As Long) As String
        If Asc(Mid(KeyValue, KeyValueSize, 1)) = 0 Then
                GetKeyValue = Left(KeyValue, KeyValueSize - 1)
        Else
                GetKeyValue = Left(KeyValue, KeyValueSize)
        End If
End Function

Public Function Encode(ByVal strVal As String) As String
        Dim strTmp As String
        Dim i As Byte

        For i = 1 To Len(strVal)
                strTmp = strTmp + Chr$(Asc(Mid$(strVal, i, 1)) Xor i)
        Next i
        Encode = strTmp
End Function

Public Function Decode(ByVal strVal As String) As String
        Dim strTmp As String
        Dim i As Byte
        
        For i = 1 To Len(strVal)
                strTmp = strTmp + pw.DecToHexStr(Asc(Mid$(strVal, i, 1)))
        Next i
        Decode = strTmp
End Function

Public Function ValidHex(ByVal HexVal As String) As Integer
        Select Case HexVal
                Case "A"
                        ValidHex = 10
                Case "B"
                        ValidHex = 11
                Case "C"
                        ValidHex = 12
                Case "D"
                        ValidHex = 13
                Case "E"
                        ValidHex = 14
                Case "F"
                        ValidHex = 15
                Case Else
                        ValidHex = CInt(HexVal)
        End Select
End Function

Public Function GetValue_Type(ByVal TypeV As Long, ByVal KeyV As String) As String
        Dim KeyVal As String
        Dim i As Integer
        
        Select Case TypeV
                Case REG_SZ
                        GetValue_Type = KeyV
                Case REG_DWORD
                        For i = Len(KeyV) To 1 Step -1
                                KeyVal = KeyVal + Hex(Asc(Mid(KeyV, i, 1)))
                        Next i
                        KeyVal = Format$("&h" + KeyVal)
                        GetValue_Type = KeyVal
                Case Else
'                        Debug.Print "Data not in string format.  Unable to interpret data."
'                        End
        End Select
End Function

Public Sub GetRegEnumValue(KeyValue As String)
    Dim valuename As String     ' name of the value being retrieved
    Dim valuelen As Long        ' length of valuename
    Dim datatype As Long        ' receives data type of value
    Dim data(0 To 254) As Byte  ' 255-byte data buffer for read information
    Dim datalen As Long         ' size of data buffer information
    Dim datastring As String    ' will receive data converted to a string, if necessary
    Dim hKey As Long            ' handle to the registry key to enumerate the values of
    Dim Index As Long           ' counter for the index of the value to enumerate
    Dim c As Long               ' counter variable
    Dim retval As Long          ' functions' return value
    
    retval = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\Cadcorp\Licence\" & KeyValue, 0, KEY_QUERY_VALUE, hKey)
    ' Check to see if an error occured.
    If retval = 0 Then
    ' Begin enumerating the values.  Get each one, displaying its name.  If it's a null-
    ' terminated string or binary data, display it.  If not, say so.
    Index = 0  ' initialize the counter
    While retval = 0  ' loop while successful
        ' Initialize the value name buffer.
        valuename = Space(255)  ' 255-space buffer
        valuelen = 255  ' length of the string
        datalen = 255  ' size of data buffer
        ' Get the next value to be enumerated
        retval = RegEnumValue(hKey, Index, valuename, valuelen, 0, datatype, data(0), datalen)
        If retval = 0 Then  ' if successful, display information
            ' Extract the useful information from the value name buffer and display it.
            valuename = Left(valuename, valuelen)
'            Debug.Print "Value Name: "; valuename
            ' Determine the data type of the value and display it.
            Select Case datatype
            Case REG_SZ  ' null-terminated string
                ' Copy the information from the byte array into the string.
                ' We subtract one because we don't want the trailing null.
                datastring = Space(datalen - 1)  ' make just enough room in the string
                CopyMemory ByVal datastring, data(0), datalen - 1  ' copy useful data
'                Debug.Print "  Data (string): "; datastring
            Case REG_BINARY  ' binary data
                ' Display the hexadecimal values of each byte of data, separated by
                ' sapces.  Use the datastring buffer to allow us to assure each byte
                ' is represented by a two-character string.
'                Debug.Print "  Data (binary):";
                For c = 0 To datalen - 1  ' loop through returned information
                    datastring = Hex(data(c))  ' convert value into hex
                    ' If needed, add leading zero(s).
                    If Len(datastring) < 2 Then datastring = _
                        String(2 - Len(datastring), "0") & datastring
'                    Debug.Print " "; datastring;
                Next c
'                Debug.Print  ' end the line
            Case Else  ' a data type this example doesn't handle
'                Debug.Print "This example doesn't know how to read that kind of data."
            End Select
        End If
        Index = Index + 1  ' increment the index counter
    Wend  ' end the loop
    
    ' Close the registry key.
    End If
    retval = RegCloseKey(hKey)
End Sub

Public Function QueryUtility_Char(Combo_B As ComboBox, Combo_E As ComboBox, Exp_Statement As String) As String
            If (LenB(Trim$(Combo_B.Text)) <> 0) Or (LenB(Trim$(Combo_E.Text)) <> 0) Then
                If (LenB(Trim$(Combo_B.Text)) <> 0) And (LenB(Trim$(Combo_E.Text)) <> 0) Then
                            If Combo_B.ListIndex < Combo_E.ListIndex Then
                                    If Combo_B = "�" Or Combo_E = "�" Then
                                            QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_B & "'"
                                    Else
                                            QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_B & "'" & " AND " & Exp_Statement & " < '" & Combo_E.List(Combo_E.ListIndex + 1) & "'"
                                    End If
                            Else
                                    If Combo_B = "�" Or Combo_E = "�" Then
                                            QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_E & "'"
                                    Else
                                            QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_E & "'" & " AND " & Exp_Statement & " < '" & Combo_B.List(Combo_B.ListIndex + 1) & "'"
                                    End If
                            End If
                End If
        End If
        If (LenB(Trim$(Combo_B.Text)) <> 0) And (LenB(Trim$(Combo_E.Text)) = 0) Then
                If Combo_B = "�" Then
                        QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_B & "'"
                Else
                        QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_B & "'" & " AND " & Exp_Statement & " < '" & Combo_B.List(Combo_B.ListIndex + 1) & "'"
                End If
        End If
        If (LenB(Trim$(Combo_B.Text)) = 0) And (LenB(Trim$(Combo_E.Text)) <> 0) Then
                If Combo_E = "�" Then
                        QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_E & "'"
                Else
                        QueryUtility_Char = QueryUtility_Char & " AND " & Exp_Statement & " > '" & Combo_E & "'" & " AND " & Exp_Statement & " < '" & Combo_E.List(Combo_E.ListIndex + 1) & "'"
                End If
        End If
End Function

Public Function Chk_ID_IN_Tax(Key_ID As String, Flag_Var As String) As Boolean ' True = ID can use; False = ID can't use
        Dim rs As ADODB.Recordset
        Dim int_Year As Integer
        Set rs = New ADODB.Recordset
        
        Set rs = Globle_Connective.Execute("exec sp_max_year '" & Flag_Var & "'", , adCmdUnknown)     '�һ�����ش�  GK1
        If IsNull(rs.Fields(0).Value) <> True Then
                int_Year = rs.Fields(0).Value
                Set rs = Globle_Connective.Execute("exec sp_check_id_in_tax '" & Key_ID & "'," & int_Year & ",'" & Flag_Var & "'", , adCmdUnknown)
                If rs.RecordCount > 0 Then '���� GK ����ش
                        Chk_ID_IN_Tax = False
                Else
                        Chk_ID_IN_Tax = True
                End If
        Else
                Chk_ID_IN_Tax = True
        End If
        Set rs = Nothing
End Function

Public Function Chk_ID_IN_Present(Key_ID As String, Flag_Var As String) As String
        Dim rs As ADODB.Recordset
        Set rs = New ADODB.Recordset
        Set rs = Globle_Connective.Execute("exec sp_check_id_in_present '" & Key_ID & "','" & Flag_Var & "'", , adCmdUnknown)
        If IsNull(rs.Fields(0).Value) <> True Then
                If rs.RecordCount > 0 Then
                        Chk_ID_IN_Present = rs.Fields(0).Value
                Else
                        Chk_ID_IN_Present = ""
                End If
        End If
        Set rs = Nothing
End Function

Private Function WindowProc(ByVal Lwnd As Long, ByVal Lmsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim MouseKeys As Long
    Dim Rotation As Long
    Dim Xpos As Long
    Dim Ypos As Long

    If Lmsg = WM_MOUSEWHEEL Then
        MouseKeys = wParam And 65535
        Rotation = wParam / 65536
        Xpos = lParam And 65535
        Ypos = lParam / 65536
'        MouseWheel MouseKeys, Rotation, Xpos, Ypos, MyGrid
        FlexGridScroll MyGrid, MouseKeys, Rotation, Xpos, Ypos
    End If
    
    WindowProc = CallWindowProc(LocalPrevWndProc, Lwnd, Lmsg, wParam, lParam)
    
End Function

Public Sub WheelHook(PassedForm As Form, PassedFlexGrid As MSHFlexGrid)

    On Error Resume Next

    Set MyForm = PassedForm
    LocalHwnd = PassedForm.hWnd
    
    Set MyGrid = PassedFlexGrid
    LocalPrevWndProc = SetWindowLong(LocalHwnd, GWL_WNDPROC, AddressOf WindowProc)
End Sub

Public Sub WheelUnHook()
    Dim WorkFlag As Long

    On Error Resume Next
    WorkFlag = SetWindowLong(LocalHwnd, GWL_WNDPROC, LocalPrevWndProc)
    Set MyForm = Nothing
End Sub

Public Sub FlexGridScroll(ByRef FG As MSHFlexGrid, ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
Dim NewValue As Long
Dim Lstep As Single
On Error Resume Next
        With FG
                Lstep = .Height / .RowHeight(0)
                Lstep = Int(Lstep)
                If .Rows < Lstep Then Exit Sub
                Do While Not (.RowIsVisible(.TopRow + Lstep))
                        Lstep = Lstep - 1
                Loop
                If Rotation > 0 Then
                        NewValue = .TopRow - Lstep
                        If NewValue < 1 Then
                                NewValue = 1
                        End If
                Else
                        NewValue = .TopRow + Lstep
                        If NewValue > .Rows - 1 Then
                                NewValue = .Rows - 1
                        End If
                End If
                .TopRow = NewValue
        End With
End Sub

Public Function CheckStandardDemo() As Boolean
        If UCase(TProduct) = "STANDARD" And TLocalDemo = "���ͺ" Then
                CheckStandardDemo = True
        Else
                CheckStandardDemo = False
        End If
End Function

Public Sub ErrorTrap()
        If Err.Number = 0 Then Exit Sub
        Select Case Err.Number
                Case 32755
                        Exit Sub
                Case 57
                        MsgBox "Device I/O error. Big Problems! Your hardware is acting up. Check the disc drive.", vbExclamation + vbOKOnly, "Error"
                Case 61
                        MsgBox "The disk is full. Try deleting some files before saving.", vbExclamation + vbOKOnly, "Error"
                Case 70
                        MsgBox "Access denied. Make sure the write protection is turned off at the source you are saving to.", vbExclamation + vbOKOnly, "Error"
                Case 71
                        MsgBox "Device not ready. I think the drive door is open - please check.", vbExclamation + vbOKOnly, "Error"
                Case 72
                        MsgBox "Disk media error. Time to inspect the media!", vbExclamation + vbOKOnly, "Error"
                Case Else
                        MsgBox "Error " & Err.Number & ": " & Err.Description, vbExclamation + vbOKOnly, "Error"
        End Select
End Sub

Function CheckCurrencyVol1(Index As Integer, tmpStr As String) As Integer
Dim i As Integer
    Select Case Index
        Case 48 To 57
            ' 0 - 9 and Return index = KeyAscii
        Case 8
            ' Back Space and Return index = KeyAscii
        Case 13
            ' Enter and Return index = KeyAscii
        Case 46 ' KeyAscii Code �ͧ����ͧ���¨ش��Ѻ����ͧ
            For i = 1 To Len(tmpStr) ' �Ѻ���������Ǣͧ��ҷ���͹����
                If Mid$(tmpStr, i, 1) = "." Then
                    Index = 0 ' ������ͧ���¨ش���� ���¡��ԡ���� Ascii ��衴 ��������칤�ҡ�Ѻ�� 0
                    Exit For    ' ����͡�ҡ�ٻ������
                End If
            Next i
        Case Else
            Index = 0
    End Select
    CheckCurrencyVol1 = Index ' Return ��ҡ�Ѻ���������Ǩ�ͺ
End Function

Function CheckCurrencyVol2(Index As Integer, Ctrl As TextBox) As Integer
'Ex. KeyAscii = CheckCurrencyVol1(KeyAscii, Text1)
    Select Case Index
        Case 48 To 57, 8, 13
            ' 0 - 9 and Return index = KeyAscii
'        Case 8, 13
            ' Back Space and Return index = KeyAscii
'        Case 13
            ' Enter and Return index = KeyAscii
        Case 46 ' ���� Ascii Code  �ͧ����ͧ���¨ش��Ѻ����ͧ
            If InStr(Ctrl, ".") Then Index = 0 ' ��ѧ���� InStr (In String) ����������ͧ���¨ش� TextBox
        Case Else
            Index = 0
    End Select
    CheckCurrencyVol2 = Index ' Return ��ҡ�Ѻ���������Ǩ�ͺ
End Function

Public Function ConvertArea(Des_Area As Single) As String
        Dim TempArea As Single
        Dim Rai As Integer
        Dim Pot As Byte
        Dim Va As String
        
        Rai = 0
        Pot = 0
        Va = "0"
        TempArea = CSng(Des_Area)
        If TempArea >= 1600 Then
                Do While TempArea >= 1600
                        Rai = Rai + 1
                        TempArea = TempArea - 1600
                Loop
        End If
        If TempArea < 1600 And TempArea >= 400 Then
                Do While TempArea >= 400
                        Pot = Pot + 1
                        TempArea = TempArea - 400
                Loop
        End If
        Va = Format$(TempArea / 4, "0.00")
        ConvertArea = Rai & " - " & Pot & " - " & Va
End Function

Public Sub SetSelFocus(CtrlV As Variant)
        With CtrlV
            .SelStart = 0
            .SelLength = Len(Trim$(.Text))
            .SetFocus
        End With
End Sub

Public Sub SetComboMaxLength(ComboBox As ComboBox, ByVal lMaxLength As Long) ' Set the maximum number of characters that can be entered in a ComboBox control
    SendMessage ComboBox.hWnd, CB_LIMITTEXT, lMaxLength, ByVal 0&
End Sub

Public Function ChkStandard() As Boolean
'            Dim TLicence As String
            
'            TProduct = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Product")
'            TLocalName = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Local Government")
'            TLicence = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Licence")
            
            If TProduct = "STANDARD" And TLocalDemo = "���ͺ" Then
                    ChkStandard = False     ' Can't use report
                    MsgBox "��ҹ���ѧ�� Version MicroTax Standard Demo " & vbCrLf & "�������ö�͡��§ҹ��" & vbCrLf & vbCrLf & "��سҵԴ��� ����ѷ �͹��෤ �ӡѴ" & vbCrLf & "Tel : 0-2933-1116", vbExclamation, "�Ӫ��ᨧ"
            Else
                    ChkStandard = True
            End If
End Function

Public Sub Hook()
   lpPrevWndProc = SetWindowLong(gHW, GWL_WNDPROC, AddressOf WindowProc)
End Sub

Public Sub UnHook()
   Dim lngReturnValue As Long
   If lpPrevWndProc <> 0 Then
      lngReturnValue = SetWindowLong(gHW, GWL_WNDPROC, lpPrevWndProc)
      lpPrevWndProc = 0
   End If
End Sub

'Function WindowProc(ByVal hw As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
'
'   Dim i As Integer
'   Dim Mouse_X As Long
'   Dim Mouse_Y As Long
'   Dim Col_X As Long
'   Dim bAbsorbMsg As Boolean
'
'   Select Case uMsg
'      Case WM_SIZE
''         Frm_Map.Caption = "Column Width Changed " & Now
'
'      Case WM_MOUSEMOVE
'         With Frm_Map.Grid_PROPERTY
'            If .MouseRow = 0 Then
'               Mouse_X = LoWord(lParam)
'               For i = 0 To .Cols - 1
'                  'Rocky - For a better way...
'                  '     You only want to lock the right edge of a column to stop
'                  '     it from resizing. When the left edge is moved, the previous
'                  '     column will resize and the current column will just move
'                  '     with it. It will not resize the current column.
'                  'Rocky - Find the right edge of the column.
'                  Col_X = (.ColPos(i) + .ColWidth(i)) \ Screen.TwipsPerPixelX
'                  If Mouse_X + 5 >= Col_X And Mouse_X - 5 <= Col_X Then
'                     'Search for i in the gsLockedColumns string
'                     If InStr(1, gsLockedColumns, "|" & i & "|") > 0 Then
'                        'If found, don't pass the message
'                        bAbsorbMsg = True
'                        Exit For
'                     End If
'                  End If
'               Next
'            End If
'         End With
'   End Select
'
'   If Not bAbsorbMsg Then
''      Debug.Print uMsg
'      WindowProc = CallWindowProc(lpPrevWndProc, hw, uMsg, wParam, lParam)
'   End If
'
'End Function

Public Function LoWord(dw As Long) As Integer

    If dw And &H8000& Then
      LoWord = &H8000& Or (dw And &H7FFF&)
    Else
      LoWord = dw And &HFFFF&
    End If
    
End Function

Public Function GridMessage(ByVal hWnd As Long, ByVal Msg As Long, ByVal wp As Long, ByVal lp As Long) As Long

If m_bLMousePressed And Msg = WM_LBUTTONUP Then
'button have been just released
m_bLMousePressed = False
m_bLMouseClicked = True
End If

If Not (m_bLMousePressed) And Msg = WM_LBUTTONDOWN Then
'button have been just pressed
m_bLMousePressed = True
m_bLMouseClicked = False
End If

If m_bLMouseClicked And (Msg = WM_ERASEBKGND) Then
'Only when resize happens this event may occur after releasing the button !
'When user is making a simple click on grid,
'the WM_ERASEBKGND event occurs before WM_LBUTTONUP,
'and therefore will not be handled there

'Debug.Print "Grid message: ", "Resized !" 'TO DO: Replace this futile code
If Frm_Map.Txt_Buffer.Visible = True Then
        Frm_Map.Txt_Buffer.Width = Frm_Map.Grid_PROPERTY.CellWidth - 40
        Frm_Map.Txt_Buffer.Left = Frm_Map.Grid_PROPERTY.Left + Frm_Map.Grid_PROPERTY.CellLeft
End If
'Form1.Text1.Width = Form1.MSFlexGrid.CellWidth - ScaleX(1, vbPixels, vbTwips)
', .Top + .CellTop, .CellWidth - ScaleX(1, vbPixels, vbTwips), .CellHeight - ScaleY(1, vbPixels, vbTwips)
'with something usefull

m_bLMouseClicked = False

End If

'call the default message handler
GridMessage = CallWindowProc(g_lngDefaultHandler, hWnd, Msg, wp, lp)

End Function

Public Sub Regenerate_Cadcorp()
        With Frm_Map.Sis1
                .RefreshDbTable "BUILDINGDATA"
                .RefreshDbTable "LANDDATA"
                .RefreshDbTable "SIGNBORDDATA"
                .RefreshDataset 1
                .Redraw 0
        End With
End Sub
