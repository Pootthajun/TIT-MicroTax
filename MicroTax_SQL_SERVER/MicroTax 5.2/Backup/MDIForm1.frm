VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.MDIForm FrmMain 
   AutoShowChildren=   0   'False
   BackColor       =   &H8000000C&
   Caption         =   " MicroTax  Software"
   ClientHeight    =   9750
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   15000
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   4800
      Top             =   2460
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   15
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":151A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":183E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":1B62
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":1E86
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":21AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":24CE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin Crystal.CrystalReport CTReport 
      Left            =   2940
      Top             =   4020
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3150
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3690
      Top             =   1470
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   25
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":27F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":2D8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":3326
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":38C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":3E5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":43F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":498E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":4F28
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":54C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":5A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":5FF6
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":6590
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":6B2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":70C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":765E
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":7BF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":8192
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":872C
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":8CC6
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":9260
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":97FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":9D94
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":A32E
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":A8C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":AE62
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Align           =   3  'Align Left
      Height          =   8565
      Left            =   0
      Picture         =   "MDIForm1.frx":B3FC
      ScaleHeight     =   8505
      ScaleWidth      =   2400
      TabIndex        =   20
      Top             =   870
      Width           =   2460
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   2
         Left            =   30
         Picture         =   "MDIForm1.frx":10A72
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   42
         Top             =   570
         Width           =   2370
         Begin VB.Label Lb_Reg 
            BackStyle       =   0  'Transparent
            Caption         =   "����¹�͹حҵ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   5
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":14F57
            MousePointer    =   99  'Custom
            TabIndex        =   48
            Top             =   2340
            Width           =   1485
         End
         Begin VB.Label Lb_Reg 
            BackStyle       =   0  'Transparent
            Caption         =   "����¹����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   4
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":15261
            MousePointer    =   99  'Custom
            TabIndex        =   47
            Top             =   1890
            Width           =   1485
         End
         Begin VB.Label Lb_Reg 
            BackStyle       =   0  'Transparent
            Caption         =   "����¹�ç���͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   3
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":1556B
            MousePointer    =   99  'Custom
            TabIndex        =   46
            Top             =   1470
            Width           =   1485
         End
         Begin VB.Label Lb_Reg 
            BackStyle       =   0  'Transparent
            Caption         =   "����¹���Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":15875
            MousePointer    =   99  'Custom
            TabIndex        =   45
            Top             =   1050
            Width           =   1485
         End
         Begin VB.Label Lb_Reg 
            BackStyle       =   0  'Transparent
            Caption         =   "����ա����Է���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":15B7F
            MousePointer    =   99  'Custom
            TabIndex        =   44
            Top             =   660
            Width           =   1485
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   2
            Left            =   390
            Top             =   120
            Visible         =   0   'False
            Width           =   1665
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   2
            Left            =   300
            MouseIcon       =   "MDIForm1.frx":15E89
            MousePointer    =   99  'Custom
            TabIndex        =   43
            Top             =   150
            Width           =   1935
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   600
            Top             =   570
            Width           =   1725
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   3
         Left            =   30
         Picture         =   "MDIForm1.frx":16193
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   21
         Top             =   1140
         Width           =   2370
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.17 �ѭ�դ�����Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   9
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1D7C5
            MousePointer    =   99  'Custom
            TabIndex        =   31
            Top             =   4020
            Width           =   1695
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.13 �ѭ�ջ�Ѻ������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   8
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1DACF
            MousePointer    =   99  'Custom
            TabIndex        =   30
            Top             =   3600
            Width           =   1725
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.12 ����¹�ŧ����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   7
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1DDD9
            MousePointer    =   99  'Custom
            TabIndex        =   29
            Top             =   3180
            Width           =   1680
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.11 ����¹�͹حҵ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   6
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1E0E3
            MousePointer    =   99  'Custom
            TabIndex        =   28
            Top             =   2760
            Width           =   1725
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.9 ����¹�ŧ�Ҥ��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   5
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1E3ED
            MousePointer    =   99  'Custom
            TabIndex        =   27
            Top             =   2340
            Width           =   1740
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "����ѵ��͹حҵ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   4
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1E6F7
            MousePointer    =   99  'Custom
            TabIndex        =   26
            Top             =   1920
            Width           =   1725
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "����ѵԷ���¹����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   3
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1EA01
            MousePointer    =   99  'Custom
            TabIndex        =   25
            Top             =   1500
            Width           =   1695
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "����ѵԷ���¹�ç���͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   330
            Index           =   2
            Left            =   630
            MouseIcon       =   "MDIForm1.frx":1ED0B
            MousePointer    =   99  'Custom
            TabIndex        =   24
            Top             =   1080
            Width           =   1740
         End
         Begin VB.Label Lb_ChgReg 
            BackStyle       =   0  'Transparent
            Caption         =   "����ѵԷ���¹���Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   330
            Index           =   1
            Left            =   660
            MouseIcon       =   "MDIForm1.frx":1F015
            MousePointer    =   99  'Custom
            TabIndex        =   23
            Top             =   690
            Width           =   1635
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   3
            Left            =   270
            Top             =   120
            Visible         =   0   'False
            Width           =   2025
         End
         Begin VB.Label Lb_menuLeft 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   180
            Index           =   3
            Left            =   240
            MouseIcon       =   "MDIForm1.frx":1F31F
            MousePointer    =   99  'Custom
            TabIndex        =   22
            Top             =   180
            Width           =   1950
         End
         Begin VB.Shape Shape4 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   600
            Top             =   570
            Width           =   1725
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   4
         Left            =   30
         Picture         =   "MDIForm1.frx":1F629
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   49
         Top             =   1710
         Width           =   2370
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.9"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   8
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":256C7
            MousePointer    =   99  'Custom
            TabIndex        =   59
            Top             =   3600
            Width           =   1275
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.12"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   7
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":259D1
            MousePointer    =   99  'Custom
            TabIndex        =   58
            Top             =   3180
            Width           =   1410
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.3"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   6
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":25CDB
            MousePointer    =   99  'Custom
            TabIndex        =   57
            Top             =   2760
            Width           =   1140
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.8"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   5
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":25FE5
            MousePointer    =   99  'Custom
            TabIndex        =   56
            Top             =   2340
            Width           =   1275
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.9"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   4
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":262EF
            MousePointer    =   99  'Custom
            TabIndex        =   55
            Top             =   1920
            Width           =   1305
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.1"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   3
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":265F9
            MousePointer    =   99  'Custom
            TabIndex        =   54
            Top             =   1500
            Width           =   1140
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.2"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   2
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":26903
            MousePointer    =   99  'Custom
            TabIndex        =   53
            Top             =   1080
            Width           =   1275
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.4"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   9
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":26C0D
            MousePointer    =   99  'Custom
            TabIndex        =   52
            Top             =   4050
            Width           =   1140
         End
         Begin VB.Label Lb_Assess 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ẻ�ʴ� �.�.�.5"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   1
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":26F17
            MousePointer    =   99  'Custom
            TabIndex        =   51
            Top             =   630
            Width           =   1305
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   4
            Left            =   300
            Top             =   120
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   4
            Left            =   300
            MouseIcon       =   "MDIForm1.frx":27221
            MousePointer    =   99  'Custom
            TabIndex        =   50
            Top             =   120
            Width           =   1845
         End
         Begin VB.Shape Shape6 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   630
            Top             =   570
            Width           =   1695
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   9
         Left            =   0
         Picture         =   "MDIForm1.frx":2752B
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   86
         Top             =   4560
         Width           =   2370
         Begin VB.Label Lb_Control 
            BackStyle       =   0  'Transparent
            Caption         =   "��駤���к��������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   3
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":2CD66
            MousePointer    =   99  'Custom
            TabIndex        =   90
            Top             =   1500
            Width           =   1725
         End
         Begin VB.Label Lb_Control 
            BackStyle       =   0  'Transparent
            Caption         =   "�к����ͧ������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":2D070
            MousePointer    =   99  'Custom
            TabIndex        =   89
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Label Lb_Control 
            BackStyle       =   0  'Transparent
            Caption         =   "�к�������ʹ���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":2D37A
            MousePointer    =   99  'Custom
            TabIndex        =   88
            Top             =   660
            Width           =   1725
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   9
            Left            =   360
            Top             =   120
            Visible         =   0   'False
            Width           =   1665
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   9
            Left            =   270
            MouseIcon       =   "MDIForm1.frx":2D684
            MousePointer    =   99  'Custom
            TabIndex        =   87
            Top             =   150
            Width           =   1875
         End
         Begin VB.Shape Shape9 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   660
            Top             =   570
            Width           =   1665
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   1
         Left            =   30
         Picture         =   "MDIForm1.frx":2D98E
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   32
         Top             =   0
         Width           =   2370
         Begin VB.Label Lb_menuLeft 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Index           =   1
            Left            =   120
            MouseIcon       =   "MDIForm1.frx":32E98
            MousePointer    =   99  'Custom
            TabIndex        =   41
            Top             =   120
            Width           =   2160
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "���͹���Ф�һ�Ѻ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   8
            Left            =   855
            MouseIcon       =   "MDIForm1.frx":331A2
            MousePointer    =   99  'Custom
            TabIndex        =   40
            Top             =   3600
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "�������Ԩ���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   7
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":334AC
            MousePointer    =   99  'Custom
            TabIndex        =   39
            Top             =   3180
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "���ջ���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   6
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":337B6
            MousePointer    =   99  'Custom
            TabIndex        =   38
            Top             =   2760
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "�����ç���͹��з��Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   5
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":33AC0
            MousePointer    =   99  'Custom
            TabIndex        =   37
            Top             =   2340
            Width           =   1485
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "���պ��ا��ͧ���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   4
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":33DCA
            MousePointer    =   99  'Custom
            TabIndex        =   36
            Top             =   1920
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "ࢵ/ࢵ����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   3
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":340D4
            MousePointer    =   99  'Custom
            TabIndex        =   35
            Top             =   1500
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "���/���"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":343DE
            MousePointer    =   99  'Custom
            TabIndex        =   34
            Top             =   1080
            Width           =   1395
         End
         Begin VB.Label Lb_Index 
            BackStyle       =   0  'Transparent
            Caption         =   "�Ӻ�/�����ҹ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   810
            MouseIcon       =   "MDIForm1.frx":346E8
            MousePointer    =   99  'Custom
            TabIndex        =   33
            Top             =   660
            Width           =   1395
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   1
            Left            =   540
            Top             =   120
            Visible         =   0   'False
            Width           =   1365
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   405
            Left            =   630
            Top             =   570
            Width           =   1695
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   6
         Left            =   0
         Picture         =   "MDIForm1.frx":349F2
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   66
         Top             =   2850
         Width           =   2370
         Begin VB.Label Lb_Find 
            BackStyle       =   0  'Transparent
            Caption         =   "���͹حҵ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   5
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":40C3B
            MousePointer    =   99  'Custom
            TabIndex        =   72
            Top             =   2370
            Width           =   1395
         End
         Begin VB.Label Lb_Find 
            BackStyle       =   0  'Transparent
            Caption         =   "�鹷���¹����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   4
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":40F45
            MousePointer    =   99  'Custom
            TabIndex        =   71
            Top             =   1950
            Width           =   1395
         End
         Begin VB.Label Lb_Find 
            BackStyle       =   0  'Transparent
            Caption         =   "�鹷���¹�ç���͹"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   3
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":4124F
            MousePointer    =   99  'Custom
            TabIndex        =   70
            Top             =   1530
            Width           =   1395
         End
         Begin VB.Label Lb_Find 
            BackStyle       =   0  'Transparent
            Caption         =   "�鹷���¹���Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":41559
            MousePointer    =   99  'Custom
            TabIndex        =   69
            Top             =   1110
            Width           =   1395
         End
         Begin VB.Label Lb_Find 
            BackStyle       =   0  'Transparent
            Caption         =   "�鹷���¹��Ѿ���Թ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   840
            MouseIcon       =   "MDIForm1.frx":41863
            MousePointer    =   99  'Custom
            TabIndex        =   68
            Top             =   660
            Width           =   1395
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   6
            Left            =   390
            Top             =   120
            Visible         =   0   'False
            Width           =   1665
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   6
            Left            =   330
            MouseIcon       =   "MDIForm1.frx":41B6D
            MousePointer    =   99  'Custom
            TabIndex        =   67
            Top             =   150
            Width           =   1860
         End
         Begin VB.Shape Shape5 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   660
            Top             =   570
            Width           =   1665
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   7
         Left            =   0
         Picture         =   "MDIForm1.frx":41E77
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   73
         Top             =   3420
         Width           =   2370
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "��Ѿ���Թ������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   720
            MouseIcon       =   "MDIForm1.frx":48E08
            MousePointer    =   99  'Custom
            TabIndex        =   75
            Top             =   660
            Width           =   1455
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "���դ�ҧ㹻է�����ҳ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   7
            Left            =   660
            MouseIcon       =   "MDIForm1.frx":49112
            MousePointer    =   99  'Custom
            TabIndex        =   81
            Top             =   3180
            Width           =   1635
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "������㹻է�����ҳ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   6
            Left            =   720
            MouseIcon       =   "MDIForm1.frx":4941C
            MousePointer    =   99  'Custom
            TabIndex        =   80
            Top             =   2760
            Width           =   1575
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "���������Ш��ѹ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   5
            Left            =   720
            MouseIcon       =   "MDIForm1.frx":49726
            MousePointer    =   99  'Custom
            TabIndex        =   79
            Top             =   2340
            Width           =   1395
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.1 <������ѡ��>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   4
            Left            =   750
            MouseIcon       =   "MDIForm1.frx":49A30
            MousePointer    =   99  'Custom
            TabIndex        =   78
            Top             =   1920
            Width           =   1365
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "�.�.1 <��ºؤ��>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   3
            Left            =   750
            MouseIcon       =   "MDIForm1.frx":49D3A
            MousePointer    =   99  'Custom
            TabIndex        =   77
            Top             =   1500
            Width           =   1365
         End
         Begin VB.Label Lb_Report 
            BackStyle       =   0  'Transparent
            Caption         =   "��Ѿ���Թ����¹�ŧ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   720
            MouseIcon       =   "MDIForm1.frx":4A044
            MousePointer    =   99  'Custom
            TabIndex        =   76
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   7
            Left            =   390
            Top             =   120
            Visible         =   0   'False
            Width           =   1665
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   7
            Left            =   360
            MouseIcon       =   "MDIForm1.frx":4A34E
            MousePointer    =   99  'Custom
            TabIndex        =   74
            Top             =   150
            Width           =   1830
         End
         Begin VB.Shape Shape8 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   630
            Top             =   570
            Width           =   1695
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   8
         Left            =   0
         Picture         =   "MDIForm1.frx":4A658
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   82
         Top             =   3990
         Width           =   2370
         Begin VB.Label Lb_Analyse 
            BackStyle       =   0  'Transparent
            Caption         =   "��ë�Ӣͧ���ʢ�����"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   780
            MouseIcon       =   "MDIForm1.frx":5109E
            MousePointer    =   99  'Custom
            TabIndex        =   85
            Top             =   1080
            Width           =   1485
         End
         Begin VB.Label Lb_Analyse 
            BackStyle       =   0  'Transparent
            Caption         =   "�����Դ��Ҵ��鹷��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   780
            MouseIcon       =   "MDIForm1.frx":513A8
            MousePointer    =   99  'Custom
            TabIndex        =   84
            Top             =   660
            Width           =   1725
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   8
            Left            =   390
            Top             =   120
            Visible         =   0   'False
            Width           =   1665
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   8
            Left            =   270
            MouseIcon       =   "MDIForm1.frx":516B2
            MousePointer    =   99  'Custom
            TabIndex        =   83
            Top             =   150
            Width           =   1950
         End
         Begin VB.Shape Shape10 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   570
            Top             =   570
            Width           =   1755
         End
      End
      Begin VB.PictureBox Pic_LfBar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   555
         Index           =   5
         Left            =   30
         Picture         =   "MDIForm1.frx":519BC
         ScaleHeight     =   555
         ScaleWidth      =   2370
         TabIndex        =   60
         Top             =   2280
         Width           =   2370
         Begin VB.Label Lb_Invoice 
            BackStyle       =   0  'Transparent
            Caption         =   "����稡Ԩ��ä��"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   4
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":571B7
            MousePointer    =   99  'Custom
            TabIndex        =   65
            Top             =   1890
            Width           =   1545
         End
         Begin VB.Label Lb_Invoice 
            BackStyle       =   0  'Transparent
            Caption         =   "����� �.�.7"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   3
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":574C1
            MousePointer    =   99  'Custom
            TabIndex        =   64
            Top             =   1530
            Width           =   1215
         End
         Begin VB.Label Lb_Invoice 
            BackStyle       =   0  'Transparent
            Caption         =   "����� �.�.�.12"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":577CB
            MousePointer    =   99  'Custom
            TabIndex        =   63
            Top             =   1110
            Width           =   1545
         End
         Begin VB.Label Lb_Invoice 
            BackStyle       =   0  'Transparent
            Caption         =   "����� �.�.�.11"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   990
            MouseIcon       =   "MDIForm1.frx":57AD5
            MousePointer    =   99  'Custom
            TabIndex        =   62
            Top             =   690
            Width           =   1545
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            Height          =   345
            Index           =   5
            Left            =   570
            Top             =   120
            Visible         =   0   'False
            Width           =   1185
         End
         Begin VB.Label Lb_menuLeft 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   180
            Index           =   5
            Left            =   330
            MouseIcon       =   "MDIForm1.frx":57DDF
            MousePointer    =   99  'Custom
            TabIndex        =   61
            Top             =   180
            Width           =   1860
         End
         Begin VB.Shape Shape7 
            BackColor       =   &H00C2723B&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00DDEEF0&
            Height          =   375
            Left            =   630
            Top             =   570
            Width           =   1695
         End
      End
   End
   Begin VB.PictureBox Picture2 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   870
      Left            =   0
      Picture         =   "MDIForm1.frx":580E9
      ScaleHeight     =   870
      ScaleWidth      =   15000
      TabIndex        =   0
      Top             =   0
      Width           =   15000
      Begin VB.PictureBox Picture21 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   12060
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   17
         Top             =   0
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.PictureBox Picture20 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   11310
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   16
         Top             =   0
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.PictureBox Picture19 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   9060
         MouseIcon       =   "MDIForm1.frx":58A28
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   15
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture18 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   8295
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   14
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture17 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   9810
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   13
         Top             =   0
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.PictureBox Picture16 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   10560
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.PictureBox Picture15 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   7545
         MouseIcon       =   "MDIForm1.frx":58D32
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   11
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture14 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   6795
         MouseIcon       =   "MDIForm1.frx":5903C
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   10
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   2250
         MouseIcon       =   "MDIForm1.frx":59346
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   9
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture12 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   6030
         MouseIcon       =   "MDIForm1.frx":59650
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   8
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture111 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   5265
         MouseIcon       =   "MDIForm1.frx":5995A
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   7
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture100 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   3735
         MouseIcon       =   "MDIForm1.frx":59C64
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   6
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Pic_Map 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   1530
         MouseIcon       =   "MDIForm1.frx":59F6E
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   5
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture10 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   3000
         MouseIcon       =   "MDIForm1.frx":5A278
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   4
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Pic_Home 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   810
         MouseIcon       =   "MDIForm1.frx":5A582
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   3
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Pic_Lock 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   60
         MouseIcon       =   "MDIForm1.frx":5A88C
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   2
         Top             =   0
         Width           =   765
      End
      Begin VB.PictureBox Picture11 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   4500
         MouseIcon       =   "MDIForm1.frx":5AB96
         MousePointer    =   99  'Custom
         ScaleHeight     =   705
         ScaleWidth      =   765
         TabIndex        =   1
         Top             =   0
         Width           =   765
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "DEMO VERSION "
         BeginProperty Font 
            Name            =   "Lucida Sans Unicode"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   405
         Left            =   12720
         MouseIcon       =   "MDIForm1.frx":5AEA0
         MousePointer    =   99  'Custom
         TabIndex        =   19
         Top             =   540
         Visible         =   0   'False
         Width           =   3075
      End
      Begin VB.Label Lb_Menu 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "menu"
         ForeColor       =   &H00C2723B&
         Height          =   195
         Left            =   105
         TabIndex        =   18
         Top             =   690
         Visible         =   0   'False
         Width           =   405
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   91
      Top             =   9435
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   8
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   2999
            MinWidth        =   2999
            Text            =   "Status : "
            TextSave        =   "Status : "
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   4939
            MinWidth        =   4939
            Picture         =   "MDIForm1.frx":5B1AA
            Text            =   "User Login : "
            TextSave        =   "User Login : "
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   4146
            MinWidth        =   3881
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   1676
            MinWidth        =   1411
            Object.ToolTipText     =   "Total number of selected items"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   1676
            MinWidth        =   1411
            Object.ToolTipText     =   "Number of editable selected items"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2558
            MinWidth        =   2558
            Object.Tag             =   "Current Layer"
            Object.ToolTipText     =   "Item class of selected item"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   2911
            MinWidth        =   2646
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4851
            MinWidth        =   4586
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu keyIndex 
      Caption         =   "�����ž�鹰ҹ"
      Begin VB.Menu keyLocation 
         Caption         =   "&�Ӻ�/�����ҹ"
         Checked         =   -1  'True
      End
      Begin VB.Menu keyRoad 
         Caption         =   "&���/���"
      End
      Begin VB.Menu keyZone 
         Caption         =   "&ࢵ/ࢵ���� (Zone/Block)"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu keyRateLand 
         Caption         =   "&�ѵ������ (���ا��ͧ���)"
      End
      Begin VB.Menu keyRateBulid 
         Caption         =   "&�ѵ������ (�ç���͹��з��Թ)"
      End
      Begin VB.Menu keyRateSignBord 
         Caption         =   "&�ѵ������ (����)"
      End
      Begin VB.Menu keyRateWorkLicense 
         Caption         =   "&����������ѡɳСԨ���"
      End
      Begin VB.Menu keyRateTax 
         Caption         =   "&���͹���Ф�һ�Ѻ"
      End
      Begin VB.Menu keyChangeAsset 
         Caption         =   "&�������¹�ŧ��Ѿ���Թ"
      End
      Begin VB.Menu keyOther 
         Caption         =   "&����������"
      End
   End
   Begin VB.Menu keyAccess 
      Caption         =   "&����¹��Ѿ���Թ"
      Begin VB.Menu keyOwnerShip 
         Caption         =   "&����ա����Է���"
      End
      Begin VB.Menu keyLandData 
         Caption         =   "&����¹���Թ"
      End
      Begin VB.Menu keyBulidingData 
         Caption         =   "&����¹�ç���͹"
      End
      Begin VB.Menu keySignbordData 
         Caption         =   "&����¹����"
      End
      Begin VB.Menu keyWorkLicense 
         Caption         =   "&����¹�͹حҵ"
      End
   End
   Begin VB.Menu keyEditAccess 
      Caption         =   "&����ѵԷ���¹��Ѿ���Թ"
      Begin VB.Menu keyEditLandData 
         Caption         =   "&����ѵԷ���¹���Թ"
      End
      Begin VB.Menu keyEditBulidingData 
         Caption         =   "&����ѵԷ���¹�ç���͹"
      End
      Begin VB.Menu keyEditSignbordData 
         Caption         =   "&����ѵԷ���¹����"
      End
      Begin VB.Menu keyEditWorkLicense 
         Caption         =   "&����ѵԷ���¹�͹حҵ"
      End
   End
   Begin VB.Menu keyContact 
      Caption         =   "&���Ẻ�����Թ"
      Begin VB.Menu keyCalTaxYear_New 
         Caption         =   "�Ѵ����¡������"
      End
      Begin VB.Menu keyCalTaxYear 
         Caption         =   "&�Ѵ�Ӻѭ�� �.�.1"
         Begin VB.Menu keyCalLand 
            Caption         =   "&���պ��ا��ͧ���"
         End
         Begin VB.Menu keyCalBuild 
            Caption         =   "&�����ç���͹��з��Թ"
         End
         Begin VB.Menu keyCalSignBord 
            Caption         =   "&���ջ���"
         End
         Begin VB.Menu keyCalLicense 
            Caption         =   "&�����͹حҵ��Сͺ��ä��"
         End
      End
      Begin VB.Menu keyInputType 
         Caption         =   "&������Ẻ"
         Begin VB.Menu keyRPTCalLand5 
            Caption         =   "&�.�.�.5  Ẻ�ʴ����պ��ا��ͧ���"
         End
         Begin VB.Menu keyRPTCalLand2 
            Caption         =   "&�.�.�.2   Ẻ����¡���������������ç���͹��з��Թ"
         End
         Begin VB.Menu keyRPTCalLand1 
            Caption         =   "&�.�.1   Ẻ�ʴ���¡�����ջ���"
         End
         Begin VB.Menu keyRPTCalLand4 
            Caption         =   "&Ẻ�ʴ���¡�������͹حҵ��Сͺ��ä��"
         End
      End
      Begin VB.Menu keyActimate 
         Caption         =   "&��û����Թ"
         Begin VB.Menu keyRPTCalLand9 
            Caption         =   "&�.�.�.9  Ẻ�����Թ���պ��ا��ͧ���"
         End
         Begin VB.Menu keyRPTCalLand8 
            Caption         =   "&�.�.�.8  ��駡�û����Թ�����õ�� 24"
         End
         Begin VB.Menu keyRPTCalLand3 
            Caption         =   "&�.�.3  ˹ѧ����駡�û����Թ����"
         End
         Begin VB.Menu keyRPTCalLand6 
            Caption         =   "&Ẻ�駻����Թ�͹حҵ��Сͺ��ä��"
         End
      End
      Begin VB.Menu keyRequest 
         Caption         =   "&����Ѻ����ͧ�ط�ó�"
         Begin VB.Menu keyRPTCalLandPBT12 
            Caption         =   "&�.�.�.12  Ẻ���طó��õ��Ҥһҹ��ҧ"
         End
         Begin VB.Menu keyRPTCalLandPRD9 
            Caption         =   "&�.�.�.9  �Ӣ��������Թ�������ҵ�� 25"
         End
         Begin VB.Menu keyRPTCalLandPT4 
            Caption         =   "&�.�.4  Ẻ�طó����ջ���"
         End
      End
      Begin VB.Menu keyBookType 
         Caption         =   "&Ẻ˹ѧ���"
         Begin VB.Menu keyRPTWarningNotic 
            Caption         =   "˹ѧ�������������Ẻ��¡��"
         End
         Begin VB.Menu keyRPTWarningNotic2 
            Caption         =   "˹ѧ���������Ҫ�������"
         End
         Begin VB.Menu keyRPTWarningNewNotic 
            Caption         =   "˹ѧ���������Ҫ������� (�������)"
         End
         Begin VB.Menu keyRPTWarningOtherNotic 
            Caption         =   "˹ѧ���������Ҫ������� (�������)"
         End
         Begin VB.Menu keyRPTWarningPBT5 
            Caption         =   "˹ѧ�����͹�������պ��ا��ͧ���"
         End
         Begin VB.Menu keyRPTWarningPRD2 
            Caption         =   "˹ѧ�����͹���������ç���͹"
         End
         Begin VB.Menu keyRPTWarningPP1 
            Caption         =   "˹ѧ�����͹�������ջ���"
         End
      End
      Begin VB.Menu keyPetition 
         Caption         =   "&����¹�������ͧ�����"
      End
   End
   Begin VB.Menu keyPayment 
      Caption         =   "&����Ѻ��������"
      Begin VB.Menu keyPaymentRBT11 
         Caption         =   "&�.�.�.11  ���պ��ا��ͧ��� (������Ѻ�Թ) "
      End
      Begin VB.Menu keyPaymentPRD12 
         Caption         =   "&�.�.�.12 �����ç���͹��з��Թ (������Ѻ�Թ)"
      End
      Begin VB.Menu keyPaymentPT7 
         Caption         =   "&�.�.7 ���ջ��� (������Ѻ�Թ)"
      End
      Begin VB.Menu keyPaymentPBA2 
         Caption         =   "&�����͹حҵ��Сͺ��ä�� (������Ѻ�Թ)"
      End
   End
   Begin VB.Menu KeyTaxOther 
      Caption         =   "&���������"
      Begin VB.Menu KeyTaxCalOther 
         Caption         =   "�Ѵ�Ӻѭ�����������"
         Begin VB.Menu keyLandOther 
            Caption         =   "���պ��ا��ͧ���"
         End
         Begin VB.Menu KeyBuildingOther 
            Caption         =   "�����ç���͹��з��Թ"
         End
         Begin VB.Menu keySignboardOther 
            Caption         =   "���ջ���"
         End
      End
      Begin VB.Menu keyPerformOther 
         Caption         =   "&������Ẻ"
         Begin VB.Menu keyRPTCalLand5Other 
            Caption         =   "&�.�.�.5  Ẻ�ʴ����պ��ا��ͧ���"
         End
         Begin VB.Menu keyRPTCalLand2Other 
            Caption         =   "&�.�.�.2   Ẻ�ʴ������ç���͹��з��Թ"
         End
         Begin VB.Menu keyRPTCalLand1Other 
            Caption         =   "&�.�.1   Ẻ�ʴ����ջ���"
         End
      End
      Begin VB.Menu KeyAcceptOther 
         Caption         =   "&��û����Թ"
         Begin VB.Menu keyRPTCalLand9Other 
            Caption         =   "&�.�.�.9  Ẻ�����Թ���պ��ا��ͧ���"
         End
         Begin VB.Menu keyRPTCalLand8Other 
            Caption         =   "&�.�.�.8  ��駡�û����Թ�����õ�� 24"
         End
         Begin VB.Menu keyRPTCalLand3Other 
            Caption         =   "&�.�.3  ˹ѧ����駡�û����Թ����"
         End
      End
      Begin VB.Menu keyPaymentOther 
         Caption         =   "&��ê�������"
         Begin VB.Menu keyPaymentRBT11Other 
            Caption         =   "&�.�.�.11  ���պ��ا��ͧ��� (������Ѻ�Թ) "
         End
         Begin VB.Menu keyPaymentPRD12Other 
            Caption         =   "&�.�.�.12 �����ç���͹��з��Թ (������Ѻ�Թ)"
         End
         Begin VB.Menu keyPaymentPT7Other 
            Caption         =   "&�.�.7 ���ջ��� (������Ѻ�Թ)"
         End
      End
      Begin VB.Menu keyAssetAdd 
         Caption         =   "&��Ǵ�Թ����"
      End
   End
   Begin VB.Menu keyQuery 
      Caption         =   "&�ͺ���������"
      Begin VB.Menu keyQueryOwner 
         Caption         =   "&����¹��Ѿ���Թ"
      End
      Begin VB.Menu keyQueryLand 
         Caption         =   "&����¹���Թ"
      End
      Begin VB.Menu keyQueryBuild 
         Caption         =   "&����¹�ç���͹"
      End
      Begin VB.Menu keyQuerySingBord 
         Caption         =   "&����¹����"
      End
      Begin VB.Menu keyQuerySale 
         Caption         =   "&����¹�͹حҵ"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu keySurvey 
         Caption         =   "&Ẻ���Ǩ��з���¹"
         Begin VB.Menu keySurveyPT1 
            Caption         =   "&�.�.1  Ẻ���Ǩ����ǡѺ���Թ"
         End
         Begin VB.Menu keySurveyPT2 
            Caption         =   "&�.�.2  Ẻ���Ǩ����ǡѺ�ç���͹"
         End
         Begin VB.Menu keySurveyPT3 
            Caption         =   "&�.�.3  Ẻ���Ǩ����ǡѺ��������͹حҵ"
         End
         Begin VB.Menu keySurveyPT4 
            Caption         =   "&�.�.4  ����¹��Ѿ���Թ"
         End
         Begin VB.Menu keySurveyPT5 
            Caption         =   "&�.�.5  ����¹�������������"
         End
      End
      Begin VB.Menu keyAdjust 
         Caption         =   "&Ẻ��§ҹ��û�Ѻ������"
         Begin VB.Menu keyAdjust1 
            Caption         =   "��§ҹ��û�Ѻ������Ἱ�������� (Ẻ 2)"
         End
         Begin VB.Menu keyAdjust2 
            Caption         =   "��§ҹ��������¹��Ѿ���Թ(Ẻ 2/1)"
         End
      End
      Begin VB.Menu keyHistory 
         Caption         =   "&����ѵԡ�ê�������"
      End
   End
   Begin VB.Menu keyReport 
      Caption         =   "&��§ҹ��ػ"
      Begin VB.Menu keyReportAllAsset 
         Caption         =   "&��§ҹ����¹��Ѿ���Թ������"
      End
      Begin VB.Menu keyReportAllAssetTranfer 
         Caption         =   "&��§ҹ��Ѿ������¹�ŧ������"
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu keyReportKG1A 
         Caption         =   "&�.�.1  �����ࡳ���ҧ�Ѻ <��ºؤ��>"
      End
      Begin VB.Menu keyReportKG1B 
         Caption         =   "&�.�.1  �����ࡳ���ҧ�Ѻ <������ѡ��>"
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu KeyReportTaxEstimate 
         Caption         =   "&�š�û���ҳ����"
      End
      Begin VB.Menu keyReportCashBilling 
         Caption         =   "&��§ҹ���շ�������Ш��ѹ"
      End
      Begin VB.Menu keyReportBudget 
         Caption         =   "&��§ҹ��������㹻է�����ҳ"
      End
      Begin VB.Menu keyReportBudgetCredit 
         Caption         =   "&��§ҹ���դ�ҧ㹻է�����ҳ"
      End
      Begin VB.Menu keyReportRankGK 
         Caption         =   "&��§ҹ�ѹ�Ѻ����ҧ��������"
      End
   End
   Begin VB.Menu keyControl 
      Caption         =   "&�к��Ǻ���"
      Begin VB.Menu keyControlSecurity 
         Caption         =   "&�к��ѡ�Ҥ�����ʹ���"
      End
      Begin VB.Menu keyControlBackup 
         Caption         =   "&�к����ͧ������"
      End
      Begin VB.Menu mnuSep5 
         Caption         =   "-"
      End
      Begin VB.Menu keyControlIndex 
         Caption         =   "&�к��絤���������"
      End
   End
   Begin VB.Menu keyAnalyse 
      Caption         =   "&�к���������"
      Begin VB.Menu keyAnalyseMap 
         Caption         =   "&������������(MIS,GIS)"
      End
      Begin VB.Menu keyScantomize 
         Caption         =   "&�����Դ��Ҵ��鹷��"
      End
      Begin VB.Menu keyAnalyseID 
         Caption         =   "&��ë�Ӣͧ���ʢ�����"
      End
   End
   Begin VB.Menu keyHelp 
      Caption         =   "&�Ը���"
      Begin VB.Menu keyHelpUse 
         Caption         =   "&�Ը��� MicroTax"
      End
      Begin VB.Menu keyHelpUseCheet 
         Caption         =   "&������Ἱ�������"
      End
      Begin VB.Menu mnuSep6 
         Caption         =   "-"
      End
      Begin VB.Menu keyHelpWeb 
         Caption         =   "&MicroTax On Web"
      End
      Begin VB.Menu mnuSep7 
         Caption         =   "-"
      End
      Begin VB.Menu keyHelpAbout 
         Caption         =   "&����ǡѺ ����� MicroTax"
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu Separator1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddName 
         Caption         =   "+ ������ª���"
      End
      Begin VB.Menu mnuDelName 
         Caption         =   "- �͹��ª���"
      End
      Begin VB.Menu Separator2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSetOwner 
         Caption         =   "@ ��˹�������Է��줹�á"
      End
      Begin VB.Menu mnuSetOwnerMail 
         Caption         =   "@ ��˹�����Ѻ������"
      End
      Begin VB.Menu mnuDelImg 
         Caption         =   "- ź�ٻ�Ҿ"
      End
      Begin VB.Menu Separator3 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPopup2 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu Separator4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddName2 
         Caption         =   "+ ������ª���"
      End
      Begin VB.Menu mnuDelName2 
         Caption         =   "- �͹��ª���"
      End
      Begin VB.Menu Separator5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSetOwner2 
         Caption         =   "@ ��˹�������Է��줹�á"
      End
      Begin VB.Menu mnuSetOwnerMail2 
         Caption         =   "@ ��˹�����Ѻ������"
      End
      Begin VB.Menu mnuDelImg2 
         Caption         =   "- ź�ٻ�Ҿ"
      End
      Begin VB.Menu Separator6 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPopup3 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu Separator7 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddName3 
         Caption         =   "+ ������ª���"
      End
      Begin VB.Menu mnuDelName3 
         Caption         =   "- �͹��ª���"
      End
      Begin VB.Menu Separator8 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSetOwner3 
         Caption         =   "@ ��˹�������Է��줹�á"
      End
      Begin VB.Menu mnuSetOwnerMail3 
         Caption         =   "@ ��˹�����Ѻ������"
      End
      Begin VB.Menu mnuDelImg3 
         Caption         =   "- ź�ٻ�Ҿ"
      End
      Begin VB.Menu Separator9 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPopup4 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu Separator10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddName4 
         Caption         =   "+ ������ª���"
      End
      Begin VB.Menu mnuDelName4 
         Caption         =   "- �͹��ª���"
      End
      Begin VB.Menu Separator11 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPopup5 
      Caption         =   "Menu"
      Visible         =   0   'False
      Begin VB.Menu mnuMAKECURRENT 
         Caption         =   "Make Current"
      End
      Begin VB.Menu Break1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditable 
         Caption         =   "Editable"
      End
      Begin VB.Menu mnuHittable 
         Caption         =   "Hittable"
      End
      Begin VB.Menu mnuVisible 
         Caption         =   "Visible"
      End
      Begin VB.Menu mnuInvisible 
         Caption         =   "Invisible"
      End
      Begin VB.Menu Break2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelOverlay 
         Caption         =   "ź��鹢�����"
      End
      Begin VB.Menu mnuRenameOverlay 
         Caption         =   "����¹���ͪ�鹢�����"
      End
   End
   Begin VB.Menu mnuTheme 
      Caption         =   "Thematic"
      Visible         =   0   'False
      Begin VB.Menu mnuAddTheme 
         Caption         =   "Add Theme"
      End
      Begin VB.Menu mnu_RemoveTheme 
         Caption         =   "Remove Theme"
      End
      Begin VB.Menu Break3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExploy 
         Caption         =   "���·�����"
      End
      Begin VB.Menu mnuReduce 
         Caption         =   "��ͷ�����"
      End
      Begin VB.Menu Break4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuReTheme 
         Caption         =   "����¹���ͻ����� Theme"
      End
   End
End
Attribute VB_Name = "FrmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WinHelp Lib "user32" Alias "WinHelpA" (ByVal hWnd As Long, ByVal lpHelpFile As String, ByVal wCommand As Long, ByVal dwData As Long) As Long
Private Const HELP_CONTENTS As Long = &H3&
Private Const HEIGTH_LEFTBAR = 555
Dim Hexi As Byte
Dim Payment_NO As Byte

Private Function pGetPicture(order As Byte) As StdPicture
        Set pGetPicture = ImageList1.ListImages(order).Picture
End Function

Private Sub Label3_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        Shape2.Top = 1410
End Sub

Private Sub Label4_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        Shape2.Top = 1830
End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        Shape2.Top = 2250
End Sub

Private Sub keyAdjust1_Click()
        fType = 0
        Frm_PrintAdjust2.Show vbModal
End Sub

Private Sub keyAdjust2_Click()
        fType = 1
        Frm_PrintAdjust2.Show vbModal
End Sub

Private Sub keyAnalyseID_Click()
        If TProduct = "STANDARD" Then
            MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
        If TProduct = "DEMO" And TCampaign = "TMIS" Then
            MsgBox "GIS Map Link Not Active on Microtax MIS  DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
        On Error GoTo ErrHld
                    Picture1.Visible = False
                    Frm_Map.ZOrder
                    Frm_Map.Show
               If Clone_Form.ActiveForm.Name = "Frm_Map" Then
                    If LenB(Trim$(Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$"))) <> 0 Then
                            Frm_AnalyseID.Show
                    Else
                            MsgBox "Not found Key License Cadcorp", vbExclamation, "����͹"
                            Exit Sub
                    End If
               Else
                    MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
               End If
        Exit Sub
ErrHld:
        MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
End Sub

Private Sub keyAnalyseMap_Click()
        On Error GoTo ErrShow
        If TProduct = "STANDARD" Then
            MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
        If TProduct = "DEMO" And TCampaign = "TMIS" Then
            MsgBox "GIS Map Link Not Active on Microtax MIS  DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
        Frm_Map.ZOrder
        Frm_Map.Show
        If Clone_Form.ActiveForm.Name = "Frm_Map" Then
                    If LenB(Trim$(Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$"))) <> 0 Then
                            Frm_AnalyseMap.Show
                    Else
                            MsgBox "Not found Key License Cadcorp", vbExclamation, "����͹"
                            Exit Sub
                    End If
       Else
                    MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
       End If
        Exit Sub
ErrShow:
        MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
End Sub

Private Sub keyAssetAdd_Click()
        Frm_Asset_Add.Show
        Frm_Asset_Add.ZOrder
End Sub

Private Sub KeyBuildingOther_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "6"
        Frm_PayTax.LB_Menu.Caption = "�����ç���͹��з��Թ(�������)"
End Sub

Private Sub keyBulidingData_Click()
        Call Lb_Reg_MouseUp(3, 1, 0, 0, 0)
End Sub

Private Sub keyCalBuild_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "2"
        Frm_PayTax.LB_Menu.Caption = "�����ç���͹��з��Թ"
End Sub

Private Sub keyCalLand_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "1"
        Frm_PayTax.LB_Menu.Caption = "���պ��ا��ͧ���"
End Sub

Private Sub keyCalLicense_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "4"
        Frm_PayTax.LB_Menu.Caption = "�����͹حҵ��Сͺ��ä��"
End Sub

Private Sub keyCalSignBord_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "3"
        Frm_PayTax.LB_Menu.Caption = "���ջ���"
End Sub

Private Sub keyCalTaxYear_New_Click()
        Frm_CalAllTax.Show
        Frm_CalAllTax.ZOrder
End Sub

Private Sub keyChangeAsset_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(9).Value = True
End Sub

Private Sub keyControlBackup_Click()
        Frm_Backup.Show vbModal
End Sub

Private Sub keyControlIndex_Click()
        Call Pic_Home_MouseUp(1, 1, 1, 1)
        Frm_Config.Show vbModal
End Sub

Private Sub keyControlSecurity_Click()
        Frm_CreatePassword.Show vbModal
End Sub

Private Sub keyEditBulidingData_Click()
        Call Lb_ChgReg_MouseUp(2, 1, 1, 1, 1)
End Sub

Private Sub keyEditLandData_Click()
        Call Lb_ChgReg_MouseUp(1, 1, 1, 1, 1)
End Sub

Private Sub keyEditSignbordData_Click()
        Call Lb_ChgReg_MouseUp(3, 1, 1, 1, 1)
End Sub

Private Sub keyEditWorkLicense_Click()
        Call Lb_ChgReg_MouseUp(4, 1, 1, 1, 1)
End Sub

Private Sub keyHelpAbout_Click()
        With Frm_Logo
            .Picture1.Visible = True
            .Timer1.Enabled = True
            .Label1.Visible = False
            If TProduct <> "STANDARD" Then
               If TProduct = "DEMO" Then
                    If TCampaign = "TGIS" Then
                          .Lb_LicenceCadcorp.Caption = Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$")
                    End If
               Else
                    .Lb_LicenceCadcorp.Caption = Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$")
               End If
             End If
            .Height = 6660
            .Show vbModal
        End With
End Sub

Private Sub keyHelpUse_Click()
        Call Picture14_MouseUp(1, 1, 1, 1)
End Sub

Private Sub keyHelpWeb_Click()
        ShellExecute hWnd, "Open", "www.envitec.co.th", &O0, &O0, vbNormalFocus
End Sub

Private Sub keyHistory_Click()
        Frm_StoryPayment.ZOrder
        Frm_StoryPayment.Show
End Sub

Private Sub keyLandData_Click()
        Call Lb_Reg_MouseUp(2, 1, 0, 0, 0)
End Sub

Private Sub keyLandOther_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "5"
        Frm_PayTax.LB_Menu.Caption = "���պ��ا��ͧ���(�������)"
End Sub

Private Sub keyLocation_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(1).Value = True
End Sub

Private Sub keyOther_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(10).Value = True
End Sub

Private Sub keyOwnerShip_Click()
        Call Lb_Reg_MouseUp(1, 1, 0, 0, 0)
End Sub

Private Sub keyPaymentPBA2_Click()
        Call Lb_Invoice_MouseUp(4, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentPRD12_Click()
        Call Lb_Invoice_MouseUp(2, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentPRD12Other_Click()
        Call Lb_Invoice_MouseUp(6, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentPT7_Click()
        Call Lb_Invoice_MouseUp(3, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentPT7Other_Click()
        Call Lb_Invoice_MouseUp(7, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentRBT11_Click()
        Call Lb_Invoice_MouseUp(1, 1, 1, 1, 1)
End Sub

Private Sub keyPaymentRBT11Other_Click()
        Call Lb_Invoice_MouseUp(5, 1, 1, 1, 1)
End Sub

Private Sub keyPetition_Click()
        Frm_Petition.Show
        Frm_Petition.ZOrder
End Sub

Private Sub keyQueryBuild_Click()
        Call Lb_Find_MouseUp(3, 1, 1, 1, 1)
End Sub

Private Sub keyQueryLand_Click()
        Call Lb_Find_MouseUp(2, 1, 1, 1, 1)
End Sub

Private Sub keyQueryOwner_Click()
        Call Lb_Find_MouseUp(1, 1, 1, 1, 1)
End Sub

Private Sub keyQuerySale_Click()
        Call Lb_Find_MouseUp(5, 1, 1, 1, 1)
End Sub

Private Sub keyQuerySingBord_Click()
        Call Lb_Find_MouseUp(4, 1, 1, 1, 1)
End Sub

Private Sub keyRateBulid_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(2).Value = True
End Sub

Private Sub keyRateLand_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(5).Value = True
End Sub

Private Sub keyRateSignBord_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(6).Value = True
End Sub

Private Sub keyRateTax_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(8).Value = True
End Sub

Private Sub keyRateWorkLicense_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(7).Value = True
End Sub

Private Sub keyReportAllAsset_Click()
        Call Lb_Report_MouseUp(1, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportAllAssetTranfer_Click()
        Call Lb_Report_MouseUp(2, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportBudget_Click()
        Call Lb_Report_MouseUp(7, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportBudgetCredit_Click()
        Call Lb_Report_MouseUp(8, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportCashBilling_Click()
        Call Lb_Report_MouseUp(6, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportKG1A_Click()
        Call Lb_Report_MouseUp(3, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportKG1B_Click()
        Call Lb_Report_MouseUp(4, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyReportRankGK_Click()
        Call Lb_Report_MouseUp(9, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub KeyReportTaxEstimate_Click()
        Call Lb_Report_MouseUp(5, 1, 1, 1, 1)
        Frm_PrintSummary.LB_Menu.Caption = ""
End Sub

Private Sub keyRoad_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(3).Value = True
End Sub

Private Sub keyRPTCalLand1_Click()
        Call Lb_Assess_MouseUp(3, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand1Other_Click()
        Call Lb_Assess_MouseUp(12, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand2_Click()
        Call Lb_Assess_MouseUp(2, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand2Other_Click()
        Call Lb_Assess_MouseUp(11, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand3_Click()
        Call Lb_Assess_MouseUp(6, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand3Other_Click()
        Call Lb_Assess_MouseUp(15, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand4_Click()
        Frm_PreformTax.Show
        Frm_PreformTax.ZOrder
        Frm_PreformTax.LB_Menu.Tag = "4"
        Frm_PreformTax.LB_Menu.Caption = "����㺻�Сͺ��ä��"
        Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡���͹حҵ"
        Frm_PreformTax.Label2(0) = "�ӹǹ�͹حҵ :"
End Sub

Private Sub keyRPTCalLand5_Click()
        Call Lb_Assess_MouseUp(1, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand5Other_Click()
        Call Lb_Assess_MouseUp(10, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand6_Click()
        Frm_AcceptTax.Show
        Frm_AcceptTax.ZOrder
        Frm_AcceptTax.LB_Menu.Tag = "4"
        Frm_AcceptTax.LB_Menu.Caption = "�����͹حҵ��Сͺ��ä��"
        Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡���͹حҵ"
End Sub

Private Sub keyRPTCalLand8_Click()
        Call Lb_Assess_MouseUp(5, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand8Other_Click()
        Call Lb_Assess_MouseUp(14, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand9_Click()
        Call Lb_Assess_MouseUp(4, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLand9Other_Click()
        Call Lb_Assess_MouseUp(13, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLandPBT12_Click()
        Call Lb_Assess_MouseUp(7, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLandPRD9_Click()
        Call Lb_Assess_MouseUp(8, 1, 1, 1, 1)
End Sub

Private Sub keyRPTCalLandPT4_Click()
        Call Lb_Assess_MouseUp(9, 1, 1, 1, 1)
End Sub

Private Sub keyRPTWarningNewNotic_Click()
        fType = 0
        With Frm_InformPayTax
                .Height = 6200
                .txt_Message(1).Visible = False
                .Label1(11).Visible = False
                .Label1(12).Top = .Label1(12).Top - 65
                .Label1(4).Top = .Label1(4).Top - 65
                .Label1(6).Top = .Label1(6).Top - 65
                .Label1(8).Top = .Label1(8).Top - 65
                .Line1.y1 = .Shape1(0).Top - 40: .Line1.y2 = .Shape1(0).Top - 40
                .Shape1(0).Top = .Shape1(0).Top - 65
                .cbDay(0).Top = .cbDay(0).Top - 65
                .cbDay(1).Top = .cbDay(1).Top - 65
                .cbDay(2).Top = .cbDay(2).Top - 65
                .cmdSave_Txt.Top = .cmdSave_Txt.Top - 65
                .Label1(3).Top = .Label1(3).Top - 65
                .Label1(5).Top = .Label1(5).Top - 65
                .Label1(9).Top = .Label1(9).Top - 65
                .Label1(7).Top = .Label1(7).Top - 65
                .Text1(2).Top = .Text1(2).Top - 65
                .Combo1(0).Top = .Combo1(0).Top - 65
                .Combo1(1).Top = .Combo1(1).Top - 65
                .Cmb_Type.Top = .Cmb_Type.Top - 65
                .Btn_Search.Top = .Btn_Search.Top - 65
                .Shape1(1).Top = .Shape1(1).Top - 65
                .Btn_Print.Top = .Btn_Print.Top - 65
                .cmd_Prit_Label.Top = .cmd_Prit_Label.Top - 65
                .opCopy.Top = .opCopy.Top - 65
                .opReal.Top = .opReal.Top - 65
                .Show vbModal
         End With
End Sub

Private Sub keyRPTWarningNotic_Click()
        Frm_Print_Warn.LB_Menu.Tag = 1
        Frm_Print_Warn.Show vbModal
        Frm_Print_Warn.ZOrder
        Frm_Print_Warn.LB_Menu.Caption = "Ẻ�������§ҹ ˹ѧ��������Ẻ"
        Chk_Ownership = 0
End Sub

Private Sub keyRPTWarningNotic2_Click()
        fType = 1
        Frm_InformPayTax.Show vbModal
        Chk_Ownership = 0
End Sub

Private Sub keyRPTWarningOtherNotic_Click()
        fType = 2
        With Frm_InformPayTax
                .Height = 6200
                .txt_Message(1).Visible = False
                .Label1(11).Visible = False
                .Label1(12).Top = .Label1(12).Top - 65
                .Label1(4).Top = .Label1(4).Top - 65
                .Label1(6).Top = .Label1(6).Top - 65
                .Label1(8).Top = .Label1(8).Top - 65
                .Line1.y1 = .Shape1(0).Top - 40: .Line1.y2 = .Shape1(0).Top - 40
                .Shape1(0).Top = .Shape1(0).Top - 65
                .cbDay(0).Top = .cbDay(0).Top - 65
                .cbDay(1).Top = .cbDay(1).Top - 65
                .cbDay(2).Top = .cbDay(2).Top - 65
                .cmdSave_Txt.Top = .cmdSave_Txt.Top - 65
                .Label1(3).Top = .Label1(3).Top - 65
                .Label1(5).Top = .Label1(5).Top - 65
                .Label1(9).Top = .Label1(9).Top - 65
                .Label1(7).Top = .Label1(7).Top - 65
                .Text1(2).Top = .Text1(2).Top - 65
                .Combo1(0).Top = .Combo1(0).Top - 65
                .Combo1(1).Top = .Combo1(1).Top - 65
                .Cmb_Type.Top = .Cmb_Type.Top - 65
                .Btn_Search.Top = .Btn_Search.Top - 65
                .Shape1(1).Top = .Shape1(1).Top - 65
                .Btn_Print.Top = .Btn_Print.Top - 65
                .cmd_Prit_Label.Top = .cmd_Prit_Label.Top - 65
                .opCopy.Top = .opCopy.Top - 65
                .opReal.Top = .opReal.Top - 65
                .Show vbModal
         End With
End Sub

Private Sub keyRPTWarningPBT5_Click()
        Frm_Print_Warn.LB_Menu.Tag = 2
        Frm_Print_Warn.LB_Menu.Caption = "Ẻ˹ѧ�����͹�������պ��ا��ͧ���"
        Frm_Print_Warn.Show vbModal
        Frm_Print_Warn.ZOrder
        Chk_Ownership = 0
End Sub

Private Sub keyRPTWarningPP1_Click()
        Frm_Print_Warn.LB_Menu.Tag = 4
        Frm_Print_Warn.LB_Menu.Caption = "Ẻ˹ѧ�����͹�������ջ���"
        Frm_Print_Warn.Show vbModal
        Frm_Print_Warn.ZOrder
        Chk_Ownership = 0
End Sub

Private Sub keyRPTWarningPRD2_Click()
        Frm_Print_Warn.LB_Menu.Tag = 3
        Frm_Print_Warn.LB_Menu.Caption = "Ẻ˹ѧ�����͹���������ç���͹"
        Frm_Print_Warn.Show vbModal
        Frm_Print_Warn.ZOrder
        Chk_Ownership = 0
End Sub

Private Sub keyScantomize_Click()
        On Error GoTo ErrShow
        If TProduct = "STANDARD" Then
            MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
        If TProduct = "DEMO" And TCampaign = "TMIS" Then
            MsgBox "GIS Map Link Not Active on Microtax MIS  DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
            Exit Sub
        End If
                    Picture1.Visible = False
                    Frm_Map.ZOrder
                    Frm_Map.Show
        If Clone_Form.ActiveForm.Name = "Frm_Map" Then
                    If LenB(Trim$(Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$"))) <> 0 Then
                            Frm_Scantomize.Show
                    Else
                            MsgBox "Not found Key License Cadcorp", vbExclamation, "����͹"
                            Exit Sub
                    End If
       Else
                    MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
       End If
        Exit Sub
ErrShow:
        MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
End Sub

Private Sub keySignboardOther_Click()
        Frm_PayTax.Show
        Frm_PayTax.ZOrder
        Frm_PayTax.LB_Menu.Tag = "7"
        Frm_PayTax.LB_Menu.Caption = "���ջ���(�������)"
End Sub

Private Sub keySignbordData_Click()
        Call Lb_Reg_MouseUp(4, 1, 0, 0, 0)
End Sub

Private Sub keySurveyPT1_Click()
        Frm_Print.Show vbModal
End Sub

Private Sub keySurveyPT2_Click()
        Frm_Print.SSTab1.Tab = 2
        Frm_Print.Label1.Caption = "Ẻ�������§ҹ �.�2"
        Frm_Print.Show vbModal
End Sub

Private Sub keySurveyPT3_Click()
        Frm_Print.SSTab1.Tab = 1
        Frm_Print.Label1.Caption = "Ẻ�������§ҹ �.�3"
        Frm_Print.Show vbModal
End Sub

Private Sub keySurveyPT4_Click()
        Frm_Print.Label2(3).Caption = "������"
        Frm_Print.Cmb_Zoneblock.Enabled = False
        Frm_Print.SSTab1.Tab = 3
        Frm_Print.Label1.Caption = "Ẻ�������§ҹ �.�4"
        Frm_Print.Option2.Visible = True
        Frm_Print.Option1.Caption = "���ا��ͧ���"
        Frm_Print.Option3.Caption = "���� - �͹حҵ��ҧ�"
        Frm_Print.Show vbModal
End Sub

Private Sub keySurveyPT5_Click()
        Frm_Print.Label2(3).Caption = "������"
        Frm_Print.Cmb_Zoneblock.Enabled = False
        Frm_Print.SSTab1.Tab = 3
        Frm_Print.Label1.Caption = "Ẻ�������§ҹ �.�5"
        Frm_Print.Option3.Caption = "���ջ��� - �͹حҵ��ҧ�"
        Frm_Print.Option1.Caption = "�����ç���͹��з��Թ - ���ا��ͧ���"
        Frm_Print.Option2.Visible = False
        Frm_Print.Show vbModal
End Sub

Private Sub keyWorkLicense_Click()
        Call Lb_Reg_MouseUp(5, 1, 0, 0, 0)
End Sub

Private Sub keyZone_Click()
        Call DETERMINE_FORM(Clone_Form.Name, False)
        Frm_Index.Show
        Frm_Index.ZOrder
        Frm_Index.OpMenu(4).Value = True
End Sub

Private Sub Label1_Click()
   MsgBox "�ӹǹ��������ҹ�����������ҧ : " & THIT & " ����", vbExclamation, "Microtax Software"
End Sub

Private Sub Lb_Analyse_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
    Lb_Analyse(Index).Top = Lb_Analyse(Index).Top + 30
End Sub

Private Sub Lb_Analyse_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 2
       Lb_Analyse(i).ForeColor = &H0&
Next i
Select Case Index
             Case 1
                    Shape10.Top = 570
             Case 2
                    Shape10.Top = 990
End Select
              Lb_Analyse(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Analyse_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
If TProduct = "STANDARD" Then
            MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
            Lb_Analyse(1).Top = 660
            Lb_Analyse(2).Top = 1080
            Exit Sub
End If
If TProduct = "DEMO" And TCampaign = "TMIS" Then
            MsgBox "GIS Map Link Not Active on Microtax MIS  DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
            Lb_Analyse(1).Top = 660
            Lb_Analyse(2).Top = 1080
    Exit Sub
End If
On Error GoTo ErrHld
            Picture1.Visible = False
            Frm_Map.ZOrder
             Frm_Map.Show
        If Clone_Form.ActiveForm.Name = "Frm_Map" Then
                If LenB(Trim$(Frm_Map.Sis1.GetStr(SIS_OT_SYSTEM, 0, "_LicenceNumber$"))) <> 0 Then
                        Select Case Index
                                Case 1
                                            Lb_Analyse(Index).Top = 660
                                            Frm_Scantomize.Show
                                Case 2
                                            Lb_Analyse(Index).Top = 1080
                                            Frm_AnalyseID.Show
                        End Select
                Else
                        MsgBox "Not found Key License Cadcorp", vbExclamation, "����͹"
                        Exit Sub
                End If
       Else
                Lb_Analyse(Index).Top = 660
                Lb_Analyse(Index).Top = 1080
                MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
       End If

Exit Sub
ErrHld:
                Lb_Analyse(1).Top = 660
                Lb_Analyse(2).Top = 1080
                MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
End Sub

Private Sub Lb_Assess_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Lb_Assess(Index).Top = Lb_Assess(Index).Top + 30
End Sub

Private Sub Lb_Assess_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 9
       Lb_Assess(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape6.Top = 570
            Case 2
                    Shape6.Top = 990
            Case 3
                     Shape6.Top = 1410
            Case 4
                    Shape6.Top = 1830
            Case 5
                  Shape6.Top = 2250
            Case 6
                  Shape6.Top = 2670
              Case 7
                  Shape6.Top = 3090
                Case 8
                  Shape6.Top = 3510
                Case 9
                  Shape6.Top = 3930
End Select
   Lb_Assess(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Assess_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case Index
            Case 1
                            Lb_Assess(Index).Top = 660
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "1"
                            Frm_PreformTax.LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.5)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.5"
                            Frm_PreformTax.Label2(0) = "�ӹǹ�ŧ :"
            Case 2
                            Lb_Assess(Index).Top = 1080
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "2"
                            Frm_PreformTax.LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.2)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.2"
                            Frm_PreformTax.Label2(0) = "�ӹǹ�ç���͹ :"
           Case 3
                            Lb_Assess(Index).Top = 1500
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "3"
                            Frm_PreformTax.LB_Menu.Caption = "���ջ��� (�.�.1)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.1"
                            Frm_PreformTax.Label2(0) = "�ӹǹ���� :"
            Case 4
                            Lb_Assess(Index).Top = 1920
                            Assets_Type = "1"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "1"
                            Frm_AcceptTax.LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.9)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.9"
            Case 5
                            Lb_Assess(Index).Top = 2340
                            Assets_Type = "2"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "2"
                            Frm_AcceptTax.LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.8)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.8"
            Case 6
                            Lb_Assess(Index).Top = 2760
                            Assets_Type = "3"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "3"
                            Frm_AcceptTax.LB_Menu.Caption = "���ջ��� (�.�.3)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� (�.�.3)"
            Case 7  '��ط�����Թ
                            Lb_Assess(Index).Top = 3180
                            With Frm_Request
                                    .Show
                                    .ZOrder
                                    .LB_Menu.Tag = "1"
                                    .LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.12)"
                                    .Btn_Post_Request.Caption = "�Ѵ����¡�� �.�.�.12"
                            End With
            Case 8 '��طó��ç���͹
                            Lb_Assess(Index).Top = 3600
                            With Frm_Request
                                    .Show
                                    .ZOrder
                                    .LB_Menu.Tag = "2"
                                    .LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.9)"
                                    .Btn_Post_Request.Caption = "�Ѵ����¡�� �.�.�.9"
                            End With
            Case 9 '��ط�ó����
                            Lb_Assess(Index).Top = 4020
                            With Frm_Request
                                    .Show
                                    .ZOrder
                                    .LB_Menu.Tag = "3"
                                    .LB_Menu.Caption = "���ջ��� (�.�.4)"
                                    .Btn_Post_Request.Caption = "�Ѵ����¡�� �.�.4"
                            End With
            Case 10   '���պ��ا��ͧ���(�������)
'                            Lb_Assess(Index).Top = 660
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "5"
                            Frm_PreformTax.LB_Menu.Caption = "���պ��ا��ͧ��� (�������)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.5"
                            Frm_PreformTax.Label2(0) = "�ӹǹ�ŧ :"
            Case 11
'                            Lb_Assess(Index).Top = 1080
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "6"
                            Frm_PreformTax.LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.2"
                            Frm_PreformTax.Label2(0) = "�ӹǹ�ç���͹ :"
            Case 12
'                            Lb_Assess(Index).Top = 1500
                            Frm_PreformTax.Show
                            Frm_PreformTax.ZOrder
                            Frm_PreformTax.LB_Menu.Tag = "7"
                            Frm_PreformTax.LB_Menu.Caption = "���ջ��� (�������)"
                            Frm_PreformTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.1"
                            Frm_PreformTax.Label2(0) = "�ӹǹ���� :"
            Case 13
'                            Lb_Assess(Index).Top = 1920
                            Assets_Type = "5"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "1"
                            Frm_AcceptTax.LB_Menu.Caption = "���պ��ا��ͧ��� (�������)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.9"
            Case 14
'                            Lb_Assess(Index).Top = 2340
                            Assets_Type = "6"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "2"
                            Frm_AcceptTax.LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� �.�.�.8"
            Case 15
'                            Lb_Assess(Index).Top = 2760
                            Assets_Type = "7"
                            Frm_AcceptTax.Show
                            Frm_AcceptTax.ZOrder
'                            Frm_AcceptTax.LB_Menu.Tag = "3"
                            Frm_AcceptTax.LB_Menu.Caption = "���ջ��� (�������)"
                            Frm_AcceptTax.Btn_Post_PBT5.Caption = "�Ѵ����¡�� (�.�.3)"
End Select
End Sub

Private Sub Lb_ChgReg_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Lb_ChgReg(Index).Top = Lb_ChgReg(Index).Top + 30
End Sub

Private Sub Lb_ChgReg_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 9
       Lb_ChgReg(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape4.Top = 570
            Case 2
                    Shape4.Top = 990
            Case 3
                     Shape4.Top = 1410
            Case 4
                    Shape4.Top = 1830
            Case 5
                  Shape4.Top = 2250
            Case 6
                  Shape4.Top = 2670
              Case 7
                  Shape4.Top = 3090
                Case 8
                  Shape4.Top = 3510
                  Case 9
                  Shape4.Top = 3930
End Select
   Lb_ChgReg(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_ChgReg_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case Index
            Case 1
                                    Lb_ChgReg(Index).Top = 660
                                     Frm_ChangeOwner.Show
                                     Frm_ChangeOwner.ZOrder
                                     Frm_ChangeOwner.LBMenu.Tag = "1"
                                     Frm_ChangeOwner.LBMenu.Caption = "�����ŷ���¹���Թ"
            Case 2
                               Lb_ChgReg(Index).Top = 1080
                                     Frm_ChangeOwner.Show
                                     Frm_ChangeOwner.ZOrder
                                     Frm_ChangeOwner.LBMenu.Tag = "2"
                                     Frm_ChangeOwner.LBMenu.Caption = "�����ŷ���¹�ç���͹"
           Case 3
                              Lb_ChgReg(Index).Top = 1500
                                     Frm_ChangeOwner.Show
                                     Frm_ChangeOwner.ZOrder
                                     Frm_ChangeOwner.LBMenu.Tag = "3"
                                     Frm_ChangeOwner.LBMenu.Caption = "�����ŷ���¹����"
            Case 4
                                Lb_ChgReg(Index).Top = 1920
                                     Frm_ChangeOwner.Show
                                     Frm_ChangeOwner.ZOrder
                                     Frm_ChangeOwner.LBMenu.Tag = "4"
                                     Frm_ChangeOwner.LBMenu.Caption = "�������͹حҵ"
            Case 5
                                Lb_ChgReg(Index).Top = 2340
            Case 6
                                Lb_ChgReg(Index).Top = 2760
            Case 7
                                Lb_ChgReg(Index).Top = 3180
            Case 8
                                Lb_ChgReg(Index).Top = 3600
            Case 9
                                Lb_ChgReg(Index).Top = 4020
End Select
             If Index > 4 Then
             MsgBox "�������ѧ������ա�èѴ�� �������ö��ҹ��", , "Information ?"
             End If
End Sub

Private Sub Lb_Control_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
    Lb_Control(Index).Top = Lb_Control(Index).Top + 30
End Sub

Private Sub Lb_Control_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 3
       Lb_Control(i).ForeColor = &H0&
Next i
Select Case Index
             Case 1
                    Shape9.Top = 570
             Case 2
                    Shape9.Top = 990
             Case 3
                    Shape9.Top = 1410
End Select
              Lb_Control(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Control_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case Index
            Case 1
                               Lb_Control(Index).Top = 660
                               Frm_CreatePassword.Show vbModal
            Case 2
                               Lb_Control(Index).Top = 1080
                               Frm_Backup.Show vbModal
           Case 3
                              Lb_Control(Index).Top = 1500
                              Frm_Config.Show vbModal
End Select
End Sub

Private Sub Lb_Find_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
 Lb_Find(Index).Top = Lb_Find(Index).Top + 30
End Sub

Private Sub Lb_Find_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 5
       Lb_Find(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape5.Top = 570
            Case 2
                    Shape5.Top = 990
            Case 3
                     Shape5.Top = 1410
            Case 4
                    Shape5.Top = 1830
            Case 5
                  Shape5.Top = 2250
End Select
Lb_Find(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Find_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Call DETERMINE_FORM(Clone_Form.Name, False)
         Frm_Search.Show
         Frm_Search.ZOrder
         Frm_Search.Shp_Menu(1).BackColor = &HD6D6D6
         Frm_Search.Shp_Menu(2).BackColor = &HD6D6D6
         Frm_Search.Shp_Menu(3).BackColor = &HD6D6D6
         Frm_Search.Shp_Menu(4).BackColor = &HD6D6D6
         Frm_Search.Shp_Menu(5).BackColor = &HD6D6D6
    Select Case Index
            Case 1
                            Lb_Find(Index).Top = 660
                            Frm_Search.SSTab1.Tab = 0
                            Frm_Search.Shp_Menu(1).BackColor = &HB8CFD6
                            Frm_Search.optCode.Visible = False
                            Frm_Search.optName.Visible = False
            Case 2
                            Lb_Find(Index).Top = 1110
                            Frm_Search.SSTab1.Tab = 1
                            Frm_Search.Shp_Menu(2).BackColor = &HB8CFD6
                            Frm_Search.optCode.Visible = True
                            Frm_Search.optName.Visible = True
                            Frm_Search.optCode.Caption = "���ʷ��Թ"
           Case 3
                            Lb_Find(Index).Top = 1530
                            Frm_Search.SSTab1.Tab = 2
                            Frm_Search.Shp_Menu(3).BackColor = &HB8CFD6
                            Frm_Search.optCode.Visible = True
                            Frm_Search.optName.Visible = True
                            Frm_Search.optCode.Caption = "�����ç���͹"
            Case 4
                            Lb_Find(Index).Top = 1950
                            Frm_Search.SSTab1.Tab = 3
                            Frm_Search.Shp_Menu(4).BackColor = &HB8CFD6
                            Frm_Search.optCode.Visible = True
                            Frm_Search.optName.Visible = True
                            Frm_Search.optCode.Caption = "���ʻ���"
            Case 5
                            Lb_Find(Index).Top = 2370
                            Frm_Search.SSTab1.Tab = 4
                            Frm_Search.Shp_Menu(5).BackColor = &HB8CFD6
                            Frm_Search.optCode.Visible = False
                            Frm_Search.optName.Visible = False
      End Select
End Sub

Private Sub Lb_Index_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Lb_Index(Index).Top = Lb_Index(Index).Top + 30
End Sub

Private Sub Lb_Index_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 8
       Lb_Index(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape3.Top = 570
            Case 2
                    Shape3.Top = 990
            Case 3
                     Shape3.Top = 1410
            Case 4
                    Shape3.Top = 1830
            Case 5
                  Shape3.Top = 2250
            Case 6
                  Shape3.Top = 2670
              Case 7
                  Shape3.Top = 3090
              Case 8
                  Shape3.Top = 3495
End Select
   Lb_Index(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Index_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Logic_Frm_Index_Menu = False
Call DETERMINE_FORM(Clone_Form.Name, False)
Clone_Form.Picture1.Visible = True
Select Case Index
            Case 1
                               Lb_Index(Index).Top = 660
                              Frm_Index.OpMenu(1).Value = True
            Case 2
                               Lb_Index(Index).Top = 1080
                              Frm_Index.OpMenu(3).Value = True
           Case 3
                              Lb_Index(Index).Top = 1500
                              Frm_Index.OpMenu(4).Value = True
            Case 4
                                Lb_Index(Index).Top = 1920
                              Frm_Index.OpMenu(5).Value = True
            Case 5
                                Lb_Index(Index).Top = 2340
                              Frm_Index.OpMenu(2).Value = True
            Case 6
                                Lb_Index(Index).Top = 2760
                              Frm_Index.OpMenu(6).Value = True
            Case 7
                                Lb_Index(Index).Top = 3180
                              Frm_Index.OpMenu(7).Value = True
            Case 8
                                Lb_Index(Index).Top = 3600
                              Frm_Index.OpMenu(8).Value = True
End Select
                              Frm_Index.Grid_VILLAGE.Rows = 5
                              Frm_Index.Show
End Sub

Private Sub Lb_Invoice_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
   Lb_Invoice(Index).Top = Lb_Invoice(Index).Top + 30
End Sub

Private Sub Lb_Invoice_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 4
       Lb_Invoice(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape7.Top = 570
            Case 2
                    Shape7.Top = 990
            Case 3
                     Shape7.Top = 1410
            Case 4
                    Shape7.Top = 1830
End Select
            Lb_Invoice(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Invoice_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case Index
            Case 1
                                Lb_Invoice(Index).Top = 660
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "1"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.�.11"
                                Frm_Payment.Label3(24).Caption = "�ŧ���Թ :"
                                Frm_Payment.LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.11)"
            Case 2
                                Lb_Invoice(Index).Top = 1080
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "2"
                                Frm_Payment.Label3(24).Caption = "�ç���͹ :"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.�.12"
                                Frm_Payment.LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.12)"
           Case 3
                                Lb_Invoice(Index).Top = 1500
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "3"
                                Frm_Payment.Label3(24).Caption = "���� :"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.7"
                                Frm_Payment.LB_Menu.Caption = "���ջ��� (�.�.7)"
            Case 4
                                Lb_Invoice(Index).Top = 1920
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "4"
                                Frm_Payment.Label3(24).Caption = "�͹حҵ :"
                                Frm_Payment.Btn_Post_PBT5.Caption = "���������͹حҵ"
                                Frm_Payment.LB_Menu.Caption = "�����͹حҵ��Сͺ��ä��"
            Case 5
'                                    Lb_Invoice(Index).Top = 660
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "5"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.�.11"
                                Frm_Payment.Label3(24).Caption = "�ŧ���Թ :"
                                Frm_Payment.LB_Menu.Caption = "���պ��ا��ͧ��� (�������)"
            Case 6
'                                    Lb_Invoice(Index).Top = 1080
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "6"
                                Frm_Payment.Label3(24).Caption = "�ç���͹ :"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.�.12"
                                Frm_Payment.LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
            Case 7
'                                    Lb_Invoice(Index).Top = 1500
                                Frm_Payment.Show
                                Frm_Payment.ZOrder
                                Frm_Payment.LB_Menu.Tag = "7"
                                Frm_Payment.Label3(24).Caption = "���� :"
                                Frm_Payment.Btn_Post_PBT5.Caption = "�������� �.�.7"
                                Frm_Payment.LB_Menu.Caption = "���ջ��� (�������)"
End Select
End Sub

Private Sub Lb_menuLeft_Click(Index As Integer)
Dim i As Byte, j As Integer
On Error Resume Next
Hexi = Index
For i = 1 To 8
      Pic_LfBar(i).Height = HEIGTH_LEFTBAR
      j = j + Pic_LfBar(i).Height
 Next i
For i = 2 To 9
               Pic_LfBar(i).Top = Pic_LfBar(i - 1).Top + Pic_LfBar(i - 1).Height
Next i
     Pic_LfBar(Index).Height = (Picture1.Height - j)
     Hexi = Index
     For i = Index To 8
          Pic_LfBar(i + 1).Top = Pic_LfBar(Hexi).Top + Pic_LfBar(Hexi).Height
          Hexi = Hexi + 1
     Next i
End Sub

Private Sub Lb_menuLeft_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Dim i As Byte
  For i = 1 To 9
      Shape1(i).Visible = False
  Next i
    Shape1(Index).Visible = True
End Sub

Private Sub Lb_Reg_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Lb_Reg(Index).Top = Lb_Reg(Index).Top + 30
End Sub

Private Sub Lb_Reg_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 5
       Lb_Reg(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape2.Top = 570
            Case 2
                    Shape2.Top = 990
            Case 3
                     Shape2.Top = 1410
            Case 4
                    Shape2.Top = 1830
            Case 5
                  Shape2.Top = 2250
End Select
   Lb_Reg(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Reg_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Call DETERMINE_FORM(Clone_Form.Name, False)
Select Case Index
            Case 1
                               Lb_Reg(Index).Top = 660
                                Frm_Ownership.Show
                                Frm_Ownership.SetFocus
            Case 2
                               Lb_Reg(Index).Top = 1050
                               Frm_LandUse.Show
'                               Frm_LandUse.ZOrder
                               Frm_LandUse.SetFocus
           Case 3
                              Lb_Reg(Index).Top = 1470
                               Frm_Building.Show
                               Frm_Building.SetFocus
            Case 4
                                Lb_Reg(Index).Top = 1890
                               Frm_SignBord.Show
                               Frm_SignBord.SetFocus
            Case 5
                                Lb_Reg(Index).Top = 2340
                                Frm_License.Show
                                Frm_License.SetFocus
End Select
End Sub

Private Sub Lb_Report_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Lb_Report(Index).Top = Lb_Report(Index).Top + 30
End Sub

Private Sub Lb_Report_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Dim i As Byte
For i = 1 To 7
       Lb_Report(i).ForeColor = &H0&
Next i
Select Case Index
            Case 1
                    Shape8.Top = 570
            Case 2
                    Shape8.Top = 990
            Case 3
                     Shape8.Top = 1410
            Case 4
                    Shape8.Top = 1830
            Case 5
                  Shape8.Top = 2250
            Case 6
                  Shape8.Top = 2670
              Case 7
                  Shape8.Top = 3090
                Case 8
                  Shape8.Top = 3510
End Select
   Lb_Report(Index).ForeColor = &HFFFFFF
End Sub

Private Sub Lb_Report_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case Index
            Case 1
                               Lb_Report(Index).Top = 660
                               Frm_PrintSummary.LB_Menu.Tag = "2"
            Case 2
                               Lb_Report(Index).Top = 1080
                               Frm_PrintSummary.LB_Menu.Tag = "3"
            Case 3
                               Lb_Report(Index).Top = 1500
                               Frm_PrintSummary.LB_Menu.Tag = "6"
            Case 4
                               Lb_Report(Index).Top = 1920
                               Frm_PrintSummary.LB_Menu.Tag = "11"
            Case 5
                               Lb_Report(Index).Top = 2340
                               Frm_PrintSummary.LB_Menu.Tag = "13"
            Case 6
                               Lb_Report(Index).Top = 2760
                               Frm_PrintSummary.LB_Menu.Tag = "14"
            Case 7
                               Lb_Report(Index).Top = 3180
                               Frm_PrintSummary.LB_Menu.Tag = "15"
            Case 8
                               Frm_PrintSummary.LB_Menu.Tag = "16"
            Case 9
                               Frm_PrintSummary.LB_Menu.Tag = "17"
End Select
                Frm_PrintSummary.Show
                Frm_PrintSummary.ZOrder
End Sub
Private Sub MDIForm_Load()
    If TProduct = "DEMO" Then Label1.Visible = True
'    pBuildMenus
    Picture100.Picture = LoadResPicture(101, 0)
    Pic_Home.Picture = LoadResPicture(114, 0)
    Picture6.Picture = LoadResPicture(108, 0)
    Pic_Lock.Picture = LoadResPicture(106, 0)
    Pic_Map.Picture = LoadResPicture(103, 0)
    Picture10.Picture = LoadResPicture(116, 0)
    Picture11.Picture = LoadResPicture(119, 0)
    Picture111.Picture = LoadResPicture(122, 0)
    Picture12.Picture = LoadResPicture(124, 0)
    Picture14.Picture = LoadResPicture(126, 0)
     Picture15.Picture = LoadResPicture(120, 0)
     Picture19.Picture = LoadResPicture(128, 0)
    Picture18.Picture = LoadResPicture(130, 0)
    Num_Time = 0
    Dim hMenu As Long, hSubMenu As Long, hID As Long
    '* Get the menuhandle of your application
hMenu = GetMenu(Me.hWnd)
'* Get the handle of the first submenu
hSubMenu = GetSubMenu(hMenu, 0)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
'hID = GetMenuItemID(hSubMenu, 3)
'SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 5)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 6)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 7)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 8)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 9)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture
hID = GetMenuItemID(hSubMenu, 10)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(4).Picture, ImageList2.ListImages(4).Picture

'* Get the handle of the second submenu of Application
hSubMenu = GetSubMenu(hMenu, 1)
'* Get the menuId of the first menu entry of Second Sub Menu
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(1).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(1).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(1).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(1).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(1).Picture

hSubMenu = GetSubMenu(hMenu, 2)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture

hSubMenu = GetSubMenu(hMenu, 4)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(6).Picture, ImageList2.ListImages(6).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(6).Picture, ImageList2.ListImages(6).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(6).Picture, ImageList2.ListImages(6).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(6).Picture, ImageList2.ListImages(6).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(6).Picture, ImageList2.ListImages(6).Picture

hSubMenu = GetSubMenu(hMenu, 5)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 8)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(2).Picture, ImageList2.ListImages(2).Picture

hSubMenu = GetSubMenu(hMenu, 6)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 6)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 7)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 8)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture
hID = GetMenuItemID(hSubMenu, 9)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(3).Picture, ImageList2.ListImages(3).Picture


hSubMenu = GetSubMenu(hMenu, 7)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 4)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 6)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 7)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 8)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 9)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture
hID = GetMenuItemID(hSubMenu, 10)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(1).Picture, ImageList2.ListImages(2).Picture

hSubMenu = GetSubMenu(hMenu, 8)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture
hID = GetMenuItemID(hSubMenu, 2)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture

hSubMenu = GetSubMenu(hMenu, 9)
hID = GetMenuItemID(hSubMenu, 0)
SetMenuItemBitmaps hMenu, hID, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture
hID = GetMenuItemID(hSubMenu, 1)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture
hID = GetMenuItemID(hSubMenu, 3)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture
hID = GetMenuItemID(hSubMenu, 5)
SetMenuItemBitmaps hMenu, hID&, MF_BITMAP, ImageList2.ListImages(5).Picture, ImageList2.ListImages(5).Picture


End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
        Set GBQueryVillage = Nothing
        Set GBQuerySoi = Nothing
        Set GBQueryStreet_Name = Nothing
        Set GBQueryZone = Nothing
        Set GBQueryProvince = Nothing
        Set GBQueryAmphoe = Nothing
        Set GBQueryTambon = Nothing
        Set GBQueryOwnerShip = Nothing
        Set GBQueryZoneTax = Nothing
        Set GBQueryLandUse = Nothing
        Set GBQuerySignBordData_Type = Nothing
        Set GBQueryLandData = Nothing
        Set GBQuerySignBordData = Nothing
        Set GBQueryBusiness = Nothing
        Set GBQueryClass = Nothing
        Set GBQueryBuildingData = Nothing
        Set GBQueryBuildingType = Nothing
        Set GBQueryBuildingRate = Nothing
        Set GBQueryDuePayTax = Nothing
        Set GBQueryLandEmployment = Nothing
        Set SearchRS = Nothing
'        Globle_Connective.Close
        Set Globle_Connective = Nothing
        Dim NewForm As Form
        For Each NewForm In Forms
'                If Not (NewForm.Name = "MDIStartup" Or NewForm.Name = "frmLogOff") Then
                        Unload NewForm
                        Set NewForm = Nothing
'                End If
        Next
End Sub

Private Sub Mnu_RemoveTheme_Click()
On Error Resume Next
    With Frm_Map.Sis1
          .RemoveOverlayTheme Frm_Map.tvwTheme.SelectedItem.Parent.Index - 1, CInt(Right(Frm_Map.tvwTheme.SelectedItem.Key, Len(Frm_Map.tvwTheme.SelectedItem.Key) - InStrRev(Frm_Map.tvwTheme.SelectedItem.Key, "-")))
           Frm_Map.tvwTheme.Nodes.Remove (Frm_Map.tvwTheme.SelectedItem.Index)
           .Redraw SIS_CURRENTWINDOW
     End With
End Sub

Private Sub mnuAddName_Click()
        If OrderListView = 1 Then
                    Status_InToFrm = "LN_USE"
                    Frm_SearchOwnerShip.Show
        End If
        If OrderListView = 2 Then
            Status_InToFrm = "LN_APY"
            Frm_SearchOwnerShip.Show
        End If
End Sub

Private Sub mnuAddName2_Click()
        If OrderListView = 1 Then
                    Status_InToFrm = "BD_OWN"
                    Frm_SearchOwnerShip.Show
        End If
        If OrderListView = 2 Then
            Status_InToFrm = "BD_APY"
            Frm_SearchOwnerShip.Show
        End If
End Sub

Private Sub mnuAddName3_Click()
        If OrderListView = 1 Then
            Status_InToFrm = "SG_OWN"
            Frm_SearchOwnerShip.Show
        End If
        If OrderListView = 2 Then
            Status_InToFrm = "SG_APY"
            Frm_SearchOwnerShip.Show
        End If
End Sub

Private Sub mnuAddName4_Click()
        Status_InToFrm = "LICENSE"
        Frm_SearchOwnerShip.Show
End Sub

Private Sub mnuAddTheme_Click()
        Frm_Map.Sis1.DoCommand "AComAddTheme"
        Call Frm_Map.Get_Overay
End Sub

Private Sub mnuDelImg_Click()
        If Frm_LandUse.LstV_Img_Name.ListItems.Count > 0 Then
                Frm_LandUse.LstV_Img_Name.ListItems.Remove (Frm_LandUse.LstV_Img_Name.SelectedItem.Index)
                Frm_LandUse.picPicture.Cls
        End If
End Sub

Private Sub mnuDelImg2_Click()
        If Frm_Building.LstV_Img_Name.ListItems.Count > 0 Then
            Frm_Building.LstV_Img_Name.ListItems.Remove (Frm_Building.LstV_Img_Name.SelectedItem.Index)
            Frm_Building.picPicture.Cls
            Frm_Building.picPicture.Left = 0
            Frm_Building.picPicture.Top = 0
        End If
End Sub

Private Sub mnuDelImg3_Click()
        If Frm_SignBord.LstV_Img_Name.ListItems.Count > 0 Then
            Frm_SignBord.LstV_Img_Name.ListItems.Remove (Frm_SignBord.LstV_Img_Name.SelectedItem.Index)
            Frm_SignBord.picPicture.Cls
        End If
End Sub

Private Sub mnuDelName_Click()
        If OrderListView = 1 Then
                With Frm_LandUse
                        If .LstV_OwnerShip.ListItems.Count > 0 Then
                            .LstV_OwnerShip.ListItems.Remove (.LstV_OwnerShip.SelectedItem.Index)
                                          .Lb_Ownership_Real_ID.Caption = ""
                                          .Lb_Owner_Type.Caption = ""
                                          .Lb_Owner_Name.Caption = ""
                                          .Lb_Owner_Address.Caption = ""
                                          .Lb_Email.Caption = ""
                        End If
                End With
        End If
        If OrderListView = 2 Then
                If Frm_LandUse.LstV_OwnerUse.ListItems.Count > 0 Then
                    Frm_LandUse.LstV_OwnerUse.ListItems.Remove (Frm_LandUse.LstV_OwnerUse.SelectedItem.Index)
                    Call Frm_LandUse.CalSumTax
                End If
        End If
End Sub

Private Sub mnuDelName2_Click()
        If OrderListView = 1 Then
                With Frm_Building
                    If .LstV_OwnerShip.ListItems.Count > 0 Then
                        .LstV_OwnerShip.ListItems.Remove (.LstV_OwnerShip.SelectedItem.Index)
                                      .Lb_Ownership_Real_ID.Caption = ""
                                      .Lb_Owner_Type.Caption = ""
                                      .Lb_Owner_Name.Caption = ""
                                      .Lb_Owner_Address.Caption = ""
                                      .Lb_Email.Caption = ""
                    End If
                End With
        End If
        If OrderListView = 2 Then
                With Frm_Building
                    If .LstV_OwnerUse.ListItems.Count > 0 Then
                        .LstV_OwnerUse.ListItems.Remove (.LstV_OwnerUse.SelectedItem.Index)
                                      .Lb_User_Real_ID.Caption = ""
                                      .Lb_User_Type.Caption = ""
                                      .Lb_User_Name.Caption = ""
                                      .Lb_User_Address.Caption = ""
                                      .Lb_User_Email.Caption = ""
                    End If
                End With
        End If
End Sub

Private Sub mnuDelName3_Click()
        If OrderListView = 1 Then
                If Frm_SignBord.LstV_OwnerShip.ListItems.Count > 0 Then
                    Frm_SignBord.LstV_OwnerShip.ListItems.Remove (Frm_SignBord.LstV_OwnerShip.SelectedItem.Index)
                End If
        End If
End Sub

Private Sub mnuDelName4_Click()
        With Frm_License
                .Lb_Owner_Add.Caption = vbNullString
                .Lb_Owner_ID.Caption = vbNullString
                .Lb_Owner_Name.Caption = vbNullString
                .Lb_Owner_Type.Caption = vbNullString
        End With
End Sub

Private Sub mnuDelOverlay_Click()
        If MsgBox("�׹�ѹ���ź��鹢����� """ & Frm_Map.tvwOverlay.SelectedItem.Text & """", vbYesNo + vbExclamation, "����͹") = vbYes Then
            Frm_Map.Sis1.RemoveOverlay (Frm_Map.tvwOverlay.SelectedItem.Index - 1)
            Frm_Map.tvwTheme.Nodes.Remove (Frm_Map.tvwOverlay.SelectedItem.Index)
            Frm_Map.tvwOverlay.Nodes.Remove (Frm_Map.tvwOverlay.SelectedItem.Index)
        End If
End Sub

Private Sub mnuEditable_Click()
        With Frm_Map
              Call .CLEAR_CHECK
              mnuEditable.Checked = True
              .tvwOverlay.Nodes(IndexNode).Checked = True
              .tvwOverlay.Nodes(IndexNode).Image = 1
              .tvwTheme.Nodes.Item(IndexNode).Checked = True
              .tvwTheme.Nodes.Item(IndexNode).Image = 1
        End With
        With Frm_Map.Sis1
             .SetInt SIS_OT_OVERLAY, CLng(IndexNode - 1), "_status&", SIS_EDITABLE
             .SetInt SIS_OT_DATASET, CLng(IndexNode - 1), "_bOwned&", True
             .SetInt SIS_OT_DATASET, CLng(IndexNode - 1), "_bEditable&", True
             .SetInt SIS_OT_DATASET, CLng(IndexNode - 1), "_bModified&", True
             .SetInt SIS_OT_DATASET, .GetInt(SIS_OT_OVERLAY, CLng(IndexNode - 1), "_nDataset&"), "_bOwned&", True
        End With
End Sub

Private Sub mnuExploy_Click()
        Dim i As Byte
        For i = 1 To Frm_Map.tvwTheme.Nodes.Count
                Frm_Map.tvwTheme.Nodes(i).Expanded = True
        Next
End Sub

Public Sub mnuHittable_Click()
        With Frm_Map
                Call .CLEAR_CHECK
                mnuHittable.Checked = True
                .tvwOverlay.Nodes(IndexNode).Checked = True
                .tvwOverlay.Nodes(IndexNode).Image = 1
                .tvwTheme.Nodes.Item(IndexNode).Checked = True
                .tvwTheme.Nodes.Item(IndexNode).Image = 1
                .Sis1.SetInt SIS_OT_OVERLAY, IndexNode - 1, "_status&", SIS_HITTABLE
        End With
End Sub

Public Sub mnuInvisible_Click()
        With Frm_Map
                Call .CLEAR_CHECK
                mnuInvisible.Checked = True
                .tvwOverlay.Nodes(IndexNode).Checked = False
                .tvwOverlay.Nodes(IndexNode).Image = 2
                .tvwTheme.Nodes.Item(IndexNode).Checked = False
                .tvwTheme.Nodes.Item(IndexNode).Image = 2
                .Sis1.DeselectAll
                .Sis1.SetInt SIS_OT_OVERLAY, IndexNode - 1, "_status&", SIS_INVISIBLE
        End With
End Sub

Private Sub mnuMAKECURRENT_Click()
        If Frm_Map.tvwOverlay.Nodes.Count = 0 Then Exit Sub
On Error Resume Next
        Dim i As Byte
        With Frm_Map
                For i = 1 To .tvwOverlay.Nodes.Count
                        .tvwOverlay.Nodes(i).Bold = False
                        .tvwOverlay.Nodes(i).ForeColor = &H0&
                Next
                 .tvwOverlay.SelectedItem.Bold = True
                 .tvwOverlay.Nodes(IndexNode).ForeColor = &HFF&
                .Sis1.SetInt SIS_OT_DATASET, .Sis1.GetInt(SIS_OT_OVERLAY, IndexNode - 1, "_nDataset&"), "_bOwned&", True
                .Sis1.SetInt SIS_OT_WINDOW, 0, "_nDefaultOverlay&", IndexNode - 1
                Call mnuEditable_Click
        End With
End Sub

Private Sub mnuReduce_Click()
        Dim i As Byte
        For i = 1 To Frm_Map.tvwTheme.Nodes.Count
                Frm_Map.tvwTheme.Nodes(i).Expanded = False
        Next
End Sub

Private Sub mnuRenameOverlay_Click()
        Frm_Map.tvwOverlay.StartLabelEdit
End Sub

Private Sub mnuReTheme_Click()
        Frm_Map.tvwTheme.StartLabelEdit
End Sub

Private Sub mnuSetOwner_Click()
With Frm_LandUse.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
                            .ListItems.Item(i).SubItems(7) = "0"
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
        End If
End With
End Sub

Private Sub mnuSetOwner2_Click()
        With Frm_Building.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
                            .ListItems.Item(i).SubItems(7) = "0"
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
        End If
End With
End Sub

Private Sub mnuSetOwner3_Click()
        With Frm_SignBord.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
                            .ListItems.Item(i).SubItems(7) = "0"
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
        End If
End With
End Sub

Private Sub mnuSetOwnerMail_Click()
With Frm_LandUse.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(8) = ""
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
        End If
End With
End Sub

Private Sub mnuSetOwnerMail2_Click()
        With Frm_Building.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(8) = ""
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
        End If
End With
End Sub

Private Sub mnuSetOwnerMail3_Click()
        With Frm_SignBord.LstV_OwnerShip
          If .ListItems.Count > 0 Then
                    Dim i As Byte
                    For i = 1 To .ListItems.Count
                            .ListItems.Item(i).SubItems(8) = ""
                    Next i
                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
        End If
End With
End Sub

Private Sub mnuVisible_Click()
        With Frm_Map
                Call .CLEAR_CHECK
                mnuVisible.Checked = True
                .tvwOverlay.Nodes(IndexNode).Checked = True
                .tvwOverlay.Nodes(IndexNode).Image = 1
                .tvwTheme.Nodes.Item(IndexNode).Checked = True
                .tvwTheme.Nodes.Item(IndexNode).Image = 1
                .Sis1.SetInt SIS_OT_OVERLAY, CLng(IndexNode - 1), "_status&", SIS_VISIBLE
        End With
End Sub

Private Sub Pic_Home_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
     Pic_Home.Top = 0
Call DETERMINE_FORM(Clone_Form.Name, False)
       Call Lb_menuLeft_Click(9)
        Pic_LfBar(9).Height = 555
        Picture1.Visible = True
End Sub

Private Sub Pic_LfBar_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
    Shape1(Index).Visible = False
End Sub

Private Sub Pic_Lock_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
     Pic_Lock.Top = Pic_Lock.Top + 35
End Sub

Private Sub Pic_Lock_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Pic_Lock.Picture = LoadResPicture(106, 0)
        Pic_Lock.Top = 0
        LB_Menu.Visible = False
        Frm_Login.Show vbModal
End Sub

Private Sub Pic_map_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
   Pic_Map.Top = Pic_Map.Top + 40
End Sub

Private Sub Pic_map_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
If TProduct = "STANDARD" Then
    MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
      LB_Menu.Visible = False
      Pic_Map.Top = 0
    Exit Sub
End If
If TProduct = "DEMO" And TCampaign = "TMIS" Then
     MsgBox "GIS Map Link Not Active on Microtax MIS  DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
      LB_Menu.Visible = False
      Pic_Map.Top = 0
      Exit Sub
End If
Call DETERMINE_FORM(Clone_Form.Name, False)
      Picture1.Visible = False
      LB_Menu.Visible = False
      Pic_Map.Top = 0
      Frm_Map.ZOrder
      Frm_Map.Show
End Sub

Private Sub Picture10_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
   Picture10.Top = Picture10.Top + 40
End Sub

Private Sub Picture10_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture10) Then
       Picture10.Picture = LoadResPicture(116, 0)
       LB_Menu.Visible = False
       Picture10.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture10.Picture = LoadResPicture(117, 0)
      LB_Menu.Caption = "�鹢�����"
      LB_Menu.Left = 3090
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture10_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
    Picture1.Visible = True
    LB_Menu.Visible = False
    Picture10.Top = 0
    With Frm_Search
            .ZOrder
            .Show
            .Frame1.Visible = False
            .SSTab1.Tab = 0
            .Shp_Menu(1).BackColor = &HB8CFD6
            .optCode.Visible = False
            .optName.Visible = False
            .Shp_Menu(2).BackColor = &HD6D6D6
            .Shp_Menu(3).BackColor = &HD6D6D6
            .Shp_Menu(4).BackColor = &HD6D6D6
            .Shp_Menu(5).BackColor = &HD6D6D6
            .Grid_VILLAGE.Rows = 2
            .Grid_VILLAGE.Clear
            .Lb_Name.Caption = ""
            .Grid_VILLAGE.ColWidth(0) = 0
            .Grid_VILLAGE.TextArray(1) = "��¡�ê���"
            If .optCode.Value = True Then
                    .Grid_VILLAGE.TextArray(1) = .optCode.Caption
            End If
            .Grid_VILLAGE.ColWidth(1) = 2550
            .Grid_VILLAGE.ColAlignmentFixed(1) = 4
    End With
End Sub

Private Sub Picture100_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Picture100.Top = Picture100.Top + 40
End Sub

Private Sub Picture100_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture100) Then
       Picture100.Picture = LoadResPicture(101, 0)
       LB_Menu.Visible = False
       Picture100.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture100.Picture = LoadResPicture(102, 0)
      LB_Menu.Caption = "��Ң�����"
      LB_Menu.Left = 3820
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture100_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture100.Picture = LoadResPicture(101, 0)
        Picture100.Top = 0
        LB_Menu.Visible = False
        Frm_Search.ZOrder
        Frm_Search.Show
        Frm_Search.SSTab1.Tab = 6
End Sub

Private Sub Picture11_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Picture11.Top = Picture11.Top + 40
End Sub

Private Sub Picture11_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture11) Then
       Picture11.Picture = LoadResPicture(119, 0)
       LB_Menu.Visible = False
       Picture11.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture11.Picture = LoadResPicture(118, 0)
      LB_Menu.Caption = "��駤��Ἱ���"
      LB_Menu.Left = 4470
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture11_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
If TProduct = "STANDARD" Then
    MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
    Exit Sub
End If
If TProduct = "DEMO" And TCampaign = "TMIS" Then
    MsgBox "GIS Map Link Not Active on Microtax MIS DEMO Version !", vbMsgBoxHelpButton, "Microtax Software"
    Picture11.Top = 0
    Exit Sub
End If
On Error GoTo ErrHld
        Picture11.Picture = LoadResPicture(119, 0)
        Picture11.Top = 0
        LB_Menu.Visible = False
        Call Pic_Home_MouseUp(1, 1, 1, 1)
        Frm_ConfigMap.Show vbModal
        
Exit Sub
ErrHld:
MsgBox "�ʴ�����������캹��鹷��ͧἹ�����ҹ�� !", vbExclamation, "����͹"
End Sub

Private Sub Picture111_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture111.Top = Picture111.Top + 40
End Sub

Private Sub Picture111_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture111) Then
       Picture111.Picture = LoadResPicture(122, 0)
       LB_Menu.Visible = False
       Picture111.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture111.Picture = LoadResPicture(123, 0)
      LB_Menu.Caption = "��駤���к��������"
      LB_Menu.Left = 5120
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture111_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture111.Picture = LoadResPicture(122, 0)
        Picture111.Top = 0
        LB_Menu.Visible = False
        Call Pic_Home_MouseUp(1, 1, 1, 1)
        Frm_Config.Show vbModal
End Sub

Private Sub Picture12_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture12.Top = Picture12.Top + 40
End Sub

Private Sub Picture12_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture12) Then
       Picture12.Picture = LoadResPicture(124, 0)
       LB_Menu.Visible = False
       Picture12.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture12.Picture = LoadResPicture(125, 0)
      LB_Menu.Caption = "���ͧ������"
      LB_Menu.Left = 5970
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture12_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture12.Picture = LoadResPicture(124, 0)
        Picture12.Top = 0
        LB_Menu.Visible = False
        Frm_Backup.Show vbModal
End Sub

Private Sub Picture14_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture14.Top = Picture14.Top + 40
End Sub

Private Sub Picture14_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture14) Then
       Picture14.Picture = LoadResPicture(126, 0) '122
       LB_Menu.Visible = False
       Picture14.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture14.Picture = LoadResPicture(127, 0) '123
      LB_Menu.Caption = "��Ǫ��������"
      LB_Menu.Left = 6800
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture14_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        LB_Menu.Visible = False
        Picture14.Picture = LoadResPicture(126, 0)
        Picture14.Top = 0
        App.HelpFile = App.Path & "\Help\MICROTAX FOR HELP.HLP"
        WinHelp hWnd, App.HelpFile, HELP_CONTENTS, ByVal 0&
End Sub

Private Sub Picture15_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture15.Top = Picture15.Top + 40
End Sub

Private Sub Picture15_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture15) Then
       Picture15.Picture = LoadResPicture(120, 0)
       LB_Menu.Visible = False
       Picture15.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture15.Picture = LoadResPicture(121, 0)
      LB_Menu.Caption = "������Ἱ�������"
      LB_Menu.Left = 7500
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture15_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture15.Picture = LoadResPicture(120, 0)
        Picture15.Top = 0
        LB_Menu.Visible = False
End Sub

Private Sub Picture18_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture18.Top = Picture18.Top + 40
End Sub

Private Sub Picture18_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture18) Then
       Picture18.Picture = LoadResPicture(130, 0)
       LB_Menu.Visible = False
       Picture18.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture18.Picture = LoadResPicture(131, 0)
      LB_Menu.Caption = "����Ἱ���"
      LB_Menu.Left = 8235
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture18_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture18.Picture = LoadResPicture(130, 0)
        Picture18.Top = 0
        LB_Menu.Visible = False
On Error GoTo Nextstep
            If TProduct = "STANDARD" Then
                MsgBox "GIS Map Link Not Active on Microtax Standard Version !", vbMsgBoxHelpButton, "Microtax Software"
                  LB_Menu.Visible = False
                  Picture18.Top = 0
                Exit Sub
            End If
        If Clone_Form.ActiveForm.Name = "Frm_Map" Then
             With Frm_Map
              If .Sis1.Left > 0 Then
                .Sis1.Left = 0
                If TProduct = "VIEWER" Then
                    .Sis1.Width = 15330
                    .Tb_Select.Left = 3930
                Else
                 .Sis1.Width = 14880
                .Tb_Mesure.Left = 3930
                .Tb_Select.Left = 6240
                .Tb_Order.Left = 9090
                End If
                  .Tb_View.Left = 30
             Else
                .Sis1.Left = 2925
                 If TProduct = "VIEWER" Then
                                        .Sis1.Width = 12400
                                        .Tb_Select.Left = 6830
                Else
                                 .Sis1.Width = 11940
                                .Tb_Mesure.Left = 6840
                                .Tb_Select.Left = 9150
                                .Tb_Order.Left = 12000
                End If
                                .Tb_View.Left = 2940
             End If
          End With
       Else
          MsgBox "Please Open Page Map Before ?", vbInformation, "Microtax Software"
        End If

Exit Sub
Nextstep:
            MsgBox "Please Open Page Map Before ?", vbInformation, "Microtax Software"
End Sub

Private Sub Picture19_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture19.Top = Picture19.Top + 40
End Sub

Private Sub Picture19_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture19) Then
       Picture19.Picture = LoadResPicture(128, 0)
       LB_Menu.Visible = False
       Picture19.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Picture19.Picture = LoadResPicture(129, 0)
      LB_Menu.Caption = "�͡�����"
      LB_Menu.Left = 8920
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture19_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Picture19.Picture = LoadResPicture(128, 0)
        Picture19.Top = 0
        LB_Menu.Visible = False
If MsgBox("Are you want to sure exit program?", vbInformation + vbYesNo, "Information") = vbNo Then Exit Sub
        Unload Me
        End
End Sub

Private Sub Pic_Home_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
     Pic_Home.Top = Pic_Home.Top + 40
End Sub

Private Sub Picture21_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
 If ReleaseObject(X, y, Picture21) Then
       Picture21.Picture = LoadResPicture(101, 0)
 Else
      Picture21.Picture = LoadResPicture(102, 0)
 End If
End Sub

Private Sub Pic_Home_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
 If ReleaseObject(X, y, Pic_Home) Then
     Pic_Home.Picture = LoadResPicture(114, 0)
      LB_Menu.Visible = False
      Pic_Home.Top = 0
 Else
     If LB_Menu.Visible = True Then Exit Sub
      Pic_Home.Picture = LoadResPicture(115, 0)
      LB_Menu.Caption = "�Դ�����"
      LB_Menu.Left = 825
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture6_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Picture6.Top = Picture6.Top + 40
End Sub

Private Sub Picture6_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If ReleaseObject(X, y, Picture6) Then
       Picture6.Picture = LoadResPicture(108, 0)
       LB_Menu.Visible = False
       Picture6.Top = 0
 Else
       LB_Menu.Left = 2000
      If LB_Menu.Visible = True Then Exit Sub
      Picture6.Picture = LoadResPicture(109, 0)
      LB_Menu.Caption = "����ѵԡ�ê�������"
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Pic_Lock_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
 If ReleaseObject(X, y, Pic_Lock) Then
      Pic_Lock.Picture = LoadResPicture(106, 0)
      LB_Menu.Visible = False
      Pic_Lock.Top = 0
 Else
      If LB_Menu.Visible = True Then Exit Sub
      Pic_Lock.Picture = LoadResPicture(107, 0)
      LB_Menu.Caption = "��ͤ�����ҹ"
      LB_Menu.Left = 75
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Pic_map_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
 If ReleaseObject(X, y, Pic_Map) Then
        Pic_Map.Picture = LoadResPicture(103, 0)
        LB_Menu.Visible = False
        Pic_Map.Top = 0
 Else
      LB_Menu.Left = 1660
     If LB_Menu.Visible = True Then Exit Sub
      Pic_Map.Picture = LoadResPicture(104, 0)

      LB_Menu.Caption = "Ἱ���"
      LB_Menu.Visible = True
 End If
End Sub

Private Sub Picture6_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
Picture6.Top = 0
Frm_StoryPayment.ZOrder
Frm_StoryPayment.Show
End Sub


