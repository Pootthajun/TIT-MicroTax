Attribute VB_Name = "mMessageLog"
'
' Brought to you by Brad Martinez
'   http://members.aol.com/btmtz/vb
'   http://www.mvps.org/ccrp
'
' - Code was developed using (and is formatted for) 8pt. MS Sans Serif font
'
' ======================================================
#If MSGLOG Then   ' >>>>>>> extends the entire mod <<<<<<<<<
' ======================================================
'
Option Explicit
'
' In the Project Properties dialog, Make tab, Conditional Compilation Arguments textbox add:
'   "MSGLOG = 1" for WriteMsgLog proc output to just the Immediate window.
'   "MSGLOG = 2" for WriteMsgLog proc output to both the Immediate window and to file.

Private Const WM_USER = &H400
Private colIgnoreMsgs As New Collection
'

Public Sub AddIngoreMsg(uMsg As Long)
  On Error GoTo Out
  colIgnoreMsgs.Add uMsg, CStr(uMsg)
Out:
End Sub

Public Sub RemoveIngoreMsg(uMsg As Long)
  On Error GoTo Out
  colIgnoreMsgs.Remove CStr(uMsg)
Out:
End Sub

Public Function IsIngoreMsg(uMsg As Long) As Boolean
  Dim i As Integer
  On Error GoTo Out
  i = colIgnoreMsgs(CStr(uMsg))
  IsIngoreMsg = True
Out:
End Function

' For general message logging to the Immediate window and to file.

Public Sub WriteMsgLog(sTxt As String, Optional fTimeStamp = False)
#If MSGLOG = 1 Then
  Debug.Print sTxt
#If MSGLOG = 2 Then
  Dim dwFF As Long
  dwFF = FreeFile
  Open "_DbgLog.txt" For Append As dwFF
  If fTimeStamp Then Print #dwFF, Date; Time
  If Len(sTxt) Then Print #dwFF, sTxt ' & vbCrLf
  Close dwFF
#End If   ' MSGLOG = 3
#End If   ' MSGLOG = 2
End Sub

' Returns a common control's generic notification code string from it's value.
' (constants are defined in Comctl32_472.bas)

Public Function GetCtrlNotifyCodeStr(code As Long) As String
  Dim sCode As String
  Select Case code
    Case NM_OUTOFMEMORY: sCode = "NM_OUTOFMEMORY"
    Case NM_CLICK: sCode = "NM_CLICK"
    Case NM_DBLCLK: sCode = "NM_DBLCLK"
    Case NM_RETURN: sCode = "NM_RETURN"
    Case NM_RCLICK: sCode = "NM_RCLICK"
    Case NM_RDBLCLK: sCode = "NM_RDBLCLK"
    Case NM_SETFOCUS: sCode = "NM_SETFOCUS"
    Case NM_KILLFOCUS: sCode = "NM_KILLFOCUS"
#If (WIN32_IE >= &H300) Then
    Case NM_CUSTOMDRAW: sCode = "NM_CUSTOMDRAW"
    Case NM_HOVER: sCode = "NM_HOVER"
#End If
#If (WIN32_IE >= &H400) Then
    Case NM_NCHITTEST: sCode = "NM_NCHITTEST"
    Case NM_KEYDOWN: sCode = "NM_KEYDOWN"
    Case NM_RELEASEDCAPTURE: sCode = "NM_RELEASEDCAPTURE"
    Case NM_SETCURSOR: sCode = "NM_SETCURSOR"
    Case NM_CHAR: sCode = "NM_CHAR"
#End If
    Case Else: sCode = GetTVNotifyCodeStr(code)
  
  End Select
  GetCtrlNotifyCodeStr = sCode

End Function

' Returns a treeview control's notification code string from it's value

Public Function GetTVNotifyCodeStr(code As Long) As String
  Dim sCode As String
  Select Case code
    Case TVN_SELCHANGING: sCode = "TVN_SELCHANGING"
    Case TVN_SELCHANGED: sCode = "TVN_SELCHANGED"
    Case TVN_GETDISPINFO: sCode = "TVN_GETDISPINFO"
    Case TVN_SETDISPINFO: sCode = "TVN_SETDISPINFO"
    Case TVN_ITEMEXPANDING: sCode = "TVN_ITEMEXPANDING"
    Case TVN_ITEMEXPANDED: sCode = "TVN_ITEMEXPANDED"
    Case TVN_BEGINDRAG: sCode = "TVN_BEGINDRAG"
    Case TVN_BEGINRDRAG: sCode = "TVN_BEGINRDRAG"
    Case TVN_DELETEITEM: sCode = "TVN_DELETEITEM"
    Case TVN_BEGINLABELEDIT: sCode = "TVN_BEGINLABELEDIT"
    Case TVN_ENDLABELEDIT: sCode = "TVN_ENDLABELEDIT"
    Case TVN_KEYDOWN: sCode = "TVN_KEYDOWN"
#If (WIN32_IE >= &H400) Then
    Case TVN_GETINFOTIP: sCode = "TVN_GETINFOTIP"
    Case TVN_SINGLEEXPAND: sCode = "TVN_SINGLEEXPAND"
#End If   ' 0x400
    
    Case Else: sCode = "&H" & Hex(code) & "(" & code & "&)" & " (unknown notification)"
  
  End Select
  GetTVNotifyCodeStr = sCode

End Function

' Returns a window's generic message string from it's value.  196 msgs...

Public Function GetWinMsgStr(uMsg As Long) As String
  Dim sMsg As String
  Select Case uMsg
    Case &H0: sMsg = "WM_NULL"
    Case &H1: sMsg = "WM_CREATE"
    Case &H2: sMsg = "WM_DESTROY"
    Case &H3: sMsg = "WM_MOVE"
    Case &H5: sMsg = "WM_SIZE"
    Case &H6: sMsg = "WM_ACTIVATE"
    Case &H7: sMsg = "WM_SETFOCUS"
    Case &H8: sMsg = "WM_KILLFOCUS"
    Case &HA: sMsg = "WM_ENABLE"
    Case &HB: sMsg = "WM_SETREDRAW"
    Case &HC: sMsg = "WM_SETTEXT"
    Case &HD: sMsg = "WM_GETTEXT"
    Case &HE: sMsg = "WM_GETTEXTLENGTH"
    Case &HF: sMsg = "WM_PAINT"
    Case &H10: sMsg = "WM_CLOSE"
    Case &H11: sMsg = "WM_QUERYENDSESSION"
    Case &H12: sMsg = "WM_QUIT"
    Case &H13: sMsg = "WM_QUERYOPEN"
    Case &H14: sMsg = "WM_ERASEBKGND"
    Case &H15: sMsg = "WM_SYSCOLORCHANGE"
    Case &H16: sMsg = "WM_ENDSESSION"
    Case &H18: sMsg = "WM_SHOWWINDOW"
    Case &H19: sMsg = "WM_CTLCOLOR"
    Case &H1A: sMsg = "WM_SETTINGCHANGE"
'    Case &H1A: sMsg = "WM_WININICHANGE"
    Case &H1B: sMsg = "WM_DEVMODECHANGE"
    Case &H1C: sMsg = "WM_ACTIVATEAPP"
    Case &H1D: sMsg = "WM_FONTCHANGE"
    Case &H1E: sMsg = "WM_TIMECHANGE"
    Case &H1F: sMsg = "WM_CANCELMODE"
    Case &H20: sMsg = "WM_SETCURSOR"
    Case &H21: sMsg = "WM_MOUSEACTIVATE"
    Case &H22: sMsg = "WM_CHILDACTIVATE"
    Case &H23: sMsg = "WM_QUEUESYNC"
    Case &H24: sMsg = "WM_GETMINMAXINFO"
    Case &H26: sMsg = "WM_PAINTICON"
    Case &H27: sMsg = "WM_ICONERASEBKGND"
    Case &H28: sMsg = "WM_NEXTDLGCTL"
    Case &H2A: sMsg = "WM_SPOOLERSTATUS"
    Case &H2B: sMsg = "WM_DRAWITEM"
    Case &H2C: sMsg = "WM_MEASUREITEM"
    Case &H2D: sMsg = "WM_DELETEITEM"
    Case &H2E: sMsg = "WM_VKEYTOITEM"
    Case &H2F: sMsg = "WM_CHARTOITEM"
    Case &H30: sMsg = "WM_SETFONT"
    Case &H31: sMsg = "WM_GETFONT"
    Case &H32: sMsg = "WM_SETHOTKEY"
    Case &H33: sMsg = "WM_GETHOTKEY"
    Case &H37: sMsg = "WM_QUERYDRAGICON"
    Case &H39: sMsg = "WM_COMPAREITEM"
    Case &H41: sMsg = "WM_COMPACTING"
    Case &H44: sMsg = "WM_COMMNOTIFY"
    Case &H46: sMsg = "WM_WINDOWPOSCHANGING"
    Case &H47: sMsg = "WM_WINDOWPOSCHANGED"
    Case &H48: sMsg = "WM_POWER"
    Case &H4A: sMsg = "WM_COPYDATA"
    Case &H4B: sMsg = "WM_CANCELJOURNAL"
    Case &H4E: sMsg = "WM_NOTIFY"
    Case &H50: sMsg = "WM_INPUTLANGCHANGEREQUEST"
    Case &H51: sMsg = "WM_INPUTLANGCHANGE"
    Case &H52: sMsg = "WM_TCARD"
    Case &H53: sMsg = "WM_HELP"
    Case &H54: sMsg = "WM_USERCHANGED"
    Case &H55: sMsg = "WM_NOTIFYFORMAT"
    Case &H7B: sMsg = "WM_CONTEXTMENU"
    Case &H7C: sMsg = "WM_STYLECHANGING"
    Case &H7D: sMsg = "WM_STYLECHANGED"
    Case &H7E: sMsg = "WM_DISPLAYCHANGE"
    Case &H7F: sMsg = "WM_GETICON"
    Case &H80: sMsg = "WM_SETICON"
    Case &H81: sMsg = "WM_NCCREATE"
    Case &H82: sMsg = "WM_NCDESTROY"
    Case &H83: sMsg = "WM_NCCALCSIZE"
    Case &H84: sMsg = "WM_NCHITTEST"
    Case &H85: sMsg = "WM_NCPAINT"
    Case &H86: sMsg = "WM_NCACTIVATE"
    Case &H87: sMsg = "WM_GETDLGCODE"
    Case &HA0: sMsg = "WM_NCMOUSEMOVE"
    Case &HA1: sMsg = "WM_NCLBUTTONDOWN"
    Case &HA2: sMsg = "WM_NCLBUTTONUP"
    Case &HA3: sMsg = "WM_NCLBUTTONDBLCLK"
    Case &HA4: sMsg = "WM_NCRBUTTONDOWN"
    Case &HA5: sMsg = "WM_NCRBUTTONUP"
    Case &HA6: sMsg = "WM_NCRBUTTONDBLCLK"
    Case &HA7: sMsg = "WM_NCMBUTTONDOWN"
    Case &HA8: sMsg = "WM_NCMBUTTONUP"
    Case &HA9: sMsg = "WM_NCMBUTTONDBLCLK"
    Case &H100: sMsg = "WM_KEYDOWN"
'    Case &H100: sMsg = "WM_KEYFIRST"
    Case &H101: sMsg = "WM_KEYUP"
    Case &H102: sMsg = "WM_CHAR"
    Case &H103: sMsg = "WM_DEADCHAR"
    Case &H104: sMsg = "WM_SYSKEYDOWN"
    Case &H105: sMsg = "WM_SYSKEYUP"
    Case &H106: sMsg = "WM_SYSCHAR"
    Case &H107: sMsg = "WM_SYSDEADCHAR"
    Case &H108: sMsg = "WM_KEYLAST"
'    Case &H109: sMsg = "WM_WNT_CONVERTREQUESTEX"
    Case &H109: sMsg = "WM_CONVERTREQUESTEX"
    Case &H10A: sMsg = "WM_CONVERTREQUEST"
    Case &H10B: sMsg = "WM_CONVERTRESULT"
    Case &H10C: sMsg = "WM_INTERIM"
    Case &H10D: sMsg = "WM_IME_STARTCOMPOSITION"
    Case &H10E: sMsg = "WM_IME_ENDCOMPOSITION"
    Case &H10F: sMsg = "WM_IME_COMPOSITION"
'    Case &H10F: sMsg = "WM_IME_KEYLAST"
    Case &H110: sMsg = "WM_INITDIALOG"
    Case &H111: sMsg = "WM_COMMAND"
    Case &H112: sMsg = "WM_SYSCOMMAND"
    Case &H113: sMsg = "WM_TIMER"
    Case &H114: sMsg = "WM_HSCROLL"
    Case &H115: sMsg = "WM_VSCROLL"
    Case &H116: sMsg = "WM_INITMENU"
    Case &H117: sMsg = "WM_INITMENUPOPUP"
    Case &H11F: sMsg = "WM_MENUSELECT"
    Case &H120: sMsg = "WM_MENUCHAR"
    Case &H121: sMsg = "WM_ENTERIDLE"
    Case &H132: sMsg = "WM_CTLCOLORMSGBOX"
    Case &H133: sMsg = "WM_CTLCOLOREDIT"
    Case &H134: sMsg = "WM_CTLCOLORLISTBOX"
    Case &H135: sMsg = "WM_CTLCOLORBTN"
    Case &H136: sMsg = "WM_CTLCOLORDLG"
    Case &H137: sMsg = "WM_CTLCOLORSCROLLBAR"
    Case &H138: sMsg = "WM_CTLCOLORSTATIC"
'    Case &H200: sMsg = "WM_MOUSEFIRST"
    Case &H200: sMsg = "WM_MOUSEMOVE"
    Case &H201: sMsg = "WM_LBUTTONDOWN"
    Case &H202: sMsg = "WM_LBUTTONUP"
    Case &H203: sMsg = "WM_LBUTTONDBLCLK"
    Case &H204: sMsg = "WM_RBUTTONDOWN"
    Case &H205: sMsg = "WM_RBUTTONUP"
    Case &H206: sMsg = "WM_RBUTTONDBLCLK"
    Case &H207: sMsg = "WM_MBUTTONDOWN"
    Case &H208: sMsg = "WM_MBUTTONUP"
    Case &H209: sMsg = "WM_MBUTTONDBLCLK"
'    Case &H20A: sMsg = "WM_MOUSELAST"        ' >= NT4
    Case &H20A: sMsg = "WM_MOUSEWHEEL"    ' >= NT4
    Case &H210: sMsg = "WM_PARENTNOTIFY"
    Case &H211: sMsg = "WM_ENTERMENULOOP"
    Case &H212: sMsg = "WM_EXITMENULOOP"
    Case &H213: sMsg = "WM_NEXTMENU"
    Case &H214: sMsg = "WM_SIZING"
    Case &H215: sMsg = "WM_CAPTURECHANGED"
    Case &H216: sMsg = "WM_MOVING"
    Case &H218: sMsg = "WM_POWERBROADCAST"
    Case &H219: sMsg = "WM_DEVICECHANGE"
    Case &H220: sMsg = "WM_MDICREATE"
    Case &H221: sMsg = "WM_MDIDESTROY"
    Case &H222: sMsg = "WM_MDIACTIVATE"
    Case &H223: sMsg = "WM_MDIRESTORE"
    Case &H224: sMsg = "WM_MDINEXT"
    Case &H225: sMsg = "WM_MDIMAXIMIZE"
    Case &H226: sMsg = "WM_MDITILE"
    Case &H227: sMsg = "WM_MDICASCADE"
    Case &H228: sMsg = "WM_MDIICONARRANGE"
    Case &H229: sMsg = "WM_MDIGETACTIVE"
    Case &H230: sMsg = "WM_MDISETMENU"
    Case &H231: sMsg = "WM_ENTERSIZEMOVE"
    Case &H232: sMsg = "WM_EXITSIZEMOVE"
    Case &H233: sMsg = "WM_DROPFILES"
    Case &H234: sMsg = "WM_MDIREFRESHMENU"
    Case &H280: sMsg = "WM_IME_REPORT"
    Case &H281: sMsg = "WM_IME_SETCONTEXT"
    Case &H282: sMsg = "WM_IME_NOTIFY"
    Case &H283: sMsg = "WM_IME_CONTROL"
    Case &H284: sMsg = "WM_IME_COMPOSITIONFULL"
    Case &H285: sMsg = "WM_IME_SELECT"
    Case &H286: sMsg = "WM_IME_CHAR"
    Case &H290: sMsg = "WM_IME_KEYDOWN"
    Case &H290: sMsg = "WM_IMEKEYDOWN"
    Case &H291: sMsg = "WM_IME_KEYUP"
    Case &H291: sMsg = "WM_IMEKEYUP"
    Case &H2A1: sMsg = "WM_MOUSEHOVER"    ' >= NT4
    Case &H2A3: sMsg = "WM_MOUSELEAVE"      ' >= NT4
    Case &H300: sMsg = "WM_CUT"
    Case &H301: sMsg = "WM_COPY"
    Case &H302: sMsg = "WM_PASTE"
    Case &H303: sMsg = "WM_CLEAR"
    Case &H304: sMsg = "WM_UNDO"
    Case &H305: sMsg = "WM_RENDERFORMAT"
    Case &H306: sMsg = "WM_RENDERALLFORMATS"
    Case &H307: sMsg = "WM_DESTROYCLIPBOARD"
    Case &H308: sMsg = "WM_DRAWCLIPBOARD"
    Case &H309: sMsg = "WM_PAINTCLIPBOARD"
    Case &H30A: sMsg = "WM_VSCROLLCLIPBOARD"
    Case &H30B: sMsg = "WM_SIZECLIPBOARD"
    Case &H30C: sMsg = "WM_ASKCBFORMATNAME"
    Case &H30D: sMsg = "WM_CHANGECBCHAIN"
    Case &H30E: sMsg = "WM_HSCROLLCLIPBOARD"
    Case &H30F: sMsg = "WM_QUERYNEWPALETTE"
    Case &H310: sMsg = "WM_PALETTEISCHANGING"
    Case &H311: sMsg = "WM_PALETTECHANGED"
    Case &H312: sMsg = "WM_HOTKEY"
    Case &H317: sMsg = "WM_PRINT"
    Case &H318: sMsg = "WM_PRINTCLIENT"
    Case Is < WM_USER:             sMsg = "&H" & Hex(uMsg) & " (unknown WM_ msg)"
    Case WM_USER To &H7FFF: sMsg = "WM_USER + " & uMsg - WM_USER & " (control specific msg)"
    Case &H8000& To &HBFFF&:   sMsg = "&H" & Hex(uMsg) & " (reserved msg)"
    Case &HC000& To &HFFFF&:   sMsg = "&H" & Hex(uMsg) & " (registered msg)"
    Case Else:                                sMsg = "&H" & Hex(uMsg) & " (reserved msg)"
  
  End Select
  GetWinMsgStr = sMsg

End Function

' ======================================================
#End If  ' MSGLOG     >>>>>>> extends the entire mod <<<<<<<<<
' ======================================================
