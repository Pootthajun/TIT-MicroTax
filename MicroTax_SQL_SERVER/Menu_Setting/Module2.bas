Attribute VB_Name = "mTreeview"
Option Explicit

' Brought to you by Brad Martinez
'   http://members.aol.com/btmtz/vb
'   http://www.mvps.org/ccrp
'
' Code was written in and formatted for 8pt MS San Serif

' ====================================================================

Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
                            (ByVal hWnd As Long, _
                            ByVal wMsg As Long, _
                            wParam As Any, _
                            lParam As Any) As Long   ' <---

Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal dwLength As Long)

' The two values of C's BOOL data type (it's unsigned)
Public Enum CBoolean
  CTrue = 1
  CFalse = 0
End Enum

Public Const WM_NOTIFY = &H4E

' The NMHDR structure contains information about a notification message. The pointer
' to this structure is specified as the lParam member of the WM_NOTIFY message.
Public Type NMHDR
    hwndFrom As Long   ' Window handle of control sending message
    idFrom As Long        ' Identifier of control sending message
    code  As Long          ' Specifies the notification code
End Type

Public Type POINTAPI   ' pt
    x As Long
    y As Long
End Type

' Generic common control WM_NOTIFY notification messages.
' The lParam of all messages points to the NMHDR struct unless noted.
Public Enum CCNotifications
  NM_FIRST = -0&              ' (0U-  0U)
  NM_LAST = -99&              ' (0U- 99U)
  NM_OUTOFMEMORY = (NM_FIRST - 1)
  NM_CLICK = (NM_FIRST - 2)
  NM_DBLCLK = (NM_FIRST - 3)
  NM_RETURN = (NM_FIRST - 4)
  NM_RCLICK = (NM_FIRST - 5)
  NM_RDBLCLK = (NM_FIRST - 6)
  NM_SETFOCUS = (NM_FIRST - 7)
  NM_KILLFOCUS = (NM_FIRST - 8)
#If (WIN32_IE >= &H300) Then
  NM_CUSTOMDRAW = (NM_FIRST - 12)
  NM_HOVER = (NM_FIRST - 13)
#End If   ' 300
#If (WIN32_IE >= &H400) Then
  NM_NCHITTEST = (NM_FIRST - 14)        ' lParam = NMMOUSE struct
  NM_KEYDOWN = (NM_FIRST - 15)        ' lParam =  NMKEY struct
  NM_RELEASEDCAPTURE = (NM_FIRST - 16)
  NM_SETCURSOR = (NM_FIRST - 17)     ' lParam =  NMMOUSE struct
  NM_CHAR = (NM_FIRST - 18)                ' lParam =  NMCHAR struct
#End If   '400
End Enum

' ====================================================================
' treeview definitions defined in Commctrl.h that is found in both the
' INetSDK and PlatformSDK at: http://msdn.microsoft.com/developer/sdk/

Public Type TVITEM   ' was TV_ITEM
    mask As Long 'TVITEM_mask
    hItem As Long
    state As Long 'TVITEM_state
    stateMask As Long
    ' If pszText was a string, it *must* be pre-allocated before
    ' it's filled indirectly when filling the NMTREEVIEW struct
    ' in Form1's NotificationHandler proc, or it's GPF big time!!!
    pszText As Long    ' String ' pointer,
    cchTextMax As Long
    iImage As Long
    iSelectedImage As Long
    cChildren As Long
    lParam As Long
End Type

' window messages
Public Const TV_FIRST = &H1100
Public Const TVM_GETNEXTITEM = (TV_FIRST + 10)

' TVM_GETNEXTITEM wParam values
Public Enum TVGN_Flags
    TVGN_ROOT = &H0
    TVGN_NEXT = &H1
    TVGN_PREVIOUS = &H2
    TVGN_PARENT = &H3
    TVGN_CHILD = &H4
    TVGN_FIRSTVISIBLE = &H5
    TVGN_NEXTVISIBLE = &H6
    TVGN_PREVIOUSVISIBLE = &H7
    TVGN_DROPHILITE = &H8
    TVGN_CARET = &H9
#If (WIN32_IE >= &H400) Then   ' >= Comctl32.dll v4.71
    TVGN_LASTVISIBLE = &HA
#End If
End Enum

' treeview notication messages
Public Enum TVNotifications
    TVN_FIRST = -400&   ' &HFFFFFE70   ' (0U-400U)
    TVN_LAST = -499&    ' &HFFFFFE0D    ' (0U-499U)
    
    #If UNICODE Then
      TVN_SELCHANGING = (TVN_FIRST - 50)
      TVN_SELCHANGED = (TVN_FIRST - 51)
      TVN_GETDISPINFO = (TVN_FIRST - 52)
      TVN_SETDISPINFO = (TVN_FIRST - 53)
      TVN_ITEMEXPANDING = (TVN_FIRST - 54)
      TVN_ITEMEXPANDED = (TVN_FIRST - 55)
      TVN_BEGINDRAG = (TVN_FIRST - 56)
      TVN_BEGINRDRAG = (TVN_FIRST - 57)
      TVN_DELETEITEM = (TVN_FIRST - 58)
      TVN_BEGINLABELEDIT = (TVN_FIRST - 59)
      TVN_ENDLABELEDIT = (TVN_FIRST - 60)
#If (WIN32_IE >= &H400) Then
      TVN_GETINFOTIPW = (TVN_FIRST - 14)
#End If   ' 0x400
    #Else                                                      ' lParam points to:
      TVN_SELCHANGING = (TVN_FIRST - 1)          ' NMTREEVIEW
      TVN_SELCHANGED = (TVN_FIRST - 2)           ' NMTREEVIEW
      TVN_GETDISPINFO = (TVN_FIRST - 3)            ' NMTVDISPINFO
      TVN_SETDISPINFO = (TVN_FIRST - 4)            ' NMTVDISPINFO
      TVN_ITEMEXPANDING = (TVN_FIRST - 5)       ' NMTREEVIEW
      TVN_ITEMEXPANDED = (TVN_FIRST - 6)        ' NMTREEVIEW
      TVN_BEGINDRAG = (TVN_FIRST - 7)              ' NMTREEVIEW
      TVN_BEGINRDRAG = (TVN_FIRST - 8)            ' NMTREEVIEW
      TVN_DELETEITEM = (TVN_FIRST - 9)             ' NMTREEVIEW
      TVN_BEGINLABELEDIT = (TVN_FIRST - 10)    ' NMTVDISPINFO
      TVN_ENDLABELEDIT = (TVN_FIRST - 11)       ' NMTVDISPINFO
#If (WIN32_IE >= &H400) Then
      TVN_GETINFOTIP = (TVN_FIRST - 13)
#End If   ' 0x400
    #End If   ' UNICODE
    TVN_KEYDOWN = (TVN_FIRST - 12)                ' NMTVKEYDOWN
#If (WIN32_IE >= &H400) Then
    TVN_SINGLEEXPAND = (TVN_FIRST - 15)
#End If   ' 0x400
End Enum   ' Notifications                      1st member of all structs is NMHDR

Public Type NMTREEVIEW   ' was NM_TREEVIEW
    hdr As NMHDR
    ' The 'action' member specifies a notification-specific action flag.
    ' Is NMTREEVIEW_action for TVN_SELCHANGING, TVN_SELCHANGED, TVN_SETDISPINFO
    ' Is TVM_EXPAND_wParam for TVN_ITEMEXPANDING, TVN_ITEMEXPANDED
    action As Long
    itemOld As TVITEM
    itemNew As TVITEM
    ptDrag As POINTAPI
End Type

' TVM_EXPAND wParam and NMTREEVIEW action param values.
Public Enum TVM_EXPAND_wParam
    TVE_COLLAPSE = &H1
    TVE_EXPAND = &H2
    TVE_TOGGLE = &H3
#If (WIN32_IE >= &H300) Then
    TVE_EXPANDPARTIAL = &H4000
#End If
    TVE_COLLAPSERESET = &H8000
End Enum

' for TVN_SELCHANGING, TVN_SELCHANGED, TVN_SETDISPINFO
Public Enum NMTREEVIEW_action
    TVC_UNKNOWN = &H0
    TVC_BYMOUSE = &H1
    TVC_BYKEYBOARD = &H2
End Enum

Public Type NMTVDISPINFO   ' was TV_DISPINFO
    hdr As NMHDR
    item As TVITEM
End Type

Public Type NMTVKEYDOWN   ' was TV_KEYDOWN
    hdr As NMHDR
    wVKey As Integer
    flags As Long   ' Always zero.
End Type
'

' If successful, returns the very first node in the specified treeview,
' returns Nothing otherwise.

' Equates to retrieving the hItem of the treeview's first item with
' TVM_GETNEXTITEM/TVGN_ROOT. (Treeview1.Nodes(1)
' or Treeview1.Nodes(1).Root do *not* necessarily return the
' treeview's very first node!)

Public Function GetFirstTVNode(objTV As TreeView) As Node
  Dim nod As Node
  On Error GoTo NoNodes
  
  ' Will err here if there are no treeview nodes
  Set nod = objTV.Nodes(1)
  
  ' Get the first node's highest parent node
  Do While (nod.Parent Is Nothing) = False
    Set nod = nod.Parent
  Loop
  
  ' Return the highest parent node's first sibling
  Set GetFirstTVNode = nod.FirstSibling
  
NoNodes:
End Function

' If successful, returns an object reference to a node object from
' the specified treeview item handle, returns Nothing otherwise.

Public Function GetNodeFromTVItem(objTV As TreeView, _
                                                            ByVal hItem As Long) As Node
  Dim hwndTV As Long
  Dim anSiblingPos() As Integer   ' contains the sibling position of the item and all it's parents
  Dim nLevel As Integer               ' hierarchical level of the item
  Dim nod As Node
  Dim i As Integer
  Dim nPos As Integer
  On Error GoTo Out
  
  hwndTV = objTV.hWnd
  
  ' Continually work backwards from the current item to the current item's
  ' first sibling, caching the current item's sibling position in the one-based
  ' array, Then get the first sibling's parent item and start over. Keep going
  ' until the position of the specified item's top level parent item is obtained.
  Do While hItem
    nLevel = nLevel + 1
    ReDim Preserve anSiblingPos(nLevel)
    anSiblingPos(nLevel) = GetTVItemSiblingPos(hwndTV, hItem)
    hItem = TreeView_GetParent(hwndTV, hItem)
  Loop
  
  ' Get the first treeview node.
  Set nod = GetFirstTVNode(objTV)
  If (nod Is Nothing) = False Then
  
    ' Now work backwards through the cached item positions in the array
    ' (from the first treeview item to the specified item), obtaining the respective
    ' object reference for each item at the cached position. When we get to the
    ' specified item's position (the value of the first element in the array), we
    ' got it's node...
    For i = nLevel To 1 Step -1
      nPos = anSiblingPos(i)
      
      Do While nPos > 1
        Set nod = nod.Next
        nPos = nPos - 1
      Loop
      
      If (i > 1) Then Set nod = nod.Child
    Next
    
    Set GetNodeFromTVItem = nod
  End If
  
Out:
End Function

' Returns the one-base position of the specified item
' with respect to it's sibling order.

Public Function GetTVItemSiblingPos(hwndTV As Long, _
                                                            ByVal hItem As Long) As Integer
  Dim nPos As Integer
  
  ' Keep counting up from one until the item has no more previous siblings
  Do While hItem
    nPos = nPos + 1
    hItem = TreeView_GetPrevSibling(hwndTV, hItem)
  Loop
  
  GetTVItemSiblingPos = nPos
  
End Function

' ===========================================================================
' Treeview macros defined in Commctrl.h

' TreeView_GetNextItem

' Retrieves the tree-view item that bears the specified relationship to a specified item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetNextItem(hWnd As Long, hItem As Long, flag As Long) As Long
  TreeView_GetNextItem = SendMessage(hWnd, TVM_GETNEXTITEM, ByVal flag, ByVal hItem)
End Function

' Retrieves the first child item. The hItem parameter must be NULL.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetChild(hWnd As Long, hItem As Long) As Long
  TreeView_GetChild = TreeView_GetNextItem(hWnd, hItem, TVGN_CHILD)
End Function

' Retrieves the next sibling item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetNextSibling(hWnd As Long, hItem As Long) As Long
  TreeView_GetNextSibling = TreeView_GetNextItem(hWnd, hItem, TVGN_NEXT)
End Function

' Retrieves the previous sibling item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetPrevSibling(hWnd As Long, hItem As Long) As Long
  TreeView_GetPrevSibling = TreeView_GetNextItem(hWnd, hItem, TVGN_PREVIOUS)
End Function

' Retrieves the parent of the specified item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetParent(hWnd As Long, hItem As Long) As Long
  TreeView_GetParent = TreeView_GetNextItem(hWnd, hItem, TVGN_PARENT)
End Function

' Retrieves the first visible item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetFirstVisible(hWnd As Long) As Long
  TreeView_GetFirstVisible = TreeView_GetNextItem(hWnd, 0, TVGN_FIRSTVISIBLE)
End Function

' Retrieves the next visible item that follows the specified item. The specified item must be visible.
' Use the TVM_GETITEMRECT message to determine whether an item is visible.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetNextVisible(hWnd As Long, hItem As Long) As Long
  TreeView_GetNextVisible = TreeView_GetNextItem(hWnd, hItem, TVGN_NEXTVISIBLE)
End Function

' Retrieves the first visible item that precedes the specified item. The specified item must be visible.
' Use the TVM_GETITEMRECT message to determine whether an item is visible.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetPrevVisible(hWnd As Long, hItem As Long) As Long
  TreeView_GetPrevVisible = TreeView_GetNextItem(hWnd, hItem, TVGN_PREVIOUSVISIBLE)
End Function

' Retrieves the currently selected item.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetSelection(hWnd As Long) As Long
  TreeView_GetSelection = TreeView_GetNextItem(hWnd, 0, TVGN_CARET)
End Function

' Retrieves the item that is the target of a drag-and-drop operation.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetDropHilight(hWnd As Long) As Long
  TreeView_GetDropHilight = TreeView_GetNextItem(hWnd, 0, TVGN_DROPHILITE)
End Function

' Retrieves the topmost or very first item of the tree-view control.
' Returns the handle to the item if successful or 0 otherwise.

Public Function TreeView_GetRoot(hWnd As Long) As Long
  TreeView_GetRoot = TreeView_GetNextItem(hWnd, 0, TVGN_ROOT)
End Function
