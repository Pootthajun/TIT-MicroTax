VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Print_Warn 
   BackColor       =   &H00B99D7F&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " ˹ѧ��������Ẻ"
   ClientHeight    =   5265
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6030
   Icon            =   "Frm_Print_Warn.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Print_Warn.frx":151A
   ScaleHeight     =   351
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   402
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtYear 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   4050
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   3090
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   4050
      TabIndex        =   14
      Top             =   2460
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   1320
      TabIndex        =   12
      Top             =   2460
      Width           =   1635
   End
   Begin VB.CommandButton Btn_Search 
      BackColor       =   &H00D6D6D6&
      Height          =   375
      Left            =   5220
      Picture         =   "Frm_Print_Warn.frx":23C3E
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   3720
      Width           =   525
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   3720
      Width           =   3825
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   4110
      TabIndex        =   1
      Top             =   780
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   72089601
      CurrentDate     =   37587
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1320
      TabIndex        =   0
      Top             =   3090
      Width           =   1635
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00D6D6D6&
      Height          =   435
      Left            =   2280
      Picture         =   "Frm_Print_Warn.frx":241C8
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4470
      Width           =   1605
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   405
      Left            =   5520
      TabIndex        =   17
      Top             =   3090
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   714
      _Version        =   393216
      Value           =   2547
      AutoBuddy       =   -1  'True
      BuddyControl    =   "txtYear"
      BuddyDispid     =   196609
      OrigLeft        =   116
      OrigTop         =   50
      OrigRight       =   133
      OrigBottom      =   77
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�͹حҵ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   8
      Left            =   3150
      TabIndex        =   15
      Top             =   2550
      Width           =   810
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   6
      Left            =   870
      TabIndex        =   13
      Top             =   2550
      Width           =   375
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� - ���ʡ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   7
      Left            =   330
      TabIndex        =   10
      Top             =   3810
      Width           =   915
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   360
      TabIndex        =   8
      Top             =   1860
      Width           =   5415
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ŧ���."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   360
      TabIndex        =   7
      Top             =   1560
      Width           =   510
   End
   Begin VB.Label lb_Menu 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ẻ�������§ҹ ˹ѧ��������Ẻ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   1590
      TabIndex        =   6
      Top             =   150
      Width           =   2895
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ����͡˹ѧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   2910
      TabIndex        =   5
      Top             =   900
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ (�.�.)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   3120
      TabIndex        =   4
      Top             =   3180
      Width           =   870
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ���˹ѧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   375
      TabIndex        =   3
      Top             =   3210
      Width           =   870
   End
End
Attribute VB_Name = "Frm_Print_Warn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Rs As ADODB.Recordset

Private Sub Create_View()
        Dim strSQL As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
'        Call SET_Execute("DROP VIEW VIEW_WARN_REPORT")
        strSQL = "ALTER VIEW [dbo].[VIEW_WARN_REPORT] AS" & _
                    " SELECT PRENAME + OWNER_NAME + '  ' + OWNER_SURNAME AS FULLNAME, OWNERSHIP_ID, CASE WHEN EXISTS" & _
                    " (SELECT OWNERSHIP_ID FROM dbo.PBT5 AS B WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS LAND_FIND, CASE WHEN EXISTS" & _
                    " (SELECT OWNERSHIP_ID FROM dbo.PRD2 AS C WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS BUILDING_FIND, CASE WHEN EXISTS" & _
                    " (SELECT OWNERSHIP_ID FROM dbo.PP1 AS D WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID)) THEN 'Y' ELSE 'N' END AS SIGNBOARD_FIND" & _
                    " FROM dbo.OWNERSHIP AS A" & _
                    " Where Exists (SELECT OWNERSHIP_ID FROM dbo.PBT5 AS B WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID) AND (PBT5_YEAR = " & txtYear.Text & ")) OR" & _
                    " Exists (SELECT OWNERSHIP_ID FROM dbo.PRD2 AS C WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID) AND (PRD2_YEAR = " & txtYear.Text & ")) OR" & _
                    " Exists (SELECT OWNERSHIP_ID FROM dbo.PP1 AS D WHERE (A.OWNERSHIP_ID = OWNERSHIP_ID) AND (PP1_YEAR = " & txtYear.Text & "))"
'        strSQL = "CREATE VIEW VIEW_WARN_REPORT AS " & _
'                " SELECT A.PRENAME & A.OWNER_NAME & '  ' & A.OWNER_SURNAME AS FULLNAME, A.OWNERSHIP_ID, IIF(Exists (SELECT *  FROM  PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID )=0,'N','Y') AS LAND_FIND, IIF(Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID )=0,'N','Y') AS BUILDING_FIND, IIF(Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID )=0,'N','Y') AS SIGNBOARD_FIND, IIF(Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID )=0,'N','Y') AS LICENSE_FIND " & _
'                " FROM OWNERSHIP AS A WHERE " & _
'                " Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_YEAR=" & txtYear.Text & " )" & _
'                " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_YEAR=" & txtYear.Text & " )" & _
'                " OR Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_YEAR=" & txtYear.Text & " )" & _
'                " OR Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID AND PBA1_YEAR=" & txtYear.Text & " )"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Exit Sub
ERR_CREATE_VIEW:
'        If Err.Number = -2147217865 Then
'                On Error GoTo ErrCreate
'                strSQL = "CREATE VIEW VIEW_WARN_REPORT AS " & _
'                        " SELECT A.PRENAME & A.OWNER_NAME & '  ' & A.OWNER_SURNAME AS FULLNAME, A.OWNERSHIP_ID, IIF(Exists (SELECT *  FROM  PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID )=0,'N','Y') AS LAND_FIND, IIF(Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID )=0,'N','Y') AS BUILDING_FIND, IIF(Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID )=0,'N','Y') AS SIGNBOARD_FIND, IIF(Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID )=0,'N','Y') AS LICENSE_FIND " & _
'                        " FROM OWNERSHIP AS A WHERE " & _
'                        " Exists (SELECT *  FROM PBT5 B  WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID AND PBT5_YEAR=" & txtYear.Text & " )" & _
'                        " OR Exists (SELECT *  FROM PRD2 C  WHERE A.OWNERSHIP_ID=C.OWNERSHIP_ID AND PRD2_YEAR=" & txtYear.Text & " )" & _
'                        " OR Exists (SELECT *  FROM PP1 D  WHERE A.OWNERSHIP_ID=D.OWNERSHIP_ID AND PP1_YEAR=" & txtYear.Text & " )" & _
'                        " OR Exists (SELECT *  FROM PBA1 E  WHERE A.OWNERSHIP_ID=E.OWNERSHIP_ID AND PBA1_YEAR=" & txtYear.Text & " )"
'                Call SET_Execute(strSQL)
'                Globle_Connective.CommitTrans
'                Me.MousePointer = 0
'        Else
                Me.MousePointer = 0
                Globle_Connective.RollbackTrans
'        End If
        Exit Sub
'ErrCreate:
'        Me.MousePointer = 0
'        Globle_Connective.RollbackTrans
End Sub

Private Sub Btn_Print_Click()
        On Error GoTo ErrPrint
        Clone_Form.CTReport.Reset
        Clone_Form.CTReport.Connect = "DSN = Microtax;UID = " & ini_Username & ";PWD = " & ini_Password & ";DSQ = Administration"
        Clone_Form.CTReport.DiscardSavedData = True
        Select Case LB_Menu.Tag
                Case "1"
                        Call Create_View
                        With Clone_Form.CTReport
                                .ReportTitle = "˹ѧ�����������Ẻ�ʴ���¡�ê�������"
                                If LenB(Trim$(Text1(2).Text)) > 0 Then .SelectionFormula = "{VIEW_WARN_REPORT.OWNERSHIP_ID} = '" & Text1(2).Tag & "'"
                                .ReportFileName = App.Path & "\Report\warn.rpt"
                                '.RetrieveDataFiles 'refresh data
                                .Formulas(0) = "Name = '" & Label2(0).Caption & "'"
'                                .Formulas(1) = "Officer_Name = '" & Text1(3).Text & "'"
                                .Formulas(1) = "Number_Book = '" & Text1(0).Text & "'"
                                .Formulas(2) = "Yearly = '" & txtYear.Text & "'"
                                .Formulas(3) = "Day_Book = '" & DTPicker1.Value & "'"
                                .Formulas(4) = "INI_SECTION = '" & iniSection & "'"
                                .Formulas(5) = "INI_ADDR = '" & iniAdd & "'"
                                .Formulas(6) = "INI_TEL = '" & iniTel & "'"
                                .Formulas(7) = "String2 = '��ɳ��� " & Text1(4).Text & "'"
                                .Formulas(8) = "String4 ='�͹حҵ��� " & Text1(5).Text & "'"
                                .Formulas(9) = "ini_position ='" & iniPosition & "'"
                                .Formulas(10) = "String5 = '                    �������ͻ���ª��ͧ�ҧ�Ҫ�����е�Ƿ�ҹ     �֧���ͤ�����سҹ���ѡ�ҹ��ҧ����ҹ���������ʴ�����ա����Է��� 㹷�Ѿ���Թ���� �� ⩹����Թ,�ѵû�Шӵ�ǻ�ЪҪ�,���ҷ���¹��ҹ,����¹�ҳԪ��,������Ѻ�Թ���ش����, ��� ��ʴ����ҹ�Ѵ������� ��ǹ��ä�ѧ'" '& iniSection & " ���㹡�˹����Ҵѧ�����  �ҡ��ҹ����ѡ�ҹ��������Ẻ�ʴ�" & _
                                                                "��¡�����մѧ������������ ���������ѡ�ҹ�����Ҿ����������Ѻ�Թ���ѧ" & iniSection & "  �·ҧ��ɳ������ͨй�仴��µ��ͧ����  �ҡ�Ѵ��ͧ��С���  �����������Һ����'"
                                .WindowMaxButton = False
                                .WindowMinButton = False
                                .Destination = crptToWindow
                                .WindowState = crptMaximized
                                .WindowShowPrintSetupBtn = True
                                .WindowShowExportBtn = False
                                .Action = 1
                        End With
                Case "2"  '���պ��ا��ͧ���
                        If Create_View2 = True Then
                                With Clone_Form.CTReport
                                        .ReportTitle = "Ẻ˹ѧ��ͤ�ҧ�������պ��ا��ͧ���"
                                        If LenB(Trim$(Text1(2).Text)) > 0 Then .SelectionFormula = "{VIEW_PBT5_ARREAR.OWNERSHIP_ID} = '" & Text1(2).Tag & "'"
                                        .ReportFileName = App.Path & "\Report\PBT5_ARREAR.rpt"
                                        .Formulas(0) = "Name = '" & Label2(0).Caption & "'"
                                        .Formulas(2) = "Number_Book = '���  " & Text1(0).Text & "'"
                                        .Formulas(3) = "Day_Book = '" & DTPicker1.Value & "'"
                                        .Formulas(4) = "INI_SECTION = '" & iniSection & "'"
                                        .Formulas(5) = "INI_ADDR = '" & iniAdd & "'"
                                        .Formulas(6) = "INI_TEL = '" & iniTel & "'"
                                        .Formulas(7) = "DATE_LINE = '�ѹ���  29  ���Ҥ�  " & txtYear.Text & "'"
                                        .Formulas(8) = "INI_POSITION ='" & iniPosition & "'"
                                        .Formulas(9) = "STRING3 ='���ا��ͧ���'"
                                        .WindowMaxButton = False
                                        .WindowMinButton = False
                                        .Destination = crptToWindow
                                        .WindowState = crptMaximized
                                        .WindowShowPrintSetupBtn = True
                                        .WindowShowExportBtn = False
                                        .Action = 1
                                End With
                        End If
                Case "3"  '�����ç���͹��з��Թ
                        If Create_View3 = True Then
                                With Clone_Form.CTReport
                                        .ReportTitle = "Ẻ˹ѧ��ͤ�ҧ���������ç���͹��з��Թ"
                                        If LenB(Trim$(Text1(2).Text)) > 0 Then .SelectionFormula = "{VIEW_PRD2_ARREAR.OWNERSHIP_ID} = '" & Text1(2).Tag & "'"
                                        .ReportFileName = App.Path & "\Report\PRD2_ARREAR.rpt"
                                        .Formulas(0) = "Name = '" & Label2(0).Caption & "'"
                                        .Formulas(2) = "Number_Book = '���  " & Text1(0).Text & "'"
                                        .Formulas(3) = "Day_Book = '" & DTPicker1.Value & "'"
                                        .Formulas(4) = "INI_SECTION = '" & iniSection & "'"
                                        .Formulas(5) = "INI_ADDR = '" & iniAdd & "'"
                                        .Formulas(6) = "INI_TEL = '" & iniTel & "'"
                                        .Formulas(7) = "DATE_LINE = '�ѹ���  29  ���Ҥ�  " & txtYear.Text & "'"
                                        .Formulas(8) = "INI_POSITION ='" & iniPosition & "'"
                                        .Formulas(9) = "STRING3 ='�ç���͹��з��Թ'"
                                        .WindowMaxButton = False
                                        .WindowMinButton = False
                                        .Destination = crptToWindow
                                        .WindowState = crptMaximized
                                        .WindowShowPrintSetupBtn = True
                                        .WindowShowExportBtn = False
                                        .Action = 1
                                End With
                        End If
                Case "4"  '���ջ���
                        If Create_View4 = True Then
                                With Clone_Form.CTReport
                                        .Reset
                                        .DiscardSavedData = True
                                        .ReportTitle = "Ẻ˹ѧ��ͤ�ҧ�������ջ���"
                                        If LenB(Trim$(Text1(2).Text)) > 0 Then .SelectionFormula = "{VIEW_PP1_ARREAR.OWNERSHIP_ID} = '" & Text1(2).Tag & "'"
                                        .ReportFileName = App.Path & "\Report\PP1_ARREAR.rpt"
                                        .Formulas(0) = "Name = '" & Label2(0).Caption & "'"
                                        .Formulas(2) = "Number_Book = '���  " & Text1(0).Text & "'"
                                        .Formulas(3) = "Day_Book = '" & DTPicker1.Value & "'"
                                        .Formulas(4) = "INI_SECTION = '" & iniSection & "'"
                                        .Formulas(5) = "INI_ADDR = '" & iniAdd & "'"
                                        .Formulas(6) = "INI_TEL = '" & iniTel & "'"
                                        .Formulas(7) = "DATE_LINE = '�ѹ���  29  ���Ҥ�  " & txtYear.Text & "'"
                                        .Formulas(8) = "INI_POSITION ='" & iniPosition & "'"
                                        .Formulas(9) = "STRING3 ='����'"
                                        .WindowMaxButton = False
                                        .WindowMinButton = False
                                        .Destination = crptToWindow
                                        .WindowState = crptMaximized
                                        .WindowShowPrintSetupBtn = True
                                        .WindowShowExportBtn = False
                                        .Action = 1
                                End With
                        End If
        End Select
'        Me.MousePointer = 0
        Exit Sub
ErrPrint:
        MsgBox Err.Description
End Sub

Private Sub Btn_Search_Click()
        Text1(2).Text = Empty
        Select Case LB_Menu.Tag
                Case 1 '������շء������
                        Chk_Ownership = 2
                Case 2 '���պ��ا��ͧ���
                        Chk_Ownership = 3
                Case 3 '�����ç���͹��з��Թ
                        Chk_Ownership = 4
                Case 4 '���ջ���
                        Chk_Ownership = 5
        End Select
        Frm_SearchOwnerShip.Show vbModal
End Sub

Private Sub Form_Load()
'      txtYear.Text = CInt(Year(Now)) + 543
       Label2(0).Caption = iniName
       DTPicker1.Value = Now
       Status_InToFrm = "WARN"
       txtYear.Text = Right$(Date, 4)
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set Frm_Print_Warn = Nothing
End Sub

Private Sub LB_Menu_Change()
Select Case LB_Menu.Tag
        Case "1"
                Text1(4).BackColor = &HFFFFFF
                Text1(5).BackColor = &HFFFFFF
        Case "2"
                Text1(4).Enabled = False
                Text1(4).BackColor = &H8000000F
                Text1(5).Enabled = False
                Text1(5).BackColor = &H8000000F
        Case "3"
                Text1(4).Enabled = False
                Text1(5).Enabled = False
                Text1(4).BackColor = &H8000000F
                Text1(5).BackColor = &H8000000F
        Case "4"
                Text1(4).Enabled = False
                Text1(5).Enabled = False
                Text1(4).BackColor = &H8000000F
                Text1(5).BackColor = &H8000000F
End Select
End Sub

Private Sub Text1_Change(Index As Integer)
        If Text1(2).Text = Empty Then Text1(2).Tag = Empty
End Sub

Private Sub Text1_DblClick(Index As Integer)
        If Index = 2 Then Text1(Index).Text = Empty
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
        If Index = 1 Then
              KeyAscii = KeyCharecter(KeyAscii, "0123456789")
        End If
End Sub

Private Function Create_View2() As Boolean
        Dim strSQL As String
        
        Globle_Connective.BeginTrans
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
'        Call SET_Execute("DROP VIEW VIEW_PBT5_ARREAR")
'        strSQL = "CREATE VIEW VIEW_PBT5_ARREAR AS " & _
'        "SELECT A.OWNERSHIP_ID, A.SUM_AMOUNT, A.SUM_MONEY_ADD, A.PBT5_YEAR, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME AS FULLNAME " & _
'        " FROM (SELECT OWNERSHIP_ID,SUM(PBT5_AMOUNT) AS SUM_AMOUNT,SUM(PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS SUM_MONEY_ADD,PBT5_YEAR " & _
'        " FROM PBT5 WHERE PBT5_YEAR=" & txtYear.Text & " AND PBT5_STATUS=0 GROUP BY OWNERSHIP_ID,PBT5_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
        strSQL = "ALTER VIEW VIEW_PBT5_ARREAR AS " & _
        "SELECT A.OWNERSHIP_ID, A.SUM_AMOUNT, A.SUM_MONEY_ADD, A.PBT5_YEAR,B.PRENAME + B.OWNER_NAME + ' ' + B.OWNER_SURNAME AS FULLNAME,B.OWNER_NAME" & _
        " FROM (SELECT OWNERSHIP_ID, SUM(PBT5_AMOUNT) AS SUM_AMOUNT, SUM(PBT5_TAXINCLUDE + PBT5_DISCOUNT) AS SUM_MONEY_ADD,PBT5_YEAR" & _
        " From dbo.PBT5 Where (PBT5_YEAR = " & txtYear.Text & ") And (PBT5_STATUS = 0)" & _
        " GROUP BY OWNERSHIP_ID, PBT5_YEAR) AS A INNER JOIN dbo.PBT5 AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View2 = True
        Exit Function
ERR_CREATE_VIEW:
'        If Err.Number = -2147217865 Then
'                On Error GoTo ErrCreate
'                strSQL = "CREATE VIEW VIEW_PBT5_ARREAR AS " & _
'                    "SELECT A.OWNERSHIP_ID, A.SUM_AMOUNT, A.SUM_MONEY_ADD, A.PBT5_YEAR, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME AS FULLNAME " & _
'                    " FROM (SELECT OWNERSHIP_ID,SUM(PBT5_AMOUNT) AS SUM_AMOUNT,SUM(PBT5_TAXINCLUDE+PBT5_DISCOUNT) AS SUM_MONEY_ADD,PBT5_YEAR " & _
'                    " FROM PBT5 WHERE PBT5_YEAR=" & txtYear.Text & " AND PBT5_STATUS=0 GROUP BY OWNERSHIP_ID,PBT5_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
'                Call SET_Execute(strSQL)
'                Globle_Connective.CommitTrans
'                Me.MousePointer = 0
'                Create_View2 = True
'        Else
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View2 = False
'        End If
        Exit Function
ErrCreate:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View2 = False
End Function

Private Function Create_View3() As Boolean
        Dim strSQL As String
        
        On Error GoTo ERR_CREATE_VIEW
        Globle_Connective.BeginTrans
        
        Me.MousePointer = 11
'        Call SET_Execute("DROP VIEW VIEW_PRD2_ARREAR")
        strSQL = "ALTER VIEW VIEW_PRD2_ARREAR AS " & _
        "SELECT A.OWNERSHIP_ID,B.PRENAME + B.OWNER_NAME + ' ' + B.OWNER_SURNAME AS FULLNAME, A.SUM_AMOUNT, A.SUM_MONEY_ADD,A.PRD2_YEAR,B.OWNER_NAME" & _
        " FROM (SELECT OWNERSHIP_ID, SUM(PRD2_RENTYEAR_TOTAL) AS SUM_AMOUNT, SUM(PRD2_TAXINCLUDE + PRD2_DISCOUNT) AS SUM_MONEY_ADD,PRD2_YEAR" & _
        " From dbo.PRD2 Where (PRD2_YEAR = " & txtYear.Text & ") And (PRD2_STATUS = 0)" & _
        " GROUP BY OWNERSHIP_ID, PRD2_YEAR) AS A INNER JOIN dbo.PRD2 AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID"
'        "SELECT A.OWNERSHIP_ID, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME AS FULLNAME, SUM_AMOUNT, SUM_MONEY_ADD, PRD2_YEAR " & _
'        " FROM (SELECT OWNERSHIP_ID,SUM(PRD2_RENTYEAR_TOTAL) AS SUM_AMOUNT,SUM(PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS SUM_MONEY_ADD,PRD2_YEAR " & _
'        " FROM PRD2 WHERE PRD2_YEAR=" & txtYear.Text & " AND PRD2_STATUS=0 GROUP BY OWNERSHIP_ID,PRD2_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View3 = True
        Exit Function
ERR_CREATE_VIEW:
'        If Err.Number = -2147217865 Then
'                On Error GoTo ErrCreate
'                strSQL = "CREATE VIEW VIEW_PRD2_ARREAR AS " & _
'                "SELECT A.OWNERSHIP_ID, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME AS FULLNAME, SUM_AMOUNT, SUM_MONEY_ADD, PRD2_YEAR " & _
'                " FROM (SELECT OWNERSHIP_ID,SUM(PRD2_RENTYEAR_TOTAL) AS SUM_AMOUNT,SUM(PRD2_TAXINCLUDE+PRD2_DISCOUNT) AS SUM_MONEY_ADD,PRD2_YEAR " & _
'                " FROM PRD2 WHERE PRD2_YEAR=" & txtYear.Text & " AND PRD2_STATUS=0 GROUP BY OWNERSHIP_ID,PRD2_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
'                Call SET_Execute(strSQL)
'                Globle_Connective.CommitTrans
'                Me.MousePointer = 0
'                Create_View3 = True
'        Else
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View3 = False
'        End If
        Exit Function
ErrCreate:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View3 = False
End Function

Private Function Create_View4() As Boolean
        Dim strSQL As String
        
        On Error GoTo ERR_CREATE_VIEW
        Me.MousePointer = 11
        Globle_Connective.BeginTrans
'        Call SET_Execute("DROP VIEW VIEW_PP1_ARREAR")
        strSQL = "ALTER VIEW VIEW_PP1_ARREAR AS " & _
        "SELECT A.OWNERSHIP_ID, B.PRENAME + B.OWNER_NAME + ' ' + B.OWNER_SURNAME AS Expr1, A.SUM_AMOUNT, A.SUM_MONEY_ADD,A.PP1_YEAR,B.OWNER_NAME" & _
        " FROM (SELECT OWNERSHIP_ID, SUM(PP1_AMOUNT) AS SUM_AMOUNT, SUM(PP1_TAXINCLUDE + PP1_DISCOUNT) AS SUM_MONEY_ADD,PP1_YEAR" & _
        " FROM dbo.PP1 WHERE (PP1_YEAR = " & txtYear.Text & ") And (PP1_STATUS = 0) GROUP BY OWNERSHIP_ID, PP1_YEAR) AS A INNER JOIN" & _
        " dbo.PP1 AS B ON A.OWNERSHIP_ID = B.OWNERSHIP_ID"
'        "SELECT A.OWNERSHIP_ID, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME, SUM_AMOUNT, SUM_MONEY_ADD, PP1_YEAR" & _
'        " FROM (SELECT OWNERSHIP_ID,SUM(PP1_AMOUNT) AS SUM_AMOUNT,SUM(PP1_TAXINCLUDE+PP1_DISCOUNT) AS SUM_MONEY_ADD,PP1_YEAR " & _
'        " FROM PP1 WHERE PP1_YEAR=" & txtYear.Text & " AND PP1_STATUS=0 GROUP BY OWNERSHIP_ID,PP1_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
        Call SET_Execute(strSQL)
        Globle_Connective.CommitTrans
        Me.MousePointer = 0
        Create_View4 = True
        Exit Function
ERR_CREATE_VIEW:
'        If Err.Number = -2147217865 Then
'                On Error GoTo ErrCreate
'                strSQL = "CREATE VIEW VIEW_PP1_ARREAR AS " & _
'                "SELECT A.OWNERSHIP_ID, PRENAME & OWNER_NAME & ' ' & OWNER_SURNAME, SUM_AMOUNT, SUM_MONEY_ADD, PP1_YEAR" & _
'                " FROM (SELECT OWNERSHIP_ID,SUM(PP1_AMOUNT) AS SUM_AMOUNT,SUM(PP1_TAXINCLUDE+PP1_DISCOUNT) AS SUM_MONEY_ADD,PP1_YEAR " & _
'                " FROM PP1 WHERE PP1_YEAR=" & txtYear.Text & " AND PP1_STATUS=0 GROUP BY OWNERSHIP_ID,PP1_YEAR) AS A, OWNERSHIP AS B WHERE A.OWNERSHIP_ID=B.OWNERSHIP_ID"
'                Call SET_Execute(strSQL)
'                Globle_Connective.CommitTrans
'                Me.MousePointer = 0
'                Create_View4 = True
'        Else
                Globle_Connective.RollbackTrans
                Me.MousePointer = 0
                Create_View4 = False
'        End If
        Exit Function
ErrCreate:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        Create_View4 = False
End Function
