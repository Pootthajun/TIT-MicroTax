VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form Frm_CreatePassword 
   BackColor       =   &H00D6D6D6&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " User Security Editor"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6000
   FillColor       =   &H000000FF&
   Icon            =   "Frm_CreatePassword.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_CreatePassword.frx":151A
   ScaleHeight     =   438
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Txt_UserName 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1470
      TabIndex        =   8
      Top             =   2640
      Width           =   4455
   End
   Begin VB.CommandButton Btn_Delete 
      BackColor       =   &H00D6D6D6&
      Caption         =   "ź�����"
      Height          =   375
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2220
      Width           =   1455
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2055
      Left            =   1500
      TabIndex        =   9
      Top             =   540
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   3625
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      TabCaption(0)   =   "���������ҹ"
      TabPicture(0)   =   "Frm_CreatePassword.frx":74AE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label2(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Txt_Sure"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Txt_Pw"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Combo1"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Txt_User"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "��Ѻ����¹���ʼ�ҹ"
      TabPicture(1)   =   "Frm_CreatePassword.frx":74CA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Txt_Chg_User"
      Tab(1).Control(1)=   "Txt_Pw_Old"
      Tab(1).Control(2)=   "Txt_Pw_New"
      Tab(1).Control(3)=   "Txt_Pw_Sure_New"
      Tab(1).Control(4)=   "Label9"
      Tab(1).Control(5)=   "Label10"
      Tab(1).Control(6)=   "Label11"
      Tab(1).Control(7)=   "Label12"
      Tab(1).ControlCount=   8
      Begin VB.TextBox Txt_Chg_User 
         Height          =   330
         Left            =   -73710
         MaxLength       =   18
         TabIndex        =   4
         Top             =   420
         Width           =   2985
      End
      Begin VB.TextBox Txt_Pw_Old 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   -73710
         PasswordChar    =   "�"
         TabIndex        =   5
         Top             =   810
         Width           =   2985
      End
      Begin VB.TextBox Txt_Pw_New 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   -73710
         PasswordChar    =   "�"
         TabIndex        =   6
         Top             =   1200
         Width           =   2985
      End
      Begin VB.TextBox Txt_Pw_Sure_New 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   -73710
         PasswordChar    =   "�"
         TabIndex        =   7
         Top             =   1590
         Width           =   2985
      End
      Begin VB.TextBox Txt_User 
         Height          =   330
         Left            =   1290
         MaxLength       =   18
         TabIndex        =   1
         Top             =   420
         Width           =   2985
      End
      Begin VB.ComboBox Combo1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_CreatePassword.frx":74E6
         Left            =   1290
         List            =   "Frm_CreatePassword.frx":74F0
         Style           =   2  'Dropdown List
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   1575
         Width           =   1275
      End
      Begin VB.TextBox Txt_Pw 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   1290
         MaxLength       =   14
         PasswordChar    =   "�"
         TabIndex        =   2
         Top             =   810
         Width           =   2985
      End
      Begin VB.TextBox Txt_Sure 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   1290
         MaxLength       =   14
         PasswordChar    =   "�"
         TabIndex        =   3
         Top             =   1200
         Width           =   2985
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ʼ�ҹ���� :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -74760
         TabIndex        =   18
         Top             =   1290
         Width           =   975
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�׹�ѹ���ʼ�ҹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -74910
         TabIndex        =   17
         Top             =   1680
         Width           =   1125
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ʼ�ҹ��� :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -74715
         TabIndex        =   16
         Top             =   885
         Width           =   930
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����ҹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -74475
         TabIndex        =   15
         Top             =   510
         Width           =   660
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����ҹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   540
         TabIndex        =   14
         Top             =   510
         Width           =   660
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ʼ�ҹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   525
         TabIndex        =   13
         Top             =   915
         Width           =   675
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�׹�ѹ���ʼ�ҹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   75
         TabIndex        =   12
         Top             =   1305
         Width           =   1125
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ʶҹ� :"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   645
         TabIndex        =   11
         Top             =   1710
         Width           =   555
      End
   End
   Begin MSComctlLib.ListView LstV_UserPassword 
      Height          =   3555
      Left            =   0
      TabIndex        =   0
      Top             =   3000
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   6271
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�����ҹ"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ʶҹ�"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "����-���ʡ��"
         Object.Width           =   4764
      EndProperty
   End
   Begin VB.CommandButton Btn_Ok 
      BackColor       =   &H00D6D6D6&
      Caption         =   "���������"
      Height          =   405
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   1830
      Width           =   1455
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����-���ʡ�� :"
      ForeColor       =   &H00000000&
      Height          =   195
      Index           =   1
      Left            =   480
      TabIndex        =   21
      Top             =   2700
      Width           =   915
   End
End
Attribute VB_Name = "Frm_CreatePassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset
Dim Status As String
Dim itmX As ListItem

Private Sub ChekStatusLogin()
      Call SET_QUERY("Select * From User_Password  Order by User_name ", Query)
              Status = Right$(Clone_Form.StatusBar1.Panels(1).Text, Len(Clone_Form.StatusBar1.Panels(1).Text) - 9)
      If Query.RecordCount = 0 Then
          Combo1.Enabled = True
      Else
           If Status = "GIS" Then
              Combo1.Enabled = True
           Else
             Combo1.Enabled = False
           End If
      End If
End Sub


Private Sub SetRefresh()
Call SSTab1_Click(0)
        Call SET_QUERY("Select * From User_Password  Order by User_name ", Query)
        LstV_UserPassword.ListItems.Clear
If Query.RecordCount = 0 Then Exit Sub
With Query
                  .MoveFirst
            Do While Not .EOF
                Set itmX = LstV_UserPassword.ListItems.Add()
                        itmX.SmallIcon = Clone_Form.ImageList1.ListImages(1).Index
                        itmX.Text = .Fields("User_Login").Value
                        itmX.SubItems(1) = .Fields("User_Status").Value
                        itmX.SubItems(2) = .Fields("User_Name").Value
                        .MoveNext
            Loop
End With
End Sub


Private Sub Btn_Delete_Click()
On Error Resume Next
 If Status <> "GIS" Then
     MsgBox "ʶҹ������ GIS MODE �������öź�����ҹ��", , "����͹ !"
    Exit Sub
 End If
        If MsgBox("��ͧ���ź�����ҹ : " & LstV_UserPassword.ListItems.Item(LstV_UserPassword.SelectedItem.Index).Text & " : ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ���ź�����ҹ�͡�ҡ�к�") = vbNo Then
             Cancel = True
             Exit Sub
        End If
           Call SET_Execute("Delete From User_Password Where User_Login = '" & LstV_UserPassword.ListItems.Item(LstV_UserPassword.SelectedItem.Index).Text & "'")
           Call SetRefresh
End Sub

Private Sub Btn_Ok_Click()

If SSTab1.Tab = 0 Then
     If Status <> "GIS" And LenB(Trim$(Status)) <> 0 Then
            MsgBox "ʶҹ������ GIS MODE �������ö���������ҹ��", , "����͹ !"
            Exit Sub
    End If
    If Len(Txt_User.Text) < 4 Then
           MsgBox "��͡���ͼ����ҹ 4 �ѡ��Т��� !", vbOKOnly, "����͹ !"
           Txt_User.SetFocus
            Exit Sub
    End If
        If Len(Txt_Pw.Text) < 4 Then
            MsgBox "��͡���ʼ�ҹ 4 �ѡ��Т��� !", vbOKOnly, "����͹ !"
            Txt_Pw.SetFocus
        Exit Sub
     End If
        If Txt_Pw.Text <> Txt_Sure.Text Then
            MsgBox "���ʼ�ҹ���ç�ѹ !", vbOKOnly, "����͹ !"
            Txt_Sure.SetFocus
            Exit Sub
        End If
        If LenB(Combo1.Text) = 0 Then
              MsgBox "�ô��˹�ʶҹм����ҹ !", vbOKOnly, "����͹ !"
              Exit Sub
        End If
Else
If Len(Txt_Chg_User.Text) < 4 Then
   MsgBox "��͡���ͼ����ҹ 4 �ѡ�â��� !", vbOKOnly, "����͹ !"
   Txt_Chg_User.SetFocus
   Exit Sub
End If
If Len(Txt_Pw_Old.Text) < 4 Then
   MsgBox "��͡���ʼ�ҹ��� 4 �ѡ�â��� !", vbOKOnly, "����͹ !"
   Txt_Pw_Old.SetFocus
   Exit Sub
End If
If Len(Txt_Pw_New.Text) < 4 Then
   MsgBox "��͡���ʼ�ҹ���� 4 �ѡ�â��� !", vbOKOnly, "����͹ !"
   Txt_Pw_New.SetFocus
   Exit Sub
End If
If Txt_Pw_New.Text <> Txt_Pw_Sure_New.Text Then
    MsgBox "���ʼ�ҹ���ç�ѹ !", vbOKOnly, "����͹ !"
    Txt_Pw_Sure_New.SetFocus
    Exit Sub
End If
End If
          If Txt_UserName.Text = Empty Then
                MsgBox "�кء�͡������й��ʡ�� �����ҹ", vbOKOnly, "����͹ !"
                Exit Sub
          End If
     If Status <> "GIS" Then
            If Trim$(Txt_Chg_User) <> Trim$(Right$(Clone_Form.StatusBar1.Panels(2).Text, Len(Clone_Form.StatusBar1.Panels(2).Text) - 13)) Then
                   MsgBox "User Name ���ç�Ѻ��������ҹ �������ö��Ѻ����¹�� ", , "����͹ !"
                   Exit Sub
            End If
     End If
    
                  If SSTab1.Tab = 0 Then
                        Call SET_QUERY("SELECT * FROM USER_PASSWORD WHERE USER_LOGIN = '" & Trim$(Txt_User.Text) & "'", Query)
                        If Query.RecordCount = 0 Then
                                            SET_Execute (" Insert into USER_PASSWORD ( USER_LOGIN , USER_PASSWORD , USER_STATUS , USER_NAME ) " & _
                                                        " VALUES ( '" & Txt_User.Text & "' , '" & Enzinc_Password(Txt_Pw.Text, "ENCODE") & "','" & Combo1.Text & "','" & Trim$(Txt_UserName.Text) & "')")
                                                        Clone_Form.StatusBar1.Panels(1).Text = "Status : " & Combo1.Text
                                                        Clone_Form.StatusBar1.Panels(2).Text = "User Login : " & Trim$(Txt_User.Text)
                                                        Status = Combo1.Text
                        Else
                                            MsgBox "���ͼ��������������к����� �ô����¹���� !", vbOKOnly, "����͹ !"
                        End If
                Else
                       Dim Buffer_Pw As String
                                       Buffer_Pw = Enzinc_Password(Trim$(Txt_Pw_Old.Text), "ENCODE")
                               Call SET_QUERY("Select  *  From  USER_PASSWORD Where User_Login = '" & Trim(Txt_Chg_User.Text) & "' And User_Password  =   '" & Buffer_Pw & "'", Query)
                                        If Query.RecordCount > 0 Then
                                                Buffer_Pw = Empty
                                                Buffer_Pw = Enzinc_Password(Txt_Pw_Old.Text, "ENCODE")
                                                                        Call SET_Execute(" UPDATE USER_PASSWORD SET  USER_PASSWORD = '" & Enzinc_Password(Txt_Pw_New.Text, "ENCODE") & "' , USER_NAME = '" & Txt_UserName.Text & "'" & _
                                                                                        "Where USER_LOGIN = '" & Txt_Chg_User.Text & "' And  USER_PASSWORD ='" & Buffer_Pw & "'")
                                      Else
                                              MsgBox "���ʼ�ҹ������١��ͧ !", vbOKOnly, "����͹ !"
                                              Exit Sub
                                      End If
                   End If
                    MsgBox "New Password Commit", vbOKOnly, "�׹�ѹ !"
                   Call SetRefresh
                           
End Sub
Private Sub Form_Load()
Set Query = New ADODB.Recordset
LstV_UserPassword.Icons = Clone_Form.ImageList1
LstV_UserPassword.SmallIcons = Clone_Form.ImageList1
Call ChekStatusLogin
Call SetRefresh
End Sub

Private Sub Form_Unload(Cancel As Integer)
 Set Query = Nothing
 Set Frm_CreatePassword = Nothing
End Sub

Private Sub Label5_Click()
   Label5.BackColor = &HC0&
   Label6.BackColor = &H7B584B
  SSTab1.Tab = 1
End Sub

Private Sub Label6_Click()
 If State_Login = "USER" Then
     MsgBox " ʶҹ���͡�Թ : USER : ����觹���������ö�ӧҹ�� "
     Exit Sub
 End If
   Label6.BackColor = &HC0&
   Label5.BackColor = &H7B584B
   SSTab1.Tab = 0
End Sub


Private Sub LstV_UserPassword_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Txt_Chg_User.Text = LstV_UserPassword.ListItems.Item(LstV_UserPassword.SelectedItem.Index).Text
  Txt_UserName.Text = LstV_UserPassword.ListItems.Item(LstV_UserPassword.SelectedItem.Index).SubItems(2)
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
If SSTab1.Tab = 0 Then
            Btn_Ok.Caption = "���������"
Else
            Btn_Ok.Caption = "��Ѻ����¹����"
End If
        Txt_User.Text = Empty
        Txt_Pw.Text = Empty
        Txt_Sure.Text = Empty
        Txt_UserName.Text = Empty
        Txt_Chg_User.Text = Empty
        Txt_Pw_Old.Text = Empty
        Txt_Pw_New.Text = Empty
        Txt_Pw_Sure_New.Text = Empty
End Sub
Private Sub Txt_Chg_User_GotFocus()
       Txt_Chg_User.SelStart = 0
       Txt_Chg_User.SelLength = Len(Txt_Chg_User.Text)
End Sub

Private Sub Txt_Chg_User_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

Private Sub Txt_Pw_GotFocus()
       Txt_Pw.SelStart = 0
       Txt_Pw.SelLength = Len(Txt_Pw.Text)
End Sub

Private Sub Txt_Pw_KeyPress(KeyAscii As Integer)
 KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

Private Sub Txt_Pw_New_GotFocus()
       Txt_Pw_New.SelStart = 0
       Txt_Pw_New.SelLength = Len(Txt_Pw_New.Text)
End Sub

Private Sub Txt_Pw_New_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

Private Sub Txt_Pw_Old_GotFocus()
       Txt_Pw_Old.SelStart = 0
       Txt_Pw_Old.SelLength = Len(Txt_Pw_Old.Text)
End Sub

Private Sub Txt_Pw_Old_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub


Private Sub Txt_Pw_Sure_New_GotFocus()
       Txt_Pw_Sure_New.SelStart = 0
       Txt_Pw_Sure_New.SelLength = Len(Txt_Pw_Sure_New.Text)
End Sub

Private Sub Txt_Pw_Sure_New_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

Private Sub Txt_Sure_GotFocus()
       Txt_Sure.SelStart = 0
       Txt_Sure.SelLength = Len(Txt_Sure.Text)
End Sub

Private Sub Txt_Sure_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

Private Sub Txt_User_GotFocus()
       Txt_User.SelStart = 0
       Txt_User.SelLength = Len(Txt_User.Text)
End Sub

Private Sub Txt_User_KeyPress(KeyAscii As Integer)
 KeyAscii = KeyCharecter(KeyAscii, Character)
End Sub

