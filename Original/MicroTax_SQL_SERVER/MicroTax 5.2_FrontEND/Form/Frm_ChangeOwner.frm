VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_ChangeOwner 
   ClientHeight    =   9075
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12915
   ControlBox      =   0   'False
   Icon            =   "Frm_ChangeOwner.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_ChangeOwner.frx":151A
   ScaleHeight     =   9075
   ScaleWidth      =   12915
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cb_Land_Change 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2820
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   480
      Width           =   2835
   End
   Begin VB.ComboBox cb_Land_Change_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3900
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   0
      Visible         =   0   'False
      Width           =   3375
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   8970
      TabIndex        =   6
      Top             =   480
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   661
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   72089601
      CurrentDate     =   38098
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   10860
      TabIndex        =   7
      Top             =   480
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   661
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   72089601
      CurrentDate     =   38098
   End
   Begin VB.TextBox Txt_UserName 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   7080
      MaxLength       =   14
      TabIndex        =   3
      Top             =   480
      Width           =   1845
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Story 
      Height          =   8130
      Left            =   0
      TabIndex        =   5
      Top             =   900
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   14340
      _Version        =   393216
      BackColor       =   16777215
      ForeColor       =   4928262
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   13281668
      ForeColorSel    =   426222
      BackColorBkg    =   16777215
      BackColorUnpopulated=   16777215
      GridColor       =   12632256
      GridColorFixed  =   0
      GridColorUnpopulated=   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   2
      GridLinesFixed  =   1
      GridLinesUnpopulated=   1
      AllowUserResizing=   3
      Appearance      =   0
      RowSizingMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.TextBox Txt_LICENSE_NO 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   8070
      MaxLength       =   14
      TabIndex        =   4
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�֧"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   0
      Left            =   10590
      TabIndex        =   8
      Top             =   570
      Width           =   210
   End
   Begin VB.Image Image1 
      Height          =   360
      Left            =   12450
      Picture         =   "Frm_ChangeOwner.frx":18571
      Top             =   480
      Width           =   360
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   3
      Left            =   6045
      TabIndex        =   2
      Top             =   540
      Width           =   945
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����¹����ѵԢ�������͹��ѧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   1
      Left            =   10410
      TabIndex        =   1
      Top             =   90
      Visible         =   0   'False
      Width           =   2325
   End
   Begin VB.Label LBMenu 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ŷ���¹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF9900&
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   510
      Width           =   1410
   End
End
Attribute VB_Name = "Frm_ChangeOwner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As Recordset

Private Sub cb_Land_Change_Click()
        cb_Land_Change_ID.ListIndex = cb_Land_Change.ListIndex
End Sub

Private Sub Form_Activate()
        Clone_Form.Picture1.Visible = True
End Sub

Private Sub Form_Load()
        Set Query = New ADODB.Recordset
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Query = Nothing
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
        Image1.Top = Image1.Top + 25
End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        Image1.Top = 480
        Grid_Story.FixedCols = 0
        Debug.Print "exec sp_view_history '" & Trim$(Txt_UserName.Text) & "','" & Trim$(Txt_LICENSE_NO.Text) & "','" & IIf(IsNull(DTPicker1.Value) = False, CvDate1(DTPicker1), "") & "','" & IIf(IsNull(DTPicker2.Value) = False, CvDate1(DTPicker2), "") & "','" & cb_Land_Change_ID.Text & "','" & LBMenu.Tag & "'"
        Set Query = Globle_Connective.Execute("exec sp_view_history '" & Trim$(Txt_UserName.Text) & "','" & Trim$(Txt_LICENSE_NO.Text) & "','" & IIf(IsNull(DTPicker1.Value) = False, CvDate1(DTPicker1), "") & "','" & IIf(IsNull(DTPicker2.Value) = False, CvDate1(DTPicker2), "") & "','" & cb_Land_Change_ID.Text & "','" & LBMenu.Tag & "'", , adCmdUnknown)
If Query.RecordCount > 0 Then
   Set Grid_Story.DataSource = Query
           Label1(1).Caption = "����¹����ѵԢ����������͹��ѧ ��Ѻ�����Ũӹǹ " & Int(Right$(Grid_Story.TextMatrix(1, 0), 4)) & " ����"
Else
           Label1(1).Caption = "����¹����ѵԢ����������͹��ѧ"
           Grid_Story.Rows = 2
           Grid_Story.Clear
           MsgBox "��辺����㹻���ѵ�" & LBMenu.Caption, vbInformation, "�Ӫ��ᨧ"
End If
      Call SetGrid_Story
      Call SetHeadGrid_Story
End Sub

Private Sub SetHeadGrid_Story()
       With Grid_Story
         Select Case LBMenu.Tag
                      Case "1"
                            .FormatString = "<���ʷ��Թ-���駷��         |^�ѹ����¹�ŧ|^⫹/���͡|^����     |^�Ţ���Թ  |^���ҧ               |^˹�����Ǩ   |^�Ţ�͡����Է���|^������       |^���     |^�ҹ     |^��       |^(Ŵ���͹) ��� |^�ҹ     |^��       |^���Ŷ��       |^��û����Թ   |^ʶҹЪ���    |^�Ҥҡ�ҧ  |^����          |^���˵�                                                                                      " & _
                                                          "||^��������ª��                                          |^��������������ª��         |^��������´                                                                                  |^����������ª��    |^����ش                    |^�ӹǹ�շ�����  |^�.�.����� (���) |^�ҹ     |^��             |^����           |^ʶҹ�             |^����͡����Է���                                                              "
                               .ColWidth(0) = 1500
                               .ColWidth(21) = 0
                               Txt_UserName.Width = 1845
                       Case "2"
                            .FormatString = "<�����ç���͹-���駷��  |^�ѹ����¹�ŧ|^⫹/���͡|^����     |^���ʷ��Թ  |^���ʺ�ҹ      |^�Ţ����ҹ   |^�ѡɳ�                                               |^������       |^�ѹ���ҧ      |^���ҧ       |^���         |^�ӹǹ���  |^�ӹǹ��ͧ  |^�����������       |^ʶҹЪ���     |^�Ҥҡ�ҧ     |^�����˵�                           |^�����ͧ   |^������          |^�Ҥ����  " & _
                                                          "|^�Թ��� �    |^�ӹǹ����� |^�ѹ��������  |^�����������»�     |^��ä��     |^��������´��ä��                     |^������ջ����Թ��    |^���˵�                                           ||^��Ңͧ�ç���͹                                                      |^����ͺ��ͧ                                            "
                                .ColWidth(0) = 1700
                               .ColWidth(29) = 0
                               Txt_UserName.Width = 1845
                       Case "3"
                            .FormatString = "<���ʻ���-���駷��         |^�ѹ����¹�ŧ|^⫹/���͡|^���ʷ��Թ     |^�����ç���͹  |^�Ţ����Ҥ��    |^���ͻ���                                         |^�ӹǹ��ҹ  |^���˵�                                     |^ʶҹЪ���   |^����͡����Է���                                               |^����������ª��     |^����ش             |^�������       " & _
                                                          "|^����ͺ��ͧ                                          |^��ҹ���  |^��ͤ���                                             |^�����˵�                                   |^�ѡɳ�                |^������                                             |^���ҧ (�.�)           |^��� (�.�)     |^��鹷�����     |^����        "
                                           .ColWidth(0) = 1500
                               Txt_UserName.Width = 1845
                       Case "4"
                            .FormatString = "<�������/�Ţ���-���駷��         |^�ѹ����¹�ŧ|^���ʷ��Թ        |^�����ç���͹        |^������躹          |^�����͹حҵ                                             |^�ѡɳСԨ��ä��                                |^���Ѿ��                                    |^ῡ��                                       |^�ѹ����͹حҵ     |^���ѧ����ͧ�ѡ� (�ç���)          |^�ӹǹ��ѡ�ҹ       " & _
                                                          "|^��鹷���Сͺ��� (��.�.)         |^ʶҹ��͹حҵ                |^�ѹ¡��ԡ�͹حҵ               |^�����˵�                                                |^��Ҹ�������     |^���˵���䢢�����                                                              |^����ͺ��ͧ                                               "
                              .ColWidth(0) = 1800
                               Txt_UserName.Width = 945
            End Select
    End With
End Sub

Private Sub SetGrid_Story()
        If Query.STATE = 0 Then Exit Sub
Dim i As Long
With Grid_Story
         If Query.RecordCount > 0 Then
             For i = 1 To Grid_Story.Rows - 1
                    With Grid_Story
                             Select Case LBMenu.Tag
                                          Case "1"
                                                            .TextMatrix(i, 1) = Format$(Day(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Format$(Month(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Right$(CDate(Left$(.TextMatrix(i, 1), 10)), 4) + 543
                                                            .TextMatrix(i, 8) = Land_Type(CByte(.TextMatrix(i, 8)))
                                                            .TextMatrix(i, 11) = FormatNumber(CByte(.TextMatrix(i, 11)), 2)
                                                            .TextMatrix(i, 14) = FormatNumber(CByte(.TextMatrix(i, 14)), 2)
                                                            If .TextMatrix(i, 15) = "1" Then
                                                                .TextMatrix(i, 15) = "�Դ���"
                                                            Else
                                                                .TextMatrix(i, 15) = "���Դ���"
                                                            End If
                                                            If .TextMatrix(i, 16) = "1" Then
                                                                .TextMatrix(i, 16) = "���˹�ҷ��"
                                                            Else
                                                                .TextMatrix(i, 16) = "�к��Ѵ��"
                                                            End If
                                                            If .TextMatrix(i, 17) = "1" Then
                                                                .TextMatrix(i, 17) = "��ͧ��������"
                                                            Else
                                                                .TextMatrix(i, 17) = "����ͧ��������"
                                                            End If
                                                            .TextMatrix(i, 18) = Format$(.TextMatrix(i, 18), "#,###,###,###0.00##")
                                                            .TextMatrix(i, 19) = Format$(.TextMatrix(i, 19), "#,###,###,###0.00##")
                                                             Select Case .TextMatrix(i, 23)
                                                                          Case "1"
                                                                                      .TextMatrix(i, 23) = "����§�ѵ��"
                                                                          Case "2"
                                                                                      .TextMatrix(i, 23) = "�������ء"
                                                                          Case "3"
                                                                                      .TextMatrix(i, 23) = "����׹��"
                                                                          Case "4"
                                                                                      .TextMatrix(i, 23) = "��ҧ����"
                                                             End Select
                                                                        If LenB(Trim$(.TextMatrix(i, 25))) > 0 Then .TextMatrix(i, 25) = Left$(Format$(.TextMatrix(i, 25), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 25), "dd/mm/yyyy"), 4)) + 543
                                                                        If LenB(Trim$(.TextMatrix(i, 26))) > 0 Then .TextMatrix(i, 26) = Left$(Format$(.TextMatrix(i, 26), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 26), "dd/mm/yyyy"), 4)) + 543
                                                                        If LenB(Trim$(.TextMatrix(i, 30))) > 0 Then .TextMatrix(i, 30) = FormatNumber(CByte(.TextMatrix(i, 30)), 2)
                                                                       If .TextMatrix(i, 32) = "0" Then
                                                                                .TextMatrix(i, 32) = "�����ͧ"
                                                                       Else
                                                                                .TextMatrix(i, 32) = "������"
                                                                       End If
                                      Case "2"
                                                            .TextMatrix(i, 1) = Format$(Day(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Format$(Month(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Right$(CDate(Left$(.TextMatrix(i, 1), 10)), 4) + 543
                                                            If .TextMatrix(i, 8) = "1" Then
                                                                .TextMatrix(i, 8) = "����"
                                                            Else
                                                                .TextMatrix(i, 8) = "���Ǥ���"
                                                            End If
                                                            .TextMatrix(i, 9) = Left$(Format$(.TextMatrix(i, 9), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 9), "dd/mm/yyyy"), 4)) + 543
                                                                       If .TextMatrix(i, 14) = "0" Then
                                                                                .TextMatrix(i, 14) = "������ŧ"
                                                                       Else
                                                                                .TextMatrix(i, 14) = "����ŧ"
                                                                       End If
                                                                       If .TextMatrix(i, 15) = "0" Then
                                                                                .TextMatrix(i, 15) = "����ͧ��������"
                                                                       Else
                                                                                .TextMatrix(i, 15) = "��ͧ��������"
                                                                       End If
                                                            .TextMatrix(i, 16) = FormatNumber(CSng(.TextMatrix(i, 16)), 2)
                                                                       If .TextMatrix(i, 18) = "0" Then
                                                                                .TextMatrix(i, 18) = " "
                                                                       Else
                                                                                .TextMatrix(i, 18) = "/"
                                                                       End If
                                                                       If .TextMatrix(i, 19) = "0" Then
                                                                                .TextMatrix(i, 19) = " "
                                                                       Else
                                                                                .TextMatrix(i, 19) = "/"
                                                                       End If
                                                                         .TextMatrix(i, 20) = FormatNumber(CSng(.TextMatrix(i, 20)), 2)
                                                                         .TextMatrix(i, 21) = FormatNumber(CSng(.TextMatrix(i, 21)), 2)
                                                                         If .TextMatrix(i, 23) <> Empty Then .TextMatrix(i, 23) = Left$(Format$(.TextMatrix(i, 23), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 23), "dd/mm/yyyy"), 4)) + 543
                                                                        .TextMatrix(i, 24) = Format$(.TextMatrix(i, 24), "#,###,###,###0.00##")
                                                                       If .TextMatrix(i, 25) = "0" Then
                                                                                .TextMatrix(i, 25) = " "
                                                                       Else
                                                                                .TextMatrix(i, 25) = "/"
                                                                       End If
                                                                        .TextMatrix(i, 27) = Format$(.TextMatrix(i, 27), "#,###,###,###0.00##")
                                      Case "3"
                                                            .TextMatrix(i, 1) = Format$(Day(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Format$(Month(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Right$(CDate(Left$(.TextMatrix(i, 1), 10)), 4) + 543
                                                                       If .TextMatrix(i, 9) = "0" Then
                                                                                .TextMatrix(i, 9) = "����ͧ��������"
                                                                       Else
                                                                                .TextMatrix(i, 9) = "��ͧ��������"
                                                                       End If
                                                       If .TextMatrix(i, 11) <> Empty Then .TextMatrix(i, 11) = Left$(Format$(.TextMatrix(i, 11), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 11), "dd/mm/yyyy"), 4)) + 543
                                                       If .TextMatrix(i, 12) <> Empty Then .TextMatrix(i, 12) = Left$(Format$(.TextMatrix(i, 12), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 12), "dd/mm/yyyy"), 4)) + 543
                                                            .TextMatrix(i, 13) = FormatNumber(CLng(.TextMatrix(i, 13)), 2)
                                                            If .TextMatrix(i, 18) = "1" Then
                                                                .TextMatrix(i, 18) = "����"
                                                            Else
                                                                .TextMatrix(i, 18) = "���Ǥ���"
                                                            End If
                                                             Select Case .TextMatrix(i, 19)
                                                                          Case "1"
                                                                                      .TextMatrix(i, 19) = "����ǹ"
                                                                          Case "2"
                                                                                      .TextMatrix(i, 19) = "�»��ѧ���"
                                                                          Case "3"
                                                                                      .TextMatrix(i, 19) = "������ѡ����"
                                                                          Case "4"
                                                                                      .TextMatrix(i, 19) = "���ºҧ��ǹ��ӡ����ѧ���"
                                                             End Select
                                                                           .TextMatrix(i, 20) = Format$(.TextMatrix(i, 20), "#######0.00##")
                                                                           .TextMatrix(i, 21) = Format$(.TextMatrix(i, 21), "#######0.00##")
                                                                           .TextMatrix(i, 22) = Format$(.TextMatrix(i, 22), "#######0.00##")
                                                                          .TextMatrix(i, 23) = Format$(.TextMatrix(i, 23), "0.00")
                                    Case "4"
                                                            .TextMatrix(i, 1) = Format$(Day(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Format$(Month(CDate(Left$(.TextMatrix(i, 1), 10))), "00") & "-" & Right$(CDate(Left$(.TextMatrix(i, 1), 10)), 4) + 543
                                                           If .TextMatrix(i, 2) = "000000" Then .TextMatrix(i, 2) = "-"
                                                           If .TextMatrix(i, 3) = "000000" Then .TextMatrix(i, 3) = "-"
                                                             Select Case .TextMatrix(i, 4)
                                                                          Case "0"
                                                                                       .TextMatrix(i, 4) = "����кط����"
                                                                            Case "1"
                                                                                                  .TextMatrix(i, 4) = "��駺��ŧ���Թ"
                                                                             Case "2"
                                                                                              .TextMatrix(i, 4) = "��駺��ç���͹"
                                                             End Select
                                                            .TextMatrix(i, 9) = Left$(Format$(.TextMatrix(i, 9), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 9), "dd/mm/yyyy"), 4)) + 543
                                                             If .TextMatrix(i, 13) = "0" Then
                                                                  .TextMatrix(i, 13) = "¡��ԡ�͹حҵ"
                                                             Else
                                                                  .TextMatrix(i, 13) = "�ѧ���͹حҵ"
                                                             End If
                                                                  If Len(.TextMatrix(i, 14)) > 0 Then .TextMatrix(i, 14) = Left$(Format$(.TextMatrix(i, 14), "dd/mm/yyyy"), 6) & CInt(Right$(Format$(.TextMatrix(i, 14), "dd/mm/yyyy"), 4)) + 543
                             End Select
                    End With
             Next i
                 .FixedCols = 1
         Else
               .FixedCols = 0
        End If
    With Grid_Story
                 .MergeCells = 4
        For i = 0 To .Cols - 1
                 .MergeCol(i) = True
        Next i
    End With
End With
End Sub

Private Sub LBMenu_Change()
    Label1(1).Caption = "����¹����ѵԢ����������͹��ѧ"
Select Case LBMenu.Tag
             Case "1"
                        Label1(3).Caption = "���ʷ��Թ :"
                        cb_Land_Change_ID.Clear
                        cb_Land_Change.Clear
                        cb_Land_Change_ID.AddItem ""
                        cb_Land_Change.AddItem ""
                        Set Query = Globle_Connective.Execute("sp_land_change2", , adCmdStoredProc)
                        If Query.RecordCount > 0 Then
                            Query.MoveFirst
                            Do While Not Query.EOF
                                  cb_Land_Change_ID.AddItem Query.Fields("LAND_CHANGE_ID").Value
                                  cb_Land_Change.AddItem Query.Fields("LAND_CHANGE_DETAILS").Value
                                  Query.MoveNext
                            Loop
                        End If
                        cb_Land_Change.Visible = True
                        cb_Land_Change.ListIndex = 0
                        cb_Land_Change_ID.ListIndex = 0
              Case "2"
                        Label1(3).Caption = "�����ç���͹ :"
                        cb_Land_Change_ID.Clear
                        cb_Land_Change.Clear
                        cb_Land_Change_ID.AddItem ""
                        cb_Land_Change.AddItem ""
                        Set Query = Globle_Connective.Execute("sp_building_change2", , adCmdStoredProc)
                        If Query.RecordCount > 0 Then
                            Query.MoveFirst
                            Do While Not Query.EOF
                                  cb_Land_Change_ID.AddItem Query.Fields("BUILDING_CHANGE_ID").Value
                                  cb_Land_Change.AddItem Query.Fields("BUILDING_CHANGE_DETAILS").Value
                                  Query.MoveNext
                            Loop
                        End If
                        cb_Land_Change.Visible = True
                        cb_Land_Change.ListIndex = 0
                        cb_Land_Change_ID.ListIndex = 0
              Case "3"
                        Label1(3).Caption = "���ʻ��� :"
                        cb_Land_Change_ID.Clear
                        cb_Land_Change.Clear
                        cb_Land_Change_ID.AddItem ""
                        cb_Land_Change.AddItem ""
                        Set Query = Globle_Connective.Execute("sp_signboard_change2", , adCmdStoredProc)
                        If Query.RecordCount > 0 Then
                            Query.MoveFirst
                            Do While Not Query.EOF
                                  cb_Land_Change_ID.AddItem Query.Fields("SIGNBOARD_CHANGE_ID").Value
                                  cb_Land_Change.AddItem Query.Fields("SIGNBOARD_CHANGE_DETAILS").Value
                                  Query.MoveNext
                            Loop
                        End If
                        cb_Land_Change.Visible = True
                        cb_Land_Change.ListIndex = 0
                        cb_Land_Change_ID.ListIndex = 0
              Case "4"
                       Label1(3).Caption = "�������/�Ţ��� :"
End Select
Txt_UserName.Text = Empty
Txt_LICENSE_NO.Text = Empty
Grid_Story.Clear
Grid_Story.Rows = 2
Grid_Story.FixedCols = 0
Call SetHeadGrid_Story
End Sub

Private Sub Txt_UserName_DblClick()
        Txt_UserName.Text = Empty
End Sub

Private Sub Txt_UserName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Txt_LICENSE_NO.SetFocus
End Sub
