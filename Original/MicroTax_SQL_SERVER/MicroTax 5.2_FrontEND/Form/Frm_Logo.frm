VERSION 5.00
Begin VB.Form Frm_Logo 
   BackColor       =   &H80000008&
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   5640
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7650
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   222
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Frm_Logo.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Logo.frx":151A
   ScaleHeight     =   5640
   ScaleWidth      =   7650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "52"
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   915
      Left            =   0
      ScaleHeight     =   915
      ScaleWidth      =   3435
      TabIndex        =   4
      Top             =   5670
      Visible         =   0   'False
      Width           =   3435
      Begin VB.TextBox Text1 
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   5895
         HideSelection   =   0   'False
         Index           =   1
         Left            =   1470
         Locked          =   -1  'True
         MousePointer    =   1  'Arrow
         MultiLine       =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Text            =   "Frm_Logo.frx":10C0A
         Top             =   330
         Width           =   1965
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAB870&
         Height          =   5895
         HideSelection   =   0   'False
         Index           =   2
         Left            =   0
         Locked          =   -1  'True
         MousePointer    =   1  'Arrow
         MultiLine       =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Text            =   "Frm_Logo.frx":10DDE
         Top             =   330
         Width           =   1395
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "MicroTax Software"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   30
         TabIndex        =   7
         Top             =   30
         Width           =   3375
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   7020
      Top             =   4290
   End
   Begin VB.Label Lb_Product 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Unknow Product"
      ForeColor       =   &H000080FF&
      Height          =   195
      Left            =   5670
      TabIndex        =   11
      Top             =   6390
      Width           =   1590
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������Ե�ѳ�� MicroTax    :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   1
      Left            =   3480
      TabIndex        =   10
      Top             =   6390
      Width           =   2145
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Ţ��Ե�ѳ�� MicroTax  :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   0
      Left            =   3480
      TabIndex        =   9
      Top             =   6180
      Width           =   2145
   End
   Begin VB.Label Lb_LicenceMicroTax 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0000-0000-0000-0000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   5670
      TabIndex        =   8
      Top             =   6180
      Width           =   1575
   End
   Begin VB.Label Lb_LicenceCadcorp 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "no licence"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   5670
      TabIndex        =   3
      Top             =   5970
      Width           =   735
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "������Ե�ѳ�� : EnviTec"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3480
      TabIndex        =   2
      Top             =   5730
      Width           =   1695
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Ţ��Ե�ѳ�� Cadcorp   :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3480
      TabIndex        =   1
      Top             =   5970
      Width           =   2130
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lnitializing Program..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0005CDF8&
      Height          =   195
      Left            =   5430
      TabIndex        =   0
      Top             =   3720
      Width           =   1950
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00E0E0E0&
      X1              =   0
      X2              =   9420
      Y1              =   5640
      Y2              =   5640
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00404040&
      BorderWidth     =   2
      X1              =   0
      X2              =   9420
      Y1              =   5640
      Y2              =   5640
   End
End
Attribute VB_Name = "Frm_Logo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long) As Long
Private Declare Function GetDriveType Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String) As Long
'Private Declare Function ReleaseCapture Lib "user32" () As Long
'Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, IParam As Long) As Long
'################### Register Edit
'Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
'Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
'Private Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
'Private Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
'Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As Long
'Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As Long
'Private Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As Long

'Private Type SECURITY_ATTRIBUTES
'                        nLength As Long
'                        lpSecurityDescriptor As Long
'                        bInheritHandle As Long
'End Type
Private Enum T_KeyClasses
                        HKEY_CLASSES_ROOT = &H80000000
                        HKEY_CURRENT_CONFIG = &H80000005
                        HKEY_CURRENT_USER = &H80000001
                        HKEY_LOCAL_MACHINE = &H80000002
                        HKEY_USERS = &H80000003
End Enum
 Const SYNCHRONIZE = &H100000
 Const STANDARD_RIGHTS_ALL = &H1F0000
' Const KEY_QUERY_VALUE = &H1
' Const KEY_SET_VALUE = &H2
' Const KEY_CREATE_LINK = &H20
' Const KEY_CREATE_SUB_KEY = &H4
' Const KEY_ENUMERATE_SUB_KEYS = &H8
 Const KEY_EVENT = &H1
' Const KEY_NOTIFY = &H10
 Const READ_CONTROL = &H20000
 Const STANDARD_RIGHTS_READ = (READ_CONTROL)
 Const STANDARD_RIGHTS_WRITE = (READ_CONTROL)
 Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))
 Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
 Const KEY_EXECUTE = (KEY_READ)
 Const KEY_WRITE = ((STANDARD_RIGHTS_WRITE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY) And (Not SYNCHRONIZE))
' Const REG_BINARY = 3
 Const REG_CREATED_NEW_KEY = &H1
' Const REG_DWORD = 4
' Const REG_DWORD_BIG_ENDIAN = 5
' Const REG_DWORD_LITTLE_ENDIAN = 4
' Const REG_EXPAND_SZ = 2
 Const REG_FULL_RESOURCE_DESCRIPTOR = 9
' Const REG_LINK = 6
' Const REG_MULTI_SZ = 7
' Const REG_NONE = 0
' Const REG_SZ = 1
 Const REG_NOTIFY_CHANGE_ATTRIBUTES = &H2
 Const REG_NOTIFY_CHANGE_LAST_SET = &H4
 Const REG_NOTIFY_CHANGE_NAME = &H1
 Const REG_NOTIFY_CHANGE_SECURITY = &H8
 Const REG_OPTION_BACKUP_RESTORE = 4
 Const REG_OPTION_CREATE_LINK = 2
 Const REG_OPTION_NON_VOLATILE = 0
 Const REG_OPTION_RESERVED = 0
 Const REG_OPTION_VOLATILE = 1
 Const REG_LEGAL_CHANGE_FILTER = (REG_NOTIFY_CHANGE_NAME Or REG_NOTIFY_CHANGE_ATTRIBUTES Or REG_NOTIFY_CHANGE_LAST_SET Or REG_NOTIFY_CHANGE_SECURITY)
 Const REG_LEGAL_OPTION = (REG_OPTION_RESERVED Or REG_OPTION_NON_VOLATILE Or REG_OPTION_VOLATILE Or REG_OPTION_CREATE_LINK Or REG_OPTION_BACKUP_RESTORE)

Private Const STANDARD = "2918"      'series key1
Private Const VIEWER = "4312"            'series key2
Private Const MANAGER = "5315"        'series key3
Private Const MODELLER = "7428"     'series key4

Const BETWEEN = 440
Dim KeyProvide As String

Private Function Unlock_Key(LICENCE As String) As Boolean
Dim i As Byte, j As Byte
Dim Degoust As Long, DIGIT1 As Integer, DIGIT2 As Integer, DIGIT3 As Integer
Dim ECDIGIT1 As Integer, ECDIGIT2 As Integer, ECDIGIT3 As Integer
                                  
                                  DIGIT1 = Mid$(LICENCE, 5, 4)
                                  DIGIT2 = Mid$(LICENCE, 9, 4)
                                  DIGIT3 = Mid$(LICENCE, 13, 4)

For j = 1 To 3
               Degoust = 0
   For i = 1 To Len(KeyProvide)
              Select Case j
                            Case 1
                                Degoust = Degoust + ((Asc(Mid$(KeyProvide, i, 1)) + i) - (Len(KeyProvide)))
                            Case 2
                                Degoust = Degoust + ((Asc(Mid$(KeyProvide, i, 1)) - i) + (Len(KeyProvide)))
                            Case 3
                                Degoust = Degoust + ((Asc(Mid$(KeyProvide, i, 1)) - i) - (Len(KeyProvide)))
                End Select
          Next i
              Select Case j
                            Case 1
                                     Select Case TProduct
                                                  Case "STANDARD"
                                                            Degoust = Degoust + 10
                                                            ECDIGIT1 = Degoust Xor STANDARD
                                                  Case "VIEWER"
                                                            Degoust = Degoust + 20
                                                            ECDIGIT1 = Degoust Xor VIEWER
                                                   Case "MANAGER"
                                                            Degoust = Degoust + 30
                                                            ECDIGIT1 = Degoust Xor MANAGER
                                                   Case "MODELLER"
                                                            Degoust = Degoust + 40
                                                            ECDIGIT1 = Degoust Xor MODELLER
                                      End Select
                            Case 2
                                     Select Case TProduct
                                                  Case "STANDARD"
                                                            Degoust = Degoust + 10
                                                            ECDIGIT2 = Degoust Xor STANDARD
                                                  Case "VIEWER"
                                                            Degoust = Degoust + 20
                                                            ECDIGIT2 = Degoust Xor VIEWER
                                                   Case "MANAGER"
                                                            Degoust = Degoust + 30
                                                            ECDIGIT2 = Degoust Xor MANAGER
                                                   Case "MODELLER"
                                                            Degoust = Degoust + 40
                                                            ECDIGIT2 = Degoust Xor MODELLER
                                      End Select
                            Case 3
                                     Select Case TProduct
                                                  Case "STANDARD"
                                                            Degoust = Degoust + 10
                                                            ECDIGIT3 = Degoust Xor STANDARD
                                                  Case "VIEWER"
                                                            Degoust = Degoust + 20
                                                            ECDIGIT3 = Degoust Xor VIEWER
                                                   Case "MANAGER"
                                                            Degoust = Degoust + 30
                                                            ECDIGIT3 = Degoust Xor MANAGER
                                                   Case "MODELLER"
                                                            Degoust = Degoust + 40
                                                            ECDIGIT3 = Degoust Xor MODELLER
                                      End Select
           End Select
Next j
  If ECDIGIT1 <> DIGIT1 Then
     Unlock_Key = False
             Exit Function
   Else
             Unlock_Key = True
  End If
    If ECDIGIT2 <> DIGIT2 Then
         Unlock_Key = False
             Exit Function
    Else
               Unlock_Key = True
  End If
    If ECDIGIT3 <> DIGIT3 Then
         Unlock_Key = False
             Exit Function
     Else
               Unlock_Key = True
  End If
End Function

Private Function SetRegValue(KeyRoot As T_KeyClasses, Path As String, sKey As String, NewValue As String) As Boolean
Dim hKey As Long, KeyValType As Long, keyValSize As Long, res As Long, X As Long
Dim KeyVal As String, tmpVal As String
Dim i As Integer

        res = RegOpenKeyEx(KeyRoot, Path, 0, KEY_ALL_ACCESS, hKey)
If res <> 0 Then GoTo Errore
        tmpVal = String(1024, 0)
        keyValSize = 1024
        res = RegQueryValueEx(hKey, sKey, 0, KeyValType, tmpVal, keyValSize)
Select Case res
Case 2
KeyValType = REG_SZ
Case Is <> 0
GoTo Errore
End Select
Select Case KeyValType
Case REG_SZ
tmpVal = NewValue
Case REG_DWORD
X = Val(NewValue)
tmpVal = vbNullString
For i = 0 To 3
tmpVal = tmpVal & Chr(X Mod 256)
X = X \ 256
Next
End Select
keyValSize = Len(tmpVal)
res = RegSetValueEx(hKey, sKey, 0, KeyValType, tmpVal, keyValSize)
If res <> 0 Then GoTo Errore
SetRegValue = True
RegCloseKey hKey
Exit Function
Errore:
SetRegValue = False
RegCloseKey hKey
End Function

' Private Sub WRITTEN_REGSVR32()
' Dim STR_HIT  As String
' Dim NUM_HIT As String
'                                   STR_HIT = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Series", "HIT")
'                               If Len(STR_HIT) = 8 Then
'                                                         NUM_HIT = SubsetHit(Left$(STR_HIT, 4), "DECODE")
'                                                         NUM_HIT = NUM_HIT & SubsetHit(Right$(STR_HIT, 4), "DECODE")
'                                                         If NUM_HIT = "" Then GoTo MESSAGGE_CONTROL
'                                                         NUM_HIT = CStr(CByte(NUM_HIT) + 1)
'                                                         THIT = NUM_HIT
'                                                      If CByte(NUM_HIT) > 59 Then
'                                                            Call SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Series", "STATUS", "ZERO")
'                                                     Else
'                                                           NUM_HIT = Format$(NUM_HIT, "0#")
'                                                           STR_HIT = SubsetHit(Left$(NUM_HIT, 1), "ENCODE")
'                                                           STR_HIT = STR_HIT & SubsetHit(Right$(NUM_HIT, 1), "ENCODE")
'                                                          Call SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Series", "HIT", STR_HIT)
'                                                    End If
'                                Else
'                                                         GoTo MESSAGGE_CONTROL
'                                End If
'
'Exit Sub
'MESSAGGE_CONTROL:
'                                     MsgBox "Microtax Unknow Product ! // Corrupt of Register " & vbCrLf & vbCrLf & "     Please Contact Envitec Company", vbMsgBoxHelpButton, "Microtax Software Licence"
'                                     Set Frm_Logo = Nothing
'                                     End
'End Sub

Private Function GetRegValue(KeyRoot As T_KeyClasses, Path As String, sKey As String) As String
Dim hKey As Long, KeyValType As Long, keyValSize As Long, res As Long
Dim tmpVal As String, KeyVal As String
Dim i As Integer

On Error GoTo Errore:
res = RegOpenKeyEx(KeyRoot, Path, 0, KEY_ALL_ACCESS, hKey)
If res <> 0 Then GoTo Errore   '�������դ���
tmpVal = Space(255)
keyValSize = 255
res = RegQueryValueEx(hKey, sKey, 0, KeyValType, ByVal tmpVal, keyValSize)
If res <> 0 Then GoTo Errore
If (Asc(Mid(tmpVal, keyValSize, 1)) = 0) Then
tmpVal = Left(tmpVal, keyValSize - 1)
Else
tmpVal = Left(tmpVal, keyValSize)
End If
Select Case KeyValType
Case REG_SZ
KeyVal = tmpVal
Case REG_DWORD
For i = Len(tmpVal) To 1 Step -1
KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, i, 1)))
Next
KeyVal = Format("&h" + KeyVal)
End Select
    GetRegValue = KeyVal
    RegCloseKey hKey
Exit Function

Errore:
GetRegValue = vbNullString
RegCloseKey hKey
End Function

Private Sub Form_Click()
  Unload Me
End Sub

Private Sub Form_Load()
Dim VolName As String, FSys As String, erg As Long
Dim VolNumber As Long, MCM As Long, FSF As Long
Dim Drive As String, DriveType As Long
Dim TLicence As String
Dim TStatus As String, MESSAGE_LOOP As String
Dim TVersionCompare As String
Dim i As Byte

VolName = Space(127)
FSys = Space(127)
'Drive = "C:\"
Drive = Left$(App.Path, 3)
DriveType = GetDriveType(Drive)
erg = GetVolumeInformation(Drive, VolName, 127, VolNumber, MCM, FSF, FSys, 127)

KeyProvide = Hex$(Abs(VolNumber))
Label10.Caption = "������Ե�ѳ�� : " & ReadINI("register owner", "section")
    Exit Sub
                For i = 1 To Len(Me.Tag)
                       TVersionCompare = TVersionCompare & SubsetHit(Mid(Me.Tag, i, 1), "ENCODE")
                Next i
                If Trim$(TVersionCompare) <> Trim$(GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Series", "ADVANCE")) Then
                     MESSAGE_LOOP = "                                      Install Uncompatible " & vbCrLf & vbCrLf & "Microtax product is incorrect version ! Please Reinstall product try again"
                     GoTo MESSAGE_CONTROL
                End If

                              TLicence = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Licence")
                              TProduct = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Product")
                              TStatus = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Series", "STATUS")
                              Lb_Product.Caption = LCase(TProduct)
                              TCampaign = GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microtax", "Campaign")
                               If TLicence <> Empty Then Lb_LicenceMicroTax.Caption = Mid$(TLicence, 1, 4) & "-" & Mid$(TLicence, 5, 4) & "-" & Mid$(TLicence, 9, 4) & "-" & Mid$(TLicence, 13, 4)
                                    If TLicence <> Empty Or TProduct <> Empty Then
                                                            Select Case UCase(TProduct)
                                                                        Case "DEMO"
                                                                                    If Mid$(TLicence, 3, 2) <> "00" Then
'                                                                                            Select Case UCase(TStatus)
'                                                                                                         Case "ZERO"
'                                                                                                                MESSAGE_LOOP = "Microtax Demo Version Expire of 59 HIT"
'                                                                                                                GoTo MESSAGE_CONTROL
'                                                                                                         Case "PASS"
'                                                                                                                  Call WRITTEN_REGSVR32
'                                                                                                         Case Else
'                                                                                                                MESSAGE_LOOP = "Microtax Demo Version Expire of 59 HIT"
'                                                                                                                GoTo MESSAGE_CONTROL
'                                                                                             End Select
'                                                                                    Else
                                                                                           MESSAGE_LOOP = "Product And Licence Not Correction ! Please to register try again"
                                                                                           GoTo MESSAGE_CONTROL
                                                                                    End If
                                                                        Case "STANDARD"
                                                                                    If Mid$(TLicence, 3, 2) = "10" Then
                                                                                            If Unlock_Key(TLicence) = False Then
                                                                                                       MESSAGE_LOOP = "Microtax Invalid Software Licence !!!"
                                                                                                       GoTo MESSAGE_CONTROL
                                                                                            End If
                                                                                    Else
                                                                                           MESSAGE_LOOP = "Product And Licence Not Equation ! Please to register try again"
                                                                                           GoTo MESSAGE_CONTROL
                                                                                    End If
                                                                        Case "VIEWER"
                                                                                    If Mid$(TLicence, 3, 2) = "20" Then
                                                                                            If Unlock_Key(TLicence) = False Then
                                                                                                       MESSAGE_LOOP = "Microtax Invalid Software Licence !!!"
                                                                                                       GoTo MESSAGE_CONTROL
                                                                                            End If
                                                                                    Else
                                                                                           MESSAGE_LOOP = "Product And Licence Not Equation ! Please to register try again"
                                                                                           GoTo MESSAGE_CONTROL
                                                                                    End If
                                                                        Case "MANAGER"
                                                                                    If Mid$(TLicence, 3, 2) = "30" Then
                                                                                            If Unlock_Key(TLicence) = False Then
                                                                                                       MESSAGE_LOOP = "Microtax Invalid Software Licence !!!"
                                                                                                       GoTo MESSAGE_CONTROL
                                                                                            End If
                                                                                    Else
                                                                                           MESSAGE_LOOP = "Product And Licence Not Equation ! Please to register try again"
                                                                                           GoTo MESSAGE_CONTROL
                                                                                    End If
                                                                        Case "MODELLER"
                                                                                    If Mid$(TLicence, 3, 2) = "40" Then
                                                                                            If Unlock_Key(TLicence) = False Then
                                                                                                       MESSAGE_LOOP = "Microtax Invalid Software Licence"
                                                                                                       GoTo MESSAGE_CONTROL
                                                                                            End If
                                                                                    Else
                                                                                           MESSAGE_LOOP = "Product And Licence Not Equation ! Please to register try again"
                                                                                           GoTo MESSAGE_CONTROL
                                                                                    End If
                                                               End Select
                                      Else
                                                                MESSAGE_LOOP = "Microtax Unknow Product ! // Corrupt of Register " & vbCrLf & vbCrLf & "     Please Contact Envitec Company"
                                                               GoTo MESSAGE_CONTROL
                                      End If
                                    
Exit Sub
MESSAGE_CONTROL:
                              MsgBox MESSAGE_LOOP, vbMsgBoxHelpButton, "Microtax Software Licence"
                              Set Frm_Login = Nothing
                              Set Clone_Form = Nothing
                              End
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
Timer1.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set Frm_Logo = Nothing
End Sub

Private Sub Label2_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
Timer1.Enabled = False
End Sub

Private Sub Text1_GotFocus(Index As Integer)
Text1(Index).SelLength = 0
End Sub

Private Sub Text1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
Timer1.Enabled = False
End Sub

Private Sub Timer1_Timer()
Label2.Top = Label2.Top - 15
Text1(1).Top = (Text1(1).Top - 15)
Text1(2).Top = (Text1(2).Top - 15)
   If Text1(2).Top + Text1(2).Height < 0 Then
          Label2.Top = 1980
      Text1(2).Top = 1980 + BETWEEN
      Text1(1).Top = 1980 + BETWEEN
   End If
End Sub
