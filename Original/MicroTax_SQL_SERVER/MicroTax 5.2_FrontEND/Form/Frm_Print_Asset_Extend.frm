VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Print_Asset_Extend 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7875
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Print_Asset_Extend.frx":0000
   ScaleHeight     =   4455
   ScaleWidth      =   7875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cbMonth1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_Print_Asset_Extend.frx":88EA2
      Left            =   3330
      List            =   "Frm_Print_Asset_Extend.frx":88ECD
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   2100
      Width           =   1890
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00D6D6D6&
      Height          =   405
      Left            =   5610
      Picture         =   "Frm_Print_Asset_Extend.frx":88F41
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3990
      Width           =   1485
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   4470
      TabIndex        =   0
      Top             =   900
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196612
      OrigLeft        =   6405
      OrigTop         =   900
      OrigRight       =   6660
      OrigBottom      =   1230
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65537
      Enabled         =   -1  'True
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ш���͹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   2370
      TabIndex        =   4
      Top             =   2160
      Width           =   885
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   3090
      TabIndex        =   3
      Top             =   960
      Width           =   645
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2548"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3810
      TabIndex        =   2
      Top             =   900
      Width           =   660
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00E0E0E0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00404040&
      Height          =   2625
      Index           =   0
      Left            =   690
      Top             =   1320
      Width           =   6405
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00404040&
      Height          =   615
      Index           =   1
      Left            =   690
      Top             =   750
      Width           =   6405
   End
End
Attribute VB_Name = "Frm_Print_Asset_Extend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Btn_Print_Click()
 On Error GoTo ErrHlde
Dim strSQL As String

 With Clone_Form.CTReport
        .Reset
        .Connect = "DSN = Microtax;UID = " & ini_Username & ";PWD = " & ini_Password & ";DSQ = Administration"
        .DiscardSavedData = True
        If fPrint = 0 Then
                If LenB(Trim$(cbMonth1.Text)) > 0 Then
                        strSQL = " ({ASSETDATA_EXTEND.ASSET_YEAR}=" & Lb_Year.Caption & ")" & " AND (Month({ASSETDATA_EXTEND.PAY_DATE})=" & cbMonth1.ListIndex & ")"
                        .Formulas(0) = "Month_Fix='" & cbMonth1.Text & "  ��Шӻ� " & Lb_Year.Caption & "'"
                        .ReportFileName = App.Path & "\Report\Asset_Extend.rpt"
                End If
        Else
                strSQL = " ({PETITIONDATA.PETITION_YEAR}=" & Lb_Year.Caption & ")"
                If cbMonth1.ListIndex > 0 Then
                        If cbMonth1.ListIndex = 1 Then
                                strSQL = strSQL & " AND ({PETITIONDATA.PETITION_PRACTICE}=TRUE)"
                        ElseIf cbMonth1.ListIndex = 2 Then
                                strSQL = strSQL & " AND ({PETITIONDATA.PETITION_PRACTICE}=FALSE)"
                        End If
                End If
                .Formulas(0) = "txt_year='" & Lb_Year.Caption & "'"
                .ReportFileName = App.Path & "\Report\Petition.rpt"
        End If
        .SelectionFormula = strSQL
        .WindowShowPrintSetupBtn = True
        .Destination = crptToWindow
        .WindowState = crptMaximized
        .WindowShowExportBtn = True
        .Action = 1
    End With
        Exit Sub
ErrHlde:
        MsgBox Err.Description & "  �������ö�ʴ� Report ! ", vbCritical, "����͹ !"
End Sub

Private Sub Form_Load()
        Lb_Year.Caption = Right$(Date, 4)
        cbMonth1.ListIndex = 0
        If fPrint = 0 Then
                Label2(2).Caption = "��Ш���͹ : "
                With cbMonth1
                        .Clear
                        .AddItem ""
                        .AddItem "���Ҥ�"
                        .AddItem "����Ҿѹ��"
                        .AddItem "�չҤ�"
                        .AddItem "����¹"
                        .AddItem "����Ҥ�"
                        .AddItem "�Զع�¹"
                        .AddItem "�á�Ҥ�"
                        .AddItem "�ԧ�Ҥ�"
                        .AddItem "�ѹ��¹"
                        .AddItem "���Ҥ�"
                        .AddItem "��Ȩԡ�¹"
                        .AddItem "�ѹ�Ҥ�"
                        .ListIndex = 0
                End With
        Else
                Label2(2).Caption = "��û�Ժѵ� :"
                With cbMonth1
                        .Clear
                        .AddItem ""
                        .AddItem "���������ҧ��Ǩ�ͺ"
                        .AddItem "��Ǩ�ͺ����"
                        .ListIndex = 0
                End With
        End If
End Sub


