VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Frm_Config 
   BackColor       =   &H00000000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   " MicroTax Configuration System "
   ClientHeight    =   5250
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7500
   Icon            =   "Frm_Config.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_Config.frx":151A
   ScaleHeight     =   5250
   ScaleWidth      =   7500
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   3885
      Left            =   180
      TabIndex        =   1
      Top             =   810
      Width           =   7185
      _ExtentX        =   12674
      _ExtentY        =   6853
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      BackColor       =   14079702
      TabCaption(0)   =   "  ��˹�ʶҹ�����"
      TabPicture(0)   =   "Frm_Config.frx":FB3A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Shape3(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Shape1(4)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Shape1(3)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Shape1(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Shape1(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Shape1(1)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label2(7)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label2(6)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label2(5)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label2(4)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label2(3)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label2(2)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label2(1)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label2(0)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Shape2(0)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Label1(0)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Shape2(1)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Label3"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Shape1(8)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Shape1(9)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Shape1(10)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Txt_Cfg_Position"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Txt_Cfg_Name"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Txt_Cfg_Tel"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Txt_Cfg_Add"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Txt_Cfg_Section"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Cmb_Province"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Cmb_Amphoe"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "Cmb_Tambon"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "Cmb_Province_ID"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "Cmb_Tambon_ID"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "Cmb_Amphoe_ID"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).ControlCount=   32
      TabCaption(1)   =   "  ��˹�Path "
      TabPicture(1)   =   "Frm_Config.frx":FC94
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chk_Payment"
      Tab(1).Control(1)=   "chk_Accept"
      Tab(1).Control(2)=   "chkPrint_Perform"
      Tab(1).Control(3)=   "Btn_Path(2)"
      Tab(1).Control(4)=   "Btn_Path(1)"
      Tab(1).Control(5)=   "Txt_PathPicture"
      Tab(1).Control(6)=   "Txt_PathMap"
      Tab(1).Control(7)=   "Label2(11)"
      Tab(1).Control(8)=   "Shape2(2)"
      Tab(1).Control(9)=   "Shape1(7)"
      Tab(1).Control(10)=   "Shape1(6)"
      Tab(1).Control(11)=   "Label2(9)"
      Tab(1).Control(12)=   "Label2(8)"
      Tab(1).Control(13)=   "Shape3(1)"
      Tab(1).ControlCount=   14
      TabCaption(2)   =   "  ����"
      TabPicture(2)   =   "Frm_Config.frx":1022E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Shape3(2)"
      Tab(2).Control(1)=   "Label4(1)"
      Tab(2).Control(2)=   "Label4(2)"
      Tab(2).Control(3)=   "Label4(4)"
      Tab(2).Control(4)=   "Label4(0)"
      Tab(2).Control(5)=   "Shape1(11)"
      Tab(2).Control(6)=   "Shape1(12)"
      Tab(2).Control(7)=   "Shape1(13)"
      Tab(2).Control(8)=   "cmdReset"
      Tab(2).Control(9)=   "cmdReset2"
      Tab(2).Control(10)=   "ProgressBar1"
      Tab(2).Control(11)=   "cmd_UpdateTaxLand"
      Tab(2).Control(12)=   "Text1"
      Tab(2).Control(13)=   "cmd_Show_error"
      Tab(2).ControlCount=   14
      TabCaption(3)   =   "��駤�� SQL Server"
      TabPicture(3)   =   "Frm_Config.frx":14B68
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmd_LookupIP"
      Tab(3).Control(1)=   "cboDB"
      Tab(3).Control(2)=   "txtPassword"
      Tab(3).Control(3)=   "txtUser"
      Tab(3).Control(4)=   "cmdRefresh"
      Tab(3).Control(5)=   "cboServer"
      Tab(3).Control(6)=   "Label7"
      Tab(3).Control(7)=   "Shape1(16)"
      Tab(3).Control(8)=   "Shape1(15)"
      Tab(3).Control(9)=   "Label6"
      Tab(3).Control(10)=   "Label5"
      Tab(3).Control(11)=   "Shape1(14)"
      Tab(3).Control(12)=   "lbServername"
      Tab(3).Control(13)=   "Shape1(5)"
      Tab(3).Control(14)=   "Shape3(3)"
      Tab(3).ControlCount=   15
      Begin VB.CommandButton cmd_LookupIP 
         BackColor       =   &H00E3DFE0&
         Height          =   375
         Left            =   -68520
         MaskColor       =   &H00FFFFFF&
         Picture         =   "Frm_Config.frx":14B84
         Style           =   1  'Graphical
         TabIndex        =   52
         ToolTipText     =   "Lookup IP"
         Top             =   1080
         Width           =   345
      End
      Begin VB.ComboBox cboDB 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -72750
         Sorted          =   -1  'True
         TabIndex        =   50
         Top             =   2280
         Width           =   3345
      End
      Begin VB.TextBox txtPassword 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         IMEMode         =   3  'DISABLE
         Left            =   -72750
         PasswordChar    =   "�"
         TabIndex        =   45
         Top             =   1920
         Width           =   3345
      End
      Begin VB.TextBox txtUser 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   -72750
         TabIndex        =   44
         Top             =   1560
         Width           =   3345
      End
      Begin VB.CommandButton cmdRefresh 
         BackColor       =   &H00E3DFE0&
         Caption         =   "Refresh"
         Height          =   375
         Left            =   -69330
         TabIndex        =   47
         Top             =   1080
         Width           =   735
      End
      Begin VB.ComboBox cboServer 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -72750
         Sorted          =   -1  'True
         TabIndex        =   43
         Top             =   1110
         Width           =   3345
      End
      Begin VB.CommandButton cmd_Show_error 
         BackColor       =   &H00E3DFE0&
         Height          =   315
         Left            =   -69510
         MaskColor       =   &H00FFFFFF&
         Picture         =   "Frm_Config.frx":1510E
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   1590
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox Text1 
         Height          =   345
         Left            =   -68760
         TabIndex        =   41
         Top             =   840
         Visible         =   0   'False
         Width           =   405
      End
      Begin VB.CommandButton cmd_UpdateTaxLand 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Update"
         Height          =   315
         Left            =   -70260
         TabIndex        =   40
         Top             =   1590
         Visible         =   0   'False
         Width           =   705
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   225
         Left            =   -74940
         TabIndex        =   39
         Top             =   3570
         Visible         =   0   'False
         Width           =   7065
         _ExtentX        =   12462
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.CommandButton cmdReset2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Reset"
         Height          =   315
         Left            =   -70260
         TabIndex        =   36
         Top             =   1200
         Visible         =   0   'False
         Width           =   705
      End
      Begin VB.CommandButton cmdReset 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Reset"
         Height          =   315
         Left            =   -70260
         TabIndex        =   33
         Top             =   810
         Visible         =   0   'False
         Width           =   705
      End
      Begin VB.CheckBox chk_Payment 
         BackColor       =   &H00E3DFE0&
         Caption         =   "�͡��§ҹ���Ф������ (�����)"
         Height          =   195
         Left            =   -73290
         TabIndex        =   31
         Top             =   3030
         Visible         =   0   'False
         Width           =   3555
      End
      Begin VB.CheckBox chk_Accept 
         BackColor       =   &H00E3DFE0&
         Caption         =   "�͡��§ҹ��û����Թ����"
         Height          =   195
         Left            =   -73290
         TabIndex        =   30
         Top             =   2700
         Visible         =   0   'False
         Width           =   2985
      End
      Begin VB.CheckBox chkPrint_Perform 
         BackColor       =   &H00E3DFE0&
         Caption         =   "�͡��§ҹ���Ẻ����"
         Height          =   195
         Left            =   -73290
         TabIndex        =   29
         Top             =   2370
         Visible         =   0   'False
         Width           =   2985
      End
      Begin VB.CommandButton Btn_Path 
         BackColor       =   &H00E3DFE0&
         Height          =   315
         Index           =   2
         Left            =   -68580
         MaskColor       =   &H00FFFFFF&
         Picture         =   "Frm_Config.frx":15698
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   1020
         Width           =   495
      End
      Begin VB.CommandButton Btn_Path 
         BackColor       =   &H00E3DFE0&
         Height          =   315
         Index           =   1
         Left            =   -68580
         MaskColor       =   &H00FFFFFF&
         Picture         =   "Frm_Config.frx":15C22
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox Txt_PathPicture 
         Appearance      =   0  'Flat
         BackColor       =   &H00E3DFE0&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   -74010
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   1080
         Width           =   5355
      End
      Begin VB.TextBox Txt_PathMap 
         Appearance      =   0  'Flat
         BackColor       =   &H00E3DFE0&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   -74010
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   780
         Width           =   5355
      End
      Begin VB.ComboBox Cmb_Amphoe_ID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   12
         TabStop         =   0   'False
         Text            =   "Cmb_Amphoe_ID"
         Top             =   1050
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.ComboBox Cmb_Tambon_ID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   11
         TabStop         =   0   'False
         Text            =   "Cmb_Tambon_ID"
         Top             =   1440
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.ComboBox Cmb_Province_ID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   10
         TabStop         =   0   'False
         Text            =   "Cmb_Province_ID"
         Top             =   660
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.ComboBox Cmb_Tambon 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2100
         TabIndex        =   9
         Top             =   1410
         Width           =   3015
      End
      Begin VB.ComboBox Cmb_Amphoe 
         Height          =   315
         Left            =   2100
         TabIndex        =   8
         Top             =   1020
         Width           =   3015
      End
      Begin VB.ComboBox Cmb_Province 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2100
         TabIndex        =   7
         Top             =   660
         Width           =   3015
      End
      Begin VB.TextBox Txt_Cfg_Section 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   1200
         TabIndex        =   6
         Top             =   2190
         Width           =   5385
      End
      Begin VB.TextBox Txt_Cfg_Add 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   1200
         TabIndex        =   5
         Top             =   2460
         Width           =   5385
      End
      Begin VB.TextBox Txt_Cfg_Tel 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   1200
         TabIndex        =   4
         Top             =   2730
         Width           =   5385
      End
      Begin VB.TextBox Txt_Cfg_Name 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   1200
         TabIndex        =   3
         Top             =   3090
         Width           =   5385
      End
      Begin VB.TextBox Txt_Cfg_Position 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   1200
         TabIndex        =   2
         Top             =   3360
         Width           =   5385
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Database :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -73620
         TabIndex        =   51
         Top             =   2370
         Width           =   780
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   375
         Index           =   16
         Left            =   -72780
         Top             =   2250
         Width           =   3405
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   15
         Left            =   -72780
         Top             =   1890
         Width           =   3405
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Password :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -73620
         TabIndex        =   49
         Top             =   1950
         Width           =   780
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "User name :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -73695
         TabIndex        =   48
         Top             =   1590
         Width           =   855
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   14
         Left            =   -72780
         Top             =   1530
         Width           =   3405
      End
      Begin VB.Label lbServername 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Server Name :"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   -73860
         TabIndex        =   46
         Top             =   1170
         Width           =   1020
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   375
         Index           =   5
         Left            =   -72780
         Top             =   1080
         Width           =   3405
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00E3DFE0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00B2ACA5&
         Height          =   3495
         Index           =   3
         Left            =   -74970
         Top             =   330
         Width           =   7125
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B2ACA5&
         Height          =   285
         Index           =   13
         Left            =   -74610
         Top             =   1620
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B2ACA5&
         Height          =   285
         Index           =   12
         Left            =   -74610
         Top             =   1230
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B2ACA5&
         Height          =   285
         Index           =   11
         Left            =   -74610
         Top             =   840
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӡ�û�Ѻ������շ��Թ���� (�����Թ���к�)"
         Height          =   195
         Index           =   0
         Left            =   -74550
         TabIndex        =   38
         Top             =   1680
         Visible         =   0   'False
         Width           =   3225
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ��Ҹ��������͹حҵ :"
         Height          =   195
         Index           =   4
         Left            =   -74550
         TabIndex        =   37
         Top             =   1740
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��˹�������ӹǳ��§ҹ��èѴ�������Ẻ 2/1 ����"
         Height          =   195
         Index           =   2
         Left            =   -74550
         TabIndex        =   35
         Top             =   1290
         Visible         =   0   'False
         Width           =   4020
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��˹�������ӹǳ��§ҹ��û�Ѻ������Ẻ 2 ����"
         Height          =   195
         Index           =   1
         Left            =   -74550
         TabIndex        =   34
         Top             =   870
         Visible         =   0   'False
         Width           =   3600
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E3DFE0&
         Caption         =   "����͡��§ҹ��������"
         ForeColor       =   &H00D54600&
         Height          =   195
         Index           =   11
         Left            =   -73860
         TabIndex        =   32
         Top             =   2040
         Visible         =   0   'False
         Width           =   1920
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00B2ACA5&
         Height          =   1215
         Index           =   2
         Left            =   -74040
         Shape           =   4  'Rounded Rectangle
         Top             =   2160
         Visible         =   0   'False
         Width           =   5415
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00E3DFE0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00B2ACA5&
         Height          =   3495
         Index           =   2
         Left            =   -74970
         Top             =   330
         Width           =   7125
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   375
         Index           =   10
         Left            =   2070
         Top             =   1380
         Width           =   3075
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   375
         Index           =   9
         Left            =   2070
         Top             =   990
         Width           =   3075
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   375
         Index           =   8
         Left            =   2070
         Top             =   630
         Width           =   3075
      End
      Begin VB.Label Label3 
         BackColor       =   &H00E3DFE0&
         Caption         =   " �к���������´"
         ForeColor       =   &H00D54600&
         Height          =   195
         Left            =   450
         TabIndex        =   28
         Top             =   1860
         Width           =   1155
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00B2ACA5&
         Height          =   1845
         Index           =   1
         Left            =   240
         Shape           =   4  'Rounded Rectangle
         Top             =   1950
         Width           =   6585
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E3DFE0&
         Caption         =   " ���͡����� "
         ForeColor       =   &H00D54600&
         Height          =   195
         Index           =   0
         Left            =   420
         TabIndex        =   27
         Top             =   420
         Width           =   735
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00B2ACA5&
         Height          =   1275
         Index           =   0
         Left            =   240
         Shape           =   4  'Rounded Rectangle
         Top             =   540
         Width           =   6585
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B2ACA5&
         Height          =   285
         Index           =   7
         Left            =   -74040
         Top             =   1050
         Width           =   5415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B2ACA5&
         Height          =   285
         Index           =   6
         Left            =   -74040
         Top             =   750
         Width           =   5415
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ٻ�Ҿ"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   9
         Left            =   -74610
         TabIndex        =   23
         Top             =   1110
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ἱ���"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   8
         Left            =   -74580
         TabIndex        =   21
         Top             =   810
         Width           =   450
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѧ��Ѵ"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   1440
         TabIndex        =   20
         Top             =   750
         Width           =   465
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   1
         Left            =   1470
         TabIndex        =   19
         Top             =   1110
         Width           =   435
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ӻ�"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   2
         Left            =   1530
         TabIndex        =   18
         Top             =   1500
         Width           =   375
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "˹��§ҹ"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   3
         Left            =   390
         TabIndex        =   17
         Top             =   2160
         Width           =   690
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   4
         Left            =   750
         TabIndex        =   16
         Top             =   2460
         Width           =   300
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���Ѿ��"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   5
         Left            =   450
         TabIndex        =   15
         Top             =   2760
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ŧ���"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   6
         Left            =   480
         TabIndex        =   14
         Top             =   3090
         Width           =   570
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���˹�"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   7
         Left            =   480
         TabIndex        =   13
         Top             =   3390
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   1
         Left            =   1170
         Top             =   2160
         Width           =   5445
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00B99D7F&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   0
         Left            =   1170
         Top             =   2430
         Width           =   5445
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   2
         Left            =   1170
         Top             =   3060
         Width           =   5445
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   3
         Left            =   1170
         Top             =   2700
         Width           =   5445
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00B99D7F&
         Height          =   285
         Index           =   4
         Left            =   1170
         Top             =   3330
         Width           =   5445
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00E3DFE0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00B2ACA5&
         Height          =   3495
         Index           =   0
         Left            =   30
         Top             =   330
         Width           =   7125
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00E3DFE0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00B2ACA5&
         Height          =   3495
         Index           =   1
         Left            =   -74970
         Top             =   330
         Width           =   7125
      End
   End
   Begin VB.CommandButton cmdSave 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Save Setting"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5940
      MaskColor       =   &H00404000&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4830
      Width           =   1425
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   120
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "Frm_Config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Const BIF_RETURNONLYFSDIRS = 1
Private Const BIF_DONTGOBELOWDOMAIN = 2
Private Const MAX_PATH = 260
Private Const WSADESCRIPTION_LEN = 256
Private Const WSASYS_STATUS_LEN = 128

Private Declare Function SHBrowseForFolder Lib "shell32" (lpbi As BrowseInfo) As Long
Private Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Private Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long

'API Command to create a Data Source Name, not used in this example
Private Declare Function SQLCreateDataSource Lib "odbccp32.dll" (ByVal hWnd&, ByVal lpszDS$) As Boolean
'API to modify/Edit/Create a Data Source Name
Private Declare Function SQLConfigDataSource Lib "odbccp32.dll" (ByVal hWnd As Long, ByVal fRequest As Integer, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Boolean

Private Type HOSTENT
    h_name As Long
    h_aliases As Long
    h_addrtype As Integer
    h_length As Integer
    h_addr_list As Long
End Type

Private Type WSADATA
    wVersion As Integer
    wHighVersion As Integer
    szDescription(WSADESCRIPTION_LEN) As Byte
    szSystemStatus(WSASYS_STATUS_LEN) As Byte
    iMaxSockets As Integer
    iMaxUdpDg As Integer
    lpVendorInfo As Long
End Type
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function gethostbyname Lib "wsock32.dll" (ByVal szName As String) As Long
Private Declare Function WSAStartup Lib "wsock32.dll" (ByVal wVersionRequested As Integer, lpWSAData As WSADATA) As Long
Private Declare Function WSACleanup Lib "wsock32.dll" () As Integer
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Private Const ODBC_ADD_SYS_DSN = 4&
Private Type BrowseInfo
            hWndOwner As Long
            pIDLRoot As Long
            pszDisplayName As Long
            lpszTitle As Long
            ulFlags As Long
            lpfnCallback As Long
            lParam As Long
            iImage As Long
End Type
Dim RsLandData As ADODB.Recordset
Dim RsLandApply As ADODB.Recordset
Dim RsLandUse As ADODB.Recordset
Dim RsZoneTax As ADODB.Recordset

Private Function AvailableSQLServers() As String()
'***********************************************
'PURPOSE:       Returns array list name of all SQL Servers
'               on the network that are visible to the
'               machine
'
'RETURNS:       String array containing names of all
'               available SQL Servers (or an array with one
'               element containing empty string if no
'               SQL Servers are available/visible)
               
'REQUIRES:      Reference to Microsoft SQLDMO object library
'               VB6 Because array is returned
'               Assumes Option Base is not set to 1
'               If you don't have VB6, and/or Option Base 1
'               is set, it should not be very hard to modify
'               this code for you own purposes

'EXAMPLE:
'Dim sServers() As String
'Dim iCtr As Integer

'sServers = AvailableSQLServers
'If sServers(0) = "" Then
'    MsgBox "No SQL Servers Available"
'Else
'    For iCtr = 0 To UBound(sServers)
'       Debug.Print sServers(iCtr)
'   Next
'End If
'***********************************************
Dim oServer As New SQLDMO.Application
Dim oNameList As SQLDMO.NameList
Dim iElement As Integer
Dim sAns() As String
Dim lCtr As Long, lCount As Long

On Error GoTo ErrorHandler

ReDim sAns(0) As String
Set oNameList = oServer.ListAvailableSQLServers
With oNameList
    .Refresh
    lCount = .Count
    If lCount > 0 Then
        For lCtr = 1 To .Count
            iElement = IIf(sAns(0) = "", 0, UBound(sAns) + 1)
            ReDim Preserve sAns(iElement) As String
            sAns(iElement) = oNameList.Item(lCtr)
        Next
    End If
End With

AvailableSQLServers = sAns
Exit Function

ErrorHandler:
'Return array with one empty element on error
ReDim sAns(0) As String
AvailableSQLServers = sAns
End Function

Private Function ComputerName() As String
     
    ' Returns the name of the local computer.
    Dim BUFFER As String * 512, Length As Long
    Length = Len(BUFFER)
    If GetComputerName(BUFFER, Length) Then
        ' this API returns non-zero if successful,
        ' and modifies the length argument
        ComputerName = Left(BUFFER, Length)
    End If
     
End Function

Private Function LookupIPAddress(ByVal sHostName As String) As String

    Dim wsa As WSADATA
    Dim nRet As Long
    Dim nTemp As Long
    Dim bTemp(0 To 3) As Byte
    Dim sOut As String
    Dim he As HOSTENT
     
    'Initialize WinSock
    WSAStartup &H10, wsa
         
    'Attempt to lookup the host
    nRet = gethostbyname(sHostName)
     
    'If it failed, just return nothing
    If nRet = 0 Then
        sOut = ""
    Else
        'Take a look at the resulting hostent structure
        CopyMemory he, ByVal nRet, Len(he)
         
        'Are there atlest four bytes, then we have
        ' at least one address
        If he.h_length >= 4 Then
            'Copy the address out,
            CopyMemory nTemp, ByVal he.h_addr_list, 4
            CopyMemory bTemp(0), ByVal nTemp, 4
            ' and format it
            sOut = Format(bTemp(0)) & "." & Format(bTemp(1)) & "." _
               & Format(bTemp(2)) & "." & Format(bTemp(3))
        Else
            sOut = ""
        End If
End If
        WSACleanup
        LookupIPAddress = sOut
End Function

Private Sub SET_REFRESH()
If GBQueryProvince.RecordCount > 0 Then
        Cmb_Province.Clear
          GBQueryProvince.MoveFirst
    Do While Not GBQueryProvince.EOF
            Cmb_Province.AddItem GBQueryProvince.Fields("Province_Name").Value
            Cmb_Province_ID.AddItem GBQueryProvince.Fields("Province_ID").Value
            GBQueryProvince.MoveNext
    Loop
       Cmb_Province.ListIndex = 0
End If
End Sub

Private Sub Btn_Path_Click(Index As Integer)
Dim lpIDList As Long
Dim sBuffer As String
Dim tBrowseInfo As BrowseInfo

On Error GoTo ErrHandler
With CommonDialog1
    Select Case Index
                 Case 1
                                .DialogTitle = "���͡���Ἱ���  .SWD"
                                .CancelError = True
                                .InitDir = "C:\"
                                .Flags = cdlOFNHideReadOnly
                                .Filter = "All Files (*.*)|*.*|Swd Files(*.swd)|*.swd"     ' Set filters
                                .FilterIndex = 2
                                '.Action = 1
                                .ShowOpen
                                Txt_PathMap.Text = Empty
                                Txt_PathMap = .FileName
                Case 3
'                                .DialogTitle = "���͡���ҹ������  .mdb"
'                                .CancelError = True
'                                .InitDir = "C:\"
'                                .Flags = cdlOFNHideReadOnly
'                                .Filter = "All Files (*.*)|*.*|mdb Files(*.mdb)|*.mdb"     ' Set filters
'                                .FilterIndex = 2
                                '.Action = 1
'                                .ShowOpen
'                                 Txt_PathDatabase.Text = .FileName
                Case 2
                                        szTitle = "This is the title"
                            With tBrowseInfo
                                        .hWndOwner = Me.hWnd
                                        .lpszTitle = lstrcat(szTitle, "")
                                        .ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN
                            End With

                                    lpIDList = SHBrowseForFolder(tBrowseInfo)
                                    If (lpIDList) Then
                                            sBuffer = Space(MAX_PATH)
                                            SHGetPathFromIDList lpIDList, sBuffer
                                            sBuffer = Left(sBuffer, InStr(sBuffer, vbNullChar) - 1)
                                             Txt_PathPicture.Text = sBuffer
                                    End If
   End Select
End With
                         Exit Sub
ErrHandler:
End Sub


Private Sub cboDB_GotFocus()
On Error GoTo ErrgetDatabase
Dim oSQLServer As New SQLDMO.SQLServer
Dim i As Integer
'If txtUser.Text = "" And txtPassword.Text = "" Then
'      oSQLServer.LoginSecure = True
'End If
Me.MousePointer = 11
cboDB.Clear
oSQLServer.Connect cboServer.Text, txtUser.Text, txtPassword.Text
For i = 1 To oSQLServer.Databases.Count
    With oSQLServer.Databases(i)
        If Not .SystemObject Then
             cboDB.AddItem .Name
        End If
    End With
Next i
Me.MousePointer = 0
Exit Sub
ErrgetDatabase:
        Me.MousePointer = 0
        MsgBox Err.Description
End Sub

Private Sub Cmb_Amphoe_Click()
      Cmb_Amphoe_ID.ListIndex = Cmb_Amphoe.ListIndex
    If Cmb_Amphoe.Text <> Empty Then
       Cmb_Tambon.Clear
       Cmb_Tambon_ID.Clear
       
       GBQueryTambon.Filter = " AMPHOE_ID = '" & Cmb_Amphoe_ID.Text & "'"
            If GBQueryTambon.RecordCount > 0 Then
                        Cmb_Tambon.Clear
                        Cmb_Tambon_ID.Clear
                      GBQueryTambon.MoveFirst
                        Do While Not GBQueryTambon.EOF
                                    Cmb_Tambon.AddItem GBQueryTambon.Fields("Tambon_Name").Value
                                    Cmb_Tambon_ID.AddItem GBQueryTambon.Fields("Tambon_ID").Value
                                    GBQueryTambon.MoveNext
                        Loop
                                    Cmb_Tambon.ListIndex = 0
            End If
    End If
End Sub

Private Sub Cmb_Amphoe_KeyPress(KeyAscii As Integer)
          KeyAscii = AutoFind(Cmb_Amphoe, KeyAscii, False)
End Sub
Private Sub CMB_GRAPHBUILDING1_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub CMB_GRAPHBUILDING2_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub CMB_GRAPHLAND1_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub CMB_GRAPHLAND2_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub CMB_GRAPHSIGN1_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub CMB_GRAPHSIGN2_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub Cmb_Province_Click()
     Cmb_Province_ID.ListIndex = Cmb_Province.ListIndex
    If LenB(Trim$(Cmb_Province.Text)) <> 0 Then
       Cmb_Amphoe.Clear
       Cmb_Amphoe_ID.Clear
       GBQueryAmphoe.Filter = " PROVINCE_ID = '" & Cmb_Province_ID.Text & "'"
            If GBQueryAmphoe.RecordCount > 0 Then
                        Cmb_Amphoe.Clear
                        Cmb_Amphoe_ID.Clear
                      GBQueryAmphoe.MoveFirst
                        Do While Not GBQueryAmphoe.EOF
                                    Cmb_Amphoe.AddItem GBQueryAmphoe.Fields("Amphoe_Name").Value
                                    Cmb_Amphoe_ID.AddItem GBQueryAmphoe.Fields("Amphoe_ID").Value
                                    GBQueryAmphoe.MoveNext
                        Loop
                                    Cmb_Amphoe.ListIndex = 0
            End If
    End If
End Sub

Private Sub Cmb_Province_KeyPress(KeyAscii As Integer)
        KeyAscii = AutoFind(Cmb_Province, KeyAscii, False)
End Sub
Private Sub Cmb_Tambon_Click()
        Cmb_Tambon_ID.ListIndex = Cmb_Tambon.ListIndex
End Sub

Private Sub Cmb_Tambon_KeyPress(KeyAscii As Integer)
        KeyAscii = AutoFind(Cmb_Tambon, KeyAscii, False)
End Sub

Private Sub cmd_LookupIP_Click()
        MsgBox LookupIPAddress(cboServer.Text), vbOKOnly, "Lookp IP"
End Sub

Private Sub cmd_Show_error_Click()
        Dim notepadID As Long
        notepadID = Shell("notepad " & App.Path & "\Microtax.log", vbNormalFocus)
End Sub

Private Sub cmd_Update_Asset_Click()
'        On Error GoTo ErrReset
'
'        Set Rs = New ADODB.Recordset
'        If MsgBox("�س��㨷��кѹ�֡������ ��������� ?", vbYesNo, "����͹") = vbYes Then
'                Globle_Connective.BeginTrans
'                If chk_Asset.Value = Checked Then
'                        Call SET_Execute("UPDATE ADJUST_ASSET_I SET LICENSE_COUNT=" & IIf(Trim$(txt_Licence(0).Text = Empty), 0, txt_Licence(0).Text) & ",LICENSE_PAYTAX=" & IIf(Trim$(txt_Licence(1).Text = Empty), 0, txt_Licence(1).Text))
'                Else
'                        Call SET_Execute("UPDATE ADJUST_ASSET_I SET LICENSE_COUNT=" & IIf(Trim$(txt_Licence(0).Text = Empty), 0, txt_Licence(0).Text) & ",LICENSE_PAYTAX=" & IIf(Trim$(txt_Licence(1).Text = Empty), 0, txt_Licence(1).Text) & " WHERE PAYMENT_STATUS = " & False)
'                End If
'                Globle_Connective.CommitTrans
'                MsgBox "��û�Ѻ��ا�������������", vbOKOnly, "��û�Ѻ��ا������"
'        End If
'        Set Rs = Nothing
'        Exit Sub
'ErrReset:
'        Globle_Connective.RollbackTrans
'        Set Rs = Nothing
End Sub

Private Sub cmd_UpdateTaxLand_Click()
        Dim strSQL As String
        Dim i As Long, j As Long
        Dim startTime As Variant
        Dim sumArea As Single, sumTax As Single
        
        On Error GoTo ErrUpdate
        
        If MsgBox("�س��㨷��л�Ѻ����¹������ ��������� ?", vbYesNo, "����͹") = vbYes Then
                Globle_Connective.BeginTrans
                Me.MousePointer = 11
                sumTax = 0#
                sumArea = 0#
                startTime = Timer
                i = 0: j = 0
                ProgressBar1.Min = 0
                ProgressBar1.Value = ProgressBar1.Min
                Call SET_QUERY("SELECT LAND_ID,ZONETAX_ID FROM LANDDATA WHERE FLAG_PAYTAX=1 ORDER BY LAND_ID", RsLandData)
                If RsLandData.RecordCount > 0 Then
                        Call SET_QUERY("SELECT LAND_ID,LM_ID,APPLY_RAI,APPLY_POT,APPLY_VA,LAND_FLAG_APPLY FROM LANDDATA_APPLY ORDER BY LAND_ID", RsLandApply)
                        Call SET_QUERY("SELECT * FROM LANDUSE ORDER BY ZONETAX_ID ASC,LM_ID ASC", RsLandUse)
                        Call SET_QUERY("SELECT * FROM ZONE_TAX", RsZoneTax)
                        RsLandData.Fields(0).Properties("Optimize") = True
                        RsLandApply.Fields(0).Properties("Optimize") = True
                        RsLandUse.Fields("LM_ID").Properties("Optimize") = True
                        RsZoneTax.Fields("ZONETAX_ID").Properties("Optimize") = True
                        With RsLandData
                                ProgressBar1.max = .RecordCount
                                .MoveFirst
                                Do While Not .EOF
                                        RsLandApply.Filter = " LAND_ID='" & .Fields("LAND_ID").Value & "'"
                                        RsLandUse.Filter = " ZONETAX_ID ='" & RsLandData.Fields("ZONETAX_ID").Value & "'"
                                        RsZoneTax.Filter = " ZONETAX_ID ='" & RsLandData.Fields("ZONETAX_ID").Value & "'"
                                        strSQL = .Fields("LAND_ID").Value & "," & RsLandData.Fields("ZONETAX_ID").Value
                                        If RsLandApply.RecordCount > 0 Then
                                                Do While RsLandApply.EOF = False
                                                        strSQL = strSQL & "," & RsLandApply.Fields("LM_ID").Value
                                                        RsLandUse.Find " LM_ID = '" & RsLandApply.Fields("LM_ID").Value & "'", , adSearchForward
                                                        sumArea = (RsLandApply.Fields("APPLY_RAI").Value * 1600) + (RsLandApply.Fields("APPLY_POT").Value * 400) + (RsLandApply.Fields("APPLY_VA").Value * 4)     '�ŧ�� ��.�
                                                        If RsLandApply.Fields("LAND_FLAG_APPLY").Value = 0 Then '  �����ͧ
                                                                On Error GoTo ErrUpdate2
                                                                If RsLandUse.Fields("TYPE_USE_PRICE").Value = 1 Then  '�ѵ�Һҷ
                                                                      On Error Resume Next
                                                                      sumTax = sumTax + FormatNumber((RsLandUse.Fields("LANDUSE_PRICE").Value / 1600) * sumArea, 2)
                                                                Else '�ѵ��%
                                                                      On Error Resume Next
                                                                      sumTax = sumTax + FormatNumber((((RsZoneTax.Fields("TAX_RATE").Value * RsLandUse.Fields("LANDUSE_PRICE").Value) / 100) / 1600) * sumArea, 2)
                                                                End If
                                                        Else '������
                                                                If RsLandUse.Fields("TYPE_USE_PRICE").Value = 1 Then '�ѵ�Һҷ
                                                                        On Error Resume Next
                                                                      sumTax = sumTax + FormatNumber((RsLandUse.Fields("LANDUSE_RATE").Value / 1600) * sumArea, 2) '�����շ�����������ͷ���������ª��
                                                                Else '�ѵ��%
                                                                        On Error Resume Next
                                                                      sumTax = sumTax + FormatNumber((((RsZoneTax.Fields("TAX_RATE").Value * RsLandUse.Fields("LANDUSE_RATE").Value) / 100) / 1600) * sumArea, 2)
                                                                End If
                                                        End If
                                                        RsLandUse.MoveFirst
                                                        RsLandApply.MoveNext
                                                Loop
                                                Call SET_Execute("UPDATE LANDDATA SET LAND_SUMTAX=" & FormatNumber(sumTax, 2) & ",FLAG_ASSESS=0 WHERE LAND_ID='" & .Fields("LAND_ID").Value & "'")
                                        End If
                                        i = i + 1
'                                        Me.Caption = Int((i / ProgressBar1.max) * 100) & "%"
                                        ProgressBar1.Value = i
                                        RsLandApply.Filter = 0
                                        RsLandUse.Filter = 0
                                        RsZoneTax.Filter = 0
                                        sumTax = 0#
                                        .MoveNext
                                        DoEvents
                                Loop
                        End With
                End If
                Globle_Connective.CommitTrans
                Me.MousePointer = 0
'                Me.Caption = Me.Caption & " ���� " & ConvertTime(Timer - startTime)
                If LenB(Trim$(Text1.Text)) > 0 Then
                        Open App.Path & "\MicroTax.log" For Append As #1
                        Print #1, Now & " : Can't Update Tax value in LandData :"
                        Print #1, iniPath_Database
                        Print #1, Text1.Text
                        Print #1, "Total " & j & " records"
                        Print #1, "********************************"
                        Close #1
                End If
                MsgBox "��û�Ѻ��ا������������� !!!", vbOKOnly, "��û�Ѻ��ا������"
        End If
        Exit Sub
ErrUpdate:
        Globle_Connective.RollbackTrans
        MsgBox Err.Description
'        MsgBox "Can't Update Database !!!", vbOKOnly, "Error Update"
        Me.MousePointer = 0
        Exit Sub
ErrUpdate2:
        j = j + 1
        Text1.Text = Text1.Text & strSQL & vbCrLf
        Resume Next
End Sub

Private Sub cmdRefresh_Click()
        Call GetSQLServer
End Sub

Private Sub GetSQLServer()
Dim sServers() As String
Dim iCtr As Integer

Me.MousePointer = 11
cboServer.Clear
sServers = AvailableSQLServers
If sServers(0) = "" Then
        MsgBox "No SQL Servers Available"
Else
    For iCtr = 0 To UBound(sServers)
            cboServer.AddItem IIf(sServers(iCtr) = "(local)", ComputerName, sServers(iCtr))
    Next
End If
cboServer.Text = vbNullString
Me.MousePointer = 0
End Sub

'Private Sub cmdReset_Click()
'        On Error GoTo ErrReset
'
'        If MsgBox("�س��㨷��С�˹��������������� ��������� ?", vbYesNo, "����͹") = vbYes Then
'                Globle_Connective.BeginTrans
'                Call SET_Execute("UPDATE ADJUST_ASSET_I SET ASSET_YEAR=0,PAYMENT_NO=0,LAND_COUNT=0,BUILDING_COUNT=0,SIGNBOARD_COUNT=0,LICENSE_COUNT=0,ASSET_NUM_OWNER=0,PAYTAX_NUM_OWNER=0,BUILDING_PAYTAX=0,LAND_PAYTAX=0,SIGNBOARD_PAYTAX=0,LICENSE_PAYTAX=0")
'                Globle_Connective.CommitTrans
'                MsgBox "��û�Ѻ��ا�������������", vbOKOnly, "��û�Ѻ��ا������"
'        End If
'        Exit Sub
'ErrReset:
'        Globle_Connective.RollbackTrans
'End Sub

'Private Sub cmdReset2_Click()
'        On Error GoTo ErrReset
'
'        If MsgBox("�س��㨷��С�˹��������������� ��������� ?", vbYesNo, "����͹") = vbYes Then
'                Globle_Connective.BeginTrans
'                Call SET_Execute("UPDATE ADJUST_ASSET_II SET ASSET_YEAR=0,PAYMENT_NO=0,ALL_COUNT_NEW=0,COUNT_NEW_PRD2=0,SUM_NEW_PRD2=0,COUNT_NEW_PBT5=0,SUM_NEW_PBT5=0,COUNT_NEW_PP1=0,SUM_NEW_PP1=0,COUNT_PRESENT_PRD2=0,SUM_PRESENT_PRD2=0,COUNT_PRESENT_PBT5=0,SUM_PRESENT_PBT5=0,COUNT_PRESENT_PP1=0,SUM_PRESENT_PP1=0")
'                Globle_Connective.CommitTrans
'                MsgBox "��û�Ѻ��ا�������������", vbOKOnly, "��û�Ѻ��ا������"
'        End If
'        Exit Sub
'ErrReset:
'        Globle_Connective.RollbackTrans
'End Sub

Private Sub cmdSave_Click()
If MsgBox("Are want you save setting configuration?", vbInformation + vbYesNo, "Save Setting!") = vbNo Then Exit Sub
    Dim hregkey As Long  ' receives handle to the newly created or opened registry key
    Dim secattr As SECURITY_ATTRIBUTES  ' security settings of the key
    Dim neworused As Long  ' receives 1 if new key was created or 2 if an existing key was opened
    Dim stringbuffer As String  ' the string to put into the registry
    Dim retval As Long  ' return value
    Dim i As Integer
    
    secattr.nLength = Len(secattr)  ' size of the structure
    secattr.lpSecurityDescriptor = 0  ' default security level
    secattr.bInheritHandle = True  ' the default value for this setting
    
    WriteINI "register", "province_id", Trim$(Cmb_Province_ID.Text)
    WriteINI "register", "province", Trim$(Cmb_Province.Text)
    WriteINI "register", "amphoe_id", Trim$(Cmb_Amphoe_ID.Text)
    WriteINI "register", "amphoe", Trim$(Cmb_Amphoe.Text)
    WriteINI "register", "tambon", Trim$(Cmb_Tambon.Text)
    WriteINI "register", "tambon_id ", Trim$(Cmb_Tambon_ID.Text)
    
    For i = 1 To Len(Trim$(txtPassword.Text))
            stringbuffer = stringbuffer & DecToHexStr(Asc(Mid$(Trim$(txtPassword.Text), i, 1)))
    Next i
    retval = RegCreateKeyEx(HKEY_CLASSES_ROOT, subkey, 0, "", 0, KEY_WRITE, secattr, hregkey, neworused)
    If retval <> 0 Then  ' error during open
            MsgBox "Error opening or creating registry key -- aborting."
            End  ' terminate the program
    End If
    stringbuffer = stringbuffer & vbNullChar
    retval = RegSetValueEx(hregkey, "Effect ID", 0, REG_SZ, ByVal stringbuffer, Len(stringbuffer))
    retval = RegCloseKey(hregkey)
    
    WriteINI "register owner", "section", Trim$(Txt_Cfg_Section.Text)
    WriteINI "register owner", "address", Trim$(Txt_Cfg_Add.Text)
    WriteINI "register owner", "telephone", Trim$(Txt_Cfg_Tel.Text)
    WriteINI "register owner", "name", Trim$(Txt_Cfg_Name.Text)
    WriteINI "register owner", "position", Trim$(Txt_Cfg_Position.Text)
     
    WriteINI "path file", "pathmap", Trim$(Txt_PathMap.Text)
    WriteINI "path file", "pathpicture", Trim$(Txt_PathPicture.Text)
'    WriteINI "path file", "pathdatabase", Trim$(Txt_PathDatabase.Text)
        WriteINI "path file", "servername", Trim$(cboServer.Text)
        WriteINI "path file", "database", Trim$(cboDB.Text)
        WriteINI "path file", "username", Trim$(txtUser.Text)
'        WriteINI "path file", "password", Trim$(txtPassword.Text)
    
    WriteINI "printing", "print_Perform", chkPrint_Perform.Value
    WriteINI "printing", "print_Accept", chk_Accept.Value
    WriteINI "printing", "print_Payment", chk_Payment.Value
'    WriteINI "other", "autoreport", chkReportAuto.Value
'    WriteINI "other", "autotime", txtTime.Text
    
''    chkReportAuto.Value = ReadINI("other", "autoreport")
    iniAutoRep = chkReportAuto
'    txtTime.Text = ReadINI("other", "autotime")
'    iniAutoTime = CByte(txtTime.Text)
    chkPrint_Perform.Value = ReadINI("printing", "print_Perform")
    iniPrint_Perform = chkPrint_Perform.Value
    chk_Accept.Value = ReadINI("printing", "print_Accept")
    iniPrint_Accept = chk_Accept.Value
    chk_Payment.Value = ReadINI("printing", "print_Payment")
    iniPrint_Payment = chk_Payment.Value
    iniTambon_Id = Trim$(Cmb_Tambon_ID.Text)
    iniTambon = Trim$(Cmb_Tambon.Text)
    iniAmphoe_Id = Trim$(Cmb_Amphoe_ID.Text)
    iniAmphoe = Trim$(Cmb_Amphoe.Text)
    iniProvince_Id = Trim$(Cmb_Province_ID.Text)
    iniProvince = Trim$(Cmb_Province.Text)
    ini_Servername = ReadINI("path file", "servername") ' SQL Server name
    ini_Database = ReadINI("path file", "database")          ' Database name in SQL Server
    ini_Username = ReadINI("path file", "username")        ' Login user name
    ini_Password = GetRegis        ' Password
    iniPath_Picture = Trim$(Txt_PathPicture.Text)
'    iniPath_Database = Trim$(Txt_PathDatabase.Text)
    iniPath_Map = Trim$(Txt_PathMap.Text)
'    If Acc_Path = True Then
'            Call SetTimer
'    End If
'    SQLConfigDataSource 0&, ODBC_ADD_SYS_DSN, "Microsoft Access Driver (*.mdb)", "DSN=Microtax" & vbNullChar & "DBQ=" & iniPath_Database & vbNullChar & "DESCRIPTION=" & vbNullChar & vbNullChar
    SQLConfigDataSource 0&, ODBC_ADD_SYS_DSN, "SQL Server", "DSN=MyNewDSN\0Network=DBMSSOCN\0Server=MySqlServer\0Database=pubs\0Description=New Data Source\0"


    MsgBox "���ͤ�������ó��÷ӡ�ûԴ�������������ҹ����", vbExclamation, "��Ѻ�к���õ�駤��������������"
End Sub

Private Sub Form_Load()
On Error GoTo ErrLoad
        Set RsLandData = New ADODB.Recordset
        Set RsLandApply = New ADODB.Recordset
        Set RsLandUse = New ADODB.Recordset
        Set RsZoneTax = New ADODB.Recordset
If Acc_Path = True Then
   Call SET_REFRESH
 End If
  Txt_PathMap.Text = iniPath_Map
  Txt_PathPicture.Text = iniPath_Picture
'  Txt_PathDatabase.Text = iniPath_Database
  
  Cmb_Tambon.Text = iniTambon: Cmb_Tambon_ID.Text = iniTambon_Id
  Cmb_Amphoe.Text = iniAmphoe:   Cmb_Amphoe_ID.Text = iniAmphoe_Id
  Cmb_Province.Text = iniProvince:   Cmb_Province_ID.Text = iniProvince_Id
  
  Txt_Cfg_Section.Text = iniSection
  Txt_Cfg_Add.Text = iniAdd
  Txt_Cfg_Tel.Text = iniTel
  Txt_Cfg_Name.Text = iniName
  Txt_Cfg_Position.Text = iniPosition
  
  cboServer.Text = ini_Servername
  txtUser.Text = ini_Username
  txtPassword.Text = ini_Password
  cboDB.Text = ini_Database
  chkPrint_Perform.Value = ReadINI("printing", "print_Perform")
  chk_Accept.Value = ReadINI("printing", "print_Accept")
  chk_Payment.Value = ReadINI("printing", "print_Payment")
'  chkReportAuto.Value = ReadINI("Other", "autoreport")
'  txtTime.Text = ReadINI("Other", "autotime")
  Exit Sub
ErrLoad:
  MsgBox Err.Description
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Acc_Path = True Then
        GBQueryTambon.Filter = " AMPHOE_ID = '" & Trim$(iniAmphoe_Id) & "'"
  Else
        Unload Me
        Unload Frm_Login
        Set Globle_Connective = Nothing
        End
  End If
        Set Frm_Config = Nothing
        Set RsLandData = Nothing
        Set RsLandApply = Nothing
        Set RsLandUse = Nothing
        Set RsZoneTax = Nothing
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)

'If SSTab1.Tab = 2 Then
'   If iniPath_Map = "" Then Exit Sub
'            Lst_Overlay.Clear
'            Cmb_Block.Clear
'           Cmb_Land.Clear
'           Cmb_Building.Clear
'           Cmb_Singbord.Clear
'  Dim i As Byte, totalOverlay As Integer, PathStr As String
'              PathStr = ReadINI("path file", "pathmap")
'              Sis1.Level = SIS_LEVEL_MODELLER
'              'Sis1.Level = SIS_LEVEL_MANAGER
'              Sis1.LoadSwd (PathStr)
'   With Sis1
'                     totalOverlay = .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
'         If totalOverlay > 0 Then
'            For i = 0 To totalOverlay - 1
'                        Lst_Overlay.AddItem i + 1 & " " & .GetStr(SIS_OT_OVERLAY, i, "_name$")
'                        Cmb_Block.AddItem i + 1
'                        Cmb_Land.AddItem i + 1
'                        Cmb_Building.AddItem i + 1
'                        Cmb_Singbord.AddItem i + 1
'            Next i
'         End If
'    End With
'    On Error Resume Next
'                        Cmb_Block.ListIndex = iniZone
'                        Cmb_Land.ListIndex = iniLand
'                        Cmb_Building.ListIndex = iniBuliding
'                        Cmb_Singbord.ListIndex = iniSignbord
'
'                 CMB_GRAPHLAND1.ListIndex = ini_landtaxaccept
'                 CMB_GRAPHLAND2.ListIndex = ini_landtaxpay
'                 CMB_GRAPHBUILDING1.ListIndex = ini_buildingtaxaccept
'                 CMB_GRAPHBUILDING2.ListIndex = ini_buildingtaxpay
'                 CMB_GRAPHSIGN1.ListIndex = ini_signbordtaxaccept
'                 CMB_GRAPHSIGN2.ListIndex = ini_signbordtaxpay
'End If
'If SSTab1.Tab = 0 Or SSTab1.Tab = 1 Then ProgressBar1.Value = 0
'Exit Sub
'If SSTab1.Tab = 3 Then
'        Call GetSQLServer
'End If
End Sub

Private Sub txt_Licence_KeyPress(Index As Integer, KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub
