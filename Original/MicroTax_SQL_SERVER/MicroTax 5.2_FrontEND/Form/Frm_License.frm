VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_License 
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12885
   ControlBox      =   0   'False
   Icon            =   "Frm_License.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   Picture         =   "Frm_License.frx":151A
   ScaleHeight     =   9030
   ScaleWidth      =   12885
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Cmb_LAND_ID 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3390
      TabIndex        =   76
      Top             =   7320
      Width           =   1605
   End
   Begin VB.CheckBox Chk_STATUS_LINK0 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   72
      Top             =   8550
      Width           =   195
   End
   Begin VB.CheckBox Chk_STATUS_LINK1 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   71
      Top             =   7470
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_STATUS_LINK2 
      Enabled         =   0   'False
      Height          =   195
      Left            =   240
      TabIndex        =   70
      Top             =   8010
      Width           =   195
   End
   Begin VB.CheckBox Chk_FLAG_STATUS0 
      Enabled         =   0   'False
      Height          =   195
      Left            =   8160
      TabIndex        =   63
      Top             =   3840
      Width           =   195
   End
   Begin VB.CheckBox Chk_FLAG_STATUS1 
      Enabled         =   0   'False
      Height          =   195
      Left            =   8160
      TabIndex        =   62
      Top             =   3450
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.TextBox Txt_LICENSE_SUMTAX 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   270
      Left            =   5910
      Locked          =   -1  'True
      TabIndex        =   61
      Text            =   "0.00"
      Top             =   5670
      Width           =   1275
   End
   Begin VB.TextBox Txt_LICENSE_REMARK 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   59
      Text            =   "-"
      Top             =   5250
      Width           =   6615
   End
   Begin VB.TextBox Txt_StoryRemark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4140
      TabIndex        =   57
      Text            =   "-"
      Top             =   900
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.ComboBox CMB_CLASS_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6030
      TabIndex        =   56
      Top             =   3420
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.ComboBox CMB_BUSINESS_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6000
      TabIndex        =   55
      Top             =   2820
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.ComboBox CMB_CLASS_DETAILS 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1050
      TabIndex        =   5
      Top             =   4740
      Width           =   6705
   End
   Begin VB.ComboBox CMB_BUSINESS_TYPE 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1050
      TabIndex        =   4
      Top             =   4290
      Width           =   6705
   End
   Begin VB.ComboBox Cmb_Building_ID 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3390
      TabIndex        =   10
      Top             =   7860
      Width           =   1605
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1050
      TabIndex        =   9
      Top             =   6780
      Width           =   1335
   End
   Begin VB.TextBox Txt_License_Fax 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7080
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   8430
      Width           =   5505
   End
   Begin VB.TextBox Txt_License_Tel 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   7080
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   7920
      Width           =   5505
   End
   Begin VB.TextBox Txt_LICENSE_AREA 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      TabIndex        =   8
      Text            =   "0"
      Top             =   5550
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_ENGINEER 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      MaxLength       =   10
      TabIndex        =   7
      Text            =   "0"
      Top             =   5070
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_EMPLOYEE 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9930
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   6
      Text            =   "0"
      Top             =   4590
      Width           =   1635
   End
   Begin VB.TextBox Txt_LICENSE_NO 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3420
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   2
      Top             =   3420
      Width           =   1305
   End
   Begin VB.TextBox Txt_LICENSE_BOOK 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   1
      Top             =   3420
      Width           =   1305
   End
   Begin VB.TextBox Txt_LICENSE_NAME 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "-"
      Top             =   3870
      Width           =   6615
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":104AA
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_License.frx":10D11
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_License.frx":13608
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_License.frx":13E9F
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":16784
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_License.frx":16FE7
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_License.frx":19A0C
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_License.frx":1A2BE
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":1CCF9
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_License.frx":1D5E8
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_License.frx":20143
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_License.frx":20971
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_License.frx":231FB
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_License.frx":23A64
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin MSComCtl2.DTPicker DTP_LICENSE_DATE 
      Height          =   330
      Left            =   11190
      TabIndex        =   13
      Top             =   3390
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_License.frx":2625C
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   173801473
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComCtl2.DTPicker DTP_LICENSE_ENDDATE 
      Height          =   330
      Left            =   11190
      TabIndex        =   67
      Top             =   3750
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_License.frx":26576
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   173801473
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin VB.Label TMP_LAND_ID 
      Height          =   285
      Left            =   5040
      TabIndex        =   77
      Top             =   7890
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   26
      Left            =   10230
      Top             =   7350
      Width           =   2385
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   25
      Left            =   10230
      Top             =   6810
      Width           =   2385
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   24
      Left            =   7050
      Top             =   7350
      Width           =   2025
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   23
      Left            =   7050
      Top             =   6810
      Width           =   2025
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   35
      Left            =   2550
      TabIndex        =   75
      Top             =   7440
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����кط����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   34
      Left            =   570
      TabIndex        =   74
      Top             =   8520
      Width           =   915
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�кط���駷��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   29
      Left            =   540
      TabIndex        =   73
      Top             =   7440
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�кط�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   28
      Left            =   540
      TabIndex        =   69
      Top             =   7980
      Width           =   1395
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ʶҹ��͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   27
      Left            =   8250
      TabIndex        =   68
      Top             =   2910
      Width           =   1785
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ¡��ԡ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   26
      Left            =   10350
      TabIndex        =   66
      Top             =   3810
      Width           =   705
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "¡��ԡ�͹حҵ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   25
      Left            =   8520
      TabIndex        =   65
      Top             =   3810
      Width           =   1275
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   23
      Left            =   8520
      TabIndex        =   64
      Top             =   3420
      Width           =   915
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   975
      Index           =   22
      Left            =   7980
      Top             =   3210
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1035
      Index           =   21
      Left            =   7950
      Top             =   3180
      Width           =   4875
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   20
      Left            =   1050
      Top             =   5220
      Width           =   6675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   24
      Left            =   180
      TabIndex        =   60
      Top             =   5280
      Width           =   765
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   19
      Left            =   9360
      Top             =   1560
      Width           =   3285
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   18
      Left            =   1020
      Top             =   2130
      Width           =   11625
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   15
      Left            =   1020
      Top             =   1560
      Width           =   6735
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   14
      Left            =   5880
      Top             =   5640
      Width           =   1335
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*���˵ػ�Ѻ����¹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   107
      Left            =   2520
      TabIndex        =   58
      Top             =   960
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ҹ�������                           �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   22
      Left            =   4680
      TabIndex        =   54
      Top             =   5670
      Width           =   3015
   End
   Begin VB.Label Lb_Owner_ID 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   150
      TabIndex        =   53
      Top             =   510
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   17
      Left            =   7050
      Top             =   8400
      Width           =   5565
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   16
      Left            =   7050
      Top             =   7890
      Width           =   5565
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   21
      Left            =   6270
      TabIndex        =   52
      Top             =   8460
      Width           =   645
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Ѿ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   20
      Left            =   6180
      TabIndex        =   51
      Top             =   7950
      Width           =   720
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   19
      Left            =   2250
      TabIndex        =   50
      Top             =   7980
      Width           =   1065
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����⫹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   18
      Left            =   240
      TabIndex        =   49
      Top             =   6930
      Width           =   705
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   33
      Left            =   6555
      TabIndex        =   48
      Top             =   7410
      Width           =   330
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   32
      Left            =   9750
      TabIndex        =   47
      Top             =   7410
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� (�����)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   31
      Left            =   9180
      TabIndex        =   46
      Top             =   6870
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   30
      Left            =   6390
      TabIndex        =   45
      Top             =   6870
      Width           =   435
   End
   Begin VB.Label Lb_Tambon 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   7080
      TabIndex        =   44
      Top             =   6840
      Width           =   1965
   End
   Begin VB.Label Lb_Village 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   10260
      TabIndex        =   43
      Top             =   6840
      Width           =   2325
   End
   Begin VB.Label Lb_Soi 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   7080
      TabIndex        =   42
      Top             =   7380
      Width           =   1965
   End
   Begin VB.Label Lb_Street 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   10260
      TabIndex        =   41
      Top             =   7380
      Width           =   2355
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ŷ����ʶҹ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   17
      Left            =   480
      TabIndex        =   40
      Top             =   6210
      Width           =   1380
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2415
      Index           =   7
      Left            =   60
      Top             =   6540
      Width           =   12705
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1485
      Index           =   8
      Left            =   30
      Top             =   1260
      Width           =   12795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   11
      Left            =   9900
      Top             =   5520
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   10
      Left            =   9900
      Top             =   5040
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   285
      Index           =   9
      Left            =   9900
      Top             =   4560
      Width           =   1695
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѡɳ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   345
      TabIndex        =   39
      Top             =   4860
      Width           =   585
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   285
      TabIndex        =   38
      Top             =   4410
      Width           =   645
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   1815
      Index           =   6
      Left            =   7950
      Top             =   4260
      Width           =   4875
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1755
      Index           =   5
      Left            =   7980
      Top             =   4290
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   4
      Left            =   3390
      Top             =   3390
      Width           =   1365
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   1050
      Top             =   3390
      Width           =   1365
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   14
      Left            =   2790
      TabIndex        =   37
      Top             =   3480
      Width           =   420
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   540
      TabIndex        =   36
      Top             =   3480
      Width           =   420
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�.�.�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   12
      Left            =   12000
      TabIndex        =   35
      Top             =   5580
      Width           =   465
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ç���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   11
      Left            =   12000
      TabIndex        =   34
      Top             =   5100
      Width           =   525
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   210
      Index           =   10
      Left            =   12000
      TabIndex        =   33
      Top             =   4620
      Width           =   240
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ�͡"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   10320
      TabIndex        =   32
      Top             =   3420
      Width           =   540
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��鹷���Сͺ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   8280
      TabIndex        =   31
      Top             =   5550
      Width           =   1350
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ѧ����ͧ�ѡ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   7
      Left            =   8280
      TabIndex        =   30
      Top             =   5070
      Width           =   1215
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ���ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   8280
      TabIndex        =   29
      Top             =   4590
      Width           =   1095
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   705
      TabIndex        =   28
      Top             =   3930
      Width           =   225
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ʶҹ��Сͺ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   270
      TabIndex        =   27
      Top             =   2910
      Width           =   1845
   End
   Begin VB.Label Lb_Owner_Add 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   255
      Left            =   1050
      TabIndex        =   26
      Top             =   2160
      Width           =   11580
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   2
      Left            =   525
      TabIndex        =   25
      Top             =   2160
      Width           =   345
   End
   Begin VB.Label Lb_Owner_Type 
      Alignment       =   2  'Center
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   9390
      TabIndex        =   24
      Top             =   1590
      Width           =   3225
   End
   Begin VB.Label Lb_Owner_Name 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1050
      TabIndex        =   23
      Top             =   1590
      Width           =   6675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   300
      TabIndex        =   22
      Top             =   1620
      Width           =   570
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   8220
      TabIndex        =   21
      Top             =   1620
      Width           =   990
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   1050
      Top             =   3840
      Width           =   6675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�â��͹حҵ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   330
      TabIndex        =   20
      Top             =   960
      Width           =   1620
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   4
      Left            =   30
      Top             =   930
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   0
      Left            =   30
      Top             =   2850
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2895
      Index           =   2
      Left            =   30
      Top             =   3180
      Width           =   7875
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   1
      Left            =   60
      Top             =   6180
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2835
      Index           =   1
      Left            =   60
      Top             =   3210
      Width           =   7815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1425
      Index           =   12
      Left            =   60
      Top             =   1290
      Width           =   12735
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2475
      Index           =   13
      Left            =   30
      Top             =   6510
      Width           =   12765
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   2
      Left            =   7950
      Top             =   2850
      Width           =   2325
   End
End
Attribute VB_Name = "Frm_License"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Status As String

Private Sub SET_REFRESH()
   GBQueryZoneTax.Requery
   GBQueryBusiness.Requery
   GBQueryClass.Requery
   
If GBQueryBusiness.RecordCount > 0 Then
   CMB_BUSINESS_TYPE.Clear
   CMB_BUSINESS_ID.Clear
    GBQueryBusiness.MoveFirst
    Do While Not GBQueryBusiness.EOF
           CMB_BUSINESS_TYPE.AddItem GBQueryBusiness.Fields("BUSINESS_TYPE").Value
           CMB_BUSINESS_ID.AddItem GBQueryBusiness.Fields("BUSINESS_ID").Value
           GBQueryBusiness.MoveNext
    Loop
End If
         Cmb_Zoneblock.Clear
         GBQueryZone.Filter = "ZONE_ID <> '' "
If GBQueryZone.RecordCount > 0 Then
            GBQueryZone.MoveFirst
    Do While Not GBQueryZone.EOF
                  If GBQueryZone.Fields("ZONE_BLOCK").Value <> Empty Then
                    Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
    Loop
End If
End Sub

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
    Btn_Search.Enabled = False:     Btn_Refresh.Enabled = False
    
    CMB_BUSINESS_TYPE.Enabled = True
    CMB_CLASS_DETAILS.Enabled = True
    Cmb_Zoneblock.Enabled = True
    Cmb_Building_ID.Enabled = True
    Cmb_Land_ID.Enabled = True
    
    Txt_LICENSE_BOOK.Locked = False:                             Txt_LICENSE_BOOK.BackColor = &H80000005
    Txt_LICENSE_NO.Locked = False:                                   Txt_LICENSE_NO.BackColor = &H80000005
    Txt_LICENSE_NAME.Locked = False:                               Txt_LICENSE_NAME.BackColor = &H80000005
    Txt_LICENSE_EMPLOYEE.Locked = False:                    Txt_LICENSE_EMPLOYEE.BackColor = &H80000005
    Txt_LICENSE_ENGINEER.Locked = False:                     Txt_LICENSE_ENGINEER.BackColor = &H80000005
    Txt_LICENSE_AREA.Locked = False:                                Txt_LICENSE_AREA.BackColor = &H80000005
    Txt_License_Tel.Locked = False:                                        Txt_License_Tel.BackColor = &H80000005
    Txt_License_Fax.Locked = False:                                        Txt_License_Fax.BackColor = &H80000005
    Txt_LICENSE_REMARK.Locked = False:                             Txt_LICENSE_REMARK.BackColor = &H80000005
    Txt_LICENSE_SUMTAX.Locked = False:                              Txt_LICENSE_SUMTAX.BackColor = &H80000005
    Lb_Owner_Name.BackColor = &H80000005
    
    Chk_FLAG_STATUS0.Enabled = True
    Chk_FLAG_STATUS1.Enabled = True
    Chk_STATUS_LINK0.Enabled = True
    Chk_STATUS_LINK1.Enabled = True
    Chk_STATUS_LINK2.Enabled = True
    
    If STATE = "EDIT" Then
           Txt_LICENSE_BOOK.Locked = True:                             Txt_LICENSE_BOOK.BackColor = &HEBEBE7
           Txt_LICENSE_NO.Locked = True:                                   Txt_LICENSE_NO.BackColor = &HEBEBE7
            
            Txt_StoryRemark.Visible = True
            Label2(107).Visible = True
            Shape3(0).BackColor = &HC0C000
            Shape3(1).BackColor = &HC0C000
            Shape3(2).BackColor = &HC0C000
            Shape3(4).BackColor = &HC0C000
    End If
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                           Lb_Owner_ID.Caption = Empty
                           CMB_BUSINESS_TYPE.Text = Empty
                           CMB_CLASS_DETAILS.Text = Empty
                           Cmb_Zoneblock.Text = Empty
                           Cmb_Building_ID.Text = Empty
                           DTP_LICENSE_DATE.Value = Now
                           Txt_LICENSE_BOOK.Text = Empty
                            Txt_LICENSE_NO.Text = Empty
                            Txt_LICENSE_NAME.Text = Empty
                            Txt_LICENSE_EMPLOYEE.Text = Empty
                            Txt_LICENSE_ENGINEER.Text = Empty
                            Txt_LICENSE_AREA.Text = Empty
                            Txt_License_Tel.Text = Empty
                            Txt_License_Fax.Text = Empty
                            Lb_Owner_Name.Caption = Empty
                            Lb_Owner_Type.Caption = Empty
                            Lb_Owner_Add.Caption = Empty
                            Lb_Tambon.Caption = Empty
                            Lb_Village.Caption = Empty
                            Lb_Soi.Caption = Empty
                            Lb_Street.Caption = Empty
                            Txt_LICENSE_SUMTAX.Text = Empty
                            Txt_LICENSE_REMARK.Text = Empty
                            TMP_LAND_ID.Caption = Empty
                            Cmb_Land_ID.Text = Empty
    If STATE <> "ADD" Then
        Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
        Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
        Btn_Search.Enabled = True:      Btn_Refresh.Enabled = True
        
    Txt_LICENSE_BOOK.Locked = True:                             Txt_LICENSE_BOOK.BackColor = &HEBEBE7
    Txt_LICENSE_NO.Locked = True:                                   Txt_LICENSE_NO.BackColor = &HEBEBE7
    Txt_LICENSE_NAME.Locked = True:                               Txt_LICENSE_NAME.BackColor = &HEBEBE7
    Txt_LICENSE_EMPLOYEE.Locked = True:                    Txt_LICENSE_EMPLOYEE.BackColor = &HEBEBE7
    Txt_LICENSE_ENGINEER.Locked = True:                     Txt_LICENSE_ENGINEER.BackColor = &HEBEBE7
    Txt_LICENSE_AREA.Locked = True:                                Txt_LICENSE_AREA.BackColor = &HEBEBE7
    Txt_License_Tel.Locked = True:                                        Txt_License_Tel.BackColor = &HEBEBE7
    Txt_License_Fax.Locked = True:                                        Txt_License_Fax.BackColor = &HEBEBE7
    Txt_LICENSE_REMARK.Locked = True:                                        Txt_LICENSE_REMARK.BackColor = &HEBEBE7
    Txt_LICENSE_SUMTAX.Locked = True:                                        Txt_LICENSE_SUMTAX.BackColor = &HEBEBE7
    Lb_Owner_Name.BackColor = &HEBEBE7
    
    CMB_BUSINESS_TYPE.Enabled = False
    CMB_CLASS_DETAILS.Enabled = False
    Cmb_Zoneblock.Enabled = False
    Cmb_Building_ID.Enabled = False
    Cmb_Land_ID.Enabled = False
    
    Chk_FLAG_STATUS0.Enabled = False
    Chk_FLAG_STATUS1.Enabled = False
    Chk_STATUS_LINK0.Enabled = False
    Chk_STATUS_LINK1.Enabled = False
    Chk_STATUS_LINK2.Enabled = False
    
    Txt_StoryRemark.Visible = False
    Label2(107).Visible = False
    Shape3(0).BackColor = &HD6D6D6
    Shape3(1).BackColor = &HD6D6D6
    Shape3(2).BackColor = &HD6D6D6
    Shape3(4).BackColor = &HD6D6D6
   End If
End If
End Sub

Private Function CheckBeforPost() As Boolean
         CheckBeforPost = True
    If Lb_Owner_ID.Caption = Empty Then
        MsgBox "�ô�кت��ͼ����͹حҵ !", vbCritical, "Warning !"
        CheckBeforPost = False
        Exit Function
    End If
    
    If Txt_LICENSE_BOOK.Text = Empty Or Txt_LICENSE_NO.Text = Empty Then
                MsgBox "�ô�к� �Ţ��� ���� ����������ú! ", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
       If Trim$(Txt_LICENSE_NAME.Text) = Empty Then
                MsgBox "�ô�к� ����ʶҹ��Сͺ���! ", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
    If CMB_CLASS_ID.Text = Empty Then
                MsgBox "�ô�к� �ѡɳ�ʶҹ��Сͺ���", vbCritical, "Warning !"
                CheckBeforPost = False
                Exit Function
   End If
   
   If Chk_STATUS_LINK1.Value Then
             If Cmb_Land_ID.Text = Empty Then
                MsgBox "�ô�к� ���ʷ��Թ", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
             End If
   ElseIf Chk_STATUS_LINK2.Value Then
             If Cmb_Building_ID.Text = Empty Then
                MsgBox "�ô�к� �����ç���͹", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
             End If
   ElseIf Chk_STATUS_LINK0.Value Then
                CheckBeforPost = True
   Else
                MsgBox "�ô�к� ����駻�Сͺ��ä��", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
   End If
End Function

Private Sub WriteHistoryFile()
Dim ROUND_COUNT As String
Dim Query As ADODB.Recordset
Set Query = New ADODB.Recordset

'sql_txt = "Select * From LICENSEDATA WHERE  LICENSE_BOOK = '" & Trim$(Txt_LICENSE_BOOK.Text) & "' AND LICENSE_NO = '" & Trim$(Txt_LICENSE_NO.Text) & "'"
'Call SET_QUERY(sql_txt, Query)
Set Query = Globle_Connective.Execute("exec sp_writehistory_license_query '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO.Text) & "'", , adCmdUnknown)
With Query
'            sql_txt = Empty
            ROUND_COUNT = RunAutoNumber("LICENSEDATASTORY", "ROUND_COUNT", Txt_LICENSE_BOOK.Text & "/" & Txt_LICENSE_NO.Text & "-0001", False, "")
            Globle_Connective.Execute "exec sp_insert_licensestory '" & ROUND_COUNT & "','" & .Fields("LICENSE_BOOK").Value & "','" & .Fields("LICENSE_NO").Value & "','" & IIf(IsNull(.Fields("LICENSE_DATE").Value), "", CvDate2(.Fields("LICENSE_DATE").Value)) & "','" & _
            .Fields("LICENSE_NAME").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_ID").Value & "'," & .Fields("STATUS_LINK").Value & ",'" & .Fields("CLASS_ID").Value & "','" & .Fields("LICENSE_TEL").Value & "','" & .Fields("LICENSE_FAX").Value & "','" & _
            .Fields("OWNERSHIP_ID").Value & "'," & .Fields("LICENSE_ENGINEER").Value & "," & .Fields("LICENSE_EMPLOYEE").Value & "," & .Fields("LICENSE_AREA").Value & ",'" & _
            Trim$(Txt_StoryRemark.Text) & "','" & CvDate2(Date) & "'," & .Fields("FLAG_STATUS").Value & ",'" & IIf(IsNull(.Fields("LICENSE_ENDDATE").Value), "", CvDate2(.Fields("LICENSE_ENDDATE").Value)) & "'," & .Fields("LICENSE_SUMTAX").Value & ",'" & .Fields("LICENSE_REMARK").Value & "','" & _
            strUser & "','" & CvDate2(Now, 1) & "'", , adCmdUnknown
'                                       sql_txt = "INSERT INTO LICENSEDATASTORY ( ROUND_COUNT,LICENSE_BOOK,LICENSE_NO,LICENSE_DATE,LICENSE_NAME,LAND_ID,BUILDING_ID,STATUS_LINK,CLASS_ID,LICENSE_TEL," & _
'                                                            " LICENSE_FAX, OWNERSHIP_ID , LICENSE_ENGINEER, LICENSE_EMPLOYEE ,LICENSE_AREA ,LICENSE_STORY,LICENSE_UPDATE,FLAG_STATUS,LICENSE_ENDDATE,LICENSE_SUMTAX,LICENSE_REMARK) "
'                                        sql_txt = sql_txt & " VALUES ('" & ROUND_COUNT & "','" & .Fields("LICENSE_BOOK").Value & "','"
'                                        sql_txt = sql_txt & .Fields("LICENSE_NO").Value & "','" & .Fields("LICENSE_DATE").Value & "','" & .Fields("LICENSE_NAME").Value & "','" & .Fields("LAND_ID").Value & "','" & .Fields("BUILDING_ID").Value & "',"
'                                        sql_txt = sql_txt & .Fields("STATUS_LINK").Value & ",'" & .Fields("CLASS_ID").Value & "','" & .Fields("LICENSE_TEL").Value & "','" & .Fields("LICENSE_FAX").Value & "','" & .Fields("OWNERSHIP_ID").Value & "',"
'                                        sql_txt = sql_txt & .Fields("LICENSE_ENGINEER").Value & "," & .Fields("LICENSE_EMPLOYEE").Value & "," & .Fields("LICENSE_AREA").Value & ",'"
'                                        sql_txt = sql_txt & Trim$(Txt_StoryRemark.Text) & "','" & Date & "'," & .Fields("FLAG_STATUS").Value & "," & IIf(IsNull(.Fields("LICENSE_ENDDATE").Value), "Null", "'" & .Fields("LICENSE_ENDDATE").Value & "'") & "," & .Fields("LICENSE_SUMTAX").Value & ",'" & .Fields("LICENSE_REMARK").Value & "')"
'                                        Call SET_Execute(sql_txt)
                                    
'     update ������������ PBA1

'            sql_txt = "UPDATE PBA1 SET PBA1_AMOUNT = " & CDbl(FormatNumber(Txt_LICENSE_SUMTAX.Text, 2)) & _
'                            " WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "' AND PBA1_YEAR = " & IIf(Year(Date) < 2500, Year(Date) + 543, Year(Date) - 543) & " AND PBA1_STATUS = 0 AND PBA1_NO_STATUS = 0 "
'            Call SET_Execute(sql_txt)
               
               'MAKE FOR UPDATE ������㹵��ҧ PBA1 ��èѴ���������������¹����͡����Է��� =================================
'                sql_txt = Empty
'                sql_txt = " Select  * From PBA1 WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "'"
'                Call SET_QUERY(sql_txt, Query)
'                If .RecordCount > 0 Then
'                    sql_txt = "UPDATE PBA1 SET OWNERSHIP_ID = '" & Lb_Owner_ID.Caption & "'" & " WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "'" & _
'                                     " AND OWNERSHIP_ID = '" & .Fields("OWNERSHIP_ID").Value & "'"
'                End If
End With
Set Query = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
'Dim sql_txt As String
Dim LAND_ID  As String, Building_id As String, FlagStatus As Byte
         If Chk_STATUS_LINK1.Value Then
                     FlagStatus = 1
                     LAND_ID = Cmb_Land_ID.Text
                     Building_id = vbNullString
         ElseIf Chk_STATUS_LINK2.Value Then
                           FlagStatus = 2
                           Building_id = Cmb_Building_ID.Text
                           LAND_ID = TMP_LAND_ID.Caption
         Else
                            FlagStatus = 0
                            Building_id = vbNullString
                            LAND_ID = vbNullString
         End If
         Globle_Connective.Execute "exec sp_manage_licensedata '" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO) & "','" & IIf(IsNull(DTP_LICENSE_DATE) = False, CvDate1(DTP_LICENSE_DATE), "") & "','" & _
                            Trim$(Txt_LICENSE_NAME.Text) & "','" & LAND_ID & "','" & Building_id & "'," & FlagStatus & ",'" & CMB_CLASS_ID.Text & "','" & Trim$(Txt_License_Tel.Text) & "','" & Trim$(Txt_License_Fax.Text) & "','" & _
                            Lb_Owner_ID.Caption & "'," & CInt(Txt_LICENSE_ENGINEER.Text) & "," & CInt(Txt_LICENSE_EMPLOYEE.Text) & "," & CSng(Txt_LICENSE_AREA.Text) & ",'" & Trim$(Txt_LICENSE_REMARK.Text) & "'," & CCur(Txt_LICENSE_SUMTAX.Text) & "," & CByte(Chk_FLAG_STATUS1.Value) & ",'" & _
                            IIf(IsNull(DTP_LICENSE_ENDDATE) = False, CvDate1(DTP_LICENSE_ENDDATE), "") & "','" & strUser & "','" & CvDate2(Now, 1) & "','" & STATE & "'", , adCmdUnknown
        If STATE = "EDIT" Then Call WriteHistoryFile
'                          If STATE = "ADD" Then
'                                      sql_txt = "INSERT INTO LICENSEDATA ( LICENSE_BOOK,LICENSE_NO,LICENSE_DATE,LICENSE_NAME,LAND_ID,BUILDING_ID,STATUS_LINK,CLASS_ID,LICENSE_TEL," & _
'                                                            " LICENSE_FAX, OWNERSHIP_ID , LICENSE_ENGINEER, LICENSE_EMPLOYEE ,LICENSE_AREA ,LICENSE_REMARK,LICENSE_SUMTAX,FLAG_STATUS,LICENSE_ENDDATE) "
'                                     sql_txt = sql_txt & " VALUES ('" & Trim$(Txt_LICENSE_BOOK.Text) & "','" & Trim$(Txt_LICENSE_NO) & "','" & Format$(DTP_LICENSE_DATE.Value, "dd/mm/yyyy") & "','" & _
'                                                            Trim$(Txt_LICENSE_NAME.Text) & "','" & LAND_ID & "','" & Building_id & "'," & FlagStatus & ",'" & CMB_CLASS_ID.Text & "','" & Trim$(Txt_License_Tel.Text) & "','" & Trim$(Txt_License_Fax.Text) & "','" & _
'                                                            Lb_Owner_ID.Caption & "'," & CSng(Txt_LICENSE_ENGINEER.Text) & "," & CSng(Txt_LICENSE_EMPLOYEE.Text) & "," & CSng(Txt_LICENSE_AREA.Text) & ",'" & Trim$(Txt_LICENSE_REMARK.Text) & "'," & Txt_LICENSE_SUMTAX.Text & "," & CByte(Chk_FLAG_STATUS1.Value) & "," & _
'                                                            IIf(Chk_FLAG_STATUS0.Value, "'" & Format$(DTP_LICENSE_ENDDATE.Value, "dd/mm/yyyy") & "'", "NULL") & ")"
'                                  ElseIf STATE = "EDIT" Then
'                                               Call WriteHistoryFile
'                                               sql_txt = "UPDATE  LICENSEDATA  SET  LICENSE_DATE = '" & Format$(DTP_LICENSE_DATE.Value, "dd/mm/yyyy") & "'," & _
'                                                                       " LICENSE_NAME =  '" & Trim$(Txt_LICENSE_NAME.Text) & "'," & _
'                                                                       " LAND_ID =  '" & LAND_ID & "'," & _
'                                                                       " BUILDING_ID =  '" & Building_id & "'," & _
'                                                                       " STATUS_LINK =  " & FlagStatus & "," & _
'                                                                       " CLASS_ID =  '" & CMB_CLASS_ID.Text & "'," & _
'                                                                       " LICENSE_TEL =  '" & Trim$(Txt_License_Tel.Text) & "'," & _
'                                                                       " LICENSE_FAX =  '" & Trim$(Txt_License_Fax.Text) & "'," & _
'                                                                       " OWNERSHIP_ID =  '" & Lb_Owner_ID.Caption & "'," & _
'                                                                       " LICENSE_ENGINEER =  " & CSng(Txt_LICENSE_ENGINEER.Text) & "," & _
'                                                                       " LICENSE_EMPLOYEE =  " & CSng(Txt_LICENSE_EMPLOYEE.Text) & "," & _
'                                                                       " LICENSE_AREA =  " & CSng(Txt_LICENSE_AREA.Text) & "," & _
'                                                                       " LICENSE_REMARK = '" & Trim$(Txt_LICENSE_REMARK.Text) & "'," & _
'                                                                       " LICENSE_SUMTAX =  " & CSng(Txt_LICENSE_SUMTAX.Text) & "," & _
'                                                                       " FLAG_STATUS =  " & CByte(Chk_FLAG_STATUS1.Value) & "," & _
'                                                                       " LICENSE_ENDDATE = " & IIf(Chk_FLAG_STATUS0.Value, "'" & Format$(DTP_LICENSE_ENDDATE.Value, "dd/mm/yyyy") & "'", "NULL") & _
'                                                                       " WHERE LICENSE_BOOK = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO & "'"
'                                  ElseIf STATE = "DEL" Then
'                                               sql_txt = "DELETE LICENSE_BOOK FROM LICENSEDATA  WHERE LICENSE_BOOK  = '" & Txt_LICENSE_BOOK.Text & "' AND LICENSE_NO = '" & Txt_LICENSE_NO.Text & "'"
'                        End If
'                                            Call SET_Execute(sql_txt)
                                            Call SET_REFRESH
End Sub

Private Sub DTC_Business_Type_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub Btn_Add_Click()
        Call SET_TEXTBOX("ADD", "Perpose")
        Status = "ADD"
End Sub

Private Sub Btn_Cancel_Click()
        Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If Lb_Owner_ID.Caption = Empty Then Exit Sub
If MsgBox("�׹�ѹ���ź�������͹حҵ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
Globle_Connective.BeginTrans
            On Error GoTo ErrHandler
                    Call SET_DATABASE("DEL")
                    Call SET_TEXTBOX("DEL")
                    Globle_Connective.CommitTrans
                    Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Edit_Click()
If Lb_Owner_ID.Caption = Empty Then Exit Sub
Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
End Sub

Private Sub Btn_Post_Click()
If CheckBeforPost = False Then Exit Sub
Globle_Connective.BeginTrans
            On Error GoTo ErrHandler
                    Call SET_DATABASE(Status)
                    Call SET_TEXTBOX("POST")
                    Globle_Connective.CommitTrans

Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "Warning!"
End Sub

Private Sub Btn_Refresh_Click()
  Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
 With Frm_Search
      .ZOrder
      .Show
      .Frame1.Visible = False
        .Shp_Menu(1).BackColor = &HD6D6D6
        .Shp_Menu(2).BackColor = &HD6D6D6
        .Shp_Menu(3).BackColor = &HD6D6D6
        .Shp_Menu(4).BackColor = &HD6D6D6
        .Shp_Menu(5).BackColor = &HB8CFD6
        .SSTab1.Tab = 4
        .optCode.Visible = False
        .optName.Visible = False
        .cmd_SendToEdit.Caption = " << ��䢢����� - �͹حҵ"
End With
End Sub

Private Sub Chk_FLAG_STATUS0_Click()
If Chk_FLAG_STATUS0.Value Then
   Chk_FLAG_STATUS1.Value = Unchecked
   DTP_LICENSE_DATE.Enabled = False
    DTP_LICENSE_ENDDATE.Enabled = True
Else
  Chk_FLAG_STATUS1.Value = Checked
End If
End Sub

Private Sub Chk_FLAG_STATUS1_Click()
If Chk_FLAG_STATUS1.Value Then
   Chk_FLAG_STATUS0.Value = Unchecked
    DTP_LICENSE_ENDDATE.Enabled = False
    DTP_LICENSE_DATE.Enabled = True
Else
  Chk_FLAG_STATUS0.Value = Checked
End If
End Sub

Private Sub Chk_STATUS_LINK0_Click()
If Chk_STATUS_LINK0.Value Then
   Chk_STATUS_LINK1.Value = Unchecked
   Chk_STATUS_LINK2.Value = Unchecked
   Cmb_Building_ID.Enabled = False
   Cmb_Land_ID.Enabled = False
            Lb_Tambon.Caption = Empty
            Lb_Village.Caption = Empty
            Lb_Soi.Caption = Empty
            Lb_Street.Caption = Empty
End If
End Sub

Private Sub Chk_STATUS_LINK1_Click()
If Chk_STATUS_LINK1.Value Then
   Chk_STATUS_LINK0.Value = Unchecked
   Chk_STATUS_LINK2.Value = Unchecked
   Cmb_Land_ID.Enabled = True
   Cmb_Building_ID.Enabled = False
            Lb_Tambon.Caption = Empty
            Lb_Village.Caption = Empty
            Lb_Soi.Caption = Empty
            Lb_Street.Caption = Empty
Else
      Cmb_Land_ID.Text = Empty
      Cmb_Land_ID.Enabled = False
End If
End Sub

Private Sub Chk_STATUS_LINK2_Click()
If Chk_STATUS_LINK2.Value Then
   Chk_STATUS_LINK0.Value = Unchecked
   Chk_STATUS_LINK1.Value = Unchecked
   Cmb_Building_ID.Enabled = True
   Cmb_Land_ID.Enabled = False
            Lb_Tambon.Caption = Empty
            Lb_Village.Caption = Empty
            Lb_Soi.Caption = Empty
            Lb_Street.Caption = Empty
            TMP_LAND_ID.Caption = Empty
Else
    Cmb_Building_ID.Text = Empty
    Cmb_Building_ID.Enabled = False
End If
End Sub

Private Sub Cmb_Building_ID_Click()
         TMP_LAND_ID.Caption = Empty
If GBQueryBuildingData.RecordCount > 0 Then
         GBQueryBuildingData.MoveFirst
         GBQueryBuildingData.Find " BUILDING_ID = '" & Cmb_Building_ID.Text & "'", , adSearchForward
         Lb_Tambon.Caption = GBQueryBuildingData.Fields("TAMBON_NAME").Value
         Lb_Village.Caption = GBQueryBuildingData.Fields("VILLAGE_NAME").Value
         Lb_Soi.Caption = GBQueryBuildingData.Fields("SOI_NAME").Value
         Lb_Street.Caption = GBQueryBuildingData.Fields("STREET_NAME").Value
         TMP_LAND_ID.Caption = GBQueryBuildingData.Fields("LAND_ID").Value
End If
End Sub

Private Sub Cmb_Building_ID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub CMB_BUSINESS_TYPE_Click()
   Txt_LICENSE_SUMTAX.Text = Empty
   CMB_BUSINESS_ID.ListIndex = CMB_BUSINESS_TYPE.ListIndex
   GBQueryClass.Filter = "BUSINESS_ID = '" & Left$(CMB_BUSINESS_ID.Text, 3) & "'"
 If GBQueryClass.RecordCount > 0 Then
     CMB_CLASS_DETAILS.Clear
     CMB_CLASS_ID.Clear
     GBQueryClass.MoveFirst
     Do While Not GBQueryClass.EOF
          CMB_CLASS_DETAILS.AddItem GBQueryClass.Fields("�ѡɳСԨ���").Value
          CMB_CLASS_ID.AddItem GBQueryClass.Fields("�ӴѺ���").Value
          GBQueryClass.MoveNext
     Loop
     CMB_CLASS_DETAILS.ListIndex = 0
 End If
End Sub

Private Sub CMB_BUSINESS_TYPE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub CMB_CLASS_DETAILS_Click()
    Txt_LICENSE_SUMTAX.Text = vbNullString
     CMB_CLASS_ID.ListIndex = CMB_CLASS_DETAILS.ListIndex
 If GBQueryClass.RecordCount > 0 Then
    GBQueryClass.MoveFirst
    GBQueryClass.Find "�ӴѺ��� = '" & CMB_CLASS_ID.Text & "'", , adSearchForward
    Txt_LICENSE_SUMTAX.Text = GBQueryClass.Fields("��Ҹ���������ͻ�").Value
 End If
End Sub

Private Sub CMB_CLASS_DETAILS_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = 0
End Sub

Private Sub Cmb_Land_ID_Click()
If Len(Cmb_Land_ID.Text) >= 6 Then
         GBQueryLandData.Filter = " LAND_ID =  '" & Cmb_Land_ID.Text & "'"
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
            Lb_Tambon.Caption = GBQueryLandData.Fields("TAMBON_NAME").Value
            Lb_Village.Caption = GBQueryLandData.Fields("VILLAGE_NAME").Value
            Lb_Soi.Caption = GBQueryLandData.Fields("SOI_NAME").Value
            Lb_Street.Caption = GBQueryLandData.Fields("STREET_NAME").Value
End If
End If
End Sub

Private Sub Cmb_Zoneblock_Click()
            Lb_Tambon.Caption = Empty
            Lb_Village.Caption = Empty
            Lb_Soi.Caption = Empty
            Lb_Street.Caption = Empty
            TMP_LAND_ID.Caption = Empty
             Cmb_Building_ID.Clear
         GBQueryBuildingData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
If GBQueryBuildingData.RecordCount > 0 Then
    Do While Not GBQueryBuildingData.EOF
                  If GBQueryBuildingData.Fields("ZONE_BLOCK").Value <> Empty Then
                    Cmb_Building_ID.AddItem GBQueryBuildingData.Fields("BUILDING_ID").Value
                  End If
                    GBQueryBuildingData.MoveNext
    Loop
End If
         Cmb_Land_ID.Clear
         GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
    Do While Not GBQueryLandData.EOF
                  If GBQueryLandData.Fields("ZONE_BLOCK").Value <> Empty Then
                    Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                  End If
                    GBQueryLandData.MoveNext
    Loop
End If
End Sub

Private Sub Cmb_Zoneblock_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then SendKeys "{Tab}"
     KeyAscii = 0
End Sub

Private Sub DTP_Start_Day_KeyPress(KeyAscii As Integer)
End Sub

Private Sub Form_Activate()
  Clone_Form.Picture1.Visible = True
  Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()
     Call SET_REFRESH
End Sub

Private Sub Txt_LICENSE_NAME_Change()
If Txt_LICENSE_NAME.Text = Empty Then Txt_LICENSE_NAME.Text = "-"
End Sub

Private Sub Txt_LICENSE_REMARK_Change()
If Txt_LICENSE_REMARK.Text = Empty Then Txt_LICENSE_REMARK.Text = "-"
End Sub

Private Sub Txt_LICENSE_SUMTAX_Change()
If Txt_LICENSE_SUMTAX.Text = Empty Then Txt_LICENSE_SUMTAX.Text = "0.00"
End Sub
Private Sub Lb_Owner_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = vbRightButton Then
  If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            PopupMenu Clone_Form.mnuPopup4, vbPopupMenuLeftAlign, Lb_Owner_Name.Left + X, Lb_Owner_Name.Top + Me.Top + Y
  End If
End If
End Sub

Private Sub mnuAddName_Click()
    Status_InToFrm = "LICENSE"
    Frm_SearchOwnerShip.Show
End Sub

Private Sub mnuDelName_Click()
Lb_Owner_Add.Caption = vbNullString
Lb_Owner_ID.Caption = vbNullString
Lb_Owner_Name.Caption = vbNullString
Lb_Owner_Type.Caption = vbNullString
End Sub

Private Sub Txt_LICENSE_AREA_Change()
If Txt_LICENSE_AREA.Text = Empty Then Txt_LICENSE_AREA.Text = "0"
End Sub

Private Sub Txt_LICENSE_AREA_GotFocus()
Txt_LICENSE_AREA.SelStart = 0
Txt_LICENSE_AREA.SelLength = Len(Txt_LICENSE_AREA.Text)
End Sub

Private Sub Txt_LICENSE_AREA_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_LICENSE_BOOK_GotFocus()
Txt_LICENSE_BOOK.SelStart = 0
Txt_LICENSE_BOOK.SelLength = Len(Txt_LICENSE_BOOK.Text)
End Sub

Private Sub Txt_LICENSE_BOOK_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_Change()
If Txt_LICENSE_EMPLOYEE.Text = Empty Then Txt_LICENSE_EMPLOYEE.Text = "0"
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_GotFocus()
Txt_LICENSE_EMPLOYEE.SelStart = 0
Txt_LICENSE_EMPLOYEE.SelLength = Len(Txt_LICENSE_EMPLOYEE.Text)
End Sub

Private Sub Txt_LICENSE_EMPLOYEE_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_ENGINEER_Change()
If Txt_LICENSE_ENGINEER.Text = Empty Then Txt_LICENSE_ENGINEER.Text = "0"
End Sub

Private Sub Txt_LICENSE_ENGINEER_GotFocus()
Txt_LICENSE_ENGINEER.SelStart = 0
Txt_LICENSE_ENGINEER.SelLength = Len(Txt_LICENSE_ENGINEER.Text)
End Sub

Private Sub Txt_LICENSE_ENGINEER_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_LICENSE_NAME_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_LICENSE_NO_GotFocus()
Txt_LICENSE_NO.SelStart = 0
Txt_LICENSE_NO.SelLength = Len(Txt_LICENSE_NO.Text)
End Sub

Private Sub Txt_LICENSE_NO_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
    KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_LICENSE_SUMTAX_KeyPress(KeyAscii As Integer)
    KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
End Sub

Private Sub Txt_License_Tel_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_StoryRemark_Change()
If Txt_StoryRemark.Text = Empty Then Txt_StoryRemark.Text = "-"
End Sub
