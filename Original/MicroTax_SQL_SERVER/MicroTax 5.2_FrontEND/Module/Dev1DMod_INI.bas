Attribute VB_Name = "Dev1DMod_INI"
Option Explicit

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Public iniProvince As String, iniProvince_Id As String, iniAmphoe As String, iniAmphoe_Id As String, iniTambon As String, iniTambon_Id As String, iniPrint_Perform As String, iniPrint_Accept As String, iniPrint_Payment As String
Public iniSection As String, iniAdd, iniTel As String, iniName As String, iniPosition As String
Public iniZone As Byte, iniLand As Byte, iniSignbord As Byte, iniBuliding As Byte
Public ini_landtaxaccept As Byte, ini_landtaxpay As Byte, ini_signbordtaxaccept As Byte, ini_signbordtaxpay As Byte, ini_buildingtaxaccept As Byte, ini_buildingtaxpay As Byte
Public iniPath_Picture As String, iniPath_Map As String, ini_Database  As String, ini_Username As String, ini_Password As String, ini_Servername As String
Public iniAutoRep As Byte, iniAutoTime As Byte

Public Function ReadINI(strsection As String, strKey As String) As String
   Dim strbuffer As String
   Let strbuffer$ = String$(750, Chr$(0&))
   Let ReadINI$ = Left$(strbuffer$, GetPrivateProfileString(strsection$, ByVal LCase$(strKey$), "", strbuffer, Len(strbuffer), App.Path & "\Configs.ini"))
End Function

Public Sub WriteINI(strsection As String, strKey As String, strkeyvalue As String)
    Call WritePrivateProfileString(strsection$, UCase$(strKey$), strkeyvalue$, App.Path & "\Configs.ini")
End Sub

Public Sub SetPathFile()
            ini_Servername = ReadINI("path file", "servername") ' SQL Server name
            ini_Database = ReadINI("path file", "database")          ' Database name in SQL Server
            ini_Username = ReadINI("path file", "username")        ' Login user name
'            ini_Password = ReadINI("path file", "password")        ' Password
            
            ini_Password = GetRegis
            
            iniPath_Map = ReadINI("path file", "pathmap")
            iniPath_Picture = ReadINI("path file", "pathpicture")
'            If LenB(Trim$(iniPath_Database)) = 0 Then
'               iniPath_Database = App.Path & "\database\MicroTax.mdb"
'            End If
            If LenB(Trim$(iniPath_Picture)) = 0 Then
                    iniPath_Picture = App.Path & "\picture"
            End If
            If LenB(Trim$(iniPath_Map)) = 0 Then
                    iniPath_Map = App.Path & "\map\NewMap\Map.swd"
            End If
        
            iniAutoRep = ReadINI("other", "autoreport")
            iniAutoTime = ReadINI("other", "autotime")
            iniPrint_Perform = ReadINI("printing", "print_Perform")
            iniPrint_Accept = ReadINI("printing", "print_Accept")
            iniPrint_Payment = ReadINI("printing", "print_Payment")
            iniTambon = ReadINI("register", "tambon")
            iniTambon_Id = ReadINI("register", "tambon_id")
            iniAmphoe = ReadINI("register", "amphoe")
            iniAmphoe_Id = ReadINI("register", "amphoe_id")
            iniProvince = ReadINI("register", "province")
            iniProvince_Id = ReadINI("register", "province_id")

            iniSection = ReadINI("register owner", "section")
            iniTel = ReadINI("register owner", "telephone")
            iniName = ReadINI("register owner", "name")
            iniPosition = ReadINI("register owner", "position")
            iniAdd = ReadINI("register owner", "address")
            
            ini_landtaxaccept = ReadINI("index theme", "landtaxaccept")
            ini_landtaxpay = ReadINI("index theme", "landtaxpay")
            ini_buildingtaxaccept = ReadINI("index theme", "buildingtaxaccept")
            ini_buildingtaxpay = ReadINI("index theme", "buildingtaxpay")
            ini_signbordtaxaccept = ReadINI("index theme", "signbordtaxaccept")
            ini_signbordtaxpay = ReadINI("index theme", "signbordtaxpay")
            
            iniZone = Abs(ReadINI("index overlay", "zone"))
            iniLand = Abs(ReadINI("index overlay", "land"))
            iniBuliding = Abs(ReadINI("index overlay", "building"))
            iniSignbord = Abs(ReadINI("index overlay", "signboard"))
End Sub
