Attribute VB_Name = "DMod_MeOnly"
Option Explicit

Public Const Character = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/!@#$%()_=<>?.][&"
Public GBQueryVillage                          As ADODB.Recordset
Public GBQuerySoi                                 As ADODB.Recordset
Public GBQueryStreet_Name               As ADODB.Recordset
Public GBQueryZone                             As ADODB.Recordset
Public GBQueryProvince                       As ADODB.Recordset
Public GBQueryAmphoe                        As ADODB.Recordset
Public GBQueryTambon                        As ADODB.Recordset
Public GBQueryOwnerShip                   As ADODB.Recordset
Public GBQueryZoneTax                        As ADODB.Recordset
Public GBQueryLandUse                       As ADODB.Recordset
Public GBQuerySignBordData_Type   As ADODB.Recordset
Public GBQueryLandData                      As ADODB.Recordset
Public GBQuerySignBordData              As ADODB.Recordset
Public GBQueryBusiness                        As ADODB.Recordset
Public GBQueryClass                               As ADODB.Recordset
Public GBQueryBuildingData                 As ADODB.Recordset
Public GBQueryBuildingType                 As ADODB.Recordset
Public GBQueryBuildingRate                  As ADODB.Recordset
Public GBQueryDuePayTax                    As ADODB.Recordset
Public GBQueryLandEmployment         As ADODB.Recordset
Public GBQueryLandChange                  As ADODB.Recordset
Public GBQueryBuildingChange            As ADODB.Recordset
Public GBQuerySignboardChange       As ADODB.Recordset
Public GBQueryBuildingDetails              As ADODB.Recordset
Public GBQueryPetitionDetails              As ADODB.Recordset
Public GBQueryOfficer        As ADODB.Recordset
Public SearchRS As ADODB.Recordset
        
Public Acc_Path As Boolean
Public sFlag As Boolean

Public GB_TmpMenu As String

Public sWidth As Single
Public sHeight As Single
Public originalWidth As Single
Public originalHeight As Single

Public Status_InToFrm As String
Public GB_ImagePath As String

Public itmX   As ListItem
Public swdFile  As String
Public isEdit As Boolean
Public Logic_Frm_Index_Menu As Boolean
Public TProduct As String, TCampaign As String
Public THIT As Byte
Public SumCheckOwner As Integer ', SumCheckLand As Integer

Public Sub Get_Table()
        Set GBQueryVillage = New ADODB.Recordset
        Set GBQuerySoi = New ADODB.Recordset
        Set GBQueryStreet_Name = New ADODB.Recordset
        Set GBQueryZone = New ADODB.Recordset
        Set GBQueryProvince = New ADODB.Recordset
        Set GBQueryAmphoe = New ADODB.Recordset
        Set GBQueryTambon = New ADODB.Recordset
        Set GBQueryOwnerShip = New ADODB.Recordset
        Set GBQueryZoneTax = New ADODB.Recordset
        Set GBQueryLandUse = New ADODB.Recordset
        Set GBQuerySignBordData_Type = New ADODB.Recordset
        Set GBQueryLandData = New ADODB.Recordset
        Set GBQuerySignBordData = New ADODB.Recordset
        Set GBQueryBusiness = New ADODB.Recordset
        Set GBQueryClass = New ADODB.Recordset
        Set GBQueryBuildingData = New ADODB.Recordset
        Set GBQueryBuildingType = New ADODB.Recordset
        Set GBQueryBuildingRate = New ADODB.Recordset
        Set GBQueryDuePayTax = New ADODB.Recordset
        Set GBQueryLandEmployment = New ADODB.Recordset
        Set GBQueryLandChange = New ADODB.Recordset
        Set GBQueryBuildingChange = New ADODB.Recordset
        Set GBQuerySignboardChange = New ADODB.Recordset
        Set SearchRS = New ADODB.Recordset
        Set GBQueryBuildingDetails = New ADODB.Recordset
        Set GBQueryPetitionDetails = New ADODB.Recordset
        Set GBQueryOfficer = New ADODB.Recordset
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "lnitializing Command..."
'              Call SET_QUERY("Select B.TAMBON_NAME, A.*, AMPHOE_ID From VILLAGE A , TAMBON B  WHERE B.TAMBON_ID = A.TAMBON_ID AND AMPHOE_ID = '" & iniAmphoe_Id & "'", GBQueryVillage)
                    Set GBQueryVillage = Globle_Connective.Execute("exec sp_village '" & iniAmphoe_Id & "'", , adCmdUnknown)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "lnitializing Prompt Menu..."
'              Call SET_QUERY("Select  * From STREET ORDER BY STREET_ID", GBQueryStreet_Name)
                    Set GBQueryStreet_Name = Globle_Connective.Execute("sp_street", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "lnitializing Data Report..."
'              Call SET_QUERY("Select  STREET_NAME,SOI_ID,SOI_NAME,A.STREET_ID FROM SOI A , STREET B  WHERE B.STREET_ID = A.STREET_ID", GBQuerySoi)
                    Set GBQuerySoi = Globle_Connective.Execute("sp_soi", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "lnitializing GIS Map..."
'              Call SET_QUERY("Select  * From  LANDZONE ORDER BY ZONE_ID , ZONE_BLOCK ASC ", GBQueryZone)
                    Set GBQueryZone = Globle_Connective.Execute("sp_zone", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Creating Visiualizing Image... "
'              Call SET_QUERY("Select  * From  PROVINCE ORDER BY PROVINCE_NAME  ASC ", GBQueryProvince)
                    Set GBQueryProvince = Globle_Connective.Execute("sp_province", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Creating Connect Database..."
'              Call SET_QUERY("Select  * From  AMPHOE ORDER BY AMPHOE_NAME ASC", GBQueryAmphoe)
                    Set GBQueryAmphoe = Globle_Connective.Execute("sp_amphoe", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Creating Meta Data..."
'              Call SET_QUERY("Select TAMBON_ID,TAMBON_NAME, AMPHOE_ID ,ZIPCODE From  Tambon  ORDER BY TAMBON_ID ASC ", GBQueryTambon)
                    Set GBQueryTambon = Globle_Connective.Execute("sp_tambon", , adCmdStoredProc)
                    Frm_Logo.Refresh
                     Frm_Logo.Label1.Caption = "Loading to dll..."
'              Call SET_QUERY("SELECT  PRENAME, OWNER_NAME, OWNER_SURNAME, OWNER_NUMBER,ID_GARD,  ADD_HOME ,  ADD_MOO, ADD_SOI, ADD_ROAD, TAMBON_NAME,AMPHOE_NAME, PROVINCE_NAME, TELEPHONE, EMAIL, OWNER_TYPE, OWNERSHIP_REAL_ID , OWNERSHIP_ID ,TAMBON_ID,ZIPCODE " & _
'                                                " FROM OWNERSHIP  ORDER BY OWNER_NAME ", GBQueryOwnerShip)
                    Set GBQueryOwnerShip = Globle_Connective.Execute("sp_ownership", , adCmdStoredProc)
                    SumCheckOwner = GBQueryOwnerShip.RecordCount
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Reading License Map..."
'              Call SET_QUERY("Select  * From  Zone_Tax  ORDER BY ZoneTax_ID ASC ", GBQueryZoneTax)
                    Set GBQueryZoneTax = Globle_Connective.Execute("sp_zonetax", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Loading Method..."
'              Call SET_QUERY("SELECT LANDUSE.*, LAND_EMPLOYMENT.LM_Details FROM LANDUSE INNER JOIN LAND_EMPLOYMENT ON LANDUSE.LM_ID = LAND_EMPLOYMENT.LM_ID ORDER BY ZoneTax_ID ASC, LANDUSE.LM_ID ASC", GBQueryLandUse)
                    Set GBQueryLandUse = Globle_Connective.Execute("sp_landuse", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Loading to Memory..."
'              Call SET_QUERY("Select  * From  SIGNBORDDATA_TYPE ORDER BY SIGNBORD_TYPE_ID ASC", GBQuerySignBordData_Type)
                    Set GBQuerySignBordData_Type = Globle_Connective.Execute("sp_signboarddata_type", , adCmdStoredProc)
                    Frm_Logo.Refresh
                    Frm_Logo.Label1.Caption = "Loading Object..."
'              Call SET_QUERY("SELECT A.* , B.TAMBON_NAME , C.STREET_NAME ,D.SOI_NAME , VILLAGE_NAME FROM LANDDATA A , TAMBON B , STREET C , SOI D , VILLAGE E Where B.TAMBON_ID = A.TAMBON_ID And C.STREET_ID = A.STREET_ID And D.SOI_ID = A.SOI_ID And E.VILLAGE_ID = A.VILLAGE_ID ORDER BY LAND_ID ", GBQueryLandData)
'              Call SET_QUERY("SELECT  A.LAND_ID,A.ZONE_BLOCK,A.LAND_NOTIC,A.LAND_TYPE,TAMBON_NAME ,STREET_NAME,SOI_NAME,VILLAGE_NAME  FROM LANDDATA A , TAMBON B , STREET C , SOI D , VILLAGE E Where B.TAMBON_ID = A.TAMBON_ID And C.STREET_ID = A.STREET_ID And D.SOI_ID = A.SOI_ID And E.VILLAGE_ID = A.VILLAGE_ID ORDER BY LAND_ID ", GBQueryLandData)
                    Set GBQueryLandData = Globle_Connective.Execute("sp_landdata", , adCmdStoredProc)
'                     SumCheckLand = GBQueryLandData.RecordCount
                     Frm_Logo.Refresh
                     Frm_Logo.Label1.Caption = "Reading License Product..."
'              Call SET_QUERY("SELECT ZONE_BLOCK,LAND_ID,SIGNBORD_ID  FROM SIGNBORDDATA", GBQuerySignBordData)
                    Set GBQuerySignBordData = Globle_Connective.Execute("sp_signboarddata", , adCmdStoredProc)
'              Call SET_QUERY("SELECT A.* ,B.* FROM SIGNBORDDATA A , SIGNBORDDATA_DETAILS  B Where B.SIGNBORD_ID = A.SIGNBORD_ID", GBQuerySignBordData)
'              Call SET_QUERY("SELECT * FROM BUSINESS_TYPE ORDER BY BUSINESS_ID ", GBQueryBusiness)
                    Set GBQueryBusiness = Globle_Connective.Execute("sp_business", , adCmdStoredProc)
'              Call SET_QUERY("SELECT A.CLASS_ID AS �ӴѺ���,A.CLASS_DETAILS AS �ѡɳСԨ���,A.CLASS_PRICE AS ��Ҹ���������ͻ�,A.BUSINESS_ID FROM CLASS_DETAILS A  , BUSINESS_TYPE B WHERE B.BUSINESS_ID = A.BUSINESS_ID ORDER BY CLASS_ID ", GBQueryClass)
                    Set GBQueryClass = Globle_Connective.Execute("sp_class", , adCmdStoredProc)
'              Call SET_QUERY("SELECT M.* , B.TAMBON_NAME ,C.STREET_NAME ,D.SOI_NAME ,VILLAGE_NAME FROM LANDDATA A , TAMBON B , STREET C , SOI D , VILLAGE E  , BUILDINGDATA M " &
'              Call SET_QUERY("SELECT M.BUILDING_ID,M.ZONE_BLOCK,M.LAND_ID,TAMBON_NAME,STREET_NAME,SOI_NAME,VILLAGE_NAME  FROM LANDDATA A , TAMBON B , STREET C , SOI D , VILLAGE E  , BUILDINGDATA M " & _
'                                                " WHERE B.TAMBON_ID = A.TAMBON_ID And C.STREET_ID = A.STREET_ID And D.SOI_ID = A.SOI_ID And E.VILLAGE_ID = A.VILLAGE_ID And M.LAND_ID = A.LAND_ID  ORDER BY BUILDING_ID ", GBQueryBuildingData)
                    Set GBQueryBuildingData = Globle_Connective.Execute("sp_buildingdata", , adCmdStoredProc)
'              Call SET_QUERY("SELECT * FROM  BUILDING_SHAPE WHERE BUILDING_SHAPE_ID IS NOT NULL ORDER BY BUILDING_SHAPE_ID", GBQueryBuildingType)
                    Set GBQueryBuildingType = Globle_Connective.Execute("sp_buildingtype", , adCmdStoredProc)
'              Call SET_QUERY("SELECT * FROM  BUILDING_RATE  WHERE BUILDING_SHAPE_ID IS NOT NULL  ORDER BY BUILDING_SHAPE_ID", GBQueryBuildingRate)
                    Set GBQueryBuildingRate = Globle_Connective.Execute("sp_buildingrate", , adCmdStoredProc)
                        Frm_Logo.Refresh
                        Frm_Logo.Label1.Caption = "Loading to Program..."
'              Call SET_QUERY("SELECT * FROM CONFIG_TAXRATE ", GBQueryDuePayTax)
                    Set GBQueryDuePayTax = Globle_Connective.Execute("sp_duepaytax", , adCmdStoredProc)
'              Call SET_QUERY("SELECT * FROM LAND_EMPLOYMENT WHERE LM_ID > 'LM-04' ORDER BY LM_ID ASC ", GBQueryLandEmployment)
                    Set GBQueryLandEmployment = Globle_Connective.Execute("sp_landemployment", , adCmdStoredProc)
                    Set GBQueryLandChange = Globle_Connective.Execute("sp_land_change", , adCmdStoredProc)
                    Set GBQueryBuildingChange = Globle_Connective.Execute("sp_building_change", , adCmdStoredProc)
                    Set GBQuerySignboardChange = Globle_Connective.Execute("sp_signboard_change", , adCmdStoredProc)
                    Set GBQueryBuildingDetails = Globle_Connective.Execute("sp_building_type", , adCmdStoredProc)
                    Set GBQueryPetitionDetails = Globle_Connective.Execute("sp_petition_type", , adCmdStoredProc)
                    Set GBQueryOfficer = Globle_Connective.Execute("sp_officer", , adCmdStoredProc)
End Sub

Public Function BETWEEN_OWNERCODE(ASCII_CODE As Byte) As Byte
Dim BUFF_STR As String
          BUFF_STR = Chr$(ASCII_CODE)
Select Case BUFF_STR
             Case "�", "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 1
             Case "�", "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 2
             Case "�", "�", "�", "�", "�"
                         BETWEEN_OWNERCODE = 3
            Case "�", "�", "�", "�", "�"
                         BETWEEN_OWNERCODE = 4
             Case "�", "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 5
             Case "�", "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 6
            Case "�", "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 7
            Case "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 8
            Case "�", "�", "�", "�"
                        BETWEEN_OWNERCODE = 9
End Select
End Function

Public Function GENERATE_OWNERCODE(Owner_Type As Byte, STR_NAME As String, Optional STR_SURNAME As String) As String
Dim i As Byte, Check  As Byte
Dim CODE As String, Digit_First As String
Check = 0
            For i = 1 To Len(STR_NAME)
                    If Check < 4 Then
                            If Asc(Mid(STR_NAME, i, 1)) >= 161 And Asc(Mid(STR_NAME, i, 1)) <= 206 Then
                                    If Check >= 1 Then
                                        CODE = CODE & BETWEEN_OWNERCODE(Asc(Mid(STR_NAME, i, 1)))
                                    End If
                                                Check = Check + 1
                                                If Check = 1 Then
                                                    Digit_First = Mid(STR_NAME, i, 1)
                                                End If
                        End If
                  End If
            Next i
                  If Check <= 3 Then
                     STR_NAME = Digit_First & CODE & String$(4 - Check, "0")
                  Else
                     STR_NAME = Digit_First & CODE
                  End If

Check = 0: CODE = Empty
         For i = 1 To Len(STR_SURNAME)
                    If Check < 4 Then
                            If Asc(Mid(STR_SURNAME, i, 1)) >= 161 And Asc(Mid(STR_SURNAME, i, 1)) <= 206 Then
                                        CODE = CODE & BETWEEN_OWNERCODE(Asc(Mid(STR_SURNAME, i, 1)))
                                        Check = Check + 1
                        End If
                  End If
            Next i
                  If Check <= 4 Then
                     STR_SURNAME = CODE & String$(4 - Check, "0")
                  Else
                     STR_SURNAME = CODE
                  End If
If Owner_Type = 1 Then
                      GENERATE_OWNERCODE = STR_NAME & "/" & STR_SURNAME
Else
                       GENERATE_OWNERCODE = GENERATE_OWNERTYPE(Owner_Type, True)
End If
End Function

Public Function GENERATE_OWNERTYPE(Owner_Type As Byte, RunNumber As Boolean) As String

Select Case Owner_Type
            Case 1
                    If RunNumber Then
                       GENERATE_OWNERTYPE = "PO"
                    End If
            Case 2
                    If RunNumber Then
                       GENERATE_OWNERTYPE = "CO"
                    Else
                       GENERATE_OWNERTYPE = "� � "
                    End If
            Case 3
                    If RunNumber Then
                       GENERATE_OWNERTYPE = "PP"
                    Else
                       GENERATE_OWNERTYPE = "� � "
                    End If
            Case 4
                    If RunNumber Then
                      GENERATE_OWNERTYPE = "GO"
                    Else
                       GENERATE_OWNERTYPE = "� � "
                    End If
            Case 5
                   If RunNumber Then
                    GENERATE_OWNERTYPE = "SG"
                   Else
                    GENERATE_OWNERTYPE = "� � "
                   End If
            Case 6
                  If RunNumber Then
                     GENERATE_OWNERTYPE = "SO"
                  Else
                     GENERATE_OWNERTYPE = "�.�. "
                  End If
            Case 7
                If RunNumber Then
                   GENERATE_OWNERTYPE = "FO"
                Else
                  GENERATE_OWNERTYPE = "�.�. "
               End If
            Case 8
                 If RunNumber Then
                    GENERATE_OWNERTYPE = "TO"
                 Else
                   GENERATE_OWNERTYPE = "�.�. "
                 End If
            Case 9
                If RunNumber Then
                    GENERATE_OWNERTYPE = "OT"
               Else
                    GENERATE_OWNERTYPE = "�.�. "
               End If
End Select
End Function

Public Function Land_Type(Land_TypeOrder As Byte) As String
     Select Case Land_TypeOrder
                  Case 1
                          Land_Type = "⩹�"
                         Case 2
                            Land_Type = "�.�. 3 �"
                         Case 3
                              Land_Type = "�.�. 3"
                         Case 4
                               Land_Type = "�.�. 1"
                         Case 5
                              Land_Type = "��Ҩͧ"
                         Case 6
                               Land_Type = "ʻ�."
                         Case 7
                               Land_Type = "��."
                         Case 8
                               Land_Type = "�.�.�"
                       Case 9
                               Land_Type = "��� �"
            End Select
End Function

Public Function Owner_Type(Owner_TypeOrder As Byte) As String
     Select Case Owner_TypeOrder
                  Case 1
                          Owner_Type = "�ؤ�Ÿ�����"
                         Case 2
                            Owner_Type = "����ѷ"
                         Case 3
                              Owner_Type = "��ҧ�����ǹ"
                         Case 4
                               Owner_Type = "�Ѱ���"
                         Case 5
                              Owner_Type = "�Ѱ����ˡԨ"
                         Case 6
                               Owner_Type = "��Ҥ�"
                         Case 7
                               Owner_Type = "��ŹԸ�"
                         Case 8
                               Owner_Type = "�Ѵ"
                       Case 9
                               Owner_Type = "��� �"
            End Select
End Function

Public Function Enzinc_Password(Input_Decode As String, STATE As String) As String
Const Enum_Valid = 14 'Input Limit 14 Charector Password
Const EnumBeric = "53974618225237"    'Static String !! Don't Change
Const EnumString = "Micro-Tax"                   'Static String !! Don't Change

Dim i As Byte, j As Byte
Dim Output_Decode  As String
Dim Joke_String    As String

If STATE = "ENCODE" Then
For i = 1 To Len(Input_Decode)
     j = Enum_Valid - (Len(Input_Decode) - i)
     j = Mid$(EnumBeric, j, 1)
     Output_Decode = Output_Decode & Chr(Asc(Mid$(Input_Decode, i, 1)) + (Asc(Mid$(EnumString, j, 1))))
     If i <= 4 Then
        Joke_String = Joke_String & Chr((Asc(Mid$(EnumString, j, 1))))
     End If
Next i
     Enzinc_Password = Output_Decode & Joke_String
     Exit Function
ElseIf STATE = "DECODE" Then
     For i = 1 To (Len(Input_Decode) - 4)
         j = Enum_Valid - ((Len(Input_Decode) - 4) - i)
         j = Mid$(EnumBeric, j, 1)
         Output_Decode = Output_Decode & Chr(Asc(Mid$(Input_Decode, i, 1)) - (Asc(Mid$(EnumString, j, 1))))
Next i
         Enzinc_Password = Output_Decode
End If
End Function

Public Function SubsetHit(JokeHit As String, STATE As String) As String
   If STATE = "ENCODE" Then
     Select Case JokeHit
                  Case "0"
                         SubsetHit = "#?x@"
                  Case "1"
                         SubsetHit = "j=(+"
                  Case "2"
                         SubsetHit = "<!&�"
                  Case "3"
                        SubsetHit = "%-\!"
                  Case "4"
                           SubsetHit = "$(*>"
                  Case "5"
                           SubsetHit = ",?@&"
                  Case "6"
                           SubsetHit = ":([^"
                  Case "7"
                        SubsetHit = ".!;]"
                  Case "8"
                        SubsetHit = "|@/&"
                  Case "9"
                         SubsetHit = "^*}!"
         End Select
   End If
   
   If STATE = "DECODE" Then
         Select Case JokeHit
                  Case "#?x@"
                         SubsetHit = "0"
                  Case "j=(+"
                         SubsetHit = "1"
                  Case "<!&�"
                         SubsetHit = "2"
                  Case "%-\!"
                        SubsetHit = "3"
                  Case "$(*>"
                           SubsetHit = "4"
                  Case ",?@&"
                           SubsetHit = "5"
                  Case ":([^"
                           SubsetHit = "6"
                  Case ".!;]"
                        SubsetHit = "7"
                  Case "|@/&"
                        SubsetHit = "8"
                  Case "^*}!"
                         SubsetHit = "9"
         End Select
   End If
End Function

Function fConvert(num As Currency) As String
Dim mStr As String, mRight As String, mLeft As String, strTemp As String, aDigit As String
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim dFlag As Boolean

mRight = Empty: strTemp = Empty
dFlag = False
mStr = Trim$(str(num))
If mStr = Empty Then
       fConvert = Empty
End If
       
       i = InStr(mStr, ".")
       If i > 0 Then
              mRight = Right$(mStr, Len(mStr) - i)
              mStr = Left$(mStr, i - 1)
               i = Len(mRight)
               If i > 2 Then
                 mRight = Left$(mRight, 2)
                 i = 2
               ElseIf i = 1 Then
                  mRight = mRight + "0"
                  i = 2
               End If
               
               Do Until i = 0
                      aDigit = Left$(mRight, 1)
                      If (i = 8 Or i = 2 Or i = 14) And aDigit <> "0" Then
                               dFlag = True
                      ElseIf i = 6 Or i = 12 Or i = 18 Then
                                dFlag = False
                      End If
                             strTemp = strTemp + fChange(aDigit, (i), dFlag)
                             mRight = Right$(mRight, i - 1)
                             i = i - 1
               Loop
      End If
              dFlag = False
              i = Len(mStr)
              Do Until i = 0
                     aDigit = Left$(mStr, 1)
                      If (i = 8 Or i = 2 Or i = 14) And aDigit <> "0" Then
                               dFlag = True
                      ElseIf i = 6 Or i = 12 Or i = 18 Then
                                dFlag = False
                      End If
                      mLeft = mLeft + fChange(aDigit, (i), dFlag)
                      mStr = Right$(mStr, i - 1)
                      i = i - 1
              Loop
              If strTemp = Empty Then
                          fConvert = mLeft + "�ҷ��ǹ"
              ElseIf mLeft = " " Then
                         fConvert = strTemp + "ʵҧ��"
              Else
                    fConvert = mLeft + "�ҷ" + strTemp + "ʵҧ��"
              End If
End Function

Function fChange(aDigit As String, aCount As Integer, aFlag As Boolean) As String
Dim fDigit As String, fCount As String
Select Case aDigit
             Case "1"
                        If (aCount = 1 Or aCount = 7 Or aCount = 13) And aFlag Then
                              fDigit = "���"
                        ElseIf (aCount = 2 Or aCount = 8 Or aCount = 14) Then
                                   fDigit = Empty
                       Else
                                   fDigit = "˹��"
                        End If
             Case "2"
                        If (aCount = 2 Or aCount = 8 Or aCount = 14) And aFlag Then
                              fDigit = "���"
                        ElseIf (aCount = 2 Or aCount = 8 Or aCount = 14) Then
                                   fDigit = Empty
                       Else
                                   fDigit = "�ͧ"
                        End If
             Case "3"
                         fDigit = "���"
             Case "4"
                         fDigit = "���"
             Case "5"
                         fDigit = "���"
             Case "6"
                         fDigit = "ˡ"
             Case "7"
                         fDigit = "��"
             Case "8"
                         fDigit = "Ỵ"
             Case "9"
                         fDigit = "���"
             Case "0"
                        If aCount = 7 Or aCount = 13 Or aCount = 19 Then
                            fDigit = "��ҹ"
                        End If
              Case Else
                       fDigit = " "
End Select
If aDigit <> "0" Then
        Select Case aCount
                     Case 2, 8, 14
                                fCount = "�Ժ"
                     Case 3, 9, 15
                                fCount = "����"
                     Case 4, 10, 16
                                fCount = "�ѹ"
                     Case 5, 11, 17
                                fCount = "����"
                     Case 6, 12, 18
                                fCount = "�ʹ"
                     Case 7, 13, 19
                                fCount = "��ҹ"
                     Case Else
                                fCount = Empty
         End Select
End If
     fChange = fDigit + fCount
End Function
