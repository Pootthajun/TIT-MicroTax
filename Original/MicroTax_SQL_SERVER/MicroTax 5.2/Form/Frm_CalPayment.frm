VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_CalPayment 
   Caption         =   ":: ��������¡�ä�ҧ���� ::"
   ClientHeight    =   4275
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Frm_CalPayment.frx":0000
   ScaleHeight     =   4275
   ScaleWidth      =   9180
   StartUpPosition =   2  'CenterScreen
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GridPayment 
      Height          =   2565
      Left            =   135
      TabIndex        =   0
      Top             =   840
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   4524
      _Version        =   393216
      BackColor       =   16777215
      Cols            =   5
      FixedCols       =   0
      BackColorFixed  =   15790320
      BackColorSel    =   14073244
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   15461351
      GridColorFixed  =   15856113
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   5
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Label Lb_SetGK 
      Caption         =   "Lb_SetGK"
      Height          =   255
      Left            =   7920
      TabIndex        =   3
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Label LabelSumAllAmount 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   3120
      TabIndex        =   2
      Top             =   3720
      Width           =   2175
   End
   Begin VB.Label Lb_Name 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ�Թ����ҧ���з����� :                                             �ҷ (�Ҥһ����Թ)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Index           =   3
      Left            =   480
      TabIndex        =   1
      Top             =   3720
      Width           =   6720
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   465
      Left            =   240
      Shape           =   4  'Rounded Rectangle
      Top             =   3600
      Width           =   8715
   End
   Begin VB.Image Image1 
      Height          =   405
      Left            =   8640
      Picture         =   "Frm_CalPayment.frx":810D4
      Top             =   150
      Width           =   405
   End
End
Attribute VB_Name = "Frm_CalPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rsPayMent As New ADODB.Recordset
Dim strOwnerShipID As String

Private Sub Form_Load()
    SetGrid
End Sub


Private Sub GridPayment_DblClick()
Dim strYear As String
Dim strSQL As String
Dim chkStatusType As Integer
Dim i As Integer

         strYear = GridPayment.TextMatrix(GridPayment.RowSel, 0)
         Frm_CalAllTax.LbYearSelect = strYear
         Frm_CalAllTax.Lb_Year = strYear
         Frm_CalAllTax.Label3(31) = "/" & Right(strYear, 2)
         Frm_CalAllTax.Label3(28) = "/" & Right(strYear, 2)
         Frm_CalAllTax.Label3(33) = "/" & Right(strYear, 2)
         
'         strSQL = "SELECT OWNERSHIP.OWNERSHIP_ID, OWNERSHIP.PRENAME, OWNERSHIP.OWNER_NAME & ' '  & OWNERSHIP.OWNER_SURNAME as fullName FROM OWNERSHIP" & _
'                            " Where OWNERSHIP_ID = '" & strOwnerShipID & "'"
'        Call SET_QUERY(strSQL, rsPayMent)
        Set rsPayMent = Globle_Connective.Execute("exec sp_search_OwnerPayMent '" & strOwnerShipID & "'", adCmdUnknown)
        With Frm_CalAllTax.Grid_ListName
                i = 1
                               .Rows = rsPayMent.RecordCount + 1
                                Do While Not rsPayMent.EOF
                                        .TextMatrix(i, 0) = rsPayMent("PRENAME") & " " & rsPayMent("fullName")
                                        .TextMatrix(i, 1) = rsPayMent("OWNERSHIP_ID")
                                i = i + 1
                                rsPayMent.MoveNext
                                Loop
                            Frm_CalAllTax.Lb_ResultSchNm.Caption = Format(rsPayMent.RecordCount, "#,##0")
        End With
        
        strSQL = ""
         
         Call Frm_CalAllTax.chkStatusNum(strOwnerShipID, strYear)
         
'With Frm_CalAllTax.Grid_TaxCount
'                            '************************ PBT5 **********************************
''                            strSQL = "SELECT Count(PBT5.LAND_ID) AS Count_ID From PBT5 WHERE ((PBT5.OWNERSHIP_ID)='" & strOwnerID & "') And ((PBT5.PBT5_YEAR)=" & CInt(strYear) & ") And  ((PBT5.PBT5_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
''                                            " And ((PBT5.PBT5_STATUS) =0)"
''                             Call SET_QUERY(strSQL, Rs)
'                            Set rsPayMent = Globle_Connective.Execute("sp_count_numOwner " & strOwnerShipID & "," & strYear & ",'" & Lb_SetGK.Caption & "', 1", adCmdUnknown)
'
'                            If rsPayMent.RecordCount > 0 Then
'                                    .TextMatrix(1, 1) = Format(rsPayMent("Count_ID"), "#,##0")
'                            Else
'                                    .TextMatrix(1, 1) = 0
'                            End If
'                            '******************** PRD2 *****************************************
''                            strSQL = "SELECT Count(PRD2.BUILDING_ID) as Count_ID From PRD2 WHERE ((PRD2.OWNERSHIP_ID)='" & strOwnerID & "') And ((PRD2.PRD2_YEAR)=" & CInt(strYear) & ") And  ((PRD2.PRD2_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
''                                        " And ((PRD2.PRD2_STATUS) =0)"
''                             Call SET_QUERY(strSQL, Rs)
'                            Set rsPayMent = Globle_Connective.Execute("sp_count_numOwner " & strOwnerShipID & "," & strYear & ",'" & Lb_SetGK.Caption & "', 2", adCmdUnknown)
'
'                            If rsPayMent.RecordCount > 0 Then
'                                    .TextMatrix(2, 1) = Format(rsPayMent("Count_ID"), "#,##0")
'                            Else
'                                    .TextMatrix(2, 1) = 0
'                            End If
'
'                            '************************PP1**************************************
''                             strSQL = "SELECT Count(SIGNBORD_ID) AS Count_ID From PP1 WHERE ((PP1.OWNERSHIP_ID)='" & strOwnerID & "') And ((PP1.PP1_YEAR)=" & CInt(strYear) & ") And  ((PP1.PP1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
''                                                " And ((PP1.PP1_STATUS) =0)"
''                             Call SET_QUERY(strSQL, Rs)
'                            Set rsPayMent = Globle_Connective.Execute("sp_count_numOwner " & strOwnerShipID & "," & strYear & ",'" & Lb_SetGK.Caption & "', 3", adCmdUnknown)
'
'                            If rsPayMent.RecordCount > 0 Then
'                                    .TextMatrix(3, 1) = Format(rsPayMent("Count_ID"), "#,##0")
'                            Else
'                                    .TextMatrix(3, 1) = 0
'                            End If
'
'                             '************************PBA1**************************************
''                            strSQL = "SELECT LICENSE_BOOK,LICENSE_NO From PBA1 WHERE ((PBA1.OWNERSHIP_ID)='" & strOwnerID & "') And ((PBA1.PBA1_YEAR)=" & CInt(strYear) & ") And  ((PBA1.PBA1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
''                                            " And ((PBA1.PBA1_STATUS)=0)"
''                            Call SET_QUERY(strSQL, Rs)
'                            Set rsPayMent = Globle_Connective.Execute("sp_count_numOwner " & strOwnerShipID & "," & strYear & ",'" & Lb_SetGK.Caption & "', 4", adCmdUnknown)
'
'                            If rsPayMent.RecordCount > 0 Then
'                                   .TextMatrix(4, 1) = Format(rsPayMent.RecordCount, "#,##0")
'                            Else
'                                   .TextMatrix(4, 1) = 0
'                            End If
'                End With
                
         Unload Me
End Sub

Private Sub Image1_Click()
        Unload Me
End Sub

Private Sub SetGrid()
    With GridPayment
                        .TextArray(0) = "�շ���ҧ����": .ColWidth(0) = 1500: .ColAlignment(0) = 4: .ColAlignmentFixed(0) = 4
                        .TextArray(1) = "���պ��ا��ͧ���(�ҷ)": .ColWidth(1) = 1700: .ColAlignment(1) = 4: .ColAlignmentFixed(1) = 4
                        .TextArray(2) = "�����ç���͹(�ҷ)": .ColWidth(2) = 1700: .ColAlignment(2) = 4: .ColAlignmentFixed(2) = 4
                       .TextArray(3) = "���ջ���(�ҷ)": .ColWidth(3) = 1700: .ColAlignment(3) = 4: .ColAlignmentFixed(3) = 4
                       .TextArray(4) = "�ӹǹ�Թ����ҧ����(�ҷ)": .ColWidth(4) = 2300: .ColAlignment(4) = 4: .ColAlignmentFixed(4) = 4
        End With
End Sub


Public Sub sumAmountAllData(strOwnerID As String, strYear As String, LbMenuTag As Integer)
        Dim strSQL As String
        Dim i As Integer
        Dim sumAmountYear As Currency
        Dim sumAmountAll As Currency
        
            Set rsPayMent = Globle_Connective.Execute("exec sp_search_sumAmountAllData '" & strOwnerID & "', '" & strYear & "'", adCmdUnknown)
'        strSQL = "SELECT A.OWNERSHIP_ID, A.PBT5_YEAR,  iif(dtPBT5.sumAmountPBT<> Null,dtPBT5.sumAmountPBT,'0.00') as sumAmountPBT, iif(dtPRD2.sumAmountPRD2 <> Null,dtPRD2.sumAmountPRD2 ,'0.00') as sumAmountPRD2,iif(dtPP1.sumAmountPP1<> Null,dtPP1.sumAmountPP1,'0.00') as sumAmountPP1" & _
'                        " FROM (([SELECT PBT5.OWNERSHIP_ID,PBT5.PBT5_YEAR From PBT5 WHERE (((PBT5.PBT5_YEAR)<" & strYear & ") AND ((PBT5.PBT5_STATUS)=0) AND ((PBT5.OWNERSHIP_ID)='" & strOwnerID & "'))" & _
'                        " Group by  PBT5.OWNERSHIP_ID,PBT5.PBT5_YEAR Union SELECT PRD2.OWNERSHIP_ID, PRD2.PRD2_YEAR From PRD2" & _
'                        " GROUP BY PRD2.OWNERSHIP_ID, PRD2.PRD2_YEAR, PRD2.PRD2_STATUS Having (((PRD2.OWNERSHIP_ID) = '" & strOwnerID & "') And ((PRD2.PRD2_YEAR) < " & strYear & ") And ((PRD2.PRD2_STATUS) = 0))" & _
'                        " Union SELECT PP1.OWNERSHIP_ID, PP1.PP1_YEAR From PP1 Where (((PP1.OWNERSHIP_ID) = '" & strOwnerID & "') And ((PP1.PP1_YEAR) < " & strYear & ") And ((PP1.PP1_STATUS) = 0))" & _
'                        " Group by PP1.OWNERSHIP_ID, PP1.PP1_YEAR]. AS A LEFT JOIN" & _
'                        " [SELECT PBT5.OWNERSHIP_ID,PBT5.PBT5_YEAR,sum(PBT5.PBT5_AMOUNT_ACCEPT) as sumAmountPBT From PBT5 WHERE (((PBT5.PBT5_YEAR)<" & strYear & ") AND ((PBT5.PBT5_STATUS)=0) AND ((PBT5.OWNERSHIP_ID)='" & strOwnerID & "'))" & _
'                        " Group by  PBT5.OWNERSHIP_ID,PBT5.PBT5_YEAR]. AS dtPBT5 ON (A.OWNERSHIP_ID = dtPBT5.OWNERSHIP_ID) AND (A.PBT5_YEAR = dtPBT5.PBT5_YEAR)) LEFT JOIN" & _
'                        " [SELECT PRD2.OWNERSHIP_ID,PRD2.PRD2_YEAR, Sum(PRD2.PRD2_AMOUNT_ACCEPT) as sumAmountPRD2 From PRD2" & _
'                        " WHERE (((PRD2.PRD2_YEAR)<" & strYear & ") AND ((PRD2.PRD2_STATUS)=0) AND ((PRD2.OWNERSHIP_ID)='" & strOwnerID & "')) " & _
'                        " Group by PRD2.OWNERSHIP_ID,PRD2.PRD2_YEAR]. AS dtPRD2 ON (A.OWNERSHIP_ID = dtPRD2.OWNERSHIP_ID) AND (A.PBT5_YEAR = dtPRD2.PRD2_YEAR)) LEFT JOIN" & _
'                        " [SELECT PP1.OWNERSHIP_ID, PP1.PP1_YEAR, sum(PP1.PP1_AMOUNT_ACCEPT) as sumAmountPP1 From PP1" & _
'                        " WHERE (((PP1.OWNERSHIP_ID)='" & strOwnerID & "') AND ((PP1.PP1_YEAR)<" & strYear & ") AND ((PP1.PP1_STATUS)=0))" & _
'                        " Group by PP1.OWNERSHIP_ID,PP1.PP1_YEAR]. AS dtPP1 ON (A.OWNERSHIP_ID = dtPP1.OWNERSHIP_ID) AND (A.PBT5_YEAR = dtPP1.PP1_YEAR)"
'
'        Call SET_QUERY(strSQL, rsPayMent)
        If rsPayMent.RecordCount > 0 Then
                With GridPayment
                               i = 1
                                .Rows = rsPayMent.RecordCount + 1
                               Do While Not rsPayMent.EOF
                                    .TextMatrix(i, 0) = rsPayMent("PBT5_YEAR")
                                    .TextMatrix(i, 1) = Format$(rsPayMent("sumAmountPBT"), "#,##0.00")
                                    .TextMatrix(i, 2) = Format$(rsPayMent("sumAmountPRD2"), "#,##0.00")
                                    .TextMatrix(i, 3) = Format$(rsPayMent("sumAmountPP1"), "#,##0.00")
                                    sumAmountYear = Format$(CCur(rsPayMent("sumAmountPBT")) + CCur(rsPayMent("sumAmountPRD2")) + CCur(rsPayMent("sumAmountPP1")), "#,##0.00")
                                    .TextMatrix(i, 4) = Format$(sumAmountYear, "#,##0.00")
                                    sumAmountAll = Format$(sumAmountAll + sumAmountYear, "#,##0.00")
                                i = i + 1
                               rsPayMent.MoveNext
                               Loop
                               LabelSumAllAmount.Caption = Format$(sumAmountAll, "#,##0.00")
                End With
        End If
        strOwnerShipID = strOwnerID
End Sub


