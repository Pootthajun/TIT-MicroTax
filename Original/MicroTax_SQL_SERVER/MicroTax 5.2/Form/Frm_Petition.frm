VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Petition 
   ClientHeight    =   8895
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12900
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Petition.frx":0000
   ScaleHeight     =   8895
   ScaleWidth      =   12900
   WindowState     =   2  'Maximized
   Begin VB.CheckBox Check2 
      BackColor       =   &H00D6D6D6&
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   10410
      TabIndex        =   42
      Top             =   2040
      Width           =   195
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   8310
      TabIndex        =   41
      Top             =   2040
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Petition.frx":17D4C
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_Petition.frx":185B3
      Style           =   1  'Graphical
      TabIndex        =   37
      TabStop         =   0   'False
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Print 
      BackColor       =   &H00E0E0E0&
      Height          =   405
      Left            =   11670
      Picture         =   "Frm_Petition.frx":1AEAA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1290
      Width           =   1155
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Petition.frx":1B594
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_Petition.frx":1BDFD
      Style           =   1  'Graphical
      TabIndex        =   35
      TabStop         =   0   'False
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Petition.frx":1E5F5
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_Petition.frx":1EE23
      Style           =   1  'Graphical
      TabIndex        =   34
      TabStop         =   0   'False
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Petition.frx":216AD
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_Petition.frx":21F9C
      Style           =   1  'Graphical
      TabIndex        =   33
      TabStop         =   0   'False
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_Petition.frx":24AF7
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_Petition.frx":253A9
      Style           =   1  'Graphical
      TabIndex        =   32
      TabStop         =   0   'False
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Petition.frx":27DE4
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_Petition.frx":28647
      Style           =   1  'Graphical
      TabIndex        =   31
      TabStop         =   0   'False
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Petition.frx":2B06C
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_Petition.frx":2B903
      Style           =   1  'Graphical
      TabIndex        =   30
      TabStop         =   0   'False
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.TextBox Txt_BOOK_NO 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   3810
      MaxLength       =   12
      TabIndex        =   28
      Top             =   4080
      Width           =   1635
   End
   Begin VB.ComboBox cmb_Officer_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Petition.frx":2E1E8
      Left            =   120
      List            =   "Frm_Petition.frx":2E20A
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   6480
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.ComboBox cmb_Officer 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Petition.frx":2E265
      Left            =   8790
      List            =   "Frm_Petition.frx":2E287
      Style           =   2  'Dropdown List
      TabIndex        =   26
      Top             =   4740
      Width           =   3795
   End
   Begin VB.ComboBox cmb_Title_ID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Petition.frx":2E2E2
      Left            =   60
      List            =   "Frm_Petition.frx":2E304
      Style           =   2  'Dropdown List
      TabIndex        =   25
      Top             =   5940
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.ComboBox cmb_Title 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Petition.frx":2E35F
      Left            =   8790
      List            =   "Frm_Petition.frx":2E381
      Style           =   2  'Dropdown List
      TabIndex        =   24
      Top             =   4140
      Width           =   3795
   End
   Begin VB.TextBox txt_Remark 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4230
      TabIndex        =   22
      Top             =   5340
      Width           =   8355
   End
   Begin VB.CheckBox Chk_Testing 
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   3315
      TabIndex        =   17
      Top             =   4860
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.CheckBox Chk_Test_End 
      BackColor       =   &H00D6D6D6&
      Caption         =   "Check1"
      Enabled         =   0   'False
      Height          =   195
      Left            =   5430
      TabIndex        =   16
      Top             =   4860
      Width           =   195
   End
   Begin VB.ComboBox cmb_OwnerType 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "Frm_Petition.frx":2E3DC
      Left            =   3870
      List            =   "Frm_Petition.frx":2E3FE
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1230
      Width           =   3795
   End
   Begin VB.TextBox txtName 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0EAED&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3900
      TabIndex        =   4
      Top             =   1680
      Width           =   3735
   End
   Begin VB.TextBox txtSurName 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0EAED&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   3900
      TabIndex        =   3
      Top             =   2040
      Width           =   3735
   End
   Begin VB.CommandButton cmd_clear 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Clear."
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5790
      MaskColor       =   &H00404000&
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2400
      Width           =   615
   End
   Begin VB.CommandButton cmdFind 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   345
      Left            =   4710
      Picture         =   "Frm_Petition.frx":2E459
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2400
      Width           =   615
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
      Height          =   7605
      Left            =   0
      TabIndex        =   6
      Top             =   1260
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   13414
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   9884603
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   4695
      TabIndex        =   7
      Top             =   3420
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196639
      OrigLeft        =   7815
      OrigTop         =   930
      OrigRight       =   8070
      OrigBottom      =   1260
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65537
      Enabled         =   0   'False
   End
   Begin MSMask.MaskEdBox MaskEdBox2 
      Height          =   465
      Left            =   6180
      TabIndex        =   14
      Top             =   4080
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   820
      _Version        =   393216
      BackColor       =   16777215
      ForeColor       =   0
      Enabled         =   0   'False
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   14.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin MSComCtl2.UpDown UpDown2 
      Height          =   330
      Left            =   9780
      TabIndex        =   38
      Top             =   1260
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "lb_Year2"
      BuddyDispid     =   196633
      OrigLeft        =   7815
      OrigTop         =   930
      OrigRight       =   8070
      OrigBottom      =   1260
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65537
      Enabled         =   0   'False
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_OWNERSHIP 
      Height          =   3225
      Left            =   2910
      TabIndex        =   45
      Top             =   5910
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   5689
      _Version        =   393216
      BackColor       =   16777215
      Rows            =   3
      Cols            =   16
      FixedCols       =   0
      BackColorFixed  =   14079702
      BackColorSel    =   14073244
      ForeColorSel    =   0
      BackColorBkg    =   15461351
      GridColor       =   15461351
      GridColorFixed  =   15856113
      FocusRect       =   0
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   16
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "��Ǩ�ͺ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   300
      Index           =   3
      Left            =   10365
      TabIndex        =   44
      Top             =   2010
      Width           =   1470
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "���������ҧ��Ǩ�ͺ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   300
      Index           =   2
      Left            =   8250
      TabIndex        =   43
      Top             =   2010
      Width           =   1995
   End
   Begin VB.Label lb_Year2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9180
      TabIndex        =   40
      Top             =   1260
      Width           =   600
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   8340
      TabIndex        =   39
      Top             =   1320
      Width           =   765
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00735924&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡�÷��鹾�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Index           =   1
      Left            =   0
      TabIndex        =   36
      Top             =   900
      Width           =   2895
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   10
      Left            =   3240
      TabIndex        =   29
      Top             =   4260
      Width           =   540
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Index           =   5
      Left            =   3270
      TabIndex        =   23
      Top             =   5400
      Width           =   840
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���˹�ҷ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   7860
      TabIndex        =   21
      Top             =   4830
      Width           =   735
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����ͧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   8220
      TabIndex        =   20
      Top             =   4260
      Width           =   375
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ��� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   5550
      TabIndex        =   15
      Top             =   4260
      Width           =   480
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "���������ҧ��Ǩ�ͺ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   300
      Index           =   28
      Left            =   3255
      TabIndex        =   19
      Top             =   4830
      Width           =   1995
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "��Ǩ�ͺ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   300
      Index           =   53
      Left            =   5370
      TabIndex        =   18
      Top             =   4830
      Width           =   1470
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00C0C0C0&
      Height          =   2715
      Index           =   0
      Left            =   2910
      Top             =   3180
      Width           =   9915
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H005C5C5F&
      BorderColor     =   &H005C5C5F&
      Height          =   2655
      Index           =   2
      Left            =   2940
      Top             =   3210
      Width           =   9870
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   6
      Left            =   3030
      TabIndex        =   13
      Top             =   1290
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   195
      Index           =   1
      Left            =   3450
      TabIndex        =   12
      Top             =   1710
      Width           =   345
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ʡ�� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   195
      Index           =   2
      Left            =   3360
      TabIndex        =   11
      Top             =   2070
      Width           =   435
   End
   Begin VB.Label lb_name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2910
      TabIndex        =   10
      Top             =   2790
      Width           =   9960
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H004A6873&
      Height          =   315
      Index           =   125
      Left            =   3870
      Top             =   1650
      Width           =   3795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H004A6873&
      Height          =   315
      Index           =   1
      Left            =   3870
      Top             =   2010
      Width           =   3795
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   3240
      TabIndex        =   9
      Top             =   3480
      Width           =   765
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4080
      TabIndex        =   8
      Top             =   3420
      Width           =   630
   End
End
Attribute VB_Name = "Frm_Petition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rs As ADODB.Recordset
Dim Rs2 As ADODB.Recordset
Dim Status As String
Dim tempArray(8)

Private Sub Btn_Add_Click()
        Status = "POST"
        Btn_Cancel.Enabled = True
        Btn_Post.Enabled = True
        Btn_Edit.Enabled = False
        Btn_Del.Enabled = False
        Btn_Search.Enabled = False
        cmb_OwnerType.Enabled = True
        txtName.Enabled = True
        txtSurName.Enabled = True
        cmdFind.Enabled = True
        cmd_clear.Enabled = True
        
        Check1.Enabled = False: Check2.Enabled = False
        Label2(2).Enabled = False: Label2(3).Enabled = False
        Lb_Name.Enabled = True
        UpDown1.Enabled = True
        Txt_BOOK_NO.Enabled = True
        MaskEdBox2.Enabled = True
        cmb_Title.Enabled = True
        cmb_Officer.Enabled = True
        Chk_Testing.Enabled = True
        Chk_Test_End.Enabled = False: Label2(53).Enabled = False
        txt_Remark.Enabled = True
        Call Clear_Grid_Ownership
End Sub

Private Sub Clear_Grid_Ownership()
        With GRID_OWNERSHIP
                .Rows = 2
                .Clear
                Call SetGrid_Ownership
        End With
End Sub


Private Sub Btn_Cancel_Click()
        Btn_Cancel.Enabled = True
        Btn_Post.Enabled = False
        Btn_Edit.Enabled = True
        Btn_Del.Enabled = True
        Btn_Search.Enabled = True
        Btn_Add.Enabled = True
        cmb_OwnerType.ListIndex = 0: cmb_OwnerType.Enabled = False
        txtName.Enabled = False
        txtSurName.Enabled = False
        cmdFind.Enabled = False
        cmd_clear.Enabled = False
        Lb_Name.Caption = ""
        UpDown1.Enabled = False
        UpDown2.Enabled = False
        Check1.Enabled = False: Check2.Enabled = False
        Label2(2).Enabled = False: Label2(3).Enabled = False
        Txt_BOOK_NO.Text = "": Txt_BOOK_NO.Enabled = False
        MaskEdBox2.Text = "__/__/____": MaskEdBox2.Enabled = False
        cmb_Title.ListIndex = 0: cmb_Title.Enabled = False
        cmb_Officer.ListIndex = 0: cmb_Officer.Enabled = False
        Chk_Testing.Enabled = False: Chk_Test_End.Enabled = False: Label2(53).Enabled = False
        txt_Remark.Text = "": txt_Remark.Enabled = False
        Grid_Result.Rows = 2
        Grid_Result.Clear
        Grid_Result.Enabled = True
        GRID_OWNERSHIP.Enabled = True
        Call SetGrid
        Call Clear_Grid_Ownership
End Sub

Private Sub Btn_Del_Click()
        On Error GoTo ErrDel
        Globle_Connective.BeginTrans
        If LenB(Trim$(Lb_Name.Caption)) = 0 Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If MsgBox("��ͧ���ź�����Ź�� ��������� ?", vbYesNo, "�׹�ѹ") = vbYes Then
                Globle_Connective.Execute "exec sp_manage_petitiondata '" & Trim$(Txt_BOOK_NO.Text) & "'," & Lb_Year.Caption & ",'" & _
                Lb_Name.Tag & "','','','','','" & cmb_Title_ID.Text & "','" & cmb_Officer_ID.Text & "','" & _
                CvDate2(CDate(MaskEdBox2.Text)) & "','" & Trim$(txt_Remark.Text) & "'," & Chk_Testing.Value & ",'" & _
                strUser & "','" & CvDate2(Now, 1) & "','" & "DEL" & "'", , adCmdUnknown
                Globle_Connective.CommitTrans
        Else
                Globle_Connective.RollbackTrans
        End If
        Call Clear_Grid_Ownership
        Lb_Name.Caption = ""
        Txt_BOOK_NO.Text = ""
        MaskEdBox2.Text = "__/__/____"
        cmb_Officer.ListIndex = 0
        cmb_Title.ListIndex = 0
        txt_Remark.Text = ""
        Chk_Testing.Value = Checked
        Chk_Test_End.Value = Unchecked
        Exit Sub
ErrDel:
        Globle_Connective.RollbackTrans
End Sub

Private Sub Btn_Edit_Click()
        If LenB(Trim$(Lb_Name.Caption)) > 0 Then
        Btn_Cancel.Enabled = True
        Btn_Post.Enabled = True
        Btn_Add.Enabled = False
        Btn_Del.Enabled = False
        Btn_Search.Enabled = False
        cmb_OwnerType.Enabled = False
        txtName.Enabled = False
        txtSurName.Enabled = False
        cmdFind.Enabled = False
        cmd_clear.Enabled = False
        UpDown2.Enabled = False
        Check1.Enabled = False: Check2.Enabled = False
        Label2(2).Enabled = False: Label2(3).Enabled = False
        Lb_Name.Enabled = True
        UpDown1.Enabled = True
        Txt_BOOK_NO.Enabled = False
        MaskEdBox2.Enabled = True
        cmb_Title.Enabled = True
        cmb_Officer.Enabled = True
        Chk_Testing.Enabled = True: Label2(53).Enabled = True
        Chk_Test_End.Enabled = True:
        txt_Remark.Enabled = True
        Grid_Result.Enabled = False
        GRID_OWNERSHIP.Enabled = False
        End If
End Sub

Private Sub Btn_Print_Click()
        fPrint = 1
        Frm_Print_Asset_Extend.Show vbModal
End Sub


Private Sub Btn_Refresh_Click()
        With GBQueryOfficer
                .Requery
                cmb_Officer_ID.Clear
                cmb_Officer.Clear
                If .RecordCount > 0 Then
                        .MoveFirst
                        Do While Not .EOF
                                cmb_Officer_ID.AddItem .Fields("OFFICER_ID").Value
                                cmb_Officer.AddItem .Fields("OFFICER_NAME").Value
                                .MoveNext
                        Loop
                End If
                cmb_Officer.ListIndex = 0
                cmb_Officer_ID.ListIndex = cmb_Officer.ListIndex
        End With
        
        With GBQueryPetitionDetails
                .Requery
                cmb_Title_ID.Clear
                cmb_Title.Clear
                If .RecordCount > 0 Then
                        .MoveFirst
                        Do While Not .EOF
                                cmb_Title_ID.AddItem .Fields("PETITION_TYPE_ID").Value
                                cmb_Title.AddItem .Fields("PETITION_TYPE_DETAILS").Value
                                .MoveNext
                        Loop
                End If
                cmb_Title.ListIndex = 0
                cmb_Title_ID.ListIndex = cmb_Title.ListIndex
        End With
End Sub

Private Sub Btn_Search_Click()
        Status = "EDIT"
        Btn_Cancel.Enabled = True
        Btn_Post.Enabled = False
        Btn_Add.Enabled = False
        Btn_Del.Enabled = True
        Btn_Edit.Enabled = True
'                Btn_Search.Enabled = False
        cmb_OwnerType.Enabled = True
        txtName.Enabled = True
        txtSurName.Enabled = True
        cmdFind.Enabled = True
        cmd_clear.Enabled = True
        UpDown2.Enabled = True
        Check1.Enabled = True: Check2.Enabled = True
        Label2(2).Enabled = True: Label2(3).Enabled = True
        Lb_Name.Enabled = True
        UpDown1.Enabled = False
        Txt_BOOK_NO.Enabled = False
        MaskEdBox2.Enabled = False
        cmb_Title.Enabled = False
        cmb_Officer.Enabled = False
        Chk_Testing.Enabled = False: Label2(53).Enabled = False
        txt_Remark.Enabled = False
End Sub

Private Sub Check1_Click()
        If Check1.Value = Checked Then
                Check2.Value = Unchecked
        Else
                Check2.Value = Checked
        End If
End Sub

Private Sub Check2_Click()
        If Check2.Value = Checked Then
                Check1.Value = Unchecked
        Else
                Check1.Value = Checked
        End If
End Sub

Private Sub Chk_Test_End_Click()
        If Chk_Test_End.Value = Checked Then
                Chk_Testing.Value = Unchecked
        Else
                Chk_Testing.Value = Checked
        End If
End Sub

Private Sub Chk_Testing_Click()
        If Chk_Testing.Value = Checked Then
                Chk_Test_End.Value = Unchecked
        Else
                Chk_Test_End.Value = Checked
        End If
End Sub

Private Sub cmb_Officer_Click()
        cmb_Officer_ID.ListIndex = cmb_Officer.ListIndex
End Sub

Private Sub cmb_Title_Click()
        cmb_Title_ID.ListIndex = cmb_Title.ListIndex
End Sub

Private Sub Btn_Post_Click()
Dim strSQL As String
On Error GoTo ErrSave
If LenB(Trim$(Txt_BOOK_NO.Text)) = 0 Then Exit Sub
Globle_Connective.BeginTrans
If MsgBox("��ͧ��úѹ�֡�����ŷ���¹�������ͧ��������� ?", vbYesNo, "�׹�ѹ") = vbYes Then
                If Status = "POST" Then
                        Set rs = Globle_Connective.Execute("exec sp_chk_key_petitiondata '" & Trim$(Txt_BOOK_NO.Text) & "'," & Lb_Year, , adCmdUnknown)
                        If rs.RecordCount > 0 Then
                                MsgBox "�Ţ�Ѻ�����㹻վ.�." & Lb_Year.Caption, vbExclamation, "�����żԴ��Ҵ"
                                Globle_Connective.RollbackTrans
                                Call Btn_Cancel_Click
                                Exit Sub
                        End If
                End If
'                Debug.Print "exec sp_manage_petitiondata '" & Trim$(Txt_BOOK_NO.Text) & "'," & Lb_Year.Caption & ",'" & _
                 IIf(Status = "POST", tempArray(0), "") & "','" & IIf(Status = "POST", tempArray(5), "") & "','" & IIf(Status = "POST", tempArray(3), "") & "','" & IIf(Status = "POST", tempArray(4), "") & "'," & IIf(Status = "POST", tempArray(2), "''") & ",'" & cmb_Title_ID.Text & "','" & cmb_Officer_ID.Text & "','" & _
                CvDate2(CDate(MaskEdBox2.Text)) & "','" & Trim$(txt_Remark.Text) & "'," & Chk_Testing.Value & ",'" & _
                strUser & "','" & CvDate2(Now, 1) & "','" & Status & "'"
                Globle_Connective.Execute "exec sp_manage_petitiondata '" & Trim$(Txt_BOOK_NO.Text) & "'," & Lb_Year.Caption & ",'" & _
                 IIf(Status = "POST", tempArray(0), "") & "','" & IIf(Status = "POST", tempArray(5), "") & "','" & IIf(Status = "POST", tempArray(3), "") & "','" & IIf(Status = "POST", tempArray(4), "") & "'," & IIf(Status = "POST", tempArray(2), "''") & ",'" & cmb_Title_ID.Text & "','" & cmb_Officer_ID.Text & "','" & _
                CvDate2(CDate(MaskEdBox2.Text)) & "','" & Trim$(txt_Remark.Text) & "'," & Chk_Testing.Value & ",'" & _
                strUser & "','" & CvDate2(Now, 1) & "','" & Status & "'", , adCmdUnknown
                Globle_Connective.CommitTrans
                Call Btn_Cancel_Click
Else
        Globle_Connective.RollbackTrans
        Call Btn_Cancel_Click
End If
Exit Sub
ErrSave:
        Globle_Connective.RollbackTrans
        Call Btn_Cancel_Click
        MsgBox Err.Description
End Sub

Private Sub cmdFind_Click()
On Error GoTo ErrFind
        Dim strSQL As String
        Me.MousePointer = 11
                If Status = "POST" Then
                        Set rs = Globle_Connective.Execute("exec sp_find_petition_add '" & cmb_OwnerType.ListIndex & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "'", , adCmdUnknown)
                Else
'                        Debug.Print "exec sp_find_petition_edit '" & cmb_OwnerType.ListIndex & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & lb_Year2.Caption & "','" & Check1.Value & "'"
                        Set rs = Globle_Connective.Execute("exec sp_find_petition_search '" & cmb_OwnerType.ListIndex & "','" & Trim$(txtName.Text) & "','" & Trim$(txtSurName.Text) & "','" & lb_Year2.Caption & "','" & Check1.Value & "'", , adCmdUnknown)
                End If
                If rs.RecordCount > 0 Then
                        Set Grid_Result.DataSource = rs
                Else
                        Grid_Result.Rows = 2
                        Grid_Result.Clear
                End If
                Call SetGrid
        Me.MousePointer = 0
        Exit Sub
ErrFind:
        MsgBox Err.Description
        Me.MousePointer = 0
End Sub

Private Sub Cmd_Clear_Click()
        txtName.Text = "": txtName.BackColor = &HE0EAED
        txtSurName.Text = "": txtSurName.BackColor = &HE0EAED
        MaskEdBox2.Text = "__/__/____"
        cmb_OwnerType.ListIndex = 0
        Grid_Result.Rows = 2
        Grid_Result.Clear
        Call SetGrid
End Sub

Private Sub SetGrid()
        With Grid_Result
                .FormatString = "|����|"
                .ColWidth(0) = 0
                .ColWidth(1) = 2850: .ColAlignmentFixed(1) = 4
                .ColWidth(2) = 0
                .ColWidth(3) = 0
                .ColWidth(4) = 0
                .ColWidth(5) = 0
                .ColWidth(6) = 0
                .ColWidth(7) = 0
        End With
End Sub

Private Sub SetGrid_Ownership()
        With GRID_OWNERSHIP
                .FormatString = "^�������|^��|^�ѹ���|^����|^����ͧ|^��û�Ժѵ�|^���˹�Ҿ�ѡ�ҹ|^�����˵�"
                .ColWidth(0) = 800
                .ColWidth(1) = 800
                .ColWidth(2) = 1200
                .ColWidth(3) = 3000
                .ColWidth(4) = 3000
                .ColWidth(5) = 1500
                .ColWidth(6) = 3000
                .ColWidth(7) = 5000
                .ColWidth(8) = 0
                .ColWidth(9) = 0
        End With
End Sub

Private Sub SET_REFRESH()
        GBQueryPetitionDetails.Requery
        cmb_Title_ID.Clear
        cmb_Title.Clear
        If GBQueryPetitionDetails.RecordCount > 0 Then
                GBQueryPetitionDetails.MoveFirst
                Do While Not GBQueryPetitionDetails.EOF
                      cmb_Title_ID.AddItem GBQueryPetitionDetails.Fields("PETITION_TYPE_ID").Value
                      cmb_Title.AddItem GBQueryPetitionDetails.Fields("PETITION_TYPE_DETAILS").Value
                      GBQueryPetitionDetails.MoveNext
                Loop
        End If
        GBQueryOfficer.Requery
        cmb_Officer_ID.Clear
        cmb_Officer.Clear
        If GBQueryOfficer.RecordCount > 0 Then
                GBQueryOfficer.MoveFirst
                Do While Not GBQueryOfficer.EOF
                        cmb_Officer_ID.AddItem GBQueryOfficer.Fields("OFFICER_ID").Value
                        cmb_Officer.AddItem GBQueryOfficer.Fields("OFFICER_NAME").Value
                        GBQueryOfficer.MoveNext
                Loop
        End If
End Sub

Private Sub Form_Load()
        Set rs = New ADODB.Recordset
        Set Rs2 = New ADODB.Recordset
        Call SET_REFRESH
        Call SetGrid
        Lb_Year.Caption = Right$(Date, 4)
        lb_Year2.Caption = Lb_Year.Caption
        cmb_OwnerType.ListIndex = 0
        cmb_Officer.ListIndex = 0
        cmb_Title.ListIndex = 0
        cmb_Title_ID.ListIndex = cmb_Title.ListIndex
        cmb_Officer_ID.ListIndex = cmb_Officer.ListIndex
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        WheelUnHook
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set rs = Nothing
        Set Rs2 = Nothing
End Sub

Private Sub GRID_OWNERSHIP_Click()
        With GRID_OWNERSHIP
                 If LenB(Trim$(.TextMatrix(1, 0))) = 0 Then Exit Sub
                Lb_Name.Tag = .TextMatrix(.Row, 8)
                Txt_BOOK_NO.Text = .TextMatrix(.Row, 0)
                Lb_Year.Caption = .TextMatrix(.Row, 1)
                MaskEdBox2.Text = .TextMatrix(.Row, 2)
                Lb_Name.Caption = .TextMatrix(.Row, 3)
                cmb_Title.Text = .TextMatrix(.Row, 4)
                If .TextMatrix(.Row, 5) = "���������ҧ��Ǩ�ͺ" Then
                        Chk_Testing.Value = Checked
                        Chk_Test_End.Value = Unchecked
                Else
                        Chk_Testing.Value = Unchecked
                        Chk_Test_End.Value = Checked
                End If
                cmb_Officer.Text = .TextMatrix(.Row, 6)
                txt_Remark.Text = .TextMatrix(.Row, 7)
                cmb_Title_ID.Text = .TextMatrix(.Row, 9)
        End With
End Sub

Private Sub Grid_Result_Click()
        Dim i As Integer '
        
        If LenB(Trim$(Grid_Result.TextMatrix(1, 0))) = 0 Then Exit Sub
        If Status = "POST" Then
                Lb_Name.Caption = Grid_Result.TextMatrix(Grid_Result.Row, 1)
                For i = 0 To 5
                        tempArray(i) = Grid_Result.TextMatrix(Grid_Result.Row, i)
                Next i
                Set rs = Globle_Connective.Execute("exec sp_check_pettition_no " & Lb_Year.Caption, , adCmdUnknown)
                If rs.RecordCount > 0 Then
                        rs.MoveLast
                        Txt_BOOK_NO.Text = Format$(rs.Fields(0).Value + 1, "0000")
                Else
                        Txt_BOOK_NO.Text = "0001"
                End If
                If MaskEdBox2.Enabled = True Then MaskEdBox2.SetFocus
        Else
                Set Rs2 = Globle_Connective.Execute("exec sp_find_petition_edit '" & Grid_Result.TextMatrix(Grid_Result.Row, 0) & "'," & IIf(Check1.Value = Checked, 1, 0), , adCmdUnknown)
                If Rs2.RecordCount > 0 Then
                        Set GRID_OWNERSHIP.DataSource = Rs2
                        For i = 1 To GRID_OWNERSHIP.Rows - 1
                                GRID_OWNERSHIP.TextMatrix(i, 2) = ConvertDate3(GRID_OWNERSHIP.TextMatrix(i, 2))
                                If UCase$(GRID_OWNERSHIP.TextMatrix(i, 5)) = "TRUE" Then
                                        GRID_OWNERSHIP.TextMatrix(i, 5) = "���������ҧ��Ǩ�ͺ"
                                Else
                                        GRID_OWNERSHIP.TextMatrix(i, 5) = "��Ǩ�ͺ����"
                                End If
                        Next i
                Else
                        GRID_OWNERSHIP.Cols = 2
                        GRID_OWNERSHIP.Clear
                End If
                Call SetGrid_Ownership
        End If
End Sub

Private Sub Grid_Result_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        WheelUnHook
        WheelHook Frm_Petition, Frm_Petition.Grid_Result
End Sub

Private Sub Label2_Click(Index As Integer)
        Select Case Index
                Case 28
                        If Chk_Testing.Value = Checked Then
                                Chk_Testing.Value = Unchecked
                                Chk_Test_End.Value = Checked
                        Else
                                Chk_Testing.Value = Checked
                                Chk_Test_End.Value = Unchecked
                        End If
                Case 53
                        If Chk_Test_End.Value = Checked Then
                                Chk_Test_End.Value = Unchecked
                                Chk_Testing.Value = Checked
                        Else
                                Chk_Test_End.Value = Checked
                                Chk_Testing.Value = Unchecked
                        End If
                Case 2
                        If Check1.Value = Checked Then
                                Check1.Value = Unchecked
                                Check2.Value = Checked
                        Else
                                Check1.Value = Checked
                                Check2.Value = Unchecked
                        End If
                Case 3
                        If Check2.Value = Checked Then
                                Check2.Value = Unchecked
                                Check1.Value = Checked
                        Else
                                Check2.Value = Checked
                                Check1.Value = Unchecked
                        End If
        End Select
        
End Sub

Private Sub Label5_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
        If Index = 1 Then WheelUnHook
End Sub

Private Sub MaskEdBox2_LostFocus()
        If InStr(1, MaskEdBox2.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(MaskEdBox2.Text) And Mid$(MaskEdBox2.Text, 4, 2) < 13 Then
             
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox2.SetFocus
        End If
End Sub

Private Sub txtName_GotFocus()
        txtName.BackColor = &HFFFFFF
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdFind_Click
End Sub

Private Sub txtSurName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdFind_Click
End Sub
