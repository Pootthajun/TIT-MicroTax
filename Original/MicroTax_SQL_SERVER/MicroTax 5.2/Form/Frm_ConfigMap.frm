VERSION 5.00
Object = "{EBAB1620-5BB5-11CF-80D0-004F4B002122}#5.0#0"; "Sis.ocx"
Begin VB.Form Frm_ConfigMap 
   Appearance      =   0  'Flat
   BackColor       =   &H00404040&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Microtax Map Setting"
   ClientHeight    =   4950
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7200
   Icon            =   "Frm_ConfigMap.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   7200
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Save Setting"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5700
      MaskColor       =   &H00404000&
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   1740
      Width           =   1395
   End
   Begin SisLib.Sis Sis1 
      Height          =   600
      Left            =   6390
      TabIndex        =   17
      Top             =   3030
      Visible         =   0   'False
      Width           =   720
      _Version        =   327680
      _ExtentX        =   1270
      _ExtentY        =   1058
      _StockProps     =   64
      BorderBevel     =   -1  'True
      Display         =   0
   End
   Begin VB.ListBox Lst_Overlay 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      ItemData        =   "Frm_ConfigMap.frx":151A
      Left            =   30
      List            =   "Frm_ConfigMap.frx":151C
      TabIndex        =   18
      Top             =   2460
      Width           =   7125
   End
   Begin VB.ComboBox Cmb_Block 
      Height          =   315
      ItemData        =   "Frm_ConfigMap.frx":151E
      Left            =   1230
      List            =   "Frm_ConfigMap.frx":1520
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   1740
      Width           =   705
   End
   Begin VB.ComboBox Cmb_Land 
      Height          =   315
      ItemData        =   "Frm_ConfigMap.frx":1522
      Left            =   1230
      List            =   "Frm_ConfigMap.frx":1524
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   750
      Width           =   705
   End
   Begin VB.ComboBox Cmb_Building 
      Height          =   315
      Left            =   1230
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   1080
      Width           =   705
   End
   Begin VB.ComboBox Cmb_Singbord 
      Height          =   315
      ItemData        =   "Frm_ConfigMap.frx":1526
      Left            =   1230
      List            =   "Frm_ConfigMap.frx":1528
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   1410
      Width           =   705
   End
   Begin VB.ComboBox CMB_GRAPHLAND1 
      Height          =   315
      Left            =   1950
      TabIndex        =   7
      Top             =   750
      Width           =   2535
   End
   Begin VB.ComboBox CMB_GRAPHLAND2 
      Height          =   315
      Left            =   4500
      TabIndex        =   6
      Top             =   750
      Width           =   2595
   End
   Begin VB.ComboBox CMB_GRAPHBUILDING1 
      Height          =   315
      Left            =   1950
      TabIndex        =   5
      Top             =   1080
      Width           =   2535
   End
   Begin VB.ComboBox CMB_GRAPHSIGN1 
      Height          =   315
      Left            =   1950
      TabIndex        =   4
      Top             =   1410
      Width           =   2535
   End
   Begin VB.ComboBox CMB_GRAPHBUILDING2 
      Height          =   315
      Left            =   4500
      TabIndex        =   3
      Top             =   1080
      Width           =   2595
   End
   Begin VB.ComboBox CMB_GRAPHSIGN2 
      Height          =   315
      Left            =   4500
      TabIndex        =   2
      Top             =   1410
      Width           =   2595
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��¡�ê�鹢�����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   30
      TabIndex        =   16
      Top             =   2130
      Width           =   7155
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��� ⫹���͡ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   11
      Left            =   180
      TabIndex        =   15
      Top             =   1800
      Width           =   1005
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��� �ŧ���Թ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   12
      Left            =   120
      TabIndex        =   14
      Top             =   810
      Width           =   1050
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��� �ç���͹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   13
      Left            =   240
      TabIndex        =   13
      Top             =   1140
      Width           =   930
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��� ���� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   14
      Left            =   570
      TabIndex        =   12
      Top             =   1470
      Width           =   615
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00E0E0E0&
      Height          =   1575
      Index           =   2
      Left            =   30
      Top             =   540
      Width           =   7125
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ػ��������Ẻ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   15
      Left            =   2550
      TabIndex        =   1
      Top             =   270
      Width           =   1485
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ػ����������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   19
      Left            =   5190
      TabIndex        =   0
      Top             =   270
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      Height          =   405
      Index           =   0
      Left            =   1980
      Shape           =   4  'Rounded Rectangle
      Top             =   180
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      Height          =   405
      Index           =   1
      Left            =   4530
      Shape           =   4  'Rounded Rectangle
      Top             =   180
      Width           =   2625
   End
End
Attribute VB_Name = "Frm_ConfigMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub GETTHEME(CaseStr As Byte)
Dim Count_Overlay  As Byte, i As Byte

     Select Case CaseStr
                  Case 1
                                 CMB_GRAPHLAND1.Clear
                                 CMB_GRAPHLAND2.Clear
                                    Count_Overlay = Sis1.GetInt(SIS_OT_OVERLAY, Abs(CByte(Val(Cmb_Land.Text)) - 1), "_nTheme&")
                                    If Count_Overlay = 0 Then Exit Sub
                                         For i = 0 To Count_Overlay - 1
                                                  Sis1.GetOverlayTheme Abs(CByte(Val(Cmb_Land.Text)) - 1), "ListTypeIndividualTheme", i
                                                  Sis1.LoadTheme "ListTypeIndividualTheme"
                                                  CMB_GRAPHLAND1.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                                  CMB_GRAPHLAND2.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                        Next i
                  Case 2
                              CMB_GRAPHBUILDING1.Clear
                              CMB_GRAPHBUILDING2.Clear
                                  Count_Overlay = Sis1.GetInt(SIS_OT_OVERLAY, Abs(CByte(Val(Cmb_Building.Text)) - 1), "_nTheme&")
                                    If Count_Overlay = 0 Then Exit Sub
                                         For i = 0 To Count_Overlay - 1
                                                  Sis1.GetOverlayTheme Abs(CByte(Val(Cmb_Building.Text)) - 1), "ListTypeIndividualTheme", i
                                                  Sis1.LoadTheme "ListTypeIndividualTheme"
                                                  CMB_GRAPHBUILDING1.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                                  CMB_GRAPHBUILDING2.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                        Next i
                  Case 3
                            CMB_GRAPHSIGN1.Clear
                            CMB_GRAPHSIGN2.Clear
                                    Count_Overlay = Sis1.GetInt(SIS_OT_OVERLAY, Abs(CByte(Val(Cmb_Singbord.Text)) - 1), "_nTheme&")
                                    If Count_Overlay = 0 Then Exit Sub
                                         For i = 0 To Count_Overlay - 1
                                                  Sis1.GetOverlayTheme Abs(CByte(Val(Cmb_Singbord.Text)) - 1), "ListTypeIndividualTheme", i
                                                  Sis1.LoadTheme "ListTypeIndividualTheme"
                                                  CMB_GRAPHSIGN1.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                                  CMB_GRAPHSIGN2.AddItem Sis1.GetStr(SIS_OT_THEME, 0, "_title$")
                                        Next i
     End Select
End Sub

Private Sub Cmb_Building_Click()
Call GETTHEME(2)
End Sub

Private Sub Cmb_Land_Click()
Call GETTHEME(1)
End Sub

Private Sub Cmb_Singbord_Click()
Call GETTHEME(3)
End Sub

Private Sub Command1_Click()
If MsgBox("Are want you save setting configuration Map ?", vbInformation + vbYesNo, "Save Setting!") = vbNo Then Exit Sub
        WriteINI "index overlay", "zone", Abs(CByte(Val(Cmb_Block.Text)) - 1)
        WriteINI "index overlay", "land", Abs(CByte(Val(Cmb_Land.Text)) - 1)
        WriteINI "index overlay", "building", Abs(CByte(Val(Cmb_Building.Text)) - 1)
        WriteINI "index overlay", "signboard", Abs(CByte(Val(Cmb_Singbord.Text)) - 1)
     
        WriteINI "index theme", "landtaxaccept", Abs(Val(CMB_GRAPHLAND1.ListIndex))
        WriteINI "index theme", "landtaxpay", Abs(Val(CMB_GRAPHLAND2.ListIndex))
        WriteINI "index theme", "signbordtaxaccept", Abs(Val(CMB_GRAPHSIGN1.ListIndex))
        WriteINI "index theme", "signbordtaxpay", Abs(Val(CMB_GRAPHSIGN2.ListIndex))
        WriteINI "index theme", "buildingtaxaccept", Abs(Val(CMB_GRAPHBUILDING1.ListIndex))
        WriteINI "index theme", "buildingtaxpay", Abs(Val(CMB_GRAPHBUILDING2.ListIndex))

        ini_landtaxaccept = Abs(Val(CMB_GRAPHLAND1.ListIndex))
        ini_landtaxpay = Abs(Val(CMB_GRAPHLAND2.ListIndex))
        ini_signbordtaxaccept = Abs(Val(CMB_GRAPHSIGN1.ListIndex))
        ini_signbordtaxpay = Abs(Val(CMB_GRAPHSIGN2.ListIndex))
        ini_buildingtaxaccept = Abs(Val(CMB_GRAPHBUILDING1.ListIndex))
        ini_buildingtaxpay = Abs(Val(CMB_GRAPHBUILDING2.ListIndex))
          
        iniZone = Abs(Val(Cmb_Block.Text) - 1)
        iniLand = Abs(Val(Cmb_Land.Text) - 1)
        iniBuliding = Abs(Val(Cmb_Building.Text) - 1)
        iniSignbord = Abs(Val(Cmb_Singbord.Text) - 1)
End Sub

Private Sub Form_Load()
    Dim i As Byte, totalOverlay As Integer, PathStr As String
            PathStr = ReadINI("path file", "pathmap")
    If LenB(Trim$(PathStr)) > 0 Then
                  Lst_Overlay.Clear
                 Cmb_Block.Clear
                 Cmb_Land.Clear
                 Cmb_Building.Clear
                 Cmb_Singbord.Clear

            Select Case TProduct
                         Case "DEMO"
                                       Sis1.Level = SIS_LEVEL_MODELLER
                         Case "VIEWER"
                                      Sis1.Level = SIS_LEVEL_VIEWER
                         Case "MANAGER"
                                      Sis1.Level = SIS_LEVEL_MANAGER
                         Case "MODELLER"
                                       Sis1.Level = SIS_LEVEL_MODELLER
            End Select
            Sis1.LoadSwd PathStr
            With Sis1
                 totalOverlay = .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
                 If totalOverlay > 0 Then
                     For i = 0 To totalOverlay - 1
                                 Lst_Overlay.AddItem i + 1 & " " & .GetStr(SIS_OT_OVERLAY, i, "_name$")
                                 Cmb_Block.AddItem i + 1
                                 Cmb_Land.AddItem i + 1
                                 Cmb_Building.AddItem i + 1
                                 Cmb_Singbord.AddItem i + 1
                     Next i
                  End If
                  
             End With
    End If
    On Error Resume Next
                        Cmb_Block.ListIndex = iniZone
                        Cmb_Land.ListIndex = iniLand
                        Cmb_Building.ListIndex = iniBuliding
                        Cmb_Singbord.ListIndex = iniSignbord

                        CMB_GRAPHLAND1.ListIndex = ini_landtaxaccept
                        CMB_GRAPHLAND2.ListIndex = ini_landtaxpay
                        CMB_GRAPHBUILDING1.ListIndex = ini_buildingtaxaccept
                        CMB_GRAPHBUILDING2.ListIndex = ini_buildingtaxpay
                        CMB_GRAPHSIGN1.ListIndex = ini_signbordtaxaccept
                        CMB_GRAPHSIGN2.ListIndex = ini_signbordtaxpay
End Sub
