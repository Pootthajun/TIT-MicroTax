VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_AnalyseMap 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "������������Ἱ���"
   ClientHeight    =   6465
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3825
   Icon            =   "Frm_AnalyseMap.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   3825
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   0
      TabIndex        =   6
      Top             =   780
      Width           =   3795
      Begin VB.OptionButton opMIS 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�������ʷ�������㹰ҹ������"
         Height          =   195
         Left            =   270
         TabIndex        =   8
         Top             =   420
         Width           =   4335
      End
      Begin VB.OptionButton opMap 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�������ʷ��������Ἱ��� "
         Height          =   195
         Left            =   270
         TabIndex        =   7
         Top             =   90
         Value           =   -1  'True
         Width           =   4695
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_ID 
      Height          =   3495
      Left            =   0
      TabIndex        =   5
      Top             =   1830
      Width           =   1890
      _ExtentX        =   3334
      _ExtentY        =   6165
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "��������..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2400
      TabIndex        =   2
      Top             =   390
      Width           =   1410
   End
   Begin VB.ComboBox cboOverlay 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_AnalyseMap.frx":151A
      Left            =   0
      List            =   "Frm_AnalyseMap.frx":1527
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   390
      Width           =   2355
   End
   Begin MSComctlLib.ProgressBar pgb 
      Height          =   345
      Left            =   930
      TabIndex        =   3
      Top             =   6120
      Width           =   2910
      _ExtentX        =   5133
      _ExtentY        =   609
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Err 
      Height          =   3495
      Left            =   1920
      TabIndex        =   10
      Top             =   1830
      Width           =   1890
      _ExtentX        =   3334
      _ExtentY        =   6165
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.Label lb_Count 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999999&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   2
      Left            =   0
      TabIndex        =   13
      Top             =   1530
      Width           =   1899
   End
   Begin VB.Label lb_Count 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999999&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "����(���١��ͧ)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Index           =   1
      Left            =   1930
      TabIndex        =   12
      Top             =   1530
      Width           =   1880
   End
   Begin VB.Label lb_Count2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999999&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1920
      TabIndex        =   11
      Top             =   5340
      Width           =   1880
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   1
      Left            =   2610
      Picture         =   "Frm_AnalyseMap.frx":1543
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   5640
      Width           =   480
   End
   Begin VB.Image Image_Print 
      Appearance      =   0  'Flat
      Height          =   420
      Index           =   0
      Left            =   720
      Picture         =   "Frm_AnalyseMap.frx":38C7
      Stretch         =   -1  'True
      ToolTipText     =   "�������§ҹ"
      Top             =   5640
      Width           =   480
   End
   Begin VB.Label lb_Count 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999999&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   0
      Left            =   0
      TabIndex        =   9
      Top             =   5340
      Width           =   1899
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      BackColor       =   &H00404040&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Index           =   3
      Left            =   0
      TabIndex        =   4
      Top             =   6120
      Width           =   915
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999999&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "���͡������������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   3810
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   1
      Left            =   0
      Top             =   5610
      Width           =   1905
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   0
      Left            =   1920
      Top             =   5610
      Width           =   1875
   End
End
Attribute VB_Name = "Frm_AnalyseMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim StayTop As Long
Dim arrCode() As String
Dim indexOverlay As Integer
Dim strSchema As String
Dim rs As ADODB.Recordset
Dim RsErr As ADODB.Recordset

Private Sub cboOverlay_Click()
        GRID_ID.Rows = 2
        GRID_ID.Clear
        Grid_Err.Rows = 2
        Grid_Err.Clear
        Call Set_Grid
End Sub

Private Sub cmdOK_Click()
        Dim i As Long
        Dim strID As String
        On Error GoTo ErrCom9
        
        Globle_Connective.BeginTrans
        Me.MousePointer = 11
        If cboOverlay.ListIndex = 0 Then
            indexOverlay = iniLand
            strSchema = "LAND_ID$"
'            Call SET_Execute("DELETE FROM LAND_MAP")
            Globle_Connective.Execute "DELETE FROM LAND_MAP", , adCmdUnknown
            Globle_Connective.Execute "DELETE FROM LAND_MAP_ERR", , adCmdUnknown
'            Call SET_Execute("DELETE FROM LAND_MAP_ERR")
        ElseIf cboOverlay.ListIndex = 1 Then
            indexOverlay = iniBuliding
            strSchema = "BUILDING_ID$"
            Globle_Connective.Execute "DELETE FROM BUILDING_MAP", , adCmdUnknown
            Globle_Connective.Execute "DELETE FROM BUILDING_MAP_ERR", , adCmdUnknown
'            Call SET_Execute("DELETE FROM BUILDING_MAP")
'            Call SET_Execute("DELETE FROM BUILDING_MAP_ERR")
        ElseIf cboOverlay.ListIndex = 2 Then
            indexOverlay = iniSignbord
            strSchema = "SIGNBORD_ID$"
            Globle_Connective.Execute "DELETE FROM SIGNBORD_MAP", , adCmdUnknown
            Globle_Connective.Execute "DELETE FROM SIGNBORD_MAP_ERR", , adCmdUnknown
'            Call SET_Execute("DELETE FROM SIGNBORD_MAP")
'            Call SET_Execute("DELETE FROM SIGNBORD_MAP_ERR")
        End If
        With Frm_Map.Sis1
                .DeselectAll
                .EmptyList "selectitem"
                .ScanOverlay "selectitem", indexOverlay, "", ""
                If .GetListSize("selectitem") > 0 Then
                        pgb.max = .GetListSize("selectitem")
                        pgb.Min = 0
                        pgb.Value = 0
                        Select Case cboOverlay.ListIndex
                                Case 0 'LAND
                                        For i = 0 To .GetListSize("selectitem") - 1
                                            .OpenList "selectitem", i
                                            strID = .GetStr(SIS_OT_CURITEM, 0, strSchema)
                                            If CheckID(strID) = True Then
'                                                    Call SET_Execute("INSERT INTO LAND_MAP (LAND_ID) VALUES ('" & strID & "')")
                                                    Globle_Connective.Execute "INSERT INTO LAND_MAP (LAND_ID) VALUES ('" & strID & "')", , adCmdUnknown
                                            Else
                                                    If LenB(Trim$(strID)) > 0 Then Globle_Connective.Execute "INSERT INTO LAND_MAP_ERR (LAND_ID) VALUES ('" & strID & "')", , adCmdUnknown     'SET_Execute("INSERT INTO LAND_MAP_ERR (LAND_ID) VALUES ('" & strID & "')")
                                            End If
                                            pgb.Value = i + 1
                                        Next i
                                Case 1 'BUILDING
                                        For i = 0 To .GetListSize("selectitem") - 1
                                            .OpenList "selectitem", i
                                            strID = .GetStr(SIS_OT_CURITEM, 0, strSchema)
                                            If CheckID(strID) = True Then
'                                                    Call SET_Execute("INSERT INTO BUILDING_MAP (BUILDING_ID) VALUES ('" & strID & "')")
                                                     Globle_Connective.Execute "INSERT INTO BUILDING_MAP (BUILDING_ID) VALUES ('" & strID & "')", , adCmdUnknown
                                            Else
                                                    If LenB(Trim$(strID)) > 0 Then Globle_Connective.Execute "INSERT INTO BUILDING_MAP_ERR (BUILDING_ID) VALUES ('" & strID & "')", , adCmdUnknown  ' Call SET_Execute("INSERT INTO BUILDING_MAP_ERR (BUILDING_ID) VALUES ('" & strID & "')")
                                            End If
                                            pgb.Value = i + 1
                                        Next i
                                Case 2 'SIGNBORD
                                        For i = 0 To .GetListSize("selectitem") - 1
                                            .OpenList "selectitem", i
                                            strID = .GetStr(SIS_OT_CURITEM, 0, strSchema)
                                            If CheckID(strID) = True Then
'                                                    Call SET_Execute("INSERT INTO SIGNBORD_MAP (SIGNBORD_ID) VALUES ('" & strID & "')")
                                                    Globle_Connective.Execute "INSERT INTO SIGNBORD_MAP (SIGNBORD_ID) VALUES ('" & strID & "')", , adCmdUnknown
                                            Else
                                                    If LenB(Trim$(strID)) > 0 Then Globle_Connective.Execute "INSERT INTO SIGNBORD_MAP_ERR (SIGNBORD_ID) VALUES ('" & strID & "')", , adCmdUnknown 'Call SET_Execute("INSERT INTO SIGNBORD_MAP_ERR (SIGNBORD_ID) VALUES ('" & strID & "')")
                                            End If
                                            pgb.Value = i + 1
                                        Next i
                        End Select
                End If
                .EmptyList "selectitem"
                Globle_Connective.CommitTrans
                On Error GoTo ErrFind
                Select Case cboOverlay.ListIndex
                        Case 0
                                If opMap.Value = True Then
'                                        Call SET_QUERY("SELECT LAND_ID FROM LANDDATA A WHERE NOT EXISTS(SELECT * FROM  LAND_MAP B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID", Rs)
'                                        Call SET_QUERY("SELECT LAND_ID FROM LAND_MAP_ERR GROUP BY LAND_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '0','0'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '0','0'", , adCmdUnknown)
                                Else
'                                        Call SET_QUERY("SELECT LAND_ID FROM LAND_MAP A WHERE NOT EXISTS(SELECT * FROM  LANDDATA B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID", Rs)
'                                        Call SET_QUERY("SELECT LAND_ID FROM LAND_MAP_ERR GROUP BY LAND_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '0','1'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '0','1'", , adCmdUnknown)
                                End If
                        Case 1
                                If opMap.Value = True Then
'                                        Call SET_QUERY("SELECT BUILDING_ID FROM BUILDINGDATA A WHERE NOT EXISTS(SELECT * FROM  BUILDING_MAP B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID", Rs)
'                                        Call SET_QUERY("SELECT BUILDING_ID FROM BUILDING_MAP_ERR GROUP BY BUILDING_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '1','0'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '1','0'", , adCmdUnknown)
                                Else
'                                        Call SET_QUERY("SELECT BUILDING_ID FROM BUILDING_MAP A WHERE NOT EXISTS(SELECT * FROM  BUILDINGDATA B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID", Rs)
'                                        Call SET_QUERY("SELECT BUILDING_ID FROM BUILDING_MAP_ERR GROUP BY BUILDING_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '1','1'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '1','1'", , adCmdUnknown)
                                End If
                        Case 2
                                If opMap.Value = True Then
'                                        Call SET_QUERY("SELECT SIGNBORD_ID FROM SIGNBORDDATA A WHERE NOT EXISTS(SELECT * FROM  SIGNBORD_MAP B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID", Rs)
'                                        Call SET_QUERY("SELECT SIGNBORD_ID FROM SIGNBORD_MAP_ERR GROUP BY SIGNBORD_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '2','0'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '2','0'", , adCmdUnknown)
                                Else
'                                        Call SET_QUERY("SELECT SIGNBORD_ID FROM SIGNBORD_MAP A WHERE NOT EXISTS(SELECT * FROM SIGNBORDDATA  B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID", Rs)
'                                        Call SET_QUERY("SELECT SIGNBORD_ID FROM SIGNBORD_MAP_ERR GROUP BY SIGNBORD_ID", RsErr)
                                        Set rs = Globle_Connective.Execute("exec sp_analysemap '2','1'", , adCmdUnknown)
                                        Set RsErr = Globle_Connective.Execute("exec sp_analysemap_err '2','1'", , adCmdUnknown)
                                End If
                End Select
                Set GRID_ID.DataSource = rs
                Set Grid_Err.DataSource = RsErr
                If rs.RecordCount > 0 Then
                        lb_Count(0).Caption = rs.RecordCount
                Else
                        lb_Count(0).Caption = "0"
                End If
                Call Set_Grid
                If RsErr.RecordCount > 0 Then
                        lb_Count2.Caption = RsErr.RecordCount
                Else
                        lb_Count2.Caption = "0"
                End If
                Me.MousePointer = 0
        End With
        Exit Sub
ErrCom9:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        MsgBox Err.Description & "," & i
        Exit Sub
ErrFind:
End Sub

Private Sub Form_Load()
        Me.Show
        Set rs = New ADODB.Recordset
        Set RsErr = New ADODB.Recordset
        StayTop = SetWindowPos(Me.hWnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
        cboOverlay.ListIndex = 0
        Call Set_Grid
End Sub

Private Sub Set_Grid()
        With GRID_ID
                .ColAlignmentFixed(0) = 4
                .ColWidth(0) = 1650
                Grid_Err.ColAlignmentFixed(0) = 4
                Grid_Err.ColWidth(0) = 1650
                Select Case cboOverlay.ListIndex
                        Case 0
                            .TextMatrix(0, 0) = "���ʷ��Թ"
                            Grid_Err.TextMatrix(0, 0) = "���ʷ��Թ(Ἱ���)"
                        Case 1
                            .TextMatrix(0, 0) = "�����ç���͹"
                            Grid_Err.TextMatrix(0, 0) = "�����ç���͹(Ἱ���)"
                        Case 2
                            .TextMatrix(0, 0) = "���ʻ���"
                            Grid_Err.TextMatrix(0, 0) = "���ʻ���(Ἱ���)"
                End Select
        End With
End Sub

Private Sub CreateView()
        On Error Resume Next
        Select Case cboOverlay.ListIndex
                Case 0
'                        Call SET_Execute("DROP VIEW VIEW_LAND_MAP")
'                        Globle_Connective.Execute "DROP VIEW VIEW_LAND_MAP", , adCmdUnknown
                        If opMap.Value = True Then
'                                Call SET_Execute("CREATE VIEW VIEW_LAND_MAP AS SELECT LAND_ID FROM LANDDATA A WHERE NOT EXISTS(SELECT * FROM  LAND_MAP B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_LAND_MAP WITH ENCRYPTION AS SELECT LAND_ID FROM LANDDATA A WHERE NOT EXISTS(SELECT * FROM  LAND_MAP B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID WITH CHECK OPTION", , adCmdUnknown
                        Else
'                                Call SET_Execute("CREATE VIEW VIEW_LAND_MAP AS SELECT LAND_ID FROM LAND_MAP A WHERE NOT EXISTS(SELECT * FROM  LANDDATA B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_LAND_MAP WITH ENCRYPTION AS SELECT LAND_ID FROM LANDDATA A WHERE NOT EXISTS(SELECT * FROM  LANDDATA B WHERE A.LAND_ID=B.LAND_ID) GROUP BY LAND_ID WITH CHECK OPTION", , adCmdUnknown
                        End If
                Case 1
'                        Call SET_Execute("DROP VIEW VIEW_BUILDING_MAP")
                        If opMap.Value = True Then
'                                Call SET_Execute("CREATE VIEW VIEW_BUILDING_MAP AS SELECT BUILDING_ID FROM BUILDINGDATA A WHERE NOT EXISTS(SELECT * FROM  BUILDING_MAP B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_BUILDING_MAP WITH ENCRYPTION AS SELECT BUILDING_ID FROM BUILDINGDATA A WHERE NOT EXISTS(SELECT * FROM  BUILDING_MAP B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID WITH CHECK OPTION", , adCmdUnknown
                        Else
'                                Call SET_Execute("CREATE VIEW VIEW_BUILDING_MAP AS SELECT BUILDING_ID FROM BUILDING_MAP A WHERE NOT EXISTS(SELECT * FROM  BUILDINGDATA B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_BUILDING_MAP WITH ENCRYPTION AS SELECT BUILDING_ID FROM BUILDINGDATA A WHERE NOT EXISTS(SELECT * FROM  BUILDINGDATA B WHERE A.BUILDING_ID=B.BUILDING_ID) GROUP BY BUILDING_ID WITH CHECK OPTION", , adCmdUnknown
                        End If
                Case 2
'                        Call SET_Execute("DROP VIEW VIEW_SIGNBORD_MAP")
                        If opMap.Value = True Then
'                                Call SET_Execute("CREATE VIEW VIEW_SIGNBORD_MAP AS SELECT SIGNBORD_ID FROM SIGNBORDDATA A WHERE NOT EXISTS(SELECT * FROM  SIGNBORD_MAP B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_SIGNBORD_MAP WITH ENCRYPTION AS SELECT SIGNBORD_ID FROM SIGNBORDDATA A WHERE NOT EXISTS(SELECT * FROM  SIGNBORD_MAP B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID WITH CHECK OPTION", , adCmdUnknown
                        Else
'                                Call SET_Execute("CREATE VIEW VIEW_SIGNBORD_MAP AS SELECT SIGNBORD_ID FROM SIGNBORD_MAP A WHERE NOT EXISTS(SELECT * FROM SIGNBORDDATA  B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID")
                                Globle_Connective.Execute "ALTER VIEW VIEW_SIGNBORD_MAP WITH ENCRYPTION AS SELECT SIGNBORD_ID FROM SIGNBORDDATA A WHERE NOT EXISTS(SELECT * FROM  SIGNBORDDATA B WHERE A.SIGNBORD_ID=B.SIGNBORD_ID) GROUP BY SIGNBORD_ID WITH CHECK OPTION", , adCmdUnknown
                        End If
        End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set rs = Nothing
        Set RsErr = Nothing
End Sub

Private Sub Grid_Err_DblClick()
        On Error Resume Next
        Call checkOverlayStatus(indexOverlay)
        Call GoMap(Grid_Err.TextMatrix(Grid_Err.Row, 0), cboOverlay.ListIndex, indexOverlay)
End Sub

Private Sub GRID_ID_DblClick()
        On Error Resume Next
        If opMIS.Value = True Then
                Call checkOverlayStatus(indexOverlay)
                Call GoMap(GRID_ID.TextMatrix(GRID_ID.Row, 0), cboOverlay.ListIndex, indexOverlay)
        End If
End Sub

Private Sub Image_Print_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
        Image_Print(Index).Top = Image_Print(Index).Top + 39
End Sub

Private Sub Image_Print_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
        On Error GoTo ErrPrint
        
        If Index = 0 Then
                Image_Print(Index).Top = 5640
                Call CreateView
        Else
                Image_Print(Index).Top = 5640
        End If
        With Clone_Form.CTReport
                .Reset
                Clone_Form.CTReport.Connect = str_ConnectReport
                .DiscardSavedData = True
                If Index = 0 Then
                        Select Case cboOverlay.ListIndex
                                Case 0
                                        .ReportFileName = App.Path & "\Report\Rpt_Land_Map.rpt"
                                Case 1
                                        .ReportFileName = App.Path & "\Report\Rpt_Building_Map.rpt"
                                Case 2
                                        .ReportFileName = App.Path & "\Report\Rpt_Signbord_Map.rpt"
                        End Select
                Else
                        Select Case cboOverlay.ListIndex
                                Case 0
                                        .ReportFileName = App.Path & "\Report\Rpt_Land_Map_Err.rpt"
                                Case 1
                                        .ReportFileName = App.Path & "\Report\Rpt_Building_Map_Err.rpt"
                                Case 2
                                        .ReportFileName = App.Path & "\Report\Rpt_Signbord_Map_Err.rpt"
                        End Select
                End If
                .WindowShowPrintSetupBtn = True
                .Destination = crptToWindow
                .WindowState = crptMaximized
                .WindowShowExportBtn = True
                .Action = 1
        End With
        Exit Sub
ErrPrint:
        MsgBox Err.Description
End Sub

Private Function CheckID(MapID As String) As Boolean
        Dim strID As String
        
        strID = Trim$(MapID)
        If Len(strID) = 6 Or Len(strID) = 10 Then
                If Len(strID) = 6 Then
                        If Asc(Mid$(strID, 3, 1)) > 64 And Asc(Mid$(strID, 3, 1)) < 91 And InStr(1, "0123456789", Mid$(strID, 1, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 2, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 4, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 5, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 6, 1), vbTextCompare) <> 0 Then
                                CheckID = True
                        Else
                                CheckID = False
                        End If
                Else
                        If Asc(Mid$(strID, 3, 1)) > 64 And Asc(Mid$(strID, 3, 1)) < 91 And InStr(1, "0123456789", Mid$(strID, 1, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 2, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 4, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 5, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 6, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 8, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 9, 1), vbTextCompare) <> 0 And InStr(1, "0123456789", Mid$(strID, 10, 1), vbTextCompare) <> 0 And Mid$(strID, 7, 1) = "/" Then
                                CheckID = True
                        Else
                                CheckID = False
                        End If
                End If
        Else
                CheckID = False
        End If
End Function

