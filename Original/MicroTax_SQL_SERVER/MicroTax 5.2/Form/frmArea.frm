VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_Area 
   BackColor       =   &H00808080&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "��¡���ŧ��鹷��"
   ClientHeight    =   4965
   ClientLeft      =   3075
   ClientTop       =   2985
   ClientWidth     =   7755
   Icon            =   "frmArea.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00CC6633&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   1
      Left            =   2970
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3855
      Visible         =   0   'False
      Width           =   2805
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid flexArea 
      Height          =   4230
      Left            =   0
      TabIndex        =   4
      Top             =   375
      Width           =   7770
      _ExtentX        =   13705
      _ExtentY        =   7461
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   12632256
      ForeColorSel    =   255
      BackColorBkg    =   14079702
      GridColor       =   12632256
      FocusRect       =   0
      GridLinesFixed  =   1
      AllowUserResizing=   1
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   9
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Index           =   0
      Left            =   990
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   4620
      Width           =   6765
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��鹷����� : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Index           =   1
      Left            =   0
      TabIndex        =   3
      Top             =   4620
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "��Ҵ��鹷��Թ ���/�ҹ/��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Index           =   0
      Left            =   -15
      TabIndex        =   0
      Top             =   0
      Width           =   7800
   End
End
Attribute VB_Name = "Frm_Area"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub flexArea_DblClick()
Dim strCode As String
Dim i As Integer, indexOverlay As Integer

For i = 0 To Frm_Map.Sis1.GetInt(SIS_OT_WINDOW, 0, "_nOverlay&") - 1
    If Frm_Map.Sis1.GetStr(SIS_OT_OVERLAY, i, "_name$") = flexArea.TextMatrix(flexArea.Row, 3) Then
        indexOverlay = i
        Exit For
    End If
Next
    If indexOverlay = iniZone Then
            If LenB(flexArea.TextMatrix(flexArea.Row, 4)) = 0 Then
                MsgBox "�������ö���Ң������� ���ͧ�ҡ���������⫹���͡", vbOKOnly + vbInformation, "���й�"
                Exit Sub
            Else
                strCode = "(zone_block$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
            End If
    ElseIf indexOverlay = iniLand Then
            If LenB(flexArea.TextMatrix(flexArea.Row, 4)) = 0 Then
                MsgBox "�������ö���Ң������� ���ͧ�ҡ��������ʷ��Թ", vbOKOnly + vbInformation, "���й�"
                Exit Sub
            Else
                strCode = "(LAND_ID$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
            End If
    ElseIf indexOverlay = iniBuliding Then
            If LenB(flexArea.TextMatrix(flexArea.Row, 4)) = 0 Then
                MsgBox "�������ö���Ң������� ���ͧ�ҡ����������ç���͹", vbOKOnly + vbInformation, "���й�"
                Exit Sub
            Else
                strCode = "(BUILDING_ID$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
            End If
    ElseIf indexOverlay = iniSignbord Then
            If LenB(flexArea.TextMatrix(flexArea.Row, 4)) = 0 Then
                MsgBox "�������ö���Ң������� ���ͧ�ҡ��������ʻ���", vbOKOnly + vbInformation, "���й�"
                Exit Sub
            Else
                strCode = "(SIGNBOARD_ID$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
            End If
    Else
            MsgBox "�������ö���Ң�������", vbOKOnly + vbInformation, "���й�"
    End If

'Select Case flexArea.TextMatrix(flexArea.Row, 3)
 '   Case "⫹���͡"
  '          If flexArea.TextMatrix(flexArea.Row, 4) = "" Then
   '             MsgBox "�������ö���Ң������� ���ͧ�ҡ���������⫹���͡", vbOKOnly + vbInformation, "���й�"
    '            Exit Sub
     '       Else
      '          strCode = "(zone_block$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
       '     End If
    'Case "�ŧ���Թ"
     '       If flexArea.TextMatrix(flexArea.Row, 4) = "" Then
      '          MsgBox "�������ö���Ң������� ���ͧ�ҡ��������ʷ��Թ", vbOKOnly + vbInformation, "���й�"
       '         Exit Sub
        '    Else
         '       strCode = "(LAND_CODE$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
          '  End If
    'Case "�ç���͹"
     '       If flexArea.TextMatrix(flexArea.Row, 4) = "" Then
      '          MsgBox "�������ö���Ң������� ���ͧ�ҡ����������ç���͹", vbOKOnly + vbInformation, "���й�"
       '         Exit Sub
        '    Else
         '       strCode = "(BUILDING_CODE$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
          '  End If
    'Case "����"
     '       If flexArea.TextMatrix(flexArea.Row, 4) = "" Then
      '          MsgBox "�������ö���Ң������� ���ͧ�ҡ��������ʻ���", vbOKOnly + vbInformation, "���й�"
       '         Exit Sub
        '    Else
         '       strCode = "(SIGNBOARD_CODE$ = " & """" & flexArea.TextMatrix(flexArea.Row, 4) & """)"
          '  End If
    'Case Else
     '       MsgBox "�������ö���Ң�������", vbOKOnly + vbInformation, "���й�"
'End Select

With Frm_Map.Sis1
    .DeselectAll
    .EmptyList "selectitem"
    .CreatePropertyFilter "filter", strCode
     If indexOverlay = iniZone Then
        .ScanOverlay "selectitem", iniZone, "filter", ""
    ElseIf indexOverlay = iniLand Then
      .ScanOverlay "selectitem", iniLand, "filter", ""
    ElseIf indexOverlay = iniBuliding Then
      .ScanOverlay "selectitem", iniBuliding, "filter", ""
    ElseIf indexOverlay = iniSignbord Then
      .ScanOverlay "selectitem", iniSignbord, "filter", ""
    End If
'Select Case flexArea.TextMatrix(flexArea.Row, 3)
 '   Case "⫹���͡"
  '      .ScanOverlay "selectitem", 0, "filter", ""
   ' Case "�ŧ���Թ"
    '  .ScanOverlay "selectitem", 3, "filter", ""
    'Case "�ç���͹"
     ' .ScanOverlay "selectitem", 5, "filter", ""
    'Case "����"
     ' .ScanOverlay "selectitem", 6, "filter", ""
'End Select
    
    If .GetListSize("selectitem") > 0 Then
        .SelectList "selectitem"
        .DoCommand "AComZoomSelect"
     If (indexOverlay = iniZone) Or (indexOverlay = iniLand) Or (indexOverlay = iniBuliding) Then
        .DoCommand "AComZoomOut"
    'ElseIf indexOverlay = mduVar.nOverlayLand Then
     '   .DoCommand "AComZoomOut"
    'ElseIf indexOverlay = mduVar.nOverlayBuilding Then
     '   .DoCommand "AComZoomOut"
    ElseIf indexOverlay = iniSignbord Then
        .SetFlt SIS_OT_WINDOW, 0, "_displayScale#", 1700
    End If
'Select Case flexArea.TextMatrix(flexArea.Row, 3)
 '   Case "⫹���͡"
  '      .DoCommand "AComZoomOut"
   ' Case "�ŧ���Թ"
    '    .DoCommand "AComZoomOut"
    'Case "�ç���͹"
     '   .DoCommand "AComZoomOut"
    'Case "����"
     '   .SetFlt SIS_OT_WINDOW, 0, "_displayScale#", 1700
'End Select
        
    End If
End With
If Frm_Map.Sis1.GetNumSel = 0 Then
    MsgBox "��������� " & flexArea.TextMatrix(flexArea.Row, 4) & " ��Ἱ�����  ", vbOKOnly + vbInformation, "���й�"
    Exit Sub
End If
End Sub

Private Sub Form_Load()
On Error GoTo errHandle

Dim dblArea As Single, dblAreaTotal As Single
Dim i As Long, j As Long, newRow As Long, numB As Long

With flexArea
    .FormatString = "|^�ӴѺ|^��Դ������|^��鹢�����|^����|^���|^�ҹ|^���ҧ��|^���ҧ����"
    .ColWidth(0) = 0
    .ColWidth(1) = 550
    .ColWidth(2) = 1000
    .ColWidth(3) = 1500
    .ColWidth(4) = 1300: .ColAlignment(4) = 1
    .ColWidth(5) = 600
    .ColWidth(6) = 550
    .ColWidth(7) = 800: .ColAlignment(7) = 7
    .ColWidth(8) = 1100: .ColAlignment(8) = 7
End With
With Frm_Map.Sis1
    .CreateListFromSelection ("SelectItems")
    i = .GetListSize("SelectItems")
    numB = 1
For j = 0 To i - 1
    .OpenList "SelectItems", j
    dblArea = .GetFlt(SIS_OT_CURITEM, 0, "_area#")
    dblAreaTotal = dblAreaTotal + dblArea
    newRow = flexArea.Rows - 1
    flexArea.TextMatrix(newRow, 1) = numB
    If .GetStr(SIS_OT_CURITEM, 0, "_class$") = "Area" Then
    flexArea.TextMatrix(newRow, 2) = .GetStr(SIS_OT_CURITEM, 0, "_class$")
    Dim X As Integer, indexOverlay As Integer
    
    For X = 0 To .GetInt(SIS_OT_WINDOW, 0, "_nOverlay&") - 1
        If .GetStr(SIS_OT_DATASET, .GetInt(SIS_OT_OVERLAY, X, "_nDataset&"), "_name$") = .GetStr(SIS_OT_CURITEM, 0, "_dataset$") Then
            flexArea.TextMatrix(newRow, 3) = .GetStr(SIS_OT_OVERLAY, X, "_name$")
            indexOverlay = X
            Exit For
        End If
    Next
    
    If indexOverlay = iniZone Then
        flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "zone_block$")
    ElseIf indexOverlay = iniLand Then
        flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "LAND_ID$")
    ElseIf indexOverlay = iniBuliding Then
        flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "BUILDING_ID$")
    ElseIf indexOverlay = iniSignbord Then
        flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "SIGNBOARD_ID$")
    Else
        flexArea.TextMatrix(newRow, 4) = "???"
    End If
    'Select Case flexArea.TextMatrix(newRow, 3)
    'Case "⫹���͡"
     '       flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "zone_block$")
    'Case "�ŧ���Թ"
     '       flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "LAND_CODE$")
    'Case "�ç���͹"
     '       flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "BUILDING_CODE$")
    'Case "����"
     '       flexArea.TextMatrix(newRow, 4) = .GetStr(SIS_OT_CURITEM, 0, "SIGNBOARD_CODE$")
    'Case Else
     '       flexArea.TextMatrix(newRow, 4) = "???"
    'End Select  ((CDbl(dblArea) Mod 1600) Mod 400) / 4
'    Dim ddd As Single
'    ddd = Val((dblArea Mod 400))
    flexArea.TextMatrix(newRow, 5) = CSng(dblArea) \ 1600
    flexArea.TextMatrix(newRow, 6) = (CSng(dblArea) Mod 1600) \ 400
    flexArea.TextMatrix(newRow, 7) = Format(modF((modF(CSng(dblArea), 1600)), 400) / 4, "#0.00")
    flexArea.TextMatrix(newRow, 8) = Format(dblArea, "#,##0.00")
    flexArea.Rows = flexArea.Rows + 1
    numB = numB + 1
    End If
Next
flexArea.Rows = flexArea.Rows - 1

Text1(0) = CSng(dblAreaTotal) \ 1600 & " ��� /" & (CSng(dblAreaTotal) Mod 1600) \ 400 & " �ҹ /" & Format(modF((modF(CSng(dblAreaTotal), 1600)), 400) / 4, "#0.00") & " ���ҧ�� /  (" & Format(dblAreaTotal, "#,##0.00") & " ���ҧ����)"
'Text1(1) = Format(dblArea, "##.00") & " ���ҧ����"
.EmptyList ("SelectItems")
Me.MousePointer = 0
End With

errHandle:
If Err.Number <> 0 Then
    MsgBox "Error Number : " & Err.Number & " - " & Err.Description, vbOKOnly + vbCritical, "��ͼԴ��Ҵ"
End If
End Sub

Private Function modF(X As Double, yMod As Single) As Double
        Dim dd As Single
        
        If X < yMod Then
                modF = X
                Exit Function
        End If
        Do
                X = X - yMod
        Loop While Not X < yMod
        modF = X
End Function

Private Sub Form_Unload(Cancel As Integer)
        Set Frm_Area = Nothing
End Sub

