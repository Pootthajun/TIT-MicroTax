VERSION 5.00
Begin VB.Form Frm_CreateAreaOfScale 
   BackColor       =   &H00CCCCCC&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "��Ѻ����ҵ��ŧ���Թ"
   ClientHeight    =   1830
   ClientLeft      =   3420
   ClientTop       =   6990
   ClientWidth     =   4770
   Icon            =   "Frm_CreateAreaOfScale.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1830
   ScaleWidth      =   4770
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      BackColor       =   &H00D6D6D6&
      Caption         =   "��Ѻ��鹷��"
      Default         =   -1  'True
      Height          =   330
      Left            =   1860
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1440
      Width           =   1245
   End
   Begin VB.TextBox txtVar 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3690
      TabIndex        =   2
      Text            =   "0"
      Top             =   930
      Width           =   735
   End
   Begin VB.TextBox txtNgan 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2070
      TabIndex        =   1
      Text            =   "0"
      Top             =   915
      Width           =   735
   End
   Begin VB.TextBox txtRai 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   330
      TabIndex        =   0
      Text            =   "0"
      Top             =   885
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "  ���                                  �ҹ                           ���ҧ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   480
      TabIndex        =   5
      Top             =   630
      Width           =   3870
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00756E60&
      BackStyle       =   0  'Transparent
      Caption         =   "�кآ�Ҵ��鹷�� ?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Index           =   0
      Left            =   1560
      TabIndex        =   4
      Top             =   90
      Width           =   1905
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00E2E2E2&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   930
      Left            =   -30
      Top             =   465
      Width           =   4830
   End
End
Attribute VB_Name = "Frm_CreateAreaOfScale"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Command1_Click()
        On Error GoTo errHandle
        Dim dblAreaPresent As Double, dblAreaWant As Double, dblScale As Double, X As Double, y As Double
        Dim i As Integer, j As Integer

        If LenB(Trim$(txtRai)) = 0 Then txtRai = 0
        If LenB(Trim$(txtNgan)) = 0 Then txtNgan = 0
        If LenB(Trim$(txtVar)) = 0 Then txtVar = 0

        dblAreaWant = (CDbl(txtRai) * 1600) + (CDbl(txtNgan) * 400) + (CDbl(txtVar) * 4)
        X = Sqr(dblAreaWant)


        Frm_Map.Sis1.CreateListFromSelection ("SelectItems")
        i = Frm_Map.Sis1.GetListSize("SelectItems")

        For j = 0 To i - 1
                Frm_Map.Sis1.OpenList "SelectItems", j
                If Frm_Map.Sis1.GetStr(SIS_OT_CURITEM, 0, "_class$") = "Area" Then
                        dblAreaPresent = Frm_Map.Sis1.GetFlt(SIS_OT_CURITEM, 0, "_area#")
                        y = Sqr(dblAreaPresent)
                        dblScale = X / y
                Exit For
                End If
        Next
        If dblScale = 0 Then
                MsgBox "���������١��ͧ �������ö��Ѻ����ҵþ�鹷���ԧ�� !", vbOKOnly + vbInformation, "Warning !"
                Unload Me
                Exit Sub
        End If
'MsgBox dblAreaPresent & " scale = " & dblScale
        For j = 0 To i - 1
                Frm_Map.Sis1.OpenList "SelectItems", j
                With Frm_Map.Sis1
                        .SetFlt SIS_OT_CURITEM, 0, "_sx#", (.GetFlt(SIS_OT_CURITEM, 0, "_sx#")) * dblScale
                        .SetFlt SIS_OT_CURITEM, 0, "_sy#", (.GetFlt(SIS_OT_CURITEM, 0, "_sy#")) * dblScale
                End With
                Frm_Map.Sis1.OpenList "SelectItems", j
        Next

        Frm_Map.Sis1.EmptyList ("SelectItems")
'Frm_map.sis1.Redraw 0
        Unload Me
        Exit Sub
errHandle:
        If Err.Number = 13 Then
                MsgBox "��س�����Ң��������١��ͧ", vbOKOnly + vbInformation, "���й�"
                txtRai.SetFocus
        Else
                MsgBox Err.Number & "-" & Err.Description
        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Frm_CreateAreaOfScale = Nothing
End Sub

Private Sub txtNgan_GotFocus()
    With txtNgan
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtNgan_KeyPress(KeyAscii As Integer)
        Call txtRai_KeyPress(KeyAscii)
End Sub


Private Sub txtRai_GotFocus()
    With txtRai
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtRai_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
End Sub


Private Sub txtVar_GotFocus()
    With txtVar
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtVar_KeyPress(KeyAscii As Integer)
        Call txtRai_KeyPress(KeyAscii)
End Sub


