VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{EBAB1620-5BB5-11CF-80D0-004F4B002122}#5.0#0"; "Sis.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form Frm_PrintMap 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ẻ��������ҧ���Թ"
   ClientHeight    =   8685
   ClientLeft      =   5625
   ClientTop       =   5025
   ClientWidth     =   13920
   Icon            =   "Frm_PrintMap.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8685
   ScaleWidth      =   13920
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin SisLib.Sis Sis1 
      Height          =   6435
      Left            =   3930
      TabIndex        =   23
      Top             =   780
      Width           =   8865
      _Version        =   327680
      _ExtentX        =   15637
      _ExtentY        =   11351
      _StockProps     =   64
      BorderBevel     =   -1  'True
      Display         =   0
   End
   Begin VB.CommandButton cmd_Recall_View_Template 
      Height          =   345
      Left            =   3960
      Picture         =   "Frm_PrintMap.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Reset Template To Nomal View"
      Top             =   270
      Width           =   375
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2550
      Top             =   1980
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":2084
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":261E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":2BB8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":3152
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":36EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":3C86
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":4220
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":47BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_PrintMap.frx":4D54
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Btn_PreformPrint 
      Cancel          =   -1  'True
      Caption         =   "��ŧ"
      Height          =   375
      Left            =   9765
      TabIndex        =   18
      Top             =   8190
      Width           =   990
   End
   Begin VB.Frame Frame5 
      Caption         =   "�ǡ�д��"
      Height          =   690
      Left            =   90
      TabIndex        =   13
      Top             =   5310
      Width           =   3750
      Begin VB.OptionButton OptX 
         Caption         =   "�ǹ͹"
         Height          =   195
         Left            =   2205
         TabIndex        =   15
         Tag             =   " Landscape"
         Top             =   315
         Width           =   1140
      End
      Begin VB.OptionButton OptY 
         Caption         =   "�ǵ��"
         Height          =   195
         Left            =   540
         TabIndex        =   14
         Tag             =   " Portrait"
         Top             =   315
         Value           =   -1  'True
         Width           =   1140
      End
   End
   Begin VB.Frame Frame4 
      Height          =   510
      Left            =   10680
      TabIndex        =   9
      Top             =   7335
      Width           =   2100
      Begin VB.TextBox txtCopy 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   900
         MaxLength       =   2
         TabIndex        =   10
         Text            =   "1"
         Top             =   135
         Width           =   450
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   330
         Left            =   1350
         TabIndex        =   20
         Top             =   135
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   582
         _Version        =   327681
         Value           =   1
         BuddyControl    =   "txtCopy"
         BuddyDispid     =   196615
         OrigRight       =   255
         OrigBottom      =   195
         Max             =   50
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ :"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   12
         Top             =   225
         Width           =   585
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ش"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   3
         Left            =   1710
         TabIndex        =   11
         Top             =   225
         Width           =   210
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "˹�ҵ�ҧ�ʴ���"
      Height          =   780
      Left            =   90
      TabIndex        =   6
      Top             =   6075
      Width           =   3750
      Begin VB.TextBox TxtScale 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1305
         TabIndex        =   8
         Text            =   "1"
         Top             =   270
         Width           =   2310
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҵ����ǹ 1 :"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   7
         Top             =   315
         Width           =   960
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "����ͧ�����"
      Height          =   960
      Left            =   90
      TabIndex        =   4
      Top             =   810
      Width           =   3750
      Begin MSComctlLib.ImageCombo cmbDefaultPrinter 
         Height          =   330
         Left            =   135
         TabIndex        =   5
         Top             =   405
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   0
         BackColor       =   16777215
         MousePointer    =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "Printer"
         ImageList       =   "ImageList1"
      End
   End
   Begin VB.Frame Frame1 
      Height          =   600
      Left            =   45
      TabIndex        =   2
      Top             =   90
      Width           =   12750
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   330
         Left            =   10620
         TabIndex        =   22
         Top             =   180
         Width           =   2070
         _ExtentX        =   3651
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   6
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AcomUndo"
               Object.ToolTipText     =   "Undo"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AcomRedo"
               Object.ToolTipText     =   "Redo"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AComBoxText"
               Object.ToolTipText     =   "Box Text"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AComText"
               Object.ToolTipText     =   "Point Text"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AComBoxLabel"
               Object.ToolTipText     =   "Box Label"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "AComPasteFrom"
               Object.ToolTipText     =   "PasteFrom"
               ImageIndex      =   9
            EndProperty
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Print Templates Wizard :  �ٻẺ�蹾����Ἱ���"
         Height          =   240
         Left            =   180
         TabIndex        =   3
         Top             =   225
         Width           =   6810
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "�����"
      Height          =   375
      Left            =   10755
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   8190
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "¡��ԡ"
      Height          =   375
      Left            =   11745
      TabIndex        =   1
      Top             =   8190
      Width           =   990
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3405
      Left            =   90
      TabIndex        =   16
      Top             =   1845
      Width           =   3780
      _ExtentX        =   6668
      _ExtentY        =   6006
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   619
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   5
      Appearance      =   1
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ѧ�����..."
      Height          =   195
      Left            =   90
      TabIndex        =   21
      Top             =   8415
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ready to Print..."
      Height          =   195
      Left            =   3915
      TabIndex        =   19
      Top             =   7380
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      X1              =   135
      X2              =   12735
      Y1              =   8010
      Y2              =   8010
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00C0C0C0&
      BorderWidth     =   2
      X1              =   135
      X2              =   12735
      Y1              =   8010
      Y2              =   8010
   End
End
Attribute VB_Name = "Frm_PrintMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long) As Long
Private Declare Function CreateDC Lib "gdi32" Alias "CreateDCA" (ByVal lpDriverName As String, ByVal lpDeviceName As String, ByVal lpOutput As Long, ByVal lpInitData As Long) As Long
Private Declare Function DeviceCapabilities Lib "winspool.drv" Alias "DeviceCapabilitiesA" (ByVal lpDeviceName As String, ByVal lpPort As String, ByVal iIndex As Long, ByVal lpOutput As String, ByVal lpDevMode As Long) As Long
Private Declare Function DeviceCapabilities2 Lib "winspool.drv" Alias "DeviceCapabilitiesA" (ByVal lpDeviceName As String, ByVal lpPort As String, ByVal iIndex As Long, ptOutput As Any, ByVal lpDevMode As Long) As Long
Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function DeleteDC Lib "gdi32" (ByVal hdc As Long) As Long
Private Type PrinterProperty
                PrinterName As String
                PrinterPapersize As Integer
End Type

Private Const DC_PAPERNAMES = 16
Private Const DC_PAPERS = 2

Dim TgPaperSize() As Long
Dim TsPaperSize() As String
Dim getpapercount As Long
Dim GetPrinterallProperty() As PrinterProperty

Dim mhDCPrinter As Long
Dim mPrinterDeviceName As String
Dim mPrinterDriverName As String
Dim mPrinterPort As String
Dim CountCurrentOverlay As Integer
Dim theExtent As String, Template_Temp As String
Dim x1#, y1#, z1#, x2#, y2#, z2#

Public Function GetPrinterPaperSizes(ByRef gPaperSize() As Long, ByRef sPaperSize() As String, ByRef gPaperSizeCounter As Long) As Long
On Error GoTo Er
Dim PaperCount As Long
Dim PaperName As String
Dim sA As String
Dim tName As String
Dim iA As Long
Dim dl As Long
Dim gArray() As Integer
Dim nodeX As Node
Dim j As Byte
    j = 0
    TreeView1.ImageList = ImageList1
'initialise with failure
GetPrinterPaperSizes = 0

'is there a printer set?
If mhDCPrinter = 0 Then Exit Function

'find out how many paper names there are
PaperCount = DeviceCapabilities(mPrinterDeviceName, mPrinterPort, DC_PAPERNAMES, vbNullString, 0&)

'error
If PaperCount <= 0 Then Exit Function

'now dimension the string large enough to hold them all
PaperName = String$(64 * PaperCount, 0)

'get 'em
dl = DeviceCapabilities(mPrinterDeviceName, mPrinterPort, DC_PAPERNAMES, PaperName, 0)

If dl = -1 Then Exit Function

'ok
ReDim sPaperSize(1 To PaperCount) As String
ReDim GetPrinterallProperty(PaperCount)
'load the results
        For iA = 1 To PaperCount
                            tName = Mid$(PaperName, (iA - 1) * 64 + 1)
                            sPaperSize(iA) = StripNulls(tName)
                            GetPrinterallProperty(iA).PrinterName = tName
        Next iA

'find out how many paper names there are, using the default DevMode
dl = 0
dl = DeviceCapabilities(mPrinterDeviceName, mPrinterPort, DC_PAPERS, vbNullString, 0&)

If dl <= 0 Or dl <> PaperCount Then Exit Function

ReDim gArray(1 To dl) As Integer

dl = DeviceCapabilities2(mPrinterDeviceName, mPrinterPort, DC_PAPERS, gArray(1), 0&)

If dl <= 0 Then Exit Function

ReDim gPaperSize(1 To PaperCount) As Long
For iA = 1 To PaperCount
        GetPrinterallProperty(iA).PrinterPapersize = gArray(iA)
        If InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A0") > 0 Or InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A1") > 0 _
          Or InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A2") > 0 Or InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A3") > 0 _
          Or InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A4") > 0 Then
'        If InStr(1, UCase(Trim$(GetPrinterallProperty(iA).PrinterName)), "A4") > 0 Then
                Set nodeX = TreeView1.Nodes.Add(, , "PaperSize" & GetPrinterallProperty(iA).PrinterPapersize, Trim$(GetPrinterallProperty(iA).PrinterName), 4)
        End If
Next iA
'For iA = 1 To PaperCount
'                                gPaperSize(iA) = gArray(iA)
'                                GetPrinterallProperty(iA).PrinterPapersize = gPaperSize(iA)
'Next iA
'
'For j = 1 To PaperCount
'                    If InStr(1, UCase(Trim$(GetPrinterallProperty(j).PrinterName)), "A0") > 0 Or InStr(1, UCase(Trim$(GetPrinterallProperty(j).PrinterName)), "A1") > 0 _
'                      Or InStr(1, UCase(Trim$(GetPrinterallProperty(j).PrinterName)), "A2") > 0 Or InStr(1, UCase(Trim$(GetPrinterallProperty(j).PrinterName)), "A3") > 0 _
'                      Or InStr(1, UCase(Trim$(GetPrinterallProperty(j).PrinterName)), "A4") > 0 Then
'                            Set nodeX = TreeView1.Nodes.Add(, , "PaperSize" & GetPrinterallProperty(j).PrinterPapersize, Trim$(GetPrinterallProperty(j).PrinterName), 4)
'                    End If
'Next

        gPaperSizeCounter = PaperCount
        GetPrinterPaperSizes = 1
        Erase gArray()
        Set nodeX = Nothing
Er:
        Erase gArray()
        Set nodeX = Nothing
End Function

Private Function CreatePrinterDC(sPrinterName As String) As Long
On Error GoTo Er

Dim X As Printer
Dim dl As Long

CreatePrinterDC = 0

'If mhDCPrinter <> 0 Then 'dl = DeleteDC(mhDCPrinter)

For Each X In Printers
            If X.DeviceName = sPrinterName Then
                    mhDCPrinter = CreateDC(X.DriverName, X.DeviceName, 0&, 0&)
                        If mhDCPrinter <> 0 Then
                        'success
                        CreatePrinterDC = 1
                        'store these values
                        mPrinterDeviceName = X.DeviceName
                        mPrinterDriverName = X.DriverName
                        mPrinterPort = X.Port
                        'get DEVMODE
                        'GetDEVMODE '<<< ignore this
                        'load data
                        'LoadPrinterData '<<< ignore this
                        End If
                            Exit Function
            End If
Next X

Set X = Nothing
        Call ClosePrinter(mhDCPrinter)
Er:
Set X = Nothing
        Call ClosePrinter(mhDCPrinter)
End Function

Private Function StripNulls(OriginalStr As String) As String
        On Error Resume Next
        If (InStr(OriginalStr, Chr$(0)) > 0) Then
                OriginalStr = Left(OriginalStr, InStr(OriginalStr, Chr$(0)) - 1)
        End If
        StripNulls = Trim$(OriginalStr)
End Function

Private Sub ParseList(lstCtl As Control, ByVal BUFFER As String)
    Dim i As Byte
    Dim s As String
    Do
        i = InStr(BUFFER, Chr(0))
        If i > 0 Then
            s = Left(BUFFER, i - 1)
            If Len(Trim(s)) Then lstCtl.ComboItems.Add , , s, 1
                BUFFER = Mid(BUFFER, i + 1)
        Else
            If Len(Trim(BUFFER)) Then lstCtl.ComboItems.Add , , s, 1
                 BUFFER = ""
             End If
    Loop While i > 0
End Sub

Private Sub Btn_PreformPrint_Click()
        On Error GoTo ErrPrint
                            Call ShowTemplate(TreeView1.SelectedItem.Text, "SHOWPROMPTPRINT")
                            Sis1.Visible = True
'                            Sis1.Left = 3915
'                            Sis_Template.Visible = False
                            Btn_PreformPrint.Visible = False
                            cmdPrint.Visible = True
'                            TreeView1.Enabled = False
'                            OptX.Enabled = False
'                            OptY.Enabled = False
'                            cmbDefaultPrinter.Enabled = False
                            Label3.Visible = True
                            Label3.Caption = "Ready to Print..." & vbCrLf & "��Ҵ��д�� : " & TreeView1.SelectedItem.Text & vbCrLf & "�ǡ�д�� : "
                            If OptX.Value Then
                                    Label3.Caption = Label3.Caption & OptX.Caption
                            Else
                                    Label3.Caption = Label3.Caption & OptY.Caption
                            End If
                            theExtent = Sis1.GetViewExtent
        Exit Sub
ErrPrint:
'                            Sis_Template.Swd = -1
End Sub

Private Sub cmd_Recall_View_Template_Click()
        Frm_PrintMap.Sis1.SetViewExtent x1#, y1#, z1#, x2#, y2#, z2#
End Sub

Private Sub cmdCancel_Click()
'         DoEvents
'         Sis1.DoCommand "AComSelectSlide"
'         Sis1.DeselectAll
        Unload Me
End Sub

Private Sub cmdPrint_Click()
If Val(TxtScale.Text) <= 0 Then
    MsgBox "��س�����ҵ����ǹ�ҡ���� 1", vbOKOnly + vbInformation, "���й�"
    Exit Sub
End If
If Val(txtCopy.Text) < 1 Then
    MsgBox "��س����ӹǹ�����ҡ���� 1 �ش", vbOKOnly + vbInformation, "���й�"
            Call SetSelFocus(txtCopy)
            Exit Sub
End If
Dim mPaperOrient As Long, mPaperSize As Long, mCopy As Long
If OptX.Value Then
        mPaperOrient = 2
Else
        mPaperOrient = 1
End If
        mPaperSize = CLng(Mid(TreeView1.SelectedItem.Key, 10, Len(TreeView1.SelectedItem.Key)))
        mCopy = CLng(txtCopy.Text)
With Sis1
        Me.Refresh
        Label4.Caption = "���ѧ�����..."
        Label4.Visible = True
        Dim x1#, y1#, z1#, x2#, y2#, z2#
                .SplitExtent x1#, y1#, z1#, x2#, y2#, z2#, theExtent
                .SetViewExtent x1#, y1#, z1#, x2#, y2#, z2#
                .SetStr SIS_OT_PRINTER, 0, "_device$", mPrinterDeviceName
                .SetStr SIS_OT_PRINTER, 0, "_driver$", mPrinterDriverName
                .SetStr SIS_OT_PRINTER, 0, "_output$", mPrinterPort
                .SetInt SIS_OT_PRINTER, 0, "_orientation&", mPaperOrient
                .SetInt SIS_OT_PRINTER, 0, "_paperSize&", mPaperSize
                '.SetInt SIS_OT_PRINTER, 0, "_paperLength&", bPaperSizeLength
                '.SetInt SIS_OT_PRINTER, 0, "_paperWidth&", bPaperSizeWidth
                .SetInt SIS_OT_PRINTER, 0, "_copies&", mCopy
                .SendPrint "", "", "", SIS_PRINTCAPS_QUERY, 1
End With
            Call DeleteDC(mhDCPrinter)
            Label4.Caption = "��þ���������������ó�"
            Label4.Visible = False
            Label3.Visible = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
        Select Case KeyCode
          'Function keys
          Case vbKeyF3  'Find dialog
            Sis1.DoCommand "AComSelect"
          Case vbKeyF5  'Redraw
            Sis1.Redraw SIS_CURRENTWINDOW
          Case vbKeyF7  'Select All
            Sis1.SelectAll
          Case vbKeyF8  'Deselect All
            Sis1.DeselectAll
            
          'Alpha keys with Ctrl key
          Case vbKeyC And Shift = vbCtrlMask 'Copy
            Sis1.DoCommand "AComCopy"
          Case vbKeyV And Shift = vbCtrlMask 'Paste
            Sis1.DoCommand "AComPaste"
          Case vbKeyX And Shift = vbCtrlMask 'Cut
            Sis1.DoCommand "AComCut"
          Case vbKeyY And Shift = vbCtrlMask 'Redo
            Sis1.DoCommand "AComRedo"
          Case vbKeyZ And Shift = vbCtrlMask 'Undo
            Sis1.DoCommand "AComUndo"
          Case vbKeyDelete
            Sis1.DoCommand "AComDelete"
        End Select
End Sub

Private Sub Form_Load()
Dim BUFFER As String
Dim i As Byte
            Btn_PreformPrint.Left = 10755
            CountCurrentOverlay = Frm_Map.Sis1.GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
            cmbDefaultPrinter.ComboItems.Clear
            BUFFER = Space(5000)
            Call GetProfileString("PrinterPorts", vbNullString, "", BUFFER, Len(BUFFER))  'list printer type style "hp,epson stylelus 3000,"
            ' Display the list of printer in the ListBox
            Call ParseList(cmbDefaultPrinter, BUFFER)
        For i = 1 To cmbDefaultPrinter.ComboItems.Count
                If cmbDefaultPrinter.ComboItems(i).Text = Printer.DeviceName Then
                        Set cmbDefaultPrinter.SelectedItem = cmbDefaultPrinter.ComboItems(i)
                        Exit For
                End If
        Next
        Call cmbDefaultPrinter_Click
End Sub

Private Sub cmbDefaultPrinter_Click()
        TreeView1.Nodes.Clear
        Call CreatePrinterDC(cmbDefaultPrinter.Text)
        Call GetPrinterPaperSizes(TgPaperSize(), TsPaperSize(), getpapercount)
End Sub

Private Sub OptX_Click()
On Error Resume Next
    Btn_PreformPrint.Visible = True
    cmdPrint.Visible = False
    Btn_PreformPrint.Left = 10755
    Call ShowTemplate(TreeView1.SelectedItem.Text, "SHOWTEMPLATE")
End Sub

Private Sub OptY_Click()
On Error Resume Next
        Btn_PreformPrint.Visible = True
        cmdPrint.Visible = False
        Btn_PreformPrint.Left = 10755
        Call ShowTemplate(TreeView1.SelectedItem.Text, "SHOWTEMPLATE")
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
        Btn_PreformPrint.Visible = True
        cmdPrint.Visible = False
        Btn_PreformPrint.Left = 10755
        Sis1.DoCommand Button.Key
        Sis1.SetFocus
End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)
        Btn_PreformPrint.Visible = True
        cmdPrint.Visible = False
        Btn_PreformPrint.Left = 10755
        Call ShowTemplate(Node.Text, "SHOWTEMPLATE")
End Sub

Private Sub ShowTemplate(PTemplate As String, STATE As String)
On Error GoTo ErrShow
Dim Scalar As Double
Dim Orientation As String
Dim j As Byte, CountLayer As Byte
            If OptY.Value Then
                        Orientation = OptY.Tag
            Else
                        Orientation = OptX.Tag
            End If
            CountLayer = Sis1.GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
            For j = 0 To CountLayer
                    Sis1.RemoveOverlay (j)
            Next
            Scalar = CDbl(TxtScale.Text)
            If UCase(STATE) = "SHOWTEMPLATE" Then  '# Show  Cadcorp Template on selected
                                        Sis1.Compose
                                        If InStr(1, PTemplate, "A0") > 0 Then
                                                PTemplate = "A0" & Orientation
                                        End If
                                        If InStr(1, PTemplate, "A1") > 0 Then
                                                PTemplate = "A1" & Orientation
                                        End If
                                        If InStr(1, PTemplate, "A2") > 0 Then
                                                PTemplate = "A2" & Orientation
                                        End If
                                        If InStr(1, PTemplate, "A3") > 0 Then
                                                PTemplate = "A3" & Orientation
                                        End If
                                        If InStr(1, PTemplate, "A4") > 0 Then
                                                PTemplate = "A4" & Orientation
                                        End If
                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
            End If
            
            If UCase(STATE) = "SHOWPROMPTPRINT" Then '# Show Crop Prompt Print
                Dim Xstart_cm As Double, Ystart_cm As Currency
'                                        With Frm_Map
'                                                    .Sis1.EmptyList "selectitem"
'                                                    .Sis1.CreatePropertyFilter "filter", "_id& = " & Frm_Map.tvwOverlay.Tag
'                                                    .Sis1.ScanOverlay "selectitem", 0, "filter", ""
'                                                    If .Sis1.GetListSize("selectitem") <> 0 Then
'                                                        .Sis1.SelectList "selectitem"
'                                    '                            DoEvents
'                                    '                    Sis1.DoCommand "AComZoomSelect"
'                                    '                    Sis1.SetFlt SIS_OT_WINDOW, 0, "_displayScale#", 500
'                                                        .Sis1.SetStr SIS_OT_CURITEM, 0, "_brush$", "Bule"
'                                                    End If
'                                        End With
                                        Frm_Map.Sis1.Compose
'                                        Frm_Map.Sis1.OpenSel 0
'                                        Frm_Map.Sis1.CreatePropertyFilter "Areas", "_id& = " & Frm_Map.Sis1.GetInt(SIS_OT_CURITEM, 0, "_id&")
'                                        Frm_Map.Sis1.SetStr SIS_OT_CURITEM, 0, "_brush$", "Blue"
'                                        Frm_Map.Sis1.UpdateItem
                                        
'                                        Frm_Map.Sis1.SetStr SIS_OT_CURITEM, 0, "_brush$", "Red"
                                         If InStr(1, PTemplate, "A4") > 0 Then   '# A4 Size Template
                                                        PTemplate = "A4" & Orientation
                                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "�ҵ����ǹ 1 : 4000 "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ҧ/�������� "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ͷ��"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "����ͤ�ͧ"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "saksit"
                                        End If
                                         If InStr(1, PTemplate, "A3") > 0 Then   '# A3 Size Template
                                                        PTemplate = "A3" & Orientation
                                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "�ҵ����ǹ 1 : 4000 "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ҧ/�������� "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ͷ��"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "����ͤ�ͧ"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "saksit"
                                        End If
                                         If InStr(1, PTemplate, "A2") > 0 Then   '# A4 Size Template
                                                        PTemplate = "A2" & Orientation
                                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "�ҵ����ǹ 1 : 4000 "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ҧ/�������� "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ͷ��"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "����ͤ�ͧ"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "saksit"
                                        End If
                                         If InStr(1, PTemplate, "A1") > 0 Then   '# A4 Size Template
                                                        PTemplate = "A1" & Orientation
                                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "�ҵ����ǹ 1 : 4000 "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ҧ/�������� "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ͷ��"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "����ͤ�ͧ"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "saksit"
                                        End If
                                         If InStr(1, PTemplate, "A0") > 0 Then   '# A4 Size Template
                                                        PTemplate = "A0" & Orientation
                                                        Sis1.PlacePrintTemplate PTemplate, 0, Scalar
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "�ҵ����ǹ 1 : 4000 "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ҧ/�������� "
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "���ͷ��"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "����ͤ�ͧ"
'                                                            Ystart_cm = Ystart_cm + 0.3
'                                                            Sis1.CreateBoxText Xstart_cm, Ystart_cm / 100, 0, 0.3 / 100, "saksit"
                                        End If
'                                        Sis1.PlacePrintTemplate PTemplate, 0, 5000
                            If Orientation = "L" Then  'Landscape
                                    Xstart_cm = 3 / 100
                                    Ystart_cm = 1
                            End If
                            If Orientation = "P" Then  'Potaint
                                    Xstart_cm = 3 / 100
                                    Ystart_cm = 1.3
                            End If
                End If
                Template_Temp = PTemplate
                Frm_PrintMap.Sis1.SplitExtent x1#, y1#, z1#, x2#, y2#, z2#, Sis1.GetViewExtent
        Exit Sub
ErrShow:
    MsgBox Err.Description
End Sub

Private Sub txtCopy_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub TxtScale_KeyPress(KeyAscii As Integer)
        Dim i As Byte
        Dim CountLayer As Byte
        On Error Resume Next
        If KeyAscii = 13 Then
                CountLayer = Sis1.GetInt(SIS_OT_WINDOW, 0, "_nOverlay&")
                For i = 0 To CountLayer
                    Sis1.RemoveOverlay (i)
                Next
                Sis1.PlacePrintTemplate Template_Temp, 0, CDbl(TxtScale.Text)
        End If
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub
