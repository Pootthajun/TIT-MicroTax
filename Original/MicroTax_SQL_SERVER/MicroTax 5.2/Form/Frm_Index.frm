VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Frm_Index 
   ClientHeight    =   9660
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12930
   ControlBox      =   0   'False
   Icon            =   "Frm_Index.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   Picture         =   "Frm_Index.frx":151A
   ScaleHeight     =   9660
   ScaleWidth      =   12930
   WindowState     =   2  'Maximized
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":1921E
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AB7549&
      Height          =   405
      Index           =   10
      Left            =   10320
      Picture         =   "Frm_Index.frx":1BF48
      Style           =   1  'Graphical
      TabIndex        =   313
      TabStop         =   0   'False
      Top             =   1320
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":1E97D
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AB7549&
      Height          =   405
      Index           =   9
      Left            =   10320
      Picture         =   "Frm_Index.frx":21BEE
      Style           =   1  'Graphical
      TabIndex        =   312
      TabStop         =   0   'False
      Top             =   930
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":24AAF
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AB7549&
      Height          =   405
      Index           =   8
      Left            =   7740
      Picture         =   "Frm_Index.frx":25425
      Style           =   1  'Graphical
      TabIndex        =   206
      TabStop         =   0   'False
      Top             =   1320
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":2977B
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   6
      Left            =   5160
      Picture         =   "Frm_Index.frx":2A0D0
      Style           =   1  'Graphical
      TabIndex        =   169
      TabStop         =   0   'False
      Top             =   1320
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":2A9D7
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   2
      Left            =   2580
      Picture         =   "Frm_Index.frx":2B48B
      Style           =   1  'Graphical
      TabIndex        =   94
      TabStop         =   0   'False
      Top             =   1320
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":2BEFD
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   5
      Left            =   0
      Picture         =   "Frm_Index.frx":2C942
      Style           =   1  'Graphical
      TabIndex        =   93
      TabStop         =   0   'False
      Top             =   1320
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6D6D6&
      DownPicture     =   "Frm_Index.frx":2D343
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   7
      Left            =   7740
      Picture         =   "Frm_Index.frx":2DD28
      Style           =   1  'Graphical
      TabIndex        =   191
      TabStop         =   0   'False
      Top             =   930
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Index.frx":2E6B9
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   4
      Left            =   5160
      Picture         =   "Frm_Index.frx":2F17E
      Style           =   1  'Graphical
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   930
      Width           =   2592
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Index.frx":2FC00
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   3
      Left            =   2580
      Picture         =   "Frm_Index.frx":304C9
      Style           =   1  'Graphical
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   930
      Width           =   2592
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Index.frx":30D39
      Height          =   405
      Left            =   4920
      Picture         =   "Frm_Index.frx":315A2
      Style           =   1  'Graphical
      TabIndex        =   60
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Index.frx":33D9A
      Height          =   405
      Left            =   7200
      Picture         =   "Frm_Index.frx":345C8
      Style           =   1  'Graphical
      TabIndex        =   67
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Index.frx":36E52
      Height          =   405
      Left            =   11760
      Picture         =   "Frm_Index.frx":37741
      Style           =   1  'Graphical
      TabIndex        =   70
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Index.frx":3A29C
      Enabled         =   0   'False
      Height          =   405
      Left            =   9480
      Picture         =   "Frm_Index.frx":3AB4E
      Style           =   1  'Graphical
      TabIndex        =   69
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Index.frx":3D589
      Enabled         =   0   'False
      Height          =   405
      Left            =   8340
      Picture         =   "Frm_Index.frx":3DDEC
      Style           =   1  'Graphical
      TabIndex        =   68
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      DisabledPicture =   "Frm_Index.frx":40811
      Height          =   405
      Left            =   6060
      Picture         =   "Frm_Index.frx":410A8
      Style           =   1  'Graphical
      TabIndex        =   66
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Frm_Index.frx":4398D
      Height          =   405
      Left            =   10620
      Picture         =   "Frm_Index.frx":441F4
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7725
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1710
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   13626
      _Version        =   393216
      Style           =   1
      Tabs            =   10
      TabsPerRow      =   10
      TabHeight       =   529
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "Frm_Index.frx":46AEB
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Image1(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Shape3(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Shape3(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Shape1(1)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label2(2)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label2(1)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Shape1(5)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Lb_Tambon_Id2"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Shape1(6)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Lb_Village_Id"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Shape2(0)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label2(0)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label2(3)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Shape1(0)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Shape1(3)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Lb_Amphoe_Id"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Lb_Tambon_Id"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Label2(25)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Label2(26)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Label2(180)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Shape2(1)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Grid_Tambon"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Grid_VILLAGE"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Tx_Village_Name"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Op_Tambon"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Tx_Tambon_Name"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Op_Village"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Tx_Zipcode"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).ControlCount=   28
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "Frm_Index.frx":46B07
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Image1(2)"
      Tab(1).Control(1)=   "Shape3(7)"
      Tab(1).Control(2)=   "Shape3(3)"
      Tab(1).Control(3)=   "Label2(4)"
      Tab(1).Control(4)=   "Label2(5)"
      Tab(1).Control(5)=   "Label2(7)"
      Tab(1).Control(6)=   "Shape1(11)"
      Tab(1).Control(7)=   "Label2(10)"
      Tab(1).Control(8)=   "Label2(11)"
      Tab(1).Control(9)=   "Label2(18)"
      Tab(1).Control(10)=   "LB_STREET_ID"
      Tab(1).Control(11)=   "Shape1(7)"
      Tab(1).Control(12)=   "Shape1(8)"
      Tab(1).Control(13)=   "LB_SOI_ID"
      Tab(1).Control(14)=   "Shape1(23)"
      Tab(1).Control(15)=   "Grid_STREET"
      Tab(1).Control(16)=   "Txt_Street_Name"
      Tab(1).Control(17)=   "Grid_SOI"
      Tab(1).Control(18)=   "Chk_Street"
      Tab(1).Control(19)=   "Op_Soi"
      Tab(1).Control(20)=   "Txt_SOI"
      Tab(1).ControlCount=   21
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "Frm_Index.frx":46B23
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Image1(3)"
      Tab(2).Control(1)=   "Shape3(5)"
      Tab(2).Control(2)=   "Shape3(4)"
      Tab(2).Control(3)=   "Shape1(4)"
      Tab(2).Control(4)=   "Label2(20)"
      Tab(2).Control(5)=   "Label2(21)"
      Tab(2).Control(6)=   "Label2(22)"
      Tab(2).Control(7)=   "Label2(23)"
      Tab(2).Control(8)=   "Shape1(17)"
      Tab(2).Control(9)=   "Label2(24)"
      Tab(2).Control(10)=   "Grid_Zone"
      Tab(2).Control(11)=   "Txt_Zone"
      Tab(2).Control(12)=   "Op_BLOCK"
      Tab(2).Control(13)=   "Op_ZONE"
      Tab(2).Control(14)=   "Cmb_Zone"
      Tab(2).Control(15)=   "TXT_BLOCK"
      Tab(2).Control(16)=   "Chk_Zone"
      Tab(2).ControlCount=   17
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "Frm_Index.frx":46B3F
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Chk_Lone_Baht(0)"
      Tab(3).Control(1)=   "Chk_Static_Baht(0)"
      Tab(3).Control(2)=   "Chk_Lone_Percent(0)"
      Tab(3).Control(3)=   "Chk_Static_Percent(0)"
      Tab(3).Control(4)=   "Txt_Rate_LandLone(0)"
      Tab(3).Control(5)=   "Txt_Rate_LandUse(0)"
      Tab(3).Control(6)=   "Combo3"
      Tab(3).Control(7)=   "Combo1"
      Tab(3).Control(8)=   "Txt_Rate_LandUse(5)"
      Tab(3).Control(9)=   "Txt_Rate_LandLone(5)"
      Tab(3).Control(10)=   "Chk_Static_Percent(5)"
      Tab(3).Control(11)=   "Chk_Lone_Percent(5)"
      Tab(3).Control(12)=   "Chk_Static_Baht(5)"
      Tab(3).Control(13)=   "Chk_Lone_Baht(5)"
      Tab(3).Control(14)=   "List1"
      Tab(3).Control(15)=   "Txt_LM_Details"
      Tab(3).Control(16)=   "Op_LM"
      Tab(3).Control(17)=   "Chk_Lone_Baht(4)"
      Tab(3).Control(18)=   "Chk_Static_Baht(4)"
      Tab(3).Control(19)=   "Chk_Lone_Baht(3)"
      Tab(3).Control(20)=   "Chk_Static_Baht(3)"
      Tab(3).Control(21)=   "Chk_Lone_Baht(2)"
      Tab(3).Control(22)=   "Chk_Static_Baht(2)"
      Tab(3).Control(23)=   "Chk_Lone_Baht(1)"
      Tab(3).Control(24)=   "Chk_Static_Baht(1)"
      Tab(3).Control(25)=   "Chk_Lone_Percent(4)"
      Tab(3).Control(26)=   "Chk_Static_Percent(4)"
      Tab(3).Control(27)=   "Chk_Lone_Percent(3)"
      Tab(3).Control(28)=   "Chk_Static_Percent(3)"
      Tab(3).Control(29)=   "Chk_Lone_Percent(2)"
      Tab(3).Control(30)=   "Chk_Static_Percent(2)"
      Tab(3).Control(31)=   "Chk_Lone_Percent(1)"
      Tab(3).Control(32)=   "Chk_Static_Percent(1)"
      Tab(3).Control(33)=   "Txt_Rate_LandLone(4)"
      Tab(3).Control(34)=   "Txt_Rate_LandLone(3)"
      Tab(3).Control(35)=   "Txt_Rate_LandLone(1)"
      Tab(3).Control(36)=   "Txt_Rate_LandUse(4)"
      Tab(3).Control(37)=   "Txt_Rate_LandUse(3)"
      Tab(3).Control(38)=   "Txt_Rate_LandLone(2)"
      Tab(3).Control(39)=   "Txt_Rate_LandUse(2)"
      Tab(3).Control(40)=   "Txt_Rate_LandUse(1)"
      Tab(3).Control(41)=   "Op_Land_Use"
      Tab(3).Control(42)=   "Op_RateTax"
      Tab(3).Control(43)=   "Txt_Zone_Rate"
      Tab(3).Control(44)=   "Txt_Zone_Price"
      Tab(3).Control(45)=   "combo2"
      Tab(3).Control(46)=   "GRID_ZONETAX"
      Tab(3).Control(47)=   "Label2(179)"
      Tab(3).Control(48)=   "Label2(178)"
      Tab(3).Control(49)=   "Label2(177)"
      Tab(3).Control(50)=   "Label2(176)"
      Tab(3).Control(51)=   "Shape1(82)"
      Tab(3).Control(52)=   "Shape1(81)"
      Tab(3).Control(53)=   "Label2(175)"
      Tab(3).Control(54)=   "Label2(174)"
      Tab(3).Control(55)=   "Label2(173)"
      Tab(3).Control(56)=   "Label2(171)"
      Tab(3).Control(57)=   "Label2(119)"
      Tab(3).Control(58)=   "Label2(118)"
      Tab(3).Control(59)=   "Shape1(51)"
      Tab(3).Control(60)=   "Label2(117)"
      Tab(3).Control(61)=   "Shape1(50)"
      Tab(3).Control(62)=   "Label2(115)"
      Tab(3).Control(63)=   "Label2(114)"
      Tab(3).Control(64)=   "Label2(113)"
      Tab(3).Control(65)=   "Label2(112)"
      Tab(3).Control(66)=   "Shape1(49)"
      Tab(3).Control(67)=   "Label2(106)"
      Tab(3).Control(68)=   "Shape3(16)"
      Tab(3).Control(69)=   "Label2(58)"
      Tab(3).Control(70)=   "Label2(57)"
      Tab(3).Control(71)=   "Label2(56)"
      Tab(3).Control(72)=   "Label2(55)"
      Tab(3).Control(73)=   "Label2(54)"
      Tab(3).Control(74)=   "Label2(53)"
      Tab(3).Control(75)=   "Label2(52)"
      Tab(3).Control(76)=   "Label2(51)"
      Tab(3).Control(77)=   "Label2(50)"
      Tab(3).Control(78)=   "Label2(49)"
      Tab(3).Control(79)=   "Label2(48)"
      Tab(3).Control(80)=   "Label2(47)"
      Tab(3).Control(81)=   "Label2(46)"
      Tab(3).Control(82)=   "Label2(45)"
      Tab(3).Control(83)=   "Label2(44)"
      Tab(3).Control(84)=   "Label2(43)"
      Tab(3).Control(85)=   "Label2(42)"
      Tab(3).Control(86)=   "Label2(41)"
      Tab(3).Control(87)=   "Label2(33)"
      Tab(3).Control(88)=   "Label2(15)"
      Tab(3).Control(89)=   "Shape1(22)"
      Tab(3).Control(90)=   "Label2(40)"
      Tab(3).Control(91)=   "Shape1(16)"
      Tab(3).Control(92)=   "Label2(39)"
      Tab(3).Control(93)=   "Label2(38)"
      Tab(3).Control(94)=   "Label2(37)"
      Tab(3).Control(95)=   "Shape1(15)"
      Tab(3).Control(96)=   "Shape1(14)"
      Tab(3).Control(97)=   "Shape1(13)"
      Tab(3).Control(98)=   "Shape1(12)"
      Tab(3).Control(99)=   "Label2(32)"
      Tab(3).Control(100)=   "Shape1(10)"
      Tab(3).Control(101)=   "Shape1(9)"
      Tab(3).Control(102)=   "Label2(19)"
      Tab(3).Control(103)=   "Label2(17)"
      Tab(3).Control(104)=   "Label2(16)"
      Tab(3).Control(105)=   "Label2(14)"
      Tab(3).Control(106)=   "Label2(13)"
      Tab(3).Control(107)=   "Label2(12)"
      Tab(3).Control(108)=   "Label2(9)"
      Tab(3).Control(109)=   "Label2(8)"
      Tab(3).Control(110)=   "Label2(6)"
      Tab(3).Control(111)=   "Label2(31)"
      Tab(3).Control(112)=   "Label2(30)"
      Tab(3).Control(113)=   "Label2(29)"
      Tab(3).Control(114)=   "Shape1(19)"
      Tab(3).Control(115)=   "Label2(28)"
      Tab(3).Control(116)=   "Label2(27)"
      Tab(3).Control(117)=   "Shape1(18)"
      Tab(3).Control(118)=   "Shape3(6)"
      Tab(3).Control(119)=   "Shape3(8)"
      Tab(3).Control(120)=   "Image1(4)"
      Tab(3).ControlCount=   121
      TabCaption(4)   =   "Tab 4"
      TabPicture(4)   =   "Frm_Index.frx":46B5B
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Image1(6)"
      Tab(4).Control(1)=   "Label2(34)"
      Tab(4).Control(2)=   "Label2(35)"
      Tab(4).Control(3)=   "Shape1(20)"
      Tab(4).Control(4)=   "Label2(36)"
      Tab(4).Control(5)=   "Shape1(21)"
      Tab(4).Control(6)=   "Label2(60)"
      Tab(4).Control(7)=   "Label2(61)"
      Tab(4).Control(8)=   "Label2(62)"
      Tab(4).Control(9)=   "Label2(63)"
      Tab(4).Control(10)=   "Label2(64)"
      Tab(4).Control(11)=   "Label2(65)"
      Tab(4).Control(12)=   "Label2(66)"
      Tab(4).Control(13)=   "Label2(67)"
      Tab(4).Control(14)=   "Label2(68)"
      Tab(4).Control(15)=   "Label2(69)"
      Tab(4).Control(16)=   "Label2(70)"
      Tab(4).Control(17)=   "Label2(71)"
      Tab(4).Control(18)=   "Label2(72)"
      Tab(4).Control(19)=   "Shape1(24)"
      Tab(4).Control(20)=   "Shape1(25)"
      Tab(4).Control(21)=   "Shape1(26)"
      Tab(4).Control(22)=   "Shape1(27)"
      Tab(4).Control(23)=   "Shape1(28)"
      Tab(4).Control(24)=   "Shape1(29)"
      Tab(4).Control(25)=   "Label2(73)"
      Tab(4).Control(26)=   "Label2(74)"
      Tab(4).Control(27)=   "Label2(75)"
      Tab(4).Control(28)=   "Label2(76)"
      Tab(4).Control(29)=   "Shape1(30)"
      Tab(4).Control(30)=   "Shape1(31)"
      Tab(4).Control(31)=   "Shape1(32)"
      Tab(4).Control(32)=   "Shape1(33)"
      Tab(4).Control(33)=   "Label2(77)"
      Tab(4).Control(34)=   "Label2(78)"
      Tab(4).Control(35)=   "Label2(79)"
      Tab(4).Control(36)=   "Label2(80)"
      Tab(4).Control(37)=   "TXT_SIGNBORD_RATE_OF_USE(1)"
      Tab(4).Control(38)=   "TXT_SIGNBORD_RATE(1)"
      Tab(4).Control(39)=   "TXT_SIGNBORD_RATE(2)"
      Tab(4).Control(40)=   "TXT_SIGNBORD_RATE(3)"
      Tab(4).Control(41)=   "TXT_SIGNBORD_RATE(4)"
      Tab(4).Control(42)=   "TXT_SIGNBORD_RATE_OF_USE(2)"
      Tab(4).Control(43)=   "TXT_SIGNBORD_RATE_OF_USE(3)"
      Tab(4).Control(44)=   "TXT_SIGNBORD_RATE_OF_USE(4)"
      Tab(4).Control(45)=   "TXT_SIGNBORD_LOW(1)"
      Tab(4).Control(46)=   "TXT_SIGNBORD_LOW(2)"
      Tab(4).Control(47)=   "TXT_SIGNBORD_LOW(3)"
      Tab(4).Control(48)=   "TXT_SIGNBORD_LOW(4)"
      Tab(4).ControlCount=   49
      TabCaption(5)   =   "Tab 5"
      TabPicture(5)   =   "Frm_Index.frx":46B77
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Image1(5)"
      Tab(5).Control(1)=   "Shape3(9)"
      Tab(5).Control(2)=   "Shape3(10)"
      Tab(5).Control(3)=   "Label2(59)"
      Tab(5).Control(4)=   "Shape1(34)"
      Tab(5).Control(5)=   "Lb_Business"
      Tab(5).Control(6)=   "Shape1(35)"
      Tab(5).Control(7)=   "Label2(81)"
      Tab(5).Control(8)=   "Label2(82)"
      Tab(5).Control(9)=   "Label2(83)"
      Tab(5).Control(10)=   "Shape1(36)"
      Tab(5).Control(11)=   "Label2(85)"
      Tab(5).Control(12)=   "Shape1(37)"
      Tab(5).Control(13)=   "Label2(86)"
      Tab(5).Control(14)=   "Label2(87)"
      Tab(5).Control(15)=   "Label2(88)"
      Tab(5).Control(16)=   "Shape1(39)"
      Tab(5).Control(17)=   "lb_Class_Order"
      Tab(5).Control(18)=   "Grid_Business"
      Tab(5).Control(19)=   "Grid_Class"
      Tab(5).Control(20)=   "Op_Business"
      Tab(5).Control(21)=   "Txt_Business_Type"
      Tab(5).Control(22)=   "Op_Class"
      Tab(5).Control(23)=   "Txt_Class_Details"
      Tab(5).Control(24)=   "Txt_Class_Price"
      Tab(5).ControlCount=   25
      TabCaption(6)   =   "Tab 6"
      TabPicture(6)   =   "Frm_Index.frx":46B93
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Image1(7)"
      Tab(6).Control(1)=   "Lb_BuildingUse(3)"
      Tab(6).Control(2)=   "Lb_BuildingUse(4)"
      Tab(6).Control(3)=   "Lb_BuildingUse(2)"
      Tab(6).Control(4)=   "Shape3(12)"
      Tab(6).Control(5)=   "Shape3(11)"
      Tab(6).Control(6)=   "Label2(84)"
      Tab(6).Control(7)=   "Shape1(38)"
      Tab(6).Control(8)=   "Shape1(40)"
      Tab(6).Control(9)=   "Lb_BuildingType_ID"
      Tab(6).Control(10)=   "Shape1(41)"
      Tab(6).Control(11)=   "Label2(90)"
      Tab(6).Control(12)=   "Label2(91)"
      Tab(6).Control(13)=   "Label2(92)"
      Tab(6).Control(14)=   "Label2(93)"
      Tab(6).Control(15)=   "Label2(94)"
      Tab(6).Control(16)=   "Label2(95)"
      Tab(6).Control(17)=   "Label2(96)"
      Tab(6).Control(18)=   "Label2(97)"
      Tab(6).Control(19)=   "Label2(98)"
      Tab(6).Control(20)=   "Shape1(43)"
      Tab(6).Control(21)=   "Shape1(44)"
      Tab(6).Control(22)=   "Label2(99)"
      Tab(6).Control(23)=   "Label2(100)"
      Tab(6).Control(24)=   "Label2(101)"
      Tab(6).Control(25)=   "Shape1(45)"
      Tab(6).Control(26)=   "Label2(102)"
      Tab(6).Control(27)=   "Label2(103)"
      Tab(6).Control(28)=   "Shape1(46)"
      Tab(6).Control(29)=   "Label2(104)"
      Tab(6).Control(30)=   "Label2(105)"
      Tab(6).Control(31)=   "Label2(107)"
      Tab(6).Control(32)=   "Shape1(47)"
      Tab(6).Control(33)=   "Shape1(48)"
      Tab(6).Control(34)=   "Label2(108)"
      Tab(6).Control(35)=   "Label2(116)"
      Tab(6).Control(36)=   "Label2(120)"
      Tab(6).Control(37)=   "Shape1(53)"
      Tab(6).Control(38)=   "Label2(121)"
      Tab(6).Control(39)=   "Label2(166)"
      Tab(6).Control(40)=   "Lb_BuildingUse(0)"
      Tab(6).Control(41)=   "Label1"
      Tab(6).Control(42)=   "Label2(110)"
      Tab(6).Control(43)=   "Label2(168)"
      Tab(6).Control(44)=   "Shape1(42)"
      Tab(6).Control(45)=   "Shape1(80)"
      Tab(6).Control(46)=   "Grid_BuildingType"
      Tab(6).Control(47)=   "Op_BuildingType"
      Tab(6).Control(48)=   "Txt_TYPE_A_RATE1"
      Tab(6).Control(49)=   "Txt_BuildingType"
      Tab(6).Control(50)=   "Txt_TYPE_A_RATE4"
      Tab(6).Control(51)=   "Txt_TYPE_B_RATE1"
      Tab(6).Control(52)=   "Txt_TYPE_B_RATE2"
      Tab(6).Control(53)=   "Txt_TYPE_C_RATE1"
      Tab(6).Control(54)=   "Txt_TYPE_C_RATE2"
      Tab(6).Control(55)=   "Txt_TYPE_D_RATE1"
      Tab(6).Control(56)=   "Txt_TYPE_A_RATE3"
      Tab(6).Control(57)=   "Op_BuildingRate"
      Tab(6).Control(58)=   "Txt_TYPE_A_RATE2"
      Tab(6).Control(59)=   "Cmb_Tamlay"
      Tab(6).Control(60)=   "Txt_TYPE_A_AREA"
      Tab(6).ControlCount=   61
      TabCaption(7)   =   "Tab 7"
      TabPicture(7)   =   "Frm_Index.frx":46BAF
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "Image1(8)"
      Tab(7).Control(1)=   "Shape3(15)"
      Tab(7).Control(2)=   "Shape3(14)"
      Tab(7).Control(3)=   "Shape3(13)"
      Tab(7).Control(4)=   "Label2(111)"
      Tab(7).Control(5)=   "Label2(122)"
      Tab(7).Control(6)=   "Shape1(54)"
      Tab(7).Control(7)=   "Label2(123)"
      Tab(7).Control(8)=   "Label2(124)"
      Tab(7).Control(9)=   "Label2(125)"
      Tab(7).Control(10)=   "Label2(126)"
      Tab(7).Control(11)=   "Label2(127)"
      Tab(7).Control(12)=   "Shape1(55)"
      Tab(7).Control(13)=   "Shape1(56)"
      Tab(7).Control(14)=   "Shape1(57)"
      Tab(7).Control(15)=   "Shape1(58)"
      Tab(7).Control(16)=   "Label2(128)"
      Tab(7).Control(17)=   "Label2(129)"
      Tab(7).Control(18)=   "Label2(130)"
      Tab(7).Control(19)=   "Label2(131)"
      Tab(7).Control(20)=   "Label2(132)"
      Tab(7).Control(21)=   "Shape1(59)"
      Tab(7).Control(22)=   "Label2(133)"
      Tab(7).Control(23)=   "Label2(134)"
      Tab(7).Control(24)=   "Label2(135)"
      Tab(7).Control(25)=   "Shape1(60)"
      Tab(7).Control(26)=   "Label2(136)"
      Tab(7).Control(27)=   "Shape1(61)"
      Tab(7).Control(28)=   "Label2(137)"
      Tab(7).Control(29)=   "Label2(138)"
      Tab(7).Control(30)=   "Shape1(62)"
      Tab(7).Control(31)=   "Label2(139)"
      Tab(7).Control(32)=   "Label2(140)"
      Tab(7).Control(33)=   "Label2(141)"
      Tab(7).Control(34)=   "Label2(142)"
      Tab(7).Control(35)=   "Shape1(63)"
      Tab(7).Control(36)=   "Label2(143)"
      Tab(7).Control(37)=   "Label2(144)"
      Tab(7).Control(38)=   "Shape1(64)"
      Tab(7).Control(39)=   "Label2(145)"
      Tab(7).Control(40)=   "Shape1(65)"
      Tab(7).Control(41)=   "Label2(146)"
      Tab(7).Control(42)=   "Label2(147)"
      Tab(7).Control(43)=   "Shape1(66)"
      Tab(7).Control(44)=   "Label2(148)"
      Tab(7).Control(45)=   "Label2(149)"
      Tab(7).Control(46)=   "Label2(150)"
      Tab(7).Control(47)=   "Label2(151)"
      Tab(7).Control(48)=   "Label2(152)"
      Tab(7).Control(49)=   "Label2(153)"
      Tab(7).Control(50)=   "Label2(154)"
      Tab(7).Control(51)=   "Label2(155)"
      Tab(7).Control(52)=   "Label2(156)"
      Tab(7).Control(53)=   "Label2(157)"
      Tab(7).Control(54)=   "Label2(158)"
      Tab(7).Control(55)=   "Shape1(67)"
      Tab(7).Control(56)=   "Label2(159)"
      Tab(7).Control(57)=   "Label2(160)"
      Tab(7).Control(58)=   "Label2(161)"
      Tab(7).Control(59)=   "Shape1(68)"
      Tab(7).Control(60)=   "Shape1(69)"
      Tab(7).Control(61)=   "Shape1(70)"
      Tab(7).Control(62)=   "Shape1(71)"
      Tab(7).Control(63)=   "Shape1(72)"
      Tab(7).Control(64)=   "Label2(162)"
      Tab(7).Control(65)=   "Shape1(52)"
      Tab(7).Control(66)=   "Shape1(73)"
      Tab(7).Control(67)=   "Shape1(74)"
      Tab(7).Control(68)=   "Label2(163)"
      Tab(7).Control(69)=   "Label2(164)"
      Tab(7).Control(70)=   "Label2(165)"
      Tab(7).Control(71)=   "Op_Cfg_Building"
      Tab(7).Control(72)=   "Txt_Due_Accept0"
      Tab(7).Control(73)=   "Txt_Due_1Month"
      Tab(7).Control(74)=   "Txt_Due_2Month"
      Tab(7).Control(75)=   "Txt_Due_3Month"
      Tab(7).Control(76)=   "Txt_Due_4Month"
      Tab(7).Control(77)=   "Txt_Due_Due_Appeal0"
      Tab(7).Control(78)=   "Txt_Tax_YPerM"
      Tab(7).Control(79)=   "Tax_MPerD"
      Tab(7).Control(80)=   "Txt_Tax_Machine"
      Tab(7).Control(81)=   "Op_Cfg_Land"
      Tab(7).Control(82)=   "Txt_Due_Accept1"
      Tab(7).Control(83)=   "Txt_Tax_OutSchedule1"
      Tab(7).Control(84)=   "Txt_Tax_NotFully"
      Tab(7).Control(85)=   "Txt_Due_Due_Appeal1"
      Tab(7).Control(86)=   "Op_Cfg_Signbord"
      Tab(7).Control(87)=   "Txt_Tax_OutTime1"
      Tab(7).Control(88)=   "Txt_Due_Due_Appeal2"
      Tab(7).Control(89)=   "Txt_Tax_NotFully2"
      Tab(7).Control(90)=   "Txt_Tax_OutSchedule2"
      Tab(7).Control(91)=   "Txt_Due_Accept2"
      Tab(7).Control(92)=   "Txt_Tax_OutTime2"
      Tab(7).Control(93)=   "Txt_B_Rai"
      Tab(7).Control(94)=   "Txt_B_Pot"
      Tab(7).Control(95)=   "Txt_B_Va"
      Tab(7).ControlCount=   96
      TabCaption(8)   =   "Tab 8"
      TabPicture(8)   =   "Frm_Index.frx":46BCB
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "txt_change"
      Tab(8).Control(1)=   "cb_type_asset"
      Tab(8).Control(2)=   "Grid_change"
      Tab(8).Control(3)=   "Shape1(75)"
      Tab(8).Control(4)=   "Shape1(76)"
      Tab(8).Control(5)=   "lb_change_id"
      Tab(8).Control(6)=   "Label2(109)"
      Tab(8).Control(7)=   "Label2(167)"
      Tab(8).Control(8)=   "Label2(89)"
      Tab(8).Control(9)=   "Image1(0)"
      Tab(8).ControlCount=   10
      TabCaption(9)   =   "Tab 9"
      TabPicture(9)   =   "Frm_Index.frx":46BE7
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "txt_Position"
      Tab(9).Control(1)=   "Opt_Officer"
      Tab(9).Control(2)=   "txt_Request"
      Tab(9).Control(3)=   "Opt_Building_Type"
      Tab(9).Control(4)=   "Opt_Request"
      Tab(9).Control(5)=   "Grid_Request"
      Tab(9).Control(6)=   "Shape1(79)"
      Tab(9).Control(7)=   "Label2(172)"
      Tab(9).Control(8)=   "Shape3(19)"
      Tab(9).Control(9)=   "Shape1(77)"
      Tab(9).Control(10)=   "lb_Request_id"
      Tab(9).Control(11)=   "Label2(169)"
      Tab(9).Control(12)=   "Label2(170)"
      Tab(9).Control(13)=   "Shape1(78)"
      Tab(9).Control(14)=   "Shape3(17)"
      Tab(9).Control(15)=   "Shape3(18)"
      Tab(9).Control(16)=   "Image1(9)"
      Tab(9).ControlCount=   17
      Begin VB.TextBox Tx_Zipcode 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   1290
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   2
         Top             =   1830
         Width           =   2415
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   0
         Left            =   -64410
         TabIndex        =   346
         Top             =   1770
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   0
         Left            =   -64410
         TabIndex        =   345
         Top             =   1350
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   0
         Left            =   -66330
         TabIndex        =   342
         Top             =   1770
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Percent 
         BackColor       =   &H00E3DCD7&
         Enabled         =   0   'False
         Height          =   195
         Index           =   0
         Left            =   -66330
         TabIndex        =   341
         Top             =   1350
         Width           =   195
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   340
         Text            =   "0"
         Top             =   1710
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   339
         Text            =   "0"
         Top             =   1290
         Width           =   975
      End
      Begin VB.CheckBox Chk_Zone 
         Height          =   195
         Left            =   -67380
         TabIndex        =   334
         Top             =   1260
         Value           =   1  'Checked
         Width           =   195
      End
      Begin VB.TextBox Txt_TYPE_A_AREA 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69660
         Locked          =   -1  'True
         TabIndex        =   331
         Text            =   "0.00"
         Top             =   4020
         Width           =   1035
      End
      Begin VB.TextBox txt_Position 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -66540
         TabIndex        =   330
         Top             =   1590
         Visible         =   0   'False
         Width           =   4305
      End
      Begin VB.OptionButton Opt_Officer 
         BackColor       =   &H00D6D6D6&
         Caption         =   "��ª������˹�ҷ��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Left            =   -68700
         TabIndex        =   328
         Top             =   870
         Width           =   1710
      End
      Begin VB.TextBox txt_change 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -72150
         Locked          =   -1  'True
         TabIndex        =   314
         Top             =   1350
         Width           =   9855
      End
      Begin VB.TextBox txt_Request 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -72030
         Locked          =   -1  'True
         TabIndex        =   323
         Top             =   1590
         Width           =   4515
      End
      Begin VB.OptionButton Opt_Building_Type 
         BackColor       =   &H00D6D6D6&
         Caption         =   "�������Ҥ��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Left            =   -71670
         TabIndex        =   322
         Top             =   870
         Width           =   1980
      End
      Begin VB.OptionButton Opt_Request 
         BackColor       =   &H00D6D6D6&
         Caption         =   "��¡���駤���ͧ�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Left            =   -74640
         TabIndex        =   321
         Top             =   870
         Value           =   -1  'True
         Width           =   2430
      End
      Begin VB.ComboBox cb_type_asset 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Index.frx":46C03
         Left            =   -69240
         List            =   "Frm_Index.frx":46C13
         Style           =   2  'Dropdown List
         TabIndex        =   316
         Top             =   660
         Width           =   2445
      End
      Begin VB.ComboBox Combo3 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         Left            =   -71220
         Style           =   2  'Dropdown List
         TabIndex        =   310
         Top             =   6480
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.ComboBox Cmb_Tamlay 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Index.frx":46C63
         Left            =   -74190
         List            =   "Frm_Index.frx":46C97
         Style           =   2  'Dropdown List
         TabIndex        =   307
         Top             =   1170
         Width           =   1755
      End
      Begin VB.TextBox Txt_B_Va 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64710
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   302
         Text            =   "0"
         Top             =   4140
         Width           =   555
      End
      Begin VB.TextBox Txt_B_Pot 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -65760
         Locked          =   -1  'True
         MaxLength       =   1
         TabIndex        =   301
         Text            =   "0"
         Top             =   4140
         Width           =   555
      End
      Begin VB.TextBox Txt_B_Rai 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -66690
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   300
         Text            =   "0"
         Top             =   4140
         Width           =   555
      End
      Begin VB.ComboBox Combo1 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         Left            =   -70200
         Style           =   2  'Dropdown List
         TabIndex        =   298
         Top             =   6270
         Width           =   7575
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   5
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   290
         Text            =   "0"
         Top             =   6780
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   5
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   289
         Text            =   "0"
         Top             =   7200
         Width           =   975
      End
      Begin VB.CheckBox Chk_Static_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   5
         Left            =   -66330
         TabIndex        =   288
         Top             =   6840
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   5
         Left            =   -66330
         TabIndex        =   287
         Top             =   7260
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   5
         Left            =   -64410
         TabIndex        =   286
         Top             =   6840
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   5
         Left            =   -64410
         TabIndex        =   285
         Top             =   7260
         Width           =   195
      End
      Begin VB.ListBox List1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1980
         ItemData        =   "Frm_Index.frx":46CDB
         Left            =   -74970
         List            =   "Frm_Index.frx":46CDD
         TabIndex        =   284
         Top             =   5520
         Width           =   3705
      End
      Begin VB.TextBox Txt_LM_Details 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -74940
         Locked          =   -1  'True
         TabIndex        =   283
         Top             =   5190
         Width           =   3645
      End
      Begin VB.OptionButton Op_LM 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74880
         TabIndex        =   281
         Top             =   4830
         Width           =   210
      End
      Begin VB.TextBox Txt_Tax_OutTime2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   64
         Text            =   "0"
         Top             =   7140
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_Accept2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   61
         Text            =   "0"
         Top             =   5925
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_OutSchedule2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   62
         Text            =   "0"
         Top             =   6330
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_NotFully2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   63
         Text            =   "0"
         Top             =   6735
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_Due_Appeal2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64695
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   65
         Text            =   "0"
         Top             =   5925
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_OutTime1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   58
         Text            =   "0"
         Top             =   4935
         Width           =   540
      End
      Begin VB.OptionButton Op_Cfg_Signbord 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -69825
         TabIndex        =   267
         Top             =   5490
         Width           =   210
      End
      Begin VB.TextBox Txt_Due_Due_Appeal1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64695
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   59
         Text            =   "0"
         Top             =   3720
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_NotFully 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   57
         Text            =   "0"
         Top             =   4530
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_OutSchedule1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   56
         Text            =   "0"
         Top             =   4080
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_Accept1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   55
         Text            =   "0"
         Top             =   3675
         Width           =   540
      End
      Begin VB.OptionButton Op_Cfg_Land 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -69825
         TabIndex        =   257
         Top             =   3240
         Width           =   210
      End
      Begin VB.TextBox Txt_Tax_Machine 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64665
         Locked          =   -1  'True
         TabIndex        =   54
         Text            =   "0"
         Top             =   2640
         Width           =   540
      End
      Begin VB.TextBox Tax_MPerD 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64665
         Locked          =   -1  'True
         TabIndex        =   53
         Text            =   "0"
         Top             =   1830
         Width           =   540
      End
      Begin VB.TextBox Txt_Tax_YPerM 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64665
         Locked          =   -1  'True
         TabIndex        =   52
         Text            =   "0"
         Top             =   1425
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_Due_Appeal0 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64665
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   51
         Text            =   "0"
         Top             =   1020
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_4Month 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   50
         Text            =   "0"
         Top             =   2640
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_3Month 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   49
         Text            =   "0"
         Top             =   2235
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_2Month 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   48
         Text            =   "0"
         Top             =   1830
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_1Month 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         TabIndex        =   47
         Text            =   "0"
         Top             =   1425
         Width           =   540
      End
      Begin VB.TextBox Txt_Due_Accept0 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69840
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   46
         Text            =   "0"
         Top             =   1020
         Width           =   540
      End
      Begin VB.OptionButton Op_Cfg_Building 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -69825
         TabIndex        =   237
         Top             =   585
         Width           =   210
      End
      Begin VB.TextBox Txt_TYPE_A_RATE2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69660
         Locked          =   -1  'True
         TabIndex        =   32
         Text            =   "0.00"
         Top             =   2640
         Width           =   1035
      End
      Begin VB.OptionButton Op_BuildingRate 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -71580
         TabIndex        =   233
         Top             =   720
         Width           =   210
      End
      Begin VB.TextBox Txt_TYPE_A_RATE3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69660
         Locked          =   -1  'True
         TabIndex        =   33
         Text            =   "0.00"
         Top             =   3105
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_D_RATE1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64800
         Locked          =   -1  'True
         TabIndex        =   39
         Text            =   "0.00"
         Top             =   5850
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_C_RATE2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64725
         Locked          =   -1  'True
         TabIndex        =   38
         Text            =   "0.00"
         Top             =   3285
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_C_RATE1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -64725
         Locked          =   -1  'True
         TabIndex        =   37
         Text            =   "0.00"
         Top             =   2820
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_B_RATE2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69645
         Locked          =   -1  'True
         TabIndex        =   36
         Text            =   "0.00"
         Top             =   6120
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_B_RATE1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69645
         Locked          =   -1  'True
         TabIndex        =   35
         Text            =   "0.00"
         Top             =   5655
         Width           =   1035
      End
      Begin VB.TextBox Txt_TYPE_A_RATE4 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69660
         Locked          =   -1  'True
         TabIndex        =   34
         Text            =   "0.00"
         Top             =   3570
         Width           =   1035
      End
      Begin VB.TextBox Txt_BuildingType 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -74160
         Locked          =   -1  'True
         TabIndex        =   213
         Top             =   1890
         Width           =   2415
      End
      Begin VB.TextBox Txt_TYPE_A_RATE1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -69660
         Locked          =   -1  'True
         TabIndex        =   31
         Text            =   "0.00"
         Top             =   2175
         Width           =   1035
      End
      Begin VB.OptionButton Op_BuildingType 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74880
         TabIndex        =   207
         Top             =   720
         Width           =   210
      End
      Begin VB.TextBox Txt_Class_Price 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -72060
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   41
         Text            =   "0.00"
         Top             =   4650
         Width           =   1635
      End
      Begin VB.TextBox Txt_Class_Details 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -72060
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   4170
         Width           =   9735
      End
      Begin VB.OptionButton Op_Class 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74850
         TabIndex        =   199
         Top             =   3660
         Width           =   210
      End
      Begin VB.TextBox Txt_Business_Type 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -72270
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   1260
         Width           =   10005
      End
      Begin VB.OptionButton Op_Business 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74880
         TabIndex        =   193
         Top             =   720
         Width           =   210
      End
      Begin VB.TextBox TXT_SIGNBORD_LOW 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   4
         Left            =   -65280
         Locked          =   -1  'True
         TabIndex        =   29
         Text            =   "0.00"
         Top             =   5130
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_LOW 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   3
         Left            =   -65250
         Locked          =   -1  'True
         TabIndex        =   26
         Text            =   "0.00"
         Top             =   3930
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_LOW 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   2
         Left            =   -65250
         Locked          =   -1  'True
         TabIndex        =   23
         Text            =   "0.00"
         Top             =   2850
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_LOW 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   1
         Left            =   -65250
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   20
         Text            =   "0.00"
         Top             =   1620
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE_OF_USE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   4
         Left            =   -69060
         Locked          =   -1  'True
         TabIndex        =   28
         Text            =   "0"
         Top             =   5130
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE_OF_USE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   3
         Left            =   -69060
         Locked          =   -1  'True
         TabIndex        =   25
         Text            =   "0"
         Top             =   3930
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE_OF_USE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   2
         Left            =   -69060
         Locked          =   -1  'True
         TabIndex        =   22
         Text            =   "0"
         Top             =   2850
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   4
         Left            =   -71490
         Locked          =   -1  'True
         TabIndex        =   27
         Text            =   "0.00"
         Top             =   5130
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   3
         Left            =   -71520
         Locked          =   -1  'True
         TabIndex        =   24
         Text            =   "0.00"
         Top             =   3930
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   2
         Left            =   -71490
         Locked          =   -1  'True
         TabIndex        =   21
         Text            =   "0.00"
         Top             =   2850
         Width           =   1095
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   -64410
         TabIndex        =   156
         Top             =   5670
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   -64410
         TabIndex        =   155
         Top             =   5250
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   3
         Left            =   -64410
         TabIndex        =   154
         Top             =   4680
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   3
         Left            =   -64410
         TabIndex        =   153
         Top             =   4290
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   2
         Left            =   -64410
         TabIndex        =   152
         Top             =   3660
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   2
         Left            =   -64410
         TabIndex        =   151
         Top             =   3240
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   -64410
         TabIndex        =   150
         Top             =   2700
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Baht 
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   -64410
         TabIndex        =   141
         Top             =   2280
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   -66330
         TabIndex        =   140
         Top             =   5670
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   -66330
         TabIndex        =   139
         Top             =   5250
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   3
         Left            =   -66330
         TabIndex        =   138
         Top             =   4680
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   3
         Left            =   -66330
         TabIndex        =   137
         Top             =   4290
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   2
         Left            =   -66330
         TabIndex        =   136
         Top             =   3660
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   2
         Left            =   -66330
         TabIndex        =   135
         Top             =   3240
         Width           =   195
      End
      Begin VB.CheckBox Chk_Lone_Percent 
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   -66330
         TabIndex        =   134
         Top             =   2700
         Width           =   195
      End
      Begin VB.CheckBox Chk_Static_Percent 
         BackColor       =   &H00E3DCD7&
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   -66330
         TabIndex        =   133
         Top             =   2280
         Width           =   195
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   4
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   17
         Text            =   "0"
         Top             =   5610
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   3
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   15
         Text            =   "0"
         Top             =   4650
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   1
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   11
         Text            =   "0"
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   4
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   16
         Text            =   "0"
         Top             =   5190
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   3
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   14
         Text            =   "0"
         Top             =   4230
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandLone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   2
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   13
         Text            =   "0"
         Top             =   3600
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   2
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   12
         Text            =   "0"
         Top             =   3180
         Width           =   975
      End
      Begin VB.TextBox Txt_Rate_LandUse 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   1
         Left            =   -67530
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   10
         Text            =   "0"
         Top             =   2220
         Width           =   975
      End
      Begin VB.OptionButton Op_Land_Use 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -71010
         TabIndex        =   118
         Top             =   660
         Width           =   210
      End
      Begin VB.OptionButton Op_RateTax 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74880
         TabIndex        =   116
         Top             =   660
         Width           =   210
      End
      Begin VB.TextBox Txt_SOI 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -70020
         Locked          =   -1  'True
         TabIndex        =   5
         Text            =   "-"
         Top             =   1590
         Width           =   3645
      End
      Begin VB.OptionButton Op_Soi 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -71610
         TabIndex        =   112
         Top             =   720
         Width           =   210
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   1
         Left            =   -71490
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   18
         Text            =   "0.00"
         Top             =   1620
         Width           =   1095
      End
      Begin VB.TextBox TXT_SIGNBORD_RATE_OF_USE 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   1
         Left            =   -69090
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   19
         Text            =   "0"
         Top             =   1620
         Width           =   1095
      End
      Begin VB.TextBox Txt_Zone_Rate 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -73770
         Locked          =   -1  'True
         TabIndex        =   9
         Text            =   "0.00"
         Top             =   1860
         Width           =   1425
      End
      Begin VB.TextBox Txt_Zone_Price 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   270
         Left            =   -73770
         Locked          =   -1  'True
         TabIndex        =   8
         Text            =   "0.00"
         Top             =   1560
         Width           =   1425
      End
      Begin VB.ComboBox combo2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Index.frx":46CDF
         Left            =   -73800
         List            =   "Frm_Index.frx":46D13
         Style           =   2  'Dropdown List
         TabIndex        =   103
         Top             =   1080
         Width           =   1485
      End
      Begin VB.OptionButton Op_Village 
         BackColor       =   &H00D6D6D6&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4170
         TabIndex        =   101
         Top             =   720
         Width           =   195
      End
      Begin VB.TextBox Tx_Tambon_Name 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   1290
         Locked          =   -1  'True
         TabIndex        =   1
         Text            =   "-"
         Top             =   1530
         Width           =   2415
      End
      Begin VB.OptionButton Op_Tambon 
         BackColor       =   &H00D6D6D6&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   95
         Top             =   720
         Width           =   195
      End
      Begin VB.TextBox TXT_BLOCK 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -69000
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   7
         Text            =   "0"
         Top             =   2160
         Width           =   1245
      End
      Begin VB.ComboBox Cmb_Zone 
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Frm_Index.frx":46D57
         Left            =   -69030
         List            =   "Frm_Index.frx":46D59
         Style           =   2  'Dropdown List
         TabIndex        =   88
         Top             =   1140
         Width           =   1305
      End
      Begin VB.OptionButton Op_ZONE 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -71700
         TabIndex        =   86
         Top             =   720
         Width           =   210
      End
      Begin VB.OptionButton Op_BLOCK 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -71700
         TabIndex        =   85
         Top             =   1740
         Width           =   210
      End
      Begin VB.TextBox Txt_Zone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -65610
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   6
         Text            =   "0"
         Top             =   1200
         Width           =   825
      End
      Begin VB.OptionButton Chk_Street 
         BackColor       =   &H00D6D6D6&
         Height          =   195
         Left            =   -74880
         TabIndex        =   83
         Top             =   720
         Width           =   210
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_SOI 
         Height          =   5415
         Left            =   -71730
         TabIndex        =   81
         Top             =   2160
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   9551
         _Version        =   393216
         BackColor       =   16777215
         Cols            =   4
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   4
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.TextBox Txt_Street_Name 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   -74310
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   1590
         Width           =   2415
      End
      Begin VB.TextBox Tx_Village_Name 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBEBE7&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Left            =   5700
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "-"
         Top             =   1590
         Width           =   3645
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_VILLAGE 
         Height          =   5325
         Left            =   4050
         TabIndex        =   73
         Top             =   2250
         Width           =   8805
         _ExtentX        =   15531
         _ExtentY        =   9393
         _Version        =   393216
         BackColor       =   16777215
         Rows            =   50
         Cols            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Zone 
         Height          =   4845
         Left            =   -72480
         TabIndex        =   92
         Top             =   2730
         Width           =   8325
         _ExtentX        =   14684
         _ExtentY        =   8546
         _Version        =   393216
         BackColor       =   16777215
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_STREET 
         Height          =   5415
         Left            =   -75000
         TabIndex        =   114
         Top             =   2160
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   9551
         _Version        =   393216
         BackColor       =   16777215
         Rows            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14079702
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         GridColor       =   14737632
         GridColorFixed  =   15856113
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   0
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_ZONETAX 
         Height          =   2385
         Left            =   -74970
         TabIndex        =   120
         Top             =   2280
         Width           =   3705
         _ExtentX        =   6535
         _ExtentY        =   4207
         _Version        =   393216
         BackColor       =   16777215
         Rows            =   3
         Cols            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   12743227
         ForeColorSel    =   16777215
         BackColorBkg    =   14079702
         GridColor       =   14737632
         GridColorFixed  =   15856113
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   0
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Class 
         Height          =   2445
         Left            =   -75000
         TabIndex        =   197
         Top             =   5130
         Width           =   12825
         _ExtentX        =   22622
         _ExtentY        =   4313
         _Version        =   393216
         BackColor       =   16777215
         Cols            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Business 
         Height          =   1725
         Left            =   -75000
         TabIndex        =   200
         Top             =   1740
         Width           =   12825
         _ExtentX        =   22622
         _ExtentY        =   3043
         _Version        =   393216
         BackColor       =   16777215
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14079702
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         GridColor       =   14737632
         GridColorFixed  =   15856113
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   0
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Tambon 
         Height          =   5325
         Left            =   0
         TabIndex        =   205
         Top             =   2250
         Width           =   4035
         _ExtentX        =   7117
         _ExtentY        =   9393
         _Version        =   393216
         BackColor       =   16777215
         Rows            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14079702
         ForeColorSel    =   0
         BackColorBkg    =   16777215
         GridColor       =   14737632
         GridColorFixed  =   15856113
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   0
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_BuildingType 
         Height          =   5295
         Left            =   -75000
         TabIndex        =   209
         Top             =   2250
         Width           =   3315
         _ExtentX        =   5847
         _ExtentY        =   9340
         _Version        =   393216
         BackColor       =   16777215
         Rows            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   12743227
         ForeColorSel    =   16777215
         BackColorBkg    =   14079702
         GridColor       =   14737632
         GridColorFixed  =   15856113
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   0
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_change 
         Height          =   5175
         Left            =   -74940
         TabIndex        =   320
         Top             =   2010
         Width           =   12705
         _ExtentX        =   22410
         _ExtentY        =   9128
         _Version        =   393216
         BackColor       =   16777215
         Cols            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         AllowBigSelection=   0   'False
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Request 
         Height          =   5175
         Left            =   -74940
         TabIndex        =   327
         Top             =   2100
         Width           =   12735
         _ExtentX        =   22463
         _ExtentY        =   9128
         _Version        =   393216
         BackColor       =   16777215
         Cols            =   3
         FixedCols       =   0
         BackColorFixed  =   14079702
         BackColorSel    =   14073244
         ForeColorSel    =   0
         BackColorBkg    =   15461351
         GridColor       =   15461351
         GridColorFixed  =   15856113
         AllowBigSelection=   0   'False
         FocusRect       =   0
         GridLinesFixed  =   1
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   3
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   1
         Left            =   1260
         Top             =   1800
         Width           =   2475
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "������ɳ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   285
         Index           =   180
         Left            =   -375
         TabIndex        =   349
         Top             =   1860
         Width           =   1545
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   179
         Left            =   -64080
         TabIndex        =   348
         Top             =   1350
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   178
         Left            =   -64080
         TabIndex        =   347
         Top             =   1710
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   177
         Left            =   -65970
         TabIndex        =   344
         Top             =   1710
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   176
         Left            =   -65970
         TabIndex        =   343
         Top             =   1350
         Width           =   1350
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   82
         Left            =   -67560
         Top             =   1680
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   81
         Left            =   -67560
         Top             =   1260
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   175
         Left            =   -69960
         TabIndex        =   338
         Top             =   1350
         Width           =   2325
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   174
         Left            =   -69660
         TabIndex        =   337
         Top             =   1710
         Width           =   2025
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��觻�١���ҧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   173
         Left            =   -70260
         TabIndex        =   336
         Top             =   1020
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   171
         Left            =   -70470
         TabIndex        =   335
         Top             =   1020
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   80
         Left            =   -69690
         Top             =   3990
         Width           =   1095
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   42
         Left            =   -69690
         Top             =   3540
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Ҵ��鹷�� ��鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   168
         Left            =   -71025
         TabIndex        =   333
         Top             =   4050
         Width           =   1185
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.�."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   110
         Left            =   -68445
         TabIndex        =   332
         Top             =   4050
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   79
         Left            =   -66570
         Top             =   1560
         Visible         =   0   'False
         Width           =   4365
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���˹�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   172
         Left            =   -67365
         TabIndex        =   329
         Top             =   1590
         Visible         =   0   'False
         Width           =   705
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   19
         Left            =   -68790
         Shape           =   4  'Rounded Rectangle
         Top             =   810
         Width           =   2535
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   77
         Left            =   -74190
         Top             =   1560
         Width           =   945
      End
      Begin VB.Label lb_Request_id 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74160
         TabIndex        =   324
         Top             =   1590
         Width           =   885
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   169
         Left            =   -74700
         TabIndex        =   325
         Top             =   1590
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������´"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   170
         Left            =   -73080
         TabIndex        =   326
         Top             =   1590
         Width           =   915
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   78
         Left            =   -72060
         Top             =   1560
         Width           =   4575
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   75
         Left            =   -72180
         Top             =   1320
         Width           =   9915
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   76
         Left            =   -74310
         Top             =   1320
         Width           =   945
      End
      Begin VB.Label lb_change_id 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74280
         TabIndex        =   319
         Top             =   1350
         Width           =   885
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   109
         Left            =   -74820
         TabIndex        =   318
         Top             =   1350
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������´"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   167
         Left            =   -73200
         TabIndex        =   315
         Top             =   1350
         Width           =   915
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   17
         Left            =   -74730
         Shape           =   4  'Rounded Rectangle
         Top             =   810
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   18
         Left            =   -71760
         Shape           =   4  'Rounded Rectangle
         Top             =   810
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   89
         Left            =   -70020
         TabIndex        =   317
         Top             =   750
         Width           =   675
      End
      Begin VB.Label lb_Class_Order 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -73950
         TabIndex        =   311
         Top             =   4200
         Width           =   915
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��˹��ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Left            =   -63555
         TabIndex        =   309
         Top             =   1260
         Width           =   1320
      End
      Begin VB.Label Lb_BuildingUse 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "    �ѵ�ҡ�������ª�����㹷��Թ����ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   2910
         Index           =   0
         Left            =   -71640
         TabIndex        =   308
         Tag             =   "    �ѵ�ҡ�������ª�����㹷��Թ����ç���͹"
         Top             =   1560
         Width           =   4590
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   166
         Left            =   -74685
         TabIndex        =   306
         Top             =   1260
         Width           =   405
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   165
         Left            =   -65085
         TabIndex        =   305
         Top             =   4170
         Width           =   285
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   164
         Left            =   -64020
         TabIndex        =   304
         Top             =   4200
         Width           =   435
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   163
         Left            =   -66060
         TabIndex        =   303
         Top             =   4170
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   74
         Left            =   -66720
         Top             =   4110
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   73
         Left            =   -64725
         Top             =   4110
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   52
         Left            =   -65790
         Top             =   4110
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ͷ��Ŵ���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   162
         Left            =   -67830
         TabIndex        =   299
         Top             =   4170
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������ª����Թ��������� �"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   119
         Left            =   -70200
         TabIndex        =   297
         Top             =   5970
         Width           =   2700
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   118
         Left            =   -69930
         TabIndex        =   296
         Top             =   6840
         Width           =   2325
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   51
         Left            =   -67560
         Top             =   6750
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   117
         Left            =   -69630
         TabIndex        =   295
         Top             =   7230
         Width           =   2025
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   50
         Left            =   -67560
         Top             =   7170
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   115
         Left            =   -65970
         TabIndex        =   294
         Top             =   6840
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   114
         Left            =   -65970
         TabIndex        =   293
         Top             =   7230
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   113
         Left            =   -64110
         TabIndex        =   292
         Top             =   6840
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   112
         Left            =   -64110
         TabIndex        =   291
         Top             =   7230
         Width           =   1455
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   49
         Left            =   -74970
         Top             =   5160
         Width           =   3705
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������������ª��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   106
         Left            =   -74550
         TabIndex        =   282
         Top             =   4800
         Width           =   1920
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   16
         Left            =   -74970
         Shape           =   4  'Rounded Rectangle
         Top             =   4740
         Width           =   2535
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   72
         Left            =   -69870
         Top             =   7110
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   71
         Left            =   -69870
         Top             =   5895
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   70
         Left            =   -69870
         Top             =   6300
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   69
         Left            =   -69870
         Top             =   6705
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   68
         Left            =   -64725
         Top             =   5895
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�����յ����͹ ����������е�����ҡ�˹�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   161
         Left            =   -73155
         TabIndex        =   280
         Top             =   7155
         Width           =   3105
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   160
         Left            =   -69135
         TabIndex        =   279
         Top             =   7200
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�����յ����͹ ����������е�����ҡ�˹�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   159
         Left            =   -73155
         TabIndex        =   278
         Top             =   4950
         Width           =   3105
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   67
         Left            =   -69870
         Top             =   4905
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   158
         Left            =   -69105
         TabIndex        =   277
         Top             =   4995
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������Ҥú��˹��ҡ�ѹ�Ѻ�� [�.�.3]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   157
         Left            =   -73005
         TabIndex        =   276
         Top             =   5940
         Width           =   2955
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   156
         Left            =   -69150
         TabIndex        =   275
         Top             =   5940
         Width           =   225
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���������������Ẻ�ʴ���¡�����㹡�˹�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   155
         Left            =   -73125
         TabIndex        =   274
         Top             =   6345
         Width           =   3075
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   154
         Left            =   -69150
         TabIndex        =   273
         Top             =   6390
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ͧ����������������Ẻ��¡�����ú"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   153
         Left            =   -73125
         TabIndex        =   272
         Top             =   6750
         Width           =   3075
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   152
         Left            =   -69150
         TabIndex        =   271
         Top             =   6795
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   151
         Left            =   -64050
         TabIndex        =   270
         Top             =   5985
         Width           =   225
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���������طó�Ѻ�ҡ�ѹ�Ѻ�� [�.�.3]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   150
         Left            =   -67680
         TabIndex        =   269
         Top             =   5940
         Width           =   2805
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ջ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   149
         Left            =   -68970
         TabIndex        =   268
         Top             =   5445
         Width           =   645
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���������طó�Ѻ�ҡ�ѹ�Ѻ�� [�.�.�.9]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   148
         Left            =   -67860
         TabIndex        =   266
         Top             =   3735
         Width           =   2985
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   66
         Left            =   -64725
         Top             =   3690
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   147
         Left            =   -64005
         TabIndex        =   265
         Top             =   3735
         Width           =   225
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   146
         Left            =   -69105
         TabIndex        =   264
         Top             =   4545
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   65
         Left            =   -69870
         Top             =   4500
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ͧ����������������Ẻ��¡�����ú"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   145
         Left            =   -73125
         TabIndex        =   263
         Top             =   4545
         Width           =   3075
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   64
         Left            =   -69870
         Top             =   4050
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   144
         Left            =   -69105
         TabIndex        =   262
         Top             =   4140
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���������������Ẻ�ʴ���¡�����㹡�˹�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   143
         Left            =   -73125
         TabIndex        =   261
         Top             =   4110
         Width           =   3075
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   63
         Left            =   -69870
         Top             =   3645
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   142
         Left            =   -69105
         TabIndex        =   260
         Top             =   3735
         Width           =   225
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������Ҥú��˹��ҡ�ѹ�Ѻ�� [�.�.�9]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   141
         Left            =   -73125
         TabIndex        =   259
         Top             =   3705
         Width           =   3075
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���պ��ا��ͧ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   140
         Left            =   -69240
         TabIndex        =   258
         Top             =   3225
         Width           =   1230
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   139
         Left            =   -63975
         TabIndex        =   256
         Top             =   2655
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   62
         Left            =   -64695
         Top             =   2610
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ��Ŵ�Ҥ�����ͧ�ѡ� (��͹�ӹǳ��»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   138
         Left            =   -67770
         TabIndex        =   255
         Top             =   2655
         Width           =   2925
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   137
         Left            =   -63975
         TabIndex        =   254
         Top             =   1890
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   61
         Left            =   -64695
         Top             =   1800
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   136
         Left            =   -63975
         TabIndex        =   253
         Top             =   1485
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   60
         Left            =   -64695
         Top             =   1395
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�����դ����ҵ�ͻ� (����ѹ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   135
         Left            =   -66870
         TabIndex        =   252
         Top             =   1845
         Width           =   1995
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�����դ����ҵ�ͻ� (�����͹)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   134
         Left            =   -67035
         TabIndex        =   251
         Top             =   1440
         Width           =   2145
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   133
         Left            =   -63975
         TabIndex        =   250
         Top             =   1035
         Width           =   225
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   59
         Left            =   -64695
         Top             =   990
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���������طó�Ѻ�ҡ�ѹ�Ѻ�� [�.�.�.8]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   132
         Left            =   -67860
         TabIndex        =   249
         Top             =   1035
         Width           =   2955
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   131
         Left            =   -69105
         TabIndex        =   248
         Top             =   2655
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   130
         Left            =   -69105
         TabIndex        =   247
         Top             =   2250
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   129
         Left            =   -69105
         TabIndex        =   246
         Top             =   1845
         Width           =   195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   128
         Left            =   -69105
         TabIndex        =   245
         Top             =   1440
         Width           =   195
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   58
         Left            =   -69870
         Top             =   2610
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   57
         Left            =   -69870
         Top             =   2205
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   56
         Left            =   -69870
         Top             =   1800
         Width           =   600
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   55
         Left            =   -69870
         Top             =   1395
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ҧ��������;鹡�˹�����Թ 4 ��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   127
         Left            =   -73035
         TabIndex        =   244
         Top             =   2655
         Width           =   2985
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ҧ��������;鹡�˹�����Թ 3 ��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   126
         Left            =   -73035
         TabIndex        =   243
         Top             =   2250
         Width           =   2985
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ҧ��������;鹡�˹�����Թ 2 ��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   125
         Left            =   -73035
         TabIndex        =   242
         Top             =   1845
         Width           =   2985
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���շ���ҧ��������;鹡�˹�����Թ 1 ��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   124
         Left            =   -73035
         TabIndex        =   241
         Top             =   1440
         Width           =   2985
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   123
         Left            =   -69105
         TabIndex        =   240
         Top             =   1035
         Width           =   225
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   54
         Left            =   -69870
         Top             =   990
         Width           =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������Ҥú��˹��ҡ�ѹ�Ѻ�� [�.�.�.8]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   122
         Left            =   -73155
         TabIndex        =   239
         Top             =   1035
         Width           =   3105
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����ç���͹��з��Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   111
         Left            =   -69420
         TabIndex        =   238
         Top             =   570
         Width           =   1710
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��.�./��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   121
         Left            =   -68445
         TabIndex        =   236
         Top             =   2670
         Width           =   1095
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   53
         Left            =   -69690
         Top             =   2610
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��鹷�������ͧ(�����»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   120
         Left            =   -71385
         TabIndex        =   235
         Top             =   2670
         Width           =   1545
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��ͧ/�ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   116
         Left            =   -63495
         TabIndex        =   234
         Top             =   5850
         Width           =   855
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������ç���͹��з��Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   108
         Left            =   -71190
         TabIndex        =   232
         Top             =   690
         Width           =   2130
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   48
         Left            =   -69690
         Top             =   3090
         Width           =   1095
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   47
         Left            =   -64830
         Top             =   5820
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��.�./��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   107
         Left            =   -68445
         TabIndex        =   231
         Top             =   3135
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��鹷����� � (�����»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   105
         Left            =   -71265
         TabIndex        =   230
         Top             =   3135
         Width           =   1425
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����Ҿ�鹷�� (�����»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   104
         Left            =   -66405
         TabIndex        =   229
         Top             =   5850
         Width           =   1485
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   46
         Left            =   -64755
         Top             =   3255
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   103
         Left            =   -63495
         TabIndex        =   228
         Top             =   3315
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���վ�鹷���鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   102
         Left            =   -65955
         TabIndex        =   227
         Top             =   3315
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   45
         Left            =   -64755
         Top             =   2790
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��.�./��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   101
         Left            =   -63495
         TabIndex        =   226
         Top             =   2820
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��鹷���ç���͹ (�����»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   100
         Left            =   -66585
         TabIndex        =   225
         Top             =   2820
         Width           =   1665
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   99
         Left            =   -68430
         TabIndex        =   224
         Top             =   6135
         Width           =   375
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   44
         Left            =   -69675
         Top             =   6090
         Width           =   1095
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   43
         Left            =   -69675
         Top             =   5625
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��ͧ/��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   98
         Left            =   -68445
         TabIndex        =   223
         Top             =   5685
         Width           =   1005
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���վ�鹷���鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   97
         Left            =   -70875
         TabIndex        =   222
         Top             =   6135
         Width           =   1035
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ����Ң�鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   96
         Left            =   -70845
         TabIndex        =   221
         Top             =   5685
         Width           =   1005
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   95
         Left            =   -68445
         TabIndex        =   220
         Top             =   3600
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������»� ��鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   94
         Left            =   -70935
         TabIndex        =   219
         Top             =   3600
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "� /��.�./��͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   93
         Left            =   -68445
         TabIndex        =   218
         Top             =   2190
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��鹷���ç���͹ (�����»�)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   240
         Index           =   92
         Left            =   -71505
         TabIndex        =   217
         Top             =   2190
         Width           =   1665
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   91
         Left            =   -74910
         TabIndex        =   216
         Top             =   1920
         Width           =   675
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   90
         Left            =   -74640
         TabIndex        =   215
         Top             =   1620
         Width           =   375
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   41
         Left            =   -74190
         Top             =   1860
         Width           =   2475
      End
      Begin VB.Label Lb_BuildingType_ID 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74160
         TabIndex        =   214
         Top             =   1590
         Width           =   2415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   40
         Left            =   -74190
         Top             =   1560
         Width           =   2475
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   38
         Left            =   -69690
         Top             =   2145
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������ç���͹"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   84
         Left            =   -74370
         TabIndex        =   208
         Top             =   690
         Width           =   1335
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   39
         Left            =   -72090
         Top             =   4620
         Width           =   1695
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ : ��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   88
         Left            =   -70260
         TabIndex        =   204
         Top             =   4680
         Width           =   675
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��Ҹ�������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   87
         Left            =   -73290
         TabIndex        =   203
         Top             =   4680
         Width           =   1065
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӴѺ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   86
         Left            =   -74640
         TabIndex        =   202
         Top             =   4200
         Width           =   585
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   37
         Left            =   -72090
         Top             =   4140
         Width           =   9795
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѡɳ�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   85
         Left            =   -72840
         TabIndex        =   201
         Top             =   4200
         Width           =   615
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   36
         Left            =   -73980
         Top             =   4170
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѡɳСԨ��ä��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   83
         Left            =   -74400
         TabIndex        =   198
         Top             =   3630
         Width           =   1305
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   82
         Left            =   -73050
         TabIndex        =   196
         Top             =   1260
         Width           =   675
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   81
         Left            =   -74610
         TabIndex        =   195
         Top             =   1260
         Width           =   375
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   35
         Left            =   -72300
         Top             =   1230
         Width           =   10065
      End
      Begin VB.Label Lb_Business 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74070
         TabIndex        =   194
         Top             =   1260
         Width           =   885
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   34
         Left            =   -74100
         Top             =   1230
         Width           =   945
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������Ԩ��ä��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   59
         Left            =   -74460
         TabIndex        =   192
         Top             =   690
         Width           =   1365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   80
         Left            =   -63930
         TabIndex        =   190
         Top             =   5190
         Width           =   405
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   79
         Left            =   -63930
         TabIndex        =   189
         Top             =   3990
         Width           =   405
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   78
         Left            =   -63930
         TabIndex        =   188
         Top             =   2910
         Width           =   405
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   77
         Left            =   -63930
         TabIndex        =   187
         Top             =   1710
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   33
         Left            =   -65310
         Top             =   5100
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   32
         Left            =   -65280
         Top             =   3900
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   31
         Left            =   -65280
         Top             =   2820
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   30
         Left            =   -65280
         Top             =   1590
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.��.      �ѵ�����բ�鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   76
         Left            =   -67710
         TabIndex        =   186
         Top             =   5190
         Width           =   2145
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.��.      �ѵ�����բ�鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   75
         Left            =   -67710
         TabIndex        =   185
         Top             =   3990
         Width           =   2145
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.��.      �ѵ�����բ�鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   74
         Left            =   -67710
         TabIndex        =   184
         Top             =   2910
         Width           =   2145
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��.��.      �ѵ�����բ�鹵��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   73
         Left            =   -67710
         TabIndex        =   183
         Top             =   1710
         Width           =   2145
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   29
         Left            =   -69090
         Top             =   5100
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   28
         Left            =   -69090
         Top             =   3900
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   27
         Left            =   -69090
         Top             =   2820
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   26
         Left            =   -71520
         Top             =   5100
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   25
         Left            =   -71550
         Top             =   3900
         Width           =   1155
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   24
         Left            =   -71520
         Top             =   2820
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ.     ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   72
         Left            =   -70170
         TabIndex        =   182
         Top             =   5190
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ.     ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   71
         Left            =   -70170
         TabIndex        =   181
         Top             =   3990
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ.     ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   70
         Left            =   -70170
         TabIndex        =   180
         Top             =   2910
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   69
         Left            =   -72450
         TabIndex        =   179
         Top             =   5190
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   68
         Left            =   -72450
         TabIndex        =   178
         Top             =   3990
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   67
         Left            =   -72450
         TabIndex        =   177
         Top             =   2910
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ.     ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   66
         Left            =   -70170
         TabIndex        =   176
         Top             =   1710
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   65
         Left            =   -72450
         TabIndex        =   175
         Top             =   1710
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "3 �"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   64
         Left            =   -73650
         TabIndex        =   174
         Top             =   4590
         Width           =   300
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "3 �"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   63
         Left            =   -73650
         TabIndex        =   173
         Top             =   3450
         Width           =   285
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   62
         Left            =   -73650
         TabIndex        =   172
         Top             =   2280
         Width           =   135
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   61
         Left            =   -73650
         TabIndex        =   171
         Top             =   1200
         Width           =   135
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������ѡ���ºҧ��ǹ�����ӡ��������ѧ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   60
         Left            =   -73320
         TabIndex        =   170
         Top             =   4590
         Width           =   3540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "5."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   58
         Left            =   -70470
         TabIndex        =   168
         Top             =   4920
         Width           =   225
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "4."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   57
         Left            =   -70470
         TabIndex        =   167
         Top             =   3960
         Width           =   225
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "3."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   56
         Left            =   -70470
         TabIndex        =   166
         Top             =   2940
         Width           =   225
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "2."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   55
         Left            =   -70470
         TabIndex        =   165
         Top             =   1980
         Width           =   225
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   54
         Left            =   -64080
         TabIndex        =   164
         Top             =   5640
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   53
         Left            =   -64080
         TabIndex        =   163
         Top             =   5250
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   52
         Left            =   -64080
         TabIndex        =   162
         Top             =   4650
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   51
         Left            =   -64080
         TabIndex        =   161
         Top             =   4290
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   50
         Left            =   -64080
         TabIndex        =   160
         Top             =   3630
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   49
         Left            =   -64080
         TabIndex        =   159
         Top             =   3210
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   48
         Left            =   -64080
         TabIndex        =   158
         Top             =   2670
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�Ҥ����   (�ҷ)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   47
         Left            =   -64080
         TabIndex        =   157
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   46
         Left            =   -65970
         TabIndex        =   149
         Top             =   5640
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   45
         Left            =   -65970
         TabIndex        =   148
         Top             =   5250
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   44
         Left            =   -65970
         TabIndex        =   147
         Top             =   4650
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   43
         Left            =   -65970
         TabIndex        =   146
         Top             =   4260
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   42
         Left            =   -65970
         TabIndex        =   145
         Top             =   3630
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   41
         Left            =   -65970
         TabIndex        =   144
         Top             =   3210
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   33
         Left            =   -65970
         TabIndex        =   143
         Top             =   2670
         Width           =   1350
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�������� (%)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   15
         Left            =   -65970
         TabIndex        =   142
         Top             =   2280
         Width           =   1350
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   22
         Left            =   -67560
         Top             =   5580
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   40
         Left            =   -69630
         TabIndex        =   132
         Top             =   5640
         Width           =   2025
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   16
         Left            =   -67560
         Top             =   4620
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   39
         Left            =   -69630
         TabIndex        =   131
         Top             =   4680
         Width           =   2025
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   38
         Left            =   -69660
         TabIndex        =   130
         Top             =   3660
         Width           =   2025
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó������� >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   37
         Left            =   -69660
         TabIndex        =   129
         Top             =   2670
         Width           =   2025
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   15
         Left            =   -67560
         Top             =   2610
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   14
         Left            =   -67560
         Top             =   5160
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   13
         Left            =   -67560
         Top             =   4200
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   12
         Left            =   -67560
         Top             =   3570
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   32
         Left            =   -69930
         TabIndex        =   128
         Top             =   5250
         Width           =   2325
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   10
         Left            =   -67560
         Top             =   3150
         Width           =   1035
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   9
         Left            =   -67560
         Top             =   2190
         Width           =   1035
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   19
         Left            =   -69930
         TabIndex        =   127
         Top             =   4290
         Width           =   2325
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   17
         Left            =   -69960
         TabIndex        =   126
         Top             =   3240
         Width           =   2325
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������ < �ó���ҹ�ͧ >"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   16
         Left            =   -69960
         TabIndex        =   125
         Top             =   2280
         Width           =   2325
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��ҧ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   14
         Left            =   -70230
         TabIndex        =   124
         Top             =   4920
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����׹��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   13
         Left            =   -70230
         TabIndex        =   123
         Top             =   3960
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������ء"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   12
         Left            =   -70230
         TabIndex        =   122
         Top             =   2940
         Width           =   645
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����§�ѵ��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   9
         Left            =   -70230
         TabIndex        =   121
         Top             =   1980
         Width           =   720
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��������ª����Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   8
         Left            =   -70620
         TabIndex        =   119
         Top             =   630
         Width           =   1650
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ�ҷ�������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   6
         Left            =   -74250
         TabIndex        =   117
         Top             =   630
         Width           =   1140
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   23
         Left            =   -70050
         Top             =   1560
         Width           =   3705
      End
      Begin VB.Label LB_SOI_ID 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   -70020
         TabIndex        =   115
         Top             =   1260
         Width           =   3645
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   8
         Left            =   -70050
         Top             =   1230
         Width           =   3705
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   7
         Left            =   -74340
         Top             =   1260
         Width           =   2475
      End
      Begin VB.Label LB_STREET_ID 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74310
         TabIndex        =   113
         Top             =   1290
         Width           =   2415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   21
         Left            =   -71520
         Top             =   1590
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������ѡ������ǹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   36
         Left            =   -73440
         TabIndex        =   111
         Top             =   1200
         Width           =   1530
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   20
         Left            =   -69120
         Top             =   1590
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����������ѡ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   35
         Left            =   -73320
         TabIndex        =   110
         Top             =   3450
         Width           =   1410
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������ѡ���»��ѧ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   34
         Left            =   -73440
         TabIndex        =   109
         Top             =   2280
         Width           =   1980
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ : ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   31
         Left            =   -72180
         TabIndex        =   108
         Top             =   1890
         Width           =   735
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѵ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   30
         Left            =   -74700
         TabIndex        =   107
         Top             =   1890
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ҥҡ�ҧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   29
         Left            =   -74730
         TabIndex        =   106
         Top             =   1530
         Width           =   810
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   19
         Left            =   -73800
         Top             =   1830
         Width           =   1485
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ҷ : ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   28
         Left            =   -72180
         TabIndex        =   105
         Top             =   1530
         Width           =   735
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "��������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   285
         Index           =   27
         Left            =   -74730
         TabIndex        =   104
         Top             =   1170
         Width           =   765
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   18
         Left            =   -73800
         Top             =   1530
         Width           =   1485
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�����ҹ (�����)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   26
         Left            =   4530
         TabIndex        =   102
         Top             =   690
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ӻ�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   25
         Left            =   630
         TabIndex        =   100
         Top             =   690
         Width           =   435
      End
      Begin VB.Label Lb_Tambon_Id 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2460
         TabIndex        =   99
         Top             =   1230
         Width           =   1245
      End
      Begin VB.Label Lb_Amphoe_Id 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1290
         TabIndex        =   98
         Top             =   1230
         Width           =   1125
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   3
         Left            =   2430
         Top             =   1200
         Width           =   1305
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   0
         Left            =   1260
         Top             =   1200
         Width           =   1185
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ӻ�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   3
         Left            =   735
         TabIndex        =   97
         Top             =   1560
         Width           =   435
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   0
         Left            =   810
         TabIndex        =   96
         Top             =   1230
         Width           =   360
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   0
         Left            =   1260
         Top             =   1500
         Width           =   2475
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��¡�èӹǹࢵ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   24
         Left            =   -70680
         TabIndex        =   91
         Top             =   1230
         Width           =   1440
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   17
         Left            =   -69030
         Top             =   2130
         Width           =   1305
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�кبӹǹࢵ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   23
         Left            =   -70710
         TabIndex        =   90
         Top             =   2190
         Width           =   1485
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "������ࢵ���� (Block)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   225
         Index           =   22
         Left            =   -71430
         TabIndex        =   89
         Top             =   1710
         Width           =   2295
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "������ࢵ (Zone)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   21
         Left            =   -71430
         TabIndex        =   87
         Top             =   690
         Width           =   1680
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�кبӹǹࢵ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   20
         Left            =   -67110
         TabIndex        =   84
         Top             =   1230
         Width           =   1170
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   4
         Left            =   -65640
         Top             =   1170
         Width           =   885
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ͫ�� <��͡>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   18
         Left            =   -71430
         TabIndex        =   82
         Top             =   1620
         Width           =   1305
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   375
         Index           =   0
         Left            =   -69480
         Shape           =   4  'Rounded Rectangle
         Top             =   4710
         Width           =   4065
      End
      Begin VB.Label Lb_Village_Id 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8280
         TabIndex        =   80
         Top             =   1260
         Width           =   1065
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   6
         Left            =   5670
         Top             =   1230
         Width           =   2595
      End
      Begin VB.Label Lb_Tambon_Id2 
         Alignment       =   2  'Center
         BackColor       =   &H00EBEBE7&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5700
         TabIndex        =   79
         Top             =   1260
         Width           =   2565
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   11
         Left            =   -70530
         TabIndex        =   78
         Top             =   1260
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "��� <��͡>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   10
         Left            =   -71190
         TabIndex        =   77
         Top             =   690
         Width           =   1095
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   11
         Left            =   -74340
         Top             =   1560
         Width           =   2475
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   7
         Left            =   -74820
         TabIndex        =   76
         Top             =   1290
         Width           =   375
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   5
         Left            =   -74820
         TabIndex        =   75
         Top             =   1620
         Width           =   375
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   4
         Left            =   -74310
         TabIndex        =   74
         Top             =   690
         Width           =   345
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H008B8B8B&
         BorderColor     =   &H00D6BD9C&
         Height          =   345
         Index           =   5
         Left            =   8250
         Top             =   1230
         Width           =   1125
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   240
         Index           =   1
         Left            =   5190
         TabIndex        =   72
         Top             =   1260
         Width           =   360
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "���������ҹ (�����)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00575555&
         Height          =   285
         Index           =   2
         Left            =   4050
         TabIndex        =   71
         Top             =   1590
         Width           =   1545
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00D6BD9C&
         BorderColor     =   &H00D6BD9C&
         Height          =   315
         Index           =   1
         Left            =   5670
         Top             =   1560
         Width           =   3705
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   1
         Left            =   30
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   1695
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   2
         Left            =   4080
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   1995
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   3
         Left            =   -74970
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   1695
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   4
         Left            =   -71760
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   5
         Left            =   -71760
         Shape           =   4  'Rounded Rectangle
         Top             =   1650
         Width           =   2565
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   3
         Left            =   -75000
         Top             =   270
         Width           =   12840
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   7
         Left            =   -71700
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   1995
      End
      Begin VB.Image Image1 
         Height          =   7125
         Index           =   2
         Left            =   -75000
         Top             =   270
         Width           =   12840
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   6
         Left            =   -74970
         Shape           =   4  'Rounded Rectangle
         Top             =   570
         Width           =   2625
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   8
         Left            =   -71160
         Shape           =   4  'Rounded Rectangle
         Top             =   570
         Width           =   2415
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   6
         Left            =   -75000
         Top             =   270
         Width           =   12780
      End
      Begin VB.Image Image1 
         Height          =   7965
         Index           =   1
         Left            =   0
         Top             =   300
         Width           =   12840
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   10
         Left            =   -74940
         Shape           =   4  'Rounded Rectangle
         Top             =   3570
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   9
         Left            =   -74970
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   2535
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   5
         Left            =   -75000
         Top             =   270
         Width           =   12840
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   11
         Left            =   -74970
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   12
         Left            =   -71640
         Shape           =   4  'Rounded Rectangle
         Top             =   630
         Width           =   2775
      End
      Begin VB.Label Lb_BuildingUse 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "    �ѵ�Ҥ����Ҥ�������Ҩ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   2985
         Index           =   2
         Left            =   -71640
         TabIndex        =   210
         Top             =   4545
         Width           =   4590
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   13
         Left            =   -69915
         Shape           =   4  'Rounded Rectangle
         Top             =   495
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   14
         Left            =   -69915
         Shape           =   4  'Rounded Rectangle
         Top             =   3150
         Width           =   2535
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00D6D6D6&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H008A8A7A&
         Height          =   345
         Index           =   15
         Left            =   -69915
         Shape           =   4  'Rounded Rectangle
         Top             =   5400
         Width           =   2535
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   8
         Left            =   -75000
         Top             =   300
         Width           =   12840
      End
      Begin VB.Label Lb_BuildingUse 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "    �ѵ�Ҩҡ��ͧ������;�鹷����ҵ���ѹ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   2985
         Index           =   4
         Left            =   -66990
         TabIndex        =   212
         Top             =   4545
         Width           =   4770
      End
      Begin VB.Label Lb_BuildingUse 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "    �ѵ�Ҥ����Ҩҡ�ѡɳ���л�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   2910
         Index           =   3
         Left            =   -66990
         TabIndex        =   211
         Top             =   1560
         Width           =   4770
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   7
         Left            =   -75000
         Top             =   270
         Width           =   12840
      End
      Begin VB.Image Image1 
         Height          =   7215
         Index           =   4
         Left            =   -75000
         Top             =   270
         Width           =   12840
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   0
         Left            =   -75000
         Top             =   300
         Width           =   12840
      End
      Begin VB.Image Image1 
         Height          =   7935
         Index           =   9
         Left            =   -75000
         Top             =   300
         Width           =   12840
      End
   End
   Begin VB.OptionButton OpMenu 
      Appearance      =   0  'Flat
      BackColor       =   &H00D6BD9C&
      DownPicture     =   "Frm_Index.frx":46D5B
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   405
      Index           =   1
      Left            =   0
      Picture         =   "Frm_Index.frx":4766A
      Style           =   1  'Graphical
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   930
      Width           =   2592
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H008B8B8B&
      Height          =   465
      Index           =   2
      Left            =   4200
      Top             =   5700
      Width           =   2325
   End
End
Attribute VB_Name = "Frm_Index"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit
  Dim Status As String
  Dim ListCurrent As Byte
  Dim secattr As SECURITY_ATTRIBUTES  ' security attributes structure

Private Sub SET_REFRESH()
On Error GoTo Err_refresh
If OpMenu(1).Value Then '�Ӻ�
       GBQueryTambon.Requery
       GBQueryTambon.Filter = " AMPHOE_ID = '" & Trim$(iniAmphoe_Id) & "'"
                If GBQueryTambon.RecordCount > 0 Then
                       Set Grid_Tambon.DataSource = GBQueryTambon
                Else
                        Grid_Tambon.Rows = 2
                        Grid_Tambon.Clear
                End If
       GBQueryVillage.Requery
            If GBQueryVillage.RecordCount > 0 Then
                    Set Grid_VILLAGE.DataSource = GBQueryVillage
            Else
                    Grid_VILLAGE.Rows = 2
                    Grid_VILLAGE.Clear
            End If
End If

If OpMenu(3).Value Then  '���-���
    GBQueryStreet_Name.Requery
        If GBQueryStreet_Name.RecordCount > 0 Then
               Set Grid_STREET.DataSource = GBQueryStreet_Name
        Else
                Grid_STREET.Rows = 2
                Grid_STREET.Clear
        End If
    GBQuerySoi.Requery
                If GBQuerySoi.RecordCount > 0 Then
                       Set Grid_SOI.DataSource = GBQuerySoi
                Else
                        Grid_SOI.Rows = 2
                        Grid_SOI.Clear
                End If
End If

If OpMenu(4).Value Then '⫹
           With GBQueryZone
                     .Requery
                        Dim BUFFER_ZONE As String
                                 Cmb_Zone.Clear
                                    .Filter = "ZONE_ID <> '' "
                        If .RecordCount > 0 Then
                                    .MoveFirst
                            Do While Not .EOF
                                    If BUFFER_ZONE <> .Fields("ZONE_ID").Value Then
                                            Cmb_Zone.AddItem .Fields("ZONE_ID").Value
                                            BUFFER_ZONE = .Fields("ZONE_ID").Value
                                    End If
                                    .MoveNext
                            Loop
                            Cmb_Zone.ListIndex = ListCurrent
                        Else
                                  Grid_Zone.Clear
                                  Grid_Zone.Rows = 2
                        End If
            End With
End If

If OpMenu(7).Value Then '�������Ԩ��ä��
                With GBQueryBusiness
                        .Requery
                        If .RecordCount > 0 Then
                                Set Grid_Business.DataSource = GBQueryBusiness
                        Else
                                Grid_Business.Rows = 2
                                Grid_Business.Clear
                        End If
                 End With
                
                With GBQueryClass
                        .Requery
                        .Filter = " Business_Id =  '" & Lb_Business.Caption & "'"
                         If .RecordCount > 0 Then
                            Set Grid_Class.DataSource = GBQueryClass
                         Else
                              Grid_Class.Rows = 2
                              Grid_Class.Clear
                         End If
                End With
End If

If OpMenu(5).Value Then '�ѵ�����պ��ا��ͧ���
'        With GBQueryZoneTax
'                .Requery
'                        If .RecordCount > 0 Then
'                                    combo2.Clear
'                                    .MoveFirst
'                            Do While Not .EOF
'                                          If .Fields("ZONETAX_ID").Value <> Empty Then
'                                               combo2.AddItem .Fields("ZONETAX_ID").Value
'                                          End If
'                                            .MoveNext
'                            Loop
'                                           combo2.ListIndex = 0
'                        End If
'            End With
     GBQueryLandUse.Requery
     'GBQueryLandData.Requery
            With GBQueryZoneTax
                            .Requery
                        If .RecordCount > 0 Then
                               Set GRID_ZONETAX.DataSource = GBQueryZoneTax
                        Else
                                GRID_ZONETAX.Rows = 2
                                GRID_ZONETAX.Clear
                        End If
             End With
     
     
            With GBQueryLandEmployment   '��������������ª��
                     .Requery
                     List1.Clear
'                     Combo1.Clear
                     If .RecordCount > 0 Then
                                .MoveFirst
                                 Do While Not .EOF
                                       List1.AddItem IIf(IsNull(.Fields("LM_Details").Value), "", .Fields("LM_Details").Value)
'                                       Combo1.AddItem .Fields("LM_Details").Value
                                       .MoveNext
                                 Loop
                     End If
            End With
            With GBQueryLandUse   '��������ª����Թ
                     .Requery
                     .Filter = "ZONETAX_ID = '" & Combo2.Text & "' AND LM_ID > 'LM-05'"
                     Combo1.Clear
                     Combo3.Clear
                     If .RecordCount > 0 Then
                                .MoveFirst
                                 Do While Not .EOF
                                       Combo1.AddItem IIf(IsNull(.Fields("LM_Details").Value), "", .Fields("LM_Details").Value)
                                       Combo3.AddItem .Fields("LM_ID").Value
                                       .MoveNext
                                 Loop
                     End If
            End With
End If

If OpMenu(2).Value Then '�ѵ�������ç���͹��з��Թ
            With GBQueryBuildingType
                        .Requery
                    If .RecordCount > 0 Then
                        Set Grid_BuildingType.DataSource = GBQueryBuildingType
                     Else
                          Grid_BuildingType.Rows = 2
                          Grid_BuildingType.Clear
                     End If
            End With
       GBQueryBuildingRate.Requery
       
'            With GBQueryZoneTax
'                        If .RecordCount > 0 Then
'                                    Cmb_Tamlay.Clear
'                                    .MoveFirst
'                            Do While Not .EOF
'                                          If .Fields("ZONETAX_ID").Value <> Empty Then
'                                               Cmb_Tamlay.AddItem .Fields("ZONETAX_ID").Value
'                                          End If
'                                            .MoveNext
'                            Loop
'                                           Cmb_Tamlay.ListIndex = 0
'                        End If
'            End With
End If

If OpMenu(6).Value Then '�ѵ�����ջ���
    GBQuerySignBordData_Type.Requery
                With GBQuerySignBordData_Type
                            If .RecordCount > 0 Then
                                                     Dim i As Byte
                                                     i = 0
                                                    .MoveFirst
                                                Do While Not .EOF
                                                          i = i + 1
                                                              TXT_SIGNBORD_RATE(i).Text = .Fields("SIGNBORD_RATE").Value
                                                              TXT_SIGNBORD_RATE_OF_USE(i).Text = .Fields("SIGNBORD_RATE_OF_USE").Value
                                                              TXT_SIGNBORD_LOW(i).Text = .Fields("SIGNBORD_LOW").Value
                                                       .MoveNext
                                                Loop
                            End If
            End With
End If

If OpMenu(8).Value Then '���͹��ѵ������
    With GBQueryDuePayTax
               .Requery
               .MoveFirst
            Do While Not .EOF
                  If .Fields("Type_Rate").Value = 0 Then
                                    Txt_Due_Accept0.Text = .Fields("Due_Accept").Value
                                    Txt_Due_1Month.Text = .Fields("Due_1Month").Value
                                    Txt_Due_2Month.Text = .Fields("Due_2Month").Value
                                    Txt_Due_3Month.Text = .Fields("Due_3Month").Value
                                    Txt_Due_4Month.Text = .Fields("Due_4Month").Value
                                    Txt_Due_Due_Appeal0.Text = .Fields("Due_Appeal").Value
                                    Txt_Tax_YPerM.Text = .Fields("Tax_YPerM").Value
                                    Tax_MPerD.Text = .Fields("Tax_MPerD").Value
                                    Txt_Tax_Machine.Text = .Fields("Tax_Machine").Value
                  End If
                   If .Fields("Type_Rate").Value = 1 Then
                                    Txt_Due_Accept1.Text = .Fields("Due_Accept").Value
                                    Txt_Tax_OutSchedule1.Text = .Fields("Tax_OutSchedule").Value
                                    Txt_Tax_NotFully.Text = .Fields("Tax_NotFully").Value
                                    Txt_Tax_OutTime1.Text = .Fields("Tax_OutTime").Value
                                    Txt_Due_Due_Appeal1.Text = .Fields("Due_Appeal").Value
                                    Txt_B_Rai.Text = .Fields("Subside_Rai").Value
                                    Txt_B_Pot.Text = .Fields("Subside_Ngan").Value
                                    Txt_B_Va.Text = .Fields("Subside_Va").Value
                  End If
                  If .Fields("Type_Rate").Value = 2 Then
                                    Txt_Due_Accept2.Text = .Fields("Due_Accept").Value
                                    Txt_Tax_OutSchedule2.Text = .Fields("Tax_OutSchedule").Value
                                    Txt_Tax_NotFully2.Text = .Fields("Tax_NotFully").Value
                                    Txt_Tax_OutTime2.Text = .Fields("Tax_OutTime").Value
                                    Txt_Due_Due_Appeal2.Text = .Fields("Due_Appeal").Value
                  End If
                     .MoveNext
            Loop
     End With
End If
If OpMenu(9).Value Then
        Select Case cb_type_asset.ListIndex
                Case 0
                        GBQueryLandChange.Requery
                        If GBQueryLandChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryLandChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 1
                        GBQueryBuildingChange.Requery
                        If GBQueryBuildingChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryBuildingChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 2
                        GBQuerySignboardChange.Requery
                        If GBQuerySignboardChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQuerySignboardChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 3
                        GBQueryLicenseChange.Requery
                        If GBQueryLicenseChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryLicenseChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
        End Select
End If
If OpMenu(10).Value Then
        If Opt_Request.Value Then
                With GBQueryPetitionDetails
                        .Requery
                        If .RecordCount > 0 Then
                                Set Grid_Request.DataSource = GBQueryPetitionDetails
                        Else
                                Grid_Request.Rows = 2
                                Grid_Request.Clear
                        End If
                End With
        ElseIf Opt_Building_Type.Value Then
                With GBQueryBuildingDetails
                        .Requery
                        If .RecordCount > 0 Then
                                Set Grid_Request.DataSource = GBQueryBuildingDetails
                        Else
                                Grid_Request.Rows = 2
                                Grid_Request.Clear
                        End If
                End With
        Else
                Call Set_Grid
                With GBQueryOfficer
                        .Requery
                        If .RecordCount > 0 Then
                                Set Grid_Request.DataSource = GBQueryOfficer
                        Else
                                Grid_Request.Cols = 3
                                Grid_Request.Rows = 2
                                Grid_Request.Clear
                        End If
                End With
        End If
End If
          Call Set_Grid
Exit Sub
Err_refresh:
        MsgBox Err.Description
End Sub

Private Sub Set_Grid()
DoEvents
          If OpMenu(1).Value Then
            Grid_Tambon.ColWidth(0) = 800 ':   Grid_Tambon.TextArray(0) = "���ʷ����"
            Grid_Tambon.ColWidth(1) = 2150 ':   Grid_Tambon.TextArray(1) = "�Ӻ�"
            Grid_Tambon.ColWidth(2) = 0
            Grid_Tambon.ColWidth(3) = 1000: Grid_Tambon.TextArray(3) = "������ɳ���"
            
            Grid_VILLAGE.ColWidth(0) = 2500 ':   Grid_VILLAGE.TextArray(0) = "���͵Ӻ�"
            Grid_VILLAGE.ColWidth(1) = 2000 ':   Grid_VILLAGE.TextArray(1) = "���������ҹ"
            Grid_VILLAGE.ColWidth(2) = 4250 ':   Grid_VILLAGE.TextArray(2) = "���������ҹ"
            Grid_VILLAGE.ColWidth(3) = 0
            Grid_VILLAGE.ColWidth(4) = 0
         End If
         
         If OpMenu(3).Value Then
            Grid_STREET.ColWidth(0) = 1000 ':   Grid_STREET.TextArray(0) = "���ʷ����"
            Grid_STREET.ColWidth(1) = 2150 ':   Grid_STREET.TextArray(1) = "���"
            
            Grid_SOI.ColWidth(0) = 2500 ':    Grid_SOI.TextArray(0) = "���Ͷ��"
            Grid_SOI.ColWidth(1) = 2500 ':   Grid_SOI.TextArray(1) = "���ʫ�� <��͡>"
            Grid_SOI.ColWidth(2) = 4250 ':   Grid_SOI.TextArray(2) = "���ͫ�� <��͡>"
            Grid_SOI.ColWidth(3) = 0
           End If
          
           If OpMenu(4).Value Then
            Grid_Zone.ColWidth(0) = 3300:    Grid_Zone.TextArray(0) = "���ʷ����ࢵ  <Zone>"
            Grid_Zone.ColWidth(1) = 4300:   Grid_Zone.TextArray(1) = "�����ࢵ����   <Block>"
            End If
           
           If OpMenu(5).Value Then
            GRID_ZONETAX.ColWidth(0) = 800 ':    GRID_ZONETAX.TextArray(0) = "��������"
            GRID_ZONETAX.ColWidth(1) = 1500: GRID_ZONETAX.ColAlignment(1) = 7 ':   GRID_ZONETAX.TextArray(1) = "�Ҥҡ�ҧ (�ҷ)": GRID_ZONETAX.ColAlignment(1) = 7
            GRID_ZONETAX.ColWidth(2) = 1100: GRID_ZONETAX.ColAlignment(2) = 7 ':   GRID_ZONETAX.TextArray(2) = "�ѵ������": GRID_ZONETAX.ColAlignment(2) = 7
          End If
          
          If OpMenu(7).Value Then
            Grid_Business.ColWidth(0) = 1500 ':   Grid_Business.TextArray(0) = "���ʻ������Ԩ���"
            Grid_Business.ColWidth(1) = 11000 ':   Grid_Business.TextArray(1) = "�������Ԩ���"
            
            Grid_Class.ColWidth(0) = 1200:   'Grid_Class.TextArray(0) = "�ӴѺ���"
            Grid_Class.ColWidth(1) = 10000:   'Grid_Class.TextArray(1) = "�ѡɳСԨ���"
            Grid_Class.ColWidth(2) = 1550:   'Grid_Class.TextArray(2) = "��Ҹ�������  ��� : ��"
            Grid_Class.ColWidth(3) = 0
           End If
           
        If OpMenu(9).Value Then
                Grid_change.ColWidth(0) = 1000:   Grid_change.TextArray(0) = "����"
                Grid_change.ColWidth(1) = 11000: Grid_change.TextArray(1) = "��������´�������¹�ŧ"
        End If
        
        If OpMenu(10).Value Then
                Grid_Request.ColWidth(0) = 1000: Grid_Request.TextArray(0) = "����"
                If Opt_Request.Value Then
                        Grid_Request.ColWidth(1) = 11000
                        Grid_Request.TextArray(1) = "��������´"
                ElseIf Opt_Building_Type.Value Then
                        Grid_Request.ColWidth(1) = 11000
                        Grid_Request.TextArray(1) = "�������Ҥ��"
                Else
'                        Grid_Request.Cols = 3
                        Grid_Request.ColWidth(1) = 5500
                        Grid_Request.ColWidth(2) = 5500
                        Grid_Request.TextArray(1) = "����"
                        Grid_Request.TextArray(2) = "���˹�"
                End If
'                Grid_Request.Refresh
        End If
           
        If OpMenu(2).Value Then
                Grid_BuildingType.ColWidth(0) = 1000 ':   Grid_BuildingType.TextArray(0) = "���ʻ�����"
                Grid_BuildingType.ColWidth(1) = 2500 ':   Grid_BuildingType.TextArray(1) = "�������ç���͹"
        End If
End Sub

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
On Error Resume Next
Dim i As Byte
Dim rs As ADODB.Recordset

Set rs = New ADODB.Recordset
If STATE = "ADD" Or STATE = "EDIT" Then
    Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
    Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
    Btn_Search.Enabled = False:     Btn_Refresh.Enabled = False
    GRID_ZONETAX.Enabled = False
     Txt_TYPE_A_RATE1.Locked = False: Txt_TYPE_A_RATE1.BackColor = &H80000005
     Txt_TYPE_A_RATE2.Locked = False: Txt_TYPE_A_RATE2.BackColor = &H80000005
     Txt_TYPE_A_RATE3.Locked = False: Txt_TYPE_A_RATE3.BackColor = &H80000005
     Txt_TYPE_A_RATE4.Locked = False: Txt_TYPE_A_RATE4.BackColor = &H80000005
     Txt_TYPE_B_RATE1.Locked = False: Txt_TYPE_B_RATE1.BackColor = &H80000005
     Txt_TYPE_B_RATE2.Locked = False: Txt_TYPE_B_RATE2.BackColor = &H80000005
     Txt_TYPE_C_RATE1.Locked = False: Txt_TYPE_C_RATE1.BackColor = &H80000005
     Txt_TYPE_C_RATE2.Locked = False: Txt_TYPE_C_RATE2.BackColor = &H80000005
     Txt_TYPE_D_RATE1.Locked = False: Txt_TYPE_D_RATE1.BackColor = &H80000005
     Txt_TYPE_A_AREA.Locked = False: Txt_TYPE_A_AREA.BackColor = &H80000005
     cb_type_asset.Locked = True: Opt_Request.Enabled = False: Opt_Building_Type.Enabled = False
End If
If STATE = "CANCEL" Then
    Lb_Amphoe_Id.Caption = "":       Lb_Tambon_Id.Caption = "": Tx_Tambon_Name.Text = "": Tx_Zipcode.Text = ""
    LB_STREET_ID.Caption = ""
    Txt_Street_Name.Text = ""
    cb_type_asset.Locked = False: Opt_Request.Enabled = True
    Opt_Building_Type.Enabled = True
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Then
    Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
    Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
    Btn_Search.Enabled = True:      Btn_Refresh.Enabled = True
                            For i = 0 To 5
                                     Chk_Static_Baht(i).Enabled = False
                                     Chk_Static_Percent(i).Enabled = False
                                     Chk_Lone_Baht(i).Enabled = False
                                     Chk_Lone_Percent(i).Enabled = False
                                     Txt_Rate_LandUse(i).Locked = True
                                     Txt_Rate_LandLone(i).Locked = True
                                     Txt_Rate_LandLone(i).BackColor = &HEBEBE7
                                     Txt_Rate_LandUse(i).BackColor = &HEBEBE7
                                     
                                     TXT_SIGNBORD_RATE(i).BackColor = &HEBEBE7
                                    TXT_SIGNBORD_RATE_OF_USE(i).BackColor = &HEBEBE7
                                    TXT_SIGNBORD_LOW(i).BackColor = &HEBEBE7
                                    TXT_SIGNBORD_RATE(i).Locked = True
                                    TXT_SIGNBORD_RATE_OF_USE(i).Locked = True
                                    TXT_SIGNBORD_LOW(i).Locked = True
                            Next i
    Tx_Tambon_Name.Locked = True:     Tx_Tambon_Name.BackColor = &HEBEBE7
    Tx_Zipcode.Locked = True: Tx_Zipcode.BackColor = &HEBEBE7
    Tx_Village_Name.Locked = True:        Tx_Village_Name.BackColor = &HEBEBE7
    
    Lb_Village_Id.Caption = "":          Lb_Tambon_Id2.Caption = "":          Tx_Village_Name.Text = ""
 
    Txt_Street_Name.Locked = True: Txt_Street_Name.BackColor = &HEBEBE7
    LB_SOI_ID.Caption = ""
    Txt_SOI.Text = "":       Txt_SOI.Locked = True: Txt_SOI.BackColor = &HEBEBE7
    
    Txt_Zone.Text = "":                                Txt_Zone.Locked = True:                  Txt_Zone.BackColor = &HEBEBE7
    TXT_BLOCK.Text = "":                          TXT_BLOCK.Locked = True:            TXT_BLOCK.BackColor = &HEBEBE7
    
     Txt_Zone_Price.Text = "":                    Txt_Zone_Price.Locked = True:                     Txt_Zone_Price.BackColor = &HEBEBE7
     Txt_Zone_Rate.Text = "":                     Txt_Zone_Rate.Locked = True:                       Txt_Zone_Rate.BackColor = &HEBEBE7
     
     Chk_Zone.Enabled = False
     GRID_ZONETAX.Enabled = True
     
     Txt_Business_Type.Text = "": Txt_Business_Type.Locked = True: Txt_Business_Type.BackColor = &HEBEBE7
     lb_Class_Order.Caption = "": lb_Class_Order.BackColor = &HEBEBE7
     Txt_Class_Details.Text = "": Txt_Class_Details.Locked = True: Txt_Class_Details.BackColor = &HEBEBE7
     Txt_Class_Price.Text = "": Txt_Class_Price.Locked = True: Txt_Class_Price.BackColor = &HEBEBE7
             
     
        Txt_BuildingType.Text = "": Txt_BuildingType.Locked = True: Txt_BuildingType.BackColor = &HEBEBE7
        Lb_BuildingType_ID.Caption = ""
     
        Txt_TYPE_A_RATE1.Locked = True: Txt_TYPE_A_RATE1.BackColor = &HEBEBE7
        Txt_TYPE_A_RATE2.Locked = True: Txt_TYPE_A_RATE2.BackColor = &HEBEBE7
        Txt_TYPE_A_RATE3.Locked = True: Txt_TYPE_A_RATE3.BackColor = &HEBEBE7
        Txt_TYPE_A_RATE4.Locked = True: Txt_TYPE_A_RATE4.BackColor = &HEBEBE7
        Txt_TYPE_B_RATE1.Locked = True: Txt_TYPE_B_RATE1.BackColor = &HEBEBE7
        Txt_TYPE_B_RATE2.Locked = True: Txt_TYPE_B_RATE2.BackColor = &HEBEBE7
        Txt_TYPE_C_RATE1.Locked = True: Txt_TYPE_C_RATE1.BackColor = &HEBEBE7
        Txt_TYPE_C_RATE2.Locked = True: Txt_TYPE_C_RATE2.BackColor = &HEBEBE7
        Txt_TYPE_D_RATE1.Locked = True: Txt_TYPE_D_RATE1.BackColor = &HEBEBE7
        Txt_TYPE_A_AREA.Locked = True: Txt_TYPE_A_AREA.BackColor = &HEBEBE7
        Txt_BuildingType.Text = "": Txt_BuildingType.Locked = True: Txt_BuildingType.BackColor = &HEBEBE7
        Lb_BuildingType_ID.Caption = ""
      
      Txt_Due_Accept0.Locked = True: Txt_Due_Accept0.BackColor = &HEBEBE7
      Txt_Due_1Month.Locked = True: Txt_Due_1Month.BackColor = &HEBEBE7
      Txt_Due_2Month.Locked = True: Txt_Due_2Month.BackColor = &HEBEBE7
      Txt_Due_3Month.Locked = True: Txt_Due_3Month.BackColor = &HEBEBE7
      Txt_Due_4Month.Locked = True: Txt_Due_4Month.BackColor = &HEBEBE7
      Txt_Due_Due_Appeal0.Locked = True: Txt_Due_Due_Appeal0.BackColor = &HEBEBE7
      Txt_Tax_YPerM.Locked = True: Txt_Tax_YPerM.BackColor = &HEBEBE7
      Tax_MPerD.Locked = True: Tax_MPerD.BackColor = &HEBEBE7
      Txt_Tax_Machine.Locked = True: Txt_Tax_Machine.BackColor = &HEBEBE7
      
      Txt_Due_Accept1.Locked = True: Txt_Due_Accept1.BackColor = &HEBEBE7
      Txt_Tax_OutSchedule1.Locked = True: Txt_Tax_OutSchedule1.BackColor = &HEBEBE7
      Txt_Tax_NotFully.Locked = True: Txt_Tax_NotFully.BackColor = &HEBEBE7
      Txt_Tax_OutTime1.Locked = True: Txt_Tax_OutTime1.BackColor = &HEBEBE7
      Txt_Due_Due_Appeal1.Locked = True: Txt_Due_Due_Appeal1.BackColor = &HEBEBE7
        Txt_B_Rai.Locked = True: Txt_B_Rai.BackColor = &HEBEBE7
        Txt_B_Pot.Locked = True: Txt_B_Pot.BackColor = &HEBEBE7
        Txt_B_Va.Locked = True: Txt_B_Va.BackColor = &HEBEBE7
      
      Txt_Due_Accept2.Locked = True: Txt_Due_Accept2.BackColor = &HEBEBE7
      Txt_Tax_OutSchedule2.Locked = True: Txt_Tax_OutSchedule2.BackColor = &HEBEBE7
      Txt_Tax_NotFully2.Locked = True: Txt_Tax_NotFully2.BackColor = &HEBEBE7
      Txt_Tax_OutTime2.Locked = True: Txt_Tax_OutTime2.BackColor = &HEBEBE7
      Txt_Due_Due_Appeal2.Locked = True: Txt_Due_Due_Appeal2.BackColor = &HEBEBE7
      Txt_LM_Details.Text = "":     Txt_LM_Details.Locked = True: Txt_LM_Details.BackColor = &HEBEBE7
      Combo1.BackColor = &HEBEBE7
      txt_change.Text = "": txt_change.Locked = True: txt_change.BackColor = &HEBEBE7
      lb_change_id.Caption = ""
      cb_type_asset.Locked = False
      
      txt_Request.Text = "": txt_Request.Locked = True: txt_Request.BackColor = &HEBEBE7
      lb_Request_id.Caption = ""
      Opt_Request.Enabled = True
      Opt_Building_Type.Enabled = True
      If Opt_Officer.Value Then
            txt_Position.Text = "": txt_Position.BackColor = &HEBEBE7
      End If
End If

If UCase(Perpose) = "PERPOSE" Then
                            If OpMenu(1).Value = True Then
                                If Op_Tambon.Value = True Then
                                                      Lb_Amphoe_Id.Caption = Grid_Tambon.TextMatrix(1, 2)
                                                      Tx_Tambon_Name.Locked = False
                                                      Tx_Tambon_Name.BackColor = &HFFFFFF
                                                      Tx_Zipcode.Locked = False
                                                      Tx_Zipcode.BackColor = &HFFFFFF
                                                   If STATE = UCase("ADD") Then
                                                       If LenB(Trim$(Grid_Tambon.TextMatrix(1, 1))) > 0 Then
                                                         Lb_Tambon_Id.Caption = Right$(Grid_Tambon.TextMatrix(Grid_Tambon.Rows - 1, 0) + 1, 2)
                                                         Tx_Tambon_Name.Text = ""
                                                         Tx_Zipcode.Text = ""
                                                      Else
                                                         Lb_Tambon_Id.Caption = iniTambon
                                                         Lb_Amphoe_Id.Caption = iniAmphoe
                                                      End If
                                                   End If
                                                   If STATE = UCase("EDIT") Then
                                                         Grid_Tambon.SetFocus
                                                          Lb_Tambon_Id.Caption = Right$(Grid_Tambon.TextMatrix(Grid_Tambon.Row, 0), 2)
                                                          Tx_Tambon_Name.Text = Grid_Tambon.TextMatrix(Grid_Tambon.Row, 1)
                                                          Tx_Zipcode.Text = Grid_Tambon.TextMatrix(Grid_Tambon.Row, 3)
                                                   End If
                                                          Tx_Tambon_Name.SetFocus
                                End If
                                
                                If Op_Village.Value = True Then
                                        Tx_Village_Name.Locked = False
                                        Tx_Village_Name.BackColor = &HFFFFFF
                                                   If STATE = UCase("ADD") Then
                                                         Tx_Village_Name.Text = ""
                                                         If LenB(Trim$(Grid_VILLAGE.TextMatrix(1, 1))) > 0 Then
                                                              Lb_Village_Id.Caption = Right$(Grid_VILLAGE.TextMatrix(Grid_VILLAGE.Rows - 1, 1) + 1, 2)
                                                              Lb_Tambon_Id2.Caption = Left$(Grid_VILLAGE.TextMatrix(Grid_VILLAGE.Row, 1), 6)
                                                         Else
                                                              Lb_Tambon_Id2.Caption = Grid_Tambon.TextMatrix(Grid_Tambon.Row, Grid_Tambon.Col)
                                                              Lb_Village_Id.Caption = "01"
                                                         End If
                                                   End If
                                                   If STATE = UCase("EDIT") Then
                                                        Grid_VILLAGE.SetFocus
                                                   End If
                                                         Tx_Village_Name.SetFocus
                                  End If
                            End If
                            
                             If OpMenu(3).Value = True Then
                                                     If Chk_Street.Value = True Then
                                                          Txt_Street_Name.Locked = False
                                                          Txt_Street_Name.BackColor = &HFFFFFF
                                                          Txt_Street_Name.SetFocus
                                                      End If
                                                      If Op_Soi.Value = True Then
                                                          Txt_SOI.Locked = False:        Txt_SOI.BackColor = &HFFFFFF
                                                          Txt_SOI.SetFocus
                                                      End If
                                                      
                                                   If STATE = UCase("ADD") Then
                                                       If Chk_Street.Value = True Then
                                                                     Txt_Street_Name.Text = ""
                                                                     LB_STREET_ID.Caption = RunAutoNumber("STREET", "STREET_ID", "RD0001", False)
                                                      End If
                                                      If Op_Soi.Value = True Then
                                                                   Txt_SOI.Text = ""
                                                                   LB_SOI_ID.Caption = RunAutoNumber("SOI", "SOI_ID", LB_STREET_ID.Caption & "-001", False, " AND STREET_ID = '" & LB_STREET_ID.Caption & "'")
                                                      End If
                                                   End If
                                                   If STATE = UCase("EDIT") Then
                                                         If Chk_Street.Value = True Then
                                                              With Grid_STREET
                                                                     LB_STREET_ID.Caption = Trim$(.TextMatrix(.Row, 0))
                                                                     Txt_Street_Name.Text = .TextMatrix(.Row, 1)
                                                                  End With
                                                           End If
                                                           If Op_Soi.Value = True Then
                                                              With Grid_SOI
                                                                     LB_SOI_ID.Caption = Trim$(.TextMatrix(.Row, 1))
                                                                     Txt_SOI.Text = .TextMatrix(.Row, 2)
                                                                  End With
                                                           End If
                                                   End If
                             End If
 
                              If OpMenu(4).Value = True Then
                                    If Op_ZONE.Value = True Then
                                        Txt_Zone.Locked = False
                                        Txt_Zone.BackColor = &HFFFFFF
                                        Chk_Zone.Enabled = True
                                        Chk_Zone.Value = Checked
                                     If STATE = UCase("ADD") Then
                                           Txt_Zone.Text = ""
                                     End If
                                         If STATE = UCase("EDIT") Then
                                             Txt_Zone.Text = Grid_Zone.TextMatrix(Grid_Zone.Row, 0)
                                         End If
                                             Txt_Zone.SetFocus
                                     End If
                                     If Op_BLOCK.Value = True Then
                                         TXT_BLOCK.Locked = False
                                         TXT_BLOCK.BackColor = &HFFFFFF
                                     If STATE = UCase("ADD") Then
                                           TXT_BLOCK.Text = ""
                                     End If
                                         TXT_BLOCK.SetFocus
                                     End If
                              End If
                              If OpMenu(2).Value = True Then
                                 If STATE = UCase("ADD") Then
                                           
                                End If
                                     If STATE = UCase("EDIT") Then
                                         End If
                              End If
                              
                              If OpMenu(5).Value = True Then
                                          If Op_RateTax.Value Then
                                                                        Txt_Zone_Price.BackColor = &HFFFFFF: Txt_Zone_Price.Locked = False
                                                                        Txt_Zone_Rate.BackColor = &HFFFFFF: Txt_Zone_Rate.Locked = False
                                                              If STATE = UCase("ADD") Then
                                                                        Txt_Zone_Price.Text = "":  Txt_Zone_Price.Text = ""
                                                                        Txt_Zone_Rate.Text = "":  Txt_Zone_Rate.Text = ""
                                                             End If
                                                             If STATE = UCase("EDIT") Then
                                                             End If
                                                                          Txt_Zone_Price.SetFocus
                                           End If
                                            
                                            If Op_Land_Use.Value Then
                                                     If STATE = UCase("EDIT") Then
                                                           Combo1.BackColor = &HFFFFFF
                                                         For i = 0 To 5
                                                                    Chk_Static_Baht(i).Enabled = True
                                                                    Chk_Static_Percent(i).Enabled = True
                                                                    Chk_Lone_Baht(i).Enabled = True
                                                                    Chk_Lone_Percent(i).Enabled = True
                                                                    Txt_Rate_LandUse(i).Locked = False
                                                                    Txt_Rate_LandLone(i).Locked = False
                                                                    Txt_Rate_LandLone(i).BackColor = &HFFFFFF
                                                                    Txt_Rate_LandUse(i).BackColor = &HFFFFFF
                                                          Next i
                                                      End If
                                             End If
                                              If Op_LM.Value Then
                                                              If STATE = UCase("ADD") Then
                                                                 Txt_LM_Details.Text = ""
                                                                 Txt_LM_Details.Locked = False
                                                                 Txt_LM_Details.BackColor = &HFFFFFF
                                                              End If
                                                              If STATE = UCase("EDIT") Then
                                                                 Txt_LM_Details.Locked = False
                                                                 Txt_LM_Details.BackColor = &HFFFFFF
                                                              End If
                                                                Txt_LM_Details.SetFocus
                                              End If
                                 End If
                                    
                                    If OpMenu(7).Value = True Then
                                            If Op_Business.Value Then
                                                     Txt_Business_Type.Locked = False
                                                      Txt_Business_Type.BackColor = &HFFFFFF
                                                 If STATE = UCase("ADD") Then
                                                      Txt_Business_Type.Text = ""
                                                      Lb_Business.Caption = RunAutoNumber("BUSINESS_TYPE", "BUSINESS_ID", "001", False)
                                                 End If
                                                       Txt_Business_Type.SetFocus
                                            End If
                                         
                                            If Op_Class.Value Then
                                                      Txt_Class_Details.Locked = False
                                                      Txt_Class_Details.BackColor = &HFFFFFF
                                                      Txt_Class_Price.Locked = False
                                                      Txt_Class_Price.BackColor = &HFFFFFF
                                                 If STATE = UCase("ADD") Then
                                                    Call SET_QUERY("SELECT CLASS_ID FROM CLASS_DETAILS WHERE BUSINESS_ID='" & Lb_Business.Caption & "' ORDER BY CLASS_ID ASC", rs)
                                                    If rs.RecordCount > 0 Then
                                                            rs.MoveLast
                                                            lb_Class_Order.Caption = Format$(Right$(rs.Fields(0).Value, 3) + 1, "000")
                                                    Else
                                                            lb_Class_Order.Caption = "001"
                                                    End If
'                                                      lb_Class_Order.Locked = False
'                                                      lb_Class_Order.BackColor = &HFFFFFF
'                                                      lb_Class_Order.Text = Empty
                                                      Txt_Class_Details.Text = ""
                                                      Txt_Class_Price.Text = ""
                                                      Txt_Class_Details.SetFocus
                                                 End If
                                                   If STATE = UCase("EDIT") Then
                                                       Txt_Class_Details.SetFocus
                                                   End If
                                            End If
                                     End If
                                    
                                    If Op_BuildingRate.Value Then
                                        End If
                                         
                                         If Op_BuildingType.Value Then
                                                Txt_BuildingType.Locked = False
                                                Txt_BuildingType.BackColor = &HFFFFFF
                                            If STATE = UCase("ADD") Then
                                                Txt_BuildingType.Text = ""
                                                Lb_BuildingType_ID.Caption = RunAutoNumber("BUILDING_SHAPE", "BUILDING_SHAPE_ID", "01", False)
                                            End If
                                                Txt_BuildingType.SetFocus
                                         End If
                                                                                  
                                         If Op_Cfg_Building.Value Then
                                                      If STATE = UCase("EDIT") Then
                                                               Txt_Due_Accept0.Locked = False: Txt_Due_Accept0.BackColor = &HFFFFFF
                                                                Txt_Due_1Month.Locked = False: Txt_Due_1Month.BackColor = &HFFFFFF
                                                                Txt_Due_2Month.Locked = False: Txt_Due_2Month.BackColor = &HFFFFFF
                                                                Txt_Due_3Month.Locked = False: Txt_Due_3Month.BackColor = &HFFFFFF
                                                                Txt_Due_4Month.Locked = False: Txt_Due_4Month.BackColor = &HFFFFFF
                                                                Txt_Due_Due_Appeal0.Locked = False: Txt_Due_Due_Appeal0.BackColor = &HFFFFFF
                                                                Txt_Tax_YPerM.Locked = False: Txt_Tax_YPerM.BackColor = &HFFFFFF
                                                                Tax_MPerD.Locked = False: Tax_MPerD.BackColor = &HFFFFFF
                                                                Txt_Tax_Machine.Locked = False: Txt_Tax_Machine.BackColor = &HFFFFFF
                                                     End If
                                                               Txt_Due_Accept0.SetFocus
                                         End If
                                        If Op_Cfg_Land.Value Then
                                              If STATE = UCase("EDIT") Then
                                                        Txt_Due_Accept1.Locked = False: Txt_Due_Accept1.BackColor = &HFFFFFF
                                                        Txt_Tax_OutSchedule1.Locked = False: Txt_Tax_OutSchedule1.BackColor = &HFFFFFF
                                                        Txt_Tax_NotFully.Locked = False: Txt_Tax_NotFully.BackColor = &HFFFFFF
                                                        Txt_Tax_OutTime1.Locked = False: Txt_Tax_OutTime1.BackColor = &HFFFFFF
                                                        Txt_Due_Due_Appeal1.Locked = False: Txt_Due_Due_Appeal1.BackColor = &HFFFFFF
                                                        Txt_B_Rai.Locked = False: Txt_B_Rai.BackColor = &HFFFFFF
                                                        Txt_B_Pot.Locked = False: Txt_B_Pot.BackColor = &HFFFFFF
                                                        Txt_B_Va.Locked = False: Txt_B_Va.BackColor = &HFFFFFF
                                              End If
                                                        Txt_Due_Accept1.SetFocus
                                        End If
                                        
                                       If Op_Cfg_Signbord.Value Then
                                           If STATE = UCase("EDIT") Then
                                                 Txt_Due_Accept2.Locked = False: Txt_Due_Accept2.BackColor = &HFFFFFF
                                                Txt_Tax_OutSchedule2.Locked = False: Txt_Tax_OutSchedule2.BackColor = &HFFFFFF
                                                Txt_Tax_NotFully2.Locked = False: Txt_Tax_NotFully2.BackColor = &HFFFFFF
                                                Txt_Tax_OutTime2.Locked = False: Txt_Tax_OutTime2.BackColor = &HFFFFFF
                                                Txt_Due_Due_Appeal2.Locked = False: Txt_Due_Due_Appeal2.BackColor = &HFFFFFF
                                           End If
                                                Txt_Due_Accept2.SetFocus
                                       End If
                                       If OpMenu(9).Value Then
                                            txt_change.Locked = False
                                            txt_change.BackColor = &HFFFFFF
                                            If STATE = UCase("ADD") Then
                                                Set rs = Globle_Connective.Execute("exec sp_search_change_pettion_building " & cb_type_asset.ListIndex, , adCmdUnknown)
                                                If rs.RecordCount > 0 Then
                                                        rs.MoveLast
                                                        lb_change_id.Caption = Format$(Right$(rs.Fields(0).Value, 2) + 1, "000")
                                                Else
                                                        lb_change_id.Caption = "000"
                                                End If
                                                 txt_change.Text = ""
                                                 txt_change.SetFocus
                                            End If
                                              If STATE = UCase("EDIT") Then
                                                    If lb_change_id.Caption = "" Then Call Btn_Cancel_Click
                                                  txt_change.SetFocus
                                              End If
                                       End If
                                       
                                       If OpMenu(10).Value Then
                                            txt_Request.Locked = False
                                            txt_Request.BackColor = &HFFFFFF
                                            If STATE = UCase("ADD") Then
                                                    If Opt_Request.Value Then
                                                            Set rs = Globle_Connective.Execute("exec sp_search_change_pettion_building 4", , adCmdUnknown)
                                                    ElseIf Opt_Building_Type.Value Then
                                                            Set rs = Globle_Connective.Execute("exec sp_search_change_pettion_building 5", , adCmdUnknown)
'                                                            Call SET_QUERY("SELECT BUILDING_TYPE_ID FROM BUILDING_TYPE ORDER BY BUILDING_TYPE_ID ASC", Rs)
                                                    Else
                                                            txt_Position.BackColor = &HFFFFFF
                                                            Set rs = Globle_Connective.Execute("exec sp_search_change_pettion_building 6", , adCmdUnknown)
                                                    End If
                                                    If rs.RecordCount > 0 Then
                                                            rs.MoveLast
                                                            lb_Request_id.Caption = Format$(Right$(Trim$(rs.Fields(0).Value), 3) + 1, "0000")
                                                    Else
                                                            lb_Request_id.Caption = "0001"
                                                    End If
                                                    txt_Request.Text = ""
                                                    txt_Request.SetFocus
                                            End If
                                            If STATE = UCase("EDIT") Then
                                                txt_Request.SetFocus
                                            End If
                                       End If
End If
                          
                                             If OpMenu(6).Value = True Then
                                                    If STATE = UCase("EDIT") Then
                                                         For i = 1 To 4
                                                                    TXT_SIGNBORD_RATE_OF_USE(i).Locked = False
                                                                    TXT_SIGNBORD_RATE(i).Locked = False
                                                                    TXT_SIGNBORD_LOW(i).Locked = False
                                                                    
                                                                    TXT_SIGNBORD_RATE(i).BackColor = &HFFFFFF
                                                                    TXT_SIGNBORD_RATE_OF_USE(i).BackColor = &HFFFFFF
                                                                    TXT_SIGNBORD_LOW(i).BackColor = &HFFFFFF
                                                          Next i
                                                   End If
                                         End If
        Set rs = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
'Dim sql_txt As String
Dim j As Byte, k As Byte, i As Byte
Dim rs As ADODB.Recordset

Set rs = New ADODB.Recordset
' sql_txt = Empty
  
  If OpMenu(1).Value = True Then   ' �Ӻ�
              If Op_Tambon.Value = True Then
                        Globle_Connective.Execute "exec sp_manage_tambon '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "','" & Tx_Tambon_Name.Text & "','" & Trim$(Lb_Amphoe_Id.Caption) & "','" & Trim$(Tx_Zipcode.Text) & "','" & STATE & "'", , adCmdUnknown
'                                If STATE = "POST" Then
'                                        Globle_Connective.Execute "exec sp_insert_tambon '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "','" & Tx_Tambon_Name.Text & "','" & Trim$(Lb_Amphoe_Id.Caption) & "'", , adCmdUnknown
''                                        sql_txt = "INSERT INTO TAMBON (TAMBON_ID, TAMBON_NAME, AMPHOE_ID) VALUES ('" & _
''                                                         Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "','" & Tx_Tambon_Name.Text & "','" & Trim$(Lb_Amphoe_Id.Caption) & "')"
'                                ElseIf STATE = "EDIT" Then
'                                        Globle_Connective.Execute "exec sp_update_tambon '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "','" & Trim$(Tx_Tambon_Name.Text) & "'"
''                                        sql_txt = "UPDATE TAMBON SET   TAMBON_NAME = '" & Trim$(Tx_Tambon_Name.Text) & "'" & _
''                                                         " WHERE TAMBON_ID = '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "'"
'                                ElseIf STATE = "DEL" Then
'                                        Globle_Connective.Execute "exec sp_delete_tambon '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "'"
''                                        sql_txt = "DELETE FROM TAMBON " & _
''                                                         " WHERE TAMBON_ID = '" & Trim$(Lb_Amphoe_Id.Caption) & Trim$(Lb_Tambon_Id.Caption) & "'"
'                                End If
                        sFlag = False
              End If
              If Op_Village.Value = True Then   ' �����ҹ
                        Globle_Connective.Execute "exec sp_manage_village '" & Trim$(Lb_Tambon_Id2.Caption) & Trim$(Lb_Village_Id.Caption) & "','" & Tx_Village_Name.Text & "','" & Trim$(Lb_Tambon_Id2.Caption) & "','" & STATE & "'", , adCmdUnknown
'                                If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO VILLAGE (VILLAGE_ID, VILLAGE_NAME, TAMBON_ID) VALUES ('" & _
'                                                         Trim$(Lb_Tambon_Id2.Caption) & Trim$(Lb_Village_Id.Caption) & "','" & Tx_Village_Name.Text & "','" & Trim$(Lb_Tambon_Id2.Caption) & "')"
'                                End If
'                                If STATE = "EDIT" Then
'                                        sql_txt = "UPDATE VILLAGE SET   VILLAGE_NAME = '" & Trim$(Tx_Village_Name.Text) & "'" & _
'                                                         " WHERE VILLAGE_ID = '" & Trim$(Lb_Tambon_Id2.Caption) & Trim$(Lb_Village_Id.Caption) & "'"
'                                End If
'                                If STATE = "DEL" Then
'                                        sql_txt = "DELETE FROM VILLAGE" & _
'                                                         " WHERE VILLAGE_ID = '" & Trim$(Lb_Tambon_Id2.Caption) & Trim$(Lb_Village_Id.Caption) & "'"
'                                End If
            End If
End If

If OpMenu(3).Value = True Then
                            If Chk_Street.Value = True Then  ' ���
                                    Globle_Connective.Execute "exec sp_manage_street '" & Trim$(LB_STREET_ID.Caption) & "','" & IIf(Len(Txt_Street_Name.Text) = 0, "-", Txt_Street_Name.Text) & "','" & Trim$(LB_STREET_ID.Caption) & "-000" & "','" & STATE & "'", , adCmdUnknown
'                                     If STATE = "POST" Then
'                                          sql_txt = "INSERT INTO STREET (STREET_ID, STREET_NAME) VALUES ('" & _
'                                                             Trim$(LB_STREET_ID.Caption) & "','" & IIf(Len(Txt_Street_Name.Text) = 0, "-", Txt_Street_Name.Text) & "')"
'                                                        Call SET_Execute(sql_txt)
'                                          sql_txt = "INSERT INTO SOI (SOI_ID, SOI_NAME , STREET_ID) VALUES ('" & _
'                                                            Trim$(LB_STREET_ID.Caption) & "-000','' ,'" & Trim$(LB_STREET_ID.Caption) & "')"
'                                                        Call SET_Execute(sql_txt)
'                                                        sql_txt = Empty
'                                      End If
'                                      If STATE = "EDIT" Then
'                                            sql_txt = "UPDATE STREET SET  STREET_NAME = '" & Trim$(Txt_Street_Name.Text) & "'" & _
'                                                             " WHERE STREET_ID  = '" & Trim$(LB_STREET_ID.Caption) & "'"
'                                      End If
'                                      If STATE = "DEL" Then
'                                            sql_txt = "DELETE FROM SOI " & _
'                                                             " WHERE STREET_ID  = '" & Trim$(LB_STREET_ID.Caption) & "'"
'                                                           Call SET_Execute(sql_txt)
'                                            sql_txt = "DELETE FROM STREET " & _
'                                                             " WHERE STREET_ID  = '" & Trim$(LB_STREET_ID.Caption) & "'"
'                                                          Call SET_Execute(sql_txt)
'                                                           sql_txt = Empty
'                                      End If
                                End If
                            If Op_Soi.Value = True Then   '  ���
                                    Globle_Connective.Execute "exec sp_manage_soi '" & Trim$(LB_SOI_ID.Caption) & "','" & Trim$(Txt_SOI.Text) & "','" & Trim$(LB_STREET_ID.Caption) & "','" & STATE & "'", , adCmdUnknown
'                                  If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO SOI (SOI_ID, SOI_NAME , STREET_ID) VALUES ('" & _
'                                                         Trim$(LB_SOI_ID.Caption) & "','" & Trim$(Txt_SOI.Text) & "','" & Trim$(LB_STREET_ID.Caption) & "')"
'                                  End If
'                                  If STATE = "EDIT" Then
'                                        sql_txt = "UPDATE SOI SET  SOI_NAME = '" & Trim$(Txt_SOI.Text) & "'" & _
'                                                         " WHERE SOI_ID  = '" & Trim$(LB_SOI_ID.Caption) & "'"
'                                  End If
'                                  If STATE = "DEL" Then
'                                        sql_txt = "DELETE FROM SOI " & _
'                                                         " WHERE SOI_ID  = '" & Trim$(LB_SOI_ID.Caption) & "'"
'                                  End If
                            End If
End If

  
  If OpMenu(4).Value = True Then
                          If Op_ZONE.Value = True Then   ' Zone ⫹
                                    If Chk_Zone.Value = Checked Then
                                            If STATE = "POST" Then
                                                    If Cmb_Zone.ListCount <= 0 Then
                                                            k = 0
                                                    Else
                                                            k = Cmb_Zone.List(Cmb_Zone.ListCount - 1)
                                                    End If
                                                    For j = 1 To CByte(Txt_Zone.Text)
                                                            k = k + 1
        '                                                    sql_txt = "INSERT INTO LANDZONE ( ZONE_ID,ZONE_BLOCK ) VALUES ('" & Format$(k, "#0#") & "','" & Format$(k, "#0#") & "A" & "')"
                                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(k, "#0#"), secattr)  '  ���ҧ Folder Runtime
                                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(k, "#0#") & "\" & "Block_" & "A", secattr)  '  ���ҧ Folder Runtime
                                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(k, "#0#") & "\" & "Block_" & "A" & "\Building", secattr)  '  ���ҧ Folder Runtime
                                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(k, "#0#") & "\" & "Block_" & "A" & "\Land", secattr)  '  ���ҧ Folder Runtime
                                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(k, "#0#") & "\" & "Block_" & "A" & "\Signboard", secattr) '  ���ҧ Folder Runtime
                                                            If j <= CByte(Txt_Zone.Text) Then Globle_Connective.Execute "exec sp_manage_zone '" & Format$(k, "#0#") & "','" & Format$(k, "#0#") & "A" & "','" & STATE & "'", , adCmdUnknown
                                                    Next j
                                            ElseIf STATE = "DEL" Then
        '                                               sql_txt = "DELETE LANDZONE  WHERE  ZONE_ID = '" & Cmb_Zone.Text & "'"
                                                    Globle_Connective.Execute "exec sp_manage_zone '" & Cmb_Zone.Text & "','','" & STATE & "'", , adCmdUnknown
                                                    If ListCurrent > 0 Then
                                                            ListCurrent = ListCurrent - 1
                                                    End If
                                            End If
                                    Else
                                            If STATE = "POST" Then
                                                    Globle_Connective.Execute "exec sp_manage_zone '" & Format$(Txt_Zone.Text, "00") & "','" & Format$(Txt_Zone.Text, "00") & "A" & "','" & STATE & "'", , adCmdUnknown
                                                    Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(Txt_Zone.Text, "00"), secattr)  '  ���ҧ Folder Runtime
                                                    Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(Txt_Zone.Text, "00") & "\" & "Block_" & "A", secattr)  '  ���ҧ Folder Runtime
                                                    Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(Txt_Zone.Text, "00") & "\" & "Block_" & "A" & "\Building", secattr)  '  ���ҧ Folder Runtime
                                                    Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(Txt_Zone.Text, "00") & "\" & "Block_" & "A" & "\Land", secattr)  '  ���ҧ Folder Runtime
                                                    Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Format$(Txt_Zone.Text, "00") & "\" & "Block_" & "A" & "\Signboard", secattr) '  ���ҧ Folder Runtime
                                            ElseIf STATE = "DEL" Then
                                                    Globle_Connective.Execute "exec sp_manage_zone '" & Cmb_Zone.Text & "','','" & STATE & "'", , adCmdUnknown
                                                    If ListCurrent > 0 Then
                                                            ListCurrent = ListCurrent - 1
                                                    End If
                                            End If
                                    End If
                         End If
                         
                         If Op_BLOCK.Value = True Then  'Block
                            If STATE = "POST" Then
                                If LenB(Trim$(Grid_Zone.TextMatrix(Grid_Zone.Rows - 1, 1))) > 0 Then
                                        k = Asc(Right$(Grid_Zone.TextMatrix(Grid_Zone.Rows - 1, 1), 1))  'get �����ʡբͧ����ѡ�â���ش���˹��ش���¢ͧ��Դ
'                                        If k = 90 Then
'                                                   Exit Sub
'                                        End If
'                                Else
                                        Set rs = Globle_Connective.Execute("exec sp_check_block '" & Cmb_Zone.Text & "'", , adCmdUnknown)
                                        With rs
                                                If rs.RecordCount > 0 Then
                                                        .MoveFirst
                                                        j = 1
                                                        Do While Not .EOF
                                                                Globle_Connective.Execute "exec sp_manage_zone '" & Cmb_Zone.Text & "','" & Cmb_Zone.Text & .Fields(0).Value & "','" & STATE & "'", , adCmdUnknown
                                                                Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & .Fields(0).Value, secattr)
                                                                Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & .Fields(0).Value & "\Building", secattr)
                                                                Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & .Fields(0).Value & "\Land", secattr)
                                                                Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & .Fields(0).Value & "\Signboard", secattr)
                                                                If j = CByte(TXT_BLOCK.Text) Then Exit Do
                                                                j = j + 1
                                                                .MoveNext
                                                        Loop
                                                End If
                                        End With
'                                        sql_txt = "DELETE LANDZONE WHERE ZONE_ID  = '" & Cmb_Zone.Text & "'"
'                                        Call SET_Execute(sql_txt)
'                                        k = 64    '  64= ����ѡ�� A
                                End If
'                                For j = 1 To CByte(TXT_BLOCK.Text)
'                                       k = k + 1
'                                       If (k <= 90 And k >= 64) Then
'                                            Globle_Connective.Execute "exec sp_manage_zone '" & Cmb_Zone.Text & "','" & Cmb_Zone.Text & Chr$(k) & "'", , adCmdUnknown
'                                            sql_txt = "INSERT INTO LANDZONE (ZONE_BLOCK ,ZONE_ID) VALUES ('" & Cmb_Zone.Text & Chr$(k) & "','" & Cmb_Zone.Text & "')"
'                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & Chr$(k), secattr)
'                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & Chr$(k) & "\Building", secattr)
'                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & Chr$(k) & "\Land", secattr)
'                                            Call CreateDirectory(Trim$(iniPath_Picture) & "\Zone" & Cmb_Zone.Text & "\" & "Block_" & Chr$(k) & "\Signboard", secattr)
'                                            If j < CByte(TXT_BLOCK.Text) Then
'                                                     Call SET_Execute(sql_txt)
'                                            End If
'                                       End If
'                                Next j
'                                sql_txt = Empty
'                             ElseIf STATE = "EDIT" Then
                             ElseIf STATE = "DEL" Then
                                        If Grid_Zone.Rows = 2 Then ListCurrent = 0
                                        Globle_Connective.Execute "exec sp_del_block '" & Grid_Zone.TextMatrix(Grid_Zone.Row, 1) & "'"
'                                        sql_txt = "DELETE FROM LANDZONE WHERE ZONE_BLOCK = '" & Grid_Zone.TextMatrix(Grid_Zone.Row, 1) & "'"
                             End If
                         End If
  End If
  
  If OpMenu(5).Value = True Then  '�ѵ�����պ��ا��ͧ���
                    If Op_RateTax.Value = True Then
                            If STATE = "POST" Then
                                    If GBQueryZoneTax.RecordCount > 0 Then
                                            GBQueryZoneTax.MoveFirst
                                            GBQueryZoneTax.Find " �������� = '" & Combo2.Text & "'", , adSearchForward
                                    End If
                                    If GBQueryZoneTax.AbsolutePage > 0 Then
                                            Globle_Connective.Execute "exec sp_update_zonetax " & CCur(Txt_Zone_Price.Text) & "," & CCur(Txt_Zone_Rate.Text) & ",'" & Combo2.Text & "'", , adCmdUnknown
'                                        sql_txt = " UPDATE ZONE_TAX  SET PRICE_RATE = " & CDbl(Txt_Zone_Price.Text) & " , TAX_RATE  = " & CDbl(Txt_Zone_Rate.Text) & _
'                                                            " WHERE ZONETAX_ID  = '" & combo2.Text & "'"
                                    Else
                                            Globle_Connective.Execute "exec sp_insert_zonetax " & CCur(Txt_Zone_Price.Text) & "," & CCur(Txt_Zone_Rate.Text) & ",'" & Combo2.Text & "'", , adCmdUnknown
'                                        sql_txt = " INSERT INTO ZONE_TAX (ZONETAX_ID , PRICE_RATE , TAX_RATE ) " & _
'                                                          " VALUES ('" & combo2.Text & "'," & CDbl(Txt_Zone_Price.Text) & "," & CDbl(Txt_Zone_Rate.Text) & ")"
'                                        Call SET_Execute(sql_txt)
                                            For j = 1 To 4
                                                    Globle_Connective.Execute "exec sp_insert_landuse '" & "LM-" & Format(j, "0#") & "','" & Combo2.Text & "'", , adCmdUnknown
'                                                                    sql_txt = "INSERT INTO LANDUSE (LM_ID,ZONETAX_ID) " & _
'                                                                                           " VALUES( 'LM-" & Format(j, "0#") & "','" & combo2.Text & "')"
'                                                                     Call SET_Execute(sql_txt)
                                            Next j
                                            If GBQueryLandEmployment.RecordCount > 0 Then
                                                    GBQueryLandEmployment.MoveFirst
                                                    Do While Not GBQueryLandEmployment.EOF
                                                            Globle_Connective.Execute "exec sp_insert_landuse '" & GBQueryLandEmployment.Fields("LM_ID").Value & "','" & Combo2.Text & "'", , adCmdUnknown
'                                                                            sql_txt = "INSERT INTO LANDUSE (LM_ID,ZONETAX_ID) " & _
'                                                                                             " VALUES( '" & GBQueryLandEmployment.Fields("LM_ID").Value & "','" & combo2.Text & "')"
'                                                                                                Call SET_Execute(sql_txt)
                                                            GBQueryLandEmployment.MoveNext
                                                    Loop
                                            End If
'                                        sql_txt = Empty
                                    End If
                            ElseIf STATE = "EDIT" Then
                                        Globle_Connective.Execute "exec sp_update_zonetax " & CCur(Txt_Zone_Price.Text) & "," & CCur(Txt_Zone_Rate.Text) & ",'" & Combo2.Text & "'", , adCmdUnknown
'                                       sql_txt = " UPDATE ZONE_TAX  SET PRICE_RATE = " & CDbl(Txt_Zone_Price.Text) & " , TAX_RATE  = " & CDbl(Txt_Zone_Rate.Text) & _
'                                                       " WHERE ZONETAX_ID  = '" & combo2.Text & "'"
                            ElseIf STATE = "DEL" Then
                                        Globle_Connective.Execute "exec sp_del_zonetax '" & Combo2.Text & "'", , adCmdUnknown
'                                            sql_txt = " DELETE FROM LANDUSE  WHERE ZONETAX_ID  = '" & combo2.Text & "'"
'                                           Call SET_Execute(sql_txt)
'                                           sql_txt = " DELETE FROM ZONE_TAX   WHERE ZONETAX_ID  = '" & combo2.Text & "'"
'                                           Call SET_Execute(sql_txt)
'                                           sql_txt = Empty
                            End If
                 End If
                 
                 If Op_Land_Use.Value = True Then  '��������ª����Թ
                       If STATE = "EDIT" Then
                            For i = 0 To 4
                                    If Chk_Static_Percent(i).Value Then
                                            k = 0
                                    Else
                                            k = 1
                                    End If
                                    If Chk_Lone_Percent(i).Value Then
                                            j = 0
                                    Else
                                            j = 1
                                    End If
                                    Globle_Connective.Execute "exec sp_manage_landuse '" & "LM-" & Format(i + 1, "0#") & "','" & Combo2.Text & "'," & CCur(Txt_Rate_LandUse(i).Text) & "," & CCur(Txt_Rate_LandLone(i).Text) & "," & k & "," & j & ",'" & STATE & "'", , adCmdUnknown
'                                    sql_txt = "UPDATE LANDUSE SET LANDUSE_PRICE = " & CDbl(Txt_Rate_LandUse(i).Text) & ", LANDUSE_RATE = " & CDbl(Txt_Rate_LandLone(i).Text)
'                                    sql_txt = sql_txt & ",TYPE_USE_PRICE = " & k & ", TYPE_USE_RATE = " & j
'                                    sql_txt = sql_txt & " WHERE  LM_ID =  'LM-" & Format(i, "0#") & "' AND ZONETAX_ID = '" & combo2.Text & "'"
'                                    Call SET_Execute(sql_txt)
                             Next i
'                             sql_txt = Empty
                            If (GBQueryLandEmployment.RecordCount > 0) Or (LenB(Trim$(Combo1.Text)) <> 0) Then ' ������ҡ���� 4 ��������鹰ҹ�֧����ö������
                                    If Chk_Static_Percent(5).Value Then
                                            k = 0
                                    Else
                                            k = 1
                                    End If
                                    If Chk_Lone_Percent(5).Value Then
                                            j = 0
                                    Else
                                            j = 1
                                    End If
                                    Globle_Connective.Execute "exec sp_manage_landuse '" & Combo3.Text & "','" & Combo2.Text & "'," & CCur(Txt_Rate_LandUse(5).Text) & "," & CCur(Txt_Rate_LandLone(5).Text) & "," & k & "," & j & ",'" & STATE & "'", , adCmdUnknown
'                                        sql_txt = "UPDATE LANDUSE SET LANDUSE_PRICE = " & CDbl(Txt_Rate_LandUse(5).Text) & ", LANDUSE_RATE = " & CDbl(Txt_Rate_LandLone(5).Text)
'                                        sql_txt = sql_txt & ",TYPE_USE_PRICE = " & k & ", TYPE_USE_RATE = " & j
'                                        sql_txt = sql_txt & " WHERE  LM_ID =  '" & Combo3.Text & "' AND ZONETAX_ID = '" & combo2.Text & "'"
                             End If
                    ElseIf STATE = "POST" Then
                        If Chk_Static_Percent(5).Value Then
                            k = 0
                        Else
                            k = 1
                        End If
                        If Chk_Lone_Percent(5).Value Then
                            j = 0
                        Else
                            j = 1
                        End If
                        GBQueryLandEmployment.MoveFirst
                        GBQueryLandEmployment.Move List1.ListIndex
                        Globle_Connective.Execute "exec sp_manage_landuse '" & GBQueryLandEmployment.Fields("LM_ID").Value & "','" & Combo2.Text & "'," & CCur(Txt_Rate_LandUse(5).Text) & "," & CCur(Txt_Rate_LandLone(5).Text) & "," & k & "," & j & ",'" & STATE & "'", , adCmdUnknown
'                        sql_txt = "INSERT INTO LANDUSE (LM_ID,ZONETAX_ID,LANDUSE_PRICE,LANDUSE_RATE,TYPE_USE_PRICE,TYPE_USE_RATE) " & _
'                                        " VALUES( '" & GBQueryLandEmployment.Fields("LM_ID").Value & "','" & combo2.Text & "'," & CDbl(Txt_Rate_LandUse(5).Text) & "," & CDbl(Txt_Rate_LandLone(5).Text) & "," & k & "," & j & ")"
                 ElseIf STATE = "DEL" And LenB(Trim$(Combo1.Text)) > 0 Then
                        Globle_Connective.Execute "exec sp_manage_landuse '','" & Combo2.Text & "',0,0,0,0,'" & STATE & "'", , adCmdUnknown
'                        sql_txt = "DELETE FROM LANDUSE WHERE ZONETAX_ID='" & combo2.Text & "'" & " AND LM_ID='" & GBQueryLandUse.Fields("LM_ID").Value & "'"
                 End If
             End If
                
                If Op_LM.Value Then   '��������������ª��
                        If STATE = "POST" Then
                                Globle_Connective.Execute "exec sp_manage_landemployment  '" & RunAutoNumber("LAND_EMPLOYMENT", "LM_ID", "LM-05", False, "") & "','" & Trim$(Txt_LM_Details.Text) & "','" & STATE & "'", , adCmdUnknown
'                          sql_txt = "INSERT INTO LAND_EMPLOYMENT ( LM_ID,LM_Details ) VALUES ('" & RunAutoNumber("LAND_EMPLOYMENT", "LM_ID", "LM-05", False, "") & "','" & Trim$(Txt_LM_Details.Text) & "')"
'                        End If
                        ElseIf STATE = "EDIT" Or STATE = "DEL" Then
                                GBQueryLandEmployment.MoveFirst
                                GBQueryLandEmployment.Move List1.ListIndex
                                Globle_Connective.Execute "exec sp_manage_landemployment  '" & GBQueryLandEmployment.Fields("LM_ID").Value & "','" & Trim$(Txt_LM_Details.Text) & "','" & STATE & "'", , adCmdUnknown
'                                sql_txt = "UPDATE LAND_EMPLOYMENT SET LM_Details  =  '" & Trim$(Txt_LM_Details.Text) & "'  WHERE LM_ID = '" & GBQueryLandEmployment.Fields("LM_ID").Value & "'"
                        End If
'                        If STATE = "DEL" Then
'                                GBQueryLandEmployment.MoveFirst
'                                GBQueryLandEmployment.Move List1.ListIndex
'                                sql_txt = "DELETE FROM LAND_EMPLOYMENT WHERE LM_ID='" & GBQueryLandEmployment.Fields("LM_ID").Value & "'"
'                        End If
                 End If
  End If
  
' If OpMenu(2).Value = True Then  '�ѵ�����ջ���
' End If
                    If OpMenu(6).Value = True Then   '�ѵ�����ջ���
                          For i = 1 To 4
                                Globle_Connective.Execute "exec sp_update_signboard " & CCur(TXT_SIGNBORD_RATE(i).Text) & "," & CSng(TXT_SIGNBORD_RATE_OF_USE(i).Text) & "," & CCur(TXT_SIGNBORD_LOW(i).Text) & ",'" & i & "'", , adCmdUnknown
'                                sql_txt = " UPDATE SIGNBORDDATA_TYPE SET SIGNBORD_RATE = " & CDbl(TXT_SIGNBORD_RATE(i).Text)
'                                sql_txt = sql_txt & ", SIGNBORD_RATE_OF_USE = " & CDbl(TXT_SIGNBORD_RATE_OF_USE(i).Text)
'                                sql_txt = sql_txt & ", SIGNBORD_LOW = " & CDbl(TXT_SIGNBORD_LOW(i).Text)
'                                sql_txt = sql_txt & " WHERE SIGNBORD_TYPE_ID = " & i
'                                        Call SET_Execute(sql_txt)
                          Next i
'                                        sql_txt = Empty
                     End If
                    
                    If OpMenu(7).Value Then   '�͹حҵ��Сͺ��ä��
                            If Op_Business.Value Then
                                    If STATE = "EDIT" Or STATE = "POST" Then
                                            Globle_Connective.Execute "exec sp_manage_bussiness_type '" & Lb_Business.Caption & "','" & Trim$(Txt_Business_Type.Text) & "','" & STATE & "'", , adCmdUnknown
                                    ElseIf STATE = "DEL" Then
                                            Globle_Connective.Execute "exec sp_manage_bussiness_type '" & Grid_Business.TextMatrix(Grid_Business.Row, 0) & "','" & Trim$(Txt_Business_Type.Text) & "','" & STATE & "'", , adCmdUnknown
                                    End If
'                                    If STATE = "EDIT" Then
'                                      sql_txt = "UPDATE BUSINESS_TYPE SET BUSINESS_TYPE =  '" & Trim$(Txt_Business_Type.Text) & "'" & _
'                                                             " WHERE  BUSINESS_ID = '" & Lb_Business.Caption & "'"
'                                    End If
'                                    If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO BUSINESS_TYPE (BUSINESS_ID ,BUSINESS_TYPE) VALUES ('" & _
'                                                              Lb_Business.Caption & "','" & Trim$(Txt_Business_Type.Text) & "')"
'                                    End If
'                                    If STATE = "DEL" Then
'                                           sql_txt = "DELETE FROM BUSINESS_TYPE WHERE BUSINESS_ID = '" & Grid_Business.TextMatrix(Grid_Business.Row, 0) & "'"
'                                    End If
                            End If
                            If Op_Class.Value Then
                                    If STATE = "EDIT" Or STATE = "DEL" Then
                                            Globle_Connective.Execute "exec sp_manage_class_details '" & Grid_Class.TextMatrix(Grid_Class.Row, 0) & "','" & Trim$(Txt_Class_Details.Text) & "','" & Lb_Business.Caption & "'," & Txt_Class_Price.Text & ",'" & STATE & "'", , adCmdUnknown
                                    ElseIf STATE = "POST" Then
                                            Globle_Connective.Execute "exec sp_manage_class_details '" & Lb_Business.Caption & "-" & lb_Class_Order.Caption & "','" & Trim$(Txt_Class_Details.Text) & "','" & Lb_Business.Caption & "'," & Txt_Class_Price.Text & ",'" & STATE & "'", , adCmdUnknown
                                    End If
'                                    If STATE = "EDIT" Then
'                                        sql_txt = "UPDATE CLASS_DETAILS SET  CLASS_DETAILS = '" & Trim$(Txt_Class_Details.Text) & "', CLASS_PRICE  = " & Txt_Class_Price.Text & _
'                                                             " WHERE CLASS_ID = '" & Grid_Class.TextMatrix(Grid_Class.Row, 0) & "'"
'                                    End If
'                                    If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO CLASS_DETAILS  ( CLASS_ID , CLASS_DETAILS , BUSINESS_ID , CLASS_PRICE  ) VALUES ('" & _
'                                                                Lb_Business.Caption & "-" & lb_Class_Order.Caption & "','" & Trim$(Txt_Class_Details.Text) & "','" & Lb_Business.Caption & "'," & Txt_Class_Price.Text & ")"
'                                    End If
'                                    If STATE = "DEL" Then
'                                           sql_txt = " DELETE  FROM  CLASS_DETAILS  WHERE  CLASS_ID  = '" & Grid_Class.TextMatrix(Grid_Class.Row, 0) & "'"
'                                    End If
                            End If
                    End If
                    
                    If Op_BuildingType.Value = True Then ' ��Դ�ͧ�ç���͹
                                Globle_Connective.Execute "exec sp_manage_building_type '" & Lb_BuildingType_ID.Caption & "','" & Trim$(Txt_BuildingType.Text) & "','" & STATE & "'", , adCmdUnknown
'                                    If STATE = "POST" Then
'                                        sql_txt = "INSERT INTO BUILDING_SHAPE ( BUILDING_SHAPE_ID , BUILDING_DETAILS ) VALUES ('" & _
'                                                                Lb_BuildingType_ID.Caption & "','" & Trim$(Txt_BuildingType.Text) & "')"
'                                    End If
'                                    If STATE = "EDIT" Then
'                                        sql_txt = "UPDATE BUILDING_SHAPE  SET BUILDING_DETAILS = '" & Trim$(Txt_BuildingType.Text) & "'" & _
'                                                              " WHERE BUILDING_SHAPE_ID = '" & Lb_BuildingType_ID.Caption & "'"
'                                    End If
'                                    If STATE = "DEL" Then
'                                        sql_txt = "DELETE  FROM BUILDING_SHAPE WHERE BUILDING_SHAPE_ID = '" & Lb_BuildingType_ID.Caption & "'"
'                                    End If
                    End If
                    
                    If Op_BuildingRate.Value Then  ' �ѵ�������ç���͹��з��Թ
                                If STATE = "POST" Or STATE = "EDIT" Then
'                                    GBQueryBuildingRate.Requery
                                    If GBQueryBuildingRate.AbsolutePosition < 0 Then
                                            Globle_Connective.Execute "exec sp_insert_buildingrate '" & Cmb_Tamlay.Text & "','" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "'," & CCur(Txt_TYPE_A_RATE1.Text) & _
                                                    "," & CCur(Txt_TYPE_A_RATE2.Text) & "," & CCur(Txt_TYPE_A_RATE3.Text) & "," & CCur(Txt_TYPE_A_RATE4.Text) & "," & CCur(Txt_TYPE_B_RATE1.Text) & _
                                                    "," & CCur(Txt_TYPE_B_RATE2.Text) & "," & CCur(Txt_TYPE_C_RATE1.Text) & "," & CCur(Txt_TYPE_C_RATE2.Text) & "," & CCur(Txt_TYPE_D_RATE1.Text) & "," & Txt_TYPE_A_AREA.Text, , adCmdUnknown
'                                            sql_txt = "INSERT INTO BUILDING_RATE (ZONETAX_ID,BUILDING_SHAPE_ID,TYPE_A_RATE1,TYPE_A_RATE2,TYPE_A_RATE3, TYPE_A_RATE4 ," & _
'                                                                 "TYPE_B_RATE1,TYPE_B_RATE2,TYPE_C_RATE1,TYPE_C_RATE2,TYPE_D_RATE1) VALUES ( '"
'                                            sql_txt = sql_txt & Cmb_Tamlay.Text & "','" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "'," & _
'                                                                  CSng(Txt_TYPE_A_RATE1.Text) & "," & CSng(Txt_TYPE_A_RATE2.Text) & "," & CSng(Txt_TYPE_A_RATE3.Text) & "," & CSng(Txt_TYPE_A_RATE4.Text) & "," & _
'                                                                  CSng(Txt_TYPE_B_RATE1.Text) & "," & CSng(Txt_TYPE_B_RATE2.Text) & "," & _
'                                                                  CSng(Txt_TYPE_C_RATE1.Text) & "," & CSng(Txt_TYPE_C_RATE2.Text) & "," & _
'                                                                  CSng(Txt_TYPE_D_RATE1.Text) & ")"
                                    Else
'                                            Debug.Print "exec sp_update_buildingrate '" & Cmb_Tamlay.Text & "','" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "','" & CCur(Txt_TYPE_A_RATE1.Text) & _
'                                                    "','" & CCur(Txt_TYPE_A_RATE2.Text) & "','" & CCur(Txt_TYPE_A_RATE3.Text) & "','" & CCur(Txt_TYPE_A_RATE4.Text) & "','" & CCur(Txt_TYPE_B_RATE1.Text) & _
'                                                    "','" & CCur(Txt_TYPE_B_RATE2.Text) & "','" & CCur(Txt_TYPE_C_RATE1.Text) & "','" & CCur(Txt_TYPE_C_RATE2.Text) & "','" & CCur(Txt_TYPE_D_RATE1.Text)
                                            Globle_Connective.Execute "exec sp_update_buildingrate '" & Cmb_Tamlay.Text & "','" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "'," & CCur(Txt_TYPE_A_RATE1.Text) & _
                                                    "," & CCur(Txt_TYPE_A_RATE2.Text) & "," & CCur(Txt_TYPE_A_RATE3.Text) & "," & CCur(Txt_TYPE_A_RATE4.Text) & "," & CCur(Txt_TYPE_B_RATE1.Text) & _
                                                    "," & CCur(Txt_TYPE_B_RATE2.Text) & "," & CCur(Txt_TYPE_C_RATE1.Text) & "," & CCur(Txt_TYPE_C_RATE2.Text) & "," & CCur(Txt_TYPE_D_RATE1.Text) & "," & Txt_TYPE_A_AREA.Text, , adCmdUnknown
'                                            sql_txt = "UPDATE BUILDING_RATE SET " & _
'                                                              " TYPE_A_RATE1 = " & CSng(Txt_TYPE_A_RATE1.Text) & "," & _
'                                                             "  TYPE_A_RATE2 = " & CSng(Txt_TYPE_A_RATE2.Text) & "," & _
'                                                             "  TYPE_A_RATE3 = " & CSng(Txt_TYPE_A_RATE3.Text) & "," & _
'                                                             "  TYPE_A_RATE4 = " & CSng(Txt_TYPE_A_RATE4.Text) & "," & _
'                                                             "  TYPE_B_RATE1 = " & CSng(Txt_TYPE_B_RATE1.Text) & "," & _
'                                                             "  TYPE_B_RATE2 = " & CSng(Txt_TYPE_B_RATE2.Text) & "," & _
'                                                             "  TYPE_C_RATE1 =" & CSng(Txt_TYPE_C_RATE1.Text) & "," & _
'                                                             "  TYPE_C_RATE2 = " & CSng(Txt_TYPE_C_RATE2.Text) & "," & _
'                                                             "  TYPE_D_RATE1 = " & CSng(Txt_TYPE_D_RATE1.Text) & _
'                                                              " WHERE BUILDING_SHAPE_ID = '" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "' AND ZONETAX_ID = '" & Cmb_Tamlay.Text & "'"
                                    End If
                                 End If
                    End If
                    
                If OpMenu(8).Value Then
                        If STATE = "EDIT" Then
                                If Op_Cfg_Building.Value Then
                                        Globle_Connective.Execute "exec sp_update_cfg_building " & Txt_Due_Accept0.Text & "," & CCur(Txt_Due_1Month.Text) & "," & CCur(Txt_Due_2Month.Text) & _
                                                "," & CCur(Txt_Due_3Month.Text) & "," & CCur(Txt_Due_4Month.Text) & "," & CCur(Txt_Due_Due_Appeal0.Text) & "," & CCur(Txt_Tax_YPerM.Text) & _
                                                "," & CCur(Tax_MPerD.Text) & "," & CCur(Txt_Tax_Machine.Text), , adCmdUnknown
'                                               sql_txt = "UPDATE CONFIG_TAXRATE SET Due_Accept = " & CByte(Txt_Due_Accept0.Text) & ",Due_1Month =" & CSng(Txt_Due_1Month.Text) & "," & _
'                                                                     " Due_2Month = " & CSng(Txt_Due_2Month.Text) & ",Due_3Month = " & CSng(Txt_Due_3Month.Text) & ",Due_4Month = " & CSng(Txt_Due_4Month.Text) & "," & _
'                                                                      "Due_Appeal = " & CSng(Txt_Due_Due_Appeal0.Text) & ",Tax_YPerM = " & CSng(Txt_Tax_YPerM.Text) & ", Tax_MPerD = " & CSng(Tax_MPerD.Text) & "," & _
'                                                                     "Tax_Machine = " & CSng(Txt_Tax_Machine.Text) & _
'                                                                     " WHERE Type_Rate = 0 "
                                End If
                                If Op_Cfg_Land.Value Then
                                        Globle_Connective.Execute "exec sp_update_cfg_land " & Txt_Due_Accept1.Text & "," & CCur(Txt_Tax_OutSchedule1.Text) & "," & CCur(Txt_Tax_NotFully.Text) & _
                                                "," & CCur(Txt_Tax_OutTime1.Text) & "," & Txt_Due_Due_Appeal1.Text & "," & Txt_B_Rai.Text & "," & Txt_B_Pot.Text & "," & CCur(Txt_B_Va), , adCmdUnknown
'                                               sql_txt = "UPDATE CONFIG_TAXRATE SET Due_Accept = " & CByte(Txt_Due_Accept1.Text) & ",Tax_OutSchedule =" & CSng(Txt_Tax_OutSchedule1.Text) & "," & _
'                                                                    " Tax_NotFully = " & CSng(Txt_Tax_NotFully.Text) & ",Tax_OutTime=" & CSng(Txt_Tax_OutTime1.Text) & ",Due_Appeal = " & CByte(Txt_Due_Due_Appeal1.Text) & _
'                                                                    ", Subside_Rai = " & CByte(Txt_B_Rai.Text) & ",Subside_Ngan = " & CByte(Txt_B_Pot.Text) & ",Subside_Va = " & CSng(Txt_B_Va) & " WHERE Type_Rate = 1 "
                                End If
                                If Op_Cfg_Signbord.Value Then
                                        Globle_Connective.Execute "exec sp_update_cfg_signboard " & Txt_Due_Accept2.Text & "," & CCur(Txt_Tax_OutSchedule2.Text) & "," & CCur(Txt_Tax_NotFully2.Text) & _
                                                "," & CCur(Txt_Tax_OutTime2.Text) & "," & Txt_Due_Due_Appeal2.Text, , adCmdUnknown
'                                               sql_txt = "UPDATE CONFIG_TAXRATE SET Due_Accept = " & CByte(Txt_Due_Accept2.Text) & ",Tax_OutSchedule =" & CSng(Txt_Tax_OutSchedule2.Text) & "," & _
'                                                                    " Tax_NotFully = " & CSng(Txt_Tax_NotFully2.Text) & " ,Tax_OutTime=" & CSng(Txt_Tax_OutTime2.Text) & ",Due_Appeal =" & CByte(Txt_Due_Due_Appeal2.Text) & _
'                                                                     " WHERE Type_Rate = 2 "
                                End If
                        End If
                End If
'                                If sql_txt <> Empty Then
'                                     Call SET_Execute(sql_txt)
'                                End If
            If OpMenu(9).Value Then
                    If lb_change_id.Caption <> "000" Then
                            Globle_Connective.Execute "exec sp_manage_change_history " & cb_type_asset.ListIndex + 1 & ",'" & lb_change_id.Caption & "','" & txt_change.Text & "','" & STATE & "'", , adCmdUnknown
'                            Select Case cb_type_asset.ListIndex
'                                    Case 0
'                                            Globle_Connective.Execute "exec sp_manage_land_change '" & lb_change_id.Caption & "','" & txt_change.Text & "','" & STATE & "'", , adCmdUnknown
'                                    Case 1
'                                            Globle_Connective.Execute "exec sp_manage_building_change '" & lb_change_id.Caption & "','" & txt_change.Text & "','" & STATE & "'", , adCmdUnknown
'                                    Case 2
'                                            Globle_Connective.Execute "exec sp_manage_signboard_change '" & lb_change_id.Caption & "','" & txt_change.Text & "','" & STATE & "'", , adCmdUnknown
'                            End Select
                    End If
            End If
            If OpMenu(10).Value Then
                    If Opt_Request.Value Then
                            Globle_Connective.Execute "exec sp_manage_petition_type '" & lb_Request_id.Caption & "','" & txt_Request.Text & "','" & STATE & "'", , adCmdUnknown
                    ElseIf Opt_Building_Type.Value Then
                            Globle_Connective.Execute "exec sp_manage_building_type2 '" & lb_Request_id.Caption & "','" & txt_Request.Text & "','" & STATE & "'", , adCmdUnknown
                    Else
                            Globle_Connective.Execute "exec sp_manage_officerdata '" & lb_Request_id.Caption & "','" & txt_Request.Text & "','" & txt_Position.Text & "','" & STATE & "'", , adCmdUnknown
                    End If
            End If
            Set rs = Nothing
End Sub

Private Sub Btn_Add_Click()
  Call SET_TEXTBOX("ADD", "Perpose")
  Status = "POST"
End Sub

Private Sub Btn_Cancel_Click()
Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If MsgBox("�׹�ѹ���ź������ ?", vbInformation + vbYesNo, "ź������!") = vbNo Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE("DEL")
        Call SET_TEXTBOX("DEL")
        Call SET_REFRESH
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        If Err.Number = -2147467259 Then
                Select Case True
                        Case Op_Soi.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ��¹���ѧ��������㹷���¹���Թ", vbCritical, "��ͼԴ��Ҵ"
                        Case Chk_Street.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ�������ѧ��������㹷���¹���Թ �����ѧ�ի������㹶�����", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_Village.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ�����ҹ����ѧ��������㹷���¹���Թ", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_Tambon.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ�ӺŹ���ѧ��������㹷���¹���Թ �����ѧ�������ҹ����㹵ӺŹ��", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_LM.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ��������������ª�����ѧ��������㹼��ӻ���ª����Թ" & vbCrLf & "���� �ѧ�ա�������ª����Թ��������������㹻�������������ª����", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_Business.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ�������Ԩ��ä�ҹ���ѧ����������ѡɳСԨ��ä��", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_BLOCK.Value, Op_ZONE.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ������ࢵ���¹���ѧ��������㹷���¹���Թ���� �ç���͹��з��Թ ���� ����", vbCritical, "��ͼԴ��Ҵ"
                        Case Op_RateTax.Value
                                MsgBox "�������öź�������� ���ͧ�ҡ��������ª����Թ����ѧ��������㹼�������ª����Թ�ͧ����¹���Թ", vbCritical, "��ͼԴ��Ҵ"
                End Select
        Else
                MsgBox Err.Number & "-" & Err.Description, vbExclamation, "��ͼԴ��Ҵ"
        End If
End Sub

Private Sub Btn_Edit_Click()
If Op_LM.Value Then
   If LenB(Trim$(Txt_LM_Details.Text)) = 0 Then Exit Sub
End If

Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
End Sub
Private Sub Btn_Post_Click()
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE(Status)
        Call SET_TEXTBOX("POST")
        Call SET_REFRESH
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        Select Case Err.Number
                Case -2147217873
                        MsgBox "�����ū�� �������ö�ѹ�֡�������� !!!", vbExclamation, "��ͼԴ��Ҵ"
        End Select
'        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Refresh_Click()
Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
'Dim x As String
 ' x = InputBox("���Ң�����" & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf, "�鹢�����")
  MsgBox "Display Form Search is not use ! ", vbExclamation, "Not Response!"
End Sub

Private Sub cb_type_asset_Click()
        lb_change_id.Caption = ""
        txt_change.Text = ""
'        Set Grid_Change.DataSource = Nothing
        Select Case cb_type_asset.ListIndex
                Case 0
'                        Set GBQueryLandChange = Globle_Connective.Execute("sp_land_change", , adCmdStoredProc)
                        GBQueryLandChange.Requery
                        If GBQueryLandChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryLandChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 1
'                        Set GBQuerydChange = Globle_Connective.Execute("sp_building_change", , adCmdStoredProc)
                        GBQueryBuildingChange.Requery
                        If GBQueryBuildingChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryBuildingChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 2
'                        Set GBQueryLandChange = Globle_Connective.Execute("sp_signboard_change", , adCmdStoredProc)
                        GBQuerySignboardChange.Requery
                        If GBQuerySignboardChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQuerySignboardChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
                Case 3
                        GBQueryLicenseChange.Requery
                        If GBQueryLicenseChange.RecordCount > 0 Then
                                Set Grid_change.DataSource = GBQueryLicenseChange
                        Else
                                Set Grid_change.DataSource = Nothing
                                Grid_change.Clear
                                Grid_change.Rows = 2
                        End If
'                        Set Grid_change.DataSource = Globle_Connective.Execute("sp_signboard_change", , adCmdStoredProc)
        End Select
        Call Set_Grid
End Sub

Private Sub Chk_Lone_Baht_Click(Index As Integer)
If Chk_Lone_Baht(Index).Value = Checked Then
        Chk_Lone_Percent(Index).Value = Unchecked
Else
        Chk_Lone_Percent(Index).Value = Checked
End If
End Sub

Private Sub Chk_Lone_Percent_Click(Index As Integer)
If Chk_Lone_Percent(Index).Value = Checked Then
     Chk_Lone_Baht(Index).Value = Unchecked
Else
    Chk_Lone_Baht(Index).Value = Checked
End If
End Sub
Private Sub Chk_Static_Baht_Click(Index As Integer)
If Chk_Static_Baht(Index).Value = Checked Then
     Chk_Static_Percent(Index).Value = Unchecked
Else
    Chk_Static_Percent(Index).Value = Checked
End If
End Sub

Private Sub Chk_Static_Percent_Click(Index As Integer)
If Chk_Static_Percent(Index).Value = Checked Then
     Chk_Static_Baht(Index).Value = Unchecked
Else
    Chk_Static_Baht(Index).Value = Checked
End If
End Sub

Private Sub Chk_Street_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(10).ForeColor = &H575555
        Label2(4).ForeColor = &HC2723B
        If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub

If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")

End Sub

Private Sub Chk_Zone_Click()
        If Chk_Zone.Value = Checked Then
                Label2(20).Caption = "�кبӹǹࢵ"
        Else
                Label2(20).Caption = "�к������Ţࢵ"
        End If
End Sub

Private Sub Cmb_Tamlay_Click()
                                Txt_TYPE_A_RATE1.Text = "0.00"
                                Txt_TYPE_A_RATE2.Text = "0.00"
                                Txt_TYPE_A_RATE3.Text = "0.00"
                                Txt_TYPE_A_RATE4.Text = "0.00"
                                Txt_TYPE_B_RATE1.Text = "0.00"
                                Txt_TYPE_B_RATE2.Text = "0.00"
                                Txt_TYPE_C_RATE1.Text = "0.00"
                                Txt_TYPE_C_RATE2.Text = "0.00"
                                Txt_TYPE_D_RATE1.Text = "0.00"
                                Txt_TYPE_A_AREA.Text = "0.00"
        GBQueryBuildingRate.Filter = "ZONETAX_ID = '" & Cmb_Tamlay.Text & "'"
End Sub


Private Sub Cmb_Zone_CLICK()
   ListCurrent = Cmb_Zone.ListIndex
   GBQueryZone.Filter = " ZONE_ID = '" & Trim$(Cmb_Zone.Text) & "'"
If GBQueryZone.RecordCount > 0 Then
    Set Grid_Zone.DataSource = GBQueryZone
Else
   Set Grid_Zone.DataSource = Nothing
          Grid_Zone.Clear
          Grid_Zone.Rows = 2
End If
     Call Set_Grid
End Sub



Private Sub Combo1_Change()
        Combo3.ListIndex = Combo1.ListIndex
End Sub

Private Sub Combo1_Click()
        Combo3.ListIndex = Combo1.ListIndex
        'GBQueryLandEmployment.MoveFirst
        'GBQueryLandEmployment.Move Combo1.ListIndex
        GBQueryLandUse.Filter = "ZONETAX_ID='" & Combo2.Text & "' AND LM_ID > 'LM-05'"
        GBQueryLandUse.MoveFirst
        GBQueryLandUse.Move Combo1.ListIndex
        'If GBQueryLandUse.AbsolutePosition > 0 Then
        '   If GBQueryLandUse.RecordCount > 0 Then
        '       GBQueryLandUse.MoveFirst
        '       GBQueryLandUse.Find " LM_ID = '" & GBQueryLandEmployment.Fields("LM_ID").Value & "'", , adSearchForward
        '    End If
        'End If
        
        With GBQueryLandUse
                If .AbsolutePosition > 0 Then
                            Txt_Rate_LandUse(5).Text = .Fields("LANDUSE_PRICE").Value
                            Txt_Rate_LandLone(5).Text = .Fields("LANDUSE_RATE").Value
                             If .Fields("TYPE_USE_PRICE").Value Then
                                 Chk_Static_Baht(5).Value = Checked
                             Else
                                Chk_Static_Percent(5).Value = Checked
                             End If
                             If .Fields("TYPE_USE_RATE").Value Then
                                Chk_Lone_Baht(5).Value = Checked
                             Else
                                Chk_Lone_Percent(5).Value = Checked
                             End If
                Else
                            Chk_Lone_Percent(5).Value = Unchecked
                            Chk_Static_Percent(5).Value = Unchecked
                            Txt_Rate_LandUse(5).Text = ""
                            Txt_Rate_LandLone(5).Text = ""
                End If
        End With
End Sub

Private Sub combo2_Change()
        On Error Resume Next
        With GBQueryLandUse   '��������ª����Թ
                .Filter = adFilterNone
                .Requery
                .Filter = "ZONETAX_ID = '" & Combo2.Text & "' AND LM_ID > 'LM-05'"
                Combo1.Clear
                Combo3.Clear
                If .RecordCount > 0 Then
                           .MoveFirst
                            Do While Not .EOF
                                  Combo1.AddItem .Fields("LM_Details").Value
                                  Combo3.AddItem .Fields("LM_ID").Value
                                  .MoveNext
                            Loop
                End If
        End With
End Sub

Private Sub combo2_Click()
    On Error Resume Next
'    Txt_Zone_Price.Text = Empty
'    Txt_Zone_Rate.Text = Empty
     With GBQueryLandUse   '��������ª����Թ
                .Filter = adFilterNone
                .Requery
                .Filter = "ZONETAX_ID = '" & Combo2.Text & "' AND LM_ID > 'LM-05'"
                Combo1.Clear
                Combo3.Clear
                If .RecordCount > 0 Then
                           .MoveFirst
                            Do While Not .EOF
                                  Combo1.AddItem .Fields("LM_Details").Value
                                  Combo3.AddItem .Fields("LM_ID").Value
                                  .MoveNext
                            Loop
                End If
        End With
    'Txt_Zone_Price.Text = GRID_ZONETAX.TextMatrix(Int(combo2.Text), 1)
    'Txt_Zone_Rate.Text = GRID_ZONETAX.TextMatrix(Int(combo2.Text), 2)
End Sub

Private Sub combo2_KeyPress(KeyAscii As Integer)
        KeyAscii = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Frm_Index = Nothing
End Sub

Private Sub Grid_BuildingType_Click()
        If LenB(Trim$(Grid_BuildingType.TextMatrix(1, 1))) = 0 Then Exit Sub
        Call Grid_BuildingType_RowColChange
End Sub

Private Sub Grid_BuildingType_RowColChange()
       Label1.Caption = Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 1)
With GBQueryBuildingRate
'        .Requery
       If .RecordCount > 0 Then
                       .MoveFirst
                       .Find " BUILDING_SHAPE_ID = '" & Grid_BuildingType.TextMatrix(Grid_BuildingType.Row, 0) & "'", , adSearchForward
                    If .AbsolutePosition > 0 Then
                                Txt_TYPE_A_RATE1.Text = IIf(IsNull(.Fields("TYPE_A_RATE1").Value), "0.00", .Fields("TYPE_A_RATE1").Value)
                                Txt_TYPE_A_RATE2.Text = IIf(IsNull(.Fields("TYPE_A_RATE2").Value), "0.00", .Fields("TYPE_A_RATE2").Value)
                                Txt_TYPE_A_RATE3.Text = IIf(IsNull(.Fields("TYPE_A_RATE3").Value), "0.00", .Fields("TYPE_A_RATE3").Value)
                                Txt_TYPE_A_RATE4.Text = IIf(IsNull(.Fields("TYPE_A_RATE4").Value), "0.00", .Fields("TYPE_A_RATE4").Value)
                                Txt_TYPE_B_RATE1.Text = IIf(IsNull(.Fields("TYPE_B_RATE1").Value), "0.00", .Fields("TYPE_B_RATE1").Value)
                                Txt_TYPE_B_RATE2.Text = IIf(IsNull(.Fields("TYPE_B_RATE2").Value), "0.00", .Fields("TYPE_B_RATE2").Value)
                                Txt_TYPE_C_RATE1.Text = IIf(IsNull(.Fields("TYPE_C_RATE1").Value), "0.00", .Fields("TYPE_C_RATE1").Value)
                                Txt_TYPE_C_RATE2.Text = IIf(IsNull(.Fields("TYPE_C_RATE2").Value), "0.00", .Fields("TYPE_C_RATE2").Value)
                                Txt_TYPE_D_RATE1.Text = IIf(IsNull(.Fields("TYPE_D_RATE1").Value), "0.00", .Fields("TYPE_D_RATE1").Value)
                                Txt_TYPE_A_AREA.Text = IIf(IsNull(.Fields("TYPE_A_AREA").Value), "0.00", .Fields("TYPE_A_AREA").Value)
                    Else
                                Txt_TYPE_A_RATE1.Text = "0.00"
                                Txt_TYPE_A_RATE2.Text = "0.00"
                                Txt_TYPE_A_RATE3.Text = "0.00"
                                Txt_TYPE_A_RATE4.Text = "0.00"
                                Txt_TYPE_B_RATE1.Text = "0.00"
                                Txt_TYPE_B_RATE2.Text = "0.00"
                                Txt_TYPE_C_RATE1.Text = "0.00"
                                Txt_TYPE_C_RATE2.Text = "0.00"
                                Txt_TYPE_D_RATE1.Text = "0.00"
                                Txt_TYPE_A_AREA.Text = "0.00"
                     End If
    End If
End With
    
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
     With Grid_BuildingType
              Lb_BuildingType_ID.Caption = .TextMatrix(.Row, 0)
              Txt_BuildingType.Text = .TextMatrix(.Row, 1)
    End With
End Sub

Private Sub Grid_Business_Click()
Call Grid_Business_RowColChange
End Sub

Private Sub Grid_Business_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
     Call SET_TEXTBOX("CANCEL")
    
    Lb_Business.Caption = Grid_Business.TextMatrix(Grid_Business.Row, 0)
    Txt_Business_Type.Text = Grid_Business.TextMatrix(Grid_Business.Row, 1)
    GBQueryClass.Requery
GBQueryClass.Filter = " Business_Id =  '" & Grid_Business.TextMatrix(Grid_Business.Row, 0) & "'"

If GBQueryClass.RecordCount > 0 Then
    Set Grid_Class.DataSource = GBQueryClass
Else
   Set Grid_Class.DataSource = Nothing
          Grid_Class.Clear
          Grid_Class.Rows = 2
End If
    Call Set_Grid
End Sub

Private Sub Grid_Change_Click()
'        lb_change_id.Caption = Grid_Change.TextMatrix(Grid_Change.Row, 0)
'        txt_change.Text = Grid_Change.TextMatrix(Grid_Change.Row, 1)
Call Grid_Change_RowColChange
End Sub

Private Sub Grid_Change_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
     Call SET_TEXTBOX("CANCEL")
    
    lb_change_id.Caption = Grid_change.TextMatrix(Grid_change.Row, 0)
    txt_change.Text = Grid_change.TextMatrix(Grid_change.Row, 1)
End Sub

Private Sub Grid_Class_Click()
Call Grid_Class_RowColChange
End Sub

Private Sub Grid_Class_RowColChange()
On Error Resume Next
lb_Class_Order.Caption = Right$(Grid_Class.TextMatrix(Grid_Class.Row, 0), 3)
Txt_Class_Details.Text = Grid_Class.TextMatrix(Grid_Class.Row, 1)
Txt_Class_Price.Text = Grid_Class.TextMatrix(Grid_Class.Row, 2)
End Sub

Private Sub Grid_Request_Click()
Call Grid_Request_RowColChange
End Sub

Private Sub Grid_Request_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
     Call SET_TEXTBOX("CANCEL")
    
    lb_Request_id.Caption = Grid_Request.TextMatrix(Grid_Request.Row, 0)
    txt_Request.Text = Grid_Request.TextMatrix(Grid_Request.Row, 1)
    If Opt_Officer.Value = True Then txt_Position.Text = Grid_Request.TextMatrix(Grid_Request.Row, 2)
End Sub

Private Sub Grid_SOI_Click()
Call Grid_SOI_RowColChange
End Sub

Private Sub Grid_SOI_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
     With Grid_SOI
        LB_SOI_ID.Caption = Trim$(.TextMatrix(.Row, 1))
        Txt_SOI.Text = .TextMatrix(.Row, 2)
    End With
End Sub

Private Sub GRID_ZONETAX_Click()
Call GRID_ZONETAX_RowColChange
End Sub

Private Sub GRID_ZONETAX_RowColChange()
On Error Resume Next
With GRID_ZONETAX
If LenB(Trim$(.TextMatrix(1, 1))) > 0 Then
   Txt_Zone_Price.Text = FormatNumber(.TextMatrix(.Row, 1), 2, vbTrue, vbUseDefault, vbTrue)
   Txt_Zone_Rate.Text = FormatNumber(.TextMatrix(.Row, 1), 2, vbTrue, vbUseDefault, vbTrue)
   Combo2.Text = Trim$(.TextMatrix(.Row, 0))
End If
End With

GBQueryLandUse.Filter = " ZoneTax_ID = '" & GRID_ZONETAX.TextMatrix(GRID_ZONETAX.Row, 0) & "'"

Dim i As Byte
                              i = 0
    With GBQueryLandUse
            If .RecordCount > 0 Then
                    .MoveFirst
                    Do While Not .EOF
                              
                                 Txt_Rate_LandUse(i).Text = .Fields("LANDUSE_PRICE").Value
                                 Txt_Rate_LandLone(i).Text = .Fields("LANDUSE_RATE").Value
                                  If .Fields("TYPE_USE_PRICE").Value Then
                                      Chk_Static_Baht(i).Value = Checked
                                  Else
                                     Chk_Static_Percent(i).Value = Checked
                                  End If
                                  If .Fields("TYPE_USE_RATE").Value Then
                                     Chk_Lone_Baht(i).Value = Checked
                                  Else
                                     Chk_Lone_Percent(i).Value = Checked
                                  End If
                                  i = i + 1
                             .MoveNext
                    Loop
            Else
                  Txt_Rate_LandUse(1).Text = "": Txt_Rate_LandUse(2).Text = ""
                  Txt_Rate_LandUse(3).Text = "": Txt_Rate_LandUse(4).Text = ""
                  Txt_Rate_LandLone(1).Text = "": Txt_Rate_LandLone(2).Text = ""
                  Txt_Rate_LandLone(3).Text = "": Txt_Rate_LandLone(4).Text = ""
            End If
    End With
    Combo1.ListIndex = 0
End Sub

Private Sub Form_Activate()
         Clone_Form.Picture1.Visible = True
         Me.WindowState = vbMaximized
         Call Set_Grid
End Sub

Private Sub Form_Load()
       secattr.nLength = Len(secattr)  ' size of the structure
       secattr.lpSecurityDescriptor = 0  ' default (normal) level of security
       secattr.bInheritHandle = 1
      
      SSTab1.Top = 1430
'      For Each Image1.Item.index In Image1
'
'      Next
    Image1(1).Picture = LoadResPicture(999, 0)
    Image1(2).Picture = LoadResPicture(999, 0)
    Image1(3).Picture = LoadResPicture(999, 0)
    Image1(4).Picture = LoadResPicture(999, 0)
    Image1(5).Picture = LoadResPicture(999, 0)
    Image1(6).Picture = LoadResPicture(999, 0)
    Image1(7).Picture = LoadResPicture(999, 0)
    Image1(8).Picture = LoadResPicture(999, 0)
    Image1(0).Picture = LoadResPicture(999, 0)
    Image1(9).Picture = LoadResPicture(999, 0)
    Combo2.ListIndex = 0
    Cmb_Tamlay.ListIndex = 0
    cb_type_asset.ListIndex = 0
    Call SET_REFRESH
  End Sub

Private Sub Grid_STREET_Click()
Call Grid_STREET_RowColChange
End Sub

Private Sub Grid_STREET_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
              Call SET_TEXTBOX("CANCEL")
     With Grid_STREET
              LB_STREET_ID.Caption = Trim$(.TextMatrix(.Row, 0))
              Txt_Street_Name.Text = .TextMatrix(.Row, 1)
    End With

GBQuerySoi.Filter = " STREET_ID = '" & Trim$(LB_STREET_ID.Caption) & "'"

If GBQuerySoi.RecordCount > 0 Then
    Set Grid_SOI.DataSource = GBQuerySoi
Else
   Set Grid_SOI.DataSource = Nothing
          Grid_SOI.Clear
          Grid_SOI.Rows = 2
End If
    Call Set_Grid
End Sub

Private Sub GRID_TAMBON_Click()
Call GRID_TAMBON_RowColChange
End Sub

Private Sub GRID_TAMBON_RowColChange()
If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Exit Sub
              Call SET_TEXTBOX("CANCEL")
     With Grid_Tambon
              Lb_Tambon_Id.Caption = Right$(.TextMatrix(.Row, 0), 2)
              Tx_Tambon_Name.Text = .TextMatrix(.Row, 1)
              Tx_Zipcode.Text = .TextMatrix(.Row, 3)
              Lb_Amphoe_Id.Caption = .TextMatrix(.Row, 2)
    End With

GBQueryVillage.Filter = " Tambon_Id = '" & Trim$(Grid_Tambon.TextMatrix(Grid_Tambon.Row, 0)) & "'"

If GBQueryVillage.RecordCount > 0 Then
    Set Grid_VILLAGE.DataSource = GBQueryVillage
Else
   Set Grid_VILLAGE.DataSource = Nothing
          Grid_VILLAGE.Clear
          Grid_VILLAGE.Rows = 2
End If
    Call Set_Grid
End Sub

Private Sub Grid_VILLAGE_Click()
Call Grid_VILLAGE_RowColChange
End Sub

Private Sub Grid_VILLAGE_RowColChange()
If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then
     With Grid_VILLAGE
        Lb_Village_Id.Caption = Right$(.TextMatrix(.Row, 1), 2)
        Lb_Tambon_Id2.Caption = .TextMatrix(.Row, 3)
        Tx_Village_Name.Text = .TextMatrix(.Row, 2)
    End With
End If
End Sub

Private Sub Label2_Click(Index As Integer)
Select Case Index
             Case 106
                          Op_LM.Value = True
            Case 8
                        Op_Land_Use.Value = True
           Case 25
                       Op_Tambon.Value = True
           Case 26
                        Op_Village.Value = True
           Case 4
                       Chk_Street.Value = True
           Case 10
                       Op_Soi.Value = True
            Case 21
                      Op_ZONE.Value = True
            Case 22
                    Op_BLOCK.Value = True
            Case 6
                    Op_RateTax.Value = True
             Case 59
                    Op_Business.Value = True
             Case 83
                      Op_Class.Value = True
             Case 84
                      Op_BuildingType.Value = True
             Case 108
                      Op_BuildingRate.Value = True
             Case 111
                 Op_Cfg_Building.Value = True
             Case 140
                       Op_Cfg_Land.Value = True
             Case 149
                   Op_Cfg_Signbord.Value = True
End Select
End Sub

Private Sub List1_Click()
Txt_LM_Details.Text = List1.List(List1.ListIndex)
End Sub

Private Sub Op_BLOCK_Click()
  Label2(21).ForeColor = &H575555
  Label2(4).ForeColor = &H575555
  Label2(7).ForeColor = &H575555
  Label2(22).ForeColor = &HC2723B
  If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_BuildingRate_Click()

        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(84).ForeColor = &H575555
        Label2(108).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Call SET_TEXTBOX("CANCEL")
    Call SET_TEXTBOX("CANCEL")
End Sub
Private Sub Op_BuildingType_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(108).ForeColor = &H575555
        Label2(84).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_Business_Click()

        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_Cfg_Building_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(84).ForeColor = &H575555
        Label2(108).ForeColor = &H575555
        Label2(140).ForeColor = &H575555
        Label2(149).ForeColor = &H575555
        Label2(111).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Call SET_TEXTBOX("CANCEL")
    Call SET_TEXTBOX("CANCEL")
        
End Sub

Private Sub Op_Cfg_Land_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(84).ForeColor = &H575555
        Label2(108).ForeColor = &H575555
        Label2(149).ForeColor = &H575555
        Label2(111).ForeColor = &H575555
        Label2(140).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Call SET_TEXTBOX("CANCEL")
    Call SET_TEXTBOX("CANCEL")
End Sub


Private Sub Op_Cfg_Signbord_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(83).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(84).ForeColor = &H575555
        Label2(108).ForeColor = &H575555
        Label2(111).ForeColor = &H575555
        Label2(140).ForeColor = &H575555
        Label2(149).ForeColor = &HC2723B
          
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then Call SET_TEXTBOX("CANCEL")
    Call SET_TEXTBOX("CANCEL")
End Sub


Private Sub Op_Class_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(59).ForeColor = &H575555
        Label2(83).ForeColor = &HC2723B
        
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub


Private Sub Op_Land_Use_Click()

        Label2(6).ForeColor = &H575555
        Label2(106).ForeColor = &H575555
        Label2(8).ForeColor = &HC2723B
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_LM_Click()
        Label2(8).ForeColor = &H575555
        Label2(6).ForeColor = &H575555
        Label2(106).ForeColor = &HC2723B
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_RateTax_Click()

        Label2(8).ForeColor = &H575555
        Label2(106).ForeColor = &H575555
        Label2(6).ForeColor = &HC2723B
If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_Soi_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(4).ForeColor = &H575555
        Label2(10).ForeColor = &HC2723B
        If Btn_Add.Enabled = True Or Btn_Edit.Enabled = True Then Exit Sub

If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

'Private Sub c_Click()
'If Op_Tambon.Value Then
'    Call SET_TEXTBOX("CANCEL")
'        Label2(21).ForeColor = &H575555
'        Label2(22).ForeColor = &H575555
'        Label2(7).ForeColor = &H575555
'        Label2(26).ForeColor = &H575555
'        Label2(25).ForeColor = &HC2723B
'End If
'End Sub

Private Sub Op_Tambon_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(4).ForeColor = &H575555
        Label2(26).ForeColor = &H575555
        Label2(25).ForeColor = &HC2723B

If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_Village_Click()
        Label2(21).ForeColor = &H575555
        Label2(22).ForeColor = &H575555
        Label2(7).ForeColor = &H575555
        Label2(4).ForeColor = &H575555
        Label2(25).ForeColor = &H575555
        Label2(26).ForeColor = &HC2723B

If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Op_ZONE_Click()
  Label2(22).ForeColor = &H575555
  Label2(4).ForeColor = &H575555
  Label2(7).ForeColor = &H575555
  Label2(21).ForeColor = &HC2723B
  If Logic_Frm_Index_Menu Then
   Logic_Frm_Index_Menu = False
   Exit Sub
End If
    Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub OpMenu_Click(Index As Integer)
    Select Case Index
                Case 1
                        SSTab1.Tab = 0
                        Op_Tambon.Value = True
                Case 2
                        SSTab1.Tab = 6
                        Op_BuildingType.Value = True
                Case 3
                        SSTab1.Tab = 1
                        Chk_Street.Value = True
                Case 4
                        SSTab1.Tab = 2
                        Op_ZONE.Value = True
                Case 5
                        SSTab1.Tab = 3
                        Op_RateTax.Value = True
                Case 6
                        SSTab1.Tab = 4
                Case 7
                        SSTab1.Tab = 5
                        Op_Business.Value = True
                Case 8
                        SSTab1.Tab = 7
                        Op_Cfg_Building.Value = True
                Case 9
                        SSTab1.Tab = 8
                Case 10
                        SSTab1.Tab = 9
                        Opt_Request.Value = True
End Select
                Call SET_REFRESH
End Sub

'Private Sub Tx_STREET_LONG_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

'Private Sub Tx_Street_Name_KeyPress(KeyAscii As Integer)
'  If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

'Private Sub Tx_TAX_BN_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

'Private Sub Tx_TAX_NN_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

Private Sub Opt_Building_Type_Click()
        Call SET_REFRESH
        Call Set_Grid
        lb_Request_id.Caption = ""
        txt_Request.Text = ""
        Label2(172).Visible = False
        txt_Position.Visible = False
        Shape1(79).Visible = False
        txt_Request.Width = 9850
        Shape1(78).Width = 9910
        Opt_Request.ForeColor = &H575555
        Opt_Building_Type.ForeColor = &HC2723B
        Opt_Officer.ForeColor = &H575555
End Sub

Private Sub Opt_Officer_Click()
        Call SET_REFRESH
        Call Set_Grid
        Label2(172).Visible = True
        txt_Position.Visible = True
        Shape1(79).Visible = True
        txt_Request.Width = 4515
        Shape1(78).Width = 4575
        Opt_Request.ForeColor = &H575555
        Opt_Building_Type.ForeColor = &H575555
        Opt_Officer.ForeColor = &HC2723B
End Sub

Private Sub Opt_Request_Click()
        Call SET_REFRESH
        Call Set_Grid
        lb_Request_id.Caption = ""
        txt_Request.Text = ""
        Label2(172).Visible = False
        txt_Position.Visible = False
        Shape1(79).Visible = False
        txt_Request.Width = 9850
        Shape1(78).Width = 9910
        Opt_Request.ForeColor = &HC2723B
        Opt_Building_Type.ForeColor = &H575555
        Opt_Officer.ForeColor = &H575555
End Sub

Private Sub Tax_MPerD_Change()
If LenB(Trim$(Tax_MPerD.Text)) = 0 Then Tax_MPerD.Text = "0"
End Sub

Private Sub Tax_MPerD_GotFocus()
Tax_MPerD.SelStart = 0
Tax_MPerD.SelLength = Len(Tax_MPerD.Text)
End Sub

Private Sub Tax_MPerD_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Tx_Zipcode_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_B_Pot_Change()
If LenB(Trim$(Txt_B_Pot.Text)) = 0 Then Txt_B_Pot.Text = "0"
End Sub

Private Sub Txt_B_Pot_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123")
End Sub

Private Sub Txt_B_Rai_Change()
 If LenB(Trim$(Txt_B_Pot.Text)) = 0 Then Txt_B_Pot.Text = "0"
End Sub

Private Sub Txt_B_Rai_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_B_Va_Change()
If LenB(Trim$(Txt_B_Va.Text)) = 0 Then Txt_B_Va.Text = "0"
     If CDbl(Val(Txt_B_Va.Text)) > 99.99 Then
        Txt_B_Va.Text = "0"
     End If
End Sub

Private Sub Txt_B_Va_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub TXT_BLOCK_Change()
   If LenB(Trim$(TXT_BLOCK.Text)) = 0 Then TXT_BLOCK.Text = "0"
End Sub

Private Sub TXT_BLOCK_GotFocus()
  TXT_BLOCK.SelStart = 0
  TXT_BLOCK.SelLength = Len(TXT_BLOCK.Text)
End Sub

Private Sub TXT_BLOCK_KeyPress(KeyAscii As Integer)
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub TXT_BLOCK_LostFocus()
        If CInt(TXT_BLOCK.Text) > 26 Then
                TXT_BLOCK.Text = ""
                TXT_BLOCK.SetFocus
        End If
End Sub

Private Sub Txt_BuildingType_GotFocus()
Txt_BuildingType.SelStart = 0
Txt_BuildingType.SelLength = Len(Txt_BuildingType.Text)
End Sub

Private Sub Txt_Class_Price_Change()
    If LenB(Trim$(Txt_Class_Price.Text)) = 0 Then Txt_Class_Price.Text = "0.00"
End Sub

Private Sub Txt_Class_Price_GotFocus()
Txt_Class_Price.SelStart = 0
Txt_Class_Price.SelLength = Len(Txt_Class_Price.Text)
End Sub

Private Sub Txt_Class_Price_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Due_1Month_Change()
If LenB(Trim$(Txt_Due_1Month.Text)) = 0 Then Txt_Due_1Month.Text = "0"
End Sub

Private Sub Txt_Due_1Month_GotFocus()
Txt_Due_1Month.SelStart = 0
Txt_Due_1Month.SelLength = Len(Txt_Due_1Month.Text)
End Sub

Private Sub Txt_Due_1Month_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Due_2Month_Change()
If LenB(Trim$(Txt_Due_2Month.Text)) = 0 Then Txt_Due_2Month.Text = "0"
End Sub

Private Sub Txt_Due_2Month_GotFocus()
Txt_Due_2Month.SelStart = 0
Txt_Due_2Month.SelLength = Len(Txt_Due_2Month.Text)
End Sub

Private Sub Txt_Due_2Month_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Due_3Month_Change()
If LenB(Trim$(Txt_Due_3Month.Text)) = 0 Then Txt_Due_3Month.Text = "0"
End Sub

Private Sub Txt_Due_3Month_GotFocus()
Txt_Due_3Month.SelStart = 0
Txt_Due_3Month.SelLength = Len(Txt_Due_3Month.Text)
End Sub

Private Sub Txt_Due_3Month_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Due_4Month_Change()
If LenB(Trim$(Txt_Due_4Month.Text)) = 0 Then Txt_Due_4Month.Text = "0"
End Sub

Private Sub Txt_Due_4Month_GotFocus()
Txt_Due_4Month.SelStart = 0
Txt_Due_4Month.SelLength = Len(Txt_Due_4Month.Text)
End Sub

Private Sub Txt_Due_4Month_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Due_Accept0_Change()
If LenB(Trim$(Txt_Due_Accept0.Text)) = 0 Then Txt_Due_Accept0.Text = "0"
End Sub

Private Sub Txt_Due_Accept0_GotFocus()
Txt_Due_Accept0.SelStart = 0
Txt_Due_Accept0.SelLength = Len(Txt_Due_Accept0.Text)
End Sub

Private Sub Txt_Due_Accept0_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Due_Accept1_Change()
If LenB(Trim$(Txt_Due_Accept1.Text)) = 0 Then Txt_Due_Accept1.Text = "0"
End Sub

Private Sub Txt_Due_Accept1_GotFocus()
Txt_Due_Accept1.SelStart = 0
Txt_Due_Accept1.SelLength = Len(Txt_Due_Accept1.Text)
End Sub

Private Sub Txt_Due_Accept1_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Due_Accept2_Change()
If LenB(Trim$(Txt_Due_Accept2.Text)) = 0 Then Txt_Due_Accept2.Text = "0"
End Sub

Private Sub Txt_Due_Accept2_GotFocus()
Txt_Due_Accept2.SelStart = 0
Txt_Due_Accept2.SelLength = Len(Txt_Due_Accept2.Text)
End Sub

Private Sub Txt_Due_Accept2_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Due_Due_Appeal0_Change()
If LenB(Trim$(Txt_Due_Due_Appeal0.Text)) = 0 Then Txt_Due_Due_Appeal0.Text = "0"
End Sub

Private Sub Txt_Due_Due_Appeal0_GotFocus()
Txt_Due_Due_Appeal0.SelStart = 0
Txt_Due_Due_Appeal0.SelLength = Len(Txt_Due_Due_Appeal0.Text)
End Sub

Private Sub Txt_Due_Due_Appeal0_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Due_Due_Appeal1_Change()
If LenB(Trim$(Txt_Due_Due_Appeal1.Text)) = 0 Then Txt_Due_Due_Appeal1.Text = "0"
End Sub

Private Sub Txt_Due_Due_Appeal1_GotFocus()
Txt_Due_Due_Appeal1.SelStart = 0
Txt_Due_Due_Appeal1.SelLength = Len(Txt_Due_Due_Appeal1.Text)
End Sub

Private Sub Txt_Due_Due_Appeal1_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Due_Due_Appeal2_Change()
If LenB(Trim$(Txt_Due_Due_Appeal2.Text)) = 0 Then Txt_Due_Due_Appeal2.Text = "0"
End Sub

Private Sub Txt_Due_Due_Appeal2_GotFocus()
Txt_Due_Due_Appeal2.SelStart = 0
Txt_Due_Due_Appeal2.SelLength = Len(Txt_Due_Due_Appeal2.Text)
End Sub

Private Sub Txt_Due_Due_Appeal2_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Rate_LandLone_Change(Index As Integer)
If LenB(Trim$(Txt_Rate_LandLone(Index).Text)) = 0 Then Txt_Rate_LandLone(Index).Text = 0
End Sub

Private Sub Txt_Rate_LandLone_GotFocus(Index As Integer)
Txt_Rate_LandLone(Index).SelStart = 0
Txt_Rate_LandLone(Index).SelLength = Len(Txt_Rate_LandLone(Index).Text)
End Sub

Private Sub Txt_Rate_LandLone_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Rate_LandUse_Change(Index As Integer)
 If LenB(Trim$(Txt_Rate_LandUse(Index).Text)) = 0 Then Txt_Rate_LandUse(Index).Text = 0
End Sub

Private Sub Txt_Rate_LandUse_GotFocus(Index As Integer)
Txt_Rate_LandUse(Index).SelStart = 0
Txt_Rate_LandUse(Index).SelLength = Len(Txt_Rate_LandUse(Index).Text)
End Sub

Private Sub Txt_Rate_LandUse_KeyPress(Index As Integer, KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub TXT_SIGNBORD_LOW_Change(Index As Integer)
        If LenB(Trim$(TXT_SIGNBORD_LOW(Index).Text)) = 0 Then TXT_SIGNBORD_LOW(Index).Text = "0.00"
End Sub

Private Sub TXT_SIGNBORD_LOW_GotFocus(Index As Integer)
        TXT_SIGNBORD_LOW(Index).SelStart = 0
        TXT_SIGNBORD_LOW(Index).SelLength = Len(TXT_SIGNBORD_LOW(Index).Text)
End Sub

Private Sub TXT_SIGNBORD_LOW_KeyPress(Index As Integer, KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub TXT_SIGNBORD_RATE_Change(Index As Integer)
        If LenB(Trim$(TXT_SIGNBORD_RATE(Index).Text)) = 0 Then TXT_SIGNBORD_RATE(Index).Text = "0.00"
End Sub

Private Sub TXT_SIGNBORD_RATE_GotFocus(Index As Integer)
        TXT_SIGNBORD_RATE(Index).SelStart = 0
        TXT_SIGNBORD_RATE(Index).SelLength = Len(TXT_SIGNBORD_RATE(Index).Text)
End Sub

Private Sub TXT_SIGNBORD_RATE_KeyPress(Index As Integer, KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub TXT_SIGNBORD_RATE_OF_USE_Change(Index As Integer)
        If LenB(Trim$(TXT_SIGNBORD_RATE_OF_USE(Index).Text)) = 0 Then TXT_SIGNBORD_RATE_OF_USE(Index).Text = "0"
End Sub

Private Sub TXT_SIGNBORD_RATE_OF_USE_GotFocus(Index As Integer)
        TXT_SIGNBORD_RATE_OF_USE(Index).SelStart = 0
        TXT_SIGNBORD_RATE_OF_USE(Index).SelLength = Len(TXT_SIGNBORD_RATE_OF_USE(Index).Text)
End Sub

Private Sub TXT_SIGNBORD_RATE_OF_USE_KeyPress(Index As Integer, KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_Machine_Change()
        If LenB(Trim$(Txt_Tax_Machine.Text)) = 0 Then Txt_Tax_Machine.Text = "0"
End Sub

Private Sub Txt_Tax_Machine_GotFocus()
        Txt_Tax_Machine.SelStart = 0
        Txt_Tax_Machine.SelLength = Len(Txt_Tax_Machine.Text)
End Sub

Private Sub Txt_Tax_NotFully_Change()
        If LenB(Trim$(Txt_Tax_NotFully.Text)) = 0 Then Txt_Tax_NotFully.Text = "0"
End Sub

Private Sub Txt_Tax_NotFully_GotFocus()
        Txt_Tax_NotFully.SelStart = 0
        Txt_Tax_NotFully.SelLength = Len(Txt_Tax_NotFully.Text)
End Sub

Private Sub Txt_Tax_NotFully_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_NotFully2_Change()
        If LenB(Trim$(Txt_Tax_NotFully2.Text)) = 0 Then Txt_Tax_NotFully2.Text = "0"
End Sub

Private Sub Txt_Tax_NotFully2_GotFocus()
        Txt_Tax_NotFully2.SelStart = 0
        Txt_Tax_NotFully2.SelLength = Len(Txt_Tax_NotFully2.Text)
End Sub

Private Sub Txt_Tax_NotFully2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_OutSchedule1_Change()
        If LenB(Trim$(Txt_Tax_OutSchedule1.Text)) = 0 Then Txt_Tax_OutSchedule1.Text = "0"
End Sub

Private Sub Txt_Tax_OutSchedule1_GotFocus()
        Txt_Tax_OutSchedule1.SelStart = 0
        Txt_Tax_OutSchedule1.SelLength = Len(Txt_Tax_OutSchedule1.Text)
End Sub

Private Sub Txt_Tax_OutSchedule1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_OutSchedule2_Change()
        If LenB(Trim$(Txt_Tax_OutSchedule2.Text)) = 0 Then Txt_Tax_OutSchedule2.Text = "0"
End Sub

Private Sub Txt_Tax_OutSchedule2_GotFocus()
        Txt_Tax_OutSchedule2.SelStart = 0
        Txt_Tax_OutSchedule2.SelLength = Len(Txt_Tax_OutSchedule2.Text)
End Sub

Private Sub Txt_Tax_OutSchedule2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_OutTime1_Change()
        If LenB(Trim$(Txt_Tax_OutTime1.Text)) = 0 Then Txt_Tax_OutTime1.Text = "0"
End Sub

Private Sub Txt_Tax_OutTime1_GotFocus()
        Txt_Tax_OutTime1.SelStart = 0
        Txt_Tax_OutTime1.SelLength = Len(Txt_Tax_OutTime1.Text)
End Sub

Private Sub Txt_Tax_OutTime1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_OutTime2_Change()
        If LenB(Trim$(Txt_Tax_OutTime2.Text)) = 0 Then Txt_Tax_OutTime2.Text = "0"
End Sub

Private Sub Txt_Tax_OutTime2_GotFocus()
        Txt_Tax_OutTime2.SelStart = 0
        Txt_Tax_OutTime2.SelLength = Len(Txt_Tax_OutTime2.Text)
End Sub

Private Sub Txt_Tax_OutTime2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Tax_YPerM_Change()
        If LenB(Trim$(Txt_Tax_YPerM.Text)) = 0 Then Txt_Tax_YPerM.Text = "0"
End Sub

Private Sub Txt_Tax_YPerM_GotFocus()
        Txt_Tax_YPerM.SelStart = 0
        Txt_Tax_YPerM.SelLength = Len(Txt_Tax_YPerM.Text)
End Sub

Private Sub Txt_Tax_YPerM_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_TYPE_A_AREA_Change()
        If Txt_TYPE_A_AREA.Text = "" Then Txt_TYPE_A_AREA.Text = "0.00"
End Sub

Private Sub Txt_TYPE_A_AREA_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_TYPE_A_RATE1_Change()
        If LenB(Trim$(Txt_TYPE_A_RATE1.Text)) = 0 Then Txt_TYPE_A_RATE1.Text = "0.00"
End Sub

Private Sub Txt_TYPE_A_RATE1_GotFocus()
        Txt_TYPE_A_RATE1.SelStart = 0
        Txt_TYPE_A_RATE1.SelLength = Len(Txt_TYPE_A_RATE1.Text)
End Sub

Private Sub Txt_TYPE_A_RATE1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_A_RATE2_Change()
        If LenB(Trim$(Txt_TYPE_A_RATE2.Text)) = 0 Then Txt_TYPE_A_RATE2.Text = "0.00"
End Sub

Private Sub Txt_TYPE_A_RATE2_GotFocus()
        Txt_TYPE_A_RATE2.SelStart = 0
        Txt_TYPE_A_RATE2.SelLength = Len(Txt_TYPE_A_RATE2.Text)
End Sub

Private Sub Txt_TYPE_A_RATE2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_A_RATE3_Change()
        If LenB(Trim$(Txt_TYPE_A_RATE3.Text)) = 0 Then Txt_TYPE_A_RATE3.Text = "0.00"
End Sub

Private Sub Txt_TYPE_A_RATE3_GotFocus()
        Txt_TYPE_A_RATE3.SelStart = 0
        Txt_TYPE_A_RATE3.SelLength = Len(Txt_TYPE_A_RATE3.Text)
End Sub

Private Sub Txt_TYPE_A_RATE3_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_A_RATE4_Change()
        If LenB(Trim$(Txt_TYPE_A_RATE4.Text)) = 0 Then Txt_TYPE_A_RATE4.Text = "0.00"
End Sub

Private Sub Txt_TYPE_A_RATE4_GotFocus()
        Txt_TYPE_A_RATE4.SelStart = 0
        Txt_TYPE_A_RATE4.SelLength = Len(Txt_TYPE_A_RATE4.Text)
End Sub
Private Sub Txt_TYPE_B_RATE1_Change()
        If LenB(Trim$(Txt_TYPE_B_RATE1.Text)) = 0 Then Txt_TYPE_B_RATE1.Text = "0.00"
End Sub

Private Sub Txt_TYPE_B_RATE1_GotFocus()
        Txt_TYPE_B_RATE1.SelStart = 0
        Txt_TYPE_B_RATE1.SelLength = Len(Txt_TYPE_B_RATE1.Text)
End Sub

Private Sub Txt_TYPE_B_RATE1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_B_RATE2_Change()
        If LenB(Trim$(Txt_TYPE_B_RATE2.Text)) = 0 Then Txt_TYPE_B_RATE2.Text = "0.00"
End Sub

Private Sub Txt_TYPE_B_RATE2_GotFocus()
        Txt_TYPE_B_RATE2.SelStart = 0
        Txt_TYPE_B_RATE2.SelLength = Len(Txt_TYPE_B_RATE2.Text)
End Sub

Private Sub Txt_TYPE_B_RATE2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_C_RATE1_Change()
        If LenB(Trim$(Txt_TYPE_C_RATE1.Text)) = 0 Then Txt_TYPE_C_RATE1.Text = "0.00"
End Sub

Private Sub Txt_TYPE_C_RATE1_GotFocus()
        Txt_TYPE_C_RATE1.SelStart = 0
        Txt_TYPE_C_RATE1.SelLength = Len(Txt_TYPE_C_RATE1.Text)
End Sub

Private Sub Txt_TYPE_C_RATE1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_C_RATE2_Change()
        If LenB(Trim$(Txt_TYPE_C_RATE2.Text)) = 0 Then Txt_TYPE_C_RATE2.Text = "0.00"
End Sub

Private Sub Txt_TYPE_C_RATE2_GotFocus()
        Txt_TYPE_C_RATE2.SelStart = 0
        Txt_TYPE_C_RATE2.SelLength = Len(Txt_TYPE_C_RATE2.Text)
End Sub

Private Sub Txt_TYPE_C_RATE2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_TYPE_D_RATE1_Change()
        If LenB(Trim$(Txt_TYPE_D_RATE1.Text)) = 0 Then Txt_TYPE_D_RATE1.Text = "0.00"
End Sub

Private Sub Txt_TYPE_D_RATE1_GotFocus()
        Txt_TYPE_D_RATE1.SelStart = 0
        Txt_TYPE_D_RATE1.SelLength = Len(Txt_TYPE_D_RATE1.Text)
End Sub

Private Sub Txt_TYPE_D_RATE1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_Zone_Change()
        If LenB(Trim$(Txt_Zone.Text)) = 0 Then Txt_Zone.Text = "0"
End Sub

Private Sub Txt_Zone_GotFocus()
        Txt_Zone.SelStart = 0
        Txt_Zone.SelLength = Len(Txt_Zone.Text)
End Sub

Private Sub Txt_Zone_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_Zone_Price_Change()
        If LenB(Trim$(Txt_Zone_Price.Text)) = 0 Then Txt_Zone_Price.Text = "0.00"
End Sub

Private Sub Txt_Zone_Price_GotFocus()
        Txt_Zone_Price.SelStart = 0
        Txt_Zone_Price.SelLength = Len(Txt_Zone_Price.Text)
End Sub

Private Sub Txt_Zone_Price_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_Zone_Rate_Change()
        If LenB(Trim$(Txt_Zone_Rate.Text)) = 0 Then Txt_Zone_Rate.Text = "0.00"
End Sub

Private Sub Txt_Zone_Rate_GotFocus()
        Txt_Zone_Rate.SelStart = 0
        Txt_Zone_Rate.SelLength = Len(Txt_Zone_Rate.Text)
End Sub

Private Sub Txt_Zone_Rate_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub
