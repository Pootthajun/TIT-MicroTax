VERSION 5.00
Object = "{D27CDB6B-AE6D-11CF-96B8-444553540000}#1.0#0"; "Flash10a.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form Frm_CalAllTax 
   ClientHeight    =   9840
   ClientLeft      =   270
   ClientTop       =   60
   ClientWidth     =   12840
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Picture         =   "Frm_CalAllTax.frx":0000
   ScaleHeight     =   9840
   ScaleWidth      =   12840
   WindowState     =   2  'Maximized
   Begin VB.CommandButton BtnClear 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   12360
      Picture         =   "Frm_CalAllTax.frx":211BBC
      Style           =   1  'Graphical
      TabIndex        =   159
      Top             =   660
      Width           =   375
   End
   Begin VB.ComboBox Cmb_Type 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_CalAllTax.frx":212146
      Left            =   990
      List            =   "Frm_CalAllTax.frx":212168
      Style           =   2  'Dropdown List
      TabIndex        =   154
      Top             =   1133
      Width           =   2385
   End
   Begin VB.CommandButton BtnSearch 
      BackColor       =   &H00E0E0E0&
      Height          =   345
      Left            =   11880
      Picture         =   "Frm_CalAllTax.frx":2121C3
      Style           =   1  'Graphical
      TabIndex        =   138
      Top             =   660
      Width           =   405
   End
   Begin VB.OptionButton OptionChkType 
      BackColor       =   &H00C0C0C0&
      Caption         =   "������ �"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   255
      Index           =   1
      Left            =   10710
      TabIndex        =   137
      Top             =   721
      Width           =   1095
   End
   Begin VB.OptionButton OptionChkType 
      BackColor       =   &H00C0C0C0&
      Caption         =   "�.�."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   255
      Index           =   0
      Left            =   10020
      TabIndex        =   136
      Top             =   721
      Value           =   -1  'True
      Width           =   735
   End
   Begin VB.Frame FramePayMent 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   120
      TabIndex        =   43
      Top             =   8400
      Width           =   3250
      Begin VB.PictureBox PicPayment 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   720
         MouseIcon       =   "Frm_CalAllTax.frx":21274D
         MousePointer    =   99  'Custom
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   45
         Top             =   75
         Width           =   480
      End
      Begin VB.Label LbPaymentTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "�ʹ��ҧ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004040&
         Height          =   255
         Left            =   1320
         TabIndex        =   44
         Top             =   180
         Width           =   1215
      End
      Begin VB.Shape Shape4 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   615
         Left            =   0
         Shape           =   4  'Rounded Rectangle
         Top             =   0
         Width           =   3225
      End
   End
   Begin Crystal.CrystalReport CTReport 
      Left            =   150
      Top             =   90
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame FrameSchNm 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   390
      Left            =   4320
      TabIndex        =   9
      Top             =   659
      Visible         =   0   'False
      Width           =   5500
      Begin VB.TextBox txtSurNm 
         Height          =   315
         Left            =   3450
         TabIndex        =   11
         Top             =   10
         Width           =   2085
      End
      Begin VB.TextBox txtNm 
         Height          =   315
         Left            =   450
         TabIndex        =   10
         Top             =   10
         Width           =   2085
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���ʡ�� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   5
         Left            =   2625
         TabIndex        =   13
         Top             =   45
         Width           =   780
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   3
         Left            =   75
         TabIndex        =   12
         Top             =   45
         Width           =   345
      End
   End
   Begin VB.TextBox txtIDCard 
      Height          =   315
      Left            =   4320
      TabIndex        =   8
      Top             =   659
      Width           =   5415
   End
   Begin VB.ComboBox ComboTypeSch 
      Height          =   315
      ItemData        =   "Frm_CalAllTax.frx":212A57
      Left            =   3000
      List            =   "Frm_CalAllTax.frx":212A67
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   659
      Width           =   1335
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7785
      Left            =   3570
      TabIndex        =   15
      Top             =   1860
      Width           =   9225
      _ExtentX        =   16272
      _ExtentY        =   13732
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      TabHeight       =   520
      BackColor       =   -2147483638
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "������Ẻ"
      TabPicture(0)   =   "Frm_CalAllTax.frx":212A9E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label2(4)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Image1(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Lb_Count"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label2(1)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label2(3)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Lb_money"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label3(30)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label3(31)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Shape2(1)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label3(50)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label2(2)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "MaskEdBox2"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "MaskEdBox1"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Grid_Result_PBT5"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Option1"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Option2"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "BtnPostPreformTax"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Btn_Print"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "cmd_Save_Date"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "cmd_Change_Date_Install"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Txt_PBT_NO"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Chk_No_Used"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).ControlCount=   22
      TabCaption(1)   =   "��û����Թ"
      TabPicture(1)   =   "Frm_CalAllTax.frx":212ABA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Image1(1)"
      Tab(1).Control(1)=   "Label3(7)"
      Tab(1).Control(2)=   "Label3(8)"
      Tab(1).Control(3)=   "Lb_moneyAccept"
      Tab(1).Control(4)=   "Label3(29)"
      Tab(1).Control(5)=   "Shape2(0)"
      Tab(1).Control(6)=   "Label3(25)"
      Tab(1).Control(7)=   "LB_Sum"
      Tab(1).Control(8)=   "Label3(26)"
      Tab(1).Control(9)=   "Shape1(0)"
      Tab(1).Control(10)=   "Label3(6)"
      Tab(1).Control(11)=   "Lb_NmAccept"
      Tab(1).Control(12)=   "Label3(52)"
      Tab(1).Control(13)=   "Lb_AcceptNo"
      Tab(1).Control(14)=   "Label3(22)"
      Tab(1).Control(15)=   "Label3(23)"
      Tab(1).Control(16)=   "Label3(51)"
      Tab(1).Control(17)=   "MaskEdBox6"
      Tab(1).Control(18)=   "Grid_Result_PBT9"
      Tab(1).Control(19)=   "Command1"
      Tab(1).Control(20)=   "btn_Print_Accept"
      Tab(1).Control(21)=   "btnPostAcceptTax"
      Tab(1).Control(22)=   "Text1"
      Tab(1).Control(23)=   "cmd_Search_No_Accept"
      Tab(1).Control(24)=   "ComboNoAccept"
      Tab(1).Control(25)=   "ComboPBT5No"
      Tab(1).ControlCount=   26
      TabCaption(2)   =   "��ê�������"
      TabPicture(2)   =   "Frm_CalAllTax.frx":212AD6
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Image1(2)"
      Tab(2).Control(1)=   "Label3(32)"
      Tab(2).Control(2)=   "Shape2(2)"
      Tab(2).Control(3)=   "Shape2(4)"
      Tab(2).Control(4)=   "Grid_Result"
      Tab(2).Control(5)=   "BtnPostPayment"
      Tab(2).Control(6)=   "Btn_Print_Payment"
      Tab(2).Control(7)=   "OptionPayment1"
      Tab(2).Control(8)=   "OptionPayment2"
      Tab(2).Control(9)=   "FramePayNormal"
      Tab(2).Control(10)=   "FramePayInsallment"
      Tab(2).Control(11)=   "ChkInstallment"
      Tab(2).Control(12)=   "ComboPayTax"
      Tab(2).Control(13)=   "Btn_Print_PBA1"
      Tab(2).ControlCount=   14
      Begin VB.CommandButton Btn_Print_PBA1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "������Ѻ�Թ"
         Height          =   555
         Left            =   -68460
         Picture         =   "Frm_CalAllTax.frx":212AF2
         Style           =   1  'Graphical
         TabIndex        =   162
         Top             =   6660
         Width           =   1820
      End
      Begin VB.ComboBox ComboPayTax 
         Height          =   315
         Left            =   -73320
         TabIndex        =   157
         Top             =   90
         Width           =   1455
      End
      Begin VB.ComboBox ComboPBT5No 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -73560
         TabIndex        =   156
         Top             =   435
         Width           =   1695
      End
      Begin VB.ComboBox ComboNoAccept 
         Height          =   315
         ItemData        =   "Frm_CalAllTax.frx":2131DC
         Left            =   -72825
         List            =   "Frm_CalAllTax.frx":2131DE
         TabIndex        =   146
         Top             =   5055
         Width           =   1575
      End
      Begin VB.CommandButton cmd_Search_No_Accept 
         BackColor       =   &H00E0E0E0&
         Height          =   345
         Left            =   -71160
         Picture         =   "Frm_CalAllTax.frx":2131E0
         Style           =   1  'Graphical
         TabIndex        =   145
         Top             =   5040
         Width           =   555
      End
      Begin VB.CheckBox Chk_No_Used 
         Caption         =   "Check1"
         Height          =   195
         Left            =   200
         TabIndex        =   142
         Top             =   750
         Width           =   195
      End
      Begin VB.CheckBox ChkInstallment 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�����繧Ǵ"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   139
         Top             =   2085
         Width           =   1335
      End
      Begin VB.Frame FramePayInsallment 
         Height          =   4280
         Left            =   -65985
         TabIndex        =   74
         Top             =   2370
         Visible         =   0   'False
         Width           =   9135
         Begin VB.ComboBox Combo3 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "Frm_CalAllTax.frx":21376A
            Left            =   4320
            List            =   "Frm_CalAllTax.frx":21377A
            Style           =   2  'Dropdown List
            TabIndex        =   134
            Top             =   3840
            Width           =   1395
         End
         Begin VB.CheckBox Chk_RoundMoney2 
            Height          =   195
            Left            =   4080
            TabIndex        =   133
            Top             =   3960
            Width           =   225
         End
         Begin VB.TextBox txtSumAmount 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   315
            Left            =   7560
            TabIndex        =   129
            Text            =   "0.00"
            Top             =   1530
            Width           =   1275
         End
         Begin VB.TextBox txtSummary 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   1710
            TabIndex        =   128
            Text            =   "0.00"
            Top             =   1555
            Width           =   1515
         End
         Begin VB.TextBox txtBookNumber 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4680
            MaxLength       =   8
            TabIndex        =   125
            Top             =   840
            Width           =   1515
         End
         Begin VB.TextBox txtBookNo 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7560
            MaxLength       =   12
            TabIndex        =   124
            Top             =   840
            Width           =   1275
         End
         Begin VB.CheckBox Chk_RoundMoney1 
            Height          =   195
            Left            =   4680
            TabIndex        =   116
            Top             =   1635
            Width           =   225
         End
         Begin VB.ComboBox Combo2 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "Frm_CalAllTax.frx":2137B0
            Left            =   4950
            List            =   "Frm_CalAllTax.frx":2137C0
            Style           =   2  'Dropdown List
            TabIndex        =   115
            Top             =   1555
            Width           =   1275
         End
         Begin VB.TextBox txtPayInstallment 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   7440
            TabIndex        =   75
            Text            =   "0"
            Top             =   3860
            Width           =   1155
         End
         Begin MSMask.MaskEdBox MaskEdBox7 
            Height          =   345
            Left            =   7560
            TabIndex        =   76
            Top             =   480
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   609
            _Version        =   393216
            BackColor       =   12632256
            ForeColor       =   0
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox MaskEdBox8 
            Height          =   345
            Left            =   4440
            TabIndex        =   77
            Top             =   3480
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   609
            _Version        =   393216
            BackColor       =   12632256
            ForeColor       =   12582912
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Installment 
            Height          =   1200
            Left            =   30
            TabIndex        =   78
            Top             =   2205
            Width           =   9060
            _ExtentX        =   15981
            _ExtentY        =   2117
            _Version        =   393216
            BackColor       =   -2147483624
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   8421504
            ForeColorFixed  =   16777215
            BackColorSel    =   14079702
            ForeColorSel    =   8079449
            BackColorBkg    =   12632256
            GridColor       =   9806502
            GridColorFixed  =   4210752
            FocusRect       =   0
            ScrollBars      =   0
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   7
            _Band(0).GridLinesBand=   1
            _Band(0).TextStyleBand=   0
            _Band(0).TextStyleHeader=   0
         End
         Begin VB.Label LbFinePrice 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   315
            Left            =   7440
            TabIndex        =   141
            Top             =   3495
            Width           =   1155
         End
         Begin VB.Label LbArrear 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   315
            Left            =   1440
            TabIndex        =   140
            Top             =   3840
            Width           =   1275
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ѵ��� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   47
            Left            =   3360
            TabIndex        =   135
            Top             =   3930
            Width           =   645
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ�Թ�Ǵ :                       �ҷ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   49
            Left            =   120
            TabIndex        =   132
            Top             =   3885
            Width           =   2970
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��һ�Ѻ�����Թ�Ǵ :                     �ҷ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   48
            Left            =   5760
            TabIndex        =   131
            Top             =   3600
            Width           =   3210
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "���շ����� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   46
            Left            =   6420
            TabIndex        =   130
            Top             =   1605
            Width           =   1020
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "������� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   45
            Left            =   4080
            TabIndex        =   127
            Top             =   900
            Width           =   525
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ��� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   44
            Left            =   6930
            TabIndex        =   126
            Top             =   900
            Width           =   510
         End
         Begin VB.Label Lb_Discount1 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   285
            Left            =   7560
            TabIndex        =   123
            Top             =   1200
            Width           =   1275
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Թ����/Ŵ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   43
            Left            =   6390
            TabIndex        =   122
            Top             =   1245
            Width           =   1050
         End
         Begin VB.Label Lb_MoneyDue1 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   4680
            TabIndex        =   121
            Top             =   1200
            Width           =   1515
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��һ�Ѻ�����Թ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   42
            Left            =   3360
            TabIndex        =   120
            Top             =   1245
            Width           =   1245
         End
         Begin VB.Label Lb_MoneyDue_Perform1 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1710
            TabIndex        =   119
            Top             =   1200
            Width           =   1515
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��һ�Ѻ���Ẻ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   41
            Left            =   420
            TabIndex        =   118
            Top             =   1245
            Width           =   1215
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ѵ��ɤ������ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   37
            Left            =   3465
            TabIndex        =   117
            Top             =   1640
            Width           =   1140
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00D1ADAD&
            Caption         =   "��������´����Ѻ���������觨����繧Ǵ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   0
            Left            =   45
            TabIndex        =   88
            Top             =   120
            Width           =   9030
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00D1ADAD&
            Caption         =   "��¡�çǴ����������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   1
            Left            =   45
            TabIndex        =   87
            Top             =   1920
            Width           =   9030
         End
         Begin VB.Label LbInstallmentID 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1710
            TabIndex        =   86
            Top             =   840
            Width           =   1515
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ�駢ͪ����繧Ǵ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   34
            Left            =   5550
            TabIndex        =   85
            Top             =   525
            Width           =   1890
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��Ť������ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   35
            Left            =   795
            TabIndex        =   84
            Top             =   1640
            Width           =   840
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ�������繧Ǵ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   36
            Left            =   120
            TabIndex        =   83
            Top             =   900
            Width           =   1515
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ������ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   38
            Left            =   3480
            TabIndex        =   82
            Top             =   3525
            Width           =   840
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ǵ��� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   39
            Left            =   720
            TabIndex        =   81
            Top             =   3525
            Width           =   570
         End
         Begin VB.Label LbInstallmentNo 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   315
            Left            =   1440
            TabIndex        =   80
            Top             =   3480
            Width           =   1275
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ�Թ���� :                     �ҷ"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   40
            Left            =   6080
            TabIndex        =   79
            Top             =   3930
            Width           =   2895
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00BAD7E6&
            BorderColor     =   &H00FFFFFF&
            Height          =   1725
            Index           =   5
            Left            =   30
            Top             =   360
            Width           =   9030
         End
      End
      Begin VB.Frame FramePayNormal 
         Height          =   4260
         Left            =   -74955
         TabIndex        =   73
         Top             =   2370
         Width           =   9135
         Begin VB.TextBox Txt_BOOK_NO 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   6975
            MaxLength       =   12
            TabIndex        =   3
            Top             =   2295
            Width           =   2025
         End
         Begin VB.ComboBox Combo1 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            ItemData        =   "Frm_CalAllTax.frx":2137F6
            Left            =   4200
            List            =   "Frm_CalAllTax.frx":213806
            Style           =   2  'Dropdown List
            TabIndex        =   91
            Top             =   3675
            Width           =   1695
         End
         Begin VB.CheckBox Chk_RoundMoney 
            Height          =   195
            Left            =   3930
            TabIndex        =   90
            Top             =   3930
            Width           =   225
         End
         Begin VB.TextBox Txt_RoundMoney 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   480
            Left            =   6975
            TabIndex        =   4
            Text            =   "0.00"
            Top             =   3690
            Width           =   2025
         End
         Begin VB.TextBox Txt_Summary 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   480
            Left            =   6975
            Locked          =   -1  'True
            TabIndex        =   89
            Text            =   "0.00"
            Top             =   3210
            Width           =   2025
         End
         Begin VB.TextBox Txt_BOOK_NUMBER 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   4215
            MaxLength       =   8
            TabIndex        =   2
            Top             =   2295
            Width           =   1635
         End
         Begin MSMask.MaskEdBox MaskEdBox4 
            Height          =   465
            Left            =   4215
            TabIndex        =   1
            Top             =   2775
            Width           =   1635
            _ExtentX        =   2884
            _ExtentY        =   820
            _Version        =   393216
            BackColor       =   16777215
            ForeColor       =   0
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label lb_Date_Accept 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   4695
            TabIndex        =   161
            Top             =   960
            Width           =   1275
         End
         Begin VB.Label lb_Date_Perform 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1530
            TabIndex        =   160
            Top             =   960
            Width           =   1275
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ������Ẻ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   15
            Left            =   345
            TabIndex        =   114
            Top             =   580
            Width           =   1080
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ������Ẻ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   10
            Left            =   390
            TabIndex        =   113
            Top             =   1020
            Width           =   1035
         End
         Begin VB.Label Lb_PBT_NO 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1530
            TabIndex        =   112
            Top             =   540
            Width           =   1275
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��һ�Ѻ���Ẻ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   9
            Left            =   210
            TabIndex        =   111
            Top             =   1390
            Width           =   1215
         End
         Begin VB.Label Lb_MoneyDue_Perform 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1530
            TabIndex        =   110
            Top             =   1370
            Width           =   1275
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Թ��˹� (�ѹ) :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   20
            Left            =   6165
            TabIndex        =   109
            Top             =   1020
            Width           =   1350
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��һ�Ѻ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   19
            Left            =   3945
            TabIndex        =   108
            Top             =   1395
            Width           =   645
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ����駻����Թ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   18
            Left            =   3150
            TabIndex        =   107
            Top             =   580
            Width           =   1440
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ����Ѻ�駻����Թ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   13
            Left            =   2970
            TabIndex        =   106
            Top             =   1020
            Width           =   1620
         End
         Begin VB.Label Lb_DateOver 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   7575
            TabIndex        =   105
            Top             =   945
            Width           =   1275
         End
         Begin VB.Label Lb_MoneyDue 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   4695
            TabIndex        =   104
            Top             =   1365
            Width           =   1275
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Թ����/Ŵ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   195
            Index           =   11
            Left            =   6465
            TabIndex        =   103
            Top             =   1395
            Width           =   1050
         End
         Begin VB.Label Lb_discount 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   7575
            TabIndex        =   102
            Top             =   1370
            Width           =   1275
         End
         Begin VB.Label Lb_PBT_NO_ACCEPT_OLD 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   4695
            TabIndex        =   101
            Top             =   540
            Width           =   1275
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00D1ADAD&
            Caption         =   "��¡�ê�������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   20
            Left            =   45
            TabIndex        =   100
            Top             =   1920
            Width           =   9030
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H005C5C5F&
            BorderColor     =   &H005C5C5F&
            Height          =   1995
            Index           =   2
            Left            =   30
            Top             =   2220
            Width           =   9060
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ѹ���� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   21
            Left            =   3360
            TabIndex        =   99
            Top             =   2910
            Width           =   765
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ţ��� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   14
            Left            =   6360
            TabIndex        =   98
            Top             =   2475
            Width           =   540
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�Ѵ��ɤ������ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   12
            Left            =   2625
            TabIndex        =   97
            Top             =   3870
            Width           =   1200
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "������� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   16
            Left            =   3585
            TabIndex        =   96
            Top             =   2475
            Width           =   540
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "��Ť������ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   17
            Left            =   6015
            TabIndex        =   95
            Top             =   3360
            Width           =   885
         End
         Begin VB.Label LbCountPayment 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   14.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   6975
            TabIndex        =   94
            Top             =   2760
            Width           =   2025
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "�ӹǹ :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H005C5C5F&
            Height          =   240
            Index           =   24
            Left            =   6240
            TabIndex        =   93
            Top             =   2910
            Width           =   660
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00D1ADAD&
            Caption         =   "��������´����Ѻ��������"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   2
            Left            =   45
            TabIndex        =   92
            Top             =   120
            Width           =   9030
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00BAD7E6&
            BorderColor     =   &H00FFFFFF&
            Height          =   1770
            Index           =   1
            Left            =   45
            Top             =   120
            Width           =   9030
         End
      End
      Begin VB.OptionButton OptionPayment2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "��˹��ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Left            =   -67155
         TabIndex        =   70
         Top             =   135
         Value           =   -1  'True
         Width           =   1180
      End
      Begin VB.OptionButton OptionPayment1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�������շ�����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Left            =   -68910
         TabIndex        =   69
         Top             =   135
         Width           =   1700
      End
      Begin VB.CommandButton Btn_Print_Payment 
         BackColor       =   &H00E0E0E0&
         Height          =   555
         Left            =   -70230
         Picture         =   "Frm_CalAllTax.frx":21383C
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   6660
         Width           =   1780
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   360
         Left            =   -73185
         MaxLength       =   7
         TabIndex        =   57
         Top             =   2978
         Width           =   2025
      End
      Begin VB.CommandButton btnPostAcceptTax 
         BackColor       =   &H00E0E0E0&
         Caption         =   "�Ѵ����¡�� �.�.�.9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   -72345
         Picture         =   "Frm_CalAllTax.frx":213F26
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   3578
         Width           =   2025
      End
      Begin VB.CommandButton btn_Print_Accept 
         BackColor       =   &H00E0E0E0&
         Height          =   555
         Left            =   -70305
         Picture         =   "Frm_CalAllTax.frx":2144B0
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   3578
         Width           =   2025
      End
      Begin VB.CommandButton Command1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "�Ѵ�ӡ���Ѻ�駻����Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   -71400
         Picture         =   "Frm_CalAllTax.frx":214B9A
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   6360
         Width           =   2355
      End
      Begin VB.TextBox Txt_PBT_NO 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   360
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   51
         Top             =   225
         Width           =   945
      End
      Begin VB.CommandButton cmd_Change_Date_Install 
         BackColor       =   &H00E0E0E0&
         Caption         =   "����¹�ѹ���Դ���"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1440
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   5085
         Visible         =   0   'False
         Width           =   1665
      End
      Begin VB.CommandButton cmd_Save_Date 
         BackColor       =   &H00E0E0E0&
         Height          =   405
         Left            =   1440
         Picture         =   "Frm_CalAllTax.frx":215124
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "�ѹ�֡�ѹ�������¹�ŧ"
         Top             =   5475
         Visible         =   0   'False
         Width           =   1665
      End
      Begin VB.CommandButton Btn_Print 
         BackColor       =   &H00E0E0E0&
         Height          =   555
         Left            =   5310
         Picture         =   "Frm_CalAllTax.frx":2157E6
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   6285
         Width           =   2205
      End
      Begin VB.CommandButton BtnPostPreformTax 
         BackColor       =   &H00E0E0E0&
         Caption         =   "�Ѵ����¡�� �.�.�.5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3000
         Picture         =   "Frm_CalAllTax.frx":215ED0
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   6285
         Width           =   2355
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "��˹��ͧ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Left            =   7770
         TabIndex        =   17
         Top             =   795
         Width           =   1245
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "���Ẻ������"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   255
         Left            =   6180
         TabIndex        =   16
         Top             =   795
         Width           =   1545
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result_PBT5 
         Height          =   3555
         Left            =   45
         TabIndex        =   22
         Top             =   1080
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   6271
         _Version        =   393216
         BackColor       =   14737632
         Cols            =   12
         FixedCols       =   0
         BackColorFixed  =   8421504
         ForeColorFixed  =   16777215
         BackColorSel    =   9884603
         ForeColorSel    =   16777215
         BackColorBkg    =   12632256
         GridColor       =   11184810
         GridColorFixed  =   4210752
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   12
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSMask.MaskEdBox MaskEdBox1 
         Height          =   345
         Left            =   7335
         TabIndex        =   23
         Top             =   4695
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   609
         _Version        =   393216
         AllowPrompt     =   -1  'True
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MaskEdBox2 
         Height          =   345
         Left            =   1440
         TabIndex        =   24
         Top             =   4695
         Visible         =   0   'False
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   609
         _Version        =   393216
         AllowPrompt     =   -1  'True
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result_PBT9 
         Height          =   1845
         Left            =   -74925
         TabIndex        =   58
         Top             =   945
         Width           =   9045
         _ExtentX        =   15954
         _ExtentY        =   3254
         _Version        =   393216
         BackColor       =   14737632
         Cols            =   6
         FixedCols       =   0
         BackColorFixed  =   8421504
         ForeColorFixed  =   16777215
         BackColorSel    =   9884603
         ForeColorSel    =   4210752
         BackColorBkg    =   12632256
         GridColor       =   8421504
         GridColorFixed  =   4210752
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.CommandButton BtnPostPayment 
         BackColor       =   &H00E0E0E0&
         Caption         =   "�������� �.�.7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   -72000
         Picture         =   "Frm_CalAllTax.frx":21645A
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   6660
         Width           =   1780
      End
      Begin MSMask.MaskEdBox MaskEdBox6 
         Height          =   345
         Left            =   -69105
         TabIndex        =   147
         Top             =   5040
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   609
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "99/99/9999"
         PromptChar      =   "_"
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Result 
         Height          =   1620
         Left            =   -74940
         TabIndex        =   158
         Top             =   435
         Width           =   9120
         _ExtentX        =   16087
         _ExtentY        =   2858
         _Version        =   393216
         BackColor       =   -2147483624
         Cols            =   9
         FixedCols       =   0
         BackColorFixed  =   8421504
         ForeColorFixed  =   16777215
         BackColorSel    =   14079702
         ForeColorSel    =   8079449
         BackColorBkg    =   12632256
         GridColor       =   9806502
         GridColorFixed  =   4210752
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   9
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   2
         Left            =   6240
         TabIndex        =   26
         Top             =   4785
         Width           =   1050
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ѻ��駻����Թ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   51
         Left            =   -74520
         TabIndex        =   153
         Top             =   5085
         Width           =   1500
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ����Ѻ�� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   23
         Left            =   -70260
         TabIndex        =   152
         Top             =   5085
         Width           =   1020
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   22
         Left            =   -69720
         TabIndex        =   151
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   5535
         Width           =   1110
      End
      Begin VB.Label Lb_AcceptNo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Left            =   -68520
         TabIndex        =   150
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   5535
         Width           =   2205
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   52
         Left            =   -74520
         TabIndex        =   149
         Top             =   5535
         Width           =   345
      End
      Begin VB.Label Lb_NmAccept 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Left            =   -73920
         TabIndex        =   148
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   5535
         Width           =   3765
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "�Ѻ�Ţ����駻����Թ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         Index           =   6
         Left            =   -74880
         TabIndex        =   144
         Top             =   4545
         Width           =   8895
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00000000&
         Height          =   1515
         Index           =   0
         Left            =   -74880
         Top             =   4550
         Width           =   8895
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Ŵ�ʹ����"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   50
         Left            =   120
         TabIndex        =   143
         Top             =   720
         Width           =   1335
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   300
         Index           =   4
         Left            =   -67530
         Shape           =   4  'Rounded Rectangle
         Top             =   2070
         Width           =   1695
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   345
         Index           =   1
         Left            =   5970
         Top             =   720
         Width           =   3195
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   345
         Index           =   2
         Left            =   -69030
         Top             =   75
         Width           =   3195
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��¡�ê������� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   32
         Left            =   -74835
         TabIndex        =   68
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   105
         Width           =   1470
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   26
         Left            =   -68340
         TabIndex        =   65
         Top             =   630
         Width           =   510
      End
      Begin VB.Label LB_Sum 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Left            =   -67680
         TabIndex        =   64
         Top             =   630
         Width           =   1005
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "��¡��"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   25
         Left            =   -66570
         TabIndex        =   63
         Top             =   630
         Width           =   510
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00C0C0FF&
         Height          =   345
         Index           =   0
         Left            =   -68490
         Top             =   585
         Width           =   2595
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   29
         Left            =   -74745
         TabIndex        =   62
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   495
         Width           =   1110
      End
      Begin VB.Label Lb_moneyAccept 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   345
         Left            =   -68235
         TabIndex        =   61
         Top             =   2948
         Width           =   1665
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ҥһ����Թ :                                �ҷ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   8
         Left            =   -69495
         TabIndex        =   60
         Top             =   3038
         Width           =   3420
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ����駻����Թ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   7
         Left            =   -74865
         TabIndex        =   59
         Top             =   3098
         Width           =   1485
      End
      Begin VB.Image Image1 
         Height          =   7470
         Index           =   2
         Left            =   -75000
         Top             =   0
         Width           =   9225
      End
      Begin VB.Image Image1 
         Height          =   7470
         Index           =   1
         Left            =   -75000
         Top             =   0
         Width           =   9195
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "/47"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   31
         Left            =   2430
         TabIndex        =   53
         Top             =   255
         Width           =   360
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�Ţ������Ẻ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   30
         Left            =   195
         TabIndex        =   52
         Tag             =   "�Ţ���Ǩ��� :"
         Top             =   250
         Width           =   1110
      End
      Begin VB.Label Lb_money 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   345
         Left            =   7335
         TabIndex        =   50
         Top             =   5430
         Width           =   1665
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�Ҥ���� (�ҷ) :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   3
         Left            =   5880
         TabIndex        =   49
         Top             =   5520
         Width           =   1410
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ӹǹ�ŧ :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   1
         Left            =   6195
         TabIndex        =   48
         Top             =   5160
         Width           =   1095
      End
      Begin VB.Label Lb_Count 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   345
         Left            =   7335
         TabIndex        =   47
         Top             =   5070
         Width           =   1665
      End
      Begin VB.Image Image1 
         Height          =   7470
         Index           =   0
         Left            =   0
         Top             =   0
         Width           =   9195
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�ѹ���Դ��駻��� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005C5C5F&
         Height          =   240
         Index           =   4
         Left            =   120
         TabIndex        =   25
         Top             =   4785
         Visible         =   0   'False
         Width           =   1260
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_ListName 
      Height          =   2925
      Left            =   75
      TabIndex        =   27
      Top             =   1920
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   5159
      _Version        =   393216
      BackColor       =   -2147483624
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      RowSizingMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_TaxCount 
      Height          =   1605
      Left            =   75
      TabIndex        =   35
      Top             =   5520
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   2831
      _Version        =   393216
      BackColor       =   -2147483624
      Cols            =   3
      FixedCols       =   0
      BackColorFixed  =   8421504
      ForeColorFixed  =   16777215
      BackColorSel    =   14079702
      ForeColorSel    =   8079449
      BackColorBkg    =   12632256
      GridColor       =   9806502
      GridColorFixed  =   4210752
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   3
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   1560
      TabIndex        =   42
      Top             =   659
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   582
      _Version        =   393216
      Value           =   2500
      BuddyControl    =   "Lb_Year"
      BuddyDispid     =   196699
      OrigLeft        =   3000
      OrigTop         =   240
      OrigRight       =   3255
      OrigBottom      =   570
      Max             =   3500
      Min             =   2500
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   0
      Left            =   150
      TabIndex        =   155
      Top             =   1200
      Width           =   765
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   465
      Index           =   5
      Left            =   75
      Shape           =   4  'Rounded Rectangle
      Top             =   1080
      Width           =   3465
   End
   Begin VB.Label LbYearSelect 
      BackStyle       =   0  'Transparent
      Caption         =   "2549"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   12000
      TabIndex        =   72
      Top             =   1515
      Width           =   615
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "�Ѵ����¡�����ջ� : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10350
      TabIndex        =   71
      Top             =   1545
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00EBEBE7&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      Height          =   345
      Index           =   4
      Left            =   10080
      Shape           =   4  'Rounded Rectangle
      Top             =   1485
      Width           =   2745
   End
   Begin ShockwaveFlashObjectsCtl.ShockwaveFlash ShockwaveFlash1 
      Height          =   495
      Left            =   3600
      TabIndex        =   46
      Top             =   1359
      Width           =   4185
      _cx             =   7382
      _cy             =   873
      FlashVars       =   ""
      Movie           =   "C:\Cadcorp_install\MicroTax\MicroTax 5.2\Tab.swf"
      Src             =   "C:\Cadcorp_install\MicroTax\MicroTax 5.2\Tab.swf"
      WMode           =   "Transparent"
      Play            =   "0"
      Loop            =   "-1"
      Quality         =   "High"
      SAlign          =   ""
      Menu            =   "-1"
      Base            =   ""
      AllowScriptAccess=   ""
      Scale           =   "NoScale"
      DeviceFont      =   "0"
      EmbedMovie      =   "0"
      BGColor         =   ""
      SWRemote        =   ""
      MovieData       =   ""
      SeamlessTabbing =   "1"
      Profile         =   "0"
      ProfileAddress  =   ""
      ProfilePort     =   "0"
      AllowNetworking =   "all"
      AllowFullScreen =   "false"
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   1680
      TabIndex        =   41
      Top             =   7320
      Width           =   975
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   4
      Left            =   1680
      TabIndex        =   40
      Top             =   7680
      Width           =   975
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   5
      Left            =   1680
      TabIndex        =   39
      Top             =   8040
      Width           =   975
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�����Ẻ :                        ��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   120
      TabIndex        =   38
      Top             =   7320
      Width           =   3330
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�û����Թ :                       ��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   37
      Top             =   7680
      Width           =   3330
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�ê������� :                     ��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   120
      TabIndex        =   36
      Top             =   8040
      Width           =   3330
   End
   Begin VB.Label LabelTile1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   ":: �ӹǹ���շ���ͧ���� ::"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Left            =   780
      TabIndex        =   34
      Top             =   5250
      Width           =   2115
   End
   Begin VB.Shape ShapeTitle1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Left            =   75
      Shape           =   4  'Rounded Rectangle
      Top             =   5190
      Width           =   3465
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "����������  :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   4
      Left            =   9510
      TabIndex        =   33
      Top             =   1140
      Width           =   1155
   End
   Begin VB.Label LB_Menu 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���պ��ا��ͧ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   10740
      TabIndex        =   32
      Top             =   1140
      Width           =   1905
   End
   Begin VB.Label Lb_Name 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   3
      Left            =   3720
      TabIndex        =   31
      Top             =   1142
      Width           =   1080
   End
   Begin VB.Label LbTaxNm 
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   4920
      TabIndex        =   30
      Top             =   1140
      Width           =   4455
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��¡�÷��鹾� :                        ��¡��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   27
      Left            =   120
      TabIndex        =   29
      Top             =   4920
      Width           =   3390
   End
   Begin VB.Label Lb_ResultSchNm 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   255
      Left            =   1590
      TabIndex        =   28
      Top             =   4920
      Width           =   1305
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00EBEBE7&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   75
      Shape           =   4  'Rounded Rectangle
      Top             =   4875
      Width           =   3465
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   ":: ��ª��ͼ���������� ::"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   2
      Left            =   1020
      TabIndex        =   14
      Top             =   1590
      Width           =   1620
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   375
      Left            =   3555
      Shape           =   4  'Rounded Rectangle
      Top             =   1080
      Width           =   9255
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   345
      Index           =   3
      Left            =   75
      Shape           =   4  'Rounded Rectangle
      Top             =   1560
      Width           =   3465
   End
   Begin VB.Label Lb_Year 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2548"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   960
      TabIndex        =   7
      Top             =   659
      Width           =   615
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "��Шӻ� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   0
      Left            =   150
      TabIndex        =   6
      Top             =   721
      Width           =   765
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���ͼ���������� :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005C5C5F&
      Height          =   240
      Index           =   1
      Left            =   1920
      TabIndex        =   5
      Top             =   721
      Width           =   1080
   End
   Begin VB.Shape ShapeTitle 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00C0C0FF&
      Height          =   465
      Left            =   75
      Top             =   600
      Width           =   12735
   End
End
Attribute VB_Name = "Frm_CalAllTax"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rs As New ADODB.Recordset
Dim strTemp As String
Dim strTemp2 As String
Dim Summary As Currency
Dim Txt_Formula As String
Dim chkStatusType As Integer
Dim tempArrearPrice As Double
Dim strOwnerShipID As String
Dim setGK As String
Dim StatusInstallment As Boolean

Private Sub btn_Print_Accept_Click()
        Frm_Print_AcceptTax.LB_Menu.Tag = Me.LB_Menu.Tag
        Frm_Print_AcceptTax.LB_Menu.Caption = Me.LB_Menu.Caption
        Frm_Print_AcceptTax.Show vbModal
End Sub

Private Sub Btn_Print_Click()
        If strOwnerShipID <> "" Then
                With Frm_Print_Preform
                        .LB_Menu.Tag = Me.LB_Menu.Tag
                        .LB_Menu.Caption = Me.LB_Menu.Caption
                        .Show vbModal
                End With
        Else
                MsgBox "��س����͡��������ª��ͼ���������ա�͹ !!", vbInformation, "�Ӫ��ᨧʶҹС����"
        End If
End Sub

Private Sub Btn_Print_Payment_Click()
    Dim strSQL As String
     If CheckStandardDemo = True Then
           MsgBox "��ҹ���ѧ��������� STANDRD DEMO" & vbCrLf & "��سҵԴ��� �.�͹��෤ �ӡѴ" & "Tel.02-9331116", vbExclamation, "����͹"
           Exit Sub
    End If
    Call SetFormulaReport
    If Trim$(Txt_BOOK_NUMBER.Tag) = Empty Or Trim$(Txt_BOOK_NO.Tag) = Empty Then Exit Sub
    
            Select Case LB_Menu.Tag
                        Case "1"
                                    Call ShowReportPayment("PBT11.rpt", Txt_Formula, False)
                        Case "2"
                                    Call ShowReportPayment("PRD12.rpt", Txt_Formula, False)
                        Case "3"
                                    Call ShowReportPayment("PP7.rpt", Txt_Formula, False)
                        Case "4"
                                    
'                                    Set rs = Globle_Connective.Execute("exec sp_get_BussinessID '" & Trim$(Txt_BOOK_NUMBER.Tag) & "','" & Trim$(Txt_BOOK_NO.Tag) & "','" & Lb_Year.Caption & "'", adCmdUnknown)
                                    
'                                    If rs.RecordCount > 0 Then
                                        
'                                            Select Case Rs.Fields(0).Value
'                                                    Case "001"   '  ���������
'                                                            Call ShowReportPayment("SOR5.rpt", Txt_Formula, False)
'                                                    Case Else     '  �����
'                                                            Call ShowReportPayment("ORP2.rpt", Txt_Formula, False, Rs.Fields(1))
'                                            End Select
                                    Call ShowReportPayment("Rpt_PBA1.rpt", Txt_Formula, False, "")
'                                    End If
    End Select
End Sub

Private Sub Btn_Print_PBA1_Click()
        If ChkStandard = False Then Exit Sub
        
        Call SetFormulaReport
        If Trim$(Txt_BOOK_NUMBER.Tag) = Empty Or Trim$(Txt_BOOK_NO.Tag) = Empty Then Exit Sub
        Call ShowReportPayment("Rpt_PBA1_1.rpt", Txt_Formula, False)
End Sub

Private Sub BtnClear_Click()
        txtNm.Text = ""
        txtSurNm.Text = ""
        Call clearSchGrid
        Call Clear_Grid
        Call SetResultSchGrid
        txtNm.SetFocus
End Sub

Private Sub btnPostAcceptTax_Click()
        Dim Sqlstring As String, Txt_Formula As String
        Dim i As Integer
        On Error GoTo ErrPost
        
        Globle_Connective.BeginTrans
        If Grid_Result_PBT9.TextMatrix(1, 0) = Empty Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If MsgBox("�Ѵ����¡�� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If Trim$(Text1.Text) = Empty Then
                MsgBox "�ô����Ţ����Ѻ�駻����Թ !", vbExclamation, "����͹"
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        
        
'        Set Rs = Globle_Connective.Execute("exec sp_check_no_accept '" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)

'        Select Case LB_Menu.Tag   '�礡�ë�Ӣͧ�Ţ����駻����Թ
'                     Case "1", "5"
'                                  Sqlstring = "SELECT  PBT5_NO_ACCEPT FROM PBT5 Where PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3 & " AND PBT5_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'                     Case "2", "6"
'                                  Sqlstring = "SELECT  PRD2_NO_ACCEPT FROM PRD2 WHERE  PRD2_YEAR = " & Lb_Year.Caption & " AND PRD2_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'                     Case "3", "7"
'                                  Sqlstring = "SELECT  PP1_NO_ACCEPT FROM PP1 WHERE  PP1_YEAR = " & Lb_Year.Caption & " AND PP1_NO_ACCEPT  = '" & Trim$(Text1.Text) & "'"
'        End Select
'        Call SET_QUERY(Sqlstring, Rs)
        Set rs = Globle_Connective.Execute("exec sp_check_no_accept '" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)

        If rs.RecordCount > 0 Then   '�礡�ë�Ӣͧ�Ţ����駻����Թ
                MsgBox "�Ţ����駻����Թ " & Trim$(Text1.Text) & " ���  �ô�к��Ţ����駻����Թ����", vbExclamation, "����͹ !"
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
'         Select Case LB_Menu.Tag
'                      Case "1", "5"
'                             Sqlstring = "UPDATE PBT5 SET PBT5_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PBT5_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PBT5_YEAR = " & CInt(Lb_Year.Caption) & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "' "
'                             SET_Execute (Sqlstring)
'                             Sqlstring = "UPDATE PBT5 SET PBT5_NO_ACCEPT = '" & Trim$(Text1.Text) & "'" & _
'                                                  " Where PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3 & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'                      Case "2", "6"
'                             Sqlstring = "UPDATE PRD2 SET PRD2_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PRD2_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PRD2_YEAR = " & CInt(Lb_Year.Caption) & " And PRD2_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'                      Case "3", "7"
'                             Sqlstring = "UPDATE PP1 SET PP1_NO_ACCEPT = '" & Trim$(Text1.Text) & "', PP1_DATE_ACCEPT = '" & Format$(Date, "dd/mm/yyyy") & "'" & _
'                                                  " Where PP1_YEAR = " & CInt(Lb_Year.Caption) & " And PP1_NO =  '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "'"
'         End Select
'           SET_Execute (Sqlstring)
        Call SET_Execute2("exec sp_update_accept '" & Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 0) & "','" & Trim$(Text1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
                        CvDate2(Date) & "','" & LB_Menu.Tag & "'", 8, False)

'                    For i = 1 To Grid_Result_PBT9.Rows - 1
'                            Select Case LB_Menu.Tag    '  ��Ѻ��ا����
'                                    Case "1", "5"
'                                            Sqlstring = "UPDATE PBT5 SET PBT5_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PBT5_YEAR =" & CInt(Lb_Year.Caption) & " And PBT5_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And LAND_ID='" & Grid_Result_PBT9.TextMatrix(i, 1) & "'"
'                                    Case "2", "6"
'                                            Sqlstring = "UPDATE PRD2 SET PRD2_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PRD2_YEAR =" & CInt(Lb_Year.Caption) & " And PRD2_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And BUILDING_ID='" & Grid_Result_PBT9.TextMatrix(i, 6) & "'"
'                                    Case "3", "7"
'                                            Sqlstring = "UPDATE PP1 SET PP1_AMOUNT_ACCEPT=" & Grid_Result_PBT9.TextMatrix(i, 3) & _
'                                                                " WHERE PP1_YEAR =" & CInt(Lb_Year.Caption) & " And PP1_NO =  '" & Grid_Result_PBT9.TextMatrix(i, 0) & "'" & " And SIGNBORD_ID='" & Grid_Result_PBT9.TextMatrix(i, 6) & "'"
'                            End Select
'
'                            SET_Execute (Sqlstring)
'                    Next i
                    Call Clear_Grid
                    Call SetGridAcceptTax
                    'Lb_OwnerShip_ID.Caption = Empty
                    Globle_Connective.CommitTrans
                    Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
                    Call getNoAccept
                    Call getPBT5No
        On Error GoTo ErrPost2
        If iniPrint_Accept = 1 Then
                    Select Case LB_Menu.Tag
                                Case "1"
                                        Txt_Formula = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                         Call ShowReportAcceptTax("PBT9.rpt", Txt_Formula)
                                Case "2"
                                        Txt_Formula = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReportAcceptTax("PRD8.rpt", Txt_Formula)
                                Case "3"
                                        Txt_Formula = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReportAcceptTax("PP3.rpt", Txt_Formula)
                                 Case "5"
                                        Txt_Formula = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                         Call ShowReportAcceptTax("PBT9_OTHER.rpt", Txt_Formula)
                                Case "6"
                                        Txt_Formula = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReportAcceptTax("PRD8_OTHER.rpt", Txt_Formula)
                                Case "7"
                                        Txt_Formula = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO_ACCEPT} =  '" & Trim$(Text1.Text) & "')"
                                        Call ShowReportAcceptTax("PP3_OTHER.rpt", Txt_Formula)
                    End Select
        End If
Exit Sub
ErrPost:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Exit Sub
ErrPost2:
End Sub

Private Sub BtnPostPayment_Click()
    Dim Rs_Check As ADODB.Recordset
    Dim i, j As Integer, RdMoney As Currency
    
    If ChkInstallment.Value = 0 Then    ' ���л���
            Set Rs_Check = New ADODB.Recordset
            On Error GoTo ErrHandler
            Globle_Connective.BeginTrans
                    If Grid_Result.TextMatrix(1, 0) = Empty Then Exit Sub
                    If Txt_BOOK_NUMBER.Text = "" Then
                        MsgBox "�ô�к�������� !", vbExclamation, "�Ӫ��ᨧ"
                        Txt_BOOK_NUMBER.SetFocus
                        Globle_Connective.RollbackTrans
                        Exit Sub
                    End If
                    If Txt_BOOK_NO.Text = "" Then
                            MsgBox "�ô�к��Ţ��� !", vbExclamation, "�Ӫ��ᨧ"
                            Txt_BOOK_NO.SetFocus
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                     
                    If CInt(LbCountPayment.Caption) = 0 Then
                            MsgBox "�ô���͡�к���¡�����ͪ������� !", vbExclamation, "�Ӫ��ᨧ"
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                    If Not IsDate(MaskEdBox4.Text) Then
                            MsgBox "��س�����ѹ��͹�շ���ͧ�ѹ�������١��ͧ����", vbCritical, "��ͼԴ��Ҵ"
                            MaskEdBox4.SetFocus
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                    
                    '�礡�ë�Ӣͧ������� �Ţ���
                    Set Rs_Check = Globle_Connective.Execute("exec sp_check_payment_book_no '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
                            LB_Menu.Tag & "'", , adCmdUnknown)
                            
                     If Rs_Check.RecordCount > 0 Then
                            Txt_BOOK_NO.Text = Empty
                            Txt_BOOK_NUMBER.Text = Empty
                            Txt_BOOK_NUMBER.SetFocus
                            MsgBox "�������������������ӡѺ㹰ҹ������ ��س����������������Ţ���������� !!!", vbCritical, "��ͼԴ��Ҵ"
                            Globle_Connective.RollbackTrans
                            Exit Sub
                     End If
                    If MsgBox("��ͧ��ê������� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                    
'                    RdMoney = 0
'                    RdMoney = CCur(Txt_RoundMoney.Text) - (CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption))
                    If LB_Menu.Tag <> "4" Then
                            Set rs = Globle_Connective.Execute("exec sp_check_first_paytax '" & strOwnerShipID & "','" & Lb_Year.Caption & "','" & LB_Menu.Tag & "'", , adCmdUnknown)
                            If rs.RecordCount < 1 Then
                                    Call SET_Execute2("exec sp_update_payment_gk '" & CvDate2(CDate(MaskEdBox4.Text)) & "','" & Lb_Year.Caption & "','" & strOwnerShipID & "','" & LB_Menu.Tag & "'", 8, False)
                            End If
                    End If
                            For i = 1 To Grid_Result.Rows - 1
                                        If Trim$(Grid_Result.TextMatrix(i, 0)) = "�ѧ������" And Trim$(Grid_Result.TextMatrix(i, Grid_Result.Cols - 1)) = "/" Then
                                      '      If i > 1 Then RdMoney = 0:     Lb_MoneyDue.Caption = "0.00":     Lb_discount.Caption = "0.00": Lb_MoneyDue_Perform.Caption = "0.00"
'                                            Select Case LB_Menu.Tag
'                                                         Case "1", "5"
'                                                                            sql_txt = "UPDATE PBT5 SET PBT5_STATUS = 1,PBT5_PAY = 1,PBT5_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
'                                                                                             ", PBT5_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & " , PBT5_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PBT5_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "', PBT5_PAY_DATE = '" & Format$(CDate(MaskEdBox4.Text), "dd/mm/yyyy") & "',PBT5_IN_GK = " & Chk_IN_GK & ",PBT5_SATANGINCLUDE = " & RdMoney & _
'                                                                                             ", PBT5_DISCOUNT = " & RdMoney & ",PBT5_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 6))) & _
'                                                                                             "  WHERE PBT5_YEAR = " & CInt(Lb_Year.Caption) & _
'                                                                                             " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "' AND LAND_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
'                                                                                             " AND PBT5_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
'
'                                                                                              Call SET_Execute(sql_txt)
'                                                         Case "2", "6"
'                                                                            sql_txt = "UPDATE PRD2 SET PRD2_STATUS =1,PRD2_PAY = 1, PRD2_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
'                                                                                             ",PRD2_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ",PRD2_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PRD2_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PRD2_PAY_DATE = '" & Format$(CDate(MaskEdBox4.Text), "dd/mm/yyyy") & "',PRD2_IN_GK = " & Chk_IN_GK & ",PRD2_SATANGINCLUDE = " & RdMoney & _
'                                                                                            ", PRD2_DISCOUNT = " & RdMoney & ",PRD2_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 5))) & _
'                                                                                             " WHERE PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "' AND BUILDING_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
'                                                                                             " AND PRD2_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
'                                                                                             Call SET_Execute(sql_txt)
'                                                         Case "3", "7"
'                                                                            sql_txt = "UPDATE PP1 SET  PP1_STATUS = 1 ,PP1_PAY = 1,PP1_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & _
'                                                                                            ",PP1_TAXINCLUDE = " & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ", PP1_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PP1_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PP1_PAY_DATE = '" & Format$(CDate(MaskEdBox4.Text), "dd/mm/yyyy") & "',PP1_IN_GK = " & Chk_IN_GK & ",PP1_SATANGINCLUDE = " & RdMoney & _
'                                                                                            ",PP1_DISCOUNT = " & RdMoney & ",PP1_AMOUNT_REAL = " & CCur(Trim$(Grid_Result.TextMatrix(i, 5))) & _
'                                                                                            " WHERE PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND OWNERSHIP_ID  = '" & Trim$(strOwnerShipID) & "' AND SIGNBORD_ID = '" & Trim$(Grid_Result.TextMatrix(i, 2)) & "'" & _
'                                                                                            " AND PP1_NO = '" & Trim$(Lb_PBT_NO.Caption) & "'"
'                                                                                            Call SET_Execute(sql_txt)
'                                                            Case "4"
'                                                                            sql_txt = "UPDATE PBA1 SET PBA1_STATUS = 1, PBA1_BOOK_NUMBER = '" & Trim$(Txt_BOOK_NUMBER.Text) & "',PBA1_BOOK_NO = '" & Trim$(Txt_BOOK_NO.Text) & "',PBA1_PAY_DATE = '" & Format$(CDate(MaskEdBox4.Text), "dd/mm/yyyy") & "',PBA1_SATANGINCLUDE = " & RdMoney & _
'                                                                                             ",PBA1_NO_ACCEPT = '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'" & ", PBA1_DISCOUNT = " & RdMoney & _
'                                                                                             " WHERE PBA1_YEAR = " & CInt(Lb_Year.Caption) & " AND LICENSE_BOOK = '" & Trim$(Grid_Result.TextMatrix(i, 3)) & "' AND LICENSE_NO = '" & Trim$(Grid_Result.TextMatrix(i, 4)) & "'" & _
'                                                                                             " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "'"
'                                                                                              Call SET_Execute(sql_txt)
'                                              End Select
                                                    If LB_Menu.Tag = "4" Then
'                                                            Debug.Print "exec sp_update_payment_pba1 '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & CvDate2(CDate(MaskEdBox4.Text)) & "'," & RdMoney & _
'                                                                    ",'" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_discount.Caption) & "," & CInt(Lb_Year.Caption) & ",'" & Trim$(Grid_Result.TextMatrix(i, 3)) & "','" & Trim$(Grid_Result.TextMatrix(i, 4)) & "','" & _
'                                                                    Trim$(strOwnerShipID) & "'"
                                                            Call SET_Execute2("exec sp_update_payment_pba1 '" & Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & Btn_Print_PBA1.Tag & "','" & CvDate2(CDate(MaskEdBox4.Text)) & "'," & CCur(Lb_discount.Caption) & _
                                                                    ",'" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_discount.Caption) & "," & CInt(Lb_Year.Caption) & ",'" & Trim$(Grid_Result.TextMatrix(i, 3)) & "','" & Trim$(Grid_Result.TextMatrix(i, 4)) & "','" & _
                                                                    strOwnerShipID & "'", 8, False)
                                                    Else
                                                            If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                                    j = 6
                                                            Else
                                                                    j = 5
                                                            End If
                                                            Call SET_Execute2("exec sp_update_payment  '" & Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) & "'," & CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption) & ",'" & _
                                                                        Trim$(Txt_BOOK_NUMBER.Text) & "','" & Trim$(Txt_BOOK_NO.Text) & "','" & CvDate2(CDate(MaskEdBox4.Text)) & "'," & Chk_IN_GK & "," & _
                                                                        CCur(Lb_discount.Caption) & "," & CCur(Lb_discount.Caption) & "," & CCur(Grid_Result.TextMatrix(i, j)) & "," & CInt(Lb_Year.Caption) & ",'" & strOwnerShipID & "','" & _
                                                                        Trim$(Grid_Result.TextMatrix(i, 2)) & "','" & Trim$(Lb_PBT_NO.Caption) & "','" & LB_Menu.Tag & "'", 8, False)
                                                    End If
                                                
                                              RdMoney = 0:     Lb_MoneyDue.Caption = "0.00":     Lb_discount.Caption = "0.00": Lb_MoneyDue_Perform.Caption = "0.00"
                                       End If
                            Next i
            '            End If  'chkInstallment
                                MsgBox "�Ѵ����¡�����º���� !! ", vbInformation, "�׹�ѹ��èѴ����¡��"
                                Call Clear_Grid
                                Call SetGridPayment
                                If Txt_BOOK_NUMBER.Text <> "" Then Txt_BOOK_NUMBER.Tag = Txt_BOOK_NUMBER.Text
                                If Txt_BOOK_NO.Text <> "" Then Txt_BOOK_NO.Tag = Txt_BOOK_NO.Text
                                Call clrTextFrmPayNormal
                                Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
                                Call SchNm
                                If LB_Menu.Tag <> 4 Then
                                        Call getPBT5NoPayTax
                                        Call DisplayPayment(Lb_Year.Caption, 2)
                                End If
                                Globle_Connective.CommitTrans
                        On Error GoTo ErrHandler2
                                If iniPrint_Payment = 1 Then
                                        Call SetFormulaReport
                                        Select Case LB_Menu.Tag
                                                Case "1"
                                                            Call ShowReportPayment("PBT11.rpt", Txt_Formula, True)
                                                Case "2"
                                                            Call ShowReportPayment("PRD12.rpt", Txt_Formula, True)
                                                Case "3"
                                                            Call ShowReportPayment("PP7.rpt", Txt_Formula, True)
                                                Case "4"
'                                                            str_SQL = "SELECT BUSINESS_TYPE.BUSINESS_ID" & _
'                                                            " FROM BUSINESS_TYPE INNER JOIN (CLASS_DETAILS INNER JOIN (PBA1 INNER JOIN LICENSEDATA ON (PBA1.LICENSE_BOOK = LICENSEDATA.LICENSE_BOOK)" & _
'                                                            " AND (PBA1.LICENSE_NO = LICENSEDATA.LICENSE_NO)) ON CLASS_DETAILS.CLASS_ID = LICENSEDATA.CLASS_ID) ON BUSINESS_TYPE.BUSINESS_ID = CLASS_DETAILS.BUSINESS_ID" & _
'                                                            " WHERE (((PBA1.PBA1_BOOK_NUMBER)='" & Trim$(Txt_BOOK_NUMBER.Tag) & "') AND ((PBA1.PBA1_BOOK_NO)='" & Trim$(Txt_BOOK_NO.Tag) & "') AND ((PBA1.PBA1_YEAR)=" & Lb_Year.Caption & "))"
'                                                            Call SET_QUERY(str_SQL, Rs)
                                                            Set rs = Globle_Connective.Execute("exec sp_get_BussinessID '" & Trim$(Txt_BOOK_NUMBER.Tag) & "','" & Trim$(Txt_BOOK_NO.Tag) & "','" & Lb_Year.Caption & "'", adCmdUnknown)
                                                            If rs.RecordCount > 0 Then
                                                                        Call ShowReportPayment("Rpt_PBA1.rpt", Txt_Formula, False, rs.Fields(0))
                                                            End If
                                        End Select
                                End If
Else    '  ���͡�����繧Ǵ
                     If MaskEdBox8.Text = "__/__/____" Then
                                MsgBox "��س�����ѹ����������Ẻ�Ǵ", vbExclamation, "�Ӫ��ᨧ"
                                MaskEdBox8.SetFocus
                                Exit Sub
                    End If
                    If LbInstallmentNo.Caption = "1" Then
                            tempArrearPrice = CCur(txtSumAmount.Text) - CCur(Grid_Installment.TextMatrix(1, 4))
                    ElseIf CInt(LbInstallmentNo.Caption) > 1 Then
                            tempArrearPrice = getArrearPrice(CInt(LbInstallmentNo.Caption))
                    End If
                    If txtPayInstallment.Text <> "" Then
'                                If LbInstallmentNo.Caption = "1" And CCur(txtPayInstallment.Text) >= CCur(txtSumAmount.Text) Then
'                                        MsgBox "�س�����Թ�Ǵ����������  �ô�к�������� !", vbExclamation, "�Ӫ��ᨧ"
'                                        txtBookNumber.Enabled = True
'                                        txtBookNumber.SetFocus
'                                        Exit Sub
'                                 ElseIf CInt(LbInstallmentNo.Caption) > 1 And
                                If (CCur(txtPayInstallment.Text) >= tempArrearPrice) And (txtBookNumber.Text = "") Then
                                        MsgBox "�س�����Թ�Ǵ����������  �ô�к�������� !", vbExclamation, "�Ӫ��ᨧ"
                                        txtBookNumber.Enabled = True
                                        txtBookNumber.SetFocus
                                        Exit Sub
                                End If
'                                If LbInstallmentNo.Caption = "1" And CCur(txtPayInstallment.Text) >= CCur(txtSumAmount.Text) Then
'                                        MsgBox "�س�����Թ�Ǵ����������  �ô�к��Ţ��� !", vbExclamation, "�Ӫ��ᨧ"
'                                        txtBookNo.Enabled = True
'                                        txtBookNo.SetFocus
'                                        Exit Sub
'                                ElseIf CInt(LbInstallmentNo.Caption) > 1 And
                                If (CCur(txtPayInstallment.Text) >= tempArrearPrice) And (txtBookNo.Text = "") Then
                                        MsgBox "�س�����Թ�Ǵ����������  �ô�к��Ţ��� !", vbExclamation, "�Ӫ��ᨧ"
                                        txtBookNo.Enabled = True
                                        txtBookNo.SetFocus
                                        Exit Sub
                                End If

                            Call NewPayInstallment
                    End If
End If
        ChkInstallment.Value = 0
        Call clrTextFrmPayNormal
Exit Sub
ErrHandler:
                    Globle_Connective.RollbackTrans
                    MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
                    Exit Sub
ErrHandler2:
            '        MsgBox "�ա�úѹ�֡���������º�������� ���Դ��ͼԴ��Ҵ " & Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub clrTextFrmPayNormal()
                 '=== ����Ẻ���� ===
                Txt_BOOK_NUMBER.Text = Empty
                Txt_BOOK_NO.Text = Empty
'                 Lb_DateOver.Caption = "0"
                 Lb_MoneyDue.Caption = "0.00"
                 Lb_MoneyDue_Perform.Caption = "0.00"
                 Lb_discount.Caption = "0.00"
'                 MaskEdBox5.Text = "__/__/____"
                 MaskEdBox4.Text = Format$(Date, "dd/mm/yyyy")      '"__/__/____"
'                 MaskEdBox3.Text = "__/__/____"
                 LbCountPayment.Caption = "0"
                 Txt_Summary.Text = "0.00"
                 Chk_RoundMoney.Value = Unchecked
                 OptionPayment2.Value = True
                 Txt_RoundMoney.Text = "0"
                 
                '=== ����Ẻ�Ǵ ===
                LbInstallmentID.Caption = Empty
                 Lb_MoneyDue_Perform1.Caption = "0.00"
                 Lb_MoneyDue1.Caption = "0.00"
                 Lb_Discount1.Caption = "0.00"
                 txtSummary.Text = "0.00"
                 Chk_RoundMoney1.Value = 0
                 txtSumAmount.Text = "0.00"
                 LbInstallmentNo.Caption = Empty
                 MaskEdBox8.Text = "__/__/____"
                 LbFinePrice.Caption = "0.00"
                 LbArrear.Caption = "0.00"
                 Chk_RoundMoney2.Value = 0
                 txtPayInstallment.Text = "0.00"
                 txtBookNumber.Text = Empty
                 txtBookNo.Text = Empty
                 txtBookNo.BackColor = &H8000000F
                 txtBookNumber.BackColor = &H8000000F
                 txtBookNumber.Locked = True
                 txtBookNo.Locked = True


                 
                 '=== �����Թ
                 MaskEdBox6.Text = "__/__/____"
                Lb_NmAccept.Caption = Empty
                Lb_AcceptNo.Caption = Empty
                ComboNoAccept.Text = Empty
                Text1.Text = Empty
End Sub

Private Sub BtnPostPreformTax_Click()
        Dim Sqlstring As String
        Dim i As Integer, j As Byte
        
        On Error GoTo ErrHandler
        
If strOwnerShipID <> "" Then
        Globle_Connective.BeginTrans
        If LenB(Grid_Result_PBT5.TextMatrix(1, 0)) = 0 Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        Select Case LB_Menu.Tag
                Case "1", "5"
                            j = 10
                Case "2", "6"
                            j = 11
                Case "3", "7"
                            j = 12
        End Select
        If Chk_No_Used.Value = Unchecked Then
                    If IsDate(MaskEdBox1.Text) = False Then
                                MaskEdBox1.Text = "__/__/____"
                                MsgBox "�ѹ��͹�����١��ͧ ��س�����ѹ��͹�����١��ͧ����", vbCritical, "����͹"
                                MaskEdBox1.SetFocus
                                Globle_Connective.RollbackTrans
                                Exit Sub
                    End If
                    If Trim$(Txt_PBT_NO.Text) = Empty Then
                                MsgBox "��سҡ�͡�Ţ������Ẻ���� !!!", vbCritical, "����͹"
                                Globle_Connective.RollbackTrans
                                Txt_PBT_NO.SetFocus
                                Exit Sub
                    End If
                    Set rs = Globle_Connective.Execute("exec sp_check_no_perform '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
            '        Select Case LB_Menu.Tag
            '                     Case "1", "5" '�����Ţ������Ẻ
            '                                  Sqlstring = "SELECT  PBT5_NO FROM PBT5 WHERE  PBT5_YEAR = " & Lb_Year.Caption & " AND PBT5_NO  = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "'"
            '                     Case "2", "6"
            '                                  Sqlstring = "SELECT  PRD2_NO FROM PRD2 WHERE  PRD2_YEAR = " & Lb_Year.Caption & " AND PRD2_NO  = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "'"
            '                     Case "3", "7"
            '                                  Sqlstring = "SELECT  PP1_NO FROM PP1 WHERE  PP1_YEAR = " & Lb_Year.Caption & " AND PP1_NO  = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "'"
            '        End Select
            '        Call SET_QUERY(Sqlstring, Rs)
                    If rs.RecordCount > 0 Then   '�礡�ë�Ӣͧ�Ţ������Ẻ
                            MsgBox "�Ţ������Ẻ " & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & " ���  �ô�к��Ţ�������", vbExclamation, "����͹ !"
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                    If MsgBox("�������èѴ����¡�� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                            Globle_Connective.RollbackTrans
                            Exit Sub
                    End If
                    Me.MousePointer = 11
                    For i = 1 To Grid_Result_PBT5.Rows - 1
                          If Grid_Result_PBT5.TextMatrix(i, Grid_Result_PBT5.Cols - 1) = "/" Then
                          
            '                    Select Case LB_Menu.Tag
            '                            Case "1", "5"
            '                                   Sqlstring = "UPDATE PBT5 SET PBT5_NO = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "', PBT5_DATE = '" & MaskEdBox1.Text & "',PBT5_NO_STATUS = 1 " & _
            '                                                        " Where PBT5_YEAR <= " & CInt(Lb_Year.Caption) & " And LAND_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND OWNERSHIP_ID='" & strOwnerShipID & "'"
            '                            Case "2", "6"
            '                                   Sqlstring = "UPDATE PRD2 SET PRD2_NO = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "', PRD2_DATE = '" & MaskEdBox1.Text & "',PRD2_NO_STATUS = 1 " & _
            '                                                        " Where PRD2_YEAR = " & CInt(Lb_Year.Caption) & " And BUILDING_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND OWNERSHIP_ID='" & strOwnerShipID & "'"
            '                            Case "3", "7"
            '                                    If cmd_Change_Date_Install.Visible = True Then
            '                                            Sqlstring = "UPDATE PP1 SET PP1_NO = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "', PP1_DATE = '" & MaskEdBox1.Text & "',PP1_NO_STATUS=1 " & _
            '                                                                 ",PP1_OWNER_DATE='" & Format$(Grid_Result_PBT5.TextMatrix(i, 3)) & _
            '                                                                 "' Where PP1_YEAR = " & CInt(Lb_Year.Caption) & " And SIGNBORD_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND OWNERSHIP_ID='" & strOwnerShipID & "'"
            '                                    Else
            '                                            Sqlstring = "UPDATE PP1 SET PP1_NO = '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "', PP1_DATE = '" & MaskEdBox1.Text & "',PP1_NO_STATUS=1 " & _
            '                                                                " Where PP1_YEAR = " & CInt(Lb_Year.Caption) & " And SIGNBORD_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND FLAG_CHANGE<>1 AND OWNERSHIP_ID='" & strOwnerShipID & "'"
            '                                    End If
            '                    End Select
            '                    SET_Execute (Sqlstring)
            
'                          Debug.Print "exec sp_update_perform '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "','" & CvDate3(MaskEdBox1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
'                                      Grid_Result_PBT5.TextMatrix(i, 1) & "','" & CStr(CInt(cmd_Change_Date_Install.Visible)) & "','" & CvDate3(Format$(Grid_Result_PBT5.TextMatrix(i, 3))) & "','" & Grid_Result_PBT5.TextMatrix(i, 0) & "','" & LB_Menu.Tag & "','" & strOwnerShipID & "'," & Grid_Result_PBT5.TextMatrix(i, j)
'                    Globle_Connective.Execute "exec sp_update_perform '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(4).Caption) & "','" & CvDate3(MaskEdBox1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
'                                      Grid_Result_PBT5.TextMatrix(i, 1) & "','" & CStr(CInt(cmd_Change_Date_Install.Visible)) & "','" & CvDate3(Format$(Grid_Result_PBT5.TextMatrix(i, 3))) & "','" & Grid_Result_PBT5.TextMatrix(i, 0) & "','" & LB_Menu.Tag & "','" & Lb_OwnerShip_ID.Caption & "'," & Grid_Result_PBT5.TextMatrix(i, j), , adCmdUnknown

                                Call SET_Execute2("exec sp_update_perform '" & Trim$(Txt_PBT_NO.Text) & Trim$(Label3(31).Caption) & "','" & CvDate3(MaskEdBox1.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
                                                  Grid_Result_PBT5.TextMatrix(i, 1) & "','" & CStr(CInt(cmd_Change_Date_Install.Visible)) & "','" & CvDate3(Format$(Grid_Result_PBT5.TextMatrix(i, 3))) & "','" & Grid_Result_PBT5.TextMatrix(i, 0) & "','" & LB_Menu.Tag & "','" & strOwnerShipID & "'," & Grid_Result_PBT5.TextMatrix(i, j), 8, False)
                        End If
                    Next i
                    Txt_PBT_NO.Tag = Txt_PBT_NO.Text & Trim$(Label3(31).Caption)
        Else
                 If MsgBox("�������èѴ����¡��Ŵ�ʹ���� " & LB_Menu.Caption & " ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
'                Frm_PreformTax.MousePointer = 11
                Frm_CalAllTax.MousePointer = 11
                For i = 1 To Grid_Result_PBT5.Rows - 1
                        If Grid_Result_PBT5.TextMatrix(i, Grid_Result_PBT5.Cols - 1) = "/" Then
                                Call SET_Execute2("exec sp_update_perform_no_used " & CInt(Lb_Year.Caption) & ",'" & _
                                Grid_Result_PBT5.TextMatrix(i, 1) & "','" & LB_Menu.Tag & "','" & strOwnerShipID & "'," & Grid_Result_PBT5.TextMatrix(i, j), 8, False)
                        End If
                Next i
        End If
        Call Clear_Grid
        Lb_Count.Caption = "0"
        Lb_money.Caption = "0.00"
        Option2.Value = True
        Me.MousePointer = 0
        Globle_Connective.CommitTrans
        Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
Else
        MsgBox "��س����͡��������ª��ͼ���������ա�͹ !!", vbInformation, "�Ӫ��ᨧʶҹС����"
End If
On Error GoTo ErrHander2
        If iniPrint_Perform = 1 Then
                Select Case LB_Menu.Tag
                            Case "1"
                                    Sqlstring = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PBT5.rpt", Sqlstring)
                            Case "2"
                                    Sqlstring = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PRD2.rpt", Sqlstring)
                            Case "3"
                                    Sqlstring = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PP1.rpt", Sqlstring)
                            Case "5"
                                    Sqlstring = "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PBT5_OTHER.rpt", Sqlstring)
                            Case "6"
                                    Sqlstring = "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PRD2_OTHER.rpt", Sqlstring)
                            Case "7"
                                    Sqlstring = "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_NO} =  '" & Trim$(Txt_PBT_NO.Tag) & Trim$(Label3(31).Caption) & "')"
                                    Call ShowReportPreform("PP1_OTHER.rpt", Sqlstring)
                End Select
        End If
        
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        Me.MousePointer = 0
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
        Exit Sub
ErrHander2:
End Sub

Private Sub BtnSearch_Click()
         Me.MousePointer = 11
        LB_Menu.Caption = "���պ��ا��ͧ���"
        strOwnerShipID = ""
        LbTaxNm.Caption = ""
        LB_Menu.Caption = ""
        SSTab1.Tab = 0
        ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
        With Grid_TaxCount
                     If chkStatusType = 0 Then
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                    Else
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                    End If
        End With
        PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
         LbPaymentTitle.ForeColor = 16448
        Call Clear_Grid
        Call clearSchGrid
        Call SetResultSchGrid
        Call SchNm
         Me.MousePointer = 0
End Sub

Private Sub Chk_No_Used_Click()
        If Chk_No_Used.Value = Checked Then
                Option1.Caption = "Ŵ�ʹ������"
                BtnPostPreformTax.Tag = BtnPostPreformTax.Caption
                BtnPostPreformTax.Caption = "�Ѵ�ӡ��Ŵ�ʹ����"
        Else
                Option1.Caption = "���Ẻ������"
                BtnPostPreformTax.Caption = BtnPostPreformTax.Tag
        End If
End Sub

Private Sub Chk_RoundMoney1_Click()
        If Chk_RoundMoney1.Value Then
                   Combo2.Enabled = True
          Else
                   Combo2.ListIndex = -1
                   Combo2.Enabled = False
                   txtSumAmount.Text = Format$(CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption), "###,##0.00")
          End If
End Sub

Private Sub Chk_RoundMoney_Click()
  If Chk_RoundMoney.Value Then
            Combo1.Enabled = True
   Else
            Combo1.ListIndex = -1
            Combo1.Enabled = False
            Txt_RoundMoney.Text = Format$(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption), "###,##0.00")
   End If
End Sub

Private Sub Chk_RoundMoney2_Click()
                  If Chk_RoundMoney2.Value = 1 And (CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption)) <> CInt((CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption))) Then
                           Combo3.Enabled = True
                  Else
                           Combo3.ListIndex = -1
                           Combo3.Enabled = False
                          txtPayInstallment.Text = Format$(CCur(LbFinePrice.Caption) + CCur(LbArrear.Caption), "#,##0.00")
                          Chk_RoundMoney2.Value = 0
                          'MsgBox "�Ҥ��繨ӹǹ����������� !!", vbInformation, "��ʶҹС�÷ӧҹ"
                  End If
End Sub

Private Sub Chkinstallment_Click()
        Dim strPayDate As String
        Dim tempDate As String
        strPayDate = ""
        If ChkInstallment.Value = 1 Then
                FramePayInsallment.Visible = True
                FramePayNormal.Visible = False
                ' check ��� �ա������ѧ
                strPayDate = getPayDate
                If strPayDate <> "" Then        ' �ա���駢������ѹ���ͧ�����繧Ǵ����
                            MaskEdBox7.Enabled = False
                            MaskEdBox7.Text = Format$(strPayDate, "dd/mm/yyyy")
                            MaskEdBox8.Text = "__/__/____"
                            StatusInstallment = True
                Else
                            MaskEdBox7.Enabled = True
                            MaskEdBox7.Text = MaskEdBox4.Text    '  �ѹ�����Тͧ˹�һ��� Format$(Date, "dd/mm/yyyy")
                            LbInstallmentNo.Caption = "1"
                            chkInstallmentID
                            Call SetPayInstallment
                            StatusInstallment = False
                            If Lb_Year.Caption = Year(Date) Then
                                    MaskEdBox8.Text = Format$(Date, "dd/mm/yyyy")
                            Else
                                    tempDate = Day(Date) & "/" & Month(Date) & "/" & (Lb_Year.Caption)
                                    MaskEdBox8.Text = Format$(tempDate, "dd/mm/yyyy")
                            End If

                End If
                
               Call CalOfPay

                If StatusInstallment = True Then
                        Call GetMainDataInstallment
                        If txtSumAmount.Text <> "0.00" And Lb_Discount1.Caption <> "0.00" Then txtSumAmount.Text = Format$(CCur(Lb_Discount1.Caption) + CCur(txtSumAmount.Text), "#,##0.00")
                End If
                SetGridInstallment
        Else
                FramePayInsallment.Visible = False
                FramePayNormal.Visible = True
        End If
End Sub


Private Sub cmd_Change_Date_Install_Click()
On Error Resume Next
        Dim i As Integer
        If IsDate(MaskEdBox2.Text) = False Or CInt(Mid$(MaskEdBox2.Text, 4, 2)) > 12 Then
                MsgBox "�����������ѹ��� ��س��������ŷ��١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                Call SetSelFocus(MaskEdBox2)
                Exit Sub
        End If
        With Grid_Result_PBT5
                If .Row <= .RowSel Then
                        For i = .Row To .RowSel
                                If LB_Menu.Tag = "3" Then
                                        .TextMatrix(i, 3) = MaskEdBox2.Text
                                Else
                                        .TextMatrix(i, 4) = MaskEdBox2.Text
                                End If
                        Next
                Else
                        For i = .RowSel To .Row
                                If LB_Menu.Tag = "3" Then
                                        .TextMatrix(i, 3) = MaskEdBox2.Text
                                Else
                                        .TextMatrix(i, 4) = MaskEdBox2.Text
                                End If
                        Next
                End If
        End With
End Sub

Private Sub cmd_Save_Date_Click()
        Dim i As Integer
        Dim Sqlstring As String
        
        On Error GoTo ErrSave
        Globle_Connective.BeginTrans
        For i = 1 To Grid_Result_PBT5.Rows - 1
                    Call SET_Execute2("exec sp_update_perform_ownerdate '" & Grid_Result_PBT5.TextMatrix(i, 1) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(Grid_Result_PBT5.TextMatrix(i, 4)) & "','" & _
                            LB_Menu.Tag & "'", 8, False)
'                    Select Case LB_Menu.Tag
'                            Case "1", "5"
'                                    Sqlstring = "UPDATE PBT5 SET PBT5_OWNER_DATE = '" & Format$(Grid_Result_PBT5.TextMatrix(i, 4)) & _
'                                                        "' Where PBT5_YEAR = " & CInt(Lb_Year.Caption) & " And LAND_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND FLAG_CHANGE=0"
'                            Case "3", "6"
'                                    Sqlstring = "UPDATE PP1 SET PP1_OWNER_DATE='" & Format$(Grid_Result_PBT5.TextMatrix(i, 3)) & _
'                                                         "' Where PP1_YEAR = " & CInt(Lb_Year.Caption) & " And SIGNBORD_ID =  '" & Grid_Result_PBT5.TextMatrix(i, 1) & "' AND FLAG_CHANGE=0"
'                    End Select
'                    SET_Execute (Sqlstring)
        Next i
        If MsgBox("�س��ͧ�ѹ�֡������ ��������� ?", vbYesNo) = vbYes Then
                Globle_Connective.CommitTrans
        Else
                Globle_Connective.RollbackTrans
        End If
        Exit Sub
ErrSave:
        Globle_Connective.RollbackTrans
        MsgBox Err.Description
End Sub

Private Sub cmd_Search_No_Accept_Click()
        Select Case LB_Menu.Tag
                Case "1"
                         Status_InToFrm = "PBT9_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���պ��ا��ͧ���"
                Case "2"
                         Status_InToFrm = "PRD8_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ�����ç���͹��з��Թ"
                Case "3"
                         Status_InToFrm = "PP3_ACCEPT"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���ջ���"
                Case "5"
                         Status_InToFrm = "PBT9_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���պ��ا��ͧ���"
                Case "6"
                         Status_InToFrm = "PRD8_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ�����ç���͹��з��Թ"
                Case "7"
                         Status_InToFrm = "PP3_ACCEPT_OTHER"
                          Frm_SerachForProcess.Caption = " �鹢����� - �Ţ����駻����Թ���ջ���"
        End Select
        Frm_SerachForProcess.Lb_chkYearCalallTax.Caption = Lb_Year.Caption
        Frm_SerachForProcess.Show vbModal
        Call SetSelFocus(MaskEdBox1)
End Sub

Private Sub Combo1_Click()
On Error GoTo ErrClick
If CCur(Txt_Summary.Text) = 0 Then Exit Sub
Select Case Combo1.ListIndex
             Case 0
                        If Chk_RoundMoney.Value Then
                              If CCur(Txt_RoundMoney.Text) - Int(CCur(Txt_RoundMoney.Text)) > 0 Then
                                    Txt_RoundMoney.Text = FormatNumber(Int(CCur(Txt_RoundMoney.Text)) + 1, 2)
                               Else
                                    Txt_RoundMoney.Text = FormatNumber(CCur(Txt_RoundMoney.Text), 2)
                              End If
                        End If
              Case 1
                            Txt_RoundMoney.Text = FormatNumber(Int(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption)) + 0.25, 2)
              Case 2
                            Txt_RoundMoney.Text = FormatNumber(Int(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption)) + 0.5, 2)
              Case 3
                            Txt_RoundMoney.Text = FormatNumber(Int(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption)) + 0.75, 2)
End Select
'               If CCur(Txt_RoundMoney.Text) < CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) Then Txt_RoundMoney.Text = CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption)
                'Lb_discount.Caption = FormatNumber(CCur(Txt_RoundMoney.Text) - (Summary + CCur(Lb_MoneyDue.Caption)), 2, vbTrue)
'                Lb_discount.Caption = FormatNumber(CCur(Txt_RoundMoney.Text) - Summary, 2, vbTrue)
Exit Sub
ErrClick:
            MsgBox Err.Description
End Sub

Private Sub combo2_Click()
    On Error GoTo ErrClick
    If CCur(txtSummary.Text) = 0 Then Exit Sub
    Select Case Combo2.ListIndex
                 Case 0
                            If Chk_RoundMoney1.Value Then
                                  If CCur(txtSumAmount.Text) - Int(CCur(txtSumAmount.Text)) <> 0 Then
                                        txtSumAmount.Text = FormatNumber(Int(CCur(txtSumAmount.Text)) + 1, 2)
                                   Else
                                        txtSumAmount.Text = FormatNumber(CCur(txtSumAmount.Text), 2)
                                  End If
                            End If
                  Case 1
                                txtSumAmount.Text = FormatNumber(Int(CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption)) + 0.25, 2)
                  Case 2
                                txtSumAmount.Text = FormatNumber(Int(CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption)) + 0.5, 2)
                  Case 3
                                txtSumAmount.Text = FormatNumber(Int(CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption)) + 0.75, 2)
    End Select
                     'If CCur(txtSumAmount.Text) < CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption) Then txtSumAmount.Text = CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption)
                    'Lb_Discount1.Caption = FormatNumber(CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption), 2, vbTrue)
                 '   txtSumAmount.Text = FormatNumber(CCur(txtSummary.Text) + CCur(Lb_Discount1.Caption), 2, vbTrue)
                 'Lb_Discount1.Caption = FormatNumber(CCur(txtSumAmount.Text) - Summary, 2, vbTrue)
    Exit Sub
ErrClick:
                MsgBox Err.Description
End Sub

Private Sub Combo3_Click()
  On Error GoTo ErrClick
    If CCur(txtPayInstallment.Text) = 0 Then Exit Sub
    Select Case Combo3.ListIndex
                 Case 0
                            If Chk_RoundMoney2.Value Then
                                        If (CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption)) <> CInt((CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption))) Then
                                                txtPayInstallment.Text = FormatNumber(Int(CCur(txtPayInstallment.Text)) + 1, 2)
                                       End If
                            End If
                  Case 1
                                txtPayInstallment.Text = FormatNumber(Int(CCur(txtPayInstallment.Text)) + 0.25, 2)
                  Case 2
                               txtPayInstallment.Text = FormatNumber(Int(CCur(txtPayInstallment.Text)) + 0.5, 2)
                  Case 3
                                txtPayInstallment.Text = FormatNumber(Int(CCur(txtPayInstallment.Text)) + 0.75, 2)
    End Select
            'LbArrear.Caption = FormatNumber((CInt(LbFinePrice.Caption) + CInt(Grid_Installment.TextMatrix(Grid_Installment.Rows - 1, 5))), 2)
    Exit Sub
ErrClick:
                MsgBox Err.Description
End Sub

Private Sub ComboNoAccept_Click()
        If ComboNoAccept.Text <> "" Then
                getPerformNo (CStr(ComboNoAccept.Text))
        End If
End Sub

Private Sub getPerformNo(ByVal strNoAccept As String)
    Dim strSQL As String
        
'        Select Case LB_Menu.Tag
'                Case 1, 5
'                        strSQL = "SELECT PBT5.PBT5_NO as PreformNo From PBT5 WHERE (((PBT5.PBT5_YEAR)=2549) AND ((PBT5.OWNERSHIP_ID)='" & strOwnerShipID & "')" & _
'                                          " AND ((PBT5.PBT5_STATUS)=0) AND ((PBT5.FLAG_RETURN)=0) AND ((PBT5.PBT5_NO_ACCEPT)='" & strNoAccept & "')) GROUP BY PBT5.PBT5_NO"
'                Case 2, 6
'                        strSQL = "SELECT PRD2.PRD2_NO as PreformNo From PRD2 WHERE (((PRD2_YEAR)=2549) AND ((OWNERSHIP_ID)='" & strOwnerShipID & "')" & _
'                                          " AND ((PRD2_STATUS)=0) AND ((FLAG_RETURN)=0) AND ((PRD2_NO_ACCEPT)='" & strNoAccept & "')) GROUP BY PRD2_NO"
'                Case 3, 7
'                        strSQL = "SELECT PP1.PP1_NO as PreformNo From PP1 WHERE (((PP1_YEAR)=2549) AND ((OWNERSHIP_ID)='" & strOwnerShipID & "')" & _
'                                          " AND ((PP1_STATUS)=0) AND ((FLAG_RETURN)=0) AND ((PP1_NO_ACCEPT)='" & strNoAccept & "')) GROUP BY PP1_NO"
'        End Select
'        Call SET_QUERY(strSQL, Rs)
        Set rs = Globle_Connective.Execute("sp_get_PerformNo " & strOwnerShipID & "," & Lb_Year.Caption & ",'" & strNoAccept & "','" & setGK & "', " & LB_Menu.Tag, adCmdUnknown)
        If rs.RecordCount > 0 Then
                Lb_AcceptNo.Caption = rs("PreformNo")
                Lb_NmAccept.Caption = LbTaxNm.Caption
        End If
End Sub

Private Sub ComboPayTax_Click()
        Dim strYear As String
            strYear = CStr(Lb_Year.Caption)
            Call DisplayPayment(strYear, 2)
            ChkInstallment.Value = 0
            Grid_Installment.Clear
End Sub

Private Sub ComboPBT5No_Click()
        Dim strYear As String
            strYear = CStr(Lb_Year.Caption)
            Call DisplayAccept(strYear, 1)
End Sub

Private Sub ComboTypeSch_Click()
        txtIDCard.Text = ""
        txtNm.Text = ""
        txtSurNm.Text = ""
        Select Case ComboTypeSch.ListIndex
                    Case 0
                                FrameSchNm.Visible = True
                    Case 1
                                FrameSchNm.Visible = False
                    Case 2
                                FrameSchNm.Visible = False
                    Case 3
                                FrameSchNm.Visible = False
        End Select
End Sub

Private Sub SchNm()
        Dim strSQL As String
        Dim strNm, strSurNm, strYear As String
        Dim i As Integer
        Dim chkW As Boolean
        Dim strIDCard As String
        Dim TypeSch As Integer
        Dim personType As String
        
        strYear = Trim(Lb_Year.Caption)
        strNm = Trim(txtNm.Text)
        strSurNm = Trim(txtSurNm.Text)
        strIDCard = Trim(txtIDCard.Text)
        TypeSch = ComboTypeSch.ListIndex
        personType = ""
        If Trim(Cmb_Type.Text) <> "" Then
            personType = Cmb_Type.ListIndex
        End If
      
'        Debug.Print "exec sp_search_calAllTax " & strYear & ",'" & strNm & "','" & strSurNm & "','" & strIDCard & "'," & TypeSch & ",'" & personType & "','" & setGK & "' "
        Set rs = Globle_Connective.Execute("exec sp_search_calAllTax " & strYear & ",'" & strNm & "','" & strSurNm & "','" & strIDCard & "'," & TypeSch & ",'" & personType & "','" & setGK & "' ", adCmdUnknown)

            If rs.RecordCount > 0 Then
                    With Grid_ListName
                       i = 1
                       .Rows = rs.RecordCount + 1
                        Do While Not rs.EOF
                                .TextMatrix(i, 0) = rs("fullName")
                                .TextMatrix(i, 1) = rs("OWNERSHIP_ID")
                        i = i + 1
                        rs.MoveNext
                        Loop
                    Lb_ResultSchNm.Caption = Format(rs.RecordCount, "#,##0")
                    End With
            Else
                Call clearSchGrid
                SetResultSchGrid
                MsgBox "��辺�����ŷ�����", vbInformation, ":: Display Result Search ::"
            End If
End Sub

Private Sub Command1_Click()
 On Error GoTo Err_Update
        Globle_Connective.BeginTrans
        If ComboNoAccept.Text = Empty Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
        If InStr(1, MaskEdBox6.Text, "_") > 0 Then
              MsgBox "�ô��͡�ѹ���-�Ѻ�������Ţ�����Թ", vbCritical, "Microtax Software"
              Globle_Connective.RollbackTrans
              Exit Sub
        End If
        Set rs = Globle_Connective.Execute("exec sp_search_accept_no '" & Trim$(ComboNoAccept.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
'                    Select Case LB_Menu.Tag
'                                 Case "1", "5"
'                                        Call SET_QUERY("SELECT PBT5_NO_ACCEPT FROM PBT5 WHERE PBT5_NO_ACCEPT <> '' AND PBT5_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & _
'                                                     " AND PBT5_STATUS = 0 AND FLAG_RETURN = 0 AND PBT5_YEAR = " & Lb_Year.Caption, Rs)
'                                Case "2", "6"
'                                        Call SET_QUERY("SELECT PRD2_NO_ACCEPT FROM PRD2 WHERE PRD2_NO_ACCEPT <> '' AND PRD2_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & _
'                                                     " AND PRD2_STATUS = 0 AND FLAG_RETURN = 0 AND PRD2_YEAR = " & Lb_Year.Caption, Rs)
'                                Case "3", "7"
'                                        Call SET_QUERY("SELECT PP1_NO_ACCEPT FROM PP1 WHERE PP1_NO_ACCEPT <> '' AND PP1_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & _
'                                                     " AND PP1_STATUS = 0 AND FLAG_RETURN = 0 AND PP1_YEAR = " & Lb_Year.Caption, Rs)
'                  End Select
        If rs.RecordCount > 0 Then
                    Globle_Connective.Execute "exec sp_update_accept_datereturn '" & Trim$(ComboNoAccept.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(MaskEdBox6.Text) & "','" & LB_Menu.Tag & "'", , adCmdUnknown
                    Call SET_Execute2("exec sp_update_accept_datereturn '" & Trim$(ComboNoAccept.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & CvDate3(MaskEdBox6.Text) & "','" & LB_Menu.Tag & "'", 8, False)
        
'                            Select Case LB_Menu.Tag
'                                         Case "1", "5"
''                                                SET_Execute ("UPDATE PBT5 SET FLAG_RETURN = 1 , PBT5_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
''                                                                           " WHERE PBT5_NO_ACCEPT = '" & Text2.Text & "'" & " AND PBT5_YEAR = " & Lb_Year.Caption)
'                                                SET_Execute ("UPDATE PBT5 SET FLAG_RETURN = 1, PBT5_DATE_RETURN = '" & MaskEdBox6.Text & "'" & _
'                                                                           " WHERE PBT5_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & " AND PBT5_YEAR >= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) & " AND PBT5_YEAR <= " & CInt(Lb_Year.Caption) - ((CInt(Lb_Year.Caption) - 2549) Mod 4) + 3)
'                                         Case "2", "6"
'                                                SET_Execute ("UPDATE PRD2 SET FLAG_RETURN = 1 , PRD2_DATE_RETURN = '" & MaskEdBox6.Text & "'" & _
'                                                                           " WHERE PRD2_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & " AND PRD2_YEAR = " & Lb_Year.Caption)
'                                         Case "3", "7"
'                                                SET_Execute ("UPDATE PP1 SET FLAG_RETURN = 1 , PP1_DATE_RETURN = '" & MaskEdBox6.Text & "'" & _
'                                                                           " WHERE PP1_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & " AND PP1_YEAR = " & Lb_Year.Caption)
''                                         Case "4"
''                                                SET_Execute ("UPDATE PBA1 SET FLAG_RETURN = 1 , PBA1_DATE_RETURN = '" & MaskEdBox1.Text & "'" & _
''                                                                           " WHERE PBA1_NO_ACCEPT = '" & ComboNoAccept.Text & "'" & " AND PBA1_YEAR = " & Lb_Year.Caption)
'                             End Select
                            MsgBox "�ӡ�õͺ�Ѻ�����Ţ�����Թ : " & ComboNoAccept.Text & " ŧ㹢���������ó�", vbOKOnly, "Microtax Software"
                            ComboNoAccept.Text = Empty
                            MaskEdBox6.Text = "__/__/____"
                            Lb_NmAccept.Caption = Empty
                            Lb_AcceptNo.Caption = Empty
                            If ComboPBT5No.Text <> "" Then
                                    Call DisplayAccept(Lb_Year.Caption, 1)
                            End If
        Else
                Globle_Connective.RollbackTrans
                MsgBox "��辺�����Ţ�����Թ : " & ComboNoAccept.Text & " 㹰ҹ������", vbCritical, "Microtax Software"
                Exit Sub
        End If
        Globle_Connective.CommitTrans
        Exit Sub
Err_Update:
        Globle_Connective.RollbackTrans
        MsgBox Err.Description
End Sub

Private Sub Form_Activate()
 Clone_Form.Picture1.Visible = True
End Sub

Private Sub Form_Load()
    Lb_Year.Caption = Right$(Date, 4)
    MaskEdBox1.Text = "__/__/____"
    MaskEdBox2.Text = "__/__/____"
'    lb_Date_Perform.Caption = ""
    MaskEdBox4.Text = "__/__/____"
'    lb_Date_Accept.Caption = ""
    MaskEdBox6.Text = "__/__/____"
    Clone_Form.Picture1.Visible = True
    Call SetGridPreformTax
    ' value=0 �.�. & ������� // value=1 ������ �
    chkStatusType = 0
     setGK = "<>"
     
     LB_Menu.Tag = "1"
     With Grid_TaxCount
                 If chkStatusType = 0 Then
                        LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                Else
                        LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                End If
    End With
                                                                                 
    PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
   
    Image1(0).Picture = LoadResPicture(999, 0)
    Image1(1).Picture = LoadResPicture(999, 0)
    Image1(2).Picture = LoadResPicture(999, 0)
    ComboTypeSch.ListIndex = 0
    
    ShockwaveFlash1.Movie = App.Path & "\Tab.swf"
End Sub

Private Sub SetResultSchGrid()
    '*************** Set Grid ************************************************
    With Grid_ListName
                .TextArray(0) = "���� - ���ʡ��": .ColWidth(0) = 3400: .ColAlignment(0) = 0: .ColAlignmentFixed(0) = 0
                .TextArray(1) = "OwnerShip_ID": .ColWidth(1) = 1500: .ColAlignment(1) = 0: .ColAlignmentFixed(1) = 0
    End With
        
    With Grid_TaxCount
                .Rows = .Rows + 3
                .TextArray(0) = "����������": .ColWidth(0) = 2200: .ColAlignment(0) = 0: .ColAlignmentFixed(0) = 0
                .TextArray(1) = "�ӹǹ": .ColWidth(1) = 1200: .ColAlignment(1) = 4: .ColAlignmentFixed(1) = 4
                .TextArray(2) = "TypeTax": .ColWidth(3) = 2000: .ColAlignment(2) = 0: .ColAlignmentFixed(2) = 0
                .TextArray(3) = "TypeTax": .ColWidth(3) = 2000: .ColAlignment(2) = 0: .ColAlignmentFixed(2) = 0
                .TextMatrix(1, 0) = "���պ��ا��ͧ���"
                .TextMatrix(2, 0) = "�����ç���͹��з��Թ"
                .TextMatrix(3, 0) = "���ջ���"
                .TextMatrix(4, 0) = "�����͹حҵԻ�Сͺ��ä��"
                .TextMatrix(1, 2) = "PBT5"
                .TextMatrix(2, 2) = "PRD2"
                .TextMatrix(3, 2) = "PP1"
                .TextMatrix(4, 2) = "PBA1"
    End With
 End Sub
 
 Private Sub SetGridPreformTax()
    '********************* Set Grid for PBT5 *********************************
    On Error Resume Next
        With Grid_Result_PBT5
                .ColWidth(0) = 0
                .ColWidth(1) = 1200: .ColAlignment(1) = 1: .ColAlignmentFixed(1) = 4
                Select Case LB_Menu.Tag
                        Case "1", "5"
                                .TextArray(1) = "���ʷ��Թ"
                                .TextArray(2) = "�Ţ����͡����Է���": .ColWidth(2) = 1500: .ColAlignment(2) = 6: .ColAlignmentFixed(2) = 4
                                .TextArray(3) = "�Ţ���Թ": .ColWidth(3) = 800: .ColAlignment(3) = 6: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "�ѹ������Ѻ�����Է���": .ColWidth(4) = 1700: .ColAlignment(4) = 4: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "���": .ColWidth(5) = 500: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 6
                                .TextArray(6) = "�ҹ": .ColWidth(6) = 400: .ColAlignmentFixed(6) = 4
                                .TextArray(7) = "��": .ColWidth(7) = 600: .ColAlignmentFixed(7) = 4
                                .TextArray(9) = "�������": .ColWidth(9) = 1400: .ColAlignmentFixed(9) = 4
                                .TextArray(11) = "���Ẻ": .ColWidth(11) = 700: .ColAlignmentFixed(11) = 4
                                .ColWidth(8) = 0
                                .ColAlignment(9) = 7
                                .ColWidth(10) = 0
                        Case "2", "6"
                                .TextArray(1) = "�����ç���͹"
                                .TextArray(2) = "���ʺ�ҹ": .ColWidth(2) = 0
                                .TextArray(3) = "��ҹ�Ţ���": .ColWidth(3) = 1100: .ColAlignment(3) = 4: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "����": .ColWidth(4) = 0
                                .TextArray(5) = "�����": .ColWidth(5) = 4000: .ColAlignmentFixed(5) = 4
                                .TextArray(6) = "�ӹǹ��ͧ": .ColWidth(6) = 0
                                .TextArray(7) = "�ӹǹ���": .ColWidth(7) = 0
                                .TextArray(8) = "���ҧ (����)": .ColWidth(8) = 0
                                .TextArray(9) = "��� (����)": .ColWidth(9) = 0
                                .TextArray(10) = "�������": .ColWidth(10) = 1800: .ColAlignment(10) = 7: .ColAlignmentFixed(10) = 4
                                .TextArray(12) = "���Ẻ": .ColWidth(12) = 700: .ColAlignmentFixed(12) = 4
                                .ColWidth(11) = 0
                        Case "3", "7"
                                .TextArray(1) = "���ʻ���"
                                .TextArray(2) = "���ͻ���": .ColWidth(2) = 0
                                .TextArray(3) = "�ѹ�Դ���": .ColWidth(3) = 1700: .ColAlignment(3) = 4
                                .TextArray(4) = "�Ţ����Ҥ��": .ColWidth(4) = 1100
                                .TextArray(5) = "���ʷ��Թ": .ColWidth(5) = 1300: .ColAlignment(5) = 1: .ColAlignmentFixed(5) = 4
                                .TextArray(6) = "�����": .ColWidth(6) = 0
                                .TextArray(7) = "�����Ҥ��": .ColWidth(7) = 0
                                .TextArray(8) = "�ѹ���Ͷ͹": .ColWidth(8) = 0
                                .TextArray(9) = "�ӹǹ��ҹ": .ColWidth(9) = 1000
                                .TextArray(11) = "�������": .ColWidth(11) = 1800: .ColAlignmentFixed(11) = 4: .ColAlignment(11) = 6
                                .TextArray(13) = "���Ẻ": .ColWidth(13) = 700: .ColAlignmentFixed(13) = 4
                                .ColWidth(10) = 0
                                .ColWidth(12) = 0
                            
                    End Select
        End With
End Sub

Private Sub SetGridAcceptTax()
    '********************* Set Grid for PRD2 *********************************
     With Grid_Result_PBT9
                    .TextArray(0) = "�Ţ������Ẻ": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 4
                    .TextArray(1) = "���ʷ��Թ": .ColWidth(1) = 1250: .ColAlignmentFixed(1) = 4
                    Select Case LB_Menu.Tag
                            Case "1", "5"
                                    .TextArray(2) = "�Ţ�͡����Է���"
                            Case "2", "6"
                                    .TextArray(2) = "��ҹ�Ţ���"
                            Case "3", "7"
                                    .TextArray(2) = "��ҹ�Ţ���"
                    End Select
                    .ColWidth(2) = 1400: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                    .TextArray(3) = "�Ҥ�����": .ColWidth(3) = 1400: .ColAlignmentFixed(3) = 4: .ColAlignment(3) = 7
                    .TextArray(4) = "�ѹ������Ẻ": .ColWidth(4) = 1300: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                    .TextArray(5) = "ʶҹз�Ѿ���Թ": .ColWidth(5) = 2200: .ColAlignmentFixed(5) = 4
                    .ColWidth(6) = 0
            End With
    End Sub
    
Private Sub SetGridPayment()
On Error Resume Next

With Grid_Result
            .TextArray(0) = "ʶҹЪ�������": .ColWidth(0) = 1400: .ColAlignment(0) = 4: .ColAlignmentFixed(0) = 4
            .ColWidth(1) = 1500: .ColAlignment(1) = 4: .ColAlignmentFixed(1) = 4
            If LB_Menu.Tag = "1" Then
                            .TextArray(1) = "�ѹ����駻����Թ"
                            .TextArray(2) = "���ʷ��Թ": .ColWidth(2) = 1300: .ColAlignmentFixed(2) = 4
                            .TextArray(3) = "�Ţ����͡����Է���": .ColWidth(3) = 1600: .ColAlignment(3) = 7: .ColAlignmentFixed(3) = 4
                            .TextArray(4) = "�Ţ���Թ": .ColWidth(4) = 0
                            .TextArray(5) = "�����": .ColWidth(5) = 0
                            .TextArray(6) = "�������": .ColWidth(6) = 1400: .ColAlignment(6) = 6: .ColAlignmentFixed(6) = 4
                            .ColWidth(7) = 0
                            .ColWidth(8) = 0
                            .ColWidth(9) = 0
                            .ColWidth(10) = 0
                            .ColWidth(11) = 0   ' REALMONEY
                            .TextArray(12) = "�Ţ��� (�Ǵ)": .ColWidth(12) = 1100: .ColAlignment(12) = 7: .ColAlignmentFixed(12) = 4
                            .ColWidth(13) = 0     ' NoticeDate of Pay Installment
                            .TextArray(14) = "����": .ColWidth(14) = 500: .ColAlignment(14) = 5: .ColAlignmentFixed(14) = 4
            End If
            If LB_Menu.Tag = "2" Then
'                            .TextArray(0) = "ʶҹЪ�������": .ColWidth(0) = 1400
                            .TextArray(1) = "�ѹ����駻����Թ"
                            .TextArray(2) = "�����ç���͹": .ColWidth(2) = 1300: .ColAlignmentFixed(2) = 4
                            .TextArray(3) = "��ҹ�Ţ���": .ColWidth(3) = 1200: .ColAlignmentFixed(3) = 4
                            .TextArray(4) = "�����": .ColWidth(4) = 0
                            .TextArray(5) = "�������": .ColWidth(5) = 1700: .ColAlignment(5) = 6: .ColAlignmentFixed(5) = 4
                            .ColWidth(6) = 0
                            .ColWidth(7) = 0
                            .ColWidth(8) = 0
                            .ColWidth(9) = 0
                            .ColWidth(10) = 0   ' REALMONEY
                            .TextArray(11) = "�Ţ��� (�Ǵ)": .ColWidth(11) = 1100: .ColAlignment(11) = 7: .ColAlignmentFixed(11) = 4
                            .ColWidth(12) = 0   ' NoticeDate of Pay Installment
                            .TextArray(13) = "����": .ColWidth(13) = 500: .ColAlignment(13) = 5
            End If
            If LB_Menu.Tag = "3" Then
'                            .TextArray(0) = "ʶҹЪ�������": .ColWidth(0) = 1400
                            .TextArray(1) = "�ѹ����駻����Թ"
                            .TextArray(2) = "���ʻ���": .ColWidth(2) = 1300: .ColAlignmentFixed(2) = 4
                            .TextArray(3) = "�ӹǹ��ҹ": .ColWidth(3) = 1200: .ColAlignmentFixed(3) = 4
                            .TextArray(4) = "�����": .ColWidth(4) = 0
                            .TextArray(5) = "�������": .ColWidth(5) = 1700: .ColAlignment(5) = 6: .ColAlignmentFixed(5) = 4
                            .ColWidth(6) = 0
                            .ColWidth(7) = 0
                            .ColWidth(8) = 0
                            .ColWidth(9) = 0
                            .ColWidth(10) = 0   ' REALMONEY
                            .TextArray(11) = "�Ţ��� (�Ǵ)": .ColWidth(11) = 1100: .ColAlignment(11) = 7: .ColAlignmentFixed(11) = 4
                            .ColWidth(12) = 0   ' NoticeDate of Pay Installment
                            .TextArray(13) = "����": .ColWidth(13) = 500: .ColAlignment(13) = 5
            End If
            If LB_Menu.Tag = "4" Then
                            .TextArray(0) = "ʶҹЪ�������": .ColWidth(0) = 1400
'                            .TextArray(1) = "�ѹ�������¡��"
                            .ColWidth(1) = 0
                            .TextArray(3) = "�������": .ColWidth(3) = 600: .ColAlignment(3) = 4: .ColAlignmentFixed(3) = 4
                            .TextArray(4) = "�Ţ���": .ColWidth(4) = 600: .ColAlignment(4) = 4: .ColAlignmentFixed(4) = 4
                            .TextArray(5) = "�����": .ColWidth(5) = 2500: .ColAlignmentFixed(5) = 4
                            .TextArray(8) = "�������": .ColWidth(8) = 1200: .ColAlignment(8) = 6: .ColAlignmentFixed(8) = 4
                            .ColWidth(2) = 0
                            .ColWidth(6) = 0
                            .TextArray(7) = "������": .ColWidth(7) = 2100: .ColAlignmentFixed(7) = 4
                            .ColWidth(9) = 0
                            .ColWidth(10) = 0
                            .ColWidth(11) = 0   ' REALMONEY
                            .ColWidth(12) = 0
                            .TextArray(13) = "����": .ColWidth(13) = 500: .ColAlignment(13) = 5
            End If
End With
End Sub

Private Sub Grid_ListName_Click()
         Dim strSQL As String
         Dim chkOldPay  As Boolean
         
         LB_Menu.Tag = 1
'         SSTab1.Tab = 0
        Call Clear_Grid
        Call SetGridPayment
        Call SetGridInstallment
        SSTab1.Tab = 0
        Label7(3).Caption = "0"
        Label7(4).Caption = "0"
        Label7(5).Caption = "0"
        ShockwaveFlash1.GotoFrame (0)
'        clrTextFrmPayNormal
        strOwnerShipID = Grid_ListName.TextMatrix(Grid_ListName.RowSel, 1)
        If strOwnerShipID <> "" Then
                Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
                chkOldPay = chkPayment
                If chkOldPay = True Then       ' �ա�ä�ҧ����
                           FramePayMent.Enabled = True
                           PicPayment.Picture = LoadResPicture(1001, vbResBitmap)
                           LbPaymentTitle.ForeColor = ColorConstants.vbRed
                Else
                           FramePayMent.Enabled = False
                           PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
                           LbPaymentTitle.ForeColor = 16448
                End If
        Else
                MsgBox "��辺�����ŷ�����", vbInformation, ":: Display Result Search ::"
        End If
        LbTaxNm.Caption = Grid_ListName.TextMatrix(Grid_ListName.RowSel, 0)
End Sub

Private Sub Summary_Money()
On Error GoTo Ctnexit
Dim i As Integer
    Lb_money.Caption = "0.00": Lb_Count.Caption = 0
    For i = 1 To Grid_Result_PBT5.Rows - 1
           If Grid_Result_PBT5.TextMatrix(i, Grid_Result_PBT5.Cols - 1) = "/" Then
                Lb_money.Caption = CDbl(Lb_money.Caption) + CDbl(Grid_Result_PBT5.TextMatrix(i, Grid_Result_PBT5.Cols - 3))
                 Lb_Count.Caption = CInt(Lb_Count.Caption) + 1
           End If
    Next i
             Lb_money.Caption = FormatNumber(CDbl(Lb_money.Caption), 2, vbTrue)
Exit Sub
Ctnexit:
End Sub

Private Sub Grid_ListName_GotFocus()
        WheelUnHook
        WheelHook Frm_CalAllTax, Grid_ListName
End Sub

Private Sub Grid_ListName_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
            Call Grid_ListName_Click
   End If
End Sub

Private Sub Grid_ListName_LostFocus()
        WheelUnHook
End Sub

Private Sub Grid_Result_DblClick()
Dim j As Integer
Dim InstallmentID As String
Dim trueRowSel As Integer

 If Grid_Result.TextMatrix(1, 0) = Empty Then Exit Sub
     Lb_MoneyDue.Caption = "0.00"
     Lb_DateOver.Caption = "0"
    
    If LB_Menu.Tag <> 4 Then SetGridInstallment
     If Not IsDate(MaskEdBox4.Text) Then
                    MsgBox "��س�����ѹ��͹�շ���ͧ�ѹ�������١��ͧ����", vbCritical, "��ͼԴ��Ҵ"
                    MaskEdBox4.SetFocus
                    Exit Sub
      End If
    tempArrearPrice = 0
     
 If Trim$(Grid_Result.TextMatrix(Grid_Result.Row, 0)) = "�ѧ������" Then
        Dim i As Integer, n As Integer, m As Integer
           
            trueRowSel = Grid_Result.Row
            
                For j = 0 To Grid_Result.Rows - 1  ' ������͡��ê����繧Ǵ�����ͧ clear ��͹�֧�����͡������੾����
                             If Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = "/" And Trim$(Grid_Result.TextMatrix(j, 0)) = "�����繧Ǵ" Then
                                     For i = 0 To Grid_Result.Cols - 1
                                                            Grid_Result.Row = j  ' Grid_Result.Row
                                                            Grid_Result.Col = i
                                                            Grid_Result.CellBackColor = &H80000018    '
                                     Next i
                                     Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                             End If
               Next j
                Grid_Result.Row = trueRowSel
                
                n = 0
                m = Grid_Result.Row
        If LB_Menu.Tag = "4" Then
                Grid_Result.Col = 1
                If Grid_Result.CellBackColor = &HC0C0FF Then
                        For i = 0 To Grid_Result.Cols - 1
                                Grid_Result.Row = Grid_Result.Row
                                Grid_Result.Col = i
                                Grid_Result.CellBackColor = &H80000018
                        Next i
                        Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                        Summary = 0
                        Txt_Summary.Text = "0.00"
                        Txt_RoundMoney.Text = "0.00"
                        Btn_Print_PBA1.Tag = Empty
                        Exit Sub
                End If
                For i = 0 To Grid_Result.Rows - 1
                        Grid_Result.Row = i
                        Grid_Result.Col = 1
                        If Grid_Result.CellBackColor = &HC0C0FF Then n = n + 1
                Next i
                If n < 1 Then
                        If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                                    For i = 0 To Grid_Result.Cols - 1
                                            Grid_Result.Row = m   '   Grid_Result.Row
                                            Grid_Result.Col = i
                                            Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                                    Next i
                                    Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = "/"
                                    Lb_PBT_NO.Caption = Grid_Result.TextMatrix(Grid_Result.Row, 3)
                                    Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(Grid_Result.Row, 4)
                                    Btn_Print_PBA1.Tag = Grid_Result.TextMatrix(Grid_Result.Row, 2)
                        Else
                                    For i = 0 To Grid_Result.Cols - 1
                                            Grid_Result.Row = Grid_Result.Row
                                            Grid_Result.Col = i
                                            Grid_Result.CellBackColor = &H80000018
                                    Next i
                                     Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                                     Btn_Print_PBA1.Tag = Empty
                        End If
                        Grid_Result.ColAlignment(Grid_Result.Cols - 1) = 5
                        Call CalOfPay
                End If
        Else        '  lb_menu.tag <> 4
                If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                           For i = 0 To Grid_Result.Cols - 1
                                   Grid_Result.Row = Grid_Result.Row
                                   Grid_Result.Col = i
                                   Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                           Next i
                            Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = "/"
                Else
                           For i = 0 To Grid_Result.Cols - 1
                                   Grid_Result.Row = Grid_Result.Row
                                   Grid_Result.Col = i
                                   Grid_Result.CellBackColor = &H80000018
                           Next i
                             Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty
                End If
                        Grid_Result.ColAlignment(Grid_Result.Cols - 1) = 5
                        Call CalOfPay
'                       chkAmountSelList
                       If LB_Menu.Tag <> 4 Then
                                chkAmountSelList
                        Else
                                ChkInstallment.Enabled = False
                                ChkInstallment.Value = 0    'un-checked
                        End If
        End If
        
ElseIf Trim$(Grid_Result.TextMatrix(Grid_Result.Row, 0)) = "�����繧Ǵ" Then
            'InstallmentID = Trim$(Grid_Result.TextMatrix(Grid_Result.Row, 12))
             InstallmentID = Trim$(Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 3))   ' get PBT5_INSTALLMENT_ID from Grid
            
            If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then
                    trueRowSel = Grid_Result.Row
                            For j = 0 To Grid_Result.Rows - 1
                                        If j <> 0 Then
                                                For i = 0 To Grid_Result.Cols - 1
                                                                       Grid_Result.Row = j  ' Grid_Result.Row
                                                                       Grid_Result.Col = i
                                                                       Grid_Result.CellBackColor = &H80000018
                                                Next i
                                                Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                                        End If
                                Next j
                            Grid_Result.Row = trueRowSel

                            If Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 3) <> "" And Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 1) = Empty Then

                                        For j = 0 To Grid_Result.Rows - 1
                                                If InstallmentID = Grid_Result.TextMatrix(j, Grid_Result.Cols - 3) Then
                                                        For i = 0 To Grid_Result.Cols - 1
                                                                Grid_Result.Row = j  ' Grid_Result.Row
                                                                Grid_Result.Col = i
                                                                Grid_Result.CellBackColor = &HC0C0FF    '  �ժ���
                                                        Next i
                                                        Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = "/"
                                                End If
                                        Next j
                                        FramePayInsallment.Visible = True
                                        FramePayNormal.Visible = False
                                        
                                        MaskEdBox7.Text = Format$(Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 2), "dd/mm/yyyy") 'get Notice_Date from Grid
                                        LbInstallmentID.Caption = InstallmentID

                                        ''check this here  ���С�͹������� calofpay
                                        ChkInstallment.Value = 1
                                      ' Call GetMainDataInstallment
                                        'chkAmountSelList
                                        ChkInstallment.Enabled = False
                                        Call DisplayInsatllment
                                        Call getInstallmentNO
                                        Call chkFineMoneyInstallment
                                        tempArrearPrice = getArrearPrice(CInt(LbInstallmentNo.Caption))
                                        
                            Else
                                    For j = 0 To Grid_Result.Rows - 1
                                            If InstallmentID = Grid_Result.TextMatrix(j, Grid_Result.Cols - 3) Then
                                                    For i = 0 To Grid_Result.Cols - 1
                                                    Grid_Result.Row = j
                                                    Grid_Result.Col = i
                                                    Grid_Result.CellBackColor = &H80000018
                                                    Next i
                                                    Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                                            End If
                                    Next j
                            End If
                            
            Else
                            For j = 0 To Grid_Result.Rows - 1
                                    If InstallmentID = Grid_Result.TextMatrix(j, Grid_Result.Cols - 3) Then
                                            For i = 0 To Grid_Result.Cols - 1
                                            Grid_Result.Row = j
                                            Grid_Result.Col = i
                                            Grid_Result.CellBackColor = &H80000018
                                            Next i
                                            Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                                    End If
                            Next j
                            ChkInstallment.Value = 0
                            Call clrTextFrmPayNormal
            End If
Else
     MsgBox "��������¡�ù������������� !  �������ö�ӡ�ê���������", vbCritical, "MicroTax Software"
End If
End Sub

Private Sub Grid_Result_PBT5_DblClick()
 If Grid_Result_PBT5.TextMatrix(1, 0) = Empty Then Exit Sub
 If Grid_Result_PBT5.TextMatrix(Grid_Result_PBT5.Row, 0) = "�ѧ������" Then
    Dim i As Integer

     If Grid_Result_PBT5.TextMatrix(Grid_Result_PBT5.Row, Grid_Result_PBT5.Cols - 1) = Empty Then
                For i = 0 To Grid_Result_PBT5.Cols - 1
                        Grid_Result_PBT5.Row = Grid_Result_PBT5.Row
                        Grid_Result_PBT5.Col = i
                        Grid_Result_PBT5.CellBackColor = &HC0C0FF
                Next i
                 Grid_Result_PBT5.TextMatrix(Grid_Result_PBT5.Row, Grid_Result_PBT5.Cols - 1) = "/"
     Else
                For i = 0 To Grid_Result_PBT5.Cols - 1
                        Grid_Result_PBT5.Row = Grid_Result_PBT5.Row
                        Grid_Result_PBT5.Col = i
                        Grid_Result_PBT5.CellBackColor = &HE0E0E0
                Next i
                  Grid_Result_PBT5.TextMatrix(Grid_Result_PBT5.Row, Grid_Result_PBT5.Cols - 1) = Empty
     End If
             Grid_Result_PBT5.ColAlignment(Grid_Result_PBT5.Cols - 1) = 5
             Call Summary_Money
             lb_Date_Accept.Caption = Format$(Date, "dd/mm/yyyy")
Else
     MsgBox "��������¡�ù������������� !  �������ö�ӡ�����Ẻ������", vbCritical, "Microtax Software"
End If
End Sub

Private Sub Grid_TaxCount_Click()
    Dim strSelTaxType As String
    
        If strOwnerShipID <> "" Then
            With Grid_TaxCount
                        If chkStatusType = 0 Then
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                        Else
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                        End If
                        
                                     strSelTaxType = CStr(.TextMatrix(.RowSel, 2))
                                     
                                     If chkStatusType = 0 Then       ' ���͡������������ �.�. & �������
                                                 Select Case strSelTaxType
                                                             Case "PBT5"         ' ************ ��ͧ��� ************
                                                                         LB_Menu.Tag = "1"
                                                                         'LB_Menu.Caption = "���պ��ا��ͧ���"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.5)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.5"
                                                                                 Label2(1).Caption = "�ӹǹ�ŧ :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.9)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.9"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.11)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.�.11"
                                                                                 Label3(24).Caption = "�ŧ���Թ :"
                                                                         End If
                                                                         
                                                             Case "PRD2"     '************  �ç���͹  ************
                                                                         LB_Menu.Tag = "2"
                                                                         'LB_Menu.Caption = "�����ç���͹��з��Թ"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 'LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.2)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.2"
                                                                                 Label2(1).Caption = "�ӹǹ�ç���͹ :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.8)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.8"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.12)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.�.12"
                                                                                 Label3(24).Caption = "�ç���͹ :"
                                                                         End If
                                                                         
                                                             Case "PP1"      '************  ����   ************
                                                                          LB_Menu.Tag = "3"
                                                                          'LB_Menu.Caption = "���ջ���"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 'LB_Menu.Caption = "���ջ��� (�.�.1)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.1"
                                                                                 Label2(1).Caption = "�ӹǹ���� :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "���ջ��� (�.�.3)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� (�.�.3)"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "���ջ��� (�.�.7)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.7"
                                                                                 Label3(24).Caption = "���� :"
                                                                         End If
                                                             Case "PBA1"         '************  �͹حҵ�   ************
                                                                         LB_Menu.Tag = "4"
                                                                         'LB_Menu.Caption = "�����͹حҵ�"
                                                                         Lb_PBT_NO.Caption = ""
                                                                         Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
                                                                         MaskEdBox4.Text = Format(Date, "dd/mm/yyyy")
                                                 End Select
                                     ElseIf chkStatusType = 1 Then        ' ���͡������������ ������ �
                                                Select Case strSelTaxType
                                                             Case "PBT5"         ' ************ ��ͧ��� ( ������ � ) ************
                                                                         LB_Menu.Tag = "5"
                                                                       '  LB_Menu.Caption = "���պ��ا��ͧ��� (�������)"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�������)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.5"
                                                                                 Label2(1).Caption = "�ӹǹ�ŧ :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.9)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.9"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "���պ��ا��ͧ��� (�.�.�.11)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.�.11"
                                                                                 Label3(24).Caption = "�ŧ���Թ :"
                                                                         End If
                                                             Case "PRD2"         '************  �ç���͹  ( ������ � ) ************
                                                                         LB_Menu.Tag = "6"
                                                                       '  LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 ' LB_Menu.Caption = "�����ç���͹��з��Թ (�������)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.2"
                                                                                 Label2(1).Caption = "�ӹǹ�ç���͹ :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.8)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.8"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "�����ç���͹��з��Թ (�.�.�.12)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.�.12"
                                                                                 Label3(24).Caption = "�ç���͹ :"
                                                                         End If
                                                             Case "PP1"       '************  ����   ( ������ � )  ************
                                                                         LB_Menu.Tag = "7"
                                                                  '       LB_Menu.Caption = "���ջ��� (�������)"
                                                                         If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                                                 'LB_Menu.Caption = "���ջ��� (�������)"
                                                                                 BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.1"
                                                                                 Label2(1).Caption = "�ӹǹ���� :"
                                                                         ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                                                 'LB_Menu.Caption = "���ջ��� (�.�.3)"
                                                                                 btnPostAcceptTax.Caption = "�Ѵ����¡�� (�.�.3)"
                                                                         ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                                                 'LB_Menu.Caption = "���ջ��� (�.�.7)"
                                                                                 BtnPostPayment.Caption = "�������� �.�.7"
                                                                                 Label3(24).Caption = "���� :"
                                                                         End If
                                                 End Select
                                     End If
                        Call Clear_Grid
                        Call ChkStatusNum_Tax(strOwnerShipID, Lb_Year.Caption)
                        Call clrTextFrmPayNormal
                        If Label7(3).Caption <> "0" Then
                                SSTab1.Tab = 0
                                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                        ElseIf Label7(4).Caption <> "0" Or Label7(5).Caption <> "0" Then
                                SSTab1.Tab = 2
                                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                        End If
                        If CInt(.TextMatrix(.RowSel, 1)) > 0 Then       ' check ��һ����������ŷ�����͡ <> 0
                                            If LB_Menu.Tag <> 4 Then
                                                    MaskEdBox6.Text = Format$(Date, "dd/mm/yyyy")
                                                    If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                                                            Call DisplayPreform(Lb_Year.Caption, 0)
                                                    ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                                                            Call getNoAccept
                                                            Call DisplayAccept(Lb_Year.Caption, 1)
                                                    ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                                                            Call DisplayPayment(Lb_Year.Caption, 2)
                                                    End If
                                             Else
                                                    SSTab1.Tab = 2
                                                    ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                                             End If
                            End If
                End With
        Else
                    If SSTab1.Tab = 1 Then
                            LB_Menu.Caption = "���պ��ا��ͧ���"
                            LB_Menu.Tag = 1
                    Else
                            MsgBox "��س����͡��������ª��ͼ���������ա�͹ !!", vbInformation, "�Ӫ��ᨧʶҹС����"
                   End If
        End If
End Sub

Private Sub DisplayPreform(strYear As String, SSTabType As Integer)
    On Error GoTo ErrDisplay
    If strOwnerShipID <> Empty Then
'                    Select Case LB_Menu.Tag
'                                Case "1", "5"
'                                        strSQL = "SELECT A.PBT5_STATUS, A.LAND_ID , LAND_NOTIC , LAND_NUMBER,PBT5_OWNER_DATE , LAND_A_RAI, LAND_A_POT , LAND_A_VA,PBT5_SET_GK,A.PBT5_AMOUNT ,''" & _
'                                                            " FROM (OWNERSHIP B RIGHT JOIN PBT5 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN LANDDATA C ON A.LAND_ID = C.LAND_ID" & _
'                                                            " WHERE ( PBT5_NO IS NULL OR PBT5_NO = '' ) AND PBT5_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2 AND  PBT5_YEAR = " & CInt(strYear) & " AND B.OWNERSHIP_ID ='" & strOwnerShipID & "'" & _
'                                                            " ORDER BY A.PBT5_STATUS "
'                                Case "2", "6"
'                                        strSQL = "SELECT A.PRD2_STATUS, A.BUILDING_ID , BUILDING_HOME_ID , BUILDING_HOME_NO, C.ZONETAX_ID,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME " & _
'                                                            " , BUILDING_ROOM, BUILDING_FLOOR , BUILDING_HEIGHT,BUILDING_WIDTH, A.PRD2_RENTYEAR_TOTAL ,''" & _
'                                                            " FROM AMPHOE  G RIGHT JOIN (SOI E RIGHT JOIN (STREET F RIGHT JOIN (TAMBON D RIGHT JOIN (LANDDATA H RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PRD2 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN BUILDINGDATA C ON A.BUILDING_ID = C.BUILDING_ID) ON H.LAND_ID = C.LAND_ID) ON D.TAMBON_ID = H.TAMBON_ID) ON F.STREET_ID = H.STREET_ID) ON E.SOI_ID = H.SOI_ID) ON G.AMPHOE_ID = D.AMPHOE_ID" & _
'                                                            " WHERE ( PRD2_NO IS NULL OR PRD2_NO = '' ) AND PRD2_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2" & _
'                                                            " AND  PRD2_YEAR = " & CInt(strYear) & " AND B.OWNERSHIP_ID = '" & strOwnerShipID & "' ORDER BY A.PRD2_STATUS "
'                                Case "3", "7"
'                                        strSQL = "SELECT A.PP1_STATUS, A.SIGNBORD_ID , SIGNBORD_NAME ,PP1_OWNER_DATE, BUILDING_NO, C.LAND_ID,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME " & _
'                                                         " ,BUILDING_ID, SIGNBORD_END_DATE , SIGNBORD_ROUND,PP1_SET_GK,A.PP1_AMOUNT,''" & _
'                                                         " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PP1 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN (LANDDATA H RIGHT JOIN SIGNBORDDATA C ON H.LAND_ID = C.LAND_ID) ON A.SIGNBORD_ID = C.SIGNBORD_ID) ON E.SOI_ID = H.SOI_ID) ON F.STREET_ID = H.STREET_ID) ON D.TAMBON_ID = H.TAMBON_ID" & _
'                                                         " WHERE ( PP1_NO IS NULL OR PP1_NO = '' ) AND PP1_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2" & _
'                                                         " AND  PP1_YEAR = " & CInt(strYear) & " AND B.OWNERSHIP_ID = '" & strOwnerShipID & "' ORDER BY A.PP1_STATUS "
'                    End Select
'
'                    Call SET_QUERY(strSQL, Rs)

'        Set Rs = Globle_Connective.Execute("exec sp_get_DisplayPreform " & strOwnerShipID & "," & strYear & ",'" & setGK & "', '" & LB_Menu.Tag & "'", adCmdUnknown)
        
        Set rs = Globle_Connective.Execute("exec sp_search_performtax_ownership '" & strOwnerShipID & "'," & CInt(strYear) & ",'" & LB_Menu.Tag & "'", , adCmdUnknown)
                            If rs.RecordCount > 0 Then
                                    Select Case LB_Menu.Tag
                                                Case "1", "5" '���պ��ا��ͧ���
                                                            Grid_Result_PBT5.Rows = 2
                                                            Grid_Result_PBT5.Cols = 12
                                                            Do While Not rs.EOF
                                                                    With Grid_Result_PBT5
                                                                            .Row = .Rows - 1
                                                                            .TextMatrix(.Row, 0) = rs.Fields(0).Value
                                                                            .TextMatrix(.Row, 1) = rs.Fields(1).Value
                                                                            .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields(2).Value) = True, "", rs.Fields(2).Value)
                                                                            .TextMatrix(.Row, 3) = IIf(IsNull(rs.Fields(3).Value) = True, "", rs.Fields(3).Value)
                                                                            .TextMatrix(.Row, 4) = Format$(rs.Fields(4).Value, "dd/mm/yyyy")
                                                                            .TextMatrix(.Row, 5) = IIf(IsNull(rs.Fields(5).Value) = True, "", rs.Fields(5).Value)
                                                                            .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields(6).Value) = True, "", rs.Fields(6).Value)
                                                                            .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields(7).Value) = True, "", rs.Fields(7).Value)
                                                                            .TextMatrix(.Row, 8) = rs.Fields(8).Value         '������ - �������
                                                                            .TextMatrix(.Row, 9) = rs.Fields(9).Value         '�ӹǹ����
                                                                            .TextMatrix(.Row, 10) = rs.Fields(10).Value    ' Count_Change
                                                                            .TextMatrix(.Row, 11) = rs.Fields(11).Value
                                                                            .Rows = .Rows + 1
                                                                    End With
                                                                    rs.MoveNext
                                                            Loop
                                                            Grid_Result_PBT5.Rows = Grid_Result_PBT5.Rows - 1
                                                            
                                                Case "2", "6"
                                                        Set Grid_Result_PBT5.DataSource = rs
                                                Case "3", "7" '���ջ���
                                                        Grid_Result_PBT5.Rows = 2
                                                        Grid_Result_PBT5.Cols = 14
                                                        Do While Not rs.EOF
                                                                With Grid_Result_PBT5
                                                                        .Row = .Rows - 1
                                                                        .TextMatrix(.Row, 0) = rs.Fields(0).Value
                                                                        .TextMatrix(.Row, 1) = rs.Fields(1).Value
                                                                        .TextMatrix(.Row, 2) = IIf(IsNull(rs.Fields(2).Value), "", rs.Fields(2).Value)
                                                                        .TextMatrix(.Row, 3) = Format$(rs.Fields(3).Value, "dd/mm/yyyy")
                                                                        .TextMatrix(.Row, 4) = IIf(IsNull(rs.Fields(4).Value), "", rs.Fields(4).Value)
                                                                        .TextMatrix(.Row, 5) = IIf(IsNull(rs.Fields(5).Value), "", rs.Fields(5).Value)
                                                                        .TextMatrix(.Row, 6) = IIf(IsNull(rs.Fields(6).Value), "", rs.Fields(6).Value)
                                                                        .TextMatrix(.Row, 7) = IIf(IsNull(rs.Fields(7).Value), "", rs.Fields(7).Value)
                                                                        .TextMatrix(.Row, 8) = Format$(rs.Fields(8).Value, "dd/mm/yyyy")
                                                                        .TextMatrix(.Row, 9) = IIf(IsNull(rs.Fields(9).Value), "", rs.Fields(9).Value)
                                                                        .TextMatrix(.Row, 10) = rs.Fields(10).Value      '������ - �������
                                                                        .TextMatrix(.Row, 11) = rs.Fields(11).Value
                                                                        .TextMatrix(.Row, 12) = rs.Fields(12).Value      ' Count_Change
                                                                        .TextMatrix(.Row, 13) = rs.Fields(13).Value
                                                                        .Rows = .Rows + 1
                                                                End With
                                                                rs.MoveNext
                                                        Loop
                                                        Grid_Result_PBT5.Rows = Grid_Result_PBT5.Rows - 1
                                        End Select
                                                Call SetGridPreformTax
                                                Dim i As Integer
                                                
                                                                Lb_Count.Caption = "0"
                                                                Lb_money.Caption = "0.00"
                                                                For i = 1 To Grid_Result_PBT5.Rows - 1
                                                                                Lb_Count.Caption = CInt(Lb_Count.Caption) + 1
                                                                            If LB_Menu.Tag = "4" Then
                                                                                Lb_money.Caption = CCur(Lb_money.Caption) + CCur(Grid_Result_PBT5.TextMatrix(i, 10))
                                                                                Select Case Grid_Result_PBT5.TextMatrix(i, 3)
                                                                                             Case "1"
                                                                                                Grid_Result_PBT5.TextMatrix(i, 8) = Grid_Result_PBT5.TextMatrix(i, 6)
                                                                                                Grid_Result_PBT5.TextMatrix(i, 3) = "��駺��ŧ���Թ"
                                                                                                 Grid_Result_PBT5.TextMatrix(i, 4) = Empty
                                                                                             Case "2"
                                                                                             Grid_Result_PBT5.TextMatrix(i, 8) = Grid_Result_PBT5.TextMatrix(i, 7)
                                                                                                Grid_Result_PBT5.TextMatrix(i, 3) = "��駺��ç���͹"
                                                                                             Case "0"
                                                                                                Grid_Result_PBT5.TextMatrix(i, 3) = "����շ����"
                                                                                                Grid_Result_PBT5.TextMatrix(i, 4) = Empty
                                                                                                Grid_Result_PBT5.TextMatrix(i, 5) = Empty
                                                                                End Select
                                                                                                Grid_Result_PBT5.TextMatrix(i, 6) = Empty
                                                                                                Grid_Result_PBT5.TextMatrix(i, 7) = Empty
                                                                            Else
                                                                                    If Grid_Result_PBT5.TextMatrix(i, 0) <> "0" Then
                                                                                         Grid_Result_PBT5.TextMatrix(i, 0) = "����"
                                                                                    Else
                                                                                           Grid_Result_PBT5.TextMatrix(i, 0) = "�ѧ������"
                                                                                           If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
'                                                                                                Grid_Result_PBT5.TextMatrix(i, 9) = CCur(Grid_Result_PBT5.TextMatrix(i, 9))
                                                                                               Lb_money.Caption = CCur(Lb_money.Caption) + CCur(Grid_Result_PBT5.TextMatrix(i, 9))
                                                                                               If IsNumeric(Grid_Result_PBT5.TextMatrix(i, 7)) = True Then Grid_Result_PBT5.TextMatrix(i, 7) = Format$(FormatNumber(CSng(Grid_Result_PBT5.TextMatrix(i, 7)), 2), "##0.00##")
                                                                                               
                                                                                               If Grid_Result_PBT5.TextMatrix(i, 8) = 0 Or Grid_Result_PBT5.TextMatrix(i, 8) = 2 Then
                                                                                                    If Grid_Result_PBT5.TextMatrix(i, 4) <> Empty Then MaskEdBox2.Text = Format$(Grid_Result_PBT5.TextMatrix(i, 4), "dd/mm/yyyy")
                                                                                                    MaskEdBox2.Visible = True
                                                                                                    Label2(4).Visible = True
                                                                                                    Label2(4).Caption = "�ѹ�������Ңͧ"
                                                                                                    cmd_Change_Date_Install.Visible = True
                                                                                                    cmd_Save_Date.Visible = True
                                                                                                Else
                                                                                                    MaskEdBox2.Visible = False
                                                                                                    Label2(4).Visible = False
                                                                                                    cmd_Change_Date_Install.Visible = False
                                                                                                    cmd_Save_Date.Visible = False
                                                                                                End If
                                                                                           End If
                                                                                           If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
'                                                                                               Grid_Result_PBT5.TextMatrix(i, 10) = CCur(Grid_Result_PBT5.TextMatrix(i, 10))
                                                                                               Lb_money.Caption = CCur(Lb_money.Caption) + CCur(Grid_Result_PBT5.TextMatrix(i, 10))
                                                                                          End If
                                                                                           If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
'                                                                                               Grid_Result_PBT5.TextMatrix(i, 11) = CCur(Grid_Result_PBT5.TextMatrix(i, 11))
                                                                                               Lb_money.Caption = CCur(Lb_money.Caption) + CCur(Grid_Result_PBT5.TextMatrix(i, 11))
                                                                                               If Grid_Result_PBT5.TextMatrix(i, 10) = 0 Or Grid_Result_PBT5.TextMatrix(i, 10) = 2 Then
                                                                                                    MaskEdBox2.Text = Format$(Grid_Result_PBT5.TextMatrix(i, 3), "dd/mm/yyyy")
                                                                                                    MaskEdBox2.Visible = True
                                                                                                    Label2(4).Visible = True
                                                                                                    Label2(4).Caption = "�ѹ���Դ��駻���"
                                                                                                    cmd_Change_Date_Install.Visible = True
                                                                                                    cmd_Save_Date.Visible = True
                                                                                                Else
                                                                                                    MaskEdBox2.Visible = False
                                                                                                    Label2(4).Visible = False
                                                                                                    cmd_Change_Date_Install.Visible = False
                                                                                                    cmd_Save_Date.Visible = False
                                                                                                End If
                                                                                          End If
                                                                                    End If
                                                                    End If
                                                                Next i
                                                            Lb_money.Caption = Format$(CCur(Lb_money.Caption), "###,##0.00")
                            End If
                            
'                            Dim sql_txt As String
                            '���ҧ�������Ẻ���
'                            Select Case LB_Menu.Tag
'                                             Case "1", "5"
'                                                    If (CInt(Lb_Year.Caption) - 2549) Mod 4 = 0 Then
''                                                            sql_txt = "SELECT PBT5_NO FROM (SELECT PBT5_NO FROM PBT5 WHERE (PBT5_NO IS NOT NULL AND PBT5_NO <> '') GROUP BY PBT5_NO)" & _
''                                                                            "  WHERE (Right(PBT5_NO,2) ='" & Right$(Lb_Year.Caption, 2) & "')"
'                                                            sql_txt = "SELECT  PBT5_NO FROM (SELECT     PBT5_NO From PBT5 WHERE (PBT5_NO IS NOT NULL) AND (PBT5_NO <> '') GROUP BY PBT5_NO) AS AA" & _
'                                                                            " WHERE     (RIGHT(PBT5_NO, 2) = '" & Right$(Lb_Year.Caption, 2) & "')"
'                                                    Else
'                                                            sql_txt = "SELECT PBT5_NO FROM (SELECT PBT5_NO FROM PBT5 WHERE (PBT5_NO IS NOT NULL AND PBT5_NO <> '') GROUP BY PBT5_NO)" & _
'                                                                            " WHERE Right(PBT5_NO,2) ='" & Right$(Lb_Year.Caption, 2) & "'"
'                                                            Call SET_QUERY(sql_txt, Rs)
'                                                            If Rs.RecordCount = 0 Then
'                                                                    sql_txt = "SELECT PBT5_NO FROM (SELECT PBT5_NO FROM PBT5 WHERE (PBT5_NO IS NOT NULL AND PBT5_NO <> '') GROUP BY PBT5_NO)" & _
'                                                                            " WHERE Right(PBT5_NO,2) ='" & CInt(Right$(Lb_Year.Caption, 2)) - 1 & "'"
'                                                            End If
'                                                    End If
'                                             Case "2", "6"
'                                                    sql_txt = " SELECT PRD2_NO From PRD2  Where  PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND   (PRD2_NO is not null AND PRD2_NO <> '') Group by  PRD2_NO Order by  PRD2_NO "
'                                             Case "3", "7"
'                                                    sql_txt = " SELECT PP1_NO From PP1  Where  PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND   (PP1_NO is not null AND PP1_NO <> '') Group by  PP1_NO Order by  PP1_NO "
'                                 End Select
'                                Call SET_QUERY(sql_txt, Rs)

'                                Debug.Print "exec sp_search_performtax_no '" & Lb_Year.Caption & "','" & LB_Menu.Tag & "'"
                                Dim str_begin_year As String, str_end_year As String
                                                    Select Case CInt((Lb_Year.Caption) - 2549) Mod 4
                                                            Case 0
                                                                        str_begin_year = CInt(Lb_Year.Caption)
                                                                        str_end_year = CInt(Lb_Year.Caption) + 3
                                                            Case 1
                                                                        str_begin_year = CInt(Lb_Year.Caption) - 1
                                                                        str_end_year = CInt(Lb_Year.Caption) + 2
                                                            Case 2
                                                                        str_begin_year = CInt(Lb_Year.Caption) - 2
                                                                        str_end_year = CInt(Lb_Year.Caption) + 1
                                                            Case 3
                                                                        str_begin_year = CInt(Lb_Year.Caption) - 3
                                                                        str_end_year = CInt(Lb_Year.Caption)
                                                    End Select
                                If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Or LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                                        str_begin_year = CInt(Lb_Year.Caption)
                                End If
                                Set rs = Globle_Connective.Execute("exec sp_search_performtax_no '" & str_begin_year & "','" & str_end_year & "','" & LB_Menu.Tag & "'", , adCmdText)
                                 If IsNull(rs.Fields(0).Value) = False Then
                                        Select Case LB_Menu.Tag
                                                    Case "1", "5"
                                                            If Label7(3).Caption <> "0" Then
                                                                    Txt_PBT_NO.Text = Format$(CInt(rs.Fields(0).Value) + 1, "0000")
                                                            Else
                                                                    Txt_PBT_NO.Text = ""
                                                            End If
                                                    Case "2", "6"
                                                            If Label7(3).Caption <> "0" Then
                                                                    Txt_PBT_NO.Text = Format$(CInt(rs.Fields(0).Value) + 1, "0000")
                                                            Else
                                                                    Txt_PBT_NO.Text = ""
                                                            End If
                                                    Case "3", "7"
'                                                            sql_txt = "4"
                                                            If Label7(3).Caption <> "0" Then
                                                                    Txt_PBT_NO.Text = Format$(CInt(rs.Fields(0).Value) + 1, "0000")
                                                            Else
                                                                    Txt_PBT_NO.Text = ""
                                                            End If
'                                                            sql_txt = "5"
'                                                    Case "4" ----- >> ����ͧ���͹حҵ����ǹ�ͧ������Ẻ
'                                                            Txt_PBT_NO.Text = Format$(CInt(Query.Fields(0).Value) + 1, "0000")
                                        End Select
                                Else
                                     Txt_PBT_NO.Text = "0001"
                                End If
                                     Grid_Result_PBT5.SetFocus
            Else
                   Call Clear_Grid
                   Call SetGridPreformTax
                   MsgBox "��辺���������ͷ���¡��", vbExclamation, "���й�"
            End If
        Exit Sub
ErrDisplay:
        MsgBox Err.Description
End Sub

Private Sub DisplayAccept(strYear As String, SSTabType As Integer)
    Dim strSQL As String
    Dim strPBT5No As String
    Dim i As Integer
    
    Lb_moneyAccept.Caption = "0.00"
    If strOwnerShipID <> Empty Then
            
                    strPBT5No = ComboPBT5No.Text
'                    Select Case LB_Menu.Tag
'                            Case "1", "5"       ' ��ͧ���
'                                        strSQL = "SELECT PBT5_NO,PBT5.LAND_ID,LAND_NOTIC,Format(PBT5_AMOUNT,'0.00'),Cstr(PBT5_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','����¹�����Է���') AS STATE_OWNER" & _
'                                            " FROM PBT5 LEFT JOIN LANDDATA ON PBT5.LAND_ID=LANDDATA.LAND_ID" & _
'                                            " WHERE PBT5_STATUS=0 And PBT5_NO_STATUS=1 AND PBT5_YEAR=" & strYear & " AND OWNERSHIP_ID='" & strOwnerShipID & "'" & " AND PBT5_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2" & _
'                                            " And PBT5_NO='" & strPBT5No & "'"
'                            Case "2", "6"       ' �ç���͹
'                                        strSQL = "SELECT PRD2_NO,BUILDINGDATA.LAND_ID,BUILDING_HOME_NO,Format(PRD2_RENTYEAR_TOTAL,'0.00'),Cstr(PRD2_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','����¹�����Է���') AS STATE_OWNER,PRD2.BUILDING_ID" & _
'                                                " FROM PRD2 LEFT JOIN BUILDINGDATA ON PRD2.BUILDING_ID = BUILDINGDATA.BUILDING_ID" & _
'                                                " WHERE PRD2_NO_STATUS=1 AND PRD2_YEAR=" & CInt(strYear) & " AND OWNERSHIP_ID='" & strOwnerShipID & "'" & " AND PRD2_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2" & _
'                                                " AND PRD2_NO = '" & Trim$(strPBT5No) & "'"
'                            Case "3", "7"       ' ����
'                                        strSQL = "SELECT PP1_NO,SIGNBORDDATA.LAND_ID,BUILDING_NO,Format(PP1_AMOUNT,'0.00'),Cstr(PP1_DATE),IIF(FLAG_CHANGE=0,'����ա������¹�����Է���','�����Է�������') AS STATE_OWNER,PP1.SIGNBORD_ID " & _
'                                                " FROM PP1 LEFT JOIN SIGNBORDDATA ON PP1.SIGNBORD_ID = SIGNBORDDATA.SIGNBORD_ID " & _
'                                                " WHERE PP1_NO_STATUS=1 AND PP1_YEAR=" & CInt(strYear) & " AND OWNERSHIP_ID='" & strOwnerShipID & "'" & " AND PP1_SET_GK" & IIf(chkStatusType = 0, "<>", "=") & "2" & _
'                                                " AND PP1_NO = '" & Trim$(strPBT5No) & "'"
'                    End Select
'
'                    Call SET_QUERY(strSQL, Rs)
                    Set rs = Globle_Connective.Execute("exec sp_search_accept_ownershipNew '" & strOwnerShipID & "'," & CInt(strYear) & ",'" & LB_Menu.Tag & "', '" & Trim$(strPBT5No) & "'", adCmdUnknown)

                    If rs.RecordCount > 0 Then
                                Set Grid_Result_PBT9.DataSource = rs
                                
                                For i = 1 To Grid_Result_PBT9.Rows - 1
                                            Lb_moneyAccept.Caption = CCur(Lb_moneyAccept.Caption) + CCur(Grid_Result_PBT9.TextMatrix(i, 3))
                                            Grid_Result_PBT9.TextMatrix(i, 4) = Format$(rs.Fields(4).Value, "dd/mm/yyyy")
                                Next i
                                Lb_moneyAccept.Caption = FormatNumber(CCur(Lb_moneyAccept.Caption), 2, vbTrue)
                       End If
                       LB_Sum.Caption = rs.RecordCount
                       Grid_Result_PBT9.Col = 0
                       Grid_Result_PBT9.Cols = 6
                       Grid_Result_PBT9.ColSel = Grid_Result_PBT9.Cols - 1
                       Grid_Result_PBT9.SetFocus
                Call SetGridAcceptTax
                Call Grid_Result_PBT9_LeaveCell
    End If
End Sub

Private Sub DisplayPayment(strYear As String, SSTabType As Integer)
    Dim strSQL As String
    Dim i As Integer
    Dim strPBT5No As String
        
'        If ComboPayTax.Text = "" Then
'                SSTab1.Tab = 0
'                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
'                Exit Sub
'        End If
        
        If strOwnerShipID <> Empty Then
                    Lb_MoneyDue.Caption = "0.00"
                    Lb_MoneyDue_Perform.Caption = "0.00"
                    strPBT5No = ComboPayTax.Text
                    
                    Set rs = Globle_Connective.Execute("exec sp_search_paymentNew '" & Trim$(strOwnerShipID) & "','" & Lb_Year.Caption & "','" & _
                                        strPBT5No & "','" & setGK & "','" & LB_Menu.Tag & "'", adCmdUnknown)
              
'                                Select Case LB_Menu.Tag
'                                        Case "1", "5"       ' ��ͧ���
'                                                    strSQL = " SELECT PBT5_STATUS,Format(PBT5_DATE_RETURN,'dd/mm/yyyy') As DateReturn, A.LAND_ID, LAND_NOTIC, LAND_NUMBER, '�-' & E.SOI_NAME & ' �.' & F.STREET_NAME & ' �.' & D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME, Format(A.PBT5_AMOUNT_ACCEPT,'###,###,###0.00##'), B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName ,  A.OWNERSHIP_ID,PBT5_NO,PBT5_NO_ACCEPT,PBT5_DATE AS PERFORM_DATE,A.PBT5_INSTALLMENT_ID,Format(A.NOTICE_DATE,'dd/mm/yyyy'),'' AS PAYMENT" & _
'                                                                       " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN (LANDDATA C RIGHT JOIN (OWNERSHIP B RIGHT JOIN PBT5 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) ON C.LAND_ID = A.LAND_ID) ON E.SOI_ID = C.SOI_ID) ON F.STREET_ID = C.STREET_ID) ON D.TAMBON_ID = C.TAMBON_ID" & _
'                                                                       " WHERE PBT5_STATUS = 0 AND  PBT5_NO_STATUS = 1 AND PBT5_YEAR =  " & CInt(Lb_Year.Caption) & "  AND PBT5_NO='" & strPBT5No & "' And  ((A.PBT5_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                                        " AND B.OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "'" & " ORDER BY A.PBT5_DATE,A.LAND_ID"
'                                        Case "2", "6"       ' �ç���͹
'                                                    strSQL = " SELECT PRD2_STATUS,Format(A.PRD2_DATE_RETURN,'dd/mm/yyyy')  As DateReturn, A.BUILDING_ID , BUILDING_HOME_NO,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME ,Format(A.PRD2_AMOUNT_ACCEPT,'###,###,###0.00##') , B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName,H.LAND_ID,PRD2_NO,PRD2_NO_ACCEPT,PRD2_DATE AS PERFORM_DATE, A.PRD2_INSTALLMENT_ID,Format(A.NOTICE_DATE,'dd/mm/yyyy'),'' AS PAYMENT" & _
'                                                                        " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PRD2 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN (LANDDATA H RIGHT JOIN BUILDINGDATA C ON H.LAND_ID = C.LAND_ID) ON A.BUILDING_ID = C.BUILDING_ID) ON E.SOI_ID = H.SOI_ID) ON F.STREET_ID = H.STREET_ID) ON D.TAMBON_ID = H.TAMBON_ID" & _
'                                                                        " WHERE PRD2_STATUS = 0 AND PRD2_NO_STATUS = 1  And PRD2_YEAR =  " & CInt(Lb_Year.Caption) & "  AND PRD2_NO='" & strPBT5No & "' And  ((A.PRD2_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                                        " AND  B.OWNERSHIP_ID  = '" & Trim$(strOwnerShipID) & "'" & " ORDER BY A.PRD2_DATE,A.BUILDING_ID"
'                                        Case "3", "7"       ' ����
'                                                    strSQL = " SELECT PP1_STATUS,Format(A.PP1_DATE_RETURN,'dd/mm/yyyy') As DateReturn , A.SIGNBORD_ID , SIGNBORD_ROUND,'�-' &  E.SOI_NAME & ' �.' &  F.STREET_NAME & ' �.' &  D.TAMBON_NAME & ' �.' & G.AMPHOE_NAME , Format(A.PP1_AMOUNT_ACCEPT,'###,###,###0.00##') , B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName,H.LAND_ID,PP1_NO,PP1_NO_ACCEPT,PP1_DATE AS PERFORM_DATE,A.PP1_INSTALLMENT_ID,Format(A.NOTICE_DATE,'dd/mm/yyyy'),'' AS PAYMENT" & _
'                                                                        " FROM (AMPHOE G RIGHT JOIN TAMBON D ON G.AMPHOE_ID = D.AMPHOE_ID) RIGHT JOIN (STREET F RIGHT JOIN (SOI E RIGHT JOIN (LANDDATA H RIGHT JOIN ((OWNERSHIP B RIGHT JOIN PP1 A ON B.OWNERSHIP_ID = A.OWNERSHIP_ID) LEFT JOIN SIGNBORDDATA C ON A.SIGNBORD_ID = C.SIGNBORD_ID) ON H.LAND_ID = C.LAND_ID) ON E.SOI_ID = H.SOI_ID) ON F.STREET_ID = H.STREET_ID) ON D.TAMBON_ID = H.TAMBON_ID" & _
'                                                                        " WHERE PP1_STATUS = 0 AND PP1_NO_STATUS = 1  AND PP1_YEAR =  " & CInt(Lb_Year.Caption) & "  AND PP1_NO='" & strPBT5No & "' And  ((A.PP1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                                        " AND  B.OWNERSHIP_ID  = '" & Trim$(strOwnerShipID) & "'" & " ORDER BY A.PP1_DATE,A.SIGNBORD_ID "
'                                        Case "4"
'                                                    strSQL = "SELECT PBA1_STATUS,PBA1_DATE AS PERFORM_DATE,Format(A.PBA1_DATE_RETURN,'dd/mm/yyyy') As DateReturn , A.LICENSE_BOOK ,A.LICENSE_NO, (SELECT  DISTINCT '�-' &  E2.SOI_NAME &  ' �.'  &  F2.STREET_NAME & ' �.' &  D2.TAMBON_NAME &  ' �.' & G2.AMPHOE_NAME  FROM LANDDATA A2 , LICENSEDATA B2  , TAMBON D2 , SOI E2 , STREET F2 , AMPHOE G2 ,PBA1 H2" & _
'                                                                        " Where B2.LAND_ID = A2.LAND_ID And A2.TAMBON_ID = D2.TAMBON_ID And E2.SOI_ID = A2.SOI_ID And F2.STREET_ID = A2.STREET_ID  AND G2.AMPHOE_ID = D2.AMPHOE_ID AND H2.LICENSE_BOOK = B2.LICENSE_BOOK  AND H2.LICENSE_NO = B2.LICENSE_NO  AND STATUS_LINK =1 AND C.LICENSE_BOOK = B2.LICENSE_BOOK AND C.LICENSE_NO = B2.LICENSE_NO) ," & _
'                                                                        " (SELECT  DISTINCT '�-' &  E2.SOI_NAME &  ' �.'  &  F2.STREET_NAME & ' �.' &  D2.TAMBON_NAME &  ' �.' & G2.AMPHOE_NAME FROM LANDDATA A2 , LICENSEDATA B2 ,BUILDINGDATA C2 , TAMBON D2 , SOI E2 , STREET F2 , AMPHOE G2 ,PBA1 H2  Where B2.Building_id = C2.Building_id And C2.LAND_ID = A2.LAND_ID And A2.TAMBON_ID = D2.TAMBON_ID And E2.SOI_ID = A2.SOI_ID" & _
'                                                                        " And F2.STREET_ID= A2.STREET_ID  AND H2.LICENSE_BOOK = B2.LICENSE_BOOK  AND H2.LICENSE_NO = B2.LICENSE_NO  AND G2.AMPHOE_ID = D2.AMPHOE_ID AND STATUS_LINK =2 AND C.LICENSE_BOOK = B2.LICENSE_BOOK AND C.LICENSE_NO = B2.LICENSE_NO)  ,'',PBA1_AMOUNT,STATUS_LINK ,B.PRENAME & B.OWNER_NAME & ' ' & B.OWNER_SURNAME as StillName ,PBA1_NO,PBA1_NO_ACCEPT ,'' AS PAYMENT" & _
'                                                                        " FROM PBA1 A,  OWNERSHIP B, LICENSEDATA C  Where  C.LICENSE_BOOK = A.LICENSE_BOOK  AND C.LICENSE_NO = A.LICENSE_NO AND PBA1_STATUS = 0  AND B.OWNERSHIP_ID = A.OWNERSHIP_ID AND PBA1_YEAR =  " & CInt(Lb_Year.Caption) & " AND  B.OWNERSHIP_ID  = '" & Trim$(strOwnerShipID) & "'" & _
'                                                                        " Order by A.LICENSE_BOOK ,A.LICENSE_NO"
'                                End Select
'
'                                 Call SET_QUERY(strSQL, Rs)
                                  If rs.RecordCount > 0 Then
                                        Set Grid_Result.DataSource = rs
                                                    If LB_Menu.Tag <> 4 Then
                                                                  If rs.Fields("DateReturn").Value = Empty Or IsNull(rs.Fields("DateReturn").Value) Then
                                                                        lb_Date_Accept.Caption = ""
                                                                  Else
                                                                        lb_Date_Accept.Caption = Format$(rs.Fields("DateReturn").Value, "dd/mm/yyyy")
                                                                  End If
                                                                  If IsNull(rs.Fields("PERFORM_DATE").Value) Then
                                                                        lb_Date_Perform.Caption = ""
                                                                  Else
                                                                        lb_Date_Perform.Caption = Format$(rs.Fields("PERFORM_DATE").Value, "dd/mm/yyyy")
                                                                  End If
                                                    End If
                                                                ' LbCountPayment.Caption = Rs.RecordCount
                                                                For i = 1 To Grid_Result.Rows - 1
                                                                            If LB_Menu.Tag = 1 Or LB_Menu.Tag = 5 Or LB_Menu.Tag = 4 Then       '  ���պ��ا��ͧ���
                                                                                               If Grid_Result.TextMatrix(i, 0) <> "0" Then
                                                                                                    Grid_Result.TextMatrix(i, 0) = "����"
                                                                                               ElseIf Grid_Result.TextMatrix(i, 0) = "0" And Grid_Result.TextMatrix(i, 12) <> "" Then
                                                                                                      Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ"
                                                                                               ElseIf Grid_Result.TextMatrix(i, 0) = "0" And Grid_Result.TextMatrix(i, 12) = "" Then
                                                                                                      Grid_Result.TextMatrix(i, 0) = "�ѧ������"
                                                                                               End If
                                                                              ElseIf LB_Menu.Tag <> 1 And LB_Menu.Tag <> 5 Then        '  ���պ��ا�ç���͹��з��Թ  / ���ջ���
                                                                                               If Grid_Result.TextMatrix(i, 0) <> "0" Then
                                                                                                    Grid_Result.TextMatrix(i, 0) = "����"
                                                                                               ElseIf Grid_Result.TextMatrix(i, 0) = "0" And Grid_Result.TextMatrix(i, 11) <> "" Then
                                                                                                      Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ"
                                                                                               ElseIf Grid_Result.TextMatrix(i, 0) = "0" And Grid_Result.TextMatrix(i, 11) = "" Then
                                                                                                      Grid_Result.TextMatrix(i, 0) = "�ѧ������"
                                                                                               End If
                                                                              End If
                                                                              
'                                                                              If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                                                        Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 6)
                                                                                        Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 5)
'                                                                              End If
'                                                                              If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
'                                                                                     Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 6)
'                                                                                     Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 5)
'                                                                              End If
'                                                                              If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
'                                                                                        Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 6)
'                                                                                        Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 5)
'                                                                               End If
                                                                              If LB_Menu.Tag = "4" Then
                                                                                        Lb_PBT_NO.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 3)
                                                                                        Lb_PBT_NO_ACCEPT_OLD.Caption = Grid_Result.TextMatrix(i, Grid_Result.Cols - 2)
                                                                               End If
                                                                    Next i
                                                                    
                                                                    
                                                                 Call CalOfPay
                                                              '   Lb_Name_Now.Caption = rs.Fields("StillName").Value
'                                                                 If Lb_PBT_NO.Caption = "" Then
'                                                                                    '���ҧ�������
'                                                                                    Select Case LB_Menu.Tag
'                                                                                                 Case "1", "5"
'                                                                                                        strSQL = " SELECT PBT5_NO From PBT5  Where  PBT5_YEAR = " & CInt(Lb_Year.Caption) & " AND (PBT5_NO IS NOT NULL AND PBT5_NO <> '') Group by  PBT5_NO Order by  PBT5_NO "
'                                                                                                 Case "2", "6"
'                                                                                                        strSQL = " SELECT PRD2_NO From PRD2  Where  PRD2_YEAR = " & CInt(Lb_Year.Caption) & " AND (PRD2_NO IS NOT NULL AND PRD2_NO <> '') Group by  PRD2_NO Order by  PRD2_NO "
'                                                                                                 Case "3", "7"
'                                                                                                        strSQL = " SELECT PP1_NO From PP1  Where  PP1_YEAR = " & CInt(Lb_Year.Caption) & " AND (PP1_NO IS NOT NULL AND PP1_NO <> '') Group by  PP1_NO Order by  PP1_NO "
'                                                                                     End Select
'                                                                                    Call SET_QUERY(strSQL, Rs)
'                                                                                    If Rs.RecordCount > 0 Then
'                                                                                        Rs.MoveLast
'                                                                                        Select Case LB_Menu.Tag
'                                                                                                        Case "1", "5"
'                                                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Rs.Fields("PBT5_NO").Value, 4)) + 1, "###000#")
'                                                                                                        Case "2", "6"
'                                                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Rs.Fields("PRD2_NO").Value, 4)) + 1, "###000#")
'                                                                                                        Case "3", "7"
'                                                                                                                Lb_PBT_NO.Caption = Format$(CInt(Left$(Rs.Fields("PP1_NO").Value, 4)) + 1, "###000#")
'                                                                                        End Select
'                                                                                                                Lb_PBT_NO.Caption = Lb_PBT_NO.Caption & "/" & Right$(Year(Now), 2)
'                                                                                    Else
'                                                                                                                Lb_PBT_NO.Caption = "0001/" & Right$(Year(Now), 2)
'                                                                                    End If
'                                                                 End If
'                                                                 If LB_Menu.Tag = "4" Then Lb_PBT_NO.Caption = Empty
                                                                 Chk_RoundMoney.Value = Checked
                                                                 Call SetGridPayment
                                  Else
                                            Call Clear_Grid
                                  End If
                                  OptionPayment2.Value = True
        End If
        
End Sub

Public Sub chkStatusNum(strOwnerID As String, strYear As String)
    Dim strSQL As String
            With Grid_TaxCount
                            '************************ PBT5 **********************************
'                            strSQL = "SELECT Count(PBT5.LAND_ID) AS Count_ID From PBT5 WHERE ((PBT5.OWNERSHIP_ID)='" & strOwnerID & "') And ((PBT5.PBT5_YEAR)=" & CInt(strYear) & ") And  ((PBT5.PBT5_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                            " And ((PBT5.PBT5_STATUS) =0)"
'                             Call SET_QUERY(strSQL, Rs)
                            Set rs = Globle_Connective.Execute("sp_count_numOwner " & strOwnerID & "," & strYear & ",'" & setGK & "', 1", adCmdUnknown)
                            
                            If rs.RecordCount > 0 Then
                                    .TextMatrix(1, 1) = Format(rs("Count_ID"), "#,##0")
                            Else
                                    .TextMatrix(1, 1) = 0
                            End If
                            '******************** PRD2 *****************************************
'                            strSQL = "SELECT Count(PRD2.BUILDING_ID) as Count_ID From PRD2 WHERE ((PRD2.OWNERSHIP_ID)='" & strOwnerID & "') And ((PRD2.PRD2_YEAR)=" & CInt(strYear) & ") And  ((PRD2.PRD2_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                        " And ((PRD2.PRD2_STATUS) =0)"
'                             Call SET_QUERY(strSQL, Rs)
                            Set rs = Globle_Connective.Execute("sp_count_numOwner " & strOwnerID & "," & strYear & ",'" & setGK & "', 2", adCmdUnknown)

                            If rs.RecordCount > 0 Then
                                    .TextMatrix(2, 1) = Format(rs("Count_ID"), "#,##0")
                            Else
                                    .TextMatrix(2, 1) = 0
                            End If
            
                            '************************PP1**************************************
'                             strSQL = "SELECT Count(SIGNBORD_ID) AS Count_ID From PP1 WHERE ((PP1.OWNERSHIP_ID)='" & strOwnerID & "') And ((PP1.PP1_YEAR)=" & CInt(strYear) & ") And  ((PP1.PP1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                " And ((PP1.PP1_STATUS) =0)"
'                             Call SET_QUERY(strSQL, Rs)
                            Set rs = Globle_Connective.Execute("sp_count_numOwner " & strOwnerID & "," & strYear & ",'" & setGK & "', 3", adCmdUnknown)

                            If rs.RecordCount > 0 Then
                                    .TextMatrix(3, 1) = Format(rs("Count_ID"), "#,##0")
                            Else
                                    .TextMatrix(3, 1) = 0
                            End If
                            
                             '************************PBA1**************************************
'                            strSQL = "SELECT LICENSE_BOOK,LICENSE_NO From PBA1 WHERE ((PBA1.OWNERSHIP_ID)='" & strOwnerID & "') And ((PBA1.PBA1_YEAR)=" & CInt(strYear) & ") And  ((PBA1.PBA1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                            " And ((PBA1.PBA1_STATUS)=0)"
'                            Call SET_QUERY(strSQL, Rs)
                            Set rs = Globle_Connective.Execute("sp_count_numOwner " & strOwnerID & "," & strYear & ",'" & setGK & "', 4", adCmdUnknown)

                            If rs.RecordCount > 0 Then
                                   .TextMatrix(4, 1) = Format(rs.RecordCount, "#,##0")
                            Else
                                   .TextMatrix(4, 1) = 0
                            End If
                End With
End Sub

Private Sub ChkStatusNum_Tax(strOwnerID As String, strYear As String)
            Select Case LB_Menu.Tag
                        Case "1", "5"        '****** PBT5 : �������ŧ���Թ ********
                                            '********************************** �ѧ������Ẻ ****************
'                                            Debug.Print "sp_count_numType " & strOwnerID & "," & strYear & ",'" & setGK & "', 1, 1"
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 1, 1", adCmdUnknown)
                                             If rs.RecordCount > 0 Then
                                                    Label7(3).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(3).Caption = 0
                                            End If
                                            '********************************** ���Ẻ�����ͻ����Թ ���� ���� ****************
'                                            Debug.Print "sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 1, 2"
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 1, 2", adCmdUnknown)
                                             If rs.RecordCount > 0 Then
                                                    Label7(4).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(4).Caption = 0
                                            End If
                                            '*************************** �����Թ ����  ���Ẻ �����ͪ���********************************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 1, 3", adCmdUnknown)
                                             If rs.RecordCount > 0 Then
                                                    Label7(5).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(5).Caption = 0
                                            End If

                        Case "2", "6"        ' ******************** PRD2 : ���͡�������ç���͹ ******************
                                            '********************************** �ѧ������Ẻ ****************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 2, 1", adCmdUnknown)
                                            If rs.RecordCount > 0 Then
                                                    Label7(3).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(3).Caption = 0
                                            End If
                                            '********************************** ���Ẻ�����ͻ����Թ ���� ���� ****************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 2, 2", adCmdUnknown)
                                            If rs.RecordCount > 0 Then
                                                    Label7(4).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(4).Caption = 0
                                            End If
                                            '*************************** �����Թ ����  ���Ẻ �����ͪ��� ********************************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 2, 3", adCmdUnknown)
                                             If rs.RecordCount > 0 Then
                                                    Label7(5).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(5).Caption = 0
                                            End If


                        Case "3", "7"       '************************PP1 : ���͡���������� **************************************
                                            '********************************** �ѧ������Ẻ ****************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 3, 1", adCmdUnknown)
                                            If rs.RecordCount > 0 Then
                                                    Label7(3).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(3).Caption = 0
                                            End If
                                            '********************************** ���Ẻ�����ͻ����Թ ���� ����  ****************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 3, 2", adCmdUnknown)
                                            If rs.RecordCount > 0 Then
                                                    Label7(4).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(4).Caption = 0
                                            End If
                                            '*************************** �����Թ ����  ���Ẻ �����ͪ��� ********************************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 3, 3", adCmdUnknown)
                                             If rs.RecordCount > 0 Then
                                                    Label7(5).Caption = Format(rs("Count_ID"), "#,##0")
                                            Else
                                                    Label7(5).Caption = 0
                                            End If
                            Case "4"    '************************PP1 : ���͡�������͹حҵ **************************************

                                          '**********************************  �͹حҵ�����ա�����Ẻ ��� �����Թ  ****************
                                            Label7(3).Caption = 0
                                            Label7(4).Caption = 0
                                             '*************************** �����Թ ����  ���Ẻ �����ͪ��� ********************************
                                            Set rs = Globle_Connective.Execute("sp_count_numType '" & strOwnerID & "'," & strYear & ",'" & setGK & "', 4, 1", adCmdUnknown)
                                            If rs.RecordCount > 0 Then
                                                    Label7(5).Caption = Format(rs.RecordCount, "#,##0")
                                            Else
                                                    Label7(5).Caption = 0
                                            End If
            End Select
End Sub

Private Sub Lb_DateOver_Change()
On Error Resume Next
  If CInt(Lb_DateOver.Caption) < 0 Then
        Lb_DateOver.Caption = "0"
  End If
  If LB_Menu.Tag <> "3" Or LB_Menu.Tag <> "5" Then Lb_MoneyDue.Caption = "0.00"
End Sub



Private Sub Lb_MoneyDue_Perform1_Change()
        On Error Resume Next
        If Lb_MoneyDue_Perform1.Caption = "" Then Lb_MoneyDue_Perform1.Caption = "0.00"
        If CCur(Val(Lb_MoneyDue_Perform1.Caption)) = 0 Then Exit Sub
                If StatusInstallment = False Then
                    If (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) <> (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
'                            Lb_Discount1.Caption = (CCur(txtSumAmount.Text) - CCur(txtSummary.Text))
                            Lb_Discount1.Caption = FormatNumber(CCur(txtSumAmount.Text) - (CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption)), 2, vbTrue)
                    ElseIf (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) = (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
                            Lb_Discount1.Caption = 0                 '(CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption))
                    End If
                End If
                    If CCur(Lb_Discount1.Caption) > 0 And Left(Lb_Discount1.Caption, 1) <> "+" Then Lb_Discount1.Caption = "+" & Lb_Discount1.Caption

End Sub


Private Sub Lb_MoneyDue1_Change()
    On Error Resume Next
        If Lb_MoneyDue1.Caption = "" Then Lb_MoneyDue1.Caption = "0.00"
        If CCur(Val(Lb_MoneyDue1.Caption)) = 0 Then Exit Sub
                If StatusInstallment = False Then
                    If (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) <> (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
'                            Lb_Discount1.Caption = (CCur(txtSumAmount.Text) - CCur(txtSummary.Text))
                            Lb_Discount1.Caption = FormatNumber(CCur(txtSumAmount.Text) - (CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption)), 2, vbTrue)
                    ElseIf (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) = (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
                            Lb_Discount1.Caption = 0                 '(CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption))
                    End If
                End If
                    If CCur(Lb_Discount1.Caption) > 0 And Left(Lb_Discount1.Caption, 1) <> "+" Then Lb_Discount1.Caption = "+" & Lb_Discount1.Caption

End Sub


Private Sub Lb_Year_Change()
        LbYearSelect.Caption = Lb_Year.Caption
        Label3(31).Caption = "/" & Right$(LbYearSelect.Caption, 2)
'        Label3(28).Caption = "/" & Right$(LbYearSelect.Caption, 2)
'        Label3(33).Caption = "/" & Right$(LbYearSelect.Caption, 2)
        strOwnerShipID = ""
        LbTaxNm.Caption = ""
        LB_Menu.Caption = ""
        SSTab1.Tab = 0
        ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
        LB_Menu.Tag = "1"
         With Grid_TaxCount
                     If chkStatusType = 0 Then
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                    Else
                            LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                    End If
        End With
        Call Clear_Grid
        Call clearSchGrid
        Call SetResultSchGrid
End Sub

Private Sub LbArrear_Change()
        txtPayInstallment.Text = LbArrear.Caption
End Sub

Private Sub MaskEdBox1_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub MaskEdBox4_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub MaskEdBox4_LostFocus()
   If InStr(1, MaskEdBox4.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(MaskEdBox4.Text) And Mid$(MaskEdBox4.Text, 4, 2) < 13 Then
                If LB_Menu.Tag <> "4" Then Call CheckForGridResult
                'MaskEdBox7.Text = MaskEdBox4.Text
                ChkInstallment.Value = 0
                ChkInstallment.Enabled = False
                Lb_DateOver.Caption = "0"
                Call CalOfPay
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox4.SetFocus
        End If
End Sub

Private Sub MaskEdBox7_KeyDown(KeyCode As Integer, Shift As Integer)
    If StatusInstallment = True Then
            MaskEdBox7.Enabled = False
            MsgBox "�ա���駪����繧Ǵ����", vbExclamation, "�Ӫ��ᨧ"
    Else
            MaskEdBox7.Enabled = True
    End If
End Sub

Private Sub MaskEdBox7_LostFocus()
    If InStr(1, MaskEdBox7.Text, "_", vbTextCompare) > 0 Then Exit Sub
        If IsDate(MaskEdBox7.Text) And Mid$(MaskEdBox7.Text, 4, 2) < 13 Then
                'MaskEdBox7.Enabled = True
                ' check ��� �ա������ѧ
                If StatusInstallment = False Then
                            LbInstallmentNo.Caption = "1"
                            chkInstallmentID
                             LbFinePrice.Caption = "0.00"
                            Call CalOfPay
                            Call SetPayInstallment
                End If
                
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox7.SetFocus
        End If
End Sub

Private Sub MaskEdBox8_LostFocus()
        If InStr(1, MaskEdBox8.Text, "_", vbTextCompare) > 0 Then Exit Sub
            If IsDate(MaskEdBox8.Text) And Mid$(MaskEdBox8.Text, 4, 2) < 13 Then
                    If StatusInstallment = True Then
                            Call DisplayInsatllment
                    End If
                        Call getInstallmentNO
                        Call chkFineMoneyInstallment
                        If CCur(txtPayInstallment.Text) <> CInt(txtPayInstallment.Text) Then
                                Chk_RoundMoney2.Enabled = True
                                Combo3.Enabled = True
                        End If
                        
                If StatusInstallment = True And LbFinePrice.Caption <> "0.00" Then
                        With Grid_Installment
                                .TextMatrix(CInt(LbInstallmentNo.Caption), 4) = Format$(txtPayInstallment.Text, "#,##0.00")
                        End With
                End If
        Else
                MsgBox "�ѹ������١��ͧ ��س�������ѹ������١��ͧ���� !!!", vbExclamation, "��ͼԴ��Ҵ"
                MaskEdBox8.SetFocus
                Exit Sub
        End If
End Sub

Private Sub Option1_Click()
        If Grid_Result_PBT5.TextMatrix(1, 0) = Empty Then Exit Sub
        Call CheckForPBT5
End Sub

Private Sub Option2_Click()
        If Grid_Result_PBT5.TextMatrix(1, 0) = Empty Then Exit Sub
        Call CheckForPBT5
End Sub


Private Sub OptionChkType_Click(Index As Integer)
    ' index=0 �.�. & ������� // index=1 ������ �
    chkStatusType = Index
    If chkStatusType = 0 Then  ' value=0 �.�. & ������� // value=1 ������ �
           setGK = "<>"
    Else
           setGK = "="
    End If
'    If chkStatusType = 0 And SSTab1.Tab = 0 Then
'        LB_Menu.Caption = "���պ��ا��ͧ���"
'    Else
'        LB_Menu.Caption = "���պ��ا��ͧ��� (������ �)"
'    End If
     LB_Menu.Tag = "1"
     With Grid_TaxCount
                 If chkStatusType = 0 Then
                        LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                Else
                        LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                End If
    End With

        strOwnerShipID = ""
        LbTaxNm.Caption = ""
        LB_Menu.Caption = ""
        SSTab1.Tab = 0
        ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
        Call Clear_Grid
        Call clearSchGrid
        Call SetResultSchGrid
        Call clrTextFrmPayNormal
        PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
        LbPaymentTitle.ForeColor = 16448
End Sub

Private Sub OptionPayment1_Click()
        On Error Resume Next
        If Grid_Result.TextMatrix(1, 0) = Empty Or LB_Menu.Tag = "4" Then Exit Sub
        Call CheckForGridResult
        Call CalOfPay
        With MaskEdBox4
                .Text = Format$(Date, "dd/mm/yyyy")
                Call SetSelFocus(MaskEdBox4)
        End With
End Sub

Private Sub OptionPayment2_Click()
 If Grid_Result.TextMatrix(1, 0) = Empty Or LB_Menu.Tag = "4" Then Exit Sub
'         chkInstallment.Enabled = False
        Call CheckForGridResult
        Summary = 0
        Lb_Count.Caption = "0"
        Txt_Summary.Text = "0.00"
        Txt_RoundMoney.Text = "0.00"
End Sub

Private Sub PicPayment_Click()
        Dim strYear As String
        'Dim intLBTag as Integer
        If strOwnerShipID <> "" Then
                strYear = Lb_Year.Caption
                Frm_CalPayment.Lb_SetGK = setGK
                Call Frm_CalPayment.sumAmountAllData(strOwnerShipID, strYear, LB_Menu.Tag)
                Frm_CalPayment.Show
        Else
                MsgBox "��س����͡��������ª��ͼ���������ա�͹ !!", vbInformation, "�Ӫ��ᨧʶҹС����"
        End If
End Sub

Private Sub PicPayment_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
            PicPayment.Top = PicPayment.Top + 40
            PicPayment.Picture = LoadResPicture(1001, vbResBitmap)
End Sub

Private Sub PicPayment_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
        PicPayment.MouseIcon = LoadResPicture(102, vbResCursor)
        If ReleaseObject(X, y, PicPayment) Then
                PicPayment.Picture = LoadResPicture(1001, vbResBitmap)
                PicPayment.Top = 75
        Else
                PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
        End If
End Sub

Private Sub PicPayment_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
        PicPayment.Picture = LoadResPicture(1001, vbResBitmap)
        PicPayment.Top = 75
End Sub

Function chkPayment() As Boolean
    Dim strYear As String
    Dim strSQL As String
    Dim chkPaymentVal As Boolean
    
    strYear = Lb_Year.Caption
    strSQL = "SELECT PBT5.* From PBT5 WHERE (((PBT5.PBT5_YEAR)<" & strYear & ") AND ((PBT5.PBT5_STATUS)=0) AND ((PBT5.OWNERSHIP_ID)='" & strOwnerShipID & "'))"
    Call SET_QUERY(strSQL, rs)
     If rs.RecordCount > 0 Then
            chkPayment = True
            Exit Function
    Else
            chkPayment = False
            Exit Function
    End If
End Function

Private Sub ShockwaveFlash1_FSCommand(ByVal command As String, ByVal args As String)
                Select Case CInt(args)
                            Case 0, 2
                                        If strOwnerShipID <> "" Then
                                                If LB_Menu.Tag <> 4 Then
                                                        MaskEdBox4.Text = Format$(Date, "dd/mm/yyyy")
                                                        SSTab1.Tab = CInt(args)
                                                Else
                                                            ShockwaveFlash1.GotoFrame (CLng(2))
                                                            MsgBox "�������������͹حҵ��Сͺ��ä������ö�������������", vbInformation, "�����Ť��й�"
                                                End If
                                        Else
                                                SSTab1.Tab = 0
                                                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                                                MsgBox "��س����͡��������ª��ͼ���������ա�͹ !!", vbInformation, "�Ӫ��ᨧʶҹС����"
                                        End If
                            Case 1      ' AcceptTax
                                        If LB_Menu.Tag <> 4 Then
                                                SSTab1.Tab = CInt(args)
                                        Else
                                                ShockwaveFlash1.GotoFrame (CLng(2))
                                                MsgBox "�������������͹حҵ��Сͺ��ä������ö�������������", vbInformation, "�����Ť��й�"
                                        End If
                End Select
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    Dim strYear As String
    Dim strSelTaxType As String
    
    strYear = Lb_Year.Caption
    strSelTaxType = CStr(Grid_TaxCount.TextMatrix(Grid_TaxCount.RowSel, 2))
    If strOwnerShipID <> "" Then
            Select Case SSTab1.Tab
                    Case 0      ' ���Ẻ
                                If strSelTaxType = "PBT5" Then
                                        BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.5"
                                        Label2(1).Caption = "�ӹǹ�ŧ :"
                                ElseIf strSelTaxType = "PRD2" Then
                                        BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.�.2"
                                        Label2(1).Caption = "�ӹǹ�ç���͹ :"
                                ElseIf strSelTaxType = "PP1" Then
                                        BtnPostPreformTax.Caption = "�Ѵ����¡�� �.�.1"
                                        Label2(1).Caption = "�ӹǹ���� :"
                                End If
                    Case 1      '�����Թ
                                If strSelTaxType = "PBT5" Then
                                        btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.9"
                                ElseIf strSelTaxType = "PRD2" Then
                                        btnPostAcceptTax.Caption = "�Ѵ����¡�� �.�.�.8"
                                ElseIf strSelTaxType = "PP1" Then
                                        btnPostAcceptTax.Caption = "�Ѵ����¡�� (�.�.3)"
                                End If
                                
                                Call SetGridAcceptTax
                                getPBT5No
                                getNoAccept
                    Case 2      ' ����
                                If strSelTaxType = "PBT5" Then
                                        BtnPostPayment.Caption = "�������� �.�.�.11"
                                        Label3(24).Caption = "�ŧ���Թ :"
                                ElseIf strSelTaxType = "PRD2" Then
                                        BtnPostPayment.Caption = "�������� �.�.�.12"
                                        Label3(24).Caption = "�ç���͹ :"
                                ElseIf strSelTaxType = "PP1" Then
                                        BtnPostPayment.Caption = "�������� �.�.7"
                                        Label3(24).Caption = "���� :"
                                ElseIf strSelTaxType = "PBA1" Then
                                        BtnPostPayment.Caption = "���������͹حҵ�"
                                        Label3(24).Caption = "�͹حҵ� :"
                                End If
                                
                                If LB_Menu.Tag <> 4 Then
                                        getPBT5NoPayTax
                                        ComboPayTax.Enabled = True
                                        Btn_Print_Payment.Caption = ""
                                        Btn_Print_PBA1.Visible = False
                                Else
                                        ComboPayTax.Enabled = False
                                        Btn_Print_Payment.Caption = "�͹حҵ�"
                                        Btn_Print_PBA1.Visible = True
                                End If
            End Select
                       
            If SSTab1.Tab = 0 Then      '   select  tab -- > ���Ẻ
                        If LB_Menu.Tag <> 4 Then Call DisplayPreform(Lb_Year.Caption, 0)
            ElseIf SSTab1.Tab = 1 Then      '   select  tab -- > �����Թ
                        If LB_Menu.Tag <> 4 Then Call DisplayAccept(Lb_Year.Caption, 1)
                        If ComboNoAccept.Text <> "" Then
                                getPerformNo (CStr(ComboNoAccept.Text))
                        End If
            ElseIf SSTab1.Tab = 2 Then      '   select  tab -- > ����
                        FramePayInsallment.Visible = False
                        FramePayNormal.Visible = True
                        ChkInstallment.Enabled = False
                        ChkInstallment.Value = 0
                        Call DisplayPayment(Lb_Year.Caption, 2)
            End If
    Else
            'MsgBox "��س����͡��������ª��ͼ���������� !!!", vbInformation
    End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789/")
End Sub

Private Sub Txt_BOOK_NO_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                SendKeys "{Tab}"
                Txt_RoundMoney.SelStart = 0
                Txt_RoundMoney.SelLength = Len(Txt_RoundMoney.Text)
        End If
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_BOOK_NUMBER_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_BOOK_NUMBER_LostFocus()
        Txt_BOOK_NO.SetFocus
End Sub

Private Sub Txt_RoundMoney_Change()
    On Error Resume Next
        If Txt_RoundMoney.Text = "" Then Txt_RoundMoney.Text = 0
        If CCur(Val(Txt_RoundMoney.Text)) = 0 Then Exit Sub
        If (CCur(Txt_RoundMoney.Text) - CCur(Txt_Summary.Text)) <> (CCur(Lb_MoneyDue_Perform.Caption) + CCur(Lb_MoneyDue.Caption)) Then
'                Lb_discount.Caption = (CCur(Txt_RoundMoney.Text) - CCur(Txt_Summary.Text))
                Lb_discount.Caption = FormatNumber(CCur(Txt_RoundMoney.Text) - (Summary + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption)), 2, vbTrue)
        ElseIf (CCur(Txt_RoundMoney.Text) - CCur(Txt_Summary.Text)) = (CCur(Lb_MoneyDue_Perform.Caption) + CCur(Lb_MoneyDue.Caption)) Then
                Lb_discount.Caption = 0    '   (CCur(Lb_MoneyDue_Perform.Caption) + CCur(Lb_MoneyDue.Caption))
        End If
        If CCur(Lb_discount.Caption) > 0 Then Lb_discount.Caption = "+" & Lb_discount.Caption
    '    Txt_Summary.Text = FormatNumber((Txt_RoundMoney.Text) - (CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption)), 2, vbTrue)
    chkAmountSelList
End Sub

Private Sub Txt_RoundMoney_KeyPress(KeyAscii As Integer)
        KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
        If FindText(Txt_RoundMoney.Text, ".") > 0 And KeyAscii = 46 Then KeyAscii = 0
End Sub

Private Sub Txt_Summary_Change()
  On Error Resume Next
        If Txt_Summary.Text = "" Then Txt_Summary.Text = 0
End Sub

Private Sub txtBookNo_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub txtBookNumber_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub txtBookNumber_LostFocus()
        txtBookNo.Enabled = True
        txtBookNo.SetFocus
End Sub

Private Sub txtIDCard_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call SchNm
End Sub

Private Sub txtNm_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Me.MousePointer = 11
                LB_Menu.Caption = "���պ��ا��ͧ���"
                strOwnerShipID = ""
                LbTaxNm.Caption = ""
                LB_Menu.Caption = ""
                SSTab1.Tab = 0
                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                With Grid_TaxCount
                             If chkStatusType = 0 Then
                                    LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                            Else
                                    LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                            End If
                End With
                PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
                LbPaymentTitle.ForeColor = 16448
                Call Clear_Grid
                Call clearSchGrid
                Call clrTextFrmPayNormal
                Call SetResultSchGrid
                Call SchNm
                Me.MousePointer = 0
        End If
End Sub

Private Sub txtPayInstallment_Change()
   With Grid_Installment
                If LbInstallmentNo.Caption = "" Then Exit Sub
                If LbInstallmentNo.Caption = "1" Then
        '                If Grid_Installment.TextMatrix(1, 4) <> "" Then
        '                        tempArrearPrice = CCur(txtSumAmount.Text) - CCur(Grid_Installment.TextMatrix(1, 4))
        '                Else
        '                        tempArrearPrice = CCur(txtSumAmount.Text) - CCur(LbArrear.Caption)
        '                End If
                        tempArrearPrice = txtSumAmount.Text
                ElseIf CInt(LbInstallmentNo.Caption) > 1 Then
                        tempArrearPrice = getArrearPrice(CInt(LbInstallmentNo.Caption))
                End If
                
                If txtPayInstallment.Text <> "" And .TextMatrix(CInt(LbInstallmentNo.Caption), .Cols - 1) <> "/" Then
                            If CInt(LbInstallmentNo.Caption) >= 1 And CCur(txtPayInstallment.Text) >= tempArrearPrice Then
                                            txtBookNumber.Locked = False
                                            txtBookNo.Locked = False
                                            txtBookNo.BackColor = &H80000005
                                            txtBookNumber.BackColor = &H80000005
                                             txtBookNumber.Enabled = True
                                             txtBookNo.Enabled = True
                            Else
                                            txtBookNumber.Locked = True
                                            txtBookNo.Locked = True
                                           txtBookNumber.Enabled = False
                                            txtBookNo.Enabled = False
                                            txtBookNumber.Text = Empty
                                            txtBookNo.Text = Empty
                                            txtBookNo.BackColor = &H8000000F
                                            txtBookNumber.BackColor = &H8000000F
                            End If
                End If
        End With
End Sub

Private Sub txtPayInstallment_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789.")
        
End Sub

Private Sub txtSumAmount_Change()
On Error Resume Next
        If txtSumAmount.Text = "" Then txtSumAmount.Text = "0.00"
        If CCur(Val(txtSumAmount.Text)) = 0 Then Exit Sub
                If StatusInstallment = False Then
                    If (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) <> (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
'                            Lb_Discount1.Caption = (CCur(txtSumAmount.Text) - CCur(txtSummary.Text))
                            Lb_Discount1.Caption = FormatNumber(CCur(txtSumAmount.Text) - (CCur(txtSummary.Text) + CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption)), 2, vbTrue)
                    ElseIf (CCur(txtSumAmount.Text) - CCur(txtSummary.Text)) = (CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)) Then
                            Lb_Discount1.Caption = 0                 '(CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption))
                    End If
                End If
                    If CCur(Lb_Discount1.Caption) > 0 And Left(Lb_Discount1.Caption, 1) <> "+" Then Lb_Discount1.Caption = "+" & Lb_Discount1.Caption
                    If Grid_Result.TextMatrix(Grid_Result.RowSel, 0) = "�ѧ������" Then
                            Call SetPayInstallment
                    End If
End Sub

Private Sub txtSumAmount_KeyPress(KeyAscii As Integer)
KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub


Private Sub txtSurNm_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
                Me.MousePointer = 11
                LB_Menu.Caption = "���պ��ا��ͧ���"
                strOwnerShipID = ""
                LbTaxNm.Caption = ""
                LB_Menu.Caption = ""
                SSTab1.Tab = 0
                ShockwaveFlash1.GotoFrame (CLng(SSTab1.Tab))
                With Grid_TaxCount
                             If chkStatusType = 0 Then
                                    LB_Menu.Caption = .TextMatrix(.RowSel, 0)
                            Else
                                    LB_Menu.Caption = .TextMatrix(.RowSel, 0) & " (�������)"
                            End If
                End With
                PicPayment.Picture = LoadResPicture(1000, vbResBitmap)
                LbPaymentTitle.ForeColor = 16448
                Call Clear_Grid
                Call clearSchGrid
                Call SetResultSchGrid
                Call SchNm
                Call clrTextFrmPayNormal
                Me.MousePointer = 0
            End If
End Sub

Private Sub Clear_Grid()
    On Error Resume Next
    Dim i As Byte
'            Select Case SSTab1.Tab
'                    Case 0      ' ���Ẻ
                            Grid_Result_PBT5.Rows = 2
                            For i = 0 To Grid_Result_PBT5.Cols - 1
                                     Grid_Result_PBT5.Col = i
                                     Grid_Result_PBT5.CellBackColor = &HE0E0E0
                                     Grid_Result_PBT5.TextMatrix(1, i) = Empty
                            Next i
                             Txt_PBT_NO.Text = Empty
                             'LbTaxNm.Caption = Empty
                             Lb_money.Caption = Empty
                             Lb_Count.Caption = Empty
                             'strOwnerShipID = Empty
                             MaskEdBox1.Text = "__/__/____"
                             MaskEdBox2.Text = "__/__/____"
'                            Exit Sub
'                    Case 1      ' �����Թ
                            Grid_Result_PBT9.Rows = 2
                            For i = 0 To Grid_Result_PBT9.Cols - 1
                                    Grid_Result_PBT9.TextMatrix(1, i) = Empty
                            Next i
                                Txt_PBT_NO.Text = Empty
                                Text1.Text = Empty
                                'Text2.Text = Empty  chang --> ComboNoAccept
                                ComboNoAccept.Clear
                                Lb_moneyAccept.Caption = "0.00"
                                MaskEdBox6.Text = "__/__/____"
'                    Case 2      ' ����
                            Grid_Result.Rows = 2
                           For i = 0 To Grid_Result.Cols - 1
                                   Grid_Result.TextMatrix(1, i) = Empty
                           Next i
                            'Txt_PBT_NO_ACCEPT.Text = Empty --> ComboPayTax
                            ComboPayTax.Clear
                             Lb_Count.Caption = "0"
                        
                             Txt_Summary.Text = "0.00"
                             Lb_PBT_NO_ACCEPT_OLD.Caption = ""
                             Lb_PBT_NO.Caption = ""
                             LbCountPayment.Caption = ""
                             Lb_discount.Caption = "0.00"
                            Chk_RoundMoney.Value = Unchecked
                            lb_Date_Perform.Caption = ""
                            lb_Date_Accept.Caption = ""
                            MaskEdBox4.Text = "__/__/____"
                            
                            Grid_Installment.Rows = 2
                            For i = 0 To Grid_Installment.Cols - 1
                                   Grid_Installment.TextMatrix(1, i) = Empty
                           Next i
'            End Select
End Sub

Private Sub clearSchGrid()
    On Error Resume Next
    Dim i As Byte
            Grid_ListName.Rows = 2
            For i = 0 To Grid_ListName.Cols - 1
                     Grid_ListName.Col = i
                     'Grid_ListName.CellBackColor = &HE0E0E0
                     Grid_ListName.TextMatrix(1, i) = Empty
            Next i
            Lb_ResultSchNm.Caption = Empty
            Grid_TaxCount.Rows = 2
            For i = 0 To Grid_TaxCount.Cols - 1
                    Grid_TaxCount.TextMatrix(1, i) = Empty
            Next i
            Label7(3).Caption = "0"
            Label7(4).Caption = "0"
            Label7(5).Caption = "0"
End Sub

Private Sub CheckForPBT5()
On Error Resume Next
Dim i  As Byte, j   As Integer
        If SSTab1.Tab = 0 Then
                With Grid_Result_PBT5
                        .ColAlignment(.Cols - 1) = 5
                        For j = 1 To .Rows - 1
                                .Row = j
                                For i = 0 To .Cols - 1
                                        .Col = i
                                    If Option1.Value Then
                                        .CellBackColor = &HC0C0FF
                                        .TextMatrix(j, .Cols - 1) = "/"
                                     Else
                                        .CellBackColor = &HE0E0E0
                                        .TextMatrix(j, .Cols - 1) = Empty
                                     End If
                                  Next i
                        Next j
                End With
        Call Summary_Money
        MaskEdBox1 = Format$(Date, "dd/mm/yyyy")
        MaskEdBox1.SelStart = 0
        MaskEdBox1.SelLength = Len(MaskEdBox1.Text)
        MaskEdBox1.SetFocus
        ElseIf SSTab1.Tab = 2 Then
               For j = 1 To Grid_Result.Rows - 1
                       Grid_Result.Row = j
                       For i = 0 To Grid_Result.Cols - 1
                               Grid_Result.Col = i
                           If OptionPayment1.Value Then
                               Grid_Result.CellBackColor = &HC0C0FF
                               Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = "/"
                            ElseIf OptionPayment2.Value Then
                               Grid_Result.CellBackColor = &H80000018
                               Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                            End If
                         Next i
            Next j
        End If
End Sub

Private Sub CheckForGridResult()
On Error Resume Next
Dim i  As Byte, j   As Integer
        For j = 1 To Grid_Result.Rows - 1
                Grid_Result.Row = j
                For i = 0 To Grid_Result.Cols - 1
                        Grid_Result.Col = i
                    If OptionPayment1.Value Then
                        Grid_Result.CellBackColor = &HC0C0FF
                        Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = "/"
                     Else
                        Grid_Result.CellBackColor = &H80000018
                        Grid_Result.TextMatrix(j, Grid_Result.Cols - 1) = Empty
                     End If
                  Next i
     Next j
End Sub

Private Sub Grid_Result_PBT9_LeaveCell()
'        Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 2) = strTemp
    Dim i As Integer
    
        strTemp2 = Empty
        For i = 1 To Grid_Result_PBT9.Rows - 1
                If Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3) = Empty Or Trim$(Grid_Result_PBT9.TextMatrix(Grid_Result_PBT9.Row, 3)) = "." Then
                        Grid_Result_PBT9.TextMatrix(i, 3) = "0.00"
                End If
                Grid_Result_PBT9.TextMatrix(i, 3) = Format$(Grid_Result_PBT9.TextMatrix(i, 3), "###,##0.00")
        Next i
End Sub

Private Sub getPBT5No()
    Dim strSQL As String
    Dim i As Integer
    
    ComboPBT5No.Clear
'    Select Case LB_Menu.Tag
'                Case "1", "5"       ' ��ͧ���
'                                strSQL = "SELECT left(PBT5.PBT5_NO,4) as PBT5_NO From PBT5 WHERE (((PBT5.PBT5_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PBT5.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PBT5.PBT5_NO_STATUS)=1) AND ((PBT5.PBT5_STATUS)=0)" & _
'                                                " And ((PBT5.PBT5_NO_ACCEPT)='' Or (PBT5.PBT5_NO_ACCEPT) is null )) Group by PBT5.PBT5_NO Order by PBT5.PBT5_NO"
'                Case "2", "6"       ' �ç���͹
'                            strSQL = "SELECT left(PRD2.PRD2_NO,4) as PRD2_NO From PRD2 WHERE (((PRD2.PRD2_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PRD2.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PRD2.PRD2_NO_STATUS)=1) AND ((PRD2.PRD2_STATUS)=0)" & _
'                                                " And ((PRD2.PRD2_NO_ACCEPT)='' Or (PRD2.PRD2_NO_ACCEPT) is null )) Group by PRD2.PRD2_NO Order by PRD2.PRD2_NO"
'                Case "3", "7"       ' ����
'                            strSQL = "SELECT left(PP1.PP1_NO,4) as PP1_NO From PP1 WHERE (((PP1.PP1_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PP1.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PP1.PP1_NO_STATUS)=1) AND ((PP1.PP1_STATUS)=0)" & _
'                                                " And ((PP1.PP1_NO_ACCEPT)='' Or (PP1.PP1_NO_ACCEPT) is  null )) Group by PP1.PP1_NO Order by PP1.PP1_NO"
'        End Select
'
'    Call SET_QUERY(strSQL, Rs)

    Set rs = Globle_Connective.Execute("exec sp_get_NoPerform '" & strOwnerShipID & "'," & Lb_Year.Caption & ", " & LB_Menu.Tag, adCmdUnknown)
    If rs.RecordCount > 0 Then
            i = 0
            Do While Not rs.EOF
                        If i = 0 Then
                                ComboPBT5No.Text = rs.Fields(0)
                                i = 1
                        End If
                        ComboPBT5No.AddItem rs.Fields(0)
            rs.MoveNext
            Loop
    End If
End Sub

Private Sub getPBT5NoPayTax()
    Dim strSQL As String
    Dim i As Integer
    
    ComboPayTax.Clear
'    Select Case LB_Menu.Tag
'                Case "1", "5"       ' ��ͧ���
'                            strSQL = "SELECT left(PBT5.PBT5_NO,4) as PBT5_NO From PBT5 WHERE (((PBT5.PBT5_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PBT5.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PBT5.PBT5_NO_STATUS)=1) AND ((PBT5.PBT5_STATUS)=0))" & _
'                                                " Group by PBT5.PBT5_NO Order by PBT5.PBT5_NO"
'                Case "2", "6"       ' �ç���͹
'                            strSQL = "SELECT left(PRD2.PRD2_NO,4) as PRD2_NO From PRD2 WHERE (((PRD2.PRD2_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PRD2.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PRD2.PRD2_NO_STATUS)=1) AND ((PRD2.PRD2_STATUS)=0))" & _
'                                                " Group by PRD2.PRD2_NO Order by PRD2.PRD2_NO"
'                Case "3", "7"       ' ����
'                            strSQL = "SELECT left(PP1.PP1_NO,4) as PP1_NO From PP1 WHERE (((PP1.PP1_YEAR)=" & CInt(Lb_Year.Caption) & ")" & _
'                                                " AND ((PP1.OWNERSHIP_ID)='" & strOwnerShipID & "') AND ((PP1.PP1_NO_STATUS)=1) AND ((PP1.PP1_STATUS)=0))" & _
'                                                " Group by PP1.PP1_NO Order by PP1.PP1_NO"
'        End Select
'    Call SET_QUERY(strSQL, Rs)
  Set rs = Globle_Connective.Execute("sp_get_NoPayTax " & strOwnerShipID & "," & Lb_Year.Caption & ",'" & setGK & "', " & LB_Menu.Tag, adCmdUnknown)

    If rs.RecordCount > 0 Then
            i = 0
            Do While Not rs.EOF
                If i = 0 Then
                        ComboPayTax.Text = rs.Fields(0)
                        i = 1
                End If
                 ComboPayTax.AddItem rs.Fields(0)
            rs.MoveNext
            Loop
    End If
End Sub

Private Sub ShowReportPreform(File_Name As String, Formula As String, Optional BEGIN_DATE As String, Optional ENDDATE As String)
On Error GoTo ErrHlde
        Call FindPrinterDeFault
        With Clone_Form.CTReport
                .Reset
                .Connect = str_ConnectReport
                .DiscardSavedData = True
                .SelectionFormula = Formula
                .PrinterDriver = TruePrinter.DriverName
                .PrinterPort = TruePrinter.Port
                .PrinterName = TruePrinter.DeviceName
                .ReportFileName = App.Path & "\Report\" & File_Name
                .Destination = crptToPrinter
                .Action = 1 '
        End With
Exit Sub
ErrHlde:
    MsgBox Err.Description & "  �������ö�ʴ� Report ! ", vbCritical, "����͹ !"
End Sub

Private Sub ShowReportPayment(File_Name As String, Formula As String, Output As Boolean, Optional BEGIN_DATE As String, Optional ENDDATE As String)
        On Error GoTo ErrHlde
                Call FindPrinterDeFault
                With Clone_Form.CTReport
                        .Reset
                        .Connect = str_ConnectReport
                        .DiscardSavedData = True
                        .SelectionFormula = Formula
                        .PrinterDriver = TruePrinter.DriverName
                        .PrinterPort = TruePrinter.Port
                        .PrinterName = TruePrinter.DeviceName
                        .ReportFileName = App.Path & "\Report\" & File_Name
                        Select Case LB_Menu.Tag
                                     Case "4"
                                              .Formulas(0) = "TitleName ='" & BEGIN_DATE & "'"
                '                              .Formulas(1) = "Province ='" & iniProvince & "'"
                '                     Case "2"
                '
                        End Select
                        .WindowShowPrintSetupBtn = True
                        If Output Then
                            .Destination = crptToPrinter
                        Else
                            .Destination = crptToWindow
                            .WindowState = crptMaximized
                            .WindowShowExportBtn = True
                        End If
                        .Action = 1
                End With
        Exit Sub
ErrHlde:
MsgBox Err.Description & "  �������ö�ʴ� Report ! ", vbCritical, "����͹ !"
End Sub

Private Sub ShowReportAcceptTax(File_Name As String, Formula As String, Optional BEGIN_DATE As String, Optional ENDDATE As String)
On Error GoTo ErrHlde
Call FindPrinterDeFault
With Clone_Form.CTReport
        .Reset
        .Connect = str_ConnectReport
        .DiscardSavedData = True
        .SelectionFormula = Formula
        .PrinterDriver = TruePrinter.DriverName
        .PrinterPort = TruePrinter.Port
        .PrinterName = TruePrinter.DeviceName
        Select Case LB_Menu.Tag
                Case "1"
                          
                Case "2"
                          .Formulas(0) = "Tambon ='" & iniTambon & "'"
                Case "3"
                         
        End Select
         .ReportFileName = App.Path & "\Report\" & File_Name
         .Destination = crptToPrinter
         .Action = 1
Exit Sub
End With
ErrHlde:
MsgBox Err.Description & "  �������ö�ʴ� Report ! ", vbCritical, "����͹ !"
End Sub

Private Sub CalOfPay()
        On Error Resume Next
                        Dim i As Integer
                        Summary = 0
                        'Lb_Count.Caption = "0"
                        LbCountPayment.Caption = "0"
                        Lb_MoneyDue.Caption = "0.00"
                        Lb_MoneyDue_Perform.Caption = "0.00"
                        Lb_MoneyDue1.Caption = "0.00"
                        Lb_MoneyDue_Perform1.Caption = "0.00"
                        i = 1
                        For i = 1 To Grid_Result.Rows - 1
                             '   If Grid_Result.TextMatrix(i, 0) = "�ѧ������" And Grid_Result.TextMatrix(i, Grid_Result.Cols - 1) = "/" Then
                                If Grid_Result.TextMatrix(i, Grid_Result.Cols - 1) = "/" And (Grid_Result.TextMatrix(i, 0) = "�����繧Ǵ" Or Grid_Result.TextMatrix(i, 0) = "�ѧ������") Then
                                            LbCountPayment.Caption = CInt(LbCountPayment.Caption) + 1
                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                Grid_Result.TextMatrix(i, 11) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 6)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 11))
                                        End If
                                        If LB_Menu.Tag = "2" Or LB_Menu.Tag = "6" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 10))
                                        End If
                                        If LB_Menu.Tag = "3" Or LB_Menu.Tag = "7" Then
                                                Grid_Result.TextMatrix(i, 10) = CalculateTax(CCur(Grid_Result.TextMatrix(i, 5)))
                                                Summary = Summary + CCur(Grid_Result.TextMatrix(i, 10))
                                        End If
                                        If LB_Menu.Tag = "4" Then
                                                    Summary = Summary + CCur(Grid_Result.TextMatrix(i, 8))
                                                    If Grid_Result.TextMatrix(i, 9) = "1" Then
                                                                     Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 4)
                                                    End If
                                                    If Grid_Result.TextMatrix(i, 9) = "2" Then
                                                                    Grid_Result.TextMatrix(i, 6) = Grid_Result.TextMatrix(i, 5)
                                                    End If
                                        End If
                                End If
                                
                                If Grid_Result.TextMatrix(i, 1) <> Empty Then
                                       If CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) < 2500 Then
                                           Grid_Result.TextMatrix(i, 1) = Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm") & _
                                                                                              "/" & CInt(Right$(Format$(CDate(Grid_Result.TextMatrix(i, 1)), "dd/mm/yyyy"), 4)) + 543
                                       End If
                                End If
                               
                        Next i
                        If CInt(Lb_MoneyDue.Caption) <> 0 Then Lb_MoneyDue.Caption = FormatNumber(Lb_MoneyDue.Caption, 2, vbTrue)
                        If CInt(Lb_MoneyDue_Perform.Caption) <> 0 Then Lb_MoneyDue_Perform.Caption = FormatNumber(Lb_MoneyDue_Perform.Caption, 2, vbTrue)
                        Txt_Summary.Text = FormatNumber(Summary, 2, vbTrue)   'END OF CALCULATETAX+++++++++++++++++++++++++++++++++++++++++
                        Txt_RoundMoney.Text = Format$(CCur(Txt_Summary.Text) + CCur(Lb_MoneyDue.Caption) + CCur(Lb_MoneyDue_Perform.Caption), "###,##0.00")
                        
                        If ChkInstallment.Value = 1 Then        ' ��Ǩ�ͺ����� ��ê����繧Ǵ�������
                                Lb_MoneyDue1.Caption = Lb_MoneyDue.Caption
                                Lb_MoneyDue_Perform1.Caption = Lb_MoneyDue_Perform.Caption
                                txtSummary.Text = Txt_Summary.Text
                                txtSumAmount.Text = Txt_RoundMoney.Text
                        End If
End Sub

Private Function CalculateTax(Money As Single) As Single

Dim DateComp As Long
Dim DateComp2 As Long
Dim TaxInclude As Single, TaxInclude2 As Single
Dim Rs_Submit As ADODB.Recordset
Dim Per_Month As Integer
Dim Per_Month2 As Integer
On Error GoTo ErrCal
            Set Rs_Submit = New ADODB.Recordset
            TaxInclude = 0#
            TaxInclude2 = 0#
            DateComp = 0
            
            If ChkInstallment.Value = 1 Then    ' ��Ǩ�ͺ����繪���Ẻ�Ǵ����
                        MaskEdBox4.Text = MaskEdBox7.Text
            End If
            
            If Trim$(MaskEdBox4.Text) = "__/__/____" Then Exit Function
            If lb_Date_Accept.Caption <> "" Then
                    DateComp = DateDiff("d", Format$(CDate(lb_Date_Accept.Caption), "dd/mm/yyyy"), CDate(MaskEdBox4.Text))  ' �ѹ�������Թ�ҡ����Ѻ�Ţ�駻����Թ
                    Select Case LB_Menu.Tag
                            Case 1 '���ا��ͧ���
                                    DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept1.Text) + 1
                            Case 2 '�ç���͹��з��Թ
                                    DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept0.Text)
                            Case 3 '����
                                    DateComp = DateComp - CInt(Frm_Index.Txt_Due_Accept2.Text) + 1
                    End Select
                    If ((DateComp + 30) / 30) - Int((DateComp + 30) / 30) > 0 Then
                            Per_Month = Int(DateComp / 30) + 1
                    Else
                            Per_Month = Int(DateComp / 30)
                    End If
            End If
            DateComp2 = DateDiff("d", CDate("30/04/" & Lb_Year.Caption), CDate(MaskEdBox4.Text))  '�ѹ�������Թ��ѧ�ҡ 31 ��.�. ������ա���駻����Թ
            If DateComp2 / 30 - Int(DateComp2 / 30) > 0 Then
                    Per_Month2 = Int(DateComp2 / 30) + 1
            Else
                    Per_Month2 = Int(DateComp2 / 30)
            End If
Select Case LB_Menu.Tag
             Case "1" '���ا��ͧ���
                            Lb_DateOver.Caption = DateComp
                            
                            Call SET_QUERY("SELECT PBT5_DATE,PBT5_NO_ACCEPT,FLAG_RETURN,PBT5_DATE_RETURN,PBT5_SET_GK,PBT5_OWNER_DATE FROM PBT5 WHERE PBT5_NO='" & Lb_PBT_NO.Caption & "' AND PBT5_YEAR=" & Lb_Year.Caption, Rs_Submit)
                                    If Rs_Submit.Fields("PBT5_SET_GK").Value = 0 Then '*******  �������  *******
                                            With Frm_Index
                                            If IsNull(Rs_Submit.Fields("PBT5_OWNER_DATE").Value) = False Then
                                                    If DateDiff("d", Format$(Rs_Submit.Fields("PBT5_OWNER_DATE").Value, "dd/mm/yyyy"), Format$(Rs_Submit.Fields("PBT5_DATE").Value, "dd/mm/yyyy")) > 30 Then   '���Ẻ��ѧ�ҡ����Ңͧ�����Է����Թ 30 �ѹ
                                                            TaxInclude2 = TaxInclude2 + (Money * CSng(.Txt_Tax_OutSchedule1.Text)) / 100
                                                    End If
                                            End If
                                            If Not IsNull(Rs_Submit.Fields("PBT5_DATE_RETURN").Value) And Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then '   ����ա���駻����Թ
                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                            Lb_DateOver.Caption = DateComp
                                                    End If
                                            End If
                                            End With
                                    Else
                                            With Frm_Index
                                                    If (CInt(Lb_Year.Caption) - 2549) Mod 4 = 0 Then  ' ����㹻���������������
                                                            If Rs_Submit.Fields(0).Value > CDate("31/01/" & (CInt(Lb_Year.Caption) - (CInt(Lb_Year.Caption) - 2549) Mod 4)) Then   '         ������ѹ�������Ҥ�
                                                                    TaxInclude2 = TaxInclude2 + (Money * CSng(.Txt_Tax_OutSchedule1.Text)) / 100      '�Դ��һ�Ѻ���Ẻ�Թ��˹� 10%
                                                                    If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                            If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                            Lb_DateOver.Caption = DateComp
                                                                                    End If
                                                                            ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                                    If CDate(MaskEdBox4.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                                    End If
                                                                            End If
                                                                    End If
                                                            Else             '  ����������Ҥ�
                                                                    If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                            If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                                    If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                            Lb_DateOver.Caption = DateComp
                                                                                    End If
                                                                            ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                                    If CDate(MaskEdBox4.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                            TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                                    End If
                                                                            End If
                                                                    End If
                                                            End If
                                                    Else    ' �������㹻��������
                                                            If Rs_Submit.Fields("FLAG_RETURN").Value = 1 Then   '   ����ա���駻����Թ
                                                                    If Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) = Lb_Year.Caption Then
                                                                            If DateComp > 0 Then     '�Թ 30 �ѹ�ҡ�Ѻ�駻����Թ�Թ���� 2% �����͹
                                                                                    TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month
                                                                                    Lb_DateOver.Caption = DateComp
                                                                            End If
                                                                    ElseIf Right(Rs_Submit.Fields("PBT5_DATE_RETURN").Value, 4) < Lb_Year.Caption Then
                                                                            If CDate(MaskEdBox4.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                                    TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime1.Text)) / 100) * Per_Month2
                                                                            End If
                                                                    End If
'                                                            End If
                                                            Else            '  ����ա���駻����Թ
                                                                    If CDate(MaskEdBox4.Text) > CDate("30/04/" & Right$(Lb_Year.Caption, 4)) Then ' ������ѧ��͹����¹���� 2% �����͹
                                                                            TaxInclude = TaxInclude + (Money * CSng(.Txt_Tax_OutTime1.Text)) / 100 * Per_Month2
                                                                    End If
                                                            End If
                                                    End If
                                            End With
                                    End If
                                            Lb_MoneyDue_Perform.Caption = FormatNumber(CSng(Lb_MoneyDue_Perform.Caption) + TaxInclude2, 2, vbTrue)
                                            Lb_MoneyDue.Caption = FormatNumber(CSng(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
                                            If ChkInstallment.Value = 1 Then        ' �����ʴ���һ�Ѻ�˹�Ҫ����繧Ǵ
                                                        Lb_MoneyDue_Perform1.Caption = Lb_MoneyDue_Perform.Caption
                                                        Lb_MoneyDue1.Caption = Lb_MoneyDue.Caption
                                            End If
            Case 3   '����    *********************************************************************************************************
                                Call SET_QUERY("SELECT DISTINCT PP1_DATE,PP1_DATE_RETURN,PP1_SET_GK,PP1_OWNER_DATE FROM PP1 WHERE PP1_STATUS=0 AND PP1_NO='" & Lb_PBT_NO.Caption & "' AND PP1_YEAR=" & Lb_Year.Caption, Rs_Submit)
                                With Frm_Index
                                        If Rs_Submit.Fields("PP1_SET_GK").Value = 0 Then  '     �������
                                                If Month(Rs_Submit.Fields("PP1_OWNER_DATE").Value) > 3 Then  '�Դ��駻������� �.� - .��.�. ����դ�һ�Ѻ
                                                        If DateDiff("d", Format$(Rs_Submit.Fields("PP1_OWNER_DATE").Value, "dd/mm/yyyy"), Format$(Rs_Submit.Fields("PP1_DATE").Value, "dd/mm/yyyy")) > 15 Then
                                                                TaxInclude2 = TaxInclude2 + (Money * CSng(.Txt_Tax_OutSchedule2.Text)) / 100
                                                        End If
                                                End If
                                                If DateComp < 0 Then DateComp = 0
                                                Lb_DateOver.Caption = DateComp
                                                If Not IsNull(Rs_Submit.Fields("PP1_DATE_RETURN").Value) Then     '   ����ա���駻����Թ
                                                        TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime2.Text)) / 100) * Per_Month
                                                End If
                                        Else
                                                If DateComp < 0 Then DateComp = 0
                                                Lb_DateOver.Caption = DateComp
                                                If Rs_Submit.Fields(0).Value > CDate("31/03/" & Lb_Year.Caption) Then   '   ���Ẻ���ѹ��˹�
                                                        TaxInclude2 = TaxInclude2 + (Money * CSng(.Txt_Tax_OutSchedule2.Text)) / 100
                                                End If
                                                '���Ẻ�ѹ��˹�
                                                If Not IsNull(Rs_Submit.Fields("PP1_DATE_RETURN").Value) Then  '   ����ա���駻����Թ
                                                        If DateComp > 0 Then  '�����Թ 30 �ѹ��ѧ���Ѻ㺵ͺ�Ѻ����駻����Թ
                                                                TaxInclude = TaxInclude + ((Money * CSng(.Txt_Tax_OutTime2.Text)) / 100) * Per_Month
                                                        End If
                                                End If
                                        End If
                                End With
                                Lb_MoneyDue_Perform.Caption = FormatNumber(CSng(Lb_MoneyDue_Perform.Caption) + TaxInclude2, 2, vbTrue)
                                Lb_MoneyDue.Caption = FormatNumber(CSng(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
             Case "2" '�ç���͹��з��Թ    ********************************************************************************************
                            If Trim$(Lb_PBT_NO_ACCEPT_OLD.Caption) <> Empty Then
                                    With Frm_Index
                                            Lb_DateOver.Caption = DateComp
                                            If DateComp > 0 Then
                                                  If DateComp > 0 And DateComp <= 30 Then
                                                          TaxInclude = (Money * CSng(.Txt_Due_1Month.Text)) / 100
                                                  End If
                                                  If DateComp > 30 And DateComp <= 60 Then
                                                          TaxInclude = (Money * CSng(.Txt_Due_2Month.Text)) / 100
                                                  End If
                                                  If DateComp > 60 And DateComp <= 90 Then
                                                          TaxInclude = (Money * CSng(.Txt_Due_3Month.Text)) / 100
                                                  End If
                                                  If DateComp > 90 Then
                                                          TaxInclude = (Money * CSng(.Txt_Due_4Month.Text)) / 100
                                                  End If
                                            End If
                                    End With
                            End If
                            Lb_MoneyDue.Caption = FormatNumber(CSng(Lb_MoneyDue.Caption) + TaxInclude, 2, vbTrue)
                            If ChkInstallment.Value = 1 And CCur(Lb_MoneyDue.Caption) > 0 Then
                                        Lb_MoneyDue1.Caption = Lb_MoneyDue.Caption
                            End If
End Select
CalculateTax = Money
Exit Function
ErrCal:
        MsgBox Err.Description
End Function

Private Sub SetFormulaReport()
 Dim BEGIN_DATE As String
 Dim LASTDATE As String
 Txt_Formula = Empty
 BEGIN_DATE = MaskEdBox4.Text
 LASTDATE = CStr(Date)

        Select Case LB_Menu.Tag
                    Case "1"
                            Txt_Formula = Txt_Formula & "({PBT5.PBT5_YEAR} = " & Lb_Year.Caption & " AND {PBT5.PBT5_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PBT5.PBT5_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                    Case "2"
                            Txt_Formula = Txt_Formula & "({PRD2.PRD2_YEAR} = " & Lb_Year.Caption & " AND {PRD2.PRD2_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PRD2.PRD2_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                    Case "3"
                            Txt_Formula = Txt_Formula & "({PP1.PP1_YEAR} = " & Lb_Year.Caption & " AND {PP1.PP1_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PP1.PP1_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "')"
                    Case "4"
                            Txt_Formula = Txt_Formula & "({PBA1.PBA1_YEAR} = " & Lb_Year.Caption & " AND {PBA1.PBA1_BOOK_NUMBER} = '" & Trim$(Txt_BOOK_NUMBER.Tag) & "' AND {PBA1.PBA1_BOOK_NO} = '" & Trim$(Txt_BOOK_NO.Tag) & "' AND {PBA1.CLASS_ID} = '" & Btn_Print_PBA1.Tag & "')"
        End Select
End Sub

Private Sub getNoAccept()
        Dim strSQL As String
        Dim i As Integer
        
        ComboNoAccept.Clear
'         Select Case LB_Menu.Tag
'                Case "1", "5"       ' ��ͧ���
'                                strSQL = "SELECT PBT5.PBT5_NO_ACCEPT From PBT5 WHERE (((PBT5.PBT5_NO_ACCEPT)<>'') AND ((PBT5.PBT5_YEAR)=" & CInt(Lb_Year.Caption) & ") And  ((PBT5.PBT5_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                " AND ((PBT5.OWNERSHIP_ID)='" & strOwnerShipID & "')) GRoup by PBT5.PBT5_NO_ACCEPT order by PBT5.PBT5_NO_ACCEPT"
'                Case "2", "6"       ' �ç���͹
'                                strSQL = "SELECT PRD2.PRD2_NO_ACCEPT From PRD2 WHERE (((PRD2.PRD2_NO_ACCEPT)<>'') AND ((PRD2.PRD2_YEAR)=" & CInt(Lb_Year.Caption) & ") And  ((PRD2.PRD2_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                " AND ((PRD2.OWNERSHIP_ID)='" & strOwnerShipID & "')) GRoup by PRD2.PRD2_NO_ACCEPT order by PRD2.PRD2_NO_ACCEPT"
'                Case "3", "7"       ' ����
'                                strSQL = "SELECT PP1.PP1_NO_ACCEPT From PP1 WHERE (((PP1.PP1_NO_ACCEPT)<>'') AND ((PP1.PP1_YEAR)=" & CInt(Lb_Year.Caption) & ") And  ((PP1.PP1_SET_GK)" & IIf(chkStatusType = 0, "<>", "=") & "2)" & _
'                                                " AND ((PP1.OWNERSHIP_ID)='" & strOwnerShipID & "')) GRoup by PP1.PP1_NO_ACCEPT order by PP1.PP1_NO_ACCEPT"
'        End Select
'
'        Call SET_QUERY(strSQL, Rs)
        Set rs = Globle_Connective.Execute("sp_get_NoAccept " & strOwnerShipID & "," & Lb_Year.Caption & ",'" & setGK & "', " & LB_Menu.Tag, adCmdUnknown)
    
        If rs.RecordCount > 0 Then
                i = 0
                Do While Not rs.EOF
                    If i = 0 Then
                            ComboNoAccept.Text = rs.Fields(0)
                            i = 1
                    End If
                     ComboNoAccept.AddItem rs.Fields(0)
                     
                rs.MoveNext
                Loop
        End If
        
        If ComboNoAccept.Text <> "" Then
                getPerformNo (CStr(ComboNoAccept.Text))
        End If
End Sub


Private Function Chk_IN_GK() As Byte
Dim DateNow As Byte, DateOwn As Byte

'Debug.Print "exec sp_check_in_gk '" & Trim$(strOwnerShipID) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'"
Set rs = Globle_Connective.Execute("exec sp_check_in_gk '" & Trim$(strOwnerShipID) & "'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", adCmdUnknown)
    If rs.RecordCount > 0 Then
            DateOwn = LooP_IN_GK(rs.Fields(1).Value)
            DateNow = LooP_IN_GK(CDate(MaskEdBox4.Text))
            If DateNow = DateOwn Then
                    Chk_IN_GK = 1
            Else
                    Chk_IN_GK = 2
            End If
    Else
            Chk_IN_GK = 1
    End If
'    Select Case LB_Menu.Tag
'                 Case "1", "5"
'                              Call SET_QUERY("SELECT PBT5_STATUS ,PBT5_PAY_DATE ,PBT5_IN_GK FROM PBT5 WHERE PBT5_STATUS = 1 AND PBT5_IN_GK = 1 AND PBT5_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "'", Rs)
'                              If Rs.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Rs.Fields("PBT5_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox4.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'                 Case "2", "6"
'                              Call SET_QUERY("SELECT PRD2_STATUS ,PRD2_PAY_DATE ,PRD2_IN_GK FROM PRD2 WHERE PRD2_STATUS = 1 AND PRD2_IN_GK = 1 AND PRD2_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "'", Rs)
'                              If Rs.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Rs.Fields("PRD2_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox4.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'                 Case "3", "7"
'                              Call SET_QUERY("SELECT PP1_STATUS ,PP1_PAY_DATE ,PP1_IN_GK FROM PP1 WHERE PP1_STATUS = 1 AND PP1_IN_GK = 1 AND PP1_YEAR =  " & Lb_Year.Caption & " AND OWNERSHIP_ID = '" & Trim$(strOwnerShipID) & "'", Rs)
'                              If Rs.RecordCount > 0 Then
'                                     DateOwn = LooP_IN_GK(Rs.Fields("PP1_PAY_DATE").Value)
'                                     DateNow = LooP_IN_GK(CDate(MaskEdBox4.Text))
'                                   If DateNow = DateOwn Then
'                                       Chk_IN_GK = 1
'                                   Else
'                                       Chk_IN_GK = 2
'                                   End If
'                              Else
'                                   Chk_IN_GK = 1
'                              End If
'    End Select
End Function

Private Function LooP_IN_GK(GK_DATE As Date) As Byte
            If Int(Month(GK_DATE)) = 10 Or Int(Month(GK_DATE)) = 11 Then
                LooP_IN_GK = 1
            End If
            If Int(Month(GK_DATE)) = 12 Or Int(Month(GK_DATE)) = 1 Then
                LooP_IN_GK = 2
            End If
            If Int(Month(GK_DATE)) = 2 Or Int(Month(GK_DATE)) = 3 Then
                LooP_IN_GK = 3
            End If
            If Int(Month(GK_DATE)) = 4 Or Int(Month(GK_DATE)) = 5 Then
                LooP_IN_GK = 4
            End If
            If Int(Month(GK_DATE)) = 6 Or Int(Month(GK_DATE)) = 7 Then
                LooP_IN_GK = 5
            End If
            If Int(Month(GK_DATE)) = 8 Or Int(Month(GK_DATE)) = 9 Then
                LooP_IN_GK = 6
            End If
End Function

Private Sub chkAmountSelList()
        Dim i As Integer
        Dim sumAmount As Currency
        Dim tempColID As Integer
        sumAmount = 0
'                For i = 0 To Grid_Result.Rows - 1
'
'                    If Trim$(Grid_Result.TextMatrix(i, Grid_Result.Cols - 1)) = "/" Then
                            If LB_Menu.Tag <> 1 And LB_Menu.Tag <> 3 Then
'                                    sumAmount = sumAmount + Grid_Result.TextMatrix(i, 5)
                                    tempColID = 11
                            Else
'                                    sumAmount = sumAmount + Grid_Result.TextMatrix(i, 6)
                                    tempColID = 12
                            End If
'                   End If
'                Next i
                sumAmount = CCur(Txt_RoundMoney.Text)
                If sumAmount >= 3000 Then
                        txtSummary.Text = Txt_Summary.Text
                        txtSumAmount.Text = Format$(sumAmount, "#,##0.00")
                        If Grid_Result.TextMatrix(Grid_Result.Row, tempColID) <> "" Then
                                ChkInstallment.Enabled = False
                        Else
                               ChkInstallment.Enabled = True
                        End If
                Else
                        ChkInstallment.Enabled = False
                        ChkInstallment.Value = 0
                End If
End Sub

Private Sub SetGridInstallment()
        With Grid_Installment
                    .TextArray(0) = "�Ǵ���": .ColWidth(0) = 750: .ColAlignmentFixed(0) = 4: .ColAlignment(0) = 5
                    .TextArray(1) = "���������ѹ���": .ColWidth(1) = 1500: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 7
                    .TextArray(2) = "�ѹ������": .ColWidth(2) = 1500: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                    .TextArray(3) = "��һ�Ѻ�Թ�Ǵ": .ColWidth(3) = 1500: .ColAlignmentFixed(3) = 4: .ColAlignment(3) = 7
                    .TextArray(4) = "�ʹ����ͧ����": .ColWidth(4) = 1500: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 7
                    .TextArray(5) = "�ӹǹ�Թ����": .ColWidth(5) = 1500: .ColAlignmentFixed(5) = 4: .ColAlignment(5) = 7
                    .TextArray(6) = "����": .ColWidth(6) = 750: .ColAlignmentFixed(6) = 4: .ColAlignment(6) = 5
            End With
End Sub

Private Sub NewPayInstallment()
        Dim strSQL As String
        Dim strYear As String
        Dim i As Integer
        Dim PBT5No As String
        Dim strINSTALLMENT_ID As String
        Dim InstallmentNo As Integer
        Dim chkVal As Boolean
        Dim fineMoney As Double
        Dim FineDiscount As Double
        Dim chkStatusPay As Integer
       
                                    PBT5No = ComboPayTax.Text  ' �Ţ��������Ẻ
                                    strINSTALLMENT_ID = LbInstallmentID.Caption
                                    InstallmentNo = CInt(LbInstallmentNo.Caption)
                                    chkVal = chkIDinstallmentTB(strINSTALLMENT_ID, InstallmentNo)       ' check ��Ң����������
                                    Dim RdMoney As Currency
                                    Dim tmpDiscountInstall As Currency      ' �Թ discount � table : installment
                                    
                                    If InstallmentNo = 1 Then  ' check ��ê�������繧Ǵ�˹  ����繧Ǵ�á�Ҩ���駢��������ѹ���ͪ���Ẻ�Ǵ
                                                If txtPayInstallment.Text <> "" And CCur(txtPayInstallment.Text) > 0 Then  ' ��Ǩ��Ҫ��ЧǴ�á��������ѹ�������������
                                                                If Not IsDate(MaskEdBox8.Text) Then
                                                                        MsgBox "�ô�к��ѹ������ !", vbExclamation, "�Ӫ��ᨧ"
                                                                        MaskEdBox8.SetFocus
                                                                        Exit Sub
                                                                End If
                                                                If txtPayInstallment.Text = "" Then
                                                                        MsgBox "�ô�кبӹǹ�Թ������ !", vbExclamation, "�Ӫ��ᨧ"
                                                                        txtPayInstallment.SetFocus
                                                                        Exit Sub
                                                                End If
                                                               If MsgBox("��ͧ��ê������� " & LB_Menu.Caption & "�繧Ǵ ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                                                                                    Exit Sub
                                                                End If
                                                                 ' add data to table Installment
                                                                 If LbFinePrice.Caption <> "" And CCur(LbFinePrice.Caption) > 0 Then
                                                                            fineMoney = CCur(LbFinePrice.Caption)
                                                                            chkStatusPay = 2
                                                                            RdMoney = 0
                                                                Else
                                                                            fineMoney = 0
                                                                            chkStatusPay = 1
                                                                            RdMoney = CCur(txtSumAmount.Text) - CCur(txtPayInstallment.Text)       ' ���շ����� - �ӹǹ�Թ����������ЧǴ
                                                                 End If
                                                                 
                                                                   If CCur(txtPayInstallment.Text) >= CCur(txtSumAmount.Text) Then
                                                                                 tmpDiscountInstall = CCur(txtPayInstallment.Text) - (CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption))
                                                                    Else
                                                                                tmpDiscountInstall = 0
                                                                    End If
                                                                              
                                                                                  If chkVal = True Then    ' New Data for Add installment
                                                                                           Dim chkStatusInstall As Integer
                                                                                                With Grid_Installment
                                                                                                           For i = 1 To .Rows - 1
                                                                                                                         If Trim(LbInstallmentNo.Caption) = .TextMatrix(i, 0) Then
                                                                                                                                          Call SET_Execute2("exec sp_Insert_Installment '" & strINSTALLMENT_ID & "'," & .TextMatrix(i, 0) & ",'" & CvDate3(.TextMatrix(i, 1)) & "','" & _
                                                                                                                                                    IIf(Len(Trim$(MaskEdBox8.Text)) = 0, Null, CvDate3(MaskEdBox8.Text)) & "','" & fineMoney & "','" & tmpDiscountInstall & "','" & .TextMatrix(i, 4) & "','" & _
                                                                                                                                                    txtPayInstallment.Text & "','" & RdMoney & "','1'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", 8, False)
                                                                                                                        Else
                                                                                                                                          Call SET_Execute2("exec sp_Insert_Installment '" & strINSTALLMENT_ID & "'," & .TextMatrix(i, 0) & ",'" & CvDate3(.TextMatrix(i, 1)) & "', NULL," & _
                                                                                                                                                    "'0','0','" & RdMoney / 2 & "','0','0','0'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", 8, False)
                                                                                                                        End If
                                                                                                             Next i
                                                                                                End With
                                                                                    Else
                                                                                                With Grid_Installment
                                                                                                
                                                                                                                   For i = 1 To .Rows - 1
                                                                                                                               If Trim(LbInstallmentNo.Caption) = .TextMatrix(i, 0) And .TextMatrix(i, .Cols - 1) <> "/" Then
                                                                                                                                                Set rs = Globle_Connective.Execute("exec sp_update_Installment '" & CvDate3(MaskEdBox8.Text) & "','" & fineMoney & "','" & tmpDiscountInstall & "' ,'" & .TextMatrix(i, 4) & "' ,'" & CCur(txtPayInstallment.Text) & _
                                                                                                                                                 "', '" & RdMoney & "'," & chkStatusPay & ",'" & LbInstallmentID.Caption & "', " & CInt(LbInstallmentNo.Caption) & "," & LbYearSelect.Caption & ",'" & LB_Menu.Tag & "'", adCmdUnknown)

                                                                                                                                ElseIf Trim(LbInstallmentNo.Caption) <> .TextMatrix(i, 0) And .TextMatrix(i, .Cols - 1) <> "/" Then
                                                                                                                                                Set rs = Globle_Connective.Execute("exec sp_update_Installment  Null,0,0 ," & RdMoney / 2 & " ,0 ,0 ,0 ,'" & _
                                                                                                                                                                LbInstallmentID.Caption & "', " & i & "," & LbYearSelect.Caption & ",'" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                                                End If
                                                                                                                     Next i
                                                                                                        End With
                                                                                        End If
                                                                                        
                                                                                         If chkStatusPay = 2 Then      '���� Delete Record ��� �Թ�Ǵ
                                                                                                        For i = CInt(LbInstallmentNo.Caption) + 1 To 3
'                                                                                                                    Debug.Print "exec sp_del_NewInstallment " & LbYearSelect.Caption & ", '" & LbInstallmentID.Caption & "', '" & i & "', '" & LB_Menu.Tag & "'"
                                                                                                                    Set rs = Globle_Connective.Execute("exec sp_del_NewInstallment " & LbYearSelect.Caption & ", '" & LbInstallmentID.Caption & "', '" & i & "', '" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                        Next i
                                                                                        End If
                                                                                       
                                                Else
                                                        If MsgBox("��ͧ����駪����繧Ǵ��ҹ�� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbYes Then
                                                                 With Grid_Installment
                                                                           For i = 1 To .Rows - 1
'                                                                                    Debug.Print "exec sp_Insert_Installment '" & strINSTALLMENT_ID & "'," & .TextMatrix(i, 0) & ",'" & CvDate3(.TextMatrix(i, 1)) & "',NULL" & _
'                                                                                                      ",'0','0','" & CCur(.TextMatrix(i, 4)) & "','0','0','0'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'"
                                                                                    Call SET_Execute2("exec sp_Insert_Installment '" & strINSTALLMENT_ID & "'," & .TextMatrix(i, 0) & ",'" & CvDate3(.TextMatrix(i, 1)) & "',NULL" & _
                                                                                                      ",'0','0','" & CCur(.TextMatrix(i, 4)) & "','0','0','0'," & CInt(Lb_Year.Caption) & ",'" & LB_Menu.Tag & "'", 8, False)
                                                                             Next i
                                                                        End With
                                                        Else
                                                                Exit Sub
                                                        End If
                                                End If
                                                
                                                                    
                                                If txtBookNumber.Text <> "" And txtBookNo.Text <> "" And (CCur(txtPayInstallment.Text) >= CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption)) Then
                                                             With Grid_Result
                                                                         For i = 0 To .Rows - 1
                                                                                    If .TextMatrix(i, .Cols - 1) = "/" Then
                                                                                                 fineMoney = CCur(Lb_MoneyDue_Perform1.Caption) + CCur(LbFinePrice.Caption)   '   ��һ�Ѻ���
                                                                                                 tmpDiscountInstall = tmpDiscountInstall + CCur(Lb_Discount1.Caption)
                                                                                                 
                                                                                                 Set rs = Globle_Connective.Execute("exec sp_update_AfterPayInstallment '" & Trim$(txtBookNumber.Text) & "','" & Trim$(txtBookNo.Text) & "','" & CCur(fineMoney) & "','" & CCur(tmpDiscountInstall) & "','" & strOwnerShipID & "','" & PBT5No & _
                                                                                                         "','" & .TextMatrix(i, 2) & "','" & LbYearSelect.Caption & "','" & setGK & "','" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                         
                                                                                                 fineMoney = 0
                                                                                                 tmpDiscountInstall = 0
                                                                                                 Lb_MoneyDue_Perform1.Caption = 0
                                                                                                 Lb_MoneyDue1.Caption = 0
                                                                                                 LbFinePrice.Caption = 0
                                                                                                 Lb_Discount1.Caption = 0
                                                                                     End If
                                                                                     
                                                                            Next i
                                                                 End With
                                                                 
                                                                 Call SetGridPayment
                                                                 FramePayInsallment.Visible = False
                                                                 FramePayNormal.Visible = True
                                                                  If txtBookNumber.Text <> Empty Then Txt_BOOK_NUMBER.Tag = txtBookNumber.Text
                                                                  If txtBookNo.Text <> Empty Then Txt_BOOK_NO.Tag = txtBookNo.Text
                                                                 Label3(4).Caption = "�Թ����/Ŵ :"
                                                                 Lb_PBT_NO.Caption = Empty
                                                                Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
                                                                Call clrTextFrmPayNormal
                                                                Call Clear_Grid
                                                                Call SetGridPayment
                                                                Call SetGridInstallment
                                                                 
                                                                 Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
                                                                 Call ChkStatusNum_Tax(strOwnerShipID, Lb_Year.Caption)
                                                Else
                                                                 '  ੾�Т����� ����駢��������ͧ������ѹ���ͪ����繧Ǵ
                                                                    With Grid_Result
                                                                        Dim j As Integer        ' s
                                                                        If LB_Menu.Tag = "1" Or LB_Menu.Tag = "5" Then
                                                                                j = 11
                                                                        Else
                                                                                j = 10
                                                                        End If
                                                                                   For i = 0 To .Rows - 1
                                                                                              If .TextMatrix(i, .Cols - 1) = "/" Then
                                                                                                           ' **************************** update data installment_id  to pbt5 ,prd2,pp1 **************************************
                                                                                                            Call SET_Execute2("exec sp_update_NewInstallmentID '" & strINSTALLMENT_ID & "','" & CvDate3(MaskEdBox7.Text) & "'," & CCur(.TextMatrix(i, j)) & "," & _
                                                                                                            CCur(Lb_MoneyDue1.Caption) + CCur(Lb_MoneyDue_Perform1.Caption) & ",'" & CvDate3(MaskEdBox7.Text) & "'," & IIf(CCur(Lb_Discount1.Caption) > 0, CCur(Lb_Discount1.Caption), 0) & ",'" & _
                                                                                                             strOwnerShipID & "','" & PBT5No & "','" & .TextMatrix(i, 2) & "'," & LbYearSelect.Caption & ",'" & Chk_IN_GK & "','" & LB_Menu.Tag & "'", 8, False)
                                                                                                           
                                                                                                            Lb_MoneyDue1.Caption = "0.00":     Lb_Discount1.Caption = "0.00": Lb_MoneyDue_Perform1.Caption = "0.00"
                                                                                                  End If
                                                                                       Next i
                                                                    End With
                                                 End If
                                             Call getPBT5NoPayTax
                                            Call DisplayPayment(strYear, 2)
                                            ChkInstallment.Value = 0
                                             ChkInstallment.Enabled = False
                                            MsgBox "�Ѵ����¡�����º���� !! ", vbInformation, "�׹�ѹ��èѴ����¡��"
                                            Exit Sub
                        ElseIf InstallmentNo > 1 Then
                                                        If Not IsDate(MaskEdBox8.Text) Then
                                                                    MsgBox "�ô�к��ѹ������ !", vbExclamation, "�Ӫ��ᨧ"
                                                                    MaskEdBox8.SetFocus
                                                                    Exit Sub
                                                            End If
                                                            If txtPayInstallment.Text = "" Then
                                                                    MsgBox "�ô�кبӹǹ�Թ������ !", vbExclamation, "�Ӫ��ᨧ"
                                                                    txtPayInstallment.SetFocus
                                                                    Exit Sub
                                                            End If
                                                            If txtBookNo.Text <> "" And txtBookNumber.Text <> "" Then
'                                                                    Debug.Print "exec sp_check_payment_book_no '" & Trim$(txtBookNumber.Text) & "','" & Trim$(txtBookNo.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
'                                                                            LB_Menu.Tag & "'"
                                                                    Set rs = Globle_Connective.Execute("exec sp_check_payment_book_no '" & Trim$(txtBookNumber.Text) & "','" & Trim$(txtBookNo.Text) & "'," & CInt(Lb_Year.Caption) & ",'" & _
                                                                            LB_Menu.Tag & "'", , adCmdUnknown)
                                                                            
                                                                     If rs.RecordCount > 0 Then
                                                                            txtBookNo.Text = Empty
                                                                            txtBookNumber.Text = Empty
                                                                            txtBookNumber.SetFocus
                                                                            MsgBox "�������������������ӡѺ㹰ҹ������ ��س����������������Ţ���������� !!!", vbCritical, "��ͼԴ��Ҵ"
                                                                            'Globle_Connective.RollbackTrans
                                                                            Exit Sub
                                                                     End If
                                                            End If
                                                            If MsgBox("��ͧ��ê������� " & LB_Menu.Caption & "�繧Ǵ ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ��èѴ����¡��") = vbNo Then
                                                                        Exit Sub
                                                            End If
                                                                  
                                                            If chkVal = False Then
                                                                         If CCur(LbFinePrice.Caption) > 0 Then
                                                                                     fineMoney = CCur(LbFinePrice.Caption)
                                                                                     chkStatusPay = 2   ' ���·����� �����Թ�Ǵ
                                                                         Else
                                                                                     fineMoney = 0
                                                                                     chkStatusPay = 1   ' ���µ���Ǵ����
                                                                          End If
                                                                          Dim tempPayPrice As Double
                                                                           
                                                                          tempPayPrice = 0
                                                                                  With Grid_Installment
                                                                                            For i = 1 To .Rows - 1
                                                                                                        If CInt(Trim(LbInstallmentNo.Caption)) > CInt(.TextMatrix(i, 0)) And .TextMatrix(i, .Cols - 1) = "/" Then
                                                                                                                        tempPayPrice = tempPayPrice + CCur(.TextMatrix(i, 5))
                                                                                                                        RdMoney = CCur(txtSumAmount.Text) - (tempPayPrice + CCur(txtPayInstallment.Text))      ' ���շ����� - �ӹǹ�Թ����������ЧǴ
                                                                                                        ElseIf Trim(LbInstallmentNo.Caption) = .TextMatrix(i, 0) And .TextMatrix(i, .Cols - 1) <> "/" Then
                                                                                       
                                                                                                                        If CCur(txtPayInstallment.Text) >= tempArrearPrice Then
                                                                                                                                    RdMoney = 0
                                                                                                                                    FineDiscount = CCur(txtPayInstallment.Text) - (tempArrearPrice + fineMoney)     ' discount in table installment
                                                                                                                        End If
                                                                                                                        
                                                                                                                            Set rs = Globle_Connective.Execute("exec sp_update_Installment '" & CvDate3(MaskEdBox8.Text) & "'," & fineMoney & "," & FineDiscount & " ," & CCur(.TextMatrix(i, 4)) & " ," & CCur(txtPayInstallment.Text) & _
                                                                                                                             ", " & RdMoney & "," & chkStatusPay & ",'" & LbInstallmentID.Caption & "', " & CInt(LbInstallmentNo.Caption) & "," & LbYearSelect.Caption & ",'" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                                             

                                                                                                         ElseIf Trim(LbInstallmentNo.Caption) <> .TextMatrix(i, 0) And .TextMatrix(i, .Cols - 1) <> "/" Then
                                                                                                                            Set rs = Globle_Connective.Execute("exec sp_update_Installment NULL,0,0 ," & RdMoney & ",0 , 0,0 ,'" & LbInstallmentID.Caption & "', " & i & "," & LbYearSelect.Caption & ",'" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                         End If
                                                                                              Next i
                                                                                              If chkStatusPay = 2 Then      '���� Delete Record ��� �Թ�Ǵ
                                                                                                        For i = CInt(LbInstallmentNo.Caption) + 1 To 3
                                                                                                                    Set rs = Globle_Connective.Execute("exec sp_del_NewInstallment " & LbYearSelect.Caption & ", '" & LbInstallmentID.Caption & "', '" & i & "', '" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                        Next i
                                                                                              End If
                                                                                 End With
                                                             End If
                                                             
                                                              If txtBookNumber.Text <> "" And txtBookNo.Text <> "" And (CCur(txtPayInstallment.Text) >= CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption)) Then
                                                              
                                                                                ' update �����ŷ������������ǡѺ��ê���  �������Ѻ status ��ê���
                                                                                With Grid_Result
                                                                                            For i = 0 To .Rows - 1
                                                                                                       If .TextMatrix(i, .Cols - 1) = "/" Then
                                                                                                                    fineMoney = CCur(fineMoney) + CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption)   '   ��һ�Ѻ���
                                                                                                                    FineDiscount = CCur(FineDiscount) + CCur(Lb_Discount1.Caption)
                                                                                                                    Set rs = Globle_Connective.Execute("exec sp_update_AfterPayInstallment '" & Trim$(txtBookNumber.Text) & "','" & Trim$(txtBookNo.Text) & "','" & CCur(fineMoney) & "','" & CCur(FineDiscount) & "','" & strOwnerShipID & "','" & PBT5No & _
                                                                                                                            "','" & .TextMatrix(i, 2) & "','" & LbYearSelect.Caption & "','" & setGK & "','" & LB_Menu.Tag & "'", adCmdUnknown)
                                                                                                                            
                                                                                                                    FineDiscount = 0
                                                                                                                    fineMoney = 0
                                                                                                                    Lb_MoneyDue_Perform1.Caption = 0
                                                                                                                    Lb_MoneyDue1.Caption = 0
                                                                                                        End If
                                                                                                        
                                                                                               Next i
                                                                                    End With
                                                                                    
                                                                                    Call SetGridPayment
                                                                                    FramePayInsallment.Visible = False
                                                                                    FramePayNormal.Visible = True
                                                                                     If txtBookNumber.Text <> Empty Then Txt_BOOK_NUMBER.Tag = txtBookNumber.Text
                                                                                     If txtBookNo.Text <> Empty Then Txt_BOOK_NO.Tag = txtBookNo.Text
                                                                                     Label3(4).Caption = "�Թ����/Ŵ :"
                                                                                     Lb_PBT_NO.Caption = Empty
                                                                                     Lb_PBT_NO_ACCEPT_OLD.Caption = Empty
                                                                                     Call clrTextFrmPayNormal
                                                                                     Call Clear_Grid
                                                                                     Call SetGridPayment
                                                                                     Call SetGridInstallment
                                                                                     Call chkStatusNum(strOwnerShipID, Lb_Year.Caption)
                                                                                     Call ChkStatusNum_Tax(strOwnerShipID, Lb_Year.Caption)
                                                                End If
                                     ChkInstallment.Value = 0
                                    ChkInstallment.Enabled = False
                                    Call getPBT5NoPayTax
                                    Call DisplayPayment(strYear, 2)
                                    MsgBox "�������էǴ��� " & LbInstallmentNo.Caption & " ���º�������� !"
                                    Exit Sub
                        End If
'                        Call DisplayInsatllment
'                        Call getInstallmentNO
'                        Call GetMainDataInstallment
End Sub

Function getArrearPrice(InstallmentNo As Integer) As Double
        Dim strSQL As String
        Dim ArrearPrice As Double
    
'        Select Case LB_Menu.Tag
'                Case "1", "5"       ' ��ͧ���
'                                        strSQL = "SELECT PBT5_INSTALLMENT.PBT5_ARREAR_PRICE as ARREAR_PRICE  From PBT5_INSTALLMENT" & _
'                                                        " WHERE (((PBT5_INSTALLMENT.PBT5_INSTALLMENT_ID)='" & Trim(LbInstallmentID.Caption) & "') AND ((PBT5_INSTALLMENT.PBT5_INSTALLMENT_NO)=" & InstallmentNo - 1 & "))"
'                Case "2", "6"       ' �ç���͹
'                                strSQL = "SELECT PRD2_INSTALLMENT.PRD2_ARREAR_PRICE as ARREAR_PRICE From PRD2_INSTALLMENT" & _
'                                                " WHERE (((PRD2_INSTALLMENT.PRD2_INSTALLMENT_ID)='" & Trim(LbInstallmentID.Caption) & "') AND ((PRD2_INSTALLMENT.PRD2_INSTALLMENT_NO)=" & CInt(LbInstallmentNo.Caption) - 1 & "))"
'                Case "3", "7"       ' ����
'                               strSQL = "SELECT PP1_INSTALLMENT.PP1_ARREAR_PRICE as ARREAR_PRICE From PP1_INSTALLMENT" & _
'                                                " WHERE (((PP1_INSTALLMENT.PP1_INSTALLMENT_ID)='" & Trim(LbInstallmentID.Caption) & "') AND ((PP1_INSTALLMENT.PP1_INSTALLMENT_NO)=" & CInt(LbInstallmentNo.Caption) - 1 & "))"
'        End Select
    Set rs = Globle_Connective.Execute("exec sp_get_ArrearPrice " & Lb_Year.Caption & ",'" & Trim(LbInstallmentID.Caption) & "'," & CInt(LbInstallmentNo.Caption) - 1 & ", '" & LB_Menu.Tag & "'", adCmdUnknown)
    
    If rs.RecordCount > 0 Then
            ArrearPrice = rs("ARREAR_PRICE")
    End If
            getArrearPrice = ArrearPrice
        
End Function

Function getPayDate() As String
        Dim strSQL As String
        Dim strYear As String
        Dim strPBTNo As String
    
        strYear = LbYearSelect.Caption
        strPBTNo = CStr(ComboPayTax.Text)
        
        Set rs = Globle_Connective.Execute("exec sp_get_NoticeDate '" & strOwnerShipID & "'," & strYear & ",'" & strPBTNo & "','" & Trim(Grid_Result.TextMatrix(Grid_Result.RowSel, 2)) & "','" & LB_Menu.Tag & "'", adCmdUnknown)
        
        If rs.RecordCount > 0 Then
                getPayDate = rs("NOTICE_DATE")
        End If
End Function

Function chkIDinstallmentTB(InstallmentID As String, InstallmentNo As Integer) As Boolean
        Dim strSQL As String
        Dim chkValue As Boolean
        
'        Select Case LB_Menu.Tag
'                        Case "1", "5"
'                                    strSQL = "Select PBT5_INSTALLMENT_ID From PBT5_INSTALLMENT where PBT5_INSTALLMENT_ID = '" & InstallmentID & "' and PBT5_INSTALLMENT_NO=" & InstallmentNo
'                        Case "2", "6"
'                                    strSQL = "Select PRD2_INSTALLMENT_ID From PRD2_INSTALLMENT where PRD2_INSTALLMENT_ID = '" & InstallmentID & "' and PRD2_INSTALLMENT_NO=" & InstallmentNo
'                        Case "3", "7"
'                                    strSQL = "Select PP1_INSTALLMENT_ID From PP1_INSTALLMENT where PP1_INSTALLMENT_ID = '" & InstallmentID & "' and PP1_INSTALLMENT_NO=" & InstallmentNo
'        End Select
'
'        Call SET_QUERY(strSQL, Rs)

        Set rs = Globle_Connective.Execute("exec sp_check_InstallmentID " & Lb_Year.Caption & ",'" & InstallmentID & "'," & InstallmentNo & ", '" & LB_Menu.Tag & "'", adCmdUnknown)
         
         If rs.RecordCount > 0 Then
                    chkValue = False        ' �բ��������� �������ö add new data ��
         Else
                    chkValue = True         ' ����ö add new data ��
         End If
         
         chkIDinstallmentTB = chkValue
End Function

Private Sub chkFineMoneyInstallment()
        Dim numDateDiff As Long
        Dim fineMoney As Currency
        Dim i, j As Integer
        If MaskEdBox8.Text = "__/__/____" Then Exit Sub
            ' ��ǹ��ҧ�ҡ�ѹ��˹����� �������Թ��˹���ҨеԴź ����Թ��˹��ѹ���Ф�Ҩ��繺ǡ
          If LbInstallmentNo.Caption <> "" Then
                    numDateDiff = DateDiff("d", Format$((Grid_Installment.TextMatrix(CInt(LbInstallmentNo.Caption), 1)), "dd/mm/yyyy"), CDate(MaskEdBox8.Text))
            Else
                    MsgBox "��辺�Ǵ����ͧ����", vbInformation, "�Ӫ��ᨧ"
            End If
            
            Select Case LB_Menu.Tag
                            Case 1, 3 '���ا��ͧ��� ,����
                                    If numDateDiff > 0 And (numDateDiff \ 30) > 0 Then  ' �Թ���� 30 �ѹ����˹���� �դ�һ�Ѻ�Թ�Ǵ
                                            fineMoney = (CCur(LbArrear.Caption) * (2 * (numDateDiff \ 30))) / 100
                                    ElseIf numDateDiff > 0 And (numDateDiff \ 30) = 0 Then
                                            fineMoney = (CCur(LbArrear.Caption) * (2)) / 100
                                    End If
                            Case 2  '  �ç���͹��з��Թ
                                    If numDateDiff > 0 And ((numDateDiff \ 30) > 0 And (numDateDiff \ 30) <= 4) Then      ' �Թ���� 30 �ѹ���ҧ����Թ  4 �Ǵ����˹���� �դ�һ�Ѻ�Թ�Ǵ
                                            fineMoney = (CCur(LbArrear.Caption) * (2.5 * (numDateDiff \ 30))) / 100
                                    ElseIf numDateDiff > 0 And (numDateDiff \ 30) > 4 Then      ' �Թ  4 �Ǵ �դ�һ�Ѻ�Թ�Ǵ
                                            fineMoney = (CCur(LbArrear.Caption) * (10)) / 100
                                    ElseIf numDateDiff > 0 And (numDateDiff \ 30) = 0 Then
                                             fineMoney = (CCur(LbArrear.Caption) * (2.5)) / 100
                                    End If
            End Select
            If fineMoney > 0 Then
                    
                    LbFinePrice.Caption = Format$(fineMoney, "#,##0.00")
                    txtPayInstallment.Text = Format$(CCur(LbArrear.Caption) + CCur(LbFinePrice.Caption), "#,##0.00")
        
                    With Grid_Installment
                    .TextMatrix(CInt(LbInstallmentNo.Caption), 3) = Format$(fineMoney, "#,##0.00")
                    
                            If LbInstallmentNo.Caption <> "" And LbFinePrice.Caption <> "" Then
                                        For i = CInt(LbInstallmentNo.Caption) + 1 To .Rows - 1
                                                            For j = 1 To .Cols - 1
                                                                        .TextMatrix(i, j) = "-"
                                                            Next j
                                        Next i
                            End If
                    End With
            Else
                    LbFinePrice.Caption = "0.00"
            End If
End Sub

Private Sub getInstallmentNO()
    Dim i As Integer
    Dim j As Integer
    Dim tempPrice As Double
        If MaskEdBox8.Text = "__/__/____" Then Exit Sub
                With Grid_Installment
                        For i = 1 To .Rows - 1
                                    If .TextMatrix(i, .Cols - 1) <> "/" And CDate(.TextMatrix(i, 1)) >= CDate(MaskEdBox8.Text) Then      ' �ѧ����㹡�˹����ҷ���ͧ����
                                                          If StatusInstallment = True Then
                                                                    LbInstallmentNo.Caption = i
                                                                    LbFinePrice.Caption = Format$(.TextMatrix(i, 3), "#,##0.00")
                                                                    txtPayInstallment.Text = Format$(CCur(LbFinePrice.Caption) + CCur(LbArrear.Caption), "#,##0.00")
                                                                    LbArrear.Caption = Format$(.TextMatrix(i, 4), "#,##0.00")
                                                                    
                                                                            For j = 0 To .Cols - 1
                                                                                   .Row = i
                                                                                   .Col = j
                                                                                   .CellBackColor = &HC0C0FF    '  �ժ���
                                                                           Next j
                                                                           Chk_RoundMoney2.Value = 0
                                                                            
                                                                            If CCur(LbArrear.Caption) - CInt(LbArrear.Caption) <> 0 Then
                                                                                        Chk_RoundMoney2.Enabled = True
                                                                                        Combo3.Enabled = True
                                                                            Else
                                                                                        Chk_RoundMoney2.Enabled = False
                                                                                        Combo3.Enabled = False
                                                                            End If
                                                                Else
                                                                        Call SetPayInstallment
                                                                End If
                                                    Exit Sub
                                    ElseIf (.TextMatrix(i, .Cols - 1) <> "/" And .TextMatrix(i, .Cols - 1) <> "-") And CDate(MaskEdBox8.Text) > CDate(.TextMatrix(i, 1)) Then     ' �Թ��˹����ҷ���ͧ����
                                                    If .TextMatrix(i, .Cols - 1) <> "/" And CDate(MaskEdBox8.Text) > CDate(.TextMatrix(i, 1)) Then
                                                                LbInstallmentNo.Caption = i
                                                                If i = 1 Then       ' �����Թ�����Ǵ�á
                                                                        tempPrice = Format$(txtSumAmount.Text, "#,##0.00")
                                                                ElseIf i > 1 Then
                                                                        tempPrice = getArrearPrice(i)
                                                                End If
                                                                 .TextMatrix(i, 4) = Format$(tempPrice, "#,##0.00")
                                                                 LbArrear.Caption = Format$(tempPrice, "#,##0.00")
                                                                Exit For
                                                    End If
                                    End If
                        Next i
                End With

End Sub

Private Sub chkInstallmentID()
    Dim strYear As String
    Dim strSQL As String
    strYear = LbYearSelect.Caption
'    Select Case LB_Menu.Tag
'                Case 1, 5
'                            strSQL = "SELECT max((left(PBT5_INSTALLMENT_ID,5))) as maxID From PBT5 WHERE (((PBT5.PBT5_YEAR)=" & strYear & ")" & _
'                                   " AND ((PBT5.PBT5_INSTALLMENT_ID)<>''))"
'                Case 2, 6
'                            strSQL = "SELECT max((left(PRD2_INSTALLMENT_ID,5))) as maxID From PRD2 WHERE (((PRD2.PRD2_YEAR)=" & strYear & ")" & _
'                                   " AND ((PRD2.PRD2_INSTALLMENT_ID)<>''))"
'                Case 3, 7
'                            strSQL = "SELECT max((left(PP1_INSTALLMENT_ID,5))) as maxID From PP1 WHERE (((PP1.PP1_YEAR)=" & strYear & ")" & _
'                                   " AND ((PP1.PP1_INSTALLMENT_ID)<>''))"
'    End Select
'    Call SET_QUERY(strSQL, Rs)
    Set rs = Globle_Connective.Execute("exec sp_get_InstallmentID " & strYear & ",'" & LB_Menu.Tag & "'", adCmdUnknown)

    strYear = Right(strYear, 2)
    If rs("maxID") <> "" Then
            LbInstallmentID.Caption = Format(CInt(rs("maxID")) + 1, "00000") & "/" & strYear
    Else
            LbInstallmentID.Caption = "00001/" & strYear
    End If
    LbInstallmentNo.Caption = "1"
    
End Sub

Private Sub GetMainDataInstallment()
    Dim strSQL As String
    Dim strYear As String
    Dim strPBTNo As String
    
    strYear = LbYearSelect.Caption
    strPBTNo = CStr(ComboPayTax.Text)
    
    Set rs = Globle_Connective.Execute("exec sp_get_MainInstallment  '" & strOwnerShipID & "','" & LbInstallmentID.Caption & "'," & strYear & ",'" & strPBTNo & "','" & LB_Menu.Tag & "'", adCmdUnknown)
    
    If rs.RecordCount > 0 And IsNull(rs("DISCOUNT")) = False Then
               Lb_Discount1.Caption = Format$(rs("DISCOUNT"), "#,##0.00")
'                'Lb_Discount1
'                If CCur(Rs("DISCOUNT")) > CCur(Lb_MoneyDue_Perform1.Caption) + CCur(Lb_MoneyDue1.Caption) Then
'                        Chk_RoundMoney1.Value = 1
'                Else
'                        Chk_RoundMoney1.Value = 0
'                End If
'                Chk_RoundMoney1.Enabled = False
'                Combo2.Enabled = False
    End If
End Sub

Private Sub DisplayInsatllment()
        Dim strSQL As String
        Dim strInstallmentID As String
        Dim i As Integer

        strInstallmentID = Grid_Result.TextMatrix(Grid_Result.Row, Grid_Result.Cols - 3)

    Set rs = Globle_Connective.Execute("exec sp_Display_Installment  '" & strInstallmentID & "'," & LbYearSelect.Caption & ",'" & LB_Menu.Tag & "'", adCmdUnknown)

        Set Grid_Installment.DataSource = rs
        Dim j As Integer
        
        j = 0
        For i = 1 To Grid_Installment.Rows - 1
                If Grid_Installment.TextMatrix(i, Grid_Installment.Cols - 1) = "1" Then
                        Grid_Installment.TextMatrix(i, Grid_Installment.Cols - 1) = "/"
                        j = j + 1
                Else
                        Grid_Installment.TextMatrix(i, Grid_Installment.Cols - 1) = ""
                End If
                
                Grid_Installment.TextMatrix(i, 1) = ConvertDate2(Grid_Installment.TextMatrix(i, 1))
                If Grid_Installment.TextMatrix(i, 2) <> "" Then
                            Grid_Installment.TextMatrix(i, 2) = ConvertDate2(Grid_Installment.TextMatrix(i, 2))
                End If
        Next i
        
        If j = 0 Then
                LbInstallmentNo.Caption = 1
        ElseIf j > 0 Then
                LbInstallmentNo.Caption = j + 1
        End If
        
        
        Call SetGridInstallment
End Sub

Private Sub SetPayInstallment()
        Dim DateTemp As Date
        Dim d, m, y, i As Integer
        Dim CurDate As Date
        Dim numPayPrice As Double
        
        CurDate = CDate(MaskEdBox7.Text)
        
        With Grid_Installment
                If tempArrearPrice > 0 And LbInstallmentNo.Caption <> "1" Then
                            LbArrear.Caption = Format$(tempArrearPrice, "#,##0.00")
                Else
                            numPayPrice = getPayPrice      ' ੾�Ф����á������͡���л繧Ǵ
                            LbArrear.Caption = Format$(numPayPrice, "#,##0.00")
                End If
                .Rows = 4
                For i = 1 To .Rows - 1
                            Select Case LB_Menu.Tag
                                        Case 1, 2
                                                    If i = 1 Then
                                                            .TextMatrix(i, 1) = DateAdd("d", 30, CurDate)
                                                            DateTemp = DateAdd("d", 30, CurDate)
                                                    ElseIf i > 1 Then
                                                             .TextMatrix(i, 1) = DateAdd("d", 30, DateTemp)
                                                             DateTemp = DateAdd("d", 30, DateTemp)
                                                    End If
                                        Case 3
                                                    If i = 1 Then
                                                            .TextMatrix(i, 1) = DateAdd("d", 15, CurDate)
                                                            DateTemp = DateAdd("d", 15, CurDate)
                                                    ElseIf i > 1 Then
                                                             .TextMatrix(i, 1) = DateAdd("d", 30, DateTemp)
                                                             DateTemp = DateAdd("d", 30, DateTemp)
                                                    End If
                                        End Select
                         .TextMatrix(i, 0) = i
                         .TextMatrix(i, 3) = "0.00"     ' ��¹ check ����դ�һ�Ѻ����
                        If .TextMatrix(i, 5) <> "/" Then
                                .TextMatrix(i, 4) = Format$(numPayPrice, "#,##0.00")
                        End If
                Next i
                 
        End With
End Sub

Function getPayPrice() As Double
        Dim numPrice As Double
        Dim numInstallmentNo As Integer
        Dim i, chkStatus As Integer
        
        With Grid_Installment
                If txtSumAmount.Text <> "" Then
                    numPrice = CCur(txtSumAmount.Text) / 3  'chkStatus
                    getPayPrice = numPrice
                Else
                    getPayPrice = 0
                End If
         End With
End Function

