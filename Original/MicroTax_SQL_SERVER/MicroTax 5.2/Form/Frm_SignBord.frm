VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_SignBord 
   ClientHeight    =   9210
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12945
   ControlBox      =   0   'False
   Icon            =   "Frm_SignBord.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   Picture         =   "Frm_SignBord.frx":151A
   ScaleHeight     =   9210
   ScaleWidth      =   12945
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Cmb_Building_ID 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1260
      TabIndex        =   81
      Top             =   1950
      Width           =   1695
   End
   Begin VB.CommandButton cmd_Chk_ID 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   6060
      Picture         =   "Frm_SignBord.frx":19325
      Style           =   1  'Graphical
      TabIndex        =   80
      Top             =   2490
      Width           =   345
   End
   Begin VB.TextBox Txt_SIGNBORD_ID2 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2940
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   78
      Top             =   1470
      Width           =   435
   End
   Begin VB.ComboBox cb_Signboard_Change_ID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   450
      Style           =   2  'Dropdown List
      TabIndex        =   77
      Top             =   390
      Visible         =   0   'False
      Width           =   3165
   End
   Begin VB.ComboBox cb_Signboard_Change 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4170
      Style           =   2  'Dropdown List
      TabIndex        =   76
      Top             =   900
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.TextBox Txt_Buffer 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   8940
      TabIndex        =   75
      Top             =   6150
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.CheckBox Chk_Flag_PayTax0 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   9990
      TabIndex        =   70
      Top             =   3510
      Width           =   195
   End
   Begin VB.CheckBox Chk_Flag_PayTax1 
      BackColor       =   &H00F1F1F1&
      Enabled         =   0   'False
      Height          =   195
      Left            =   8580
      TabIndex        =   69
      Top             =   3510
      Value           =   1  'Checked
      Width           =   195
   End
   Begin VB.TextBox Txt_StoryRemark 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3900
      TabIndex        =   67
      Text            =   "-"
      Top             =   60
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.CommandButton btn_add_SignBord 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   345
      Left            =   8550
      Picture         =   "Frm_SignBord.frx":1B01F
      Style           =   1  'Graphical
      TabIndex        =   65
      Top             =   6150
      Width           =   375
   End
   Begin VB.PictureBox picParent 
      BackColor       =   &H006E6B68&
      Height          =   2145
      Left            =   10560
      ScaleHeight     =   2085
      ScaleWidth      =   2235
      TabIndex        =   60
      Top             =   1260
      Width           =   2295
      Begin VB.PictureBox picPicture 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   2085
         Left            =   0
         MouseIcon       =   "Frm_SignBord.frx":1B709
         ScaleHeight     =   2085
         ScaleWidth      =   2235
         TabIndex        =   61
         Top             =   0
         Width           =   2235
      End
      Begin VB.Image ImgSize 
         Height          =   1305
         Left            =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.ComboBox Cmb_Land_ID 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6840
      TabIndex        =   5
      Top             =   1920
      Width           =   1575
   End
   Begin VB.CommandButton Btn_FullScreen 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_SignBord.frx":1BA13
      Style           =   1  'Graphical
      TabIndex        =   53
      Top             =   3090
      Width           =   345
   End
   Begin VB.TextBox Txt_SignBord_Remark 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   6210
      Width           =   7125
   End
   Begin VB.TextBox Txt_SIGNBORD_LABEL1 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1140
      Left            =   4020
      Locked          =   -1  'True
      MaxLength       =   255
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   4920
      Width           =   4425
   End
   Begin VB.TextBox Txt_SIGNBORD_HEIGHT 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1305
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   10
      Text            =   "0"
      Top             =   5340
      Width           =   825
   End
   Begin VB.TextBox Txt_SIGNBORD_WIDTH 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1320
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   9
      Text            =   "0"
      Top             =   4920
      Width           =   795
   End
   Begin VB.ComboBox Cmb_Type_SignBord 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_SignBord.frx":1BF9D
      Left            =   4980
      List            =   "Frm_SignBord.frx":1BFAD
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   4410
      Width           =   3495
   End
   Begin VB.ComboBox Cmb_SignBord_Round 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Frm_SignBord.frx":1C028
      Left            =   2850
      List            =   "Frm_SignBord.frx":1C02A
      TabIndex        =   7
      Top             =   4410
      Width           =   975
   End
   Begin VB.CheckBox Chk_SignBord_Alway 
      Height          =   195
      Left            =   2400
      TabIndex        =   34
      Top             =   4050
      Width           =   195
   End
   Begin VB.CheckBox Chk_SignBord_Temp 
      Height          =   195
      Left            =   1290
      TabIndex        =   32
      Top             =   4050
      Width           =   195
   End
   Begin VB.TextBox Txt_BUILDING_NO 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4260
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   1980
      Width           =   1665
   End
   Begin VB.TextBox Txt_SignBord_Round 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1320
      MaxLength       =   3
      TabIndex        =   6
      Text            =   "0"
      Top             =   4470
      Width           =   795
   End
   Begin VB.TextBox Txt_SIGNBORD_NAME 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4260
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   1470
      Width           =   4095
   End
   Begin VB.ComboBox Cmb_Zoneblock 
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1260
      TabIndex        =   1
      Top             =   1440
      Width           =   915
   End
   Begin VB.TextBox Txt_SIGNBORD_ID 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBE7&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2250
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   2
      Top             =   1470
      Width           =   435
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":1C02C
      Height          =   405
      Left            =   4890
      Picture         =   "Frm_SignBord.frx":1C895
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "����������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":1F08D
      Height          =   405
      Left            =   7170
      Picture         =   "Frm_SignBord.frx":1F8BB
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "ź������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Refresh 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":22145
      Height          =   405
      Left            =   11730
      Picture         =   "Frm_SignBord.frx":22A34
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Refresh Data"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":2558F
      Enabled         =   0   'False
      Height          =   405
      Left            =   9450
      Picture         =   "Frm_SignBord.frx":25E41
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "¡��ԡ������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":2887C
      Enabled         =   0   'False
      Height          =   405
      Left            =   8310
      Picture         =   "Frm_SignBord.frx":290DF
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "�ѹ�֡������"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":2BB04
      Height          =   405
      Left            =   6030
      Picture         =   "Frm_SignBord.frx":2C39B
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "��䢢�����"
      Top             =   480
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Search 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Frm_SignBord.frx":2EC80
      Height          =   405
      Left            =   10590
      Picture         =   "Frm_SignBord.frx":2F4E7
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "���Ң�����"
      Top             =   480
      Width           =   1140
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID_SIGNBORD 
      Height          =   2145
      Left            =   8550
      TabIndex        =   38
      Top             =   3930
      Width           =   4275
      _ExtentX        =   7541
      _ExtentY        =   3784
      _Version        =   393216
      BackColor       =   16777215
      Rows            =   3
      Cols            =   11
      FixedCols       =   0
      BackColorFixed  =   14079702
      BackColorSel    =   14079702
      ForeColorSel    =   16711680
      BackColorBkg    =   16777215
      GridColor       =   14737632
      GridColorFixed  =   15856113
      FocusRect       =   0
      HighLight       =   2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   11
      _Band(0).GridLinesBand=   0
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
   Begin MSComctlLib.ListView LstV_Img_Name 
      Height          =   2145
      Left            =   8550
      TabIndex        =   52
      Top             =   1260
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   3784
      View            =   3
      Arrange         =   2
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�ٻ����"
         Object.Width           =   2752
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton Btn_SelectImg 
      BackColor       =   &H00D6D6D6&
      Enabled         =   0   'False
      Height          =   315
      Left            =   10200
      Picture         =   "Frm_SignBord.frx":31DDE
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   2790
      Width           =   345
   End
   Begin MSComctlLib.ListView LstV_OwnerShip 
      Height          =   1875
      Left            =   120
      TabIndex        =   59
      Top             =   7170
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   3307
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "��������Ңͧ"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "������Ңͧ�����Է���"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "�������"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Email"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "��˹��Է��"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "�Ѻ������"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTP_Start_Date 
      Height          =   330
      Left            =   4260
      TabIndex        =   62
      Top             =   3960
      Width           =   1665
      _ExtentX        =   2937
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_SignBord.frx":32368
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   16580609
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComCtl2.DTPicker DTP_End_Date 
      Height          =   330
      Left            =   6930
      TabIndex        =   63
      Top             =   3960
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   582
      _Version        =   393216
      MousePointer    =   99
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "Frm_SignBord.frx":32682
      CalendarBackColor=   16777215
      CalendarForeColor=   0
      CalendarTitleBackColor=   11236681
      CalendarTitleForeColor=   16777215
      CalendarTrailingForeColor=   16729907
      Format          =   16580609
      CurrentDate     =   37987
      MaxDate         =   94234
      MinDate         =   36526
   End
   Begin MSComctlLib.ListView LstV_OwnerUse 
      Height          =   1875
      Left            =   6630
      TabIndex        =   64
      Top             =   7170
      Width           =   6165
      _ExtentX        =   10874
      _ExtentY        =   3307
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "��ҹ���"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "������"
         Object.Width           =   2558
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "����ͺ��ͧ"
         Object.Width           =   4587
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "�������"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   83
      Left            =   2910
      Top             =   1440
      Width           =   495
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   114
      Left            =   2760
      TabIndex        =   79
      Top             =   1500
      Width           =   120
   End
   Begin VB.Label Lb_SignBord_TotalTax 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00EBEBE7&
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   10530
      TabIndex        =   74
      Top             =   6270
      Width           =   1755
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   1
      Left            =   10500
      Top             =   6240
      Width           =   1815
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ê�������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   108
      Left            =   6930
      TabIndex        =   73
      Top             =   3510
      Width           =   1050
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�������Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   110
      Left            =   10290
      TabIndex        =   72
      Top             =   3510
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����Թ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   111
      Left            =   8880
      TabIndex        =   71
      Top             =   3510
      Width           =   960
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*���˵ػ�Ѻ����¹ :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   28
      Left            =   2520
      TabIndex        =   68
      Top             =   960
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Lb_SignBord_SumLand 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00EBEBE7&
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   255
      Left            =   1290
      TabIndex        =   66
      Top             =   5760
      Width           =   855
   End
   Begin VB.Label Lb_Street 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   4260
      TabIndex        =   58
      Top             =   2970
      Width           =   1665
   End
   Begin VB.Label Lb_Soi 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1290
      TabIndex        =   57
      Top             =   2970
      Width           =   1635
   End
   Begin VB.Label Lb_Village 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   4260
      TabIndex        =   56
      Top             =   2490
      Width           =   1665
   End
   Begin VB.Label Lb_Tambon 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C2723B&
      Height          =   285
      Left            =   1290
      TabIndex        =   55
      Top             =   2490
      Width           =   1635
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1995
      Index           =   18
      Left            =   6600
      Top             =   7110
      Width           =   6225
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����ͺ��ͧ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   26
      Left            =   7080
      TabIndex        =   51
      Top             =   6810
      Width           =   1320
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��Ңͧ����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   25
      Left            =   750
      TabIndex        =   50
      Top             =   6810
      Width           =   900
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "������ջ���                                  �ҷ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   23
      Left            =   9420
      TabIndex        =   49
      Top             =   6300
      Width           =   3360
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   17
      Left            =   1290
      Top             =   6180
      Width           =   7185
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����˵�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   22
      Left            =   360
      TabIndex        =   48
      Top             =   6210
      Width           =   795
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��.��."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   21
      Left            =   2310
      TabIndex        =   47
      Top             =   5790
      Width           =   615
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   1185
      Index           =   14
      Left            =   3990
      Top             =   4890
      Width           =   4485
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��鹷��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   20
      Left            =   750
      TabIndex        =   46
      Top             =   5790
      Width           =   405
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ͤ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   19
      Left            =   3180
      TabIndex        =   45
      Top             =   4980
      Width           =   675
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   18
      Left            =   2310
      TabIndex        =   44
      Top             =   5400
      Width           =   315
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   17
      Left            =   2310
      TabIndex        =   43
      Top             =   4980
      Width           =   315
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   13
      Left            =   1290
      Top             =   5310
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   16
      Left            =   840
      TabIndex        =   42
      Top             =   5400
      Width           =   315
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   12
      Left            =   1290
      Top             =   4890
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ҧ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   15
      Left            =   720
      TabIndex        =   41
      Top             =   4980
      Width           =   435
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����������"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   14
      Left            =   3960
      TabIndex        =   40
      Top             =   4530
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��ҹ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   13
      Left            =   2280
      TabIndex        =   39
      Top             =   4530
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ӹǹ��ҹ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   12
      Left            =   270
      TabIndex        =   37
      Top             =   4530
      Width           =   885
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ���Ͷ͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   11
      Left            =   6060
      TabIndex        =   36
      Top             =   4050
      Width           =   795
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѹ�Դ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   10
      Left            =   3435
      TabIndex        =   35
      Top             =   4050
      Width           =   705
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   9
      Left            =   2715
      TabIndex        =   33
      Top             =   4050
      Width           =   435
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�ѡɳл���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   8
      Left            =   240
      TabIndex        =   31
      Top             =   4050
      Width           =   915
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "��������´����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   7
      Left            =   570
      TabIndex        =   30
      Top             =   3510
      Width           =   1215
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   6
      Left            =   4230
      Top             =   1950
      Width           =   1725
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   3
      Left            =   1290
      Top             =   4440
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ţ����Ҥ��"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   6
      Left            =   3180
      TabIndex        =   29
      Top             =   2010
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�Ӻ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   30
      Left            =   720
      TabIndex        =   28
      Top             =   2520
      Width           =   435
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���� (�����)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   31
      Left            =   3180
      TabIndex        =   27
      Top             =   2520
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   32
      Left            =   3750
      TabIndex        =   26
      Top             =   3000
      Width           =   345
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   33
      Left            =   825
      TabIndex        =   25
      Top             =   3000
      Width           =   330
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2925
      Index           =   9
      Left            =   60
      Top             =   3780
      Width           =   12825
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2865
      Index           =   11
      Left            =   90
      Top             =   3810
      Width           =   12765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   5
      Left            =   120
      TabIndex        =   24
      Top             =   2010
      Width           =   1065
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʷ��Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   3
      Left            =   6030
      TabIndex        =   23
      Top             =   2010
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���Ǥ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   2
      Left            =   1575
      TabIndex        =   22
      Top             =   4050
      Width           =   675
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   315
      Index           =   0
      Left            =   4230
      Top             =   1440
      Width           =   4155
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ͻ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   1
      Left            =   3540
      TabIndex        =   21
      Top             =   1530
      Width           =   555
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʻ���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00575555&
      Height          =   240
      Index           =   4
      Left            =   480
      TabIndex        =   20
      Top             =   1560
      Width           =   675
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H008B8B8B&
      BorderColor     =   &H00D6BD9C&
      Height          =   345
      Index           =   10
      Left            =   2220
      Top             =   1440
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2145
      Index           =   24
      Left            =   30
      Top             =   1260
      Width           =   8505
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   2085
      Index           =   8
      Left            =   60
      Top             =   1290
      Width           =   8445
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����駻���"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   780
      TabIndex        =   19
      Top             =   990
      Width           =   675
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   4
      Left            =   30
      Top             =   930
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   5
      Left            =   60
      Top             =   3450
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   0
      Left            =   60
      Top             =   6750
      Width           =   2325
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   1
      Left            =   6570
      Top             =   6750
      Width           =   2325
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00FFFFFF&
      Height          =   1995
      Index           =   5
      Left            =   90
      Top             =   7110
      Width           =   6465
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2055
      Index           =   4
      Left            =   60
      Top             =   7080
      Width           =   6525
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00BAD7E6&
      BorderColor     =   &H00C0C0C0&
      Height          =   2055
      Index           =   19
      Left            =   6570
      Top             =   7080
      Width           =   6285
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H00D6D6D6&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H008A8A7A&
      Height          =   315
      Index           =   3
      Left            =   6330
      Top             =   3450
      Width           =   2205
   End
End
Attribute VB_Name = "Frm_SignBord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Status As String

Private Sub ShowPicture(Path_Picture As String)
                On Error GoTo HldNoPic
                Dim ShrinkScale As Single
                
                ImgSize.Stretch = False
                ImgSize.Picture = LoadPicture(Path_Picture)
                originalWidth = ImgSize.Width
                originalHeight = ImgSize.Height
                sWidth = ImgSize.Width
                sHeight = ImgSize.Height
                picPicture.AutoSize = True
                picPicture.Picture = LoadPicture(Path_Picture)
                
                picPicture.Left = 0
                picPicture.Top = 0
                        
                ImgSize.Stretch = True
                ImgSize.Width = sWidth
                ImgSize.Height = sHeight
        
                sWidth = picParent.Width
                ShrinkScale = picParent.Width / ImgSize.Width
                sHeight = CSng(sHeight * ShrinkScale)
        
        If sHeight > picParent.Height Then
                ShrinkScale = picParent.Height / sHeight
                sWidth = CSng(sWidth * ShrinkScale)
                sHeight = CSng(sHeight * ShrinkScale) - 1
        End If
                ImgSize.Stretch = False
                  Call SizeImage(picPicture, ImgSize.Picture, sWidth, sHeight)
                  Exit Sub
HldNoPic:
            picPicture.Picture = LoadPicture("")
            ImgSize.Picture = LoadPicture("")
End Sub

Private Sub SizeImage(picBox As PictureBox, sizePic As Picture, sizeWidth As Single, sizeHeight As Single)
On Error Resume Next
        picBox.Picture = LoadPicture("")
        picBox.Width = sizeWidth
        picBox.Height = sizeHeight
        picBox.AutoRedraw = True
        picBox.PaintPicture sizePic, 0, 0, sizeWidth, sizeHeight
        picBox.Picture = picBox.Image
        picBox.AutoRedraw = False
        picBox.Left = (picParent.ScaleWidth / 2) - (picPicture.ScaleWidth / 2)
End Sub

Private Sub SET_TEXTBOX(STATE As String, Optional Perpose As String)
Dim i As Byte
If STATE = "ADD" Then cmd_Chk_ID.Enabled = True
If STATE = "ADD" Or STATE = "EDIT" Then    'Manage Button
        Btn_Add.Enabled = False:           Btn_Edit.Enabled = False
        Btn_Del.Enabled = False:            Btn_Post.Enabled = True:       Btn_Cancel.Enabled = True
        Btn_Search.Enabled = False:     'Btn_Refresh.Enabled = False
        Btn_SelectImg.Enabled = True: Btn_FullScreen.Enabled = True: btn_add_SignBord.Enabled = True
        Cmb_Zoneblock.Enabled = True
        Cmb_Land_ID.Enabled = True
        Cmb_Building_ID.Enabled = True
    
        Txt_SIGNBORD_ID.Locked = False:                 Txt_SIGNBORD_ID.BackColor = &H80000005
        Txt_SIGNBORD_ID2.Locked = False:              Txt_SIGNBORD_ID2.BackColor = &H80000005
        Txt_SIGNBORD_NAME.Locked = False:        Txt_SIGNBORD_NAME.BackColor = &H80000005
'        Txt_BUILDING_ID.Locked = False:                        Txt_BUILDING_ID.BackColor = &H80000005
        Txt_BUILDING_NO.Locked = False:                 Txt_BUILDING_NO.BackColor = &H80000005
    
        Txt_SignBord_Round.Locked = False:             Txt_SignBord_Round.BackColor = &H80000005
        Txt_SIGNBORD_WIDTH.Locked = False:      Txt_SIGNBORD_WIDTH.BackColor = &H80000005
        Txt_SIGNBORD_HEIGHT.Locked = False:    Txt_SIGNBORD_HEIGHT.BackColor = &H80000005
    
        Txt_SIGNBORD_LABEL1.Locked = False:                               Txt_SIGNBORD_LABEL1.BackColor = &H80000005
    '    Txt_SIGNBORD_LABEL2.Locked = False:                               Txt_SIGNBORD_LABEL2.BackColor = &H80000005
    '    Txt_SIGNBORD_LABEL3.Locked = False:                                Txt_SIGNBORD_LABEL3.BackColor = &H80000005
        Txt_SignBord_Remark.Locked = False:                                    Txt_SignBord_Remark.BackColor = &H80000005
        Chk_Flag_PayTax0.Enabled = True
        Chk_Flag_PayTax1.Enabled = True
        If STATE = "EDIT" Then
             Cmb_Zoneblock.Enabled = False
             Txt_SIGNBORD_ID.Locked = True: Txt_SIGNBORD_ID.BackColor = &HEBEBE7
             Txt_StoryRemark.Visible = True
             Label2(28).Visible = True
             Shape3(0).BackColor = &HC0C000
             Shape3(1).BackColor = &HC0C000
             Shape3(3).BackColor = &HC0C000
             Shape3(4).BackColor = &HC0C000
             Shape3(5).BackColor = &HC0C000
             cb_Signboard_Change_ID.Clear
             cb_Signboard_Change.Clear
             GBQuerySignboardChange.Requery
             If GBQuerySignboardChange.RecordCount > 0 Then
                 GBQuerySignboardChange.MoveFirst
                 Do While Not GBQuerySignboardChange.EOF
                       cb_Signboard_Change_ID.AddItem GBQuerySignboardChange.Fields("CHANGE_ID").Value
                       cb_Signboard_Change.AddItem GBQuerySignboardChange.Fields("CHANGE_DETAILS").Value
                       GBQuerySignboardChange.MoveNext
                 Loop
             End If
             cb_Signboard_Change.Visible = True
             cb_Signboard_Change.ListIndex = 0
             cb_Signboard_Change_ID.ListIndex = 0
        End If
End If

If STATE = "POST" Or STATE = "CANCEL" Or STATE = "DEL" Or STATE = "ADD" Then 'Manage TextBox
                        Txt_SIGNBORD_ID.Text = ""
                        Txt_SIGNBORD_ID2.Text = ""
                        Txt_SIGNBORD_NAME.Text = ""
'                        Txt_BUILDING_ID.Text = ""
                        Txt_BUILDING_NO.Text = ""
                        Txt_SignBord_Round.Text = ""
                        
                        Txt_SIGNBORD_WIDTH.Text = ""
                        Txt_SIGNBORD_HEIGHT.Text = ""
                        Txt_SIGNBORD_LABEL1.Text = ""
'                        Txt_SIGNBORD_LABEL2.Text = ""
'                        Txt_SIGNBORD_LABEL3.Text = ""
                        Txt_SignBord_Remark.Text = ""
                        
                        Lb_Tambon.Caption = ""
                        Lb_Village.Caption = ""
                        Lb_Soi.Caption = ""
                        Lb_Street.Caption = ""
                        Lb_SignBord_SumLand.Caption = ""
                        Lb_SignBord_TotalTax.Caption = ""
                          
                        LstV_Img_Name.ListItems.Clear
                        LstV_OwnerShip.ListItems.Clear
                        LstV_OwnerUse.ListItems.Clear
                        Cmb_SignBord_Round.Clear
                        Cmb_Type_SignBord.ListIndex = -1
                        DTP_End_Date.Value = Now
                        DTP_Start_Date.Value = Now
                        picPicture.Picture = LoadPicture("")
                        picPicture.Width = 2235
                        picPicture.Height = 2085
                        picPicture.Left = 0
                        cb_Signboard_Change.Visible = False
                        Label2(4).Tag = ""
                        Label2(6).Tag = ""
                        Call Set_Grid
         If STATE <> "ADD" Then
                Btn_Add.Enabled = True:            Btn_Edit.Enabled = True
                Btn_Del.Enabled = True:             Btn_Post.Enabled = False:   Btn_Cancel.Enabled = False
                Btn_Search.Enabled = True:      'Btn_Refresh.Enabled = True
                Btn_SelectImg.Enabled = False: Btn_FullScreen.Enabled = False: btn_add_SignBord.Enabled = False
                cmd_Chk_ID.Enabled = False
                Cmb_Building_ID.Enabled = False
             
                Txt_SIGNBORD_ID.Locked = True:                   Txt_SIGNBORD_ID.BackColor = &HEBEBE7
                Txt_SIGNBORD_ID2.Locked = True:                   Txt_SIGNBORD_ID2.BackColor = &HEBEBE7
                Txt_SIGNBORD_ID2.Enabled = False
                Txt_SIGNBORD_NAME.Locked = True:             Txt_SIGNBORD_NAME.BackColor = &HEBEBE7
'                Txt_BUILDING_ID.Locked = True:                Txt_BUILDING_ID.BackColor = &HEBEBE7
                Txt_BUILDING_NO.Locked = True:             Txt_BUILDING_NO.BackColor = &HEBEBE7
            
                
                Txt_SignBord_Round.Locked = True:                Txt_SignBord_Round.BackColor = &HEBEBE7
                Txt_SIGNBORD_WIDTH.Locked = True:             Txt_SIGNBORD_WIDTH.BackColor = &HEBEBE7
                Txt_SIGNBORD_HEIGHT.Locked = True:          Txt_SIGNBORD_HEIGHT.BackColor = &HEBEBE7
            
                Txt_SIGNBORD_LABEL1.Locked = True:                            Txt_SIGNBORD_LABEL1.BackColor = &HEBEBE7
            '    Txt_SIGNBORD_LABEL2.Locked = True:                            Txt_SIGNBORD_LABEL2.BackColor = &HEBEBE7
            '    Txt_SIGNBORD_LABEL3.Locked = True:                            Txt_SIGNBORD_LABEL3.BackColor = &HEBEBE7
                Txt_SignBord_Remark.Locked = True:                                Txt_SignBord_Remark.BackColor = &HEBEBE7
            
                Cmb_Zoneblock.Enabled = False
                Cmb_Land_ID.Enabled = False
                Txt_StoryRemark.Visible = False
                Label2(28).Visible = False
                Shape3(0).BackColor = &HD6D6D6
                Shape3(1).BackColor = &HD6D6D6
                Shape3(3).BackColor = &HD6D6D6
                Shape3(4).BackColor = &HD6D6D6
                Shape3(5).BackColor = &HD6D6D6
                Chk_Flag_PayTax0.Enabled = False
                Chk_Flag_PayTax1.Enabled = False
                Txt_Buffer.Text = ""
        End If
End If
End Sub
Private Sub Set_Grid()
            With GRID_SIGNBORD
             .Clear
             .Rows = 2
            .ColWidth(0) = 550:   .TextArray(0) = "��ҹ���":                                  .ColAlignment(0) = flexAlignCenterCenter
            .ColWidth(1) = 1000:   .TextArray(1) = "������� (�ҷ)": .ColAlignment(1) = flexAlignCenterCenter
            .ColWidth(2) = 950:   .TextArray(2) = "�ѡɳл���": .ColAlignment(2) = flexAlignLeftTop
            .ColWidth(3) = 3500:   .TextArray(3) = "����������": .ColAlignment(3) = flexAlignLeftTop
            .ColWidth(4) = 4000:     .TextArray(4) = "��ͤ���": .ColAlignment(4) = flexAlignLeftTop
            .ColWidth(5) = 900:    .TextArray(5) = "�������ҧ": .ColAlignment(5) = flexAlignCenterCenter
            .ColWidth(6) = 900:   .TextArray(6) = "�������": .ColAlignment(6) = flexAlignCenterCenter
            .ColWidth(7) = 1500:   .TextArray(7) = "��鹷�����": .ColAlignment(7) = flexAlignCenterCenter
            .ColWidth(8) = 2500:   .TextArray(8) = "�����˵�": .ColAlignment(8) = flexAlignLeftTop
            .ColWidth(9) = 0
            .ColWidth(10) = 0
            End With
End Sub

Private Sub SET_REFRESH()
   GBQueryZoneTax.Requery
   GBQuerySignBordData_Type.Requery
   GBQueryLandData.Requery
   GBQuerySignBordData.Requery
   
         Cmb_Zoneblock.Clear
         GBQueryZone.Filter = "ZONE_ID <> '' "
If GBQueryZone.RecordCount > 0 Then
            GBQueryZone.MoveFirst
    Do While Not GBQueryZone.EOF
                  If LenB(Trim$(GBQueryZone.Fields("ZONE_BLOCK").Value)) > 0 Then
                    Cmb_Zoneblock.AddItem GBQueryZone.Fields("ZONE_BLOCK").Value
                  End If
                    GBQueryZone.MoveNext
    Loop
End If
End Sub
Private Function CheckBeforPost() As Boolean
        
        CheckBeforPost = True
        Dim i As Byte
    If TProduct = "MANAGER" Or TProduct = "MODELLER" Then
            If LenB(CheckDongleKey) = 0 Then
                    CheckBeforPost = False
                    Exit Function
            End If
    End If
     If Len(Cmb_Zoneblock.Text) + Len(Txt_SIGNBORD_ID.Text) <> 6 Then
                MsgBox "�ô�к����ʷ���駻������ҧ��� 6 ��ѡ !", vbCritical, "�Ӫ��ᨧ !"
                CheckBeforPost = False
                Exit Function
     End If
     If LenB(Trim$(Txt_SIGNBORD_NAME.Text)) = 0 Then
            MsgBox "�ô�кت��ͻ��� !", vbCritical, "�Ӫ��ᨧ !"
           CheckBeforPost = False
          Exit Function
    End If
     
    If LenB(Trim$(Cmb_Land_ID.Text)) = 0 Then
        MsgBox "�ô�к����ʷ��Թ !", vbCritical, "�Ӫ��ᨧ !"
        CheckBeforPost = False
        Exit Function
    End If
    
    If LenB(Trim$(Txt_SignBord_Round.Text)) = 0 Then
            MsgBox "�ô�к�  ��������´���´��� !", vbCritical, "�Ӫ��ᨧ !"
            CheckBeforPost = False
            Exit Function
    End If
    
    For i = 1 To Txt_SignBord_Round.Text
                If LenB(Trim$(GRID_SIGNBORD.TextMatrix(i, 1))) = 0 Then
                    MsgBox "�ô�к�  ��������´��ҹ���ú�ӹǹ !", vbCritical, "�Ӫ��ᨧ !"
                    CheckBeforPost = False
                    Exit Function
                End If
    Next i

    If LstV_OwnerShip.ListItems.Count = 0 Then
         MsgBox "�к� ������Ңͧ���� ! ", vbCritical, "�Ӫ��ᨧ !"
         CheckBeforPost = False
         Exit Function
    End If
    
    If LstV_OwnerUse.ListItems.Count = 0 Then
            MsgBox "�к� ���ͼ���ͺ��ͧ ! ", vbCritical, "�Ӫ��ᨧ !"
            CheckBeforPost = False
            Exit Function
    End If
    
    For i = 1 To LstV_OwnerUse.ListItems.Count
            If LenB(Trim$(LstV_OwnerUse.ListItems.Item(i).SubItems(2))) = 0 Then
                    MsgBox "�к� ���ͼ���ͺ��ͧ ! ", vbCritical, "�Ӫ��ᨧ !"
                    CheckBeforPost = False
                    Exit Function
            Else
                    CheckBeforPost = True
            End If
    Next i
 
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LstV_OwnerShip.ListItems.Item(i).SubItems(7) = "1" Then
                 CheckBeforPost = True
                 Exit Function
            Else
                 CheckBeforPost = False
           End If
    Next i
    If CheckBeforPost = False Then
            MsgBox "�ô�кؼ���ա����Է��줹�á !", vbExclamation, "�Ӫ��ᨧ "
            Exit Function
    End If
                                       
    For i = 1 To LstV_OwnerShip.ListItems.Count
           If LenB(Trim$(LstV_OwnerShip.ListItems.Item(i).SubItems(8))) = 0 Then
                 CheckBeforPost = False
           Else
                 CheckBeforPost = True
                 Exit For
           End If
     Next i
    If CheckBeforPost = False Then
             MsgBox "�ô�кؼ���Ѻ������ !", vbExclamation, "�Ӫ��ᨧ "
             Exit Function
    End If
    
    On Error GoTo ErrLabel
    If LstV_OwnerUse.ListItems.Count <> 0 Then
       For i = 1 To LstV_OwnerUse.ListItems.Count
                If LenB(Trim$(LstV_OwnerUse.ListItems(i).ListSubItems(4).Text)) = 0 Then
                     MsgBox "�к� ���ͼ���ͺ��ͧ�ѧ���ú�ӹǹ ", vbCritical, "�Ӫ��ᨧ !"
                     CheckBeforPost = False
                     Exit Function
                End If
       Next i
    End If
Exit Function
ErrLabel:
        MsgBox "�к� ���ͼ���ͺ��ͧ�ѧ���ú�ӹǹ ", vbCritical, "�Ӫ��ᨧ !"
        CheckBeforPost = False
End Function

Private Sub WriteHistoryFile(Tmp_SIGNBORD_ID As String)
Dim sql_txt As String, i As Byte, ROUND_COUNT As String, Temp_Change As String
Dim Flag_Change As Byte
Dim Query1 As ADODB.Recordset
Set Query1 = New ADODB.Recordset
i = 1
'sql_txt = " Select A.* , B.OWNERSHIP_ID  From SIGNBORDDATA A , SIGNBORDDATA_NOTIC B WHERE  B.SIGNBORD_ID = A.SIGNBORD_ID AND A.SIGNBORD_ID = '" & Tmp_SIGNBORD_ID & "'"
'Call SET_QUERY(sql_txt, Query)
Set Query1 = Globle_Connective.Execute("exec sp_writehistory_signboard_query '" & Tmp_SIGNBORD_ID & "','0'", , adCmdUnknown)
With Query1
                     .MoveFirst
               Do While Not .EOF
'                        sql_txt = Empty
                     If i = 1 Then
                            If Status = "EDIT" Then
                                    Temp_Change = cb_Signboard_Change_ID.Text
                                    Flag_Change = 3
                            ElseIf Status = "DEL" Then
                                    Temp_Change = "002"
                                    Flag_Change = 7
                            ElseIf Status = "ADD" Then
                                    Temp_Change = "001"
                                    Flag_Change = 7
                            End If
                            ROUND_COUNT = RunAutoNumber("SIGNBORDDATASTORY", "ROUND_COUNT", Tmp_SIGNBORD_ID & "-0001", False, " AND SIGNBORD_ID = '" & Tmp_SIGNBORD_ID & "'")
                            Call SET_Execute2("exec sp_insert_signboarddata_story '" & ROUND_COUNT & "','" & .Fields("SIGNBORD_ID").Value & "','" & _
                                    .Fields("SIGNBORD_NAME").Value & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("BUILDING_NO").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & .Fields("LAND_ID").Value & "','" & _
                                    IIf(IsNull(.Fields("SIGNBORD_START_DATE").Value) = False, CvDate2(.Fields("SIGNBORD_START_DATE").Value), "") & "','" & IIf(IsNull(.Fields("SIGNBORD_END_DATE").Value) = False, CvDate2(.Fields("SIGNBORD_END_DATE").Value), "") & "'," & .Fields("SIGNBORD_ROUND").Value & "," & CCur(.Fields("SIGNBORD_SUMTAX").Value) & "," & .Fields("FLAG_PAYTAX").Value & ",'" & _
                                    .Fields("USER_LOGIN").Value & "','" & IIf(IsNull(.Fields("DATE_ACCESS").Value) = False, CvDate2(.Fields("DATE_ACCESS").Value, 1), CvDate2(Date)) & " " & Time & "'," & Flag_Change & ",'" & Temp_Change & "'", 8, False)
'                                        sql_txt = "INSERT INTO SIGNBORDDATASTORY ( ROUND_COUNT,SIGNBORD_ID,SIGNBORD_NAME , BUILDING_ID ,BUILDING_NO,ZONE_BLOCK,LAND_ID,SIGNBORD_START_DATE, SIGNBORD_END_DATE,SIGNBORD_ROUND,SIGNBORD_SUMTAX ,STORY_REMARK,STORY_UPDATE,FLAG_PAYTAX )"
'                                        sql_txt = sql_txt & " VALUES ('" & ROUND_COUNT & "','" & .Fields("SIGNBORD_ID").Value & "','"
'                                        sql_txt = sql_txt & .Fields("SIGNBORD_NAME").Value & "','" & .Fields("BUILDING_ID").Value & "','" & .Fields("BUILDING_NO").Value & "','" & .Fields("ZONE_BLOCK").Value & "','" & .Fields("LAND_ID").Value & "',"
'                                        sql_txt = sql_txt & IIf(IsNull(.Fields("SIGNBORD_START_DATE").Value), "Null", "'" & .Fields("SIGNBORD_START_DATE").Value & "'") & "," & IIf(IsNull(.Fields("SIGNBORD_END_DATE").Value), "Null", "'" & .Fields("SIGNBORD_END_DATE").Value & "'") & "," & .Fields("SIGNBORD_ROUND").Value & "," & .Fields("SIGNBORD_SUMTAX").Value & ",'" & Trim$(Txt_StoryRemark.Text) & "','" & Date & "'," & .Fields("FLAG_PAYTAX").Value & ")"
'                                        Call SET_Execute(sql_txt)
                                i = 0
                     End If
'                           sql_txt = Empty
'                            Globle_Connective.Execute "exec sp_insert_signboard_notic_story '" & RunAutoNumber("SIGNBORDDATA_NOTIC_STORY", "SIGNBORD_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                    ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", , adCmdUnknown
                            Call SET_Execute2("exec sp_insert_signboard_notic_story '" & RunAutoNumber("SIGNBORDDATA_NOTIC_STORY", "SIGNBORD_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                    ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'", 8, False)
'                                                        sql_txt = " INSERT INTO SIGNBORDDATA_NOTIC_STORY ( SIGNBORD_NOTIC_ID, ROUND_COUNT,OWNERSHIP_ID ) "
'                                                        sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("SIGNBORDDATA_NOTIC_STORY", "SIGNBORD_NOTIC_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                                                        sql_txt = sql_txt & ROUND_COUNT & "','"
'                                                        sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "')"
'                                                        Call SET_Execute(sql_txt)
                       Query1.MoveNext
'                       i = i + 1
                Loop
                
'                sql_txt = Empty
'                sql_txt = " Select *  From SIGNBORDDATA_APPLY WHERE SIGNBORD_ID  = '" & Tmp_SIGNBORD_ID & "'"
                Set Query1 = Globle_Connective.Execute("exec sp_writehistory_signboard_query '" & Tmp_SIGNBORD_ID & "','1'", , adCmdUnknown)
'                Call SET_QUERY(sql_txt, Query)
            With Query1
                .MoveFirst
               Do While Not .EOF
'                        Globle_Connective.Execute "exec sp_insert_signboard_apply_story '" & RunAutoNumber("SIGNBORDDATA_APPLY_STORY", "SIGNBORD_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'," & .Fields("SIGNBORD_PAGE").Value, , adCmdUnknown
                        Call SET_Execute2("exec sp_insert_signboard_apply_story '" & RunAutoNumber("SIGNBORDDATA_APPLY_STORY", "SIGNBORD_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                ROUND_COUNT & "','" & .Fields("OWNERSHIP_ID").Value & "'," & .Fields("SIGNBORD_PAGE").Value, 8, False)
'                                sql_txt = Empty
'                                sql_txt = "INSERT INTO SIGNBORDDATA_APPLY_STORY ( SIGNBORD_APPLY_ID,ROUND_COUNT,OWNERSHIP_ID,SIGNBORD_PAGE) "
'                                sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("SIGNBORDDATA_APPLY_STORY", "SIGNBORD_APPLY_ID", ROUND_COUNT & "/01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                                sql_txt = sql_txt & ROUND_COUNT & "','"
'                                sql_txt = sql_txt & .Fields("OWNERSHIP_ID").Value & "'," & .Fields("SIGNBORD_PAGE").Value & ")"
'                                Call SET_Execute(sql_txt)
                        .MoveNext
                Loop
            End With
                
'                sql_txt = Empty
'                sql_txt = " Select *  From SIGNBORDDATA_DETAILS  WHERE SIGNBORD_ID  = '" & Tmp_SIGNBORD_ID & "'"
'                Call SET_QUERY(sql_txt, Query)
                Set Query1 = Globle_Connective.Execute("exec sp_writehistory_signboard_query '" & Tmp_SIGNBORD_ID & "','2'", , adCmdUnknown)
            With Query1
                         .MoveFirst
                    Do While Not .EOF
'                        Debug.Print "exec sp_insert_signboard_details_story '" & RunAutoNumber("SIGNBORDDATA_DETAILS_STORY", "SIGNBORD_SUB_ID", ROUND_COUNT & "-01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                ROUND_COUNT & "','" & .Fields("SIGNBORD_LABEL").Value & "','" & .Fields("SIGNBORD_REMARK").Value & "'," & .Fields("SIGNBORD_CLASS").Value & "," & .Fields("SIGNBORD_PAGE").Value & "," & _
'                                .Fields("SIGNBORD_TYPE_ID").Value & "," & .Fields("SIGNBORD_WIDTH").Value & "," & .Fields("SIGNBORD_HEIGHT").Value & "," & .Fields("SIGNBORD_TOTAL").Value & "," & .Fields("SIGNBORD_AMOUNT").Value
'                        Globle_Connective.Execute "exec sp_insert_signboard_details_story '" & RunAutoNumber("SIGNBORDDATA_DETAILS_STORY", "SIGNBORD_SUB_ID", ROUND_COUNT & "-01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
'                                ROUND_COUNT & "','" & .Fields("SIGNBORD_LABEL").Value & "','" & .Fields("SIGNBORD_REMARK").Value & "'," & .Fields("SIGNBORD_CLASS").Value & "," & .Fields("SIGNBORD_PAGE").Value & "," & _
'                                .Fields("SIGNBORD_TYPE_ID").Value & "," & .Fields("SIGNBORD_WIDTH").Value & "," & .Fields("SIGNBORD_HEIGHT").Value & "," & .Fields("SIGNBORD_TOTAL").Value & "," & .Fields("SIGNBORD_AMOUNT").Value, , adCmdUnknown
                        Call SET_Execute2("exec sp_insert_signboard_details_story '" & RunAutoNumber("SIGNBORDDATA_DETAILS_STORY", "SIGNBORD_SUB_ID", ROUND_COUNT & "-01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','" & _
                                ROUND_COUNT & "','" & .Fields("SIGNBORD_LABEL").Value & "','" & .Fields("SIGNBORD_REMARK").Value & "'," & .Fields("SIGNBORD_CLASS").Value & "," & .Fields("SIGNBORD_PAGE").Value & "," & _
                                .Fields("SIGNBORD_TYPE_ID").Value & "," & .Fields("SIGNBORD_WIDTH").Value & "," & .Fields("SIGNBORD_HEIGHT").Value & "," & .Fields("SIGNBORD_TOTAL").Value & "," & .Fields("SIGNBORD_AMOUNT").Value, 8, False)
'                         sql_txt = "INSERT INTO SIGNBORDDATA_DETAILS_STORY ( SIGNBORD_SUB_ID, ROUND_COUNT, SIGNBORD_LABEL,SIGNBORD_REMARK , "
'                         sql_txt = sql_txt & "SIGNBORD_CLASS,SIGNBORD_PAGE,SIGNBORD_TYPE_ID,SIGNBORD_WIDTH,SIGNBORD_HEIGHT,SIGNBORD_TOTAL,SIGNBORD_AMOUNT) "
'                         sql_txt = sql_txt & " VALUES ( '" & RunAutoNumber("SIGNBORDDATA_DETAILS_STORY", "SIGNBORD_SUB_ID", ROUND_COUNT & "-01", False, " and ROUND_COUNT = '" & ROUND_COUNT & "'") & "','"
'                         sql_txt = sql_txt & ROUND_COUNT & "','"
'                         sql_txt = sql_txt & .Fields("SIGNBORD_LABEL").Value & "','" & .Fields("SIGNBORD_REMARK").Value & "'," & .Fields("SIGNBORD_CLASS").Value & "," & .Fields("SIGNBORD_PAGE").Value & ","
'                         sql_txt = sql_txt & .Fields("SIGNBORD_TYPE_ID").Value & "," & .Fields("SIGNBORD_WIDTH").Value & "," & .Fields("SIGNBORD_HEIGHT").Value & "," & .Fields("SIGNBORD_TOTAL").Value & "," & .Fields("SIGNBORD_AMOUNT").Value & ")"
'                        Call SET_Execute(sql_txt)
                        .MoveNext
                    Loop
            End With
                
'           Dim Logic As Boolean   'MAKE FOR insert ����������㹵��ҧ PP1 ��èѴ���������������¹����͡����Է��� =================================
'                 Logic = False
'                sql_txt = Empty
'                sql_txt = " Select  * From PP1 WHERE SIGNBORD_ID  = '" & Tmp_SIGNBORD_ID & "' ORDER BY PP1_PAY DESC"
'                Call SET_QUERY(sql_txt, Query)
'                If .RecordCount > 0 Then
'                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                           .MoveFirst
'                            Do While Not .EOF
'                                                              If Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) = Trim$(.Fields("OWNERSHIP_ID").Value) Then
'                                                                  Logic = True
'                                                                  .MoveLast
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                    .MoveNext
'                           Loop
'                                                             If Logic = False Then
'                                                                 .MovePrevious
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "INSERT INTO PP1 ( SIGNBORD_ID, OWNERSHIP_ID ,PP1_YEAR,PP1_NO,PP1_DATE , PP1_NO_STATUS,PP1_AMOUNT,PP1_STATUS,PP1_PAY" & _
'                                                                                   ",PP1_REMARK,FLAG_YEAR,PP1_NO_ACCEPT,PP1_DATE_ACCEPT,PP1_BOOK_NUMBER,PP1_BOOK_NO,PP1_PAY_DATE,PP1_TAXINCLUDE)"
'                                                                  sql_txt = sql_txt & "VALUES ('" & .Fields("SIGNBORD_ID").Value & "','" & Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) & "'," & .Fields("PP1_YEAR").Value & ",'" & _
'                                                                                   .Fields("PP1_NO").Value & "'," & IIf(IsNull(.Fields("PP1_DATE").Value), "Null", "'" & .Fields("PP1_DATE").Value & "'") & "," & .Fields("PP1_NO_STATUS").Value & "," & _
'                                                                                   .Fields("PP1_AMOUNT").Value & "," & .Fields("PP1_STATUS").Value & "," & .Fields("PP1_PAY").Value & ",'" & .Fields("PP1_REMARK").Value & "'," & .Fields("FLAG_YEAR").Value & ",'" & _
'                                                                                   .Fields("PP1_NO_ACCEPT").Value & "'," & IIf(IsNull(.Fields("PP1_DATE_ACCEPT").Value), "Null", "'" & .Fields("PP1_DATE_ACCEPT").Value & "'") & ",'" & .Fields("PP1_BOOK_NUMBER").Value & "','" & _
'                                                                                   .Fields("PP1_BOOK_NO").Value & "'," & IIf(IsNull(.Fields("PP1_PAY_DATE").Value), "Null", "'" & .Fields("PP1_PAY_DATE").Value & "'") & "," & .Fields("PP1_TAXINCLUDE").Value & ")"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                    Next i
'                           '========================================================
'                                    sql_txt = "UPDATE PP1 SET PP1_AMOUNT = " & CDbl(FormatNumber(Lb_SignBord_TotalTax.Caption, 2)) & _
'                                                    " WHERE SIGNBORD_ID = '" & Tmp_SIGNBORD_ID & "' AND PP1_YEAR = " & IIf(Year(Date) < 2500, Year(Date) + 543, Year(Date) - 543) & " AND PP1_STATUS = 0 AND PP1_NO_STATUS = 0 "
'                                    Call SET_Execute(sql_txt)
'                             Logic = False
'                                               .MoveFirst
'                                        Do While Not .EOF
'                                                    For i = 1 To LstV_OwnerShip.ListItems.Count
'                                                              If Trim$(.Fields("OWNERSHIP_ID").Value) = Trim$(LstV_OwnerShip.ListItems(i).ListSubItems(4).Text) Then
'                                                                  Logic = True
'                                                                   i = LstV_OwnerShip.ListItems.Count + 7
'                                                              Else
'                                                                  Logic = False
'                                                              End If
'                                                    Next i
'                                                             If Logic = False Then
'                                                                 sql_txt = Empty
'                                                                 sql_txt = "DELETE SIGNBORD_ID  FROM PP1 WHERE SIGNBORD_ID = '" & Tmp_SIGNBORD_ID & "' AND OWNERSHIP_ID = '" & Trim$(.Fields("OWNERSHIP_ID").Value) & "'"
'                                                                Call SET_Execute(sql_txt)
'                                                             End If
'                                                    .MoveNext
'                                        Loop
'                End If
End With
Set Query1 = Nothing
End Sub

Private Sub SET_DATABASE(STATE As String)
Dim sql_txt As String, i As Byte, Temp_SIGNBORD_ID As String
Dim Rs As ADODB.Recordset
 
 Set Rs = New ADODB.Recordset
If Txt_SIGNBORD_ID2.Text = Empty Then    ' ���� BUILDING_ID ����� Temp_BUILDING_ID ��� Procedure
       Temp_SIGNBORD_ID = Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text
Else
      Temp_SIGNBORD_ID = Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text & "/" & Txt_SIGNBORD_ID2.Text
End If
' Call SET_QUERY("SELECT MAX(PP1_YEAR) FROM PP1 WHERE SIGNBORD_ID='" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "'", Rs)
 Set Rs = Globle_Connective.Execute("exec sp_max_year_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','','0','',''", , adCmdUnknown)     '�һ�����ش�  GK1
If IsNull(Rs.Fields(0).Value) <> True Then
            Label2(4).Tag = Rs.Fields(0).Value
Else
            Label2(4).Tag = ""
End If
If STATE = "DEL" Then
        Call WriteHistoryFile(Temp_SIGNBORD_ID)
End If
Call SET_Execute2("exec sp_manage_signboarddata '" & Temp_SIGNBORD_ID & "','" & Trim$(Txt_SIGNBORD_NAME.Text) & "','" & _
                    Trim$(Cmb_Building_ID.Text) & "','" & Trim$(Txt_BUILDING_NO.Text) & "','" & Trim$(Cmb_Zoneblock.Text) & "','" & UCase$(Trim$(Cmb_Land_ID.Text)) & "','" & _
                    IIf(IsNull(DTP_Start_Date) = False, CvDate1(DTP_Start_Date), "") & "','" & IIf(IsNull(DTP_End_Date) = False, CvDate1(DTP_End_Date), "") & "'," & _
                    CByte(Txt_SignBord_Round.Text) & "," & CCur(Lb_SignBord_TotalTax.Caption) & "," & CByte(Chk_Flag_PayTax1.Value) & ",'" & strUser & "','" & CvDate2(Now, 1) & "','" & STATE & "'", 8, False)
                                      
                                      If STATE <> "DEL" Then
                                                With GRID_SIGNBORD
                                                        If LenB(Trim$(.TextMatrix(1, 1))) > 0 Then
                                                            Call SET_Execute2("exec sp_del_notic_apply_img_signboarddata '" & Temp_SIGNBORD_ID & "','3'", 8, False)
                                                            For i = 1 To .Rows - 2
                                                                    Call SET_Execute2("exec sp_insert_signboard_details '" & Temp_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','" & _
                                                                            Temp_SIGNBORD_ID & "','" & .TextMatrix(i, 4) & "','" & .TextMatrix(i, 8) & "'," & _
                                                                            CByte(.TextMatrix(i, 9)) & "," & CByte(.TextMatrix(i, 0)) & "," & CByte(.TextMatrix(i, 10)) & "," & CSng(Trim$(.TextMatrix(i, 5))) & "," & CSng(Trim$(.TextMatrix(i, 6))) & "," & _
                                                                            CSng(Trim$(.TextMatrix(i, 7))) & "," & CCur(Trim$(.TextMatrix(i, 1))), 8, False)
                                                            Next i
                                                        End If
                                                End With
                                                Call SET_Execute2("exec sp_del_notic_apply_img_signboarddata '" & Temp_SIGNBORD_ID & "','0'", 8, False)
'                                                           sql_txt = "Delete FROM SIGNBORDDATA_IMAGE  WHERE SIGNBORD_ID = '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "'"
'                                                           Call SET_Execute(sql_txt)
                                                If LstV_Img_Name.ListItems.Count > 0 Then
                                                    For i = 1 To LstV_Img_Name.ListItems.Count
                                                            Call SET_Execute2("exec sp_insert_image '" & Temp_SIGNBORD_ID & "','" & Temp_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','" & LstV_Img_Name.ListItems(i).Text & "','2'", 8, False)
'                                                           sql_txt = " INSERT INTO SIGNBORDDATA_IMAGE (SIGNBORD_IMG_ID, SIGNBORD_ID ,SIGNBORD_IMG_NAME ) "
'                                                           sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','"
'                                                           sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','"
'                                                           sql_txt = sql_txt & LstV_Img_Name.ListItems(i).Text & "')"
'                                                           Call SET_Execute(sql_txt)
                                                           Call CopyFile(LstV_Img_Name.ListItems(i).SubItems(1), iniPath_Picture & "\Zone" & Left$(Cmb_Zoneblock.Text, 2) & "\Block_" & Right$(Cmb_Zoneblock.Text, 1) & "\Signboard\" & LstV_Img_Name.ListItems(i).Text, 0)
                                                   Next i
                                              End If
                                            If LstV_OwnerShip.ListItems.Count > 0 Then
                                                    Call SET_Execute2("exec sp_del_notic_apply_img_signboarddata '" & Temp_SIGNBORD_ID & "','1'", 8, False)
'                                                     sql_txt = "Delete FROM SIGNBORDDATA_NOTIC WHERE SIGNBORD_ID = '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "'"
'                                                     Call SET_Execute(sql_txt)
                                                 For i = 1 To LstV_OwnerShip.ListItems.Count
                                                        Call SET_Execute2("exec sp_insert_signboard_notic '" & Temp_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','" & _
                                                                Temp_SIGNBORD_ID & "','" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'," & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "," & _
                                                                IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = "", 0, 1), 8, False)
'                                                        sql_txt = " INSERT INTO SIGNBORDDATA_NOTIC ( SIGNBORD_NOTIC_ID, SIGNBORD_ID,OWNERSHIP_ID,OWNERSHIP_NOTIC,OWNERSHIP_MAIL) "
'                                                        sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','"
'                                                        sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','"
'                                                        sql_txt = sql_txt & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'," & LstV_OwnerShip.ListItems(i).ListSubItems(7).Text & "," & _
'                                                                                         IIf(LstV_OwnerShip.ListItems(i).ListSubItems(8).Text = Empty, 0, 1) & ")"
'                                                        Call SET_Execute(sql_txt)
              ' �礡������¹�����Է���
                                                        If STATE = "EDIT" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" And LstV_OwnerShip.ListItems(i).ListSubItems(4).Text <> str_OwnershipID2 Then
        '                                                                Call SET_QUERY("SELECT MAX(PBT5_YEAR) FROM PBT5 WHERE LAND_ID='" & Temp_LAND_ID & "'", Rs)
                                                                        If LenB(Trim$(Label2(4).Tag)) > 0 Then
                                                                                Set Rs = Globle_Connective.Execute("exec sp_max_year_pp1 '" & Temp_SIGNBORD_ID & "','','3',''," & Label2(4).Tag, , adCmdUnknown) '�� COUNT_CHANGE
                                                                                If IsNull(Rs.Fields(0).Value) = False Then
                                                                                        Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                Else
                                                                                        Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                End If
                                                                    
                                                                                Select Case Rs.Fields(0).Value '  PBT5_SET_GK
                                                                                        Case 0
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pp1 '" & Temp_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0,''", 8, False)
                                                                                        Case 1
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pp1 '" & Temp_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & CInt(Label2(6).Tag) + 1 & ",1,''", 8, False)
                                                                                        Case 2
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pp1 '" & Temp_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",0.''", 8, False)
                                                                                End Select
        '                                                                        sql_txt = "UPDATE PBT5 SET FLAG_CHANGE=1 WHERE LAND_ID= '" & Temp_LAND_ID & "' AND PBT5_YEAR=" & Rs.Fields(0).Value
        '                                                                        Call SET_Execute(sql_txt)
                                                                        End If
                                                                End If
                                                        ElseIf STATE = "ADD" Then
                                                                If LstV_OwnerShip.ListItems(i).ListSubItems(7).Text = "1" Then
                                                                        If Chk_Flag_PayTax1.Value = Checked Then  '�ա�û����Թ����
                                                                                If LenB(Trim$(Label2(4).Tag)) > 0 Then
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_pbt5 '" & Temp_SIGNBORD_ID & "','','3',''," & Label2(4).Tag, , adCmdUnknown)  '�� COUNT_CHANGE
                                                                                        If IsNull(Rs.Fields(0).Value) = False Then
                                                                                                Label2(6).Tag = Rs.Fields(0).Value ' COUNT_CHANGE
                                                                                        Else
                                                                                                Label2(6).Tag = 0 'COUNT_CHANGE
                                                                                        End If
                                                                                        Set Rs = Globle_Connective.Execute("exec sp_max_year_pp1 '" & Temp_SIGNBORD_ID & "','','5'," & Label2(6).Tag & "," & Label2(4).Tag, , adCmdUnknown)  '�� COUNT_CHANGE
                                                                                        If Rs.Fields(1).Value = LstV_OwnerShip.ListItems(i).ListSubItems(4).Text Then
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pp1 '" & Temp_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",2,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        Else
'                                                                                                Globle_Connective.Execute "exec sp_update_flag_change_pp1 '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", , adCmdUnknown
                                                                                                Call SET_Execute2("exec sp_update_flag_change_pp1 '" & Temp_SIGNBORD_ID & "','" & Label2(4).Tag & "'," & Label2(6).Tag & ",3,'" & LstV_OwnerShip.ListItems(i).ListSubItems(4).Text & "'", 8, False)
                                                                                        End If
                                                                                End If
                                                                        End If
                                                                End If
                                                        End If
                                                 Next i
                                            End If
                                            If LstV_OwnerUse.ListItems.Count > 0 Then
                                                    Call SET_Execute2("exec sp_del_notic_apply_img_signboarddata '" & Temp_SIGNBORD_ID & "','2'", 8, False)
'                                                     sql_txt = "Delete FROM SIGNBORDDATA_APPLY WHERE SIGNBORD_ID = '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "'"
'                                                     Call SET_Execute(sql_txt)
                                                     
                                                 For i = 1 To LstV_OwnerUse.ListItems.Count
                                                            Call SET_Execute2("exec sp_insert_signboard_apply '" & Temp_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','" & _
                                                                            Temp_SIGNBORD_ID & "','" & LstV_OwnerUse.ListItems(i).ListSubItems(4).Text & "'," & CByte(LstV_OwnerUse.ListItems(i).Text), 8, False)
'                                                                sql_txt = "INSERT INTO SIGNBORDDATA_APPLY( SIGNBORD_APPLY_ID, SIGNBORD_ID, OWNERSHIP_ID , SIGNBORD_PAGE ) "
'                                                                sql_txt = sql_txt & " VALUES ( '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "-" & Format$(i, "##0#") & "','"
'                                                                sql_txt = sql_txt & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "','"
'                                                                sql_txt = sql_txt & LstV_OwnerUse.ListItems(i).ListSubItems(4).Text & "'," & CByte(LstV_OwnerUse.ListItems(i).Text) & ")"
'                                                               Call SET_Execute(sql_txt)
                                                 Next i
                                            End If
                                            Call WriteHistoryFile(Temp_SIGNBORD_ID)
                                     End If
                                    Call SET_REFRESH
'                                    If IsNull(Rs.Fields(0).Value) = False Then
'                                            Call SET_Execute("UPDATE PP1 SET PP1_AMOUNT_ACCEPT=" & Lb_SignBord_TotalTax.Caption & " WHERE SIGNBORD_ID='" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID & "' AND PP1_YEAR=" & Rs.Fields(0).Value & " AND FLAG_CHANGE=0 AND PP1_STATUS=0")
'                                    End If
'                                    Globle_Connective.CommitTrans
            Exit Sub
'Err_SetDatabase:
'            Globle_Connective.RollbackTrans
'            MsgBox Err.Description
End Sub

Private Sub Btn_Add_Click()
  Call SET_TEXTBOX("ADD", "Perpose")
  Status = "ADD"
  Txt_SIGNBORD_ID.SetFocus
End Sub

Private Sub btn_add_SignBord_Click()
'On Error Resume Next
Dim j As Integer
If LenB(Trim$(Cmb_SignBord_Round.Text)) = 0 Or Lb_SignBord_SumLand.Caption = "0" Or LenB(Trim$(Cmb_Type_SignBord.Text)) = 0 Then Exit Sub
   Dim Total  As Double, DigitNum As Double
 
'       Total = FormatNumber(CDbl(Lb_SignBord_SumLand.Caption)) / GBQuerySignBordData_Type.Fields("SIGNBORD_RATE_OF_USE").Value
'       DigitNum = CDbl(Total) - Int(Total)
'    If DigitNum <> 0 Then
'       If FormatNumber(DigitNum, 1) > 0.4 Then
'                     Total = Int(Total + 1)
'       Else
'                     Total = Int(Total)
'       End If
'    End If
'        Total = Total * GBQuerySignBordData_Type.Fields("SIGNBORD_RATE").Value
        Total = FormatNumber(CSng(Lb_SignBord_SumLand.Caption)) / 500
        DigitNum = Int(Total)
        Total = Total - Int(Total)
        If Total > 0.5 Then
                Total = (DigitNum * 500) + 500
        Else
                Total = (DigitNum * 500)
        End If
        Total = (Total / 500) * GBQuerySignBordData_Type.Fields("SIGNBORD_RATE").Value
      If Total < GBQuerySignBordData_Type.Fields("SIGNBORD_LOW").Value Then Total = GBQuerySignBordData_Type.Fields("SIGNBORD_LOW").Value

If Txt_SignBord_Round.Text > 0 Then
  If Cmb_SignBord_Round.ListIndex <> Cmb_SignBord_Round.ListCount - 1 Then
    With GRID_SIGNBORD
                            .TextMatrix(Cmb_SignBord_Round.Text, 1) = Total
                        If Chk_SignBord_Temp.Value Then
                            .TextMatrix(Cmb_SignBord_Round.Text, 2) = "���Ǥ���"
                        Else
                            .TextMatrix(Cmb_SignBord_Round.Text, 2) = "����"
                        End If
                              .TextMatrix(Cmb_SignBord_Round.Text, 3) = Cmb_Type_SignBord.Text
                              .TextMatrix(Cmb_SignBord_Round.Text, 4) = Trim$(Txt_SIGNBORD_LABEL1.Text) '& " " & Trim$(Txt_SIGNBORD_LABEL2.Text) & " " & Trim$(Txt_SIGNBORD_LABEL3.Text)
                              .TextMatrix(Cmb_SignBord_Round.Text, 5) = CSng(Txt_SIGNBORD_WIDTH.Text)
                              .TextMatrix(Cmb_SignBord_Round.Text, 6) = CSng(Txt_SIGNBORD_HEIGHT.Text)
                              .TextMatrix(Cmb_SignBord_Round.Text, 7) = Lb_SignBord_SumLand.Caption
                              .TextMatrix(Cmb_SignBord_Round.Text, 8) = Trim$(Txt_SignBord_Remark.Text)
                              If Chk_SignBord_Temp.Value Then
                                    .TextMatrix(Cmb_SignBord_Round.Text, 9) = "0"
                              Else
                                   .TextMatrix(Cmb_SignBord_Round.Text, 9) = "1"
                              End If
                                   .TextMatrix(Cmb_SignBord_Round.Text, 10) = Cmb_Type_SignBord.ListIndex + 1
   End With
 Else
      Dim i As Byte
      If Cmb_SignBord_Round.ListCount > 1 Then
            j = 2
      Else
            j = 1
      End If
         For i = 0 To Cmb_SignBord_Round.ListCount - j
                With GRID_SIGNBORD
                           .TextMatrix(i + 1, 1) = Total
                        If Chk_SignBord_Temp.Value Then
                            .TextMatrix(i + 1, 2) = "���Ǥ���"
                        Else
                            .TextMatrix(i + 1, 2) = "����"
                        End If
                              .TextMatrix(i + 1, 3) = Cmb_Type_SignBord.Text
                              .TextMatrix(i + 1, 4) = Trim$(Txt_SIGNBORD_LABEL1.Text) '& " " & Trim$(Txt_SIGNBORD_LABEL2.Text) & " " & Trim$(Txt_SIGNBORD_LABEL3.Text)
                              .TextMatrix(i + 1, 5) = CSng(Txt_SIGNBORD_WIDTH.Text)
                              .TextMatrix(i + 1, 6) = CSng(Txt_SIGNBORD_HEIGHT.Text)
                              .TextMatrix(i + 1, 7) = Lb_SignBord_SumLand.Caption
                              .TextMatrix(i + 1, 8) = Trim$(Txt_SignBord_Remark.Text)
                              If Chk_SignBord_Temp.Value Then
                                    .TextMatrix(i + 1, 9) = "0"
                              Else
                                   .TextMatrix(i + 1, 9) = "1"
                              End If
                              .TextMatrix(i + 1, 10) = Cmb_Type_SignBord.ListIndex + 1
                End With
         Next i
 End If
End If
                        Txt_SIGNBORD_WIDTH.Text = ""
                        Txt_SIGNBORD_HEIGHT.Text = ""
                        Txt_SIGNBORD_LABEL1.Text = ""
'                        Txt_SIGNBORD_LABEL2.Text = ""
'                        Txt_SIGNBORD_LABEL3.Text = ""
                        Txt_SignBord_Remark.Text = ""
                        Lb_SignBord_SumLand.Caption = ""
                        Total = 0
                        For i = 1 To GRID_SIGNBORD.Rows - 2
                              If LenB(Trim$((GRID_SIGNBORD.TextMatrix(i, 1)))) > 0 Then
                               Total = Total + CSng(GRID_SIGNBORD.TextMatrix(i, 1))
                              End If
                        Next i
                               Lb_SignBord_TotalTax.Caption = Total
End Sub

Private Sub Btn_Cancel_Click()
Call SET_TEXTBOX("CANCEL")
End Sub

Private Sub Btn_Del_Click()
If (LenB(Trim$(Cmb_Zoneblock.Text)) = 0) Or (MsgBox("�׹�ѹ���ź�����ŷ���¹���� ?", vbInformation + vbYesNo, "ź������!") = vbNo) Then Exit Sub
        On Error GoTo ErrHandler
        Globle_Connective.BeginTrans
        Status = "DEL"
        Call SET_DATABASE("DEL")
        Call SET_TEXTBOX("DEL")
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Edit_Click()
If LenB(Trim$(Cmb_Zoneblock.Text)) = 0 Then Exit Sub
Call SET_TEXTBOX("EDIT", "PERPOSE")
Status = "EDIT"
End Sub

Private Sub Btn_FullScreen_Click()
   If LstV_Img_Name.ListItems.Count > 0 Then
   GB_ImagePath = LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1)
  Frm_FocusImage.Show vbModal
  End If
End Sub

Private Sub Btn_Post_Click()
If CheckBeforPost = False Then Exit Sub
        Globle_Connective.BeginTrans
        On Error GoTo ErrHandler
        Call SET_DATABASE(Status)
        Call SET_TEXTBOX("POST")
        If TProduct = "MANAGER" Or TProduct = "MODELLER" Then
                Call Regenerate_Cadcorp
        End If
        Globle_Connective.CommitTrans
        Exit Sub
ErrHandler:
        Globle_Connective.RollbackTrans
        MsgBox Err.Number & "-" & Err.Description, vbOKOnly + vbExclamation, "��ͼԴ��Ҵ"
End Sub

Private Sub Btn_Refresh_Click()
     Call SET_REFRESH
End Sub

Private Sub Btn_Search_Click()
 With Frm_Search
      .ZOrder
      .Show
        .Shp_Menu(1).BackColor = &HD6D6D6
         .Shp_Menu(2).BackColor = &HD6D6D6
        .Shp_Menu(3).BackColor = &HD6D6D6
        .Shp_Menu(5).BackColor = &HD6D6D6
        .Shp_Menu(4).BackColor = &HB8CFD6
        .SSTab1.Tab = 3
        .optCode.Visible = True
        .optName.Visible = True
        .optCode.Caption = "���ʻ���"
        .cmd_SendToEdit.Caption = " << ��䢢����� - ����"
End With
End Sub

Private Sub Btn_SelectImg_Click()
Dim Picture_Path  As String
    With Clone_Form.CommonDialog1
            .DialogTitle = "���͡�ٻ�Ҿ�͡����Է���"
            .CancelError = True
              On Error GoTo ErrHandler
            .InitDir = "C:\"
            .Flags = cdlOFNHideReadOnly
            .Filter = "All Files (*.*)|*.*|Picture Files(*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif"     ' Set filters
            .FilterIndex = 2
            '.Action = 1
            .ShowOpen
             Picture_Path = .FileName
    End With
              If LenB(Trim$(Picture_Path)) > 0 Then
                 Call ShowPicture(Picture_Path)
           End If
                Set itmX = LstV_Img_Name.ListItems.Add()
                       itmX.SmallIcon = Clone_Form.ImageList1.ListImages(2).Index
                       Dim i As Byte, File_Name As String
                       For i = 0 To Len(Picture_Path)
                             If Mid$(Picture_Path, Len(Picture_Path) - i, 1) <> "\" Then
                                 File_Name = File_Name + Mid$(Picture_Path, Len(Picture_Path) - i, 1)
                              Else
                                  File_Name = StrReverse(File_Name)
                                  Exit For
                             End If
                       Next i
                       itmX.Text = File_Name
                       itmX.SubItems(1) = Picture_Path
                       Exit Sub
ErrHandler:
  picPicture.Cls
End Sub

Private Sub cb_Signboard_Change_Click()
        cb_Signboard_Change_ID.ListIndex = cb_Signboard_Change.ListIndex
End Sub


Private Sub Chk_Flag_PayTax0_Click()
If Chk_Flag_PayTax0.Value Then
   Chk_Flag_PayTax1.Value = Unchecked
   Lb_SignBord_TotalTax.Caption = "0.00"
Else
  Chk_Flag_PayTax1.Value = Checked
End If
End Sub

Private Sub Chk_Flag_PayTax1_Click()
Dim Total As Single, i As Byte
If Chk_Flag_PayTax1.Value Then
   Chk_Flag_PayTax0.Value = Unchecked
                        Total = 0
                        For i = 1 To GRID_SIGNBORD.Rows - 2
                              If LenB(Trim$((GRID_SIGNBORD.TextMatrix(i, 1)))) > 0 Then
                               Total = Total + CDbl(GRID_SIGNBORD.TextMatrix(i, 1))
                              End If
                        Next i
                               Lb_SignBord_TotalTax.Caption = Format$(Total, "0.00")
Else
  Chk_Flag_PayTax0.Value = Checked
End If
End Sub

Private Sub Chk_SignBord_Alway_Click()
If Chk_SignBord_Alway.Value Then
   Chk_SignBord_Temp.Value = Unchecked
Else
  Chk_SignBord_Temp.Value = Checked
End If
End Sub

Private Sub Chk_SignBord_Temp_Click()
If Chk_SignBord_Temp.Value Then
   Chk_SignBord_Alway.Value = Unchecked
Else
  Chk_SignBord_Alway.Value = Checked
End If
End Sub

Private Sub Cmb_Building_ID_Click()
        Dim Rs As ADODB.Recordset
        On Error GoTo ErrClick
        Set Rs = New ADODB.Recordset
        If LenB(Cmb_Building_ID.Text) > 0 Then
                Call SET_QUERY("SELECT BUILDING_HOME_NO FROM BUILDINGDATA WHERE BUILDING_ID='" & Cmb_Building_ID.Text & "'", Rs)
                If Rs.RecordCount > 0 Then
                        Txt_BUILDING_NO.Text = Rs.Fields(0).Value
                End If
        Else
                Txt_BUILDING_NO.Text = ""
        End If
        Set Rs = Nothing
        Exit Sub
ErrClick:
End Sub

Private Sub Cmb_Building_ID_KeyPress(KeyAscii As Integer)
        If KeyAscii <> 13 Then
                KeyAscii = 0
        End If
End Sub

Private Sub Cmb_Land_ID_Click()
If Len(Cmb_Land_ID.Text) >= 6 Then
        Txt_BUILDING_NO.Text = ""
        GBQueryLandData.Requery
        GBQueryLandData.Filter = " LAND_ID =  '" & Cmb_Land_ID.Text & "'"
        If GBQueryLandData.RecordCount > 0 Then
                    GBQueryLandData.MoveFirst
                    Lb_Tambon.Caption = GBQueryLandData.Fields("TAMBON_NAME").Value
                    Lb_Village.Caption = GBQueryLandData.Fields("VILLAGE_NAME").Value
                    Lb_Soi.Caption = GBQueryLandData.Fields("SOI_NAME").Value
                    Lb_Street.Caption = GBQueryLandData.Fields("STREET_NAME").Value
        End If
        GBQueryBuildingData.Requery
        GBQueryBuildingData.Filter = " LAND_ID = '" & Cmb_Land_ID.Text & "'"
        Cmb_Building_ID.Clear
        Cmb_Building_ID.AddItem ""
        If GBQueryBuildingData.RecordCount > 0 Then
                GBQueryBuildingData.MoveFirst
                Do While Not GBQueryBuildingData.EOF
                        If Cmb_Building_ID.List(Cmb_Building_ID.ListCount - 1) <> GBQueryBuildingData.Fields("BUILDING_ID").Value Then
                              Cmb_Building_ID.AddItem GBQueryBuildingData.Fields("BUILDING_ID").Value
                        End If
                        GBQueryBuildingData.MoveNext
                Loop
        End If
End If
End Sub

Private Sub Cmb_Land_ID_KeyPress(KeyAscii As Integer)
  If Status = "ADD" Then
 '     KeyAscii = 0
 '      Exit Sub
 Else
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
     
     If KeyAscii = 13 And Len(Cmb_Land_ID.Text) > 5 Then
                  GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Left$(Cmb_Land_ID.Text, 3) & "'"
                  Cmb_Land_ID.Clear
                If GBQueryLandData.RecordCount > 0 Then
                            GBQueryLandData.MoveFirst
                    Do While Not GBQueryLandData.EOF
                                  If LenB(Trim$(GBQueryLandData.Fields("ZONE_BLOCK").Value)) > 0 Then
                                    Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                                  End If
                                    GBQueryLandData.MoveNext
                    Loop
                End If
        End If
 End If
End Sub

Private Sub Cmb_SignBord_Round_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Cmb_Type_SignBord_Click()
        GBQuerySignBordData_Type.MoveFirst
        GBQuerySignBordData_Type.Find "  SIGNBORD_TYPE_ID = " & Cmb_Type_SignBord.ListIndex + 1
End Sub

Private Sub Cmb_Type_SignBord_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Cmb_Zoneblock_Click()
            Txt_SIGNBORD_ID.Text = ""
            Lb_Tambon.Caption = ""
            Lb_Village.Caption = ""
            Lb_Soi.Caption = ""
            Lb_Street.Caption = ""
     If LenB(Trim$(Cmb_Zoneblock.Text)) = 0 Then Exit Sub
           Cmb_Land_ID.Clear
         GBQueryLandData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
            
If GBQueryLandData.RecordCount > 0 Then
            GBQueryLandData.MoveFirst
    Do While Not GBQueryLandData.EOF
                  If LenB(Trim$(GBQueryLandData.Fields("ZONE_BLOCK").Value)) > 0 Then
                    Cmb_Land_ID.AddItem GBQueryLandData.Fields("LAND_ID").Value
                  End If
                    GBQueryLandData.MoveNext
    Loop
End If
End Sub

Private Sub Cmb_Zoneblock_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub cmd_Chk_ID_Click()
        Dim Temp_SIGNBORD_ID As String
        If Len(Trim$(Txt_SIGNBORD_ID.Text)) = 3 Then
                    If Txt_SIGNBORD_ID2.Text = Empty Then    ' ���� BUILDING_ID ����� Temp_BUILDING_ID ��� Procedure
                            Temp_SIGNBORD_ID = Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text
                    Else
                            Temp_SIGNBORD_ID = Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text & "/" & Txt_SIGNBORD_ID2.Text
                    End If
                    GBQuerySignBordData.Requery
                    GBQuerySignBordData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
                    If GBQuerySignBordData.RecordCount > 0 Then
                             GBQuerySignBordData.MoveFirst
                             GBQuerySignBordData.Find " SIGNBORD_ID  =  '" & Temp_SIGNBORD_ID & "'"
                            If GBQuerySignBordData.AbsolutePosition > 0 Or Chk_ID_IN_Tax(Temp_SIGNBORD_ID, 2) = False Then
                                    If Len(Trim$(Txt_SIGNBORD_ID2.Text)) = 3 Then
'                                            Txt_SIGNBORD_ID2.Text =
                                    Else
                                            MsgBox "���ҧ���ʻ��«�Ӣ��������  �ô����¹���ʻ������� !", vbCritical, "Warning!"
                                            Call SetSelFocus(Txt_SIGNBORD_ID)
                                    End If
                            Else
                                    Txt_SIGNBORD_NAME.SetFocus
                            End If
                    End If
            End If
End Sub

Private Sub Form_Activate()
  Clone_Form.Picture1.Visible = True
  Me.WindowState = vbMaximized
End Sub

Private Sub Form_Initialize()
LstV_OwnerShip.Icons = Clone_Form.ImageList1
LstV_OwnerShip.SmallIcons = Clone_Form.ImageList1

LstV_OwnerUse.Icons = Clone_Form.ImageList1
LstV_OwnerUse.SmallIcons = Clone_Form.ImageList1

LstV_Img_Name.Icons = Clone_Form.ImageList1
LstV_Img_Name.SmallIcons = Clone_Form.ImageList1
End Sub

Private Sub Form_Load()
Clone_Form.mnuPopup3.Visible = False
Call SET_REFRESH
Call Set_Grid
  Chk_SignBord_Alway.Value = Checked
End Sub


Private Sub Grid_Signbord_Click()
Call GRID_SIGNBORD_EnterCell
End Sub

Private Sub GRID_SIGNBORD_Collapse(Cancel As Boolean)
Txt_Buffer.Visible = False
End Sub

Private Sub GRID_SIGNBORD_EnterCell()
If Btn_Add.Enabled = True And Btn_Edit.Enabled = True Then Exit Sub
With GRID_SIGNBORD
   If (.Col = 1) And LenB(Trim$(.TextMatrix(.Row, .Col))) > 0 Then
     Txt_Buffer.Visible = True
     Txt_Buffer.Text = .TextMatrix(.Row, .Col)
     Txt_Buffer.Left = .CellLeft + .Left - 8
     Txt_Buffer.Top = .CellTop + .Top - 8
     Txt_Buffer.Width = .CellWidth - 1
     Txt_Buffer.Height = .CellHeight
     Txt_Buffer.SelStart = 0
     Txt_Buffer.SelLength = Len(Txt_Buffer.Text)
     Txt_Buffer.SetFocus
  Else
      Txt_Buffer.Visible = False
   End If
End With
End Sub

Private Sub GRID_SIGNBORD_Expand(Cancel As Boolean)
Txt_Buffer.Visible = False
End Sub

Private Sub GRID_SIGNBORD_Scroll()
   Txt_Buffer.Visible = False
End Sub

Private Sub Lb_SignBord_SumLand_Change()
    If LenB(Trim$(Lb_SignBord_SumLand.Caption)) = 0 Then Lb_SignBord_SumLand.Caption = "0"
End Sub

Private Sub Lb_SignBord_TotalTax_Change()
If LenB(Trim$(Lb_SignBord_TotalTax.Caption)) = 0 Then Lb_SignBord_TotalTax.Caption = "0.00"
If Chk_Flag_PayTax0.Value Then Lb_SignBord_TotalTax.Caption = "0.00"
'If CDbl(Lb_SignBord_TotalTax.Caption) - Int(Lb_SignBord_TotalTax.Caption) > 0 Then Lb_SignBord_TotalTax.Caption = Int(Lb_SignBord_TotalTax.Caption) + 1
End Sub

Private Sub LstV_Img_Name_ItemClick(ByVal Item As MSComctlLib.ListItem)
If LstV_Img_Name.ListItems.Count > 0 Then Call ShowPicture(LstV_Img_Name.ListItems.Item(LstV_Img_Name.SelectedItem.Index).SubItems(1))
End Sub

Private Sub LstV_Img_Name_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
        Clone_Form.mnuAddName3.Visible = False
        Clone_Form.mnuDelName3.Visible = False
        Clone_Form.mnuSetOwner3.Visible = False
        Clone_Form.mnuSetOwnerMail3.Visible = False
        Clone_Form.mnuDelImg3.Visible = True
        PopupMenu Clone_Form.mnuPopup3, vbPopupMenuLeftAlign, LstV_Img_Name.Left + X, LstV_Img_Name.Top + Me.Top + y
        picPicture.Picture = LoadPicture("")
        picPicture.Width = 2235
        picPicture.Height = 2085
   End If
   End If
End Sub

Private Sub LstV_OwnerUse_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   If LstV_OwnerUse.SortOrder = lvwAscending Then
      LstV_OwnerUse.SortOrder = lvwDescending
   Else
     LstV_OwnerUse.SortOrder = lvwAscending
   End If
    LstV_OwnerUse.SortKey = ColumnHeader.Index - 1
    LstV_OwnerUse.Sorted = True
End Sub

Private Sub LstV_OwnerUse_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If CByte(Txt_SignBord_Round.Text) <= 0 Then Exit Sub
If Button = vbRightButton Then
  If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
     If LstV_OwnerUse.ListItems.Count > 0 Then
            OrderListView = 2
            Clone_Form.mnuAddName3.Caption = "+ ����¹����ͺ��ͧ"
            Clone_Form.mnuDelImg3.Visible = False
            Clone_Form.mnuAddName3.Visible = True
            Clone_Form.mnuDelName3.Visible = False
            Clone_Form.mnuSetOwner3.Visible = False
            Clone_Form.mnuSetOwnerMail3.Visible = False
            PopupMenu Clone_Form.mnuPopup3, vbPopupMenuLeftAlign, LstV_OwnerUse.Left + X, LstV_OwnerUse.Top + Me.Top + y
     End If
  End If
End If
End Sub

'Private Sub mnuAddName_Click()
'If OrderListView = 1 Then
'            Status_InToFrm = "SG_OWN"
'            Frm_SearchOwnerShip.Show
'End If
'If OrderListView = 2 Then
'    Status_InToFrm = "SG_APY"
'    Frm_SearchOwnerShip.Show
'End If
'End Sub

'Private Sub mnuDelImg_Click()
'If LstV_Img_Name.ListItems.Count > 0 Then
'    LstV_Img_Name.ListItems.Remove (LstV_Img_Name.SelectedItem.Index)
'    picPicture.Cls
'End If
'End Sub

'Private Sub mnuDelName_Click()
'If OrderListView = 1 Then
'        If LstV_OwnerShip.ListItems.Count > 0 Then
'            LstV_OwnerShip.ListItems.Remove (LstV_OwnerShip.SelectedItem.Index)
'        End If
'End If
'End Sub

Private Sub LstV_OwnerShip_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   If LstV_OwnerShip.SortOrder = lvwAscending Then
      LstV_OwnerShip.SortOrder = lvwDescending
   Else
     LstV_OwnerShip.SortOrder = lvwAscending
   End If
    LstV_OwnerShip.SortKey = ColumnHeader.Index - 1
    LstV_OwnerShip.Sorted = True
End Sub

'Private Sub mnuSetOwner_Click()
'With LstV_OwnerShip
'          If .ListItems.Count > 0 Then
'                    Dim i As Byte
'                    For i = 1 To .ListItems.Count
'                            .ListItems.Item(i).SubItems(6) = "�����Է�������"
'                            .ListItems.Item(i).SubItems(7) = "0"
'                    Next i
'                            .ListItems.Item(.SelectedItem.Index).SubItems(6) = "�����Է��줹�á"
'                            .ListItems.Item(.SelectedItem.Index).SubItems(7) = "1"
'        End If
'End With
'End Sub

'Private Sub mnuSetOwnerMail_Click()
'With LstV_OwnerShip
'          If .ListItems.Count > 0 Then
'                    Dim i As Byte
'                    For i = 1 To .ListItems.Count
'                            .ListItems.Item(i).SubItems(8) = ""
'                    Next i
'                            .ListItems.Item(.SelectedItem.Index).SubItems(8) = "����Ѻ������"
'        End If
'End With
'End Sub

Private Sub picParent_DblClick()
Call Btn_FullScreen_Click
End Sub

Private Sub picPicture_DblClick()
        Call Btn_FullScreen_Click
End Sub

Private Sub Txt_Buffer_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 38 Or KeyCode = 40 Then
        Txt_Buffer.Visible = False
        GRID_SIGNBORD.SetFocus
End If
End Sub

Private Sub Txt_Buffer_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
                GRID_SIGNBORD.TextMatrix(GRID_SIGNBORD.Row, GRID_SIGNBORD.Col) = Txt_Buffer.Text
                Txt_Buffer.Visible = False
                        Dim Total As Single, i As Byte
                        Total = 0
                        For i = 1 To GRID_SIGNBORD.Rows - 2
                              If LenB(Trim$(GRID_SIGNBORD.TextMatrix(i, 1))) > 0 Then
                               Total = Total + CDbl(GRID_SIGNBORD.TextMatrix(i, 1))
                              End If
                        Next i
                               Lb_SignBord_TotalTax.Caption = Total
                                GRID_SIGNBORD.SetFocus
End If
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_BUILDING_NO_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_SIGNBORD_HEIGHT_Change()
    If LenB(Trim$(Txt_SIGNBORD_HEIGHT.Text)) = 0 Then Txt_SIGNBORD_HEIGHT.Text = "0"
    Lb_SignBord_SumLand.Caption = CSng(Val(Txt_SIGNBORD_WIDTH.Text)) * CSng(Val(Txt_SIGNBORD_HEIGHT.Text))
End Sub

Private Sub Txt_SIGNBORD_HEIGHT_GotFocus()
Txt_SIGNBORD_HEIGHT.SelStart = 0
Txt_SIGNBORD_HEIGHT.SelLength = Len(Txt_SIGNBORD_HEIGHT.Text)
End Sub

Private Sub Txt_SIGNBORD_HEIGHT_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_SIGNBORD_ID_Change()
        On Error Resume Next
        If Len(Trim$(Txt_SIGNBORD_ID.Text)) = 0 Then
                Txt_SIGNBORD_ID2.Enabled = False
        Else
                Txt_SIGNBORD_ID2.Enabled = True
        End If
End Sub

Private Sub Txt_SIGNBORD_ID_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_SIGNBORD_ID_LostFocus()
   Txt_SIGNBORD_ID.Text = Format$(Txt_SIGNBORD_ID.Text, "###000")
End Sub

Private Sub Txt_SIGNBORD_ID_Validate(Cancel As Boolean)
'        If Status = "ADD" Then
'                Cancel = False
'                If Len(Txt_SIGNBORD_ID.Text) = 3 Then
'                       GBQuerySignBordData.Requery
'                       GBQuerySignBordData.Filter = " ZONE_BLOCK =  '" & Cmb_Zoneblock.Text & "'"
'                        If GBQuerySignBordData.RecordCount > 0 Then
'                                GBQuerySignBordData.MoveFirst
'                                GBQuerySignBordData.Find "SIGNBORD_ID  =  '" & Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text & "'"
'                                If GBQuerySignBordData.AbsolutePosition > 0 Or Chk_ID_IN_Tax(Cmb_Zoneblock.Text & Txt_SIGNBORD_ID.Text, 2) = False Then
'                                      MsgBox "���ҧ���ʻ��«�Ӣ��������  �ô����¹���ʻ������� !", vbCritical, "Warning!"
'                                      Cancel = True ' = �͡�����
'                                Else
'                                      Cancel = False
'                                End If
'                        End If
'                End If
'        End If
End Sub

Private Sub Txt_SIGNBORD_ID2_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then SendKeys "{Tab}"
        KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_SIGNBORD_ID2_LostFocus()
        Txt_SIGNBORD_ID2.Text = Format$(Txt_SIGNBORD_ID2, "000")
End Sub

Private Sub Txt_SIGNBORD_LABEL1_Change()
If LenB(Trim$(Txt_SIGNBORD_LABEL1.Text)) = 0 Then Txt_SIGNBORD_LABEL1.Text = "-"
End Sub

Private Sub Txt_SIGNBORD_LABEL1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

'Private Sub Txt_SIGNBORD_LABEL2_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub
'
'Private Sub Txt_SIGNBORD_LABEL3_KeyPress(KeyAscii As Integer)
'If KeyAscii = 13 Then SendKeys "{Tab}"
'End Sub

Private Sub Txt_SIGNBORD_NAME_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Txt_SignBord_Remark_Change()
If LenB(Trim$(Txt_SignBord_Remark.Text)) = 0 Then Txt_SignBord_Remark.Text = "-"
End Sub

Private Sub Txt_SignBord_Round_Change()
If LenB(Trim$(Txt_SignBord_Round.Text)) = 0 Then Txt_SignBord_Round.Text = "0"
End Sub

Private Sub Txt_SignBord_Round_GotFocus()
Txt_SignBord_Round.SelStart = 0
Txt_SignBord_Round.SelLength = Len(Txt_SignBord_Round.Text)
End Sub

Private Sub Txt_SignBord_Round_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, "0123456789")
End Sub

Private Sub Txt_SignBord_Round_LostFocus()
   Dim CountList As Byte, i As Byte
            CountList = 0
      If CByte(Txt_SignBord_Round.Text) = 0 Then
                LstV_OwnerUse.ListItems.Clear
                Cmb_SignBord_Round.Clear
                Lb_SignBord_TotalTax.Caption = "0.00"
                Call Set_Grid
      Else
        If Cmb_SignBord_Round.ListCount = 0 Then
                  CountList = CByte(Txt_SignBord_Round.Text)
        Else
                 CountList = Abs((CByte(Txt_SignBord_Round.Text)) - ((Cmb_SignBord_Round.ListCount - 1)))
        End If
             If CByte(Txt_SignBord_Round.Text) > (Cmb_SignBord_Round.ListCount - 1) Then '�ó�������ҹ
                        For i = 0 To CountList - 1 ' �������� 0 ����Ѻ Add Record ��ͷ���
                                 Set itmX = LstV_OwnerUse.ListItems.Add()
                                        itmX.SmallIcon = Clone_Form.ImageList1.ListImages(4).Index
                                        itmX.Text = LstV_OwnerUse.ListItems.Count
                                        GRID_SIGNBORD.TextMatrix((GRID_SIGNBORD.Rows - 1), 0) = GRID_SIGNBORD.Rows - 1
                                        GRID_SIGNBORD.Rows = GRID_SIGNBORD.Rows + 1
                       Next i
           End If
                    If CByte(Txt_SignBord_Round.Text) < (Cmb_SignBord_Round.ListCount - 1) Then '�ó�Ŵ��ҹ
                          For i = 1 To CountList
                                            GRID_SIGNBORD.RemoveItem (GRID_SIGNBORD.Rows - 1) - 1
                                            LstV_OwnerUse.ListItems.Remove (LstV_OwnerUse.ListItems.Count)
                          Next i
                    End If
         End If
            If CByte(Txt_SignBord_Round.Text) > 0 Then
                           Cmb_SignBord_Round.Clear
                 For i = 0 To CByte(Txt_SignBord_Round.Text) - 1
                                Cmb_SignBord_Round.AddItem i + 1, i
                 Next i
                                Cmb_SignBord_Round.AddItem "�ء��ҹ"
           End If
End Sub

Private Sub Txt_SIGNBORD_WIDTH_Change()
If LenB(Trim$(Txt_SIGNBORD_WIDTH.Text)) = 0 Then Txt_SIGNBORD_WIDTH.Text = "0"
Lb_SignBord_SumLand.Caption = CSng(Val(Txt_SIGNBORD_WIDTH.Text)) * CSng(Val(Txt_SIGNBORD_HEIGHT.Text))
End Sub
Private Sub LstV_OwnerShip_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If Button = vbRightButton Then
   If Btn_Add.Enabled = False Or Btn_Edit.Enabled = False Then
            OrderListView = 1
            Clone_Form.mnuAddName3.Caption = "+ ������ª���"
            Clone_Form.mnuDelImg3.Visible = False
            Clone_Form.mnuAddName3.Visible = True
            Clone_Form.mnuDelName3.Visible = True
            Clone_Form.mnuSetOwner3.Visible = True
            Clone_Form.mnuSetOwnerMail3.Visible = True
        PopupMenu Clone_Form.mnuPopup3, vbPopupMenuLeftAlign, LstV_OwnerShip.Left + X, LstV_OwnerShip.Top + Me.Top + y
   End If
   End If
End Sub

Private Sub Txt_SIGNBORD_WIDTH_GotFocus()
Txt_SIGNBORD_WIDTH.SelStart = 0
Txt_SIGNBORD_WIDTH.SelLength = Len(Txt_SIGNBORD_WIDTH.Text)
End Sub

Private Sub Txt_SIGNBORD_WIDTH_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then SendKeys "{Tab}"
   KeyAscii = KeyCharecter(KeyAscii, ".0123456789")
End Sub

Private Sub Txt_StoryRemark_Change()
If LenB(Trim$(Txt_StoryRemark.Text)) = 0 Then Txt_StoryRemark.Text = "-"
End Sub
