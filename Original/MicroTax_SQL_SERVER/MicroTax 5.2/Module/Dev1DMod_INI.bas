Attribute VB_Name = "Dev1DMod_INI"
Option Explicit

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Public iniProvince As String, iniProvince_Id As String, iniAmphoe As String, iniAmphoe_Id As String, iniTambon As String, iniTambon_Id As String, iniPrint_Perform As String, iniPrint_Accept As String, iniPrint_Payment As String
Public iniSection As String, iniAdd, iniTel As String, iniName As String, iniPosition As String
Public iniZone As Byte, iniLand As Byte, iniSignbord As Byte, iniBuliding As Byte
Public ini_landtaxaccept As Byte, ini_landtaxpay As Byte, ini_signbordtaxaccept As Byte, ini_signbordtaxpay As Byte, ini_buildingtaxaccept As Byte, ini_buildingtaxpay As Byte
Public iniPath_Picture As String, iniPath_Map As String, ini_Database  As String, ini_Username As String, ini_Password As String, ini_Servername As String
Public iniAutoRep As Byte, iniAutoTime As Byte
Public iniPng As String, iniPermit As String

Public Function ReadINI(strSection As String, strKey As String) As String
   Dim strbuffer As String
   Let strbuffer$ = String$(750, Chr$(0&))
   Let ReadINI$ = Left$(strbuffer$, GetPrivateProfileString(strSection$, ByVal LCase$(strKey$), "", strbuffer, Len(strbuffer), App.Path & "\Configs.ini"))
End Function

Public Sub WriteINI(strSection As String, strKey As String, strkeyvalue As String)
    Call WritePrivateProfileString(strSection$, UCase$(strKey$), strkeyvalue$, App.Path & "\Configs.ini")
End Sub

Public Sub SetPathFile()
            Dim strSection As String, strKey As String, strkeyvalue As String
            
            On Error GoTo ErrSetPath
            
            strkeyvalue = ""
            ini_Servername = ReadINI("path file", "servername") ' SQL Server name
            ini_Database = ReadINI("path file", "database")          ' Database name in SQL Server
            ini_Username = ReadINI("path file", "username")        ' Login user name
'            ini_Password = ReadINI("path file", "password")        ' Password
            
            ini_Password = GetRegis
            
            iniPath_Map = ReadINI("path file", "pathmap")
            iniPath_Picture = ReadINI("path file", "pathpicture")
'            If LenB(Trim$(iniPath_Database)) = 0 Then
'               iniPath_Database = App.Path & "\database\MicroTax.mdb"
'            End If
            If LenB(Trim$(iniPath_Picture)) = 0 Then
                    iniPath_Picture = App.Path & "\picture"
            End If
            If LenB(Trim$(iniPath_Map)) = 0 Then
                    iniPath_Map = App.Path & "\map\NewMap\Map.swd"
            End If
            On Error GoTo ErrSetPath2
            strSection = "other": strKey = "autoreport": strkeyvalue = 1
            iniAutoRep = ReadINI("other", "autoreport")
            strSection = "other": strKey = "autotime": strkeyvalue = 1
            iniAutoTime = ReadINI("other", "autotime")
            strSection = "other": strKey = "post": strkeyvalue = ""
            iniPng = ReadINI("other", "Post")
            strSection = "other": strKey = "permit": strkeyvalue = ""
            iniPermit = ReadINI("other", "Permit")
            strSection = "printing": strKey = "print_Perform": strkeyvalue = 0
            iniPrint_Perform = ReadINI("printing", "print_Perform")
            strSection = "printing": strKey = "print_Accept": strkeyvalue = 0
            iniPrint_Accept = ReadINI("printing", "print_Accept")
            strSection = "printing": strKey = "print_Payment": strkeyvalue = 0
            iniPrint_Payment = ReadINI("printing", "print_Payment")
            strSection = "register": strKey = "tambon": strkeyvalue = "�ѧ�ͧ��ҧ"
            iniTambon = ReadINI("register", "tambon")
            strSection = "register": strKey = "tambon_id": strkeyvalue = "100602"
            iniTambon_Id = ReadINI("register", "tambon_id")
            strSection = "register": strKey = "amphoe": strkeyvalue = "�ѧ�ͧ��ҧ"
            iniAmphoe = ReadINI("register", "amphoe")
            strSection = "register": strKey = "autoreport": strkeyvalue = "1006"
            iniAmphoe_Id = ReadINI("register", "amphoe_id")
            strSection = "register": strKey = "autoreport": strkeyvalue = "��ا෾��ҹ��"
            iniProvince = ReadINI("register", "province")
            strSection = "register": strKey = "province_id": strkeyvalue = "10"
            iniProvince_Id = ReadINI("register", "province_id")
            
            strSection = "register owner": strKey = "section": strkeyvalue = "xxxxxxxxxx"
            iniSection = ReadINI("register owner", "section")
            strSection = "register owner": strKey = "telephone": strkeyvalue = "xxx-xxxxxxx"
            iniTel = ReadINI("register owner", "telephone")
            strSection = "register owner": strKey = "name": strkeyvalue = "��������"
            iniName = ReadINI("register owner", "name")
            strSection = "register owner": strKey = "position": strkeyvalue = "����ӹ�¡�áͧ��ѧ"
            iniPosition = ReadINI("register owner", "position")
            strSection = "register owner": strKey = "address": strkeyvalue = "xxxxx"
            iniAdd = ReadINI("register owner", "address")
            
            strSection = "index theme": strKey = "landtaxaccept": strkeyvalue = 1
            ini_landtaxaccept = ReadINI("index theme", "landtaxaccept")
            strSection = "index theme": strKey = "landtaxpay": strkeyvalue = 1
            ini_landtaxpay = ReadINI("index theme", "landtaxpay")
            strSection = "index theme": strKey = "buildingtaxaccept": strkeyvalue = 1
            ini_buildingtaxaccept = ReadINI("index theme", "buildingtaxaccept")
            strSection = "index theme": strKey = "buildingtaxpay": strkeyvalue = 1
            ini_buildingtaxpay = ReadINI("index theme", "buildingtaxpay")
            strSection = "index theme": strKey = "signbordtaxaccept": strkeyvalue = 1
            ini_signbordtaxaccept = ReadINI("index theme", "signbordtaxaccept")
            strSection = "index theme": strKey = "signbordtaxpay": strkeyvalue = 1
            ini_signbordtaxpay = ReadINI("index theme", "signbordtaxpay")
            
            strSection = "index overlay": strKey = "zone": strkeyvalue = 1
            iniZone = Abs(ReadINI("index overlay", "zone"))
            strSection = "index overlay": strKey = "land": strkeyvalue = 2
            iniLand = Abs(ReadINI("index overlay", "land"))
            strSection = "index overlay": strKey = "building": strkeyvalue = 3
            iniBuliding = Abs(ReadINI("index overlay", "building"))
            strSection = "index overlay": strKey = "signboard": strkeyvalue = 4
            iniSignbord = Abs(ReadINI("index overlay", "signboard"))
            Exit Sub
ErrSetPath:
            MsgBox Err.Description
            Exit Sub
ErrSetPath2:
            WriteINI strSection, strKey, strkeyvalue
            Resume Next
End Sub
