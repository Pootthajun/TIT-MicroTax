VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   ":: MicroTax Administrator Utility ::"
   ClientHeight    =   7620
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8940
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "Form1.frx":06EA
   ScaleHeight     =   508
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   596
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_Show_PW 
      Height          =   315
      Left            =   8100
      Picture         =   "Form1.frx":E3D1
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2460
      Width           =   315
   End
   Begin VB.TextBox Txt_Sure 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   5055
      Locked          =   -1  'True
      MaxLength       =   14
      PasswordChar    =   "�"
      TabIndex        =   5
      Top             =   3210
      Width           =   2985
   End
   Begin VB.TextBox Txt_Pw 
      BackColor       =   &H00EBEBE7&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   5055
      Locked          =   -1  'True
      MaxLength       =   14
      PasswordChar    =   "�"
      TabIndex        =   4
      Top             =   2820
      Width           =   2985
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00EBEBE7&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Form1.frx":E95B
      Left            =   5055
      List            =   "Form1.frx":E965
      Style           =   2  'Dropdown List
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   3585
      Width           =   1275
   End
   Begin VB.TextBox Txt_User 
      BackColor       =   &H00EBEBE7&
      Height          =   330
      Left            =   5055
      Locked          =   -1  'True
      MaxLength       =   14
      TabIndex        =   0
      Top             =   1380
      Width           =   2985
   End
   Begin VB.TextBox Txt_UserName 
      BackColor       =   &H00EBEBE7&
      Height          =   315
      Left            =   5055
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1770
      Width           =   2985
   End
   Begin VB.TextBox Txt_Pw_Old 
      BackColor       =   &H00EBEBE7&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   5055
      Locked          =   -1  'True
      MaxLength       =   14
      PasswordChar    =   "�"
      TabIndex        =   3
      Top             =   2430
      Width           =   2985
   End
   Begin VB.CheckBox Check1 
      Caption         =   "����¹�ŧ���ʼ�ҹ"
      Enabled         =   0   'False
      Height          =   195
      Left            =   5055
      TabIndex        =   2
      Top             =   2160
      Width           =   1785
   End
   Begin VB.CommandButton Btn_Add 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Form1.frx":E973
      Height          =   405
      Left            =   3240
      Picture         =   "Form1.frx":F1DC
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "����������"
      Top             =   750
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Del 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      CausesValidation=   0   'False
      DisabledPicture =   "Form1.frx":119D4
      Height          =   405
      Left            =   5520
      Picture         =   "Form1.frx":12202
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "ź������"
      Top             =   750
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Form1.frx":14A8C
      Enabled         =   0   'False
      Height          =   405
      Left            =   7800
      Picture         =   "Form1.frx":1533E
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "¡��ԡ������"
      Top             =   750
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Post 
      Appearance      =   0  'Flat
      BackColor       =   &H0045C1FE&
      DisabledPicture =   "Form1.frx":17D79
      Enabled         =   0   'False
      Height          =   405
      Left            =   6660
      Picture         =   "Form1.frx":185DC
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "�ѹ�֡������"
      Top             =   750
      Width           =   1140
   End
   Begin VB.CommandButton Btn_Edit 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      CausesValidation=   0   'False
      DisabledPicture =   "Form1.frx":1B001
      Height          =   405
      Left            =   4380
      Picture         =   "Form1.frx":1B898
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "��䢢�����"
      Top             =   750
      Width           =   1140
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4140
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form1.frx":1E17D
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form1.frx":1E717
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form1.frx":1ECB1
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form1.frx":1F24B
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   6855
      Left            =   0
      TabIndex        =   8
      Top             =   750
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   12091
      _Version        =   393217
      Style           =   7
      Checkboxes      =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      Enabled         =   0   'False
   End
   Begin MSComctlLib.ListView LstV_UserPassword 
      Height          =   3465
      Left            =   3270
      TabIndex        =   7
      Top             =   4140
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   6112
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "�����ҹ"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ʶҹ�"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "����-���ʡ��"
         Object.Width           =   4764
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ʶҹ� :"
      ForeColor       =   &H00000000&
      Height          =   195
      Index           =   0
      Left            =   4410
      TabIndex        =   19
      Top             =   3660
      Width           =   555
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�׹�ѹ���ʼ�ҹ :"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3840
      TabIndex        =   18
      Top             =   3315
      Width           =   1125
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʼ�ҹ :"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4290
      TabIndex        =   17
      Top             =   2925
      Width           =   675
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�����ҹ :"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4305
      TabIndex        =   16
      Top             =   1470
      Width           =   660
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "����-���ʡ�� :"
      ForeColor       =   &H00000000&
      Height          =   195
      Index           =   1
      Left            =   4050
      TabIndex        =   15
      Top             =   1830
      Width           =   915
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "���ʼ�ҹ��� :"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4035
      TabIndex        =   14
      Top             =   2535
      Width           =   930
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00000000&
      Height          =   2955
      Left            =   3270
      Top             =   1170
      Width           =   5655
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Query As ADODB.Recordset
Dim Query_user As ADODB.Recordset
Dim itmX As ListItem
Dim Status  As String

Private Sub Btn_Add_Click()
'        SSTab1.Tab = 0
        SET_TEXTBOX ("ADD")
        Label3.Top = Label3.Top - 50: Txt_Pw.Top = Txt_Pw.Top - 50
        Label4.Top = Label4.Top - 50: Txt_Sure.Top = Txt_Sure.Top - 50
        Label2(0).Top = Label2(0).Top - 50: Combo1.Top = Combo1.Top - 50
        Check1.Visible = False
        Label11.Visible = False
        Txt_Pw_Old.Visible = False
        cmd_Show_PW.Visible = False
        Txt_User.SetFocus
        Status = "ADD"
End Sub

Private Sub SET_TEXTBOX(state As String)
        If state = "ADD" Or state = "EDIT" Then   'Manage Button
                Btn_Add.Enabled = False
                Btn_Edit.Enabled = False
                Btn_Del.Enabled = False
                Btn_Post.Enabled = True
                Btn_Cancel.Enabled = True
                Combo1.Enabled = True
                If state = "ADD" Then
                        Txt_User.Text = "": Txt_User.BackColor = &H80000005: Txt_User.Locked = False
                        Txt_Pw.Text = "": Txt_Pw.BackColor = &H80000005: Txt_Pw.Locked = False
                        Txt_Sure.Text = "": Txt_Sure.BackColor = &H80000005: Txt_Sure.Locked = False
                End If
                If state = "ADD" Then
                        Txt_UserName.Text = ""
                Else
                        Txt_UserName.SelStart = 0
                        Txt_UserName.SelLength = Len(Txt_UserName.Text)
                        Txt_UserName.SetFocus
                End If
                Combo1.BackColor = &H80000005
                Txt_UserName.BackColor = &H80000005: Txt_UserName.Locked = False
                LstV_UserPassword.Enabled = False
                TreeView1.Enabled = True
        ElseIf state = "CANCEL" Or state = "POST" Then 'Manage Button
                If Status = "ADD" Then
                        Label3.Top = Label3.Top + 50: Txt_Pw.Top = Txt_Pw.Top + 50
                        Label4.Top = Label4.Top + 50: Txt_Sure.Top = Txt_Sure.Top + 50
                        Label2(0).Top = Label2(0).Top + 50: Combo1.Top = Combo1.Top + 50
                End If
                Btn_Add.Enabled = True
                Btn_Edit.Enabled = True
                Btn_Del.Enabled = True
                Btn_Post.Enabled = False
                Btn_Cancel.Enabled = False
                Combo1.Enabled = False
                Txt_User.Enabled = True
                Txt_User.Text = "": Txt_User.BackColor = &HEBEBE7: Txt_User.Locked = True
                Txt_Pw.Text = "": Txt_Pw.BackColor = &HEBEBE7: Txt_Pw.Locked = True
                Txt_Sure.Text = "": Txt_Sure.BackColor = &HEBEBE7: Txt_Sure.Locked = True
                Txt_UserName.Text = "": Txt_UserName.BackColor = &HEBEBE7: Txt_UserName.Locked = True
                Txt_Pw_Old.Text = "": Txt_Pw_Old.Enabled = False: Txt_Pw_Old.BackColor = &HEBEBE7: Txt_Pw_Old.Locked = True
                Check1.Visible = True
                Check1.Enabled = False
                Label11.Visible = True
                Txt_Pw_Old.Visible = True
                cmd_Show_PW.Visible = True
                Combo1.BackColor = &HEBEBE7
                LstV_UserPassword.Enabled = True
                TreeView1.Enabled = False
        End If
End Sub

Private Sub Btn_Cancel_Click()
        Check1.Value = Unchecked
        Check1.Enabled = False
        cmd_Show_PW.Enabled = False
        SET_TEXTBOX ("CANCEL")
End Sub

Private Sub Btn_Del_Click()
        On Error GoTo ErrDel
        Globle_Connective.BeginTrans
        If LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(1) = "ADMIN" Then
                MsgBox "�������öź��", vbOKOnly, "����͹"
                Exit Sub
        End If
        If MsgBox("��ͧ���ź�����ҹ : " & LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).Text & " : ��ԧ������� ?", vbInformation + vbYesNo, "�׹�ѹ���ź�����ҹ�͡�ҡ�к�") = vbNo Then
                Globle_Connective.RollbackTrans
                Exit Sub
        End If
                Call SET_Execute("DELETE FROM User_Password WHERE User_Login = '" & LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).Text & "'")
                Globle_Connective.CommitTrans
                Txt_User.Text = ""
                Txt_UserName.Text = ""
                Call SetRefresh
        Exit Sub
ErrDel:
        Globle_Connective.RollbackTrans
End Sub

Private Sub Btn_Edit_Click()
        If LenB(Trim$(Txt_User.Text)) > 0 Then
                SET_TEXTBOX ("EDIT")
                Txt_User.Enabled = False
                Txt_UserName.SetFocus
                Check1.Value = Unchecked
                Check1.Enabled = True
                cmd_Show_PW.Enabled = True
                If Txt_User.Tag = "ADMIN" Then
                        Combo1.Visible = False
                        Label2(0).Visible = False
                Else
                        Combo1.Visible = True
                        Label2(0).Visible = True
                End If
                Status = "EDIT"
        End If
End Sub

Private Sub Btn_Post_Click()
        On Error GoTo ErrPost
        Dim i As Byte
        Globle_Connective.BeginTrans
        If LenB(Trim$(Txt_UserName.Text)) = 0 Then
              MsgBox "�кء�͡������й��ʡ�� �����ҹ", vbOKOnly, "����͹ !"
              Globle_Connective.RollbackTrans
              Exit Sub
        End If
        If Status = "ADD" Then
                If LenB(Trim$(Txt_User.Text)) = 0 Then
                        MsgBox "��س������ͼ������� !!!", vbOKOnly, "����͹ !"
                        Globle_Connective.RollbackTrans
                        Txt_User.SetFocus
                        Exit Sub
                End If
                If Trim$(Txt_Pw.Text) <> Trim$(Txt_Sure.Text) Then
                        MsgBox "���ʼ�ҹ���ç�ѹ !!!", vbOKOnly, "����͹ !"
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
                Call SET_QUERY("SELECT * FROM USER_PASSWORD WHERE USER_LOGIN = '" & Trim$(Txt_User.Text) & "'", Query)
                If Query.RecordCount = 0 Then
                        SET_Execute ("INSERT INTO USER_PASSWORD ( USER_LOGIN , USER_PASSWORD , USER_STATUS , USER_NAME ) " & _
                                                " VALUES ( '" & Txt_User.Text & "' , '" & Enzinc_Password(Txt_Pw.Text, "ENCODE") & "','" & Combo1.Text & "','" & Trim$(Txt_UserName.Text) & "')")
                        For i = 1 To TreeView1.Nodes.Count
                                If Not TreeView1.Nodes(i).Parent Is Nothing Then
                                        Label1.Tag = TreeView1.Nodes(TreeView1.Nodes(i).Parent.Index).Key
                                Else
                                        Label1.Tag = "FIRST"
                                End If
                                
                                SET_Execute ("INSERT INTO MENU_SETTING (USER_LOGIN,RELETIVE_KEY,KEY_ID,MENU_FLAG) " & _
                                            " VALUES ('" & Txt_User.Text & "','" & Label1.Tag & "','" & TreeView1.Nodes(i).Key & "','" & TreeView1.Nodes(i).Checked & "')")
                        Next i
'                        Clone_Form.StatusBar1.Panels(1).Text = "Status : " & Combo1.Text
'                        Clone_Form.StatusBar1.Panels(2).Text = "User Login : " & Trim$(Txt_User.Text)
'                        Status = Combo1.Text
                Else
                        MsgBox "���ͼ��������������к����� �ô����¹���� !", vbOKOnly, "����͹ !"
                        Globle_Connective.RollbackTrans
                        Exit Sub
                End If
        ElseIf Status = "EDIT" Then
                If Txt_User.Tag = "ADMIN" Then
                        Call SET_Execute(" UPDATE USER_PASSWORD SET  USER_NAME = '" & Trim$(Txt_UserName.Text) & "',User_Status = 'ADMIN'" & _
                                                            "WHERE USER_LOGIN = '" & Txt_User.Text & "'")
                Else
                        Call SET_Execute(" UPDATE USER_PASSWORD SET  USER_NAME = '" & Trim$(Txt_UserName.Text) & "',User_Status = '" & Combo1.Text & "'" & _
                                                            "WHERE USER_LOGIN = '" & Txt_User.Text & "'")
                End If
                For i = 1 To TreeView1.Nodes.Count
                        Call SET_Execute("UPDATE MENU_SETTING SET MENU_FLAG = '" & TreeView1.Nodes(i).Checked & "' WHERE USER_LOGIN='" & Txt_User.Text & "' AND KEY_ID='" & TreeView1.Nodes(i).Key & "'")
                Next i
                If Check1.Value = Checked Then
                        Dim Buffer_Pw As String
                        Buffer_Pw = Enzinc_Password(Trim$(Txt_Pw_Old.Text), "ENCODE")
                        Call SET_QUERY("SELECT  *  FROM  USER_PASSWORD WHERE User_Login = '" & Trim(Txt_User.Text) & "' And User_Password  =   '" & Buffer_Pw & "'", Query)
                        If Query.RecordCount = 0 Then
                                MsgBox "���ʼ�ҹ������١��ͧ !", vbOKOnly, "����͹ !"
                                Globle_Connective.RollbackTrans
                                Exit Sub
                        End If
                        If Trim$(Txt_Pw.Text) <> Trim$(Txt_Sure.Text) Then
                                MsgBox "���ʼ�ҹ���ç�ѹ !!!", vbOKOnly, "����͹ !"
                                Globle_Connective.RollbackTrans
                                Exit Sub
                        End If
                        Call SET_Execute(" UPDATE USER_PASSWORD SET  USER_PASSWORD = '" & Enzinc_Password(Txt_Pw.Text, "ENCODE") & "' , USER_NAME = '" & Txt_UserName.Text & "'" & _
                                                            "WHERE USER_LOGIN = '" & Txt_User.Text & "' And  USER_PASSWORD ='" & Buffer_Pw & "'")
                End If
        End If
        SET_TEXTBOX ("POST")
        Globle_Connective.CommitTrans
        MsgBox "�ӡ�úѹ�֡�����������", vbOKOnly, "�׹�ѹ !"
        Call SetRefresh
        Exit Sub
ErrPost:
        Globle_Connective.RollbackTrans
        LstV_UserPassword.Enabled = True
        Call SET_TEXTBOX("CANCEL")
'       Debug.Print Err.Description
End Sub

Private Sub Check1_Click()
        If Check1.Value = Checked Then
                Txt_Pw_Old.Enabled = True
                Txt_Pw_Old.Locked = False: Txt_Pw_Old.BackColor = &H80000005
                Txt_Pw.Locked = False: Txt_Pw.BackColor = &H80000005
                Txt_Sure.Locked = False: Txt_Sure.BackColor = &H80000005
                Call SET_QUERY("SELECT  User_Password  FROM  USER_PASSWORD WHERE User_Login = '" & Trim(Txt_User.Text) & "'", Query)
                If Query.RecordCount > 0 Then
                       Txt_Pw_Old.Text = Enzinc_Password(Query.Fields(0).Value, "DECODE")
                End If
                Txt_Pw.SetFocus
        Else
                Txt_Pw_Old.Enabled = False
                Txt_Pw_Old.Text = "": Txt_Pw_Old.Locked = True: Txt_Pw_Old.BackColor = &HEBEBE7
                Txt_Pw.Text = "":  Txt_Pw.Locked = True: Txt_Pw.BackColor = &HEBEBE7
                Txt_Sure.Text = "": Txt_Sure.Locked = True: Txt_Sure.BackColor = &HEBEBE7
        End If
End Sub

Private Sub cmd_Show_PW_Click()
        On Error GoTo Err_Show
        Call SET_QUERY("SELECT  User_Password  FROM  USER_PASSWORD WHERE User_Login = '" & Trim(Txt_User.Text) & "'", Query)
        If Query.RecordCount > 0 Then
'               Txt_Pw_Old.Text = Enzinc_Password(Query.Fields(0).Value, "DECODE")
                MsgBox Enzinc_Password(Query.Fields(0).Value, "DECODE"), vbOKOnly, "���ʼ�ҹ"
        End If
        Exit Sub
Err_Show:
        MsgBox Err.Description
End Sub

Private Sub Form_Load()
        On Error GoTo ErrLoad
        
        Dim nodX As Node ' Create a tree.
        Set Query = New ADODB.Recordset
        Set Query_user = New ADODB.Recordset
        Set Command_Exec = New ADODB.Command
        Call SET_QUERY("SELECT * FROM User_Password Order by User_name ", Query_user)
        LstV_UserPassword.Icons = ImageList1
        LstV_UserPassword.SmallIcons = ImageList1
'        Set nodX = TreeView1.Nodes.Add(, , "Root", "����", 1)
        Set nodX = TreeView1.Nodes.Add(, , "M0001", "�����ž�鹰ҹ", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0002", "����¹��Ѿ���Թ", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0003", "����ѵԷ���¹��Ѿ���Թ", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0004", "���Ẻ�����Թ", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0005", "�ͺ���������", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0006", "��§ҹ��ػ", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0007", "�к��Ǻ���", 3, 3)
        Set nodX = TreeView1.Nodes.Add(, , "M0008", "�к���������", 3, 3)
'        Set nodX = TreeView1.Nodes.Add(1, tvwChild, "M0011", "�Ը���", 3, 2)
        
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-01", "�Ӻ�/�����ҹ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-02", "���/���", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-03", "ࢵ/ࢵ����(Zone/Block)", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-04", "�ѵ������(���ا��ͧ���)", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-05", "�ѵ������(�ç���͹��з��Թ)", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-06", "�ѵ������(����)", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-07", "����������ѡɳСԨ���", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-08", "���͹���Ф�һ�Ѻ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-09", "�������¹�ŧ��Ѿ���Թ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0001", tvwChild, "M0001-10", "����������", 3, 3)

        Set nodX = TreeView1.Nodes.Add("M0002", tvwChild, "M0002-01", "����ա����Է���", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0002-01", tvwChild, "M0002-01-01", "����,���,ź", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0002", tvwChild, "M0002-02", "����¹���Թ", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0002", tvwChild, "M0002-03", "����¹�ç���͹", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0002", tvwChild, "M0002-04", "����¹����", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0002", tvwChild, "M0002-05", "����¹�͹حҵ", 3, 3)
        
'        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-01", "����¹��Ѿ���Թ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-02", "����¹���Թ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-03", "����¹�ç���͹", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-04", "����¹����", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-05", "����¹�͹حҵ", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-06", "Ẻ���Ǩ��з���¹", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-07", "Ẻ��§ҹ��Ѻ������", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-08", "����ѵԡ�ê�������", 3, 3)
        Set nodX = TreeView1.Nodes.Add("M0005", tvwChild, "M0005-09", "����ѵԡ�ê�������Ẻ�Ǵ", 3, 3)
        
'        Set nodX = TreeView1.Nodes.Add("M0003", tvwChild, "M0003-01", "����ѵԷ���¹���Թ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0003", tvwChild, "M0003-02", "����ѵԷ���¹�ç���͹��з��Թ", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0003", tvwChild, "M0003-03", "����ѵԷ���¹����", 3, 3)
'        Set nodX = TreeView1.Nodes.Add("M0003", tvwChild, "M0003-04", "����ѵԷ���¹�͹حҵ", 3, 3)
'        TreeView1.Nodes(TreeView1.Nodes.Count).EnsureVisible
'        TreeView1.Nodes(1).EnsureVisible = True
        TreeView1.LabelEdit = tvwManual
        Dim i As Integer
        For i = 1 To TreeView1.Nodes.Count
           ' Expand all nodes.
           TreeView1.Nodes(i).Expanded = True
        Next i
        Set nodX = Nothing
        Combo1.ListIndex = 0
        Call SetRefresh
        Call SubClass(hWnd)
        Exit Sub
ErrLoad:
        MsgBox Err.Description
End Sub

Private Sub SetRefresh()
        Query_user.Requery
        LstV_UserPassword.ListItems.Clear
        With Query_user
                If .RecordCount > 0 Then
                        .MoveFirst
                        Do While Not .EOF
                                Set itmX = LstV_UserPassword.ListItems.Add()
                                itmX.SmallIcon = ImageList1.ListImages(4).Index
                                itmX.Text = .Fields("User_Login").Value
                                itmX.SubItems(1) = .Fields("User_Status").Value
                                itmX.SubItems(2) = .Fields("User_Name").Value
                                .MoveNext
                        Loop
                End If
        End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Call UnSubClass(hWnd)
End Sub

Private Sub LstV_UserPassword_ItemClick(ByVal item As MSComctlLib.ListItem)
        Dim i As Integer
        Call SET_QUERY("SELECT B.* FROM USER_PASSWORD A,MENU_SETTING B WHERE A.User_login=B.User_login AND A.User_login = '" & item.Text & "'", Query)
        If Query.RecordCount > 0 Then
                For i = 1 To TreeView1.Nodes.Count
                        Query.Filter = " KEY_ID='" & TreeView1.Nodes(i).Key & "'"
                        If Query.RecordCount > 0 Then
                                TreeView1.Nodes(i).Checked = Query.Fields("MENU_FLAG").Value
                        End If
                        Query.Filter = 0
                 Next i
         End If
        Txt_User.Text = LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).Text
        Txt_User.Tag = LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(1)
        Txt_UserName.Text = LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(2)
        If LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(1) = "ADMIN" Then
'                Combo1.Enabled = True
'                Combo1.Text = LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(1)
'                Combo1.Enabled = False
        Else
                Combo1.Text = LstV_UserPassword.ListItems.item(LstV_UserPassword.SelectedItem.Index).SubItems(1)
        End If
'         MsgBox Item.Text
End Sub

Private Function IsIDE() As Boolean
  On Error GoTo Out
  Debug.Print 1 / 0
Out:
  IsIDE = Err
End Function

' The form's notification message handler.

Public Function NotificationHandler(wParam As Long, lParam As Long) As Long
  Dim nmh As NMHDR
  Dim nmtv As NMTREEVIEW
  Dim nod As Node

  ' Fill the NMHDR struct from the lParam pointer.
  ' (for any WM_NOTIFY msg, lParam always points to a struct which is
  '  either the NMHDR struct, or whose 1st member is the NMHDR struct)
  MoveMemory nmh, ByVal lParam, Len(nmh)

#If MSGLOG Then
' Uncomment this to see all the treeview's notification messages
' in the Immediate window.
'  WriteMsgLog GetCtrlNotifyCodeStr(nmh.code)
#End If
  
  ' The actual notification message is in the code member
  ' of the NMHDR struct.
  Select Case nmh.code
    
    ' An item is about to be expanded or collapsed.
    ' Return TRUE to prevent the item from expanding or collapsing.
    ' One little problem here... VB superclasses the treeview and does
    ' mysterious things when the Entry key is pressed, preforming the
    ' expand/collapse before a TVN_ITEMEXPANDING or any other
    ' message is sent. Go figure. I never did find out how to trap it either...
    Case TVN_ITEMEXPANDING

      ' lParam points to the NMTREEVIEW struct, fill it.
      MoveMemory nmtv, ByVal lParam, Len(nmtv)
      
      ' If an item is expanding and expands are being prevented
      If (nmtv.action = TVE_EXPAND) Then
        ' Get the Node object for the item that is about to be expanded.
        Set nod = GetNodeFromTVItem(TreeView1, nmtv.itemNew.hItem)
'        Label1 = nod.Text & " wasn't expanded"
        ' Return 1 to cancel the expand
        NotificationHandler = CTrue
      End If
      
      ' If an item is collapsing and collapses are being prevented...
      If (nmtv.action = TVE_COLLAPSE) Then
        ' Get the Node object for the item that is about to be collapsed.
        Set nod = GetNodeFromTVItem(TreeView1, nmtv.itemNew.hItem)
'        Label1 = nod.Text & " wasn't collapsed"
        ' Return 1 to cancel the collapse
        NotificationHandler = CTrue
      End If
    
    ' The current selection is about to change.
    ' Return TRUE to prevent the selection from changing.
    Case TVN_SELCHANGING

      ' lParam points to the NMTREEVIEW struct, fill it.
      MoveMemory nmtv, ByVal lParam, Len(nmtv)

      ' If we're not allowing the slection to change...
      If chkNoSelection Then
        ' If nmtv.itemNew.hItem  has a value, then the item is being selected.
        If nmtv.itemNew.hItem Then
          ' Get the Node object for the item that is about to become selected.
          Set nod = GetNodeFromTVItem(TreeView1, nmtv.itemNew.hItem)
          Label1 = nod.Text & " wasn't selected"
          ' Return 1 to cancel the selection
          NotificationHandler = CTrue
        
        ' If nmtv.itemNew.hItem = 0, then nmtv.itemOld.hItem is being de-selected.
        Else
          ' Get the Node object for the item that is about to become deselected.
          Set nod = GetNodeFromTVItem(TreeView1, nmtv.itemOld.hItem)
          Label1 = nod.Text & " wasn't deselected"
          ' Return 1 to cancel the de-selection
          NotificationHandler = CTrue
        
        End If
      End If
      
  End Select

End Function

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)
'   If Not Node.Parent Is Nothing Then
'      MsgBox TreeView1.Nodes(Node.Parent.Index).Key
'   End If
End Sub
