VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmUpdate_LandID 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Microtax Regenerate Identifier"
   ClientHeight    =   6285
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9015
   Icon            =   "frmUpdate_LandID.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "frmUpdate_LandID.frx":151A
   ScaleHeight     =   6285
   ScaleWidth      =   9015
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   2460
      TabIndex        =   34
      Top             =   1440
      Width           =   6555
      Begin VB.Label lbFullName 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Service Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00B99D7F&
         Height          =   300
         Left            =   2340
         TabIndex        =   35
         Top             =   0
         Width           =   1665
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4185
      Left            =   2460
      TabIndex        =   7
      Top             =   1440
      Width           =   6555
      _ExtentX        =   11562
      _ExtentY        =   7382
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BackColor       =   12164479
      TabCaption(0)   =   "���Թ"
      TabPicture(0)   =   "frmUpdate_LandID.frx":DC5E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Grid_LandData(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Text1(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "�ç���͹"
      TabPicture(1)   =   "frmUpdate_LandID.frx":DC7A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Grid_LandData(1)"
      Tab(1).Control(1)=   "Frame1(1)"
      Tab(1).Control(2)=   "Text1(7)"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "����"
      TabPicture(2)   =   "frmUpdate_LandID.frx":DC96
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Grid_LandData(2)"
      Tab(2).Control(1)=   "Text1(8)"
      Tab(2).Control(2)=   "Frame1(2)"
      Tab(2).ControlCount=   3
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "��� SIGNBOARD_ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Index           =   2
         Left            =   -74970
         TabIndex        =   27
         Top             =   2100
         Width           =   3285
         Begin VB.CommandButton cmdUpdate 
            BackColor       =   &H00B99D7F&
            Caption         =   "&Update"
            Height          =   345
            Index           =   2
            Left            =   2340
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   1590
            Width           =   855
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   11
            Left            =   1980
            MaxLength       =   6
            TabIndex        =   29
            Top             =   900
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   10
            Left            =   1980
            MaxLength       =   10
            TabIndex        =   28
            Top             =   510
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "SIGNBOARD_ID ���� :"
            Height          =   195
            Index           =   5
            Left            =   300
            TabIndex        =   32
            Top             =   960
            Width           =   1650
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "SIGNBOARD_ID ��� :"
            Height          =   195
            Index           =   4
            Left            =   330
            TabIndex        =   31
            Top             =   570
            Width           =   1605
         End
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   2025
         Index           =   8
         Left            =   -71670
         MultiLine       =   -1  'True
         TabIndex        =   26
         Top             =   2100
         Width           =   3165
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   2025
         Index           =   7
         Left            =   -71670
         MultiLine       =   -1  'True
         TabIndex        =   24
         Top             =   2100
         Width           =   3165
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "��� BUILDING_ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Index           =   1
         Left            =   -74970
         TabIndex        =   18
         Top             =   2100
         Width           =   3285
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   5
            Left            =   1980
            MaxLength       =   6
            TabIndex        =   21
            Top             =   900
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   4
            Left            =   1980
            MaxLength       =   6
            TabIndex        =   20
            Top             =   510
            Width           =   1095
         End
         Begin VB.CommandButton cmdUpdate 
            BackColor       =   &H00B99D7F&
            Caption         =   "&Update"
            Height          =   345
            Index           =   1
            Left            =   2340
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   1590
            Width           =   855
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "BUILDING_ID ��� :"
            Height          =   195
            Index           =   3
            Left            =   495
            TabIndex        =   23
            Top             =   570
            Width           =   1410
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "BUILDING_ID ���� :"
            Height          =   195
            Index           =   2
            Left            =   450
            TabIndex        =   22
            Top             =   960
            Width           =   1455
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "��� LAND_ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Index           =   0
         Left            =   30
         TabIndex        =   9
         Top             =   2100
         Width           =   3285
         Begin VB.CommandButton cmdUpdate 
            BackColor       =   &H00B99D7F&
            Caption         =   "&Update"
            Height          =   345
            Index           =   0
            Left            =   2340
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   1590
            Width           =   855
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   1
            Left            =   1560
            MaxLength       =   6
            TabIndex        =   12
            Top             =   900
            Width           =   825
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   0
            Left            =   1560
            MaxLength       =   10
            TabIndex        =   11
            Top             =   510
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   3
            Left            =   2610
            MaxLength       =   3
            TabIndex        =   10
            Top             =   900
            Width           =   525
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "LAND_ID ���� :"
            Height          =   195
            Index           =   1
            Left            =   360
            TabIndex        =   16
            Top             =   930
            Width           =   1125
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "LAND_ID  ��� :"
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   15
            Top             =   570
            Width           =   1125
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "/"
            Height          =   195
            Index           =   0
            Left            =   2460
            TabIndex        =   14
            Top             =   960
            Width           =   75
         End
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   2025
         Index           =   2
         Left            =   3330
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   2100
         Width           =   3165
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_LandData 
         Height          =   1755
         Index           =   0
         Left            =   30
         TabIndex        =   17
         Top             =   330
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   3096
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         BackColorFixed  =   12164479
         BackColorSel    =   13977088
         BackColorBkg    =   14933984
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_LandData 
         Height          =   1755
         Index           =   1
         Left            =   -74970
         TabIndex        =   25
         Top             =   330
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   3096
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         BackColorFixed  =   12164479
         BackColorSel    =   13977088
         BackColorBkg    =   14737632
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_LandData 
         Height          =   1755
         Index           =   2
         Left            =   -74970
         TabIndex        =   33
         Top             =   330
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   3096
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         BackColorFixed  =   12164479
         BackColorSel    =   13977088
         BackColorBkg    =   14737632
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   90
      Top             =   810
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Microsoft Access Databases (*.mdb)|*.mdb"
      InitDir         =   "c:\"
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid_Ownership 
      Height          =   2685
      Left            =   0
      TabIndex        =   6
      Top             =   2910
      Width           =   2445
      _ExtentX        =   4313
      _ExtentY        =   4736
      _Version        =   393216
      FixedCols       =   0
      BackColorFixed  =   12164479
      BackColorBkg    =   12632256
      GridColor       =   12164479
      GridColorFixed  =   14737632
      HighLight       =   2
      GridLinesFixed  =   1
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   3
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFFFFF&
      Caption         =   " ���� ?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1455
      Left            =   0
      TabIndex        =   0
      Top             =   1440
      Width           =   2445
      Begin VB.CommandButton cmdSearch 
         BackColor       =   &H00B99D7F&
         Caption         =   "&Search"
         Height          =   315
         Left            =   1530
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1050
         Width           =   795
      End
      Begin VB.TextBox txtSurname 
         Height          =   285
         Left            =   870
         TabIndex        =   2
         Top             =   690
         Width           =   1485
      End
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   870
         TabIndex        =   1
         Top             =   300
         Width           =   1485
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���ʡ�� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   75
         TabIndex        =   4
         Top             =   720
         Width           =   750
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "���� :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   222
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   3
         Top             =   360
         Width           =   345
      End
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "����"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B67159&
      Height          =   225
      Index           =   2
      Left            =   7290
      TabIndex        =   38
      Top             =   1110
      Width           =   1110
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "�ç���͹"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B67159&
      Height          =   225
      Index           =   1
      Left            =   4680
      TabIndex        =   37
      Top             =   1110
      Width           =   1110
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "���Թ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B67159&
      Height          =   225
      Index           =   0
      Left            =   2160
      TabIndex        =   36
      Top             =   1110
      Width           =   1110
   End
   Begin VB.Menu mnu_File 
      Caption         =   "File"
      Begin VB.Menu mnuOpen 
         Caption         =   "Connect Database..."
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_Exit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "frmUpdate_LandID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Cnn As ADODB.Connection
Dim cmd As ADODB.Command
Dim Rs As ADODB.Recordset
Dim RsSearch As ADODB.Recordset
Dim RsLand As ADODB.Recordset
Dim Errloop As ADODB.Error
'Dim PathMDB As String

Private Sub ConnectDatabase()
        On Error GoTo ErrConnect
        Set RsLand = New ADODB.Recordset
                With Cnn
                        If .State = adStateOpen Then .Close
                        .ConnectionString = "Provider=SQLNCLI;Server=" & ReadINI("path file", "servername") & ";Database=" & ReadINI("path file", "database") & ";UID=" & ReadINI("path file", "username") & ";PWD=" & GetRegis & ";"
                        .ConnectionTimeout = 20
                        .CursorLocation = adUseClient
'                        .Properties("Prompt") = adPromptAlways
                        .Open
                End With
                Grid_Ownership.Rows = 2
                Grid_Ownership.Clear
                Call SetGrid_Ownership
                Grid_LandData(0).Rows = 2
                Grid_LandData(0).Clear
                Call SetGrid_Landdata(3)
                Text1(0).Text = ""
                Text1(1).Text = ""
        Exit Sub
ErrConnect:
        MsgBox Err.Description
End Sub

Private Sub cmdSearch_Click()
        Dim strSQL As String
        
        On Error GoTo ErrSearch
'        If Cnn.State = adStateClosed Then
'                MsgBox "�ѧ����� connect database ��������� ������ .....", vbCritical, "��������պ Search �Ҿ���ʧ �͡ ���� ���� #@!%"
'                Exit Sub
'        End If
        Me.MousePointer = 11
        Set RsSearch = Cnn.Execute("exec sp_search_regenerate '" & Trim$(txtName.Text) & "','" & Trim$(txtSurname.Text) & "'", , adCmdUnknown)
        If RsSearch.State <> adStateClosed And RsSearch.RecordCount > 0 Then
                Set Grid_Ownership.DataSource = RsSearch
        End If
        Call SetGrid_Ownership
        Me.MousePointer = 0
        Exit Sub
ErrSearch:
        Me.MousePointer = 0
        For Each Errloop In Cnn.Errors
                Debug.Print Errloop.NativeError
                Debug.Print Errloop.Description
        Next
End Sub

Private Sub cmdUpdate_Click(Index As Integer)
        Dim strSQL As String
        Dim strLandID As String
'        Dim arrHis() As String
'        Dim i As Integer
        On Error GoTo ErrUpdate
        If Cnn.State = adStateOpen Then
                Cnn.BeginTrans
         Select Case Index
                    Case 0 '���Թ  ++++++++++++++++++++++++++++++++++++++++++++++++++++
                        Select Case Len(Trim$(Text1(3).Text))
                                Case 0
                                        strLandID = Trim$(Text1(1).Text)
                                Case 3
                                        strLandID = Trim$(Text1(1).Text) & "/" & Trim$(Text1(3).Text)
                                Case Else
                                        Text1(3).SelStart = 0
                                        Text1(3).SelLength = Len(Text1(3).Text)
                                        Text1(3).SetFocus
                                        MsgBox "��س�����Ţ㹪�ͧ���ú 3 ��ѡ����"
                                        Exit Sub
                        End Select
                        If Len(Trim$(Text1(3).Text)) = 3 Then
                                strLandID = Trim$(Text1(1).Text) & "/" & Trim$(Text1(3).Text)
                        Else
                                strLandID = Trim$(Text1(1).Text)
                        End If
                        Text1(2).Text = Empty
                        If Len(Trim$(Text1(1).Text)) >= 6 Then
                                
'                                Set RSHistory = New ADODB.Recordset
                                'LANDDATA
                                strSQL = "SELECT LAND_ID FROM LANDDATA WITH (ROWLOCK) WHERE LAND_ID='" & Trim$(Text1(0).Text) & "'"
                                Call Query_Land_ID(strSQL, Rs)
                                If Rs.State <> adStateClosed And Rs.RecordCount > 0 Then
                                        strSQL = "UPDATE LANDDATA SET LAND_ID='" & strLandID & "' WHERE LAND_ID='" & Trim$(Text1(0).Text) & "'"
                                        Call Update_LAND_ID(strSQL, "LANDDATA", cmd, False)
                                        Text1(2).Text = "Update Complete"
                                Else
                                        Text1(2).Text = "���ҧ LANDDATA ����ա����䢢�����"
                                End If
                                Cnn.CommitTrans
                                Text1(0).Text = Empty
                                Text1(1).Text = Empty
                                If RsLand.State = adStateOpen Then
                                        RsLand.Requery
                                        Set Grid_LandData(0).DataSource = RsLand
                                        Call SetGrid_Landdata(0)
                                End If
                        Else
                                MsgBox "��س������ LAND_ID ���١��ͧ���� (��� LAND_ID �դ�����������¡��� 6 ����ѡ��) !!!"
                        End If
                
                
                Case 1 '�ç���͹   ++++++++++++++++++++++++++++++++++++++++++++++++++++
                        If Len(Trim$(Text1(5).Text)) < 6 Then
                                MsgBox "��س���������ç���͹㹪�ͧ���ú 6 ��ѡ����"
                                Exit Sub
                        End If
                        strLandID = Trim$(Text1(5).Text)
                        Text1(7).Text = Empty
                        Set cmd = New ADODB.Command
                        Set Rs = New ADODB.Recordset
                                'BUILDINGDATA
                                strSQL = "SELECT BUILDING_ID FROM BUILDINGDATA WITH (ROWLOCK) WHERE BUILDING_ID='" & Trim$(Text1(4).Text) & "'"
                                Call Query_Land_ID(strSQL, Rs)
                                If Rs.State <> adStateClosed And Rs.RecordCount > 0 Then
                                        strSQL = "UPDATE BUILDINGDATA SET BUILDING_ID='" & strLandID & "' WHERE BUILDING_ID='" & Trim$(Text1(4).Text) & "'"
                                        Call Update_LAND_ID(strSQL, "BUILDINGDATA", cmd, False)
                                        Text1(7).Text = "Update Complete"
                                Else
                                        Text1(7).Text = "���ҧ BUILDINGDATA ����ա����䢢�����"
                                End If
                        Cnn.CommitTrans
                        Text1(4).Text = Empty
                        Text1(5).Text = Empty
                        If RsLand.State = adStateOpen Then
                                RsLand.Requery
                                Set Grid_LandData(1).DataSource = RsLand
                                Call SetGrid_Landdata(1)
                        End If
                
                
                Case 2 '���� +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        If Len(Trim$(Text1(11).Text)) < 6 Then
                                MsgBox "��س�������ʻ���㹪�ͧ���ú 6 ��ѡ����"
                                Exit Sub
                        End If
                        strLandID = Trim$(Text1(11).Text)
                        Text1(8).Text = Empty
                        Set cmd = New ADODB.Command
                        Set Rs = New ADODB.Recordset
                        'SIGNBORDDATA
                        strSQL = "SELECT SIGNBORD_ID FROM SIGNBORDDATA WITH (ROWLOCK)  WHERE SIGNBORD_ID='" & Trim$(Text1(10).Text) & "'"
                        Call Query_Land_ID(strSQL, Rs)
                        If Rs.State <> adStateClosed And Rs.RecordCount > 0 Then
                                strSQL = "UPDATE SIGNBORDDATA SET SIGNBORD_ID='" & strLandID & "' WHERE SIGNBORD_ID='" & Trim$(Text1(10).Text) & "'"
                                Call Update_LAND_ID(strSQL, "SIGNBORDDATA", cmd, False)
                                Text1(8).Text = "Update Complete"
                        Else
                                Text1(8).Text = "���ҧ SIGNBORDDATA ����ա����䢢�����"
                        End If
                        Cnn.CommitTrans
                        Text1(10).Text = Empty
                        Text1(11).Text = Empty
                        If RsLand.State = adStateOpen Then
                                RsLand.Requery
                                Set Grid_LandData(2).DataSource = RsLand
                                Call SetGrid_Landdata(2)
                        End If
                End Select
                End If
        Exit Sub
ErrUpdate:
        Cnn.RollbackTrans
'        MsgBox Err.Number
        Text1(Index).Text = Empty
        For Each Errloop In Cnn.Errors
                'Debug.Print Errloop.Description
                If Errloop.NativeError = -105121349 Then MsgBox "������ѡ���١��Ѻ��ا ����ӡѺ������ѡ㹰ҹ������ !!!"
                If Errloop.NativeError = -72156238 Then MsgBox "�������ö��Ѻ��ا����������� ��س��ͧ��Ѻ��ا��������ա���� !!!"
                MsgBox Errloop.Description
        Next
End Sub

Private Sub Form_Load()
        Set Cnn = New ADODB.Connection
        Set cmd = New ADODB.Command
        Set Rs = New ADODB.Recordset
        Set RsSearch = New ADODB.Recordset
        Set RsLand = New ADODB.Recordset
        Call SetGrid_Ownership
        Call SetGrid_Landdata(3)
        SSTab1.Tab = 0
End Sub
 
Private Sub Update_LAND_ID(TXTSQL As String, NameTable As String, CmdVal As ADODB.Command, Flag As Boolean)
         With CmdVal
                .ActiveConnection = Cnn
                .CommandType = adCmdText
                .CommandTimeout = 90
                .CommandText = TXTSQL
'                .Prepared = True
                .Execute
         End With
End Sub

Private Sub Query_Land_ID(TXTSQL As String, RsVal As ADODB.Recordset)
        On Error GoTo ErrQuery
        With RsVal
                If .State = adStateOpen Then .Close
                .ActiveConnection = Cnn
                .CursorType = adOpenStatic
                .LockType = adLockPessimistic
                .CursorLocation = adUseClient
                .Open TXTSQL
        End With
        Exit Sub
ErrQuery:
        For Each Errloop In Cnn.Errors
                Debug.Print Errloop.NativeError
        Next
        Debug.Print Err.Description
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Set Cnn = Nothing
        Set cmd = Nothing
        Set Rs = Nothing
        Set RsSearch = Nothing
        Set RsLand = Nothing
        Set Errloop = Nothing
End Sub

Private Function FindButtom(strVal As String) As String
        Dim i As Byte
        Dim strTmp As String
        
        On Error GoTo ErrFind
        
        i = 1
        strTmp = Right$(strVal, i)
        If Len(Trim$(strVal)) > 0 And InStr(1, strVal, "-", vbTextCompare) <> 0 Then
                Do While InStr(1, strTmp, "-", vbTextCompare) = 0
                        i = i + 1
                        strTmp = Right$(strVal, i)
                Loop
                FindButtom = strTmp
        End If
        Exit Function
ErrFind:
        MsgBox Err.Description
        Resume Next
End Function

Private Function FindButtom2(strVal As String) As String
        Dim i As Byte
        Dim strTmp As String
        
        On Error GoTo ErrFind
        
        i = 1
        strTmp = Right$(strVal, i)
        If Len(Trim$(strVal)) > 0 And InStr(1, strVal, "/", vbTextCompare) <> 0 Then
                Do While InStr(1, strTmp, "/", vbTextCompare) = 0
                        i = i + 1
                        strTmp = Right$(strVal, i)
                Loop
                FindButtom2 = strTmp
        End If
        Exit Function
ErrFind:
        MsgBox Err.Description
        Resume Next
End Function

Private Sub SetGrid_Ownership()
        With Grid_Ownership
                .ColWidth(0) = 0
                .TextArray(1) = "��¡�ê���": .ColWidth(1) = 2400: .ColAlignmentFixed(1) = 4
        End With
End Sub

Private Sub SetGrid_Landdata(Flag_Type As Byte)
        Select Case Flag_Type
                Case 0
                        With Grid_LandData(0)
                                .TextArray(0) = "�ŧ���Թ": .ColWidth(0) = 1200: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�Ţ���Թ": .ColWidth(2) = 800: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "���": .ColWidth(3) = 800: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "�ҹ": .ColWidth(4) = 800: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "��": .ColWidth(5) = 900: .ColAlignmentFixed(5) = 4
                        End With
                Case 1
                        With Grid_LandData(1)
                                .TextArray(0) = "�����ç���͹": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�ŧ���Թ": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "�Ţ����ҹ": .ColWidth(3) = 800: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "����": .ColWidth(4) = 800: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "�ѡɳ��ç���͹": .ColWidth(5) = 1200: .ColAlignmentFixed(5) = 4
                        End With
                Case 2
                        With Grid_LandData(2)
                                .TextArray(0) = "���ʻ���": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�ŧ���Թ": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "�����ç���͹": .ColWidth(3) = 1000: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "�ӹǹ��ҹ": .ColWidth(4) = 1000: .ColAlignmentFixed(4) = 4: .ColAlignment(4) = 4
                                .TextArray(5) = "���ͻ���": .ColWidth(5) = 3000: .ColAlignmentFixed(5) = 4
                        End With
                Case 3
                        With Grid_LandData(0)
                                .TextArray(0) = "�ŧ���Թ": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�Ţ���Թ": .ColWidth(2) = 800: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "���": .ColWidth(3) = 800: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "�ҹ": .ColWidth(4) = 800: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "��": .ColWidth(5) = 900: .ColAlignmentFixed(5) = 4
                        End With
                        With Grid_LandData(1)
                                .TextArray(0) = "�����ç���͹": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�ŧ���Թ": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "�Ţ����ҹ": .ColWidth(3) = 800: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "����": .ColWidth(4) = 800: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "�ѡɳ��ç���͹": .ColWidth(5) = 1200: .ColAlignmentFixed(5) = 4
                        End With
                        With Grid_LandData(2)
                                .TextArray(0) = "���ʻ���": .ColWidth(0) = 1150: .ColAlignmentFixed(0) = 4
                                
                                .TextArray(1) = "⫹���͡": .ColWidth(1) = 800: .ColAlignmentFixed(1) = 4: .ColAlignment(1) = 4
                                .TextArray(2) = "�ŧ���Թ": .ColWidth(2) = 1200: .ColAlignmentFixed(2) = 4: .ColAlignment(2) = 7
                                .TextArray(3) = "�����ç���͹": .ColWidth(3) = 800: .ColAlignmentFixed(3) = 4
                                .TextArray(4) = "�ӹǹ��ҹ": .ColWidth(4) = 800: .ColAlignmentFixed(4) = 4
                                .TextArray(5) = "���ͻ���": .ColWidth(5) = 1500: .ColAlignmentFixed(5) = 4
                        End With
            End Select
End Sub

Private Sub Grid_LandData_Click(Index As Integer)
        Select Case Index
                Case 0
                        If Grid_LandData(Index).Rows = 1 Then Exit Sub
                        Text1(0).Text = Grid_LandData(Index).TextMatrix(Grid_LandData(Index).Row, 0)
                        Text1(1).Text = Empty
                        Text1(3).Text = Empty
                        Text1(1).SetFocus
                Case 1
                        If Grid_LandData(Index).Rows = 1 Then Exit Sub
                        Text1(4).Text = Grid_LandData(Index).TextMatrix(Grid_LandData(Index).Row, 0)
                        Text1(5).Text = Empty
                        Text1(5).SetFocus
                Case 2
                        If Grid_LandData(Index).Rows = 1 Then Exit Sub
                        Text1(10).Text = Grid_LandData(Index).TextMatrix(Grid_LandData(Index).Row, 0)
                        Text1(11).Text = Empty
                        Text1(11).SetFocus
        End Select
End Sub

Private Sub Grid_Ownership_Click()
        Dim strSQL As String
        
        On Error GoTo ErrGetData
        
        lbFullName.Caption = Grid_Ownership.TextMatrix(Grid_Ownership.Row, 1)
        Select Case SSTab1.Tab
                Case 0 '���Թ
                        If Len(Trim$(Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0))) > 0 Then
                                Set RsLand = Cnn.Execute("exec sp_search_regenerate_ownership " & SSTab1.Tab & ",'" & Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0) & "'", , adCmdUnknown)
                                If RsLand.State <> adStateClosed And RsLand.RecordCount > 0 Then
                                        Set Grid_LandData(0).DataSource = RsLand
                                Else
                                        Grid_LandData(0).Rows = 2
                                        Grid_LandData(0).Clear
                                End If
                        End If
                        Call SetGrid_Landdata(0)
                Case 1 '�ç���͹
                        If Len(Trim$(Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0))) > 0 Then
                                Set RsLand = Cnn.Execute("exec sp_search_regenerate_ownership " & SSTab1.Tab & ",'" & Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0) & "'", , adCmdUnknown)
                                If RsLand.State <> adStateClosed And RsLand.RecordCount > 0 Then
                                        Set Grid_LandData(1).DataSource = RsLand
                                Else
                                        Grid_LandData(1).Rows = 2
                                        Grid_LandData(1).Clear
                                End If
                        End If
                        Call SetGrid_Landdata(1)
                Case 2 '����
                        If Len(Trim$(Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0))) > 0 Then
                                Set RsLand = Cnn.Execute("exec sp_search_regenerate_ownership " & SSTab1.Tab & ",'" & Grid_Ownership.TextMatrix(Grid_Ownership.Row, 0) & "'", , adCmdUnknown)
                                If RsLand.State <> adStateClosed And RsLand.RecordCount > 0 Then
                                        Set Grid_LandData(2).DataSource = RsLand
                                Else
                                        Grid_LandData(2).Rows = 2
                                        Grid_LandData(2).Clear
                                End If
                        End If
                        Call SetGrid_Landdata(2)
        End Select
        Exit Sub
ErrGetData:
        MsgBox Err.Description
End Sub

Private Sub Label2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
Label2(Index).Top = Label2(Index).Top + 40
End Sub

Private Sub Label2_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
Label2(Index).Top = 1110
                      SSTab1.Tab = Index
End Sub

Private Sub mnu_Exit_Click()
        Set Cnn = Nothing
        Set cmd = Nothing
        Set Rs = Nothing
        Set RsSearch = Nothing
        Set RsLand = Nothing
        Set Errloop = Nothing
        Unload Me
End Sub

Private Sub mnuAbout_Click()
        frmAbout.Show vbModal
End Sub

Private Sub mnuOpen_Click()
'    CommonDialog1.CancelError = True
'
'    On Error GoTo ErrHandler
'    CommonDialog1.FilterIndex = 2
'    CommonDialog1.ShowOpen
'    PathMDB = CommonDialog1.FileName
    Call ConnectDatabase
    Exit Sub
ErrHandler:
  'User pressed the Cancel button
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
        Select Case SSTab1.Tab
                Case 0
                        Grid_LandData(1).Rows = 2
                        Grid_LandData(1).Clear
                        Call SetGrid_Landdata(1)
                        Text1(4).Text = Empty
                        Text1(5).Text = Empty
                        Text1(7).Text = Empty
                        Grid_LandData(2).Rows = 2
                        Grid_LandData(2).Clear
                        Call SetGrid_Landdata(2)
                        Text1(8).Text = Empty
                        Text1(10).Text = Empty
                        Text1(11).Text = Empty
                Case 1
                        Grid_LandData(0).Rows = 2
                        Grid_LandData(0).Clear
                        Call SetGrid_Landdata(0)
                        Text1(0).Text = Empty
                        Text1(1).Text = Empty
                        Text1(2).Text = Empty
                        Text1(3).Text = Empty
                        Grid_LandData(2).Rows = 2
                        Grid_LandData(2).Clear
                        Call SetGrid_Landdata(2)
                        Text1(8).Text = Empty
                        Text1(10).Text = Empty
                        Text1(11).Text = Empty
                Case 2
                        Grid_LandData(0).Rows = 2
                        Grid_LandData(0).Clear
                        Call SetGrid_Landdata(0)
                        Text1(0).Text = Empty
                        Text1(1).Text = Empty
                        Text1(2).Text = Empty
                        Text1(3).Text = Empty
                        Grid_LandData(1).Rows = 2
                        Grid_LandData(1).Clear
                        Call SetGrid_Landdata(1)
                        Text1(4).Text = Empty
                        Text1(5).Text = Empty
                        Text1(7).Text = Empty
        End Select
End Sub

Private Sub Text1_Change(Index As Integer)
            Select Case Index
                    Case 0
                            Text1(0).Text = UCase$(Text1(0).Text)
                            Text1(0).SelStart = Len(Text1(0).Text)
                    Case 1
                            Text1(1).Text = UCase$(Text1(1).Text)
                            Text1(1).SelStart = Len(Text1(1).Text)
                    Case 4
                            Text1(4).Text = UCase$(Text1(4).Text)
                            Text1(4).SelStart = Len(Text1(4).Text)
                    Case 5
                            Text1(5).Text = UCase$(Text1(5).Text)
                            Text1(5).SelStart = Len(Text1(5).Text)
                    Case 10
                            Text1(10).Text = UCase$(Text1(10).Text)
                            Text1(10).SelStart = Len(Text1(10).Text)
                    Case 11
                            Text1(11).Text = UCase$(Text1(11).Text)
                            Text1(11).SelStart = Len(Text1(11).Text)
            End Select
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
        Select Case Index
                Case 0
                    If KeyAscii = 13 Then Call cmdUpdate_Click(0)
                Case 5
                    If KeyAscii = 13 Then Call cmdUpdate_Click(1)
                Case 11
                    If KeyAscii = 13 Then Call cmdUpdate_Click(2)
        End Select
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdSearch_Click
End Sub

Private Sub txtSurname_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then Call cmdSearch_Click
End Sub
