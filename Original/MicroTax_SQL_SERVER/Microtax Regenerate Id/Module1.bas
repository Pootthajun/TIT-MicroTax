Attribute VB_Name = "Module1"
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long         ' Note that if you declare the lpData parameter as String, you must pass it By Value.
Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Public Const subkey = "Event.Effect\CLSID"
Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const REG_SZ = 1
Public Const REG_DWORD = 4
Public Const KEY_READ = &H20019

Public Function ReadINI(strsection As String, strKey As String) As String
   Dim strbuffer As String
   Let strbuffer$ = String$(750, Chr$(0&))
   Let ReadINI$ = Left$(strbuffer$, GetPrivateProfileString(strsection$, ByVal LCase$(strKey$), "", strbuffer, Len(strbuffer), App.Path & "\Configs.ini"))
End Function

Public Function GetKeyValue(ByVal KeyValue As String, ByVal KeyValueSize As Long) As String
        If Asc(Mid(KeyValue, KeyValueSize, 1)) = 0 Then
                GetKeyValue = Left(KeyValue, KeyValueSize - 1)
        Else
                GetKeyValue = Left(KeyValue, KeyValueSize)
        End If
End Function

Public Function HexToDecStr(ByVal strVal As String) As String
    Dim strTmp As String
    Dim strVal1 As String
    Dim strVal2 As String
    Dim strResult As String
    Dim n As Integer
    Dim i As Integer
    
    strTmp = Trim$(strVal)
    For i = 1 To Len(strTmp) Step 2
            strVal1 = Mid$(strTmp, i, 1)
            strVal2 = Mid$(strTmp, i + 1, 1)
            n = (ValidHex(strVal1) * 16) + ValidHex(strVal2)
            strResult = strResult & Chr(n)
    Next i
    HexToDecStr = strResult
End Function

Public Function ValidHex(ByVal HexVal As String) As Integer
        Select Case HexVal
                Case "A"
                        ValidHex = 10
                Case "B"
                        ValidHex = 11
                Case "C"
                        ValidHex = 12
                Case "D"
                        ValidHex = 13
                Case "E"
                        ValidHex = 14
                Case "F"
                        ValidHex = 15
                Case Else
                        ValidHex = CInt(HexVal)
        End Select
End Function

Public Function GetValue_Type(ByVal TypeV As Long, ByVal KeyV As String) As String
        Dim KeyVal As String
        Dim i As Integer
        
        Select Case TypeV
                Case REG_SZ
                        GetValue_Type = KeyV
                Case REG_DWORD
                        For i = Len(KeyV) To 1 Step -1
                                KeyVal = KeyVal + Hex(Asc(Mid(KeyV, i, 1)))
                        Next i
                        KeyVal = Format$("&h" + KeyVal)
                        GetValue_Type = KeyVal
                Case Else
                        Debug.Print "Data not in string format.  Unable to interpret data."
'                        End
        End Select
End Function

Public Function GetRegis() As String
        Dim hregkey As Long
        Dim retval As Long
        Dim datatype As Long  '�Ѻ data type �ҡ�����ҹ���
        Dim tmpVal As String
        Dim keyValSize As Long
        
        tmpVal = Space(255)
        keyValSize = 255
        retval = RegOpenKeyEx(HKEY_CLASSES_ROOT, subkey, 0, KEY_READ, hregkey)
        retval = RegQueryValueEx(hregkey, "Effect ID", 0, datatype, ByVal tmpVal, keyValSize)
        tmpVal = GetKeyValue(tmpVal, keyValSize)
        tmpVal = HexToDecStr(tmpVal)
        tmpVal = GetValue_Type(datatype, tmpVal)
        retval = RegCloseKey(hregkey)
        GetRegis = tmpVal
End Function
